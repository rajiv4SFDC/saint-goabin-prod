declare module "@salesforce/apex/GeoTagCaptureForCheckIn.createGeoTagRecords" {
  export default function createGeoTagRecords(param: {checkInData: any}): Promise<any>;
}
declare module "@salesforce/apex/GeoTagCaptureForCheckIn.createcheckInRecords" {
  export default function createcheckInRecords(param: {checkInData: any}): Promise<any>;
}
declare module "@salesforce/apex/GeoTagCaptureForCheckIn.createcheckOutRecords" {
  export default function createcheckOutRecords(param: {checkInData: any}): Promise<any>;
}
declare module "@salesforce/apex/GeoTagCaptureForCheckIn.createEndDayRecords" {
  export default function createEndDayRecords(param: {checkInData: any}): Promise<any>;
}
declare module "@salesforce/apex/GeoTagCaptureForCheckIn.checkForStartMyDayRecords" {
  export default function checkForStartMyDayRecords(): Promise<any>;
}
declare module "@salesforce/apex/GeoTagCaptureForCheckIn.getCheckInOutRecords" {
  export default function getCheckInOutRecords(): Promise<any>;
}

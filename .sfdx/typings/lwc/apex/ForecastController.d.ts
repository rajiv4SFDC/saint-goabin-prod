declare module "@salesforce/apex/ForecastController.viewForecast" {
  export default function viewForecast(param: {optyId: any}): Promise<any>;
}
declare module "@salesforce/apex/ForecastController.getOptyProd" {
  export default function getOptyProd(param: {optyId: any}): Promise<any>;
}
declare module "@salesforce/apex/ForecastController.getOptyProdCount" {
  export default function getOptyProdCount(param: {optyId: any}): Promise<any>;
}
declare module "@salesforce/apex/ForecastController.findById" {
  export default function findById(param: {optyId: any}): Promise<any>;
}
declare module "@salesforce/apex/ForecastController.getOptyProdYr" {
  export default function getOptyProdYr(param: {optyprodId: any}): Promise<any>;
}
declare module "@salesforce/apex/ForecastController.getOptyProdId" {
  export default function getOptyProdId(param: {optyprodId: any}): Promise<any>;
}
declare module "@salesforce/apex/ForecastController.saveMonthForecasts" {
  export default function saveMonthForecasts(param: {months: any}): Promise<any>;
}
declare module "@salesforce/apex/ForecastController.getProductUOM" {
  export default function getProductUOM(param: {oppProductId: any}): Promise<any>;
}

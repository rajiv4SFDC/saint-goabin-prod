declare module "@salesforce/apex/Account_NOD_OSBalance.nod" {
  export default function nod(param: {region: any, salesOffice: any, Month: any, Year: any}): Promise<any>;
}
declare module "@salesforce/apex/Account_NOD_OSBalance.selectedYear" {
  export default function selectedYear(): Promise<any>;
}
declare module "@salesforce/apex/Account_NOD_OSBalance.selectedMonth" {
  export default function selectedMonth(): Promise<any>;
}
declare module "@salesforce/apex/Account_NOD_OSBalance.accountRegions" {
  export default function accountRegions(): Promise<any>;
}
declare module "@salesforce/apex/Account_NOD_OSBalance.salesOfficeNames" {
  export default function salesOfficeNames(): Promise<any>;
}

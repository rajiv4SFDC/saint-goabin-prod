declare module "@salesforce/apex/FetchRegion.fetchRecord" {
  export default function fetchRecord(param: {Month: any, Year: any, seletedRegion: any}): Promise<any>;
}
declare module "@salesforce/apex/FetchRegion.selectedYear" {
  export default function selectedYear(): Promise<any>;
}
declare module "@salesforce/apex/FetchRegion.selectedMonth" {
  export default function selectedMonth(): Promise<any>;
}

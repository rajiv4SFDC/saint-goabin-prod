declare module "@salesforce/apex/FilePreviewController.getRelatedContentVersionIds" {
  export default function getRelatedContentVersionIds(param: {opptyId: any}): Promise<any>;
}

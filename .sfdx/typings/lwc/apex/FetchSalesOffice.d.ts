declare module "@salesforce/apex/FetchSalesOffice.fetchRecord" {
  export default function fetchRecord(param: {ToDate: any, EndDate: any, selectedsoffice: any}): Promise<any>;
}
declare module "@salesforce/apex/FetchSalesOffice.fetchingRecords" {
  export default function fetchingRecords(param: {selectedMonth: any, selectedYear: any, selectedsoffice: any}): Promise<any>;
}
declare module "@salesforce/apex/FetchSalesOffice.salesOfficeNames" {
  export default function salesOfficeNames(): Promise<any>;
}
declare module "@salesforce/apex/FetchSalesOffice.selectedMonth" {
  export default function selectedMonth(): Promise<any>;
}
declare module "@salesforce/apex/FetchSalesOffice.selectedYear" {
  export default function selectedYear(): Promise<any>;
}

declare module "@salesforce/apex/AccountForecast.SaveAccounts" {
  export default function SaveAccounts(param: {accountForecastList: any, selectedYears: any, accountForecastListOld: any}): Promise<any>;
}
declare module "@salesforce/apex/AccountForecast.savemonthlyPlan" {
  export default function savemonthlyPlan(param: {monthlyPlanList: any, accountForecastId: any}): Promise<any>;
}
declare module "@salesforce/apex/AccountForecast.getAllAttributesForMonthlyPlan" {
  export default function getAllAttributesForMonthlyPlan(param: {accountForecastId: any}): Promise<any>;
}
declare module "@salesforce/apex/AccountForecast.selectedYear" {
  export default function selectedYear(): Promise<any>;
}
declare module "@salesforce/apex/AccountForecast.productCategories" {
  export default function productCategories(): Promise<any>;
}
declare module "@salesforce/apex/AccountForecast.getAllAccountForecast" {
  export default function getAllAccountForecast(param: {yearSelected: any}): Promise<any>;
}
declare module "@salesforce/apex/AccountForecast.submitAndSaveAccounts" {
  export default function submitAndSaveAccounts(param: {accountForecastList: any, selectedYears: any, accountForecastListOld: any}): Promise<any>;
}
declare module "@salesforce/apex/AccountForecast.submitAndSavemonthlyPlan" {
  export default function submitAndSavemonthlyPlan(param: {monthlyPlanList: any, accountForecastId: any, monthlyPlanListOld: any}): Promise<any>;
}
declare module "@salesforce/apex/AccountForecast.getMonthlyPlan" {
  export default function getMonthlyPlan(param: {account: any, productCat: any, accountForecastId: any}): Promise<any>;
}

declare module "@salesforce/apex/OpportunityProductsController.getOpportunityDetail" {
  export default function getOpportunityDetail(param: {oppId: any}): Promise<any>;
}
declare module "@salesforce/apex/OpportunityProductsController.getOpportunityProductDetail" {
  export default function getOpportunityProductDetail(param: {opportunityProductId: any}): Promise<any>;
}
declare module "@salesforce/apex/OpportunityProductsController.getNewProducts" {
  export default function getNewProducts(param: {MasterProduct: any}): Promise<any>;
}
declare module "@salesforce/apex/OpportunityProductsController.upsertOppProductAndLineItems" {
  export default function upsertOppProductAndLineItems(param: {prdlist: any, parentPrdId: any, opportunityid: any, masterPrd: any, glassPrd: any, oppProduct: any, pdType: any, customername: any}): Promise<any>;
}
declare module "@salesforce/apex/OpportunityProductsController.getSpecificProducts" {
  export default function getSpecificProducts(param: {selectedProdId: any, parentPrdId: any}): Promise<any>;
}

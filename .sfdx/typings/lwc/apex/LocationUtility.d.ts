declare module "@salesforce/apex/LocationUtility.getNearByProjectsFrmDB" {
  export default function getNearByProjectsFrmDB(param: {Lat: any, Lon: any, compare: any, currentProjectId: any}): Promise<any>;
}
declare module "@salesforce/apex/LocationUtility.setCapturedGeoLocation" {
  export default function setCapturedGeoLocation(param: {Lat: any, Lon: any, currentProjectId: any}): Promise<any>;
}
declare module "@salesforce/apex/LocationUtility.getCurrentProjectDetail" {
  export default function getCurrentProjectDetail(param: {currentProjectId: any}): Promise<any>;
}

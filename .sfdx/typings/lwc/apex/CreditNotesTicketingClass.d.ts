declare module "@salesforce/apex/CreditNotesTicketingClass.getSelectedInvoices" {
  export default function getSelectedInvoices(param: {invoiceName: any}): Promise<any>;
}
declare module "@salesforce/apex/CreditNotesTicketingClass.getPriceRequestRecords" {
  export default function getPriceRequestRecords(param: {priceRequestRecords: any}): Promise<any>;
}
declare module "@salesforce/apex/CreditNotesTicketingClass.getPriceRequestPandPRecords" {
  export default function getPriceRequestPandPRecords(param: {priceRequestPandPRecords: any}): Promise<any>;
}
declare module "@salesforce/apex/CreditNotesTicketingClass.getInsertedRecords" {
  export default function getInsertedRecords(param: {sendCurrentRecordId: any}): Promise<any>;
}
declare module "@salesforce/apex/CreditNotesTicketingClass.saveCreditNotesTickt" {
  export default function saveCreditNotesTickt(param: {creditNotesTicketingListParam: any}): Promise<any>;
}
declare module "@salesforce/apex/CreditNotesTicketingClass.editCreditNotesTickt" {
  export default function editCreditNotesTickt(param: {creditNotesTicketingListParam: any, creditRecordID: any}): Promise<any>;
}

declare module "@salesforce/apex/FetchWelcomeUserName.fetchUser" {
  export default function fetchUser(): Promise<any>;
}

declare module "@salesforce/apex/customLookUpController_product.fetchLookUpValues" {
  export default function fetchLookUpValues(param: {searchKeyWord: any, ObjectName: any, searchfilter: any}): Promise<any>;
}
declare module "@salesforce/apex/customLookUpController_product.doFetchPriceRequest" {
  export default function doFetchPriceRequest(param: {accId: any, proId: any}): Promise<any>;
}

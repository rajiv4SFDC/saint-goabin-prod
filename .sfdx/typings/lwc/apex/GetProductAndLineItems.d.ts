declare module "@salesforce/apex/GetProductAndLineItems.getOppProductRecords" {
  export default function getOppProductRecords(param: {oppProductId: any}): Promise<any>;
}
declare module "@salesforce/apex/GetProductAndLineItems.getProductLineItemRecords" {
  export default function getProductLineItemRecords(param: {proId: any}): Promise<any>;
}
declare module "@salesforce/apex/GetProductAndLineItems.createRecord" {
  export default function createRecord(param: {pr: any, opli: any}): Promise<any>;
}
declare module "@salesforce/apex/GetProductAndLineItems.returnValues" {
  export default function returnValues(param: {priceRequestId: any}): Promise<any>;
}

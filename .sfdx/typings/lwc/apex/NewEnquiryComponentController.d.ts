declare module "@salesforce/apex/NewEnquiryComponentController.getSalesAreaValues" {
  export default function getSalesAreaValues(): Promise<any>;
}
declare module "@salesforce/apex/NewEnquiryComponentController.getBillToCustomers2" {
  export default function getBillToCustomers2(param: {salesarea: any, ifORS: any}): Promise<any>;
}
declare module "@salesforce/apex/NewEnquiryComponentController.getShipToCustomers" {
  export default function getShipToCustomers(param: {accountId: any}): Promise<any>;
}
declare module "@salesforce/apex/NewEnquiryComponentController.getEmptyLineItem" {
  export default function getEmptyLineItem(): Promise<any>;
}
declare module "@salesforce/apex/NewEnquiryComponentController.displayORSInformation" {
  export default function displayORSInformation(): Promise<any>;
}
declare module "@salesforce/apex/NewEnquiryComponentController.getCategoryChange" {
  export default function getCategoryChange(param: {categoryName: any}): Promise<any>;
}
declare module "@salesforce/apex/NewEnquiryComponentController.getProductChange" {
  export default function getProductChange(param: {productName: any}): Promise<any>;
}
declare module "@salesforce/apex/NewEnquiryComponentController.getLengthAndWidth" {
  export default function getLengthAndWidth(param: {productName: any, category: any}): Promise<any>;
}
declare module "@salesforce/apex/NewEnquiryComponentController.getPackType" {
  export default function getPackType(param: {productName: any}): Promise<any>;
}
declare module "@salesforce/apex/NewEnquiryComponentController.getonPriceChange" {
  export default function getonPriceChange(param: {productId: any, region: any, priceType: any, prdCat: any, accountid: any}): Promise<any>;
}
declare module "@salesforce/apex/NewEnquiryComponentController.saveEnquiryRec" {
  export default function saveEnquiryRec(param: {region: any, billToCustomer: any, shipToCustomer: any}): Promise<any>;
}
declare module "@salesforce/apex/NewEnquiryComponentController.getEnquiryRecord" {
  export default function getEnquiryRecord(param: {recId: any}): Promise<any>;
}
declare module "@salesforce/apex/NewEnquiryComponentController.updateEnqRecord" {
  export default function updateEnqRecord(param: {enquRecId: any, regionVal: any, billToStr1: any, shipToStr1: any}): Promise<any>;
}
declare module "@salesforce/apex/NewEnquiryComponentController.saveLineItemRec" {
  export default function saveLineItemRec(param: {prdCat: any, prdName: any, prdThick: any, prdlength: any, packTypeTons: any, prdCases: any, UOM: any, prdlistPriceCst: any, prdbasePrice2: any, priceVar: any, sp_requestQuanitity: any, sp_priceRequestCst: any, validFrom: any, validTo: any, remarks: any, enqRecId: any, orderQuanitity: any, recId: any, Standardpriceperunit: any, priceRequest: any, Required_Price_per_Sq_M: any, submitRecStatus: any, LstInvRate: any}): Promise<any>;
}
declare module "@salesforce/apex/NewEnquiryComponentController.getExistingEnqLineitemRec" {
  export default function getExistingEnqLineitemRec(param: {recId: any}): Promise<any>;
}
declare module "@salesforce/apex/NewEnquiryComponentController.getLineItemsIds" {
  export default function getLineItemsIds(param: {enqRecId: any}): Promise<any>;
}
declare module "@salesforce/apex/NewEnquiryComponentController.addrow" {
  export default function addrow(): Promise<any>;
}
declare module "@salesforce/apex/NewEnquiryComponentController.displayExistingLineItems" {
  export default function displayExistingLineItems(param: {enqRecId: any}): Promise<any>;
}
declare module "@salesforce/apex/NewEnquiryComponentController.deleteEnqLineItem" {
  export default function deleteEnqLineItem(param: {RecId: any}): Promise<any>;
}
declare module "@salesforce/apex/NewEnquiryComponentController.getPriceRequestOnSearch" {
  export default function getPriceRequestOnSearch(param: {prdName: any, salesArea: any, billToCustomerId: any, prdCategory: any}): Promise<any>;
}
declare module "@salesforce/apex/NewEnquiryComponentController.savePRrecord" {
  export default function savePRrecord(param: {PRrec: any, priceType: any, Sales_Office: any, Product_Category: any, selectedCustomerRec: any, fromDate: any, Todate: any, requiredPrice: any, remarks: any, enqRecId: any, selectedProduct: any, quantityInSqMt: any, requiredPriceFloat: any, productLenghtWidth: any, approvalLevel: any, tierPrice: any}): Promise<any>;
}
declare module "@salesforce/apex/NewEnquiryComponentController.GetFieldPikclistValues" {
  export default function GetFieldPikclistValues(param: {ObjectApi_name: any, picklistField: any}): Promise<any>;
}
declare module "@salesforce/apex/NewEnquiryComponentController.getAccStatus" {
  export default function getAccStatus(param: {EnqId: any}): Promise<any>;
}
declare module "@salesforce/apex/NewEnquiryComponentController.submitEnquiry" {
  export default function submitEnquiry(param: {recid: any}): Promise<any>;
}
declare module "@salesforce/apex/NewEnquiryComponentController.displayRecentInvoiceRate" {
  export default function displayRecentInvoiceRate(param: {accId: any, productName: any}): Promise<any>;
}
declare module "@salesforce/apex/NewEnquiryComponentController.displayProductstockInformation" {
  export default function displayProductstockInformation(param: {productName: any, ProductDimentions: any}): Promise<any>;
}
declare module "@salesforce/apex/NewEnquiryComponentController.getonPriceChangeTierPrice" {
  export default function getonPriceChangeTierPrice(param: {productId: any, region: any, priceType: any, prdCat: any, accountid: any}): Promise<any>;
}
declare module "@salesforce/apex/NewEnquiryComponentController.displayEnquiryLineItemStatusRec" {
  export default function displayEnquiryLineItemStatusRec(param: {enlId: any}): Promise<any>;
}

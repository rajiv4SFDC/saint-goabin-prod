declare module "@salesforce/label/c.Account_Forecast_Years" {
    var Account_Forecast_Years: string;
    export default Account_Forecast_Years;
}
declare module "@salesforce/label/c.All_old_month_forecasting" {
    var All_old_month_forecasting: string;
    export default All_old_month_forecasting;
}
declare module "@salesforce/label/c.Dummy_opportunity_product_id" {
    var Dummy_opportunity_product_id: string;
    export default Dummy_opportunity_product_id;
}
declare module "@salesforce/label/c.DynamicMessage" {
    var DynamicMessage: string;
    export default DynamicMessage;
}
declare module "@salesforce/label/c.End_Year" {
    var End_Year: string;
    export default End_Year;
}
declare module "@salesforce/label/c.Freight" {
    var Freight: string;
    export default Freight;
}
declare module "@salesforce/label/c.Inspire_Max_Price" {
    var Inspire_Max_Price: string;
    export default Inspire_Max_Price;
}
declare module "@salesforce/label/c.Inspire_Min_Price" {
    var Inspire_Min_Price: string;
    export default Inspire_Min_Price;
}
declare module "@salesforce/label/c.Installation_Cost" {
    var Installation_Cost: string;
    export default Installation_Cost;
}
declare module "@salesforce/label/c.Lower_Limit_clear_glass_3mm_3_5mm_4mm_5mm_6mm" {
    var Lower_Limit_clear_glass_3mm_3_5mm_4mm_5mm_6mm: string;
    export default Lower_Limit_clear_glass_3mm_3_5mm_4mm_5mm_6mm;
}
declare module "@salesforce/label/c.Lower_Limit_clear_glass_8mm_10mm_12mm" {
    var Lower_Limit_clear_glass_8mm_10mm_12mm: string;
    export default Lower_Limit_clear_glass_8mm_10mm_12mm;
}
declare module "@salesforce/label/c.OpportunityTrigger" {
    var OpportunityTrigger: string;
    export default OpportunityTrigger;
}
declare module "@salesforce/label/c.Parsol_Lower_limit" {
    var Parsol_Lower_limit: string;
    export default Parsol_Lower_limit;
}
declare module "@salesforce/label/c.Parsol_Upper_Limit" {
    var Parsol_Upper_Limit: string;
    export default Parsol_Upper_Limit;
}
declare module "@salesforce/label/c.StartYear" {
    var StartYear: string;
    export default StartYear;
}
declare module "@salesforce/label/c.TargetActualBackUpdateOpenWindow" {
    var TargetActualBackUpdateOpenWindow: string;
    export default TargetActualBackUpdateOpenWindow;
}
declare module "@salesforce/label/c.Tax" {
    var Tax: string;
    export default Tax;
}
declare module "@salesforce/label/c.Upper_Limit_Clear_Class" {
    var Upper_Limit_Clear_Class: string;
    export default Upper_Limit_Clear_Class;
}
declare module "@salesforce/label/c.Upper_Limit_Clear_Class_8mm_10mm_12mm" {
    var Upper_Limit_Clear_Class_8mm_10mm_12mm: string;
    export default Upper_Limit_Clear_Class_8mm_10mm_12mm;
}
declare module "@salesforce/label/c.ship_to_customer_object_id" {
    var ship_to_customer_object_id: string;
    export default ship_to_customer_object_id;
}

// This file is generated as an Apex representation of the
//     corresponding sObject and its fields.
// This read-only file is used by the Apex Language Server to
//     provide code smartness, and is deleted each time you
//     refresh your sObject definitions.
// To edit your sObjects and their fields, edit the corresponding
//     .object-meta.xml and .field-meta.xml files.

global class Contact {
    global List<AcceptedEventRelation> AcceptedEventRelations;
    global Account Account;
    global List<AccountContactRelation> AccountContactRelations;
    global List<AccountContactRole> AccountContactRoles;
    global Id AccountId;
    global List<ActivityHistory> ActivityHistories;
    global String Address__c;
    global List<Asset> Assets;
    global List<AttachedContentDocument> AttachedContentDocuments;
    global List<AttachedContentNote> AttachedContentNotes;
    global List<Attachment> Attachments;
    global List<CampaignMember> CampaignMembers;
    global List<CaseContactRole> CaseContactRoles;
    global List<Case> Cases;
    global Id City__c;
    global City__c City__r;
    global List<CombinedAttachment> CombinedAttachments;
    global List<ContactRequest> ContactRequests;
    global List<ContentDocumentLink> ContentDocumentLinks;
    global List<ContractContactRole> ContractContactRoles;
    global List<Contract> ContractsSigned;
    global List<Lead> ConvertedContact;
    global User CreatedBy;
    global Id CreatedById;
    global Datetime CreatedDate;
    global List<Order> CustomerAuthorizedBy;
    global List<DeclinedEventRelation> DeclinedEventRelations;
    global String Department;
    global String Description;
    global List<DuplicateRecordItem> DuplicateRecordItems;
    global List<Edge_Meet_Attendee__c> Edge_Meet_Attendees__r;
    global String Email;
    global Datetime EmailBouncedDate;
    global String EmailBouncedReason;
    global List<EmailMessageRelation> EmailMessageRelations;
    global List<EmailStatus> EmailStatuses;
    global List<EntitlementContact> EntitlementContacts;
    global List<EventRelation> EventRelations;
    global List<EventWhoRelation> EventWhoRelations;
    global List<Event> Events;
    global String Fax;
    global List<EntitySubscription> FeedSubscriptionsForEntity;
    global List<ContactFeed> Feeds;
    global String FirstName;
    global List<ContentVersion> FirstPublishLocation;
    global List<ContactHistory> Histories;
    global Id Id;
    global Boolean IsDeleted;
    global Boolean IsEmailBounced;
    global String Jigsaw;
    global String JigsawContactId;
    global List<Key_Account__c> Key_Accounts__r;
    global Date LastActivityDate;
    global Datetime LastCURequestDate;
    global Datetime LastCUUpdateDate;
    global User LastModifiedBy;
    global Id LastModifiedById;
    global Datetime LastModifiedDate;
    global String LastName;
    global Datetime LastReferencedDate;
    global Datetime LastViewedDate;
    global List<CampaignMember> LeadOrContact;
    global List<ListEmailIndividualRecipient> ListEmailIndividualRecipients;
    global Address MailingAddress;
    global String MailingCity;
    global String MailingCountry;
    global String MailingGeocodeAccuracy;
    global Double MailingLatitude;
    global Double MailingLongitude;
    global String MailingPostalCode;
    global String MailingState;
    global String MailingStreet;
    global Contact MasterRecord;
    global Id MasterRecordId;
    global List<CaseTeamMember> Member;
    global List<Merchandise_Request__c> Merchandise_Requests__r;
    global String MiddleName;
    global String MobilePhone;
    global String Name;
    global List<Note> Notes;
    global List<NoteAndAttachment> NotesAndAttachments;
    global List<OpenActivity> OpenActivities;
    global List<OpportunityContactRole> OpportunityContactRoles;
    global String OtherPhone;
    global List<OutgoingEmailRelation> OutgoingEmailRelations;
    global User Owner;
    global Id OwnerId;
    global List<FeedItem> Parent;
    global List<UserEmailPreferredPerson> PersonRecord;
    global String Phone;
    global String PhotoUrl;
    global String Pin_Code__c;
    global List<Plant_Visitor__c> Plant_Visitors__r;
    global List<OpenActivity> PrimaryWho;
    global List<ProcessInstance> ProcessInstances;
    global List<ProcessInstanceHistory> ProcessSteps;
    global List<Quote> Quotes;
    global Boolean Recent_uploaded__c;
    global List<RecordActionHistory> RecordActionHistories;
    global List<RecordAction> RecordActions;
    global List<CollaborationGroupRecord> RecordAssociatedGroups;
    global List<ContentDistribution> RelatedRecord;
    global List<Contact> ReportsTo;
    global Id ReportsToId;
    global String Salutation;
    global List<Sample_Request__c> Sample_Request__r;
    global List<ServiceContract> ServiceContracts;
    global List<ContactShare> Shares;
    global Id State__c;
    global State__c State__r;
    global String Suffix;
    global Datetime SystemModstamp;
    global List<TaskRelation> TaskRelations;
    global List<TaskWhoRelation> TaskWhoRelations;
    global List<Task> Tasks;
    global String Title;
    global List<TopicAssignment> TopicAssignments;
    global List<UndecidedEventRelation> UndecidedEventRelations;
    global List<User> Users;
    global List<OutgoingEmail> Who;

    global Contact () 
    {
    }
}
// This file is generated as an Apex representation of the
//     corresponding sObject and its fields.
// This read-only file is used by the Apex Language Server to
//     provide code smartness, and is deleted each time you
//     refresh your sObject definitions.
// To edit your sObjects and their fields, edit the corresponding
//     .object-meta.xml and .field-meta.xml files.

global class ActivityHistory {
    global Account Account;
    global Id AccountId;
    global Date ActivityDate;
    global String ActivitySubtype;
    global String ActivityType;
    global EmailMessage AlternateDetail;
    global Id AlternateDetailId;
    global String CallDisposition;
    global Integer CallDurationInSeconds;
    global String CallObject;
    global String CallType;
    global User CreatedBy;
    global Id CreatedById;
    global Datetime CreatedDate;
    global Date Custom_Due_Date__c;
    global String Description;
    global Integer DurationInMinutes;
    global Datetime EndDateTime;
    global Id Id;
    global Boolean IsAllDayEvent;
    global Boolean IsClosed;
    global Boolean IsDeleted;
    global Boolean IsHighPriority;
    global Boolean IsReminderSet;
    global Boolean IsTask;
    global Boolean IsVisibleInSelfService;
    global Boolean Is_Related_to_Account__c;
    global User LastModifiedBy;
    global Id LastModifiedById;
    global Datetime LastModifiedDate;
    global String Location;
    global String Meeting_Agenda__c;
    global SObject Owner;
    global Id OwnerId;
    global Account PrimaryAccount;
    global Id PrimaryAccountId;
    global SObject PrimaryWho;
    global Id PrimaryWhoId;
    global String Priority;
    global String Region__c;
    global String Related_To_Type__c;
    global Datetime ReminderDateTime;
    global String Sales_Area__c;
    global String Sales_Area_or_Region__c;
    global Datetime StartDateTime;
    global String Status;
    global String Subject;
    global Datetime SystemModstamp;
    global SObject What;
    global Id WhatId;
    global SObject Who;
    global Id WhoId;

    global ActivityHistory () 
    {
    }
}
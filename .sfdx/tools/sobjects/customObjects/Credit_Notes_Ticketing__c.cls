// This file is generated as an Apex representation of the
//     corresponding sObject and its fields.
// This read-only file is used by the Apex Language Server to
//     provide code smartness, and is deleted each time you
//     refresh your sObject definitions.
// To edit your sObjects and their fields, edit the corresponding
//     .object-meta.xml and .field-meta.xml files.

global class Credit_Notes_Ticketing__c {
    global String Account_Sales_Office__c;
    global Id Account__c;
    global Account Account__r;
    global String Approval_Status__c;
    global List<AttachedContentDocument> AttachedContentDocuments;
    global List<AttachedContentNote> AttachedContentNotes;
    global List<Attachment> Attachments;
    global List<CombinedAttachment> CombinedAttachments;
    global List<ContactRequest> ContactRequests;
    global List<ContentDocumentLink> ContentDocumentLinks;
    global User CreatedBy;
    global Id CreatedById;
    global Datetime CreatedDate;
    global Date Date__c;
    global Decimal Difference_In_Rate__c;
    global String Dimension__c;
    global List<DuplicateRecordItem> DuplicateRecordItems;
    global List<EmailMessage> Emails;
    global Id Enquiry__c;
    global Enquiry__c Enquiry__r;
    global List<EventRelation> EventRelations;
    global List<EntitySubscription> FeedSubscriptionsForEntity;
    global List<ContentVersion> FirstPublishLocation;
    global Id Id;
    global String Invoice_Capture__c;
    global String Invoice_Number__c;
    global String Invoice_Rate__c;
    global Id Invoice__c;
    global Invoice__c Invoice__r;
    global Boolean IsDeleted;
    global String Item_Tonnage__c;
    global User LastModifiedBy;
    global Id LastModifiedById;
    global Datetime LastModifiedDate;
    global Datetime LastReferencedDate;
    global Datetime LastViewedDate;
    global Id NSM__c;
    global User NSM__r;
    global String Name;
    global Id National_Head__c;
    global User National_Head__r;
    global List<Note> Notes;
    global List<NoteAndAttachment> NotesAndAttachments;
    global SObject Owner;
    global Id OwnerId;
    global List<FeedItem> Parent;
    global String Plant_Location__c;
    global Id Price_Request__c;
    global Price_Request__c Price_Request__r;
    global Double Price__c;
    global List<ProcessInstance> ProcessInstances;
    global List<ProcessInstanceHistory> ProcessSteps;
    global String Product_Category__c;
    global String Product_Name__c;
    global Id Product__c;
    global Product2 Product__r;
    global String Qty_in_tonnes__c;
    global String Quantity__c;
    global Id RSM__c;
    global User RSM__r;
    global List<RecordActionHistory> RecordActionHistories;
    global List<RecordAction> RecordActions;
    global List<CollaborationGroupRecord> RecordAssociatedGroups;
    global Id Regional_Head__c;
    global User Regional_Head__r;
    global List<FlowRecordRelation> RelatedRecord;
    global String Remarks__c;
    global List<Credit_Notes_Ticketing__Share> Shares;
    global String Sharing_Region__c;
    global Datetime SystemModstamp;
    global List<TaskRelation> TaskRelations;
    global String Territory__c;
    global String Thickness__c;
    global List<TopicAssignment> TopicAssignments;
    global Id price_request_PandP__c;
    global price_request_PandP__c price_request_PandP__r;

    global Credit_Notes_Ticketing__c () 
    {
    }
}
// This file is generated as an Apex representation of the
//     corresponding sObject and its fields.
// This read-only file is used by the Apex Language Server to
//     provide code smartness, and is deleted each time you
//     refresh your sObject definitions.
// To edit your sObjects and their fields, edit the corresponding
//     .object-meta.xml and .field-meta.xml files.

global class Opportunity_Product_Team__c {
    global Double Appropriated_Sales_Sq_M__c;
    global List<AttachedContentDocument> AttachedContentDocuments;
    global List<AttachedContentNote> AttachedContentNotes;
    global List<Attachment> Attachments;
    global List<CombinedAttachment> CombinedAttachments;
    global List<ContactRequest> ContactRequests;
    global List<ContentDocumentLink> ContentDocumentLinks;
    global User CreatedBy;
    global Id CreatedById;
    global Datetime CreatedDate;
    global Double Current_User_Check__c;
    global List<DuplicateRecordItem> DuplicateRecordItems;
    global List<EmailMessage> Emails;
    global Boolean Err_Check__c;
    global List<EventRelation> EventRelations;
    global String External_Id__c;
    global List<EntitySubscription> FeedSubscriptionsForEntity;
    global List<ContentVersion> FirstPublishLocation;
    global List<Opportunity_Product_Team__History> Histories;
    global Id Id;
    global Boolean IsDeleted;
    global User LastModifiedBy;
    global Id LastModifiedById;
    global Datetime LastModifiedDate;
    global String Name;
    global List<Note> Notes;
    global List<NoteAndAttachment> NotesAndAttachments;
    global String Opportunity_Team_Id__c;
    global List<FeedComment> Parent;
    global String Parent_Opportunity_Id__c;
    global Double Planned_Quantity__c;
    global List<ProcessInstance> ProcessInstances;
    global List<ProcessInstanceHistory> ProcessSteps;
    global Id Product_Forecast__c;
    global Product_Forecast__c Product_Forecast__r;
    global Double Quantity_Sq_M__c;
    global List<RecordActionHistory> RecordActionHistories;
    global List<RecordAction> RecordActions;
    global List<CollaborationGroupRecord> RecordAssociatedGroups;
    global List<ContentDistribution> RelatedRecord;
    global String Sales_Area__c;
    global Double Share_pct__c;
    global Datetime SystemModstamp;
    global List<TaskRelation> TaskRelations;
    global String TeamMemberRole__c;
    global List<TopicAssignment> TopicAssignments;
    global Double Unplanned_Quantity__c;
    global Id User__c;
    global User User__r;

    global Opportunity_Product_Team__c () 
    {
    }
}
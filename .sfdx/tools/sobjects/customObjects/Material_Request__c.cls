// This file is generated as an Apex representation of the
//     corresponding sObject and its fields.
// This read-only file is used by the Apex Language Server to
//     provide code smartness, and is deleted each time you
//     refresh your sObject definitions.
// To edit your sObjects and their fields, edit the corresponding
//     .object-meta.xml and .field-meta.xml files.

global class Material_Request__c {
    global String Account_name__c;
    global List<ActivityHistory> ActivityHistories;
    global String Approval_Status__c;
    global String Area__c;
    global List<AttachedContentDocument> AttachedContentDocuments;
    global List<AttachedContentNote> AttachedContentNotes;
    global List<Attachment> Attachments;
    global Id Bill_to_Customer__c;
    global Account Bill_to_Customer__r;
    global Double Cases__c;
    global List<CombinedAttachment> CombinedAttachments;
    global List<ContactRequest> ContactRequests;
    global List<ContentDocumentLink> ContentDocumentLinks;
    global User CreatedBy;
    global Id CreatedById;
    global Datetime CreatedDate;
    global List<DuplicateRecordItem> DuplicateRecordItems;
    global List<EmailMessage> Emails;
    global List<EventRelation> EventRelations;
    global List<Event> Events;
    global Date Expected_Production_Date__c;
    global List<EntitySubscription> FeedSubscriptionsForEntity;
    global List<ContentVersion> FirstPublishLocation;
    global List<Material_Request__History> Histories;
    global Id Id;
    global Boolean IsDeleted;
    global Date LastActivityDate;
    global User LastModifiedBy;
    global Id LastModifiedById;
    global Datetime LastModifiedDate;
    global Datetime LastReferencedDate;
    global Datetime LastViewedDate;
    global String Length__c;
    global String Name;
    global List<Note> Notes;
    global List<NoteAndAttachment> NotesAndAttachments;
    global List<OpenActivity> OpenActivities;
    global SObject Owner;
    global Id OwnerId;
    global String Owner_Name__c;
    global List<FeedItem> Parent;
    global List<ProcessInstance> ProcessInstances;
    global List<ProcessInstanceHistory> ProcessSteps;
    global String Product_Code__c;
    global String Product_Family__c;
    global String Product_Name__c;
    global String Product_Thickness__c;
    global Id Product__c;
    global Product2 Product__r;
    global String Project_Code__c;
    global Id Project_Name__c;
    global Project__c Project_Name__r;
    global String Project__c;
    global List<RecordActionHistory> RecordActionHistories;
    global List<RecordAction> RecordActions;
    global List<CollaborationGroupRecord> RecordAssociatedGroups;
    global String Region__c;
    global Id RegionalHead__c;
    global User RegionalHead__r;
    global Id RegionalManager__c;
    global User RegionalManager__r;
    global Id Regional_Manager__c;
    global User Regional_Manager__r;
    global List<FlowRecordRelation> RelatedRecord;
    global List<OutgoingEmail> RelatedTo;
    global String Remarks__c;
    global String SAP_Code__c;
    global String Sales_Office_Sharing__c;
    global String Sales_office__c;
    global List<Material_Request__Share> Shares;
    global Datetime SystemModstamp;
    global List<TaskRelation> TaskRelations;
    global List<Task> Tasks;
    global String Thickness__c;
    global List<TopicAssignment> TopicAssignments;
    global String Width__c;

    global Material_Request__c () 
    {
    }
}
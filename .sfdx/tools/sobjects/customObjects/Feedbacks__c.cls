// This file is generated as an Apex representation of the
//     corresponding sObject and its fields.
// This read-only file is used by the Apex Language Server to
//     provide code smartness, and is deleted each time you
//     refresh your sObject definitions.
// To edit your sObjects and their fields, edit the corresponding
//     .object-meta.xml and .field-meta.xml files.

global class Feedbacks__c {
    global Double Accessories__c;
    global List<ActivityHistory> ActivityHistories;
    global List<AttachedContentDocument> AttachedContentDocuments;
    global List<AttachedContentNote> AttachedContentNotes;
    global List<Attachment> Attachments;
    global List<CombinedAttachment> CombinedAttachments;
    global String Comments__c;
    global List<ContactRequest> ContactRequests;
    global String Contact_No__c;
    global List<ContentDocumentLink> ContentDocumentLinks;
    global User CreatedBy;
    global Id CreatedById;
    global Datetime CreatedDate;
    global Date Date__c;
    global Double Diomant_Drilling_Bits__c;
    global List<DuplicateRecordItem> DuplicateRecordItems;
    global List<EmailMessage> Emails;
    global List<EventRelation> EventRelations;
    global List<Event> Events;
    global List<EntitySubscription> FeedSubscriptionsForEntity;
    global Double Feedback_from_customer__c;
    global List<ContentVersion> FirstPublishLocation;
    global Double Glass_Fittings_Handling__c;
    global List<Feedbacks__History> Histories;
    global Id Id;
    global Double Independently_Identifying_problem__c;
    global Boolean Installation_Delay__c;
    global Id Installer_Team_Member__c;
    global Key_Account__c Installer_Team_Member__r;
    global Id Installer_Team__c;
    global Key_Account__c Installer_Team__r;
    global Id Installer__c;
    global Key_Account__c Installer__r;
    global Boolean IsDeleted;
    global Date LastActivityDate;
    global User LastModifiedBy;
    global Id LastModifiedById;
    global Datetime LastModifiedDate;
    global Datetime LastReferencedDate;
    global Datetime LastViewedDate;
    global String Location__c;
    global String Name;
    global Double New_and_improvement_in_practice__c;
    global Double No_of_Cubicles_Completed__c;
    global List<Note> Notes;
    global List<NoteAndAttachment> NotesAndAttachments;
    global List<OpenActivity> OpenActivities;
    global Id Opportunity__c;
    global Opportunity Opportunity__r;
    global SObject Owner;
    global Id OwnerId;
    global List<FeedItem> Parent;
    global String Place__c;
    global Double Post_Installation_Check_List__c;
    global Double Pre_Installation_Check_List__c;
    global Double Proactively_resolve_problem__c;
    global List<ProcessInstance> ProcessInstances;
    global List<ProcessInstanceHistory> ProcessSteps;
    global Double Process_on_sites__c;
    global Double Product__c;
    global Id Project__c;
    global Project__c Project__r;
    global List<RecordActionHistory> RecordActionHistories;
    global List<RecordAction> RecordActions;
    global List<CollaborationGroupRecord> RecordAssociatedGroups;
    global RecordType RecordType;
    global Id RecordTypeId;
    global String Region__c;
    global List<FlowRecordRelation> RelatedRecord;
    global List<OutgoingEmail> RelatedTo;
    global Double Responding_the_words__c;
    global String SG_Application_specialist__c;
    global Double SG_suggested_tools_Kit__c;
    global Double Self_Check_List__c;
    global Boolean Service__c;
    global Boolean Shower_Cubicle_quality__c;
    global Double Speed_up_the_Instalaltion__c;
    global Datetime SystemModstamp;
    global List<TaskRelation> TaskRelations;
    global List<Task> Tasks;
    global Boolean Technical_support__c;
    global Double Time_per_Cubicle__c;
    global List<TopicAssignment> TopicAssignments;
    global Double Usage_of_PPE__c;
    global Double Usage_of_Silicon_Sealant__c;
    global Double Usage_of_Small_Ladders__c;
    global Double Use_of_only_SG_approved_material__c;
    global String Where_will_you_rate_Saint_Gobains_Shower__c;
    global Double Without_damaging_the_Tiles_Floor__c;
    global Double on_time_completion_of_Instalaltion__c;

    global Feedbacks__c () 
    {
    }
}
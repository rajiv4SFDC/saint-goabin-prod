// This file is generated as an Apex representation of the
//     corresponding sObject and its fields.
// This read-only file is used by the Apex Language Server to
//     provide code smartness, and is deleted each time you
//     refresh your sObject definitions.
// To edit your sObjects and their fields, edit the corresponding
//     .object-meta.xml and .field-meta.xml files.

global class Mock_Up_Request__c {
    global String Account_Region__c;
    global List<ActivityHistory> ActivityHistories;
    global String Approval_Status__c;
    global List<AttachedContentDocument> AttachedContentDocuments;
    global List<AttachedContentNote> AttachedContentNotes;
    global List<Attachment> Attachments;
    global Id Bill_To_Customer__c;
    global Account Bill_To_Customer__r;
    global Date Billing_date__c;
    global List<CombinedAttachment> CombinedAttachments;
    global List<ContactRequest> ContactRequests;
    global List<ContentDocumentLink> ContentDocumentLinks;
    global User CreatedBy;
    global Id CreatedById;
    global Datetime CreatedDate;
    global Decimal Credit_Note_Amount__c;
    global Date Credit_Note_Date__c;
    global String Credit_Note_Number__c;
    global String Customer__c;
    global Id D_C_Manager__c;
    global User D_C_Manager__r;
    global List<DuplicateRecordItem> DuplicateRecordItems;
    global List<EmailMessage> Emails;
    global List<EventRelation> EventRelations;
    global List<Event> Events;
    global List<EntitySubscription> FeedSubscriptionsForEntity;
    global List<Mock_Up_Request__Feed> Feeds;
    global List<ContentVersion> FirstPublishLocation;
    global List<Mock_Up_Request__History> Histories;
    global Id Id;
    global Decimal Invoice_amount__c;
    global String Invoice_number__c;
    global Boolean IsDeleted;
    global String KAM__c;
    global Date LastActivityDate;
    global User LastModifiedBy;
    global Id LastModifiedById;
    global Datetime LastModifiedDate;
    global Datetime LastReferencedDate;
    global Datetime LastViewedDate;
    global Double Mock_Up_Product_Count__c;
    global List<Mock_Up_Product__c> Mock_Up_Products__r;
    global String Name;
    global List<Note> Notes;
    global List<NoteAndAttachment> NotesAndAttachments;
    global List<OpenActivity> OpenActivities;
    global String Opportunity_Name__c;
    global Id Opportunity__c;
    global Opportunity Opportunity__r;
    global String OwnerId__c;
    global Decimal PI_Amount__c;
    global String PI_Number__c;
    global Id P_P_manager__c;
    global User P_P_manager__r;
    global List<FeedComment> Parent;
    global List<ProcessInstance> ProcessInstances;
    global List<ProcessInstanceHistory> ProcessSteps;
    global List<RecordActionHistory> RecordActionHistories;
    global List<RecordAction> RecordActions;
    global List<CollaborationGroupRecord> RecordAssociatedGroups;
    global RecordType RecordType;
    global Id RecordTypeId;
    global Id Regional_Manager__c;
    global User Regional_Manager__r;
    global List<FlowRecordRelation> RelatedRecord;
    global List<OutgoingEmail> RelatedTo;
    global String Remarks__c;
    global String Sales_Area__c;
    global String Status__c;
    global Datetime SystemModstamp;
    global Double Tagged_Doc_Count__c;
    global List<TaskRelation> TaskRelations;
    global List<Task> Tasks;
    global List<TopicAssignment> TopicAssignments;

    global Mock_Up_Request__c () 
    {
    }
}
// This file is generated as an Apex representation of the
//     corresponding sObject and its fields.
// This read-only file is used by the Apex Language Server to
//     provide code smartness, and is deleted each time you
//     refresh your sObject definitions.
// To edit your sObjects and their fields, edit the corresponding
//     .object-meta.xml and .field-meta.xml files.

global class Price_Request_Line_Item__c {
    global List<ActivityHistory> ActivityHistories;
    global String Approval_Status__c;
    global List<AttachedContentDocument> AttachedContentDocuments;
    global List<AttachedContentNote> AttachedContentNotes;
    global List<Attachment> Attachments;
    global Double Balance_Quantity__c;
    global List<CombinedAttachment> CombinedAttachments;
    global List<ContactRequest> ContactRequests;
    global List<ContentDocumentLink> ContentDocumentLinks;
    global User CreatedBy;
    global Id CreatedById;
    global Datetime CreatedDate;
    global String Customer_Code__c;
    global Decimal Discount_Amount__c;
    global Double Discount__c;
    global List<DuplicateRecordItem> DuplicateRecordItems;
    global List<EmailMessage> Emails;
    global List<EventRelation> EventRelations;
    global List<Event> Events;
    global List<EntitySubscription> FeedSubscriptionsForEntity;
    global Decimal Final_Price__c;
    global List<ContentVersion> FirstPublishLocation;
    global List<Price_Request_Line_Item__History> Histories;
    global Id Id;
    global List<Invoice__c> Invoices__r;
    global Boolean IsDeleted;
    global Date LastActivityDate;
    global User LastModifiedBy;
    global Id LastModifiedById;
    global Datetime LastModifiedDate;
    global Decimal List_Price__c;
    global String Name;
    global List<Note> Notes;
    global List<NoteAndAttachment> NotesAndAttachments;
    global List<OpenActivity> OpenActivities;
    global Id Opportunity_Product__c;
    global Opportunity_Product__c Opportunity_Product__r;
    global List<FeedItem> Parent;
    global Id Price_Request__c;
    global Price_Request__c Price_Request__r;
    global List<ProcessInstance> ProcessInstances;
    global List<ProcessInstanceHistory> ProcessSteps;
    global String Product_Item_Code__c;
    global Id Product__c;
    global Product2 Product__r;
    global String Project_Area__c;
    global String Project_Code__c;
    global String Project_Location__c;
    global String Project_Name__c;
    global Double Quantity__c;
    global List<RecordActionHistory> RecordActionHistories;
    global List<RecordAction> RecordActions;
    global List<CollaborationGroupRecord> RecordAssociatedGroups;
    global List<ContentDistribution> RelatedRecord;
    global List<OutgoingEmail> RelatedTo;
    global Double Requested_Quantity__c;
    global Double Supplied_Quantity__c;
    global Datetime SystemModstamp;
    global List<TaskRelation> TaskRelations;
    global List<Task> Tasks;
    global List<TopicAssignment> TopicAssignments;
    global String Uom__c;
    global Date Valid_From__c;
    global Date Valid_To__c;

    global Price_Request_Line_Item__c () 
    {
    }
}
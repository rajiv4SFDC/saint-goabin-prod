// This file is generated as an Apex representation of the
//     corresponding sObject and its fields.
// This read-only file is used by the Apex Language Server to
//     provide code smartness, and is deleted each time you
//     refresh your sObject definitions.
// To edit your sObjects and their fields, edit the corresponding
//     .object-meta.xml and .field-meta.xml files.

global class Credit_Note__c {
    global List<ActivityHistory> ActivityHistories;
    global String Approval_Status__c;
    global List<AttachedContentDocument> AttachedContentDocuments;
    global List<AttachedContentNote> AttachedContentNotes;
    global List<Attachment> Attachments;
    global Double Balance__c;
    global Decimal Basic_Price_to_Dealer__c;
    global Decimal Billing_Price__c;
    global List<CombinedAttachment> CombinedAttachments;
    global List<ContactRequest> ContactRequests;
    global List<ContentDocumentLink> ContentDocumentLinks;
    global User CreatedBy;
    global Id CreatedById;
    global Datetime CreatedDate;
    global Id Customer_Name__c;
    global Key_Account__c Customer_Name__r;
    global Double Dealer_margin__c;
    global Double Design_wastage__c;
    global Double Difference__c;
    global List<DuplicateRecordItem> DuplicateRecordItems;
    global Id ERP_Account__c;
    global Account ERP_Account__r;
    global List<EmailMessage> Emails;
    global List<EventRelation> EventRelations;
    global List<Event> Events;
    global List<EntitySubscription> FeedSubscriptionsForEntity;
    global List<ContentVersion> FirstPublishLocation;
    global List<Credit_Note__History> Histories;
    global Id Id;
    global Boolean IsDeleted;
    global Date LastActivityDate;
    global User LastModifiedBy;
    global Id LastModifiedById;
    global Datetime LastModifiedDate;
    global Datetime LastReferencedDate;
    global Datetime LastViewedDate;
    global String Name;
    global Double Net_realisation__c;
    global List<Note> Notes;
    global List<NoteAndAttachment> NotesAndAttachments;
    global List<OpenActivity> OpenActivities;
    global Id Opportunity_Product_Customer__c;
    global Opportunity_Product_with_Account__c Opportunity_Product_Customer__r;
    global Id Opportunity_Product__c;
    global Opportunity_Product__c Opportunity_Product__r;
    global Id Opportunity__c;
    global Opportunity Opportunity__r;
    global List<FeedComment> Parent;
    global List<ProcessInstance> ProcessInstances;
    global List<ProcessInstanceHistory> ProcessSteps;
    global String Project_Name__c;
    global String Reason__c;
    global List<RecordActionHistory> RecordActionHistories;
    global List<RecordAction> RecordActions;
    global List<CollaborationGroupRecord> RecordAssociatedGroups;
    global List<ContentDistribution> RelatedRecord;
    global List<OutgoingEmail> RelatedTo;
    global Double Selling_price_sq_ft__c;
    global Double Supplied__c;
    global Datetime SystemModstamp;
    global List<TaskRelation> TaskRelations;
    global List<Task> Tasks;
    global Decimal Test__c;
    global Double Thickness__c;
    global Decimal Tier_Price__c;
    global List<TopicAssignment> TopicAssignments;
    global Double Total_Quantity_sqm__c;
    global Date Valid_from__c;
    global Date Valid_to__c;
    global Double Value_addition_Charges__c;

    global Credit_Note__c () 
    {
    }
}
// This file is generated as an Apex representation of the
//     corresponding sObject and its fields.
// This read-only file is used by the Apex Language Server to
//     provide code smartness, and is deleted each time you
//     refresh your sObject definitions.
// To edit your sObjects and their fields, edit the corresponding
//     .object-meta.xml and .field-meta.xml files.

global class Target_and_Actual__c {
    global Double Achievement_pct__c;
    global Double Actual__c;
    global String Area_Type__c;
    global List<AttachedContentDocument> AttachedContentDocuments;
    global List<AttachedContentNote> AttachedContentNotes;
    global List<Attachment> Attachments;
    global Double Backend_Achievement__c;
    global Double Backend_Points_Eligible__c;
    global Boolean Calculate_Actuals__c;
    global Boolean Calculate_Point_Scored__c;
    global String City_External_Id__c;
    global Id City__c;
    global City__c City__r;
    global List<CombinedAttachment> CombinedAttachments;
    global List<ContactRequest> ContactRequests;
    global List<ContentDocumentLink> ContentDocumentLinks;
    global User CreatedBy;
    global Id CreatedById;
    global Datetime CreatedDate;
    global List<DuplicateRecordItem> DuplicateRecordItems;
    global List<EmailMessage> Emails;
    global String Error_Message__c;
    global List<EventRelation> EventRelations;
    global List<EntitySubscription> FeedSubscriptionsForEntity;
    global List<ContentVersion> FirstPublishLocation;
    global List<Target_and_Actual__History> Histories;
    global Id Id;
    global String Incentive_Type__c;
    global Boolean IsDeleted;
    global String Label__c;
    global User LastModifiedBy;
    global Id LastModifiedById;
    global Datetime LastModifiedDate;
    global Datetime LastReferencedDate;
    global Datetime LastViewedDate;
    global String Name;
    global List<Note> Notes;
    global List<NoteAndAttachment> NotesAndAttachments;
    global SObject Owner;
    global Id OwnerId;
    global List<FeedItem> Parent;
    global Double Points_Eligible__c;
    global Double Points_Scored__c;
    global List<ProcessInstance> ProcessInstances;
    global List<ProcessInstanceHistory> ProcessSteps;
    global String Product_Range__c;
    global List<RecordActionHistory> RecordActionHistories;
    global List<RecordAction> RecordActions;
    global List<CollaborationGroupRecord> RecordAssociatedGroups;
    global String Region__c;
    global List<ContentDistribution> RelatedRecord;
    global String Sales_Area__c;
    global List<Target_and_Actual__Share> Shares;
    global Datetime SystemModstamp;
    global Date Target_Date__c;
    global String Target_Type__c;
    global Double Target__c;
    global String Target_year__c;
    global List<TaskRelation> TaskRelations;
    global List<TopicAssignment> TopicAssignments;
    global String Unique_Target_Actual_Identifier__c;

    global Target_and_Actual__c () 
    {
    }
}
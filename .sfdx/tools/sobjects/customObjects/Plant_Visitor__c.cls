// This file is generated as an Apex representation of the
//     corresponding sObject and its fields.
// This read-only file is used by the Apex Language Server to
//     provide code smartness, and is deleted each time you
//     refresh your sObject definitions.
// To edit your sObjects and their fields, edit the corresponding
//     .object-meta.xml and .field-meta.xml files.

global class Plant_Visitor__c {
    global String Account__c;
    global Double Age_Yrs__c;
    global List<AttachedContentDocument> AttachedContentDocuments;
    global List<AttachedContentNote> AttachedContentNotes;
    global List<Attachment> Attachments;
    global List<CombinedAttachment> CombinedAttachments;
    global List<ContactRequest> ContactRequests;
    global String Contact_Name__c;
    global Id Contact__c;
    global Contact Contact__r;
    global List<ContentDocumentLink> ContentDocumentLinks;
    global User CreatedBy;
    global Id CreatedById;
    global Datetime CreatedDate;
    global Double Customer_s_Total_No_of_Projects__c;
    global List<DuplicateRecordItem> DuplicateRecordItems;
    global String Email__c;
    global List<EmailMessage> Emails;
    global List<EventRelation> EventRelations;
    global List<EntitySubscription> FeedSubscriptionsForEntity;
    global List<ContentVersion> FirstPublishLocation;
    global Id Id;
    global Id Internal_Team_Member__c;
    global User Internal_Team_Member__r;
    global Boolean IsDeleted;
    global Id Key_Account__c;
    global Account Key_Account__r;
    global User LastModifiedBy;
    global Id LastModifiedById;
    global Datetime LastModifiedDate;
    global String Member_Type__c;
    global String Name;
    global List<Note> Notes;
    global List<NoteAndAttachment> NotesAndAttachments;
    global List<FeedComment> Parent;
    global String Phone__c;
    global Boolean Plant_Visit_Status__c;
    global Id Plant_Visit__c;
    global Plant_Visit__c Plant_Visit__r;
    global List<ProcessInstance> ProcessInstances;
    global List<ProcessInstanceHistory> ProcessSteps;
    global List<RecordActionHistory> RecordActionHistories;
    global List<RecordAction> RecordActions;
    global List<CollaborationGroupRecord> RecordAssociatedGroups;
    global List<ContentDistribution> RelatedRecord;
    global Double Shoe_Size__c;
    global Datetime SystemModstamp;
    global List<TaskRelation> TaskRelations;
    global String Title__c;
    global List<TopicAssignment> TopicAssignments;
    global Date Visit_Date__c;

    global Plant_Visitor__c () 
    {
    }
}
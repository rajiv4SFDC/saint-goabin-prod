// This file is generated as an Apex representation of the
//     corresponding sObject and its fields.
// This read-only file is used by the Apex Language Server to
//     provide code smartness, and is deleted each time you
//     refresh your sObject definitions.
// To edit your sObjects and their fields, edit the corresponding
//     .object-meta.xml and .field-meta.xml files.

global class Enquiry_Line_Item__c {
    global List<ActivityHistory> ActivityHistories;
    global List<AttachedContentDocument> AttachedContentDocuments;
    global List<AttachedContentNote> AttachedContentNotes;
    global List<Attachment> Attachments;
    global Double Base_price__c;
    global String Cases__c;
    global String Category__c;
    global String Coating__c;
    global List<CombinedAttachment> CombinedAttachments;
    global List<ContactRequest> ContactRequests;
    global List<ContentDocumentLink> ContentDocumentLinks;
    global User CreatedBy;
    global Id CreatedById;
    global Datetime CreatedDate;
    global String Customer_Request_Name__c;
    global Id Customer_Request__c;
    global Price_Request__c Customer_Request__r;
    global List<DuplicateRecordItem> DuplicateRecordItems;
    global Date EXPECTED_DATE__c;
    global List<EmailMessage> Emails;
    global String Enquiry_Name__c;
    global String Enquiry_SF_Number__c;
    global List<Enquiry_Status__c> Enquiry_Status__r;
    global Id Enquiry__c;
    global Enquiry__c Enquiry__r;
    global List<EventRelation> EventRelations;
    global List<Event> Events;
    global List<EntitySubscription> FeedSubscriptionsForEntity;
    global List<ContentVersion> FirstPublishLocation;
    global Boolean Glass_Direct_Line_Item__c;
    global List<Enquiry_Line_Item__History> Histories;
    global Double ITEM_QUANTITY__c;
    global String ITEM_UNIT_OF_MEASURE__c;
    global Id Id;
    global Boolean IsDeleted;
    global String Laminate__c;
    global Date LastActivityDate;
    global User LastModifiedBy;
    global Id LastModifiedById;
    global Datetime LastModifiedDate;
    global Datetime LastReferencedDate;
    global Datetime LastViewedDate;
    global String Length__c;
    global String Name;
    global List<Note> Notes;
    global List<NoteAndAttachment> NotesAndAttachments;
    global Boolean Old_submitted__c;
    global List<OpenActivity> OpenActivities;
    global Double Order_quantity__c;
    global SObject Owner;
    global Id OwnerId;
    global String PRICING_REMARKS__c;
    global String PROJECT_CODE__c;
    global String Pak_Type_Tons__c;
    global List<FeedItem> Parent;
    global String Parent_Reference_Number__c;
    global String Pattern__c;
    global List<Price_Request__c> Price_Requests__r;
    global List<price_request_PandP__c> Price_Requests_pandp__r;
    global String Price__c;
    global String Price_request_name__c;
    global List<ProcessInstance> ProcessInstances;
    global List<ProcessInstanceHistory> ProcessSteps;
    global String Product_Item_Code__c;
    global String Product_Length__c;
    global String Product_Name__c;
    global String Product_Thickness__c;
    global String Product_Width__c;
    global Id Product__c;
    global Product2 Product__r;
    global String Product_category__c;
    global String Product_name1__c;
    global String Project_Code_Item__c;
    global String Quality_Code__c;
    global List<RecordActionHistory> RecordActionHistories;
    global List<RecordAction> RecordActions;
    global List<CollaborationGroupRecord> RecordAssociatedGroups;
    global String Reference_Number__c;
    global List<FlowRecordRelation> RelatedRecord;
    global List<OutgoingEmail> RelatedTo;
    global Double Required_Price_per_Sq_M__c;
    global Boolean SAP_Line_Item__c;
    global Boolean SFDC_Line_Item__c;
    global String SUB_FAMILY__c;
    global List<Sales_Order_Line_Item__c> Sales_Order_Line_Items__r;
    global Double Sales_Order_in_Shipment__c;
    global List<Sales_Order__c> Sales_Orders__r;
    global String Special_price_request_cst__c;
    global String Special_request_quantity__c;
    global Double Standard_price_per_unit__c;
    global Boolean Submitted__c;
    global Datetime Submitted_date_time__c;
    global Datetime SystemModstamp;
    global List<TaskRelation> TaskRelations;
    global List<Task> Tasks;
    global String Thickness__c;
    global String Tint__c;
    global List<TopicAssignment> TopicAssignments;
    global String UOM__c;
    global Date Valid_from__c;
    global Date Valid_to__c;
    global Double listPriceCst__c;
    global Double recently_updated_Invoice_rate__c;
    global String remarks__c;

    global Enquiry_Line_Item__c () 
    {
    }
}
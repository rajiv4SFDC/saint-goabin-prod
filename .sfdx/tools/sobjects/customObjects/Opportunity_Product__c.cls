// This file is generated as an Apex representation of the
//     corresponding sObject and its fields.
// This read-only file is used by the Apex Language Server to
//     provide code smartness, and is deleted each time you
//     refresh your sObject definitions.
// To edit your sObjects and their fields, edit the corresponding
//     .object-meta.xml and .field-meta.xml files.

global class Opportunity_Product__c {
    global List<ActivityHistory> ActivityHistories;
    global Double Additional_Product_Qty__c;
    global Id Additional_Product__c;
    global Product2 Additional_Product__r;
    global Double Appropriated_Quantity__c;
    global List<AttachedContentDocument> AttachedContentDocuments;
    global List<AttachedContentNote> AttachedContentNotes;
    global List<Attachment> Attachments;
    global Double Balance_Quantity_Frm_Forecast__c;
    global Double Balance_Quantity__c;
    global List<Account> ChannelPartner__r;
    global List<Checklists__c> Checklists__r;
    global List<CombinedAttachment> CombinedAttachments;
    global String Competitor_Remarks__c;
    global String Competitor__c;
    global List<ContactRequest> ContactRequests;
    global List<ContentDocumentLink> ContentDocumentLinks;
    global User CreatedBy;
    global Id CreatedById;
    global Datetime CreatedDate;
    global List<Credit_Note__c> Credit_Notes__r;
    global Id Customer_Name__c;
    global Account Customer_Name__r;
    global List<DuplicateRecordItem> DuplicateRecordItems;
    global List<EmailMessage> Emails;
    global Double Estimated_Quantity__c;
    global List<EventRelation> EventRelations;
    global List<Event> Events;
    global Decimal Expected_Price__c;
    global List<EntitySubscription> FeedSubscriptionsForEntity;
    global List<ContentVersion> FirstPublishLocation;
    global Date First_Billing_Date__c;
    global List<Opportunity_Product__History> Histories;
    global Id Id;
    global Double Invoiced_Quantity__c;
    global List<Invoice__c> Invoices__r;
    global Boolean IsDeleted;
    global Date LastActivityDate;
    global User LastModifiedBy;
    global Id LastModifiedById;
    global Datetime LastModifiedDate;
    global Datetime LastReferencedDate;
    global Datetime LastViewedDate;
    global Date Last_Billing_Date__c;
    global List<Manual_Accounting__c> Manual_Accounting__r;
    global String Name;
    global List<Note> Notes;
    global List<NoteAndAttachment> NotesAndAttachments;
    global List<OpenActivity> OpenActivities;
    global List<Opportunity_Product_line_items__c> Opportunity_Product_line_items__r;
    global List<Opportunity_Product_with_Account__c> Opportunity_Product_with_Accounts__r;
    global Id Opportunity__c;
    global Opportunity Opportunity__r;
    global List<FeedItem> Parent;
    global List<Price_Request_Line_Item__c> Price_Request_Line_Items__r;
    global List<Price_Request__c> Price_Requests__r;
    global List<price_request_PandP__c> Price_Requests_pandp__r;
    global List<ProcessInstance> ProcessInstances;
    global List<ProcessInstanceHistory> ProcessSteps;
    global List<Product_Forecast__c> Product_Forecast__r;
    global String Product_Name__c;
    global String Product_Range__c;
    global String Product_Type__c;
    global Id Product__c;
    global Product2 Product__r;
    global String Project_Code__c;
    global String Project_Name__c;
    global Boolean Recently_Uploaded__c;
    global List<RecordActionHistory> RecordActionHistories;
    global List<RecordAction> RecordActions;
    global List<CollaborationGroupRecord> RecordAssociatedGroups;
    global RecordType RecordType;
    global Id RecordTypeId;
    global String Record_type_name__c;
    global List<ContentDistribution> RelatedRecord;
    global List<OutgoingEmail> RelatedTo;
    global String Sales_Office__c;
    global Decimal Sales_Price__c;
    global String Status__c;
    global Datetime SystemModstamp;
    global List<TaskRelation> TaskRelations;
    global List<Task> Tasks;
    global Double Thickness__c;
    global Double Ton__c;
    global List<TopicAssignment> TopicAssignments;
    global Decimal Total_Price__c;
    global String Unique_Serial_Number__c;

    global Opportunity_Product__c () 
    {
    }
}
// This file is generated as an Apex representation of the
//     corresponding sObject and its fields.
// This read-only file is used by the Apex Language Server to
//     provide code smartness, and is deleted each time you
//     refresh your sObject definitions.
// To edit your sObjects and their fields, edit the corresponding
//     .object-meta.xml and .field-meta.xml files.

global class Manual_Accounting__c {
    global List<ActivityHistory> ActivityHistories;
    global String Approval_Satus__c;
    global List<AttachedContentDocument> AttachedContentDocuments;
    global List<AttachedContentNote> AttachedContentNotes;
    global List<Attachment> Attachments;
    global List<CombinedAttachment> CombinedAttachments;
    global List<ContactRequest> ContactRequests;
    global List<ContentDocumentLink> ContentDocumentLinks;
    global User CreatedBy;
    global Id CreatedById;
    global Datetime CreatedDate;
    global List<DuplicateRecordItem> DuplicateRecordItems;
    global List<EmailMessage> Emails;
    global List<EventRelation> EventRelations;
    global List<Event> Events;
    global String External_Id_formula__c;
    global List<EntitySubscription> FeedSubscriptionsForEntity;
    global List<ContentVersion> FirstPublishLocation;
    global Boolean First_time_approval_done__c;
    global List<Manual_Accounting__History> Histories;
    global Id Id;
    global Id Invoice__c;
    global Invoice__c Invoice__r;
    global Boolean IsDeleted;
    global Boolean Is_project_validated__c;
    global Date LastActivityDate;
    global User LastModifiedBy;
    global Id LastModifiedById;
    global Datetime LastModifiedDate;
    global Datetime LastReferencedDate;
    global Datetime LastViewedDate;
    global String Name;
    global List<Note> Notes;
    global List<NoteAndAttachment> NotesAndAttachments;
    global List<OpenActivity> OpenActivities;
    global Id Opportunity_Product_Customer__c;
    global Opportunity_Product_with_Account__c Opportunity_Product_Customer__r;
    global Id Opportunity_Product__c;
    global Opportunity_Product__c Opportunity_Product__r;
    global String Opportunity__c;
    global String Opportunity_product_id__c;
    global String Opportunity_product_record_type__c;
    global List<FeedComment> Parent;
    global List<ProcessInstance> ProcessInstances;
    global List<ProcessInstanceHistory> ProcessSteps;
    global Id Product_Forecast__c;
    global Product_Forecast__c Product_Forecast__r;
    global String Product_code__c;
    global String Project_Name__c;
    global Double Quantity__c;
    global List<RecordActionHistory> RecordActionHistories;
    global List<RecordAction> RecordActions;
    global List<CollaborationGroupRecord> RecordAssociatedGroups;
    global List<FlowRecordRelation> RelatedRecord;
    global List<OutgoingEmail> RelatedTo;
    global Datetime SystemModstamp;
    global List<TaskRelation> TaskRelations;
    global List<Task> Tasks;
    global List<TopicAssignment> TopicAssignments;

    global Manual_Accounting__c () 
    {
    }
}
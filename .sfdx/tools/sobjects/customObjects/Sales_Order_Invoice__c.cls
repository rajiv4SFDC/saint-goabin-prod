// This file is generated as an Apex representation of the
//     corresponding sObject and its fields.
// This read-only file is used by the Apex Language Server to
//     provide code smartness, and is deleted each time you
//     refresh your sObject definitions.
// To edit your sObjects and their fields, edit the corresponding
//     .object-meta.xml and .field-meta.xml files.

global class Sales_Order_Invoice__c {
    global List<ActivityHistory> ActivityHistories;
    global List<AttachedContentDocument> AttachedContentDocuments;
    global List<AttachedContentNote> AttachedContentNotes;
    global List<Attachment> Attachments;
    global String Bill_to_customer__c;
    global String City__c;
    global List<CombinedAttachment> CombinedAttachments;
    global List<ContactRequest> ContactRequests;
    global List<ContentDocumentLink> ContentDocumentLinks;
    global User CreatedBy;
    global Id CreatedById;
    global Datetime CreatedDate;
    global List<DuplicateRecordItem> DuplicateRecordItems;
    global List<EmailMessage> Emails;
    global List<EventRelation> EventRelations;
    global List<Event> Events;
    global List<EntitySubscription> FeedSubscriptionsForEntity;
    global List<ContentVersion> FirstPublishLocation;
    global String Geography__c;
    global List<Sales_Order_Invoice__History> Histories;
    global Id Id;
    global Id Invoice__c;
    global Invoice__c Invoice__r;
    global Double Invoice_order_quanitity__c;
    global Double Invoice_quantity__c;
    global Boolean IsDeleted;
    global Date LastActivityDate;
    global User LastModifiedBy;
    global Id LastModifiedById;
    global Datetime LastModifiedDate;
    global String Material_code__c;
    global String Material_description__c;
    global String Name;
    global List<Note> Notes;
    global List<NoteAndAttachment> NotesAndAttachments;
    global List<OpenActivity> OpenActivities;
    global Double Order_quantity__c;
    global SObject Owner;
    global Id OwnerId;
    global List<FeedItem> Parent;
    global Double Pending_order__c;
    global List<ProcessInstance> ProcessInstances;
    global List<ProcessInstanceHistory> ProcessSteps;
    global String Product_Name__c;
    global String Product_Range__c;
    global List<RecordActionHistory> RecordActionHistories;
    global List<RecordAction> RecordActions;
    global List<CollaborationGroupRecord> RecordAssociatedGroups;
    global List<FlowRecordRelation> RelatedRecord;
    global List<OutgoingEmail> RelatedTo;
    global String SAP_code__c;
    global Id Sales_Order__c;
    global Sales_Order__c Sales_Order__r;
    global String Sales_office_Sharing__c;
    global String Sales_office__c;
    global String Sales_order_number__c;
    global String Sales_order_shipping_status__c;
    global List<Sales_Order_Invoice__Share> Shares;
    global String Ship_to_customer_Name__c;
    global String Ship_to_customer_SAP_code__c;
    global String Ship_to_customer__c;
    global Datetime SystemModstamp;
    global List<TaskRelation> TaskRelations;
    global List<Task> Tasks;
    global List<TopicAssignment> TopicAssignments;

    global Sales_Order_Invoice__c () 
    {
    }
}
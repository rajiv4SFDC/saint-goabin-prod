// This file is generated as an Apex representation of the
//     corresponding sObject and its fields.
// This read-only file is used by the Apex Language Server to
//     provide code smartness, and is deleted each time you
//     refresh your sObject definitions.
// To edit your sObjects and their fields, edit the corresponding
//     .object-meta.xml and .field-meta.xml files.

global class Enquiry__c {
    global String Account_Name__c;
    global List<ActivityHistory> ActivityHistories;
    global List<AttachedContentDocument> AttachedContentDocuments;
    global List<AttachedContentNote> AttachedContentNotes;
    global List<Attachment> Attachments;
    global String Bill_to_Customer_Name__c;
    global String Bill_to_SAP_Code__c;
    global Id Bill_to_customer__c;
    global Account Bill_to_customer__r;
    global List<CombinedAttachment> CombinedAttachments;
    global String Comments__c;
    global List<ContactRequest> ContactRequests;
    global List<ContentDocumentLink> ContentDocumentLinks;
    global User CreatedBy;
    global Id CreatedById;
    global Datetime CreatedDate;
    global List<Credit_Notes_Ticketing__c> Credit_Notes_Ticketing__r;
    global List<DuplicateRecordItem> DuplicateRecordItems;
    global List<EmailMessage> Emails;
    global String Enquiry_Id__c;
    global List<Enquiry_Line_Item__c> Enquiry_Line_Items__r;
    global String Enquiry_Status__c;
    global List<Enquiry_Status__c> Enquiry_Status__r;
    global List<EventRelation> EventRelations;
    global List<Event> Events;
    global Date Expected_Date_of_Delivery__c;
    global List<EntitySubscription> FeedSubscriptionsForEntity;
    global List<ContentVersion> FirstPublishLocation;
    global String GD_Reference_Number__c;
    global Boolean Glass_Direct_Enquiry__c;
    global List<Enquiry__History> Histories;
    global Id Id;
    global Boolean IsDeleted;
    global Date LastActivityDate;
    global User LastModifiedBy;
    global Id LastModifiedById;
    global Datetime LastModifiedDate;
    global Datetime LastReferencedDate;
    global Datetime LastViewedDate;
    global String Name;
    global List<Note> Notes;
    global List<NoteAndAttachment> NotesAndAttachments;
    global List<OpenActivity> OpenActivities;
    global SObject Owner;
    global Id OwnerId;
    global List<FeedItem> Parent;
    global List<Price_Request__c> Price_Requests__r;
    global List<price_request_PandP__c> Price_Requests_pandp__r;
    global List<ProcessInstance> ProcessInstances;
    global List<ProcessInstanceHistory> ProcessSteps;
    global List<RecordActionHistory> RecordActionHistories;
    global List<RecordAction> RecordActions;
    global List<CollaborationGroupRecord> RecordAssociatedGroups;
    global String Region__c;
    global Id RegionalHead__c;
    global User RegionalHead__r;
    global Id RegionalManager__c;
    global User RegionalManager__r;
    global List<FlowRecordRelation> RelatedRecord;
    global List<OutgoingEmail> RelatedTo;
    global String Requested_By__c;
    global String SAP_Code__c;
    global Boolean SAP_Enquiry__c;
    global Boolean SFDC_Enquiry__c;
    global String Sales_Office__c;
    global String Sales_Person__c;
    global String Sales_area__c;
    global List<Enquiry__Share> Shares;
    global String Ship_To_Customer_Name__c;
    global Id Ship_To_Customer__c;
    global Ship_To_Customer__c Ship_To_Customer__r;
    global String Ship_To_SAP_Code__c;
    global Datetime SystemModstamp;
    global List<TaskRelation> TaskRelations;
    global List<Task> Tasks;
    global List<TopicAssignment> TopicAssignments;
    global Boolean submitted__c;

    global Enquiry__c () 
    {
    }
}
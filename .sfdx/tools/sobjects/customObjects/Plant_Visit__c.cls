// This file is generated as an Apex representation of the
//     corresponding sObject and its fields.
// This read-only file is used by the Apex Language Server to
//     provide code smartness, and is deleted each time you
//     refresh your sObject definitions.
// To edit your sObjects and their fields, edit the corresponding
//     .object-meta.xml and .field-meta.xml files.

global class Plant_Visit__c {
    global String Accomodation_Required__c;
    global List<ActivityHistory> ActivityHistories;
    global String Approval_Status__c;
    global List<AttachedContentDocument> AttachedContentDocuments;
    global List<AttachedContentNote> AttachedContentNotes;
    global List<Attachment> Attachments;
    global Decimal Boarding_and_Lodging_Expenses__c;
    global List<CombinedAttachment> CombinedAttachments;
    global List<ContactRequest> ContactRequests;
    global List<ContentDocumentLink> ContentDocumentLinks;
    global User CreatedBy;
    global Id CreatedById;
    global Datetime CreatedDate;
    global String Creator__c;
    global Id D_C_Manager__c;
    global User D_C_Manager__r;
    global String Design_Engineer_Available_from_HO__c;
    global List<DuplicateRecordItem> DuplicateRecordItems;
    global List<EmailMessage> Emails;
    global List<EventRelation> EventRelations;
    global List<Event> Events;
    global List<EntitySubscription> FeedSubscriptionsForEntity;
    global List<Plant_Visit__Feed> Feeds;
    global List<ContentVersion> FirstPublishLocation;
    global List<Plant_Visit__History> Histories;
    global Id Id;
    global Boolean IsDeleted;
    global String KAM_Region2__c;
    global String KAM_Region__c;
    global Date LastActivityDate;
    global User LastModifiedBy;
    global Id LastModifiedById;
    global Datetime LastModifiedDate;
    global Datetime LastReferencedDate;
    global Datetime LastViewedDate;
    global Boolean Mail_Trigger__c;
    global String Mode_of_Transport__c;
    global String Name;
    global List<Note> Notes;
    global List<NoteAndAttachment> NotesAndAttachments;
    global List<OpenActivity> OpenActivities;
    global String Opportunity_Name__c;
    global Id Opportunity__c;
    global Opportunity Opportunity__r;
    global SObject Owner;
    global Id OwnerId;
    global Id Owner_Manager__c;
    global User Owner_Manager__r;
    global String Owner_Profile__c;
    global List<FeedItem> Parent;
    global String Plant_Email__c;
    global Double Plant_Visitors_Count__c;
    global List<Plant_Visitor__c> Plant_Visitors__r;
    global String Plant__c;
    global List<ProcessInstance> ProcessInstances;
    global List<ProcessInstanceHistory> ProcessSteps;
    global List<RecordActionHistory> RecordActionHistories;
    global List<RecordAction> RecordActions;
    global List<CollaborationGroupRecord> RecordAssociatedGroups;
    global String Region__c;
    global Id Regional_Manager__c;
    global User Regional_Manager__r;
    global List<FlowRecordRelation> RelatedRecord;
    global List<OutgoingEmail> RelatedTo;
    global String Remarks__c;
    global Date Requisition_Date__c;
    global List<Plant_Visit__Share> Shares;
    global Datetime SystemModstamp;
    global List<TaskRelation> TaskRelations;
    global List<Task> Tasks;
    global Decimal Tentative_Budget_INR__c;
    global List<TopicAssignment> TopicAssignments;
    global Double Total_Glass_Volume__c;
    global Double Total_No_of_Guests_Invited__c;
    global String Transportation_Required__c;
    global Decimal Travel_Expenses__c;
    global String Type__c;
    global Date Visit_Complete_Date__c;
    global Datetime Visit_Date__c;

    global Plant_Visit__c () 
    {
    }
}
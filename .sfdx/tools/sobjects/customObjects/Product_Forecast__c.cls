// This file is generated as an Apex representation of the
//     corresponding sObject and its fields.
// This read-only file is used by the Apex Language Server to
//     provide code smartness, and is deleted each time you
//     refresh your sObject definitions.
// To edit your sObjects and their fields, edit the corresponding
//     .object-meta.xml and .field-meta.xml files.

global class Product_Forecast__c {
    global Double Appropriated_Quantity__c;
    global List<AttachedContentDocument> AttachedContentDocuments;
    global List<AttachedContentNote> AttachedContentNotes;
    global List<Attachment> Attachments;
    global Double Available_quantity_after_approval__c;
    global Boolean Chk_Appro_grt_than_Qty__c;
    global List<CombinedAttachment> CombinedAttachments;
    global List<ContactRequest> ContactRequests;
    global List<ContentDocumentLink> ContentDocumentLinks;
    global User CreatedBy;
    global Id CreatedById;
    global Datetime CreatedDate;
    global List<DuplicateRecordItem> DuplicateRecordItems;
    global List<EmailMessage> Emails;
    global List<EventRelation> EventRelations;
    global String External_Id__c;
    global List<EntitySubscription> FeedSubscriptionsForEntity;
    global List<ContentVersion> FirstPublishLocation;
    global Date First_Billing_Date__c;
    global Date First_Of_Next_Month__c;
    global Date Forecast_Date__c;
    global List<Forecast_History__c> Forecast_History__r;
    global List<Forecast_Invoice__c> Forecast_Invoices__r;
    global List<Product_Forecast__History> Histories;
    global Id Id;
    global Boolean Include_In_Sales_Plan__c;
    global Boolean IsDeleted;
    global User LastModifiedBy;
    global Id LastModifiedById;
    global Datetime LastModifiedDate;
    global Datetime LastReferencedDate;
    global Datetime LastViewedDate;
    global Date Last_Billing_Date__c;
    global List<Manual_Accounting__c> Manual_Accounting__r;
    global Double Manual_accounted_quantity__c;
    global String Month__c;
    global String Name;
    global Double Non_Approriated_Quantity__c;
    global List<Note> Notes;
    global List<NoteAndAttachment> NotesAndAttachments;
    global String Opportunity_ID__c;
    global List<Opportunity_Product_Team__c> Opportunity_Product_Team__r;
    global Id Opportunity_Product__c;
    global Opportunity_Product__c Opportunity_Product__r;
    global List<FeedItem> Parent;
    global Double Planned_Quantity__c;
    global List<ProcessInstance> ProcessInstances;
    global List<ProcessInstanceHistory> ProcessSteps;
    global String Product_Name__c;
    global String Product_Range__c;
    global String Project_Code__c;
    global String Project_Segment__c;
    global Double Quantity_Sq_M_in_pending__c;
    global Double Quantity__c;
    global Boolean Re_Calculate__c;
    global Boolean Recent_upload__c;
    global List<RecordActionHistory> RecordActionHistories;
    global List<RecordAction> RecordActions;
    global List<CollaborationGroupRecord> RecordAssociatedGroups;
    global List<ContentDistribution> RelatedRecord;
    global Double SC_Quantity__c;
    global Datetime SystemModstamp;
    global List<TaskRelation> TaskRelations;
    global Double Ton__c;
    global List<TopicAssignment> TopicAssignments;
    global Double Total_Appropriable_Quantity__c;
    global Double Total_available_quantity__c;
    global Double Unplanned_Quantity__c;
    global String Year__c;
    global Double available_quantity__c;

    global Product_Forecast__c () 
    {
    }
}
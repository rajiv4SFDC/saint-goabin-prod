// This file is generated as an Apex representation of the
//     corresponding sObject and its fields.
// This read-only file is used by the Apex Language Server to
//     provide code smartness, and is deleted each time you
//     refresh your sObject definitions.
// To edit your sObjects and their fields, edit the corresponding
//     .object-meta.xml and .field-meta.xml files.

global class Project__c {
    global List<ActivityHistory> ActivityHistories;
    global String Address__c;
    global List<AttachedContentDocument> AttachedContentDocuments;
    global List<AttachedContentNote> AttachedContentNotes;
    global List<Attachment> Attachments;
    global Id City__c;
    global City__c City__r;
    global List<CombinedAttachment> CombinedAttachments;
    global List<ContactRequest> ContactRequests;
    global List<ContentDocumentLink> ContentDocumentLinks;
    global Boolean Cool_Homes__c;
    global User CreatedBy;
    global Id CreatedById;
    global Datetime CreatedDate;
    global String Description__c;
    global List<DuplicateRecordItem> DuplicateRecordItems;
    global List<EmailMessage> Emails;
    global List<EventRelation> EventRelations;
    global List<Event> Events;
    global List<EntitySubscription> FeedSubscriptionsForEntity;
    global List<Feedbacks__c> Feedbacks__r;
    global List<Project__Feed> Feeds;
    global List<ContentVersion> FirstPublishLocation;
    global Double Geolocation__Latitude__s;
    global Double Geolocation__Longitude__s;
    global Location Geolocation__c;
    global Boolean Glass_Govern__c;
    global Boolean Glass_Heals__c;
    global String Government_Sub_Segment__c;
    global List<Project__History> Histories;
    global Id Id;
    global String Inspire_Sales_Area__c;
    global List<Invoice__c> Invoices__r;
    global Boolean IsDeleted;
    global Date LastActivityDate;
    global User LastModifiedBy;
    global Id LastModifiedById;
    global Datetime LastModifiedDate;
    global Datetime LastReferencedDate;
    global Datetime LastViewedDate;
    global Datetime Last_Image_Tagged_Date__c;
    global Boolean Luxe_Glass__c;
    global Boolean Luxe_Homes__c;
    global List<Material_Request__c> Material_Requests__r;
    global String Name;
    global Boolean Non_Mission_Magnify__c;
    global List<Note> Notes;
    global List<NoteAndAttachment> NotesAndAttachments;
    global List<OpenActivity> OpenActivities;
    global List<Opportunity> Opportunities__r;
    global SObject Owner;
    global Id OwnerId;
    global List<FeedItem> Parent;
    global String Pin_Code__c;
    global List<Price_Request_Customer__c> Price_Request_Customers__r;
    global List<ProcessInstance> ProcessInstances;
    global List<ProcessInstanceHistory> ProcessSteps;
    global String Project_Code__c;
    global String Project_Location__c;
    global String Project_Status__c;
    global Datetime Project_Validated_Date__c;
    global Boolean Recent_uploaded__c;
    global List<RecordActionHistory> RecordActionHistories;
    global List<RecordAction> RecordActions;
    global List<CollaborationGroupRecord> RecordAssociatedGroups;
    global RecordType RecordType;
    global Id RecordTypeId;
    global String Reference__c;
    global Id Referrer_Account__c;
    global Account Referrer_Account__r;
    global String Region__c;
    global List<FlowRecordRelation> RelatedRecord;
    global List<OutgoingEmail> RelatedTo;
    global Boolean SG_Assured__c;
    global String Sales_Area__c;
    global String Segment__c;
    global List<Project__Share> Shares;
    global String Shower_Cubicles_Sales_Area__c;
    global Id State__c;
    global State__c State__r;
    global Datetime SystemModstamp;
    global Double Tagged_Image_Count__c;
    global String Tags__c;
    global List<TaskRelation> TaskRelations;
    global List<Task> Tasks;
    global List<TopicAssignment> TopicAssignments;
    global Boolean Validated__c;
    global Boolean X1st_Manual_Accounting_Done__c;
    global Boolean X5G_Surge__c;

    global Project__c () 
    {
    }
}
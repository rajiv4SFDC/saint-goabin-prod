// This file is generated as an Apex representation of the
//     corresponding sObject and its fields.
// This read-only file is used by the Apex Language Server to
//     provide code smartness, and is deleted each time you
//     refresh your sObject definitions.
// To edit your sObjects and their fields, edit the corresponding
//     .object-meta.xml and .field-meta.xml files.

global class Sales_Order_Line_Item__c {
    global List<ActivityHistory> ActivityHistories;
    global List<AttachedContentDocument> AttachedContentDocuments;
    global List<AttachedContentNote> AttachedContentNotes;
    global List<Attachment> Attachments;
    global Id Bill_To_Account__c;
    global Account Bill_To_Account__r;
    global String Bill_To_SAP_Code__c;
    global Double Cases__c;
    global String Coating__c;
    global List<CombinedAttachment> CombinedAttachments;
    global List<ContactRequest> ContactRequests;
    global List<ContentDocumentLink> ContentDocumentLinks;
    global User CreatedBy;
    global Id CreatedById;
    global Datetime CreatedDate;
    global List<DuplicateRecordItem> DuplicateRecordItems;
    global List<EmailMessage> Emails;
    global Id Enquiry_Line_Item__c;
    global Enquiry_Line_Item__c Enquiry_Line_Item__r;
    global List<EventRelation> EventRelations;
    global List<Event> Events;
    global String External_Id__c;
    global List<EntitySubscription> FeedSubscriptionsForEntity;
    global List<ContentVersion> FirstPublishLocation;
    global String Geography__c;
    global List<Sales_Order_Line_Item__History> Histories;
    global Id Id;
    global Id Invoice__c;
    global Invoice__c Invoice__r;
    global Double Invoiced_Quantity__c;
    global Boolean IsDeleted;
    global String Laminate__c;
    global Date LastActivityDate;
    global User LastModifiedBy;
    global Id LastModifiedById;
    global Datetime LastModifiedDate;
    global Datetime LastReferencedDate;
    global Datetime LastViewedDate;
    global String Length__c;
    global String Name;
    global List<Note> Notes;
    global List<NoteAndAttachment> NotesAndAttachments;
    global List<OpenActivity> OpenActivities;
    global Double Order_Quantity_In_Report__c;
    global SObject Owner;
    global Id OwnerId;
    global Double Pack_Type_Tons__c;
    global List<FeedItem> Parent;
    global String Pattern__c;
    global Double Pending_Quantity__c;
    global List<ProcessInstance> ProcessInstances;
    global List<ProcessInstanceHistory> ProcessSteps;
    global String Product_Category__c;
    global String Product_Item_Code__c;
    global Id Product__c;
    global Product2 Product__r;
    global Double Quantity_in_Tons__c;
    global List<RecordActionHistory> RecordActionHistories;
    global List<RecordAction> RecordActions;
    global List<CollaborationGroupRecord> RecordAssociatedGroups;
    global String Reference_Number__c;
    global String Region_Workflow__c;
    global String Region__c;
    global List<FlowRecordRelation> RelatedRecord;
    global List<OutgoingEmail> RelatedTo;
    global String Reporting_Range__c;
    global String Reporting_range_workflow__c;
    global Date Sales_Order_Date__c;
    global String Sales_Order_Line_Item_Number__c;
    global List<Sales_Order_Status__c> Sales_Order_Status__r;
    global Id Sales_Order__c;
    global Sales_Order__c Sales_Order__r;
    global String Sales_area__c;
    global String Sales_office__c;
    global Id Ship_To_Customer__c;
    global Ship_To_Customer__c Ship_To_Customer__r;
    global String Ship_To_SAP_Code__c;
    global String Shipping_Status__c;
    global String Sub_Family__c;
    global Datetime SystemModstamp;
    global List<TaskRelation> TaskRelations;
    global List<Task> Tasks;
    global String Thickness_Code__c;
    global String Tint__c;
    global List<TopicAssignment> TopicAssignments;
    global String Width__c;
    global String order_office_for_OrdInv__c;

    global Sales_Order_Line_Item__c () 
    {
    }
}
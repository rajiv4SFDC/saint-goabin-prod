// This file is generated as an Apex representation of the
//     corresponding sObject and its fields.
// This read-only file is used by the Apex Language Server to
//     provide code smartness, and is deleted each time you
//     refresh your sObject definitions.
// To edit your sObjects and their fields, edit the corresponding
//     .object-meta.xml and .field-meta.xml files.

global class Merchandise_Request__c {
    global String Address__c;
    global Date Approval_Submission_Date__c;
    global List<AttachedContentDocument> AttachedContentDocuments;
    global List<AttachedContentNote> AttachedContentNotes;
    global List<Attachment> Attachments;
    global String City_cus__c;
    global List<CombinedAttachment> CombinedAttachments;
    global List<ContactRequest> ContactRequests;
    global String Contact_No__c;
    global Id Contact__c;
    global Contact Contact__r;
    global List<ContentDocumentLink> ContentDocumentLinks;
    global Boolean Copy_Address_from_Contact__c;
    global User CreatedBy;
    global Id CreatedById;
    global Datetime CreatedDate;
    global List<DuplicateRecordItem> DuplicateRecordItems;
    global List<EmailMessage> Emails;
    global List<EventRelation> EventRelations;
    global List<EntitySubscription> FeedSubscriptionsForEntity;
    global List<Merchandise_Request__Feed> Feeds;
    global List<ContentVersion> FirstPublishLocation;
    global Id Id;
    global Boolean IsDeleted;
    global User LastModifiedBy;
    global Id LastModifiedById;
    global Datetime LastModifiedDate;
    global Datetime LastReferencedDate;
    global Datetime LastViewedDate;
    global String Name;
    global List<Note> Notes;
    global List<NoteAndAttachment> NotesAndAttachments;
    global Id Opportunity__c;
    global Opportunity Opportunity__r;
    global SObject Owner;
    global Id OwnerId;
    global List<FeedComment> Parent;
    global String Pin_Code__c;
    global List<ProcessInstance> ProcessInstances;
    global List<ProcessInstanceHistory> ProcessSteps;
    global Double Product_Item_Count__c;
    global List<RecordActionHistory> RecordActionHistories;
    global List<RecordAction> RecordActions;
    global List<CollaborationGroupRecord> RecordAssociatedGroups;
    global RecordType RecordType;
    global Id RecordTypeId;
    global List<ContentDistribution> RelatedRecord;
    global String Remarks__c;
    global Double Request_Age__c;
    global List<Request_Item__c> Request_Items__r;
    global String Request_Type__c;
    global Date Requesting_Date__c;
    global String Requesting_Year__c;
    global Boolean Send_Email_Notification__c;
    global Boolean Send_To_Self__c;
    global List<Merchandise_Request__Share> Shares;
    global String Shipment_Status__c;
    global String State_cus__c;
    global String Status__c;
    global Datetime SystemModstamp;
    global List<TaskRelation> TaskRelations;
    global String Technical_Team_Email__c;
    global String Test_Email_Field__c;
    global List<TopicAssignment> TopicAssignments;
    global Double Tracking_No__c;
    global String Type__c;

    global Merchandise_Request__c () 
    {
    }
}
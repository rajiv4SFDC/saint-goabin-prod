@isTest
private class ForecastController_Test{
  public  static testmethod void DisplayOptyProdRecordsgetOptyProd(){
        Test.startTest();
        Account acc = new Account();
        acc.Name ='test3';
        acc.Type ='Industry';
        acc.Sales_Area_Region__c ='North';
        insert acc;
        
        Opportunity opp = new Opportunity();
        opp.AccountId =acc.Id;
        opp.Name ='test1';
        opp.CloseDate = Date.today();
        opp.Amount = 1000;
        opp.StageName ='Prospecting';
        opp.Probability= 0.5;
        insert opp;
        
        Product2 pro = new Product2();
        pro.Name = opp.Id;
        pro.Product_Category__c ='Infinity';
        insert pro;
        
        Opportunity_Product__c oppproduct = new Opportunity_Product__c();
        oppproduct.Opportunity__c = opp.Id;
        insert oppproduct;
        
        Product_Forecast__c pf = new Product_Forecast__c();
        String Month;
        String oppdyid ;
        Id opportunityid;
        pf.quantity__c=10;
        pf.month__c='JAN';
        pf.year__c='2018';
        pf.Include_In_Sales_Plan__c=true;
        pf.Opportunity_Product__c   =oppproduct.id;
        oppdyid =oppproduct.id; 
        opportunityid= opp.id;
        Month ='January';
        
        insert pf;
        
        ForecastController.SaveResultWrapper wrapper1 = new ForecastController.SaveResultWrapper();
        wrapper1.errorFlag = true;
        wrapper1.errorMessage = 'testing';
        
        ForecastController.UploadRecordsWrapper wrapper2 = new  ForecastController.UploadRecordsWrapper();
        wrapper2.Month ='JAN';
        wrapper2.year='2018';
        wrapper2.optyprod =''; 
        wrapper2.quantity = 4;
        wrapper2.id =0 ;
         
        ForecastController.viewForecast(oppdyid);
        ForecastController.getOptyProd(opportunityid);
        ForecastController.getOptyProdCount(opportunityid);
        ForecastController.findById(opportunityid);
        ForecastController.getOptyProdYr(oppdyid);
        ForecastController.getOptyProdId(oppdyid);
        //  by sai  
       // ForecastController.saveMonthForecasts(System.JSON.serialize(Month));
        ForecastController.getProductUOM(oppdyid);
        Test.stopTest();
    }//end of the DisplayOptyProdRecords method
   
}//end of the test class
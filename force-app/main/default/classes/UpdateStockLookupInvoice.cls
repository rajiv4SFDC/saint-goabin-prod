/* Created By         : Rajiv Kumar Singh
   Created Date       : 11/07/2018
   Class Name         : UpdateStockLookupInvoice
   Description        : update the perticular stock record lookup field on invoice
   Last Modified By   : Rajiv Kumar Singh
   Last Modified Date : 11/07/2018
*/
public class UpdateStockLookupInvoice{

  @InvocableMethod(label='UpdateStockLookupInvoice' description='update the perticular stock record lookup field on invoice')
  public static void UpdateLookupValue(List<Id> SalesIds)
    {
       
       
       map<string,string> SalesWithProductMap = new map<string,string>();  
       set<string> presentProductCodesSet = new set<String>();
       list<Invoice__c> NewList = new list<Invoice__c>(); 
       list<Invoice__c> FinalListToUpdate = new list<Invoice__c>();

       for(Invoice__c ptp:[select id,Plant_location__c,Product_Stock__c from Invoice__c where id IN: SalesIds])
       {
         if(ptp.Plant_location__c!= null)
          {
            presentProductCodesSet.add(ptp.Plant_location__c);      
            NewList.add(ptp);
            system.debug('The ptp.id is: ' +ptp.id);
          }  
           
       }
       system.debug('The NewList is: ' +NewList+'......'+presentProductCodesSet);
       if(presentProductCodesSet.size()>0)
       {
          for(Product_Stock__c pd:[select id,Plant_Code__c from Product_Stock__c where Plant_Code__c in:presentProductCodesSet])
           {
            SalesWithProductMap.put(pd.Plant_Code__c,pd.id);
           }
       system.debug('The SalesWithProductMap is: ' +SalesWithProductMap);
        for(Invoice__c ptp:NewList)
        {
        ptp.Product_Stock__c = SalesWithProductMap.get(ptp.Plant_location__c);
        system.debug('The  ptp.product__c is: ' + ptp.Plant_location__c);
        FinalListToUpdate.add(ptp);
        }
       }
       
          update FinalListToUpdate;
       
    }
}
/* Created By: Rajiv Kumar Singh
Created Date: 05/06/2018
Class Name: ProductDimensionInsertAPI 
Story : product Dimension flow from SAP to SFDC
Last Modified By :Rajiv Kumar Singh
Last Modified Date: 05/06/2018
*/

@RestResource(urlMapping='/productDimension')
global class ProductDimensionInsertAPI {
    @HttpPost
    global static void doHandleInsertRequest(){
        
        List<DimensionWrapper> wrapList = new List<DimensionWrapper>();
        List<Product_Dimension__c> ProdimList = new List<Product_Dimension__c>();
        RestRequest Req = RestContext.request;
        RestResponse res = RestContext.response;
        String body = Req.requestBody.toString().trim();
        
        if(body != null && body != '') {
            
            try {
                wrapList = (List<DimensionWrapper>)JSON.deserialize(body, List<DimensionWrapper>.class);
                SYSTEM.DEBUG('JSON'+wrapList);
                
                for(DimensionWrapper we :wrapList){
                    Product_Dimension__c proDim = new Product_Dimension__c();
                    if(we.ProductItemCode!= null && we.ProductItemCode!=''){
                        proDim.Product_Item_Code__c = we.ProductItemCode;
                    }
                    if(we.Length!= null && we.Length!=''){
                        proDim.Length__c = we.Length;
                    }
                    if(we.Width!= null && we.Width!=''){
                        proDim.Width__c = we.Width;
                    }
                    if(we.thick!= null && we.thick!=''){
                        proDim.Thickness_Code__c = we.thick;
                    }
                    if(we.ProductItemCode!= null && we.thick!=null){
                        proDim.Product_External_ID__c = we.ProductItemCode+'-'+we.thick;
                    }
                    if(we.DimensionKey!= null && we.DimensionKey!=null){
                        proDim.Dimension_Unique_Key__c = we.DimensionKey;
                    }
                    ProdimList.add(proDim);
                }
                Schema.SObjectField ftoken = Product_Dimension__c.Fields.Dimension_Unique_Key__c;
                Database.UpsertResult[] srList = Database.upsert(ProdimList,ftoken,false);   
                res.statusCode = 200;
                res.responseBody = Blob.valueOf(String.valueof(ProdimList));
                
            } catch (Exception e) {
                
                CommonUtilitiesHandler.handleExceptions(e);
                res.statusCode = 500;
                res.responseBody = Blob.valueOf(e.getMessage());
            }
        } else {
            res.statusCode = 500;
            res.responseBody = Blob.valueOf('Invalid Request');
        } 
    }
    
    public class DimensionWrapper{
        public String ProductItemCode{get;set;}
        public String Length{get;set;}
        public String Width{get;set;}
        public String thick{get;set;}
        public String DimensionKey{get;set;}
        
    }
}
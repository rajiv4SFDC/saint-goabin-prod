Public class pricerequesthandler{

    public static void handlepricerequest(List<Price_Request__c> PricereqList){ 
 
         List<Opportunity_Product_line_items__c> OppProdLinItem = new List<Opportunity_Product_line_items__c>();
         List<Price_Request_Line_Item__c> PricReList = new List<Price_Request_Line_Item__c>();
         Set<id> ProdIdset = new Set<id>();
         id prid;
         decimal disc;
         date validFrom;
         date validTo;
        
         for(Price_Request__c pre : PricereqList){
             prid=pre.id;
             disc = pre.Discount__c;
             validFrom = pre.Valid_From__c;
             validTo = pre.Valid_To__c;
             
                 
                 if(pre.Opportunity_Product__c !=null){
                   
                      ProdIdset.add(pre.Opportunity_Product__c);
                 }   
                
         }
     
         if(ProdIdset.size() > 0){
              OppProdLinItem = [SELECT Opportunity_Product__c,Product__r.External_Id__c,Product__r.UOM__c,List_Price__c,Glass_Item__c,Product__r.Article_number__c,Quantity__c,Product__c FROM Opportunity_Product_line_items__c WHERE Opportunity_Product__c IN:ProdIdset];
              system.debug('list values'+OppProdLinItem);
          
              for(Opportunity_Product_line_items__c opprod :OppProdLinItem){
                 
                 if(opprod.Glass_Item__c !=true){
                 Price_Request_Line_Item__c pr = new Price_Request_Line_Item__c();
                 pr.Opportunity_Product__c = opprod.Opportunity_Product__c;
                 pr.Price_Request__c = prid;
                 pr.Quantity__c = opprod.Quantity__c;
                 pr.Product__c =opprod.Product__c;
                 pr.List_Price__c = opprod.List_Price__c;
                 pr.Discount__c = disc;
                 pr.Valid_From__c =validFrom;
                 pr.Valid_To__c = validTo;
                 Pr.Uom__c = opprod.Product__r.UOM__c;
                 pr.Product_Item_Code__c =opprod.Product__r.External_Id__c;
                 PricReList.add(pr);    
              
              }
              }
              if(PricReList.size() > 0){
              
              system.debug('Values to be inerted'+PricReList);
              INSERT PricReList;
              }
         }
     
     }
     
}
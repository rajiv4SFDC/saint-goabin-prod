@isTest
public class InvoiceAppropriationHelperForScTest{
    
    @testSetup
    public static void environmentSetup() {
        
    Account acc = new Account();
        acc.Name ='test3';
        acc.Type ='Industry';
        acc.Sales_Area_Region__c ='North';
        acc.RecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('ERP').getRecordTypeId();
        insert acc;
        
        Opportunity opp = new Opportunity();
        String oppid1;
        opp.AccountId =acc.Id;
        opp.Name ='test1';
        opp.CloseDate = Date.today();
        opp.Amount = 1000;
        opp.StageName ='Prospecting';
        opp.Probability= 0.5;
        oppid1=acc.id;
        insert opp;
        
        acc = new Account();
        acc.Name ='test31';
        acc.Type ='Industry1';
        acc.Sales_Area_Region__c ='South';
        acc.RecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('CRM').getRecordTypeId();
        insert acc;
        
        Key_Account__c keyAcct = new Key_Account__c();
        keyAcct.Opportunity__c = opp.Id;
        keyAcct.Account__c = acc.Id;
        insert keyAcct;
        
        opp.Customer_Name__c = keyAcct.Id;
        update opp;
        
        Product2 masterProduct = new Product2();
        masterProduct.Name = opp.id;
        masterProduct.Article_number__c ='test1234';
        masterProduct.Product_Category__c ='Shower Cubicles';
        masterProduct.master_product__c = true;
        insert masterProduct;
        
        Product2 pro = new Product2();
        pro.Name = 'test prod';
        pro.Product_Category__c ='Shower Cubicles';
        pro.ProductCode ='test123';
        pro.Parent_product__c = masterProduct.id;
        pro.Article_number__c = 'test123';
        pro.Quantity__c  = 2;
        pro.global_product__c = true;
        insert pro;   
        List<Product2> prodList = new List<product2>();
        prodList.add(pro);
        
        List<ProductsWrapper> prodWrapper =  new List<ProductsWrapper>();
        ProductsWrapper prodWrap = new ProductsWrapper();
        prodWrap.prdName = pro.Name;
        prodWrap.prdCode = pro.ProductCode;
        prodWrap.prdQuantitiy = Integer.valueof(pro.Quantity__c);
        prodWrap.prdArticlenumber = pro.Article_number__c;
        prodWrap.prdParentId = pro.Parent_product__c;
        //prodWrap.oppProductLineItemId = pro.Name;
        prodWrapper.add(prodWrap);
        
        Opportunity_Product__c oppproduct = new Opportunity_Product__c();
        oppproduct.Opportunity__c = opp.Id;
        oppproduct.Product__c = masterProduct.Id;
        insert oppproduct;
        
        Opportunity_Product_line_items__c oppline = new Opportunity_Product_line_items__c();
        oppline.Opportunity_Product__c=oppproduct.id;
        oppline.Product__c = pro.Id;
        oppline.Quantity__c = 2;
        insert oppline;
        
        Product_Forecast__c forecast = new Product_Forecast__c();
        forecast.Quantity__c = 10000;
        forecast.Opportunity_Product__c = oppproduct.Id;
        insert forecast;
        
        Price_Request__c pr = new Price_Request__c();
        pr.Price_Type__c   = 'Tier Price';
        pr.Opportunity_Product__c = oppproduct.id;
        pr.Valid_From__c = System.today();
        pr.Valid_To__c = System.today();
        pr.Requested_Quantity__c = 1000;
        pr.Approval_status__c ='Approved';
        insert pr;
        
           Price_Request_Line_Item__c pr2 = new Price_Request_Line_Item__c();
       // pr2.Approval_Status__c = 'Approved';
        pr2.Opportunity_Product__c = oppproduct.id;
       // pr2.Requested_Quantity__c = 12000;
        pr2.Price_Request__c = pr.id;
        pr2.Valid_From__c =  Date.newInstance(2017,8, 27);
        pr2.Valid_To__c = Date.newInstance(2017,8, 31);
        insert pr2;
        
        
        // create invoice here. 
        Price_Request_Line_Item__c prl = [select id,name from Price_Request_Line_Item__c limit 1];
        system.debug('The prl is: '+ prl);
        
         Invoice__c inv =  new Invoice__c(Name='Invoice Succ',
                                        Transaction_No__c=prl.Name,
                                        Billing_Date__c = Date.newInstance(2018,5,03),
                                        Non_Appropriable_Quantity__c = 0,
                                        Appropriable_Quantity__c = 0,
                                        Quantity__c = 500,
                                        Invoice_type__c = 'Shower Cubicles',
                                        To_be_processed__c=true,
                                        Processed__c = false,
                                        Tagged__c = false);
                                        insert inv;
                                        
                                        system.debug('the transaction no is: '  + inv.Transaction_No__c+'......'+prl.Name);
    String CRON_EXP = '0 0 17 1/1 * ? *';
    
    System.schedule('InvoiceTaggingScheduler', CRON_EXP, new InvoiceTaggingScheduler()); 
    
    database.executebatch(new InvoiceTaggingBatchable());

    }
    
    @isTest
    private static void fetchLookUpValuesTest() {
        
        List<Price_Request__c> priceReq = [SELECT Id, Discount__c, Valid_From__c, Valid_To__c, Opportunity_Product__c FROM Price_Request__c];
        pricerequesthandler.handlepricerequest(priceReq);
    }
}
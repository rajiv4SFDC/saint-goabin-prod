@isTest
public class MaterailRequestHandlerTest {
    static testMethod void testMaterialRequest() {
    	Material_Request__c Mat1=new Material_Request__c();
        try {
            Mat1.Width__c='22';
            Mat1.Approval_Status__c='Pending';
            Mat1.Expected_Production_Date__c=NULL;
            Mat1.Remarks__c = NULL;
            insert Mat1;          
            
            Mat1.Approval_Status__c = 'Approved by Planning/CSD';         
            Mat1.Remarks__c = null;   
            mat1.Expected_Production_Date__c= system.Today() ;   
            update Mat1;
            
            Mat1.Approval_Status__c = 'Approved by Planning/CSD';         
            Mat1.Remarks__c = 'test';   
            mat1.Expected_Production_Date__c= system.Today() ;   
            update Mat1;
            
            Mat1.Approval_Status__c = 'Rejected by Planning/CSD';
            update Mat1;
            
        } catch(Exception ex) {
            Boolean expectedExceptionThrown =  ex.getMessage().contains('Script-thrown exception') ? true : false;
			System.assertEquals(expectedExceptionThrown, false);
        }
    	
    	Material_Request__c Mat2 = new Material_Request__c();
        try {
            Mat2.Width__c='22';
            Mat2.Approval_Status__c='Pending';
            //Mat1.Expected_Production_Date__c=NULL;
            //Mat1.Remarks__c = NULL;
            insert Mat2;          
                     
            Mat2.Remarks__c = NULL;   
            Mat1.Approval_Status__c = 'Rejected by Planning/CSD';
            update Mat2;
            
        } catch(Exception ex) {
            Boolean expectedExceptionThrown =  ex.getMessage().contains('Script-thrown exception') ? true : false;
			System.assertEquals(expectedExceptionThrown, false);
        }
    
     // Database.SaveResult result = Database.insert(Mat1, false);
            //System.assertEquals('The Last Name "'+Mat1.Approval_Status__c+'" is not allowed for DML',result.getErrors()[0].getMessage());
    
     
     
      /*  
     Material_Request__c Mat2=new Material_Request__c();
     Mat2.Width__c='22';
     Mat2.Approval_Status__c='Pending';
      
      insert Mat2;  
        
    Mat2.Approval_Status__c = 'Approved by Planning/CSD'; 
    
        
     
    update Mat2;
    
      Database.SaveResult result = Database.insert(Mat1, false);
            System.assertEquals('The Last Name "'+Mat2.Approval_Status__c+'" is not allowed for DML',result.getErrors()[0].getMessage());
    
     mat2.Expected_Production_Date__c= system.Today() ;  
    */
     
    }
}
public class MockUpRequestTriggerHandler extends TriggerHandler {
	
    public MockUpRequestTriggerHandler() {}
    
    public override void beforeInsert() {
        //System.debug('MUR before Insert Start');
        new MockUpRequestManager().fetchBeforeInsertRecordsForUpdation((List<Mock_Up_Request__c>)Trigger.new );
        //System.debug('MUR before Insert end');
    }    

}
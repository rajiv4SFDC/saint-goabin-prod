/* Created By: Rajiv Kumar Singh
Created Date: 07/06/2017
Class Name: customerStatusUpdateAPI
Description : Update status on Account for ERP.
Last Modified By :Rajiv Kumar Singh
Last Modified Date: 13/06/2017
*/
@RestResource(urlMapping='/customerStatus')
global class customerStatusUpdateAPI {
    @HttpPost
    global static void doHandleInsertRequest(){
        
        List<statusWrapper> wrapList = new List<statusWrapper>();
        List<Account> accList = new List<Account>();
        RestRequest Req = RestContext.request;
        RestResponse res = RestContext.response;
        String body = Req.requestBody.toString().trim();
        
        if(body != null && body != '') {
            
            try {
                wrapList = (List<statusWrapper>)JSON.deserialize(body, List<statusWrapper>.class);
                for(statusWrapper we :wrapList){
                    Account acc = new Account();
                    if(we.SAPcode!=null && we.SAPcode!=''){
                        acc.Sap_Code__c = we.SAPcode;
                    }
                    if(we.Status!=null && we.Status=='True'){
                        acc.Status__c = boolean.valueof(we.Status);
                    }
                    if(we.Status!=null && we.Status=='False'){
                        acc.Status__c = boolean.valueof(we.Status);
                    }
                    if(we.BlockStatus!=null && we.BlockStatus=='True'){
                        acc.Credit_Blocked__c = boolean.valueof(we.BlockStatus);
                    }
                    if(we.BlockStatus!=null && we.BlockStatus=='False'){
                        acc.Credit_Blocked__c = boolean.valueof(we.BlockStatus);
                    }   
                    
                    accList.add(acc);  
                }
                Schema.SObjectField ftoken = Account.Fields.Sap_Code__c;
                Database.UpsertResult[] srList = Database.upsert(acclist,ftoken,false);           
                res.statusCode = 200;
                res.responseBody = Blob.valueOf(String.valueof(acclist));
                
                
            } catch (Exception e) {
                
                CommonUtilitiesHandler.handleExceptions(e);
                res.statusCode = 500;
                res.responseBody = Blob.valueOf(e.getMessage());
            }
        } else {
            res.statusCode = 500;
            res.responseBody = Blob.valueOf('Invalid Request');
        }   
    }
    
    public class statusWrapper{
        public String SAPcode{get;set;}
        public string Status{get;set;}
        public string BlockStatus{get;set;}
        
    }
}
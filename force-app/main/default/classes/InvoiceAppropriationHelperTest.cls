@isTest
public class InvoiceAppropriationHelperTest {
    
    public static String CRON_EXP = '0 0 17 1/1 * ? *';

    @testSetup
    public static void environmentSetup() {
        //Create Product
        //Create Project
        //Create Opportunity
       // Opportunity op = createOpportunity();
        //insert op;

        //Create Opportunity Product
       // Opportunity_Product__c oprod = createOpportunityProduct(op.id, null);
       // insert oprod;

        TestDataFactory.createCustomSetting();
        TestDataFactory.createTestProductData();
        TestDataFactory.createTestStateAndCityRecordData();
        TestDataFactory.createTestCatalogueBudgetData();
        TestDataFactory.createTestCampaignRecordData();
        TestDataFactory.createTestAccountData();
        TestDataFactory.createTestProjectData();
        TestDataFactory.createTestOpportunityData();
        TestDataFactory.createTestOpportunityProductData();
        TestDataFactory.createTestPriceRequestData();
        TestDataFactory.createTestProductForecateData();
        TestDataFactory.createTestForecateInvoiceData();
        TestDataFactory.createTestMockupRecordData();
        TestDataFactory.createTestInvoiceRecordData();
        TestDataFactory.createTestTargetActualRecordData();
    }


      /****************/
     /* Test Method  */
    /****************/

    //Unit Test queryPriceRequest
    testmethod public static void qprTestCase1(){
        //Create Price Request
        List<Price_Request__c> prList = new List<Price_Request__c>();
       // for(Opportunity_Product__c oprod : [SELECT ID FROM Opportunity_Product__c]) {
        //    prList.add(createPriceRequest(oprod.id,100));
       // }
       // insert prList;

        Set<String> emptyTransactionIdSet = new Set<String>();
        Set<String> randomTransactionIdSet = new Set<String>{'TR98769961','TR98769961'};
        Set<String> existingTransactionIdSet = new Set<String>();
        for(Price_Request__c pr : [SELECT Name FROM Price_Request__c]) {
            existingTransactionIdSet.add(pr.Name);
        }
        Test.startTest();
            //Test With No Transaction Id exist in the database
            
            prList = InvoiceAppropriationHelper.queryPriceRequest(randomTransactionIdSet);
            
            //Test With Transaction Id exist in the database
            prList = InvoiceAppropriationHelper.queryPriceRequest(emptyTransactionIdSet);
            
            //Test With Empty Transaction Id Set exist in the database
            prList = InvoiceAppropriationHelper.queryPriceRequest(existingTransactionIdSet);
           
            Date startdate = Date.newInstance(2017, 8, 1);
            String jobId = System.schedule('InvoiceTaggingScheduler', CRON_EXP, new InvoiceTaggingScheduler()); 
        
        Test.stopTest();
    }

    //Test queryInvoice
    testmethod public static void qiTestCase1(){
        Test.startTest();
        //Test With No Invoice Id exist in the database

        //Test With Invoice Id exist in the database

        //Test With Empty Invoice Id Set exist in the database

        Invoice__c inv = [SELECT id,To_be_processed__c,
                          Processed__c,Tagged__c,Status__c,
                          Appropriable_Quantity__c
                          FROM Invoice__c
                          WHERE Name ='Invoice Succ' LIMIT 1];

        inv.Status__c = 'Reversed';
        inv.Appropriable_Quantity__c = 200;
        inv.Invoice_type__c='Infinity';
        update inv;

        Date startdate = Date.newInstance(2017, 8, 1);
        String jobId = System.schedule('InvoiceTaggingScheduler', CRON_EXP, new InvoiceTaggingScheduler()); 
         Test.stopTest();
    }

    testmethod public static void prTestCase2(){
        Test.startTest();
        Price_Request__c priceReq = [SELECT id,Name FROM Price_Request__c LIMIT 1];
        Invoice__c inv = [SELECT id,Price_Request__c
                          FROM Invoice__c WHERE Price_Request__c =null
                          LIMIT 1];
        inv.Price_Request__c =  priceReq.id;    
        inv.Invoice_type__c='Infinity';            
        update inv;

        Database.executeBatch( new RecomputeAppropriationBatchable(inv.Price_Request__c));
         Database.executeBatch( new RecomputeAppropriationBatchable(new Set<Id>{inv.Price_Request__c} ));
        
        Test.stopTest();
    }




    //Test for Invoices which has Product Forecast
    //Test for Invoices which doesn't have Product Forecast with one, two & three Invoices for same month

    //Test for Case where there is no Price Request for Invoice
    //Test for Case where there is Price Request but no Billing Date
    //Test for Invoice cancellation
    //Test for Recalculating Price Request


    
    // Create Opportunity //
    static Opportunity createOpportunity() {
        Opportunity op = new Opportunity();
        op.StageName = 'Confirmed';
        op.CloseDate = System.today();
        op.Name = 'Test';
        return op;
    }

    // Create Opportunity Product //
     static Opportunity_Product__c createOpportunityProduct(Id opportunityId, Id productId){
        Opportunity_Product__c op = new Opportunity_Product__c();
        op.Opportunity__c = opportunityId;
        return op;
    }

    //* Create Price Request *//
      static Price_Request__c createPriceRequest(Id opportunityProductId, Double quantity){
        Price_Request__c pr = new Price_Request__c();
        pr.Approval_Status__c = 'Approved';
        pr.Opportunity_Product__c = opportunityProductId;
        pr.Requested_Quantity__c = quantity;
        return pr;
    }

    //* Create Invoice *//
    static Invoice__c createInvoice(String transactionNumber, Date billing_Date, Double quantity){
        return null;    
    }
}
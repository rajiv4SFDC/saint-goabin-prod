/* Created By: Rajiv Kumar Singh
Created Date: 07/06/2017
Class Name: CustomertshipToUpsertAPITest
Description : Test class for CustomertshipToUpsertAPI
Last Modified By :Rajiv Kumar Singh
Last Modified Date: 07/06/2017
*/

@IsTest
private class CustomertshipToUpsertAPITest{
    
    static testMethod void testPostMethod(){
        String JSONMsg = '[{"BillToCode":"3321","ShipToCode":"True","ShipToName":"3321","ShipToAdress":"True","ShipToOffice":"321"}]';
        RestRequest request = new RestRequest();
        request.requestUri ='/services/apexrest/shipToUpsertAPI';
        request.httpMethod = 'POST';
        request.requestBody = Blob.valueof(JSONMsg);
        RestContext.request = request;
        RestContext.response = new RestResponse();
        CustomertshipToUpsertAPI.doHandleInsertRequest();
        
        JSONMsg = '[1';
        request = new RestRequest();
        request.requestUri ='/services/apexrest/shipToUpsertAPI';
        request.httpMethod = 'POST';
        request.requestBody = Blob.valueof(JSONMsg);
        RestContext.request = request;
        RestContext.response = new RestResponse();
        CustomertshipToUpsertAPI.doHandleInsertRequest();
        
        request = new RestRequest();
        request.requestUri ='/services/apexrest/shipToUpsertAPI';
        request.httpMethod = 'POST';
        request.requestBody = Blob.valueof('');
        RestContext.request = request;
        RestContext.response = new RestResponse();
        CustomertshipToUpsertAPI.doHandleInsertRequest();
    }
}
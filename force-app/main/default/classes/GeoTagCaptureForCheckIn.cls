/******************************************
* Created By  : Ajeet Singh Shekhawat
* Created On  : 22 Nov 2018
* Modified By : Ajeet Singh Shekhawat
* Modified On : 22 Nov 2018
* Description :  
* 
******************************************/ 
public class GeoTagCaptureForCheckIn {
    
    @AuraEnabled
    public static Map<String, Object> createGeoTagRecords(String checkInData) {
        
        System.debug('Insert checkInData  **** ' + checkInData);
        List<Geo_Tagging__c> geoTagList = new List<Geo_Tagging__c>();
        List<Object> geoTagObject;
        Geo_Tagging__c geoTag;
        Map<String, Object> res = new Map<String, Object>();
        
        try {
            
            geoTagObject = (List<Object>) JSON.deserializeUntyped(checkInData);
            System.debug('geoTagObject  ****** ' + geoTagObject);
            system.debug('UERID:'+UserInfo.getUserId());
            //check if user has already performed start of day
            List<Geo_Tagging__c> startMydayCheck = [SELECT Id, End_My_Day__c, CreatedDate, CreatedById FROM Geo_Tagging__c WHERE CreatedDate = TODAY AND CreatedById = :UserInfo.getUserId()];
            System.debug('startMydayCheck  ****** ' + startMydayCheck);
            if(startMydayCheck.size() > 0) {
                
                res.put('STATUS', 'Failure');
                res.put('GEOTAG', startMydayCheck[0]); 
                
            } else {
                System.debug('coming in else');
                for (Object iterateObjList : geoTagObject) {
                    System.debug('iterateObjList  ****** ' + iterateObjList);
                    if(iterateObjList != null) {
                        Map<String, Object> geoTagMap = (Map<String, Object>) iterateObjList;
                        geoTag = new Geo_Tagging__c();
                        System.debug('geoTagMap of 1st method  ****** ' + geoTagMap);
                        if(geoTagMap.get('Start_My_Day__latitude__s') != null && geoTagMap.get('Start_My_Day__latitude__s') != '') {
                            geoTag.Start_My_Day__latitude__s = Decimal.valueOf((String)geoTagMap.get('Start_My_Day__latitude__s'));
                            System.debug('coming lati ****** ' + geoTag.Start_My_Day__latitude__s);
                        }         
                        if(geoTagMap.get('Start_My_Day__longitude__s') != null && geoTagMap.get('Start_My_Day__longitude__s') != '') {
                            geoTag.Start_My_Day__longitude__s = Decimal.valueOf((String)geoTagMap.get('Start_My_Day__longitude__s'));
                            System.debug('coming long ****** ' + geoTag.Start_My_Day__longitude__s);
                        }  
                        geoTag.Visit_Date__c = DateTime.now();
                        geoTagList.add(geoTag);  
                    }
                }
                System.debug('Insert geoTagList  **** ' + geoTagList);
                if(geoTagList.size() > 0 && !geoTagList.isEmpty()){	// null check
                    insert geoTagList;
                } 
            }
        } catch(Exception ex) {
            throw new AuraHandledException(ex.getMessage());
        }
        
        return res;
    }
    
    @AuraEnabled
    public static List<CheckIn_CheckOut__c> createcheckInRecords(String checkInData) {
        Integer countClickOnCheckIn = 1;
        List<CheckIn_CheckOut__c> checkInList = new List<CheckIn_CheckOut__c>();
        Location currentLocation;
        Double currentCheckInLat;
        Double currentCheckOutLon;
        
        // for 1st checkin for particular day
        //if(countClickOnCheckIn == 1) {
        //++countClickOnCheckIn;
        
        try {
            
            List<Geo_Tagging__c> getUserGeoTagRecord = [SELECT Id, End_My_Day__c 
                                                        FROM Geo_Tagging__c 
                                                        WHERE CreatedDate = TODAY 
                                                        AND CreatedById = :UserInfo.getUserId()];
            
            
            System.debug('Insert checkInData1  **** ' + checkInData);
            System.debug('getUserGeoTagRecord  **** ' + getUserGeoTagRecord);
            List<Object> checkInObject = (List<Object>) JSON.deserializeUntyped(checkInData);
            System.debug('--> '+checkInObject);
            CheckIn_CheckOut__c geoTag;
            
            for (Object iterateObjList : checkInObject) {
                System.debug('inside for loop **** ' + iterateObjList);
                if(iterateObjList != null) {
                    Map<String, Object> geoTagMap = (Map<String, Object>) iterateObjList;
                    geoTag = new CheckIn_CheckOut__c();
                    
                    System.debug('**** '+geoTagMap);
                    if(geoTagMap.get('CheckIn__latitude__s') != null && geoTagMap.get('CheckIn__latitude__s') != '') {
                        System.debug('inside if  CheckIn__latitude__s **** '+geoTagMap.get('CheckIn__latitude__s'));
                        geoTag.CheckIn__latitude__s = Decimal.valueOf((String)geoTagMap.get('CheckIn__latitude__s'));
                    }         
                    if(geoTagMap.get('CheckIn__longitude__s') != null && geoTagMap.get('CheckIn__longitude__s') != '') {
                        geoTag.CheckIn__longitude__s = Decimal.valueOf((String)geoTagMap.get('CheckIn__longitude__s'));
                    }  
                    if(getUserGeoTagRecord.size() > 0) {
                        geoTag.Geo_Tagging__c = getUserGeoTagRecord[0].Id;
                    }  
                    if((geoTagMap.get('CheckIn__latitude__s') != null && geoTagMap.get('CheckIn__latitude__s') != '') && (geoTagMap.get('CheckIn__longitude__s') != null && geoTagMap.get('CheckIn__longitude__s') != '')) {
                        currentCheckInLat = Decimal.valueOf((String)geoTagMap.get('CheckIn__latitude__s'));
                        currentCheckOutLon = Decimal.valueOf((String)geoTagMap.get('CheckIn__longitude__s'));
                        currentLocation = Location.newInstance(currentCheckInLat, currentCheckOutLon);
                    }  
                    System.debug('geoTagMap  **** ' + geoTagMap);
                    /*
if(startDayLocInstance != null && currentLocation != null) {
geoTag.Distance_Travel__c = Location.getDistance(startDayLocInstance, currentLocation, 'mi');
} 
*/
                    geoTag.Visit_Date__c = DateTime.now();
                    checkInList.add(geoTag);  
                }
            }
            System.debug('Insert checkInList  **** ' + checkInList);
            if(checkInList.size() > 0){	// null check
                insert checkInList;
            }  
        } catch(Exception ex) {
            throw new AuraHandledException(ex.getMessage());
        }
        
        /*
} else {
// for multiple checkin for particular day
try {
System.debug('Insert checkInData  **** ' + checkInData);
List<Object> checkInObject = (List<Object>) JSON.deserializeUntyped(checkInData);
CheckIn_CheckOut__c geoTag;

Geo_Tagging__c geoTagId = [Select id, Start_My_Day__c  From Geo_Tagging__c order by createdDate Desc Limit 1];
Location startDayLoc = geoTagId.Start_My_Day__c;
Double startDayLat = startDayLoc.latitude;
Double startDayLon = startDayLoc.longitude;
Location startDayLocInstance = Location.newInstance(startDayLat, startDayLon); 
System.debug('get geoTagId lastcreated is ' + geoTagId);  

CheckIn_CheckOut__c checkInOutLastRecord = [Select id, CheckOut__c  From CheckIn_CheckOut__c 
Where Geo_Tagging__c =: geoTagId.Id order by createdDate Desc Limit 1];


Location checkOutLoc = checkInOutLastRecord.CheckOut__c;
Double checkOutLat = checkOutLoc.latitude;
Double checkOutLon = checkOutLoc.longitude;
Location checkOutLocInstance = Location.newInstance(checkOutLat, checkOutLon);
System.debug('get llocation is ' + checkInOutLastRecord);
for (Object iterateObjList : checkInObject) {

if(iterateObjList != null) {
Map<String, Object> geoTagMap = (Map<String, Object>) iterateObjList;
geoTag = new CheckIn_CheckOut__c();

if(geoTagMap.get('CheckIn__latitude__s') != null && geoTagMap.get('CheckIn__latitude__s') != '') {
geoTag.CheckIn__latitude__s = Decimal.valueOf((String)geoTagMap.get('CheckIn__latitude__s'));
}         
if(geoTagMap.get('CheckIn__longitude__s') != null && geoTagMap.get('CheckIn__longitude__s') != '') {
geoTag.CheckIn__longitude__s = Decimal.valueOf((String)geoTagMap.get('CheckIn__longitude__s'));
}  
if(geoTagId != null) {
geoTag.Geo_Tagging__c = geoTagId.Id;
}  
if((geoTagMap.get('CheckIn__latitude__s') != null && geoTagMap.get('CheckIn__latitude__s') != '') && (geoTagMap.get('CheckIn__longitude__s') != null && geoTagMap.get('CheckIn__longitude__s') != '')) {
currentCheckInLat = Decimal.valueOf((String)geoTagMap.get('CheckIn__latitude__s'));
currentCheckOutLon = Decimal.valueOf((String)geoTagMap.get('CheckIn__longitude__s'));
currentLocation = Location.newInstance(currentCheckInLat, currentCheckOutLon);
}  

if(checkOutLocInstance != null && currentLocation != null) {
geoTag.Distance_Travel__c = Location.getDistance(checkOutLocInstance, currentLocation, 'mi');
}  

geoTag.Visit_Date__c = DateTime.now();
checkInList.add(geoTag);  
}
}
System.debug('Insert checkInList  **** ' + checkInList);
if(checkInList.size() > 0 && !checkInList.isEmpty()){	// null check
Insert checkInList;
}  
} catch(Exception ex) {
throw new AuraHandledException(ex.getMessage());
}
}
*/
        List<CheckIn_CheckOut__c> checkInListData = GeoTagCaptureForCheckIn.getUserCheckInRecords();
        return checkInListData;
    }
    
    @AuraEnabled
    public static List<CheckIn_CheckOut__c> createcheckOutRecords(String checkInData) {
        try {
            System.debug('Insert checkInData  **** ' + checkInData);
            List<CheckIn_CheckOut__c> checkInList = new List<CheckIn_CheckOut__c>();
            List<Object> checkInObject = (List<Object>) JSON.deserializeUntyped(checkInData);
            CheckIn_CheckOut__c geoTag;
            Decimal longitude, latitude;            
            Location loc1, loc2;
            Double dist;
            
            CheckIn_CheckOut__c checkInId = [SELECT Id FROM CheckIn_CheckOut__c WHERE CreatedById = :UserInfo.getUserId() AND CreatedDate = TODAY ORDER BY Id DESC LIMIT 1];
            System.debug('checkInId ---8888- ' + checkInId);
            //get the last checkin record and nothing is available then get the geo tag record checkin details
            List<CheckIn_CheckOut__c> lastCheckInRecord = [SELECT CheckOut__latitude__s, CheckOut__longitude__s 
                                                           FROM CheckIn_CheckOut__c 
                                                           WHERE CreatedById = :UserInfo.getUserId() AND CreatedDate = TODAY 
                                                           AND CheckOut_Completed__c = true AND Id != :checkInId.Id
                                                           ORDER BY Id DESC LIMIT 1];
            System.debug('lastCheckInRecord ---- ' + lastCheckInRecord);
            if(lastCheckInRecord.size() == 0) {
                
                List<Geo_Tagging__c> getTagRecords = [SELECT Id, Start_My_Day__Latitude__s, Start_My_Day__Longitude__s 
                                                      FROM Geo_Tagging__c 
                                                      WHERE CreatedDate = TODAY AND CreatedById = :UserInfo.getUserId()];
                longitude = getTagRecords[0].Start_My_Day__Longitude__s;
                latitude = getTagRecords[0].Start_My_Day__Latitude__s;
                
            } else {
                
                latitude = lastCheckInRecord[0].CheckOut__Latitude__s;
                longitude = lastCheckInRecord[0].CheckOut__Longitude__s;
            }
            System.debug('latitude  **** ' + latitude + 'longitude ----' + longitude);
            System.debug('checkInObject  **** ' + checkInObject);
            for (Object iterateObjList : checkInObject) {
                
                if(iterateObjList != null) {
                    Map<String, Object> geoTagMap = (Map<String, Object>) iterateObjList;
                    geoTag = new CheckIn_CheckOut__c();
                    System.debug('geoTagMap  **++++** ' + geoTagMap);
                    
                    if(geoTagMap.get('CheckOut__Latitude__s') != null && geoTagMap.get('CheckOut__Latitude__s') != '') {
                        
                        geoTag.CheckOut__Latitude__s = Decimal.valueOf(String.valueOf(geoTagMap.get('CheckOut__Latitude__s')));
                        System.debug('geoTag.CheckOut__latitude__s   **++++** ' + geoTag.CheckOut__Latitude__s);
                    }         
                    if(geoTagMap.get('CheckOut__Longitude__s') != null && geoTagMap.get('CheckOut__Longitude__s') != '') {
                        
                        geoTag.CheckOut__Longitude__s = Decimal.valueOf((String.valueOf(geoTagMap.get('CheckOut__Longitude__s'))));
                    }  
                    
                    Map<String, Object> acct = (Map<String, Object>)geoTagMap.get('Account__c');
                    system.debug('acct:'+acct);
                    if(acct != null) {
                        
                        geoTag.Account__c  = (Id)acct.get('Id');
                    }
                    geoTag.Minutes_of_Meeting__c = (String)geoTagMap.get('Minutes_of_Meeting__c');
                    
                    geoTag.Reason_of_Visit__c  = (String)geoTagMap.get('Reason_of_Visit__c');
                    
                    if(geoTagMap.get('Prospect__c') != null) {
                        
                        geoTag.Prospect__c  = (String)geoTagMap.get('Prospect__c');
                    }
                    if(checkInId != null) {
                        geoTag.Id = checkInId.Id;
                    }  
                    
                    loc1 = Location.newInstance(latitude, longitude);
                    loc2 = Location.newInstance(geoTag.CheckOut__Latitude__s, geoTag.CheckOut__Longitude__s);          
                    System.debug('loc1  **++++** ' + loc1 + 'loc2 ==== ' + loc2);
                    System.debug('Insert checkInList  **** ' + geoTag);
                    
                    if(!Test.isRunningTest()) {
                        
                        geoTag.Distance_Travel__c = Decimal.valueOf(Location.getDistance(loc1, loc2, 'mi'));
                        //System.debug('CheckOut Record Distance  **** ' + Decimal.valueOf(Location.getDistance(loc1, loc2, 'mi')));
                        checkInList.add(geoTag); 
                    }
                }
            }
           // System.debug('Insert checkInList  **** ' + checkInList);
            if(checkInList.size() > 0){	// null check
                update checkInList;
            }  
            
            List<CheckIn_CheckOut__c> checkInListData = GeoTagCaptureForCheckIn.getUserCheckInRecords();
            
            return checkInListData;
            
        } catch(Exception ex) {
            throw new AuraHandledException(ex.getMessage());
        }
    }
    
    @AuraEnabled
    public static List<CheckIn_CheckOut__c> createEndDayRecords(String checkInData) {
        
        List<CheckIn_CheckOut__c> checkInListData = new List<CheckIn_CheckOut__c>();
        
        try {
            System.debug('createEndDayRecords  **** ' + checkInData);
            
            List<Geo_Tagging__c> geoTagList = new List<Geo_Tagging__c>();
            List<Object> geoTagObject = (List<Object>) JSON.deserializeUntyped(checkInData);
            Geo_Tagging__c geoTag;
            Geo_Tagging__c geoTagId = [SELECT Id FROM Geo_Tagging__c WHERE CreatedDate = TODAY AND CreatedById = :UserInfo.getUserId() LIMIT 1];
            for (Object iterateObjList : geoTagObject) {
                
                if(iterateObjList != null) {
                    Map<String, Object> geoTagMap = (Map<String, Object>) iterateObjList;
                    geoTag = new Geo_Tagging__c();
                    
                    if(geoTagMap.get('End_My_Day__latitude__s') != null && geoTagMap.get('End_My_Day__latitude__s') != '') {
                        geoTag.End_My_Day__latitude__s = Decimal.valueOf((String)geoTagMap.get('End_My_Day__latitude__s'));
                    }         
                    if(geoTagMap.get('End_My_Day__longitude__s') != null && geoTagMap.get('End_My_Day__longitude__s') != '') {
                        geoTag.End_My_Day__longitude__s = Decimal.valueOf((String)geoTagMap.get('End_My_Day__longitude__s'));
                    }  
                    if(geoTagId != null) {
                        geoTag.Id = geoTagId.Id;
                    }  
                    geoTagList.add(geoTag);  
                }
            }
            
            //create a last record in checkin/checkout obj after user logs out
            CheckIn_CheckOut__c checkin = new CheckIn_CheckOut__c();
            checkin.Geo_Tagging__c = geoTag.Id;
            checkin.CheckIn__Latitude__s = geoTag.End_My_Day__latitude__s;
            checkin.CheckIn__Longitude__s = geoTag.End_My_Day__longitude__s;
            checkin.CheckOut__Latitude__s = geoTag.End_My_Day__latitude__s;
            checkin.CheckOut__Longitude__s = geoTag.End_My_Day__longitude__s;
            checkin.Minutes_of_Meeting__c = 'N/A';
            checkin.Reason_of_Visit__c = 'N/A';
            checkin.Prospect__c = 'LOGGED OUT';
            checkin.Visit_Date__c = DateTime.now();
            
            List<CheckIn_CheckOut__c> lastCheckInRecord = [SELECT CheckOut__latitude__s, CheckOut__longitude__s 
                                                           FROM CheckIn_CheckOut__c 
                                                           WHERE CreatedById = :UserInfo.getUserId() AND CreatedDate = TODAY 
                                                           AND CheckOut_Completed__c = true 
                                                           ORDER BY Id DESC LIMIT 1];
        
            if(lastCheckInRecord.size() > 0) {
                
                Location loc1 = Location.newInstance(lastCheckInRecord[0].CheckOut__latitude__s, lastCheckInRecord[0].CheckOut__longitude__s);
                Location loc2 = Location.newInstance(geoTagList[0].End_My_Day__latitude__s, geoTagList[0].End_My_Day__longitude__s);  
                checkin.Distance_Travel__c = Decimal.valueOf(Location.getDistance(loc1, loc2, 'mi'));
                System.debug('EndDay Record Distance  **** ' + Decimal.valueOf(Location.getDistance(loc1, loc2, 'mi')));
            } else {
                
                checkin.Distance_Travel__c = 0;
            }
            insert checkin;
            
            System.debug('Insert geoTagList  **** ' + geoTagList);
            if(geoTagList.size() > 0){	// null check
                update geoTagList;
            }  
            
            checkInListData = GeoTagCaptureForCheckIn.getUserCheckInRecords();
            
        } catch(Exception ex) {
            throw new AuraHandledException(ex.getMessage());
        }
        
        return checkInListData;
    }
    
    // check when goe tagging record is created
    @AuraEnabled
    public static Boolean checkForStartMyDayRecords() {
        Integer listSize;
        Boolean check;
        try {
            List<Geo_Tagging__c> geoTagList = new List<Geo_Tagging__c>([SELECT Id FROM Geo_Tagging__c WHERE createdDate = Today AND CreatedById = :UserInfo.getUserId()]);
            listSize = geoTagList.size();
            if(listSize >= 1) {
                check = true;
            } else {
                check = false;
            }
        } catch(Exception ex) {
            throw new AuraHandledException(ex.getMessage());
        }
        return check;
    }
    
    @AuraEnabled
    public static Map<String, Object> getCheckInOutRecords() {
        List<CheckIn_CheckOut__c> checkInList;
        Map<String, Object> res = new Map<String, Object>();
        List<Map<String, String>> prospectOptions = new List<Map<String, String>>();
        Map<String, String> tmp;    
        
        try {
            
            checkInList = GeoTagCaptureForCheckIn.getUserCheckInRecords();
            res.put('CHECKIN_RECORDS', checkInList);
            
            Schema.DescribeFieldResult fieldResult = CheckIn_CheckOut__c.Prospect__c.getDescribe();
            List<Schema.PicklistEntry> ple = fieldResult.getPicklistValues();
            for( Schema.PicklistEntry f : ple)
            {
                tmp = new Map<String, String>();
                tmp.put('label', f.getLabel());
                tmp.put('value',  f.getValue());
                prospectOptions.add(tmp);
            }       
            res.put('PROSPECTS', prospectOptions);
            
        } catch(Exception ex) {
            throw new AuraHandledException(ex.getMessage());
        }
        
        System.debug('getCheckInOutRecords  **** ' + checkInList);
        return res;
    }
    
    private static List<CheckIn_CheckOut__c> getUserCheckInRecords() {
        
        List<CheckIn_CheckOut__c> checkInList = [SELECT Id, Visit_Date__c, Location__c, Reason_of_Visit__c, 
                                                 Minutes_of_Meeting__c, Account__c, Account__r.Name, CheckOut_Completed__c,
                                                 Prospect__c, CheckIn__latitude__s, CheckIn__longitude__s, 
                                                 Geo_Tagging__r.End_My_Day__latitude__s 
                                                 FROM CheckIn_CheckOut__c 
                                                 WHERE CreatedDate = TODAY AND CreatedById = :UserInfo.getUserId() 
                                                 ORDER BY Id ASC];
        System.debug('Inside getUserCheckInRecords()**** ' + checkInList);
        return checkInList;
    }
}
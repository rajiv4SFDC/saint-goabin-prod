public class Account_NOD_OSBalance {
 	@AuraEnabled 
    public static List<NODs__c> nod(String region, String salesOffice, String Month, String Year){
        Map<String,Decimal> mapOfMonth = new Map<String,Decimal>{'Jan' => 1, 'Feb' => 2, 'Mar' => 3, 'Apr' => 4, 'May' => 5, 
                                                                 'Jun' => 6, 'Jul' => 7, 'Aug' => 8, 'Sep' => 9, 'Oct' => 10, 
            													 'Nov' => 11, 'Dec' => 12};
        String convertMonthToNumber = String.ValueOf(mapOfMonth.get(Month));
        System.debug('convertMonthToNumber -> '+convertMonthToNumber);
        List<NODs__c> nodList = [SELECT id, Account__c, Account__r.Name, Account__r.Sales_Area_Region__c, Account__r.Sales_Office_ERP__c,  
                                 Account_Wise_NOD__c, Sales_Area_Wise_NOD__c, Average_Outstanding__c, SAP_Code__c FROM NODs__c	
                                 WHERE Account__r.Sales_Area_Region__c =:region AND Account__r.Sales_Office_ERP__c =:salesOffice AND 
                                 CALENDAR_MONTH(CreatedDate) =:Integer.ValueOf(convertMonthToNumber) AND CALENDAR_YEAR(CreatedDate) =:Integer.ValueOf(Year) 
                                 ORDER BY CreatedDate DESC];
        System.debug('nodList --> '+nodList+' month->'+Month+' Year->'+Year);
        Set<id> accountIds = new Set<id>();
        List<NODs__c> newNodList = new List<NODs__c>();
        for(NODs__c nods : nodList){
            if(!accountIds.Contains(nods.Account__c)){
                newNodList.add(nods);
                accountIds.add(nods.Account__c);
            }                
        }
        return newNodList;
    }  
    
/*    @AuraEnabled 
    public static List<O_S_Balance__c> osBalance(String region, String salesOffice, String Month, String Year){
        Map<String,Decimal> mapOfMonth = new Map<String,Decimal>{'Jan' => 1, 'Feb' => 2, 'Mar' => 3, 'Apr' => 4, 'May' => 5, 
                                                                 'Jun' => 6, 'Jul' => 7, 'Aug' => 8, 'Sep' => 9, 'Oct' => 10, 
            													 'Nov' => 11, 'Dec' => 12};
        String convertMonthToNumber = String.ValueOf(mapOfMonth.get(Month));
        List<O_S_Balance__c> osBalanceList = [SELECT id, Name, Account__c, Account__r.Name, Opening_Balance__c, Closing_balance__c, Transaction_Date__c,
                                              Account__r.Sales_Area_Region__c, Account__r.Sales_Office_ERP__c FROM O_S_Balance__c WHERE 
                                 			  Account__r.Sales_Area_Region__c =:region AND Account__r.Sales_Office_ERP__c =:salesOffice   
                                              AND CALENDAR_MONTH(CreatedDate) =:Integer.ValueOf(convertMonthToNumber) AND CALENDAR_YEAR
                                              (CreatedDate) =:Integer.ValueOf(Year) ORDER BY CreatedDate DESC];
        List<O_S_Balance__c> osBalList = new List<O_S_Balance__c>();
        Set<id> accountIds = new Set<id>();
        for(O_S_Balance__c os : osBalanceList){
            if(!accountIds.contains(os.Account__c)){
                osBalList.add(os);
                accountIds.add(os.Account__c);            }
        }
        System.debug('osBalanceList --> '+osBalanceList);
        return osBalList;
    }   
*/	
    @AuraEnabled
    //to get years
    public static List<String> selectedYear(){
        String startYear = Label.StartYear;
		String endYear = Label.End_Year;
        List<String> yearList = new List<String>();
        for(Decimal i = Decimal.ValueOf(startYear) ; i<= Decimal.ValueOf(endYear) ;i++){
            yearList.add(String.ValueOf(i));
        }
        System.debug('yearList '+yearList);
        return yearList;    
    }

	 @AuraEnabled
    //to get months
    public static List<String> selectedMonth(){
        Map<String,Decimal> mapOfMonth = new Map<String,Decimal>{'Jan' => 1, 'Feb' => 2, 'Mar' => 3, 'Apr' => 4, 'May' => 5, 
                                                                 'Jun' => 6, 'Jul' => 7, 'Aug' => 8, 'Sep' => 9, 'Oct' => 10, 
            													 'Nov' => 11, 'Dec' => 12};
        List<String> mapKeyList = new List<String>();
        mapKeyList.addAll(mapOfMonth.KeySet());
        return mapKeyList;    
    }

	@AuraEnabled
    //to get account regions
    public static List<String> accountRegions(){
        List<String> options = new List<String>();
        Schema.DescribeFieldResult fieldResult = Account.Sales_Area_Region__c.getDescribe();
		List<Schema.PicklistEntry> ple = fieldResult.getPicklistValues();
		for( Schema.PicklistEntry pickListVal : ple){
			options.add(pickListVal.getLabel());
		}     
        return options;
    }
    
    //to get master sales office
    @AuraEnabled
    public static List<String> salesOfficeNames(){
        List<Sales_Office__c> salesOfficeList = [SELECT id,Name FROM Sales_Office__c LIMIT 10000];
        List<String> salesOfficeNewList = new List<String>();
        salesOfficeNewList.add('None');
        for(Sales_Office__c so : salesOfficeList){
            if(so.Name != null)
                salesOfficeNewList.add(so.Name);
        }
        return salesOfficeNewList; 
    }
/*    
    //to get all related accounts with region and sales office and month and year
    @AuraEnabled
    public static List<String> getssalesOfficeAccounts(String month, String year, String region, String salesOffice){
        Map<String,Decimal> mapOfMonth = new Map<String,Decimal>{'Jan' => 1, 'Feb' => 2, 'Mar' => 3, 'Apr' => 4, 'May' => 5, 
                                                                 'Jun' => 6, 'Jul' => 7, 'Aug' => 8, 'Sep' => 9, 'Oct' => 10, 
            													 'Nov' => 11, 'Dec' => 12};
        String convertMonthToNumber = String.ValueOf(mapOfMonth.get(month));
        System.debug('convertMonthToNumber--> '+convertMonthToNumber );
        List<String> accountNames = new List<String>();
        List<Account> accList = [SELECT id, Name, Sales_Area_Region__c,Sales_Office_ERP__c FROM Account WHERE 
                                 Sales_Area_Region__c =:region AND Sales_Office_ERP__c =:salesOffice
                                 LIMIT 10000];
        System.debug('accList --> '+accList +' --> sizze'+accList.size());                         			  
        for(Account acc : accList){
        	accountNames.add(acc.Name);    
        }
        return accountNames;
    }
*/    
}

/*public class Account_NOD_OSBalance {
 	@AuraEnabled 
    public static List<NODs__c> nod(Account acc){
        List<NODs__c> nodList = [SELECT id, Account__c, Account_Wise_NOD__c, Sales_Area_Wise_NOD__c, Average_Outstanding__c, SAP_Code__c,	
                                 Posting_Date__c FROM NODs__c WHERE Account__c =:acc.id ORDER BY CreatedDate DESC LIMIT 1];
        System.debug('nodList --> '+nodList);
        return nodList;
    }  
    
    @AuraEnabled 
    public static List<O_S_Balance__c> osBalance(Account acc){
        List<O_S_Balance__c> osBalanceList = [SELECT id, Name, Account__c, Opening_Balance__c, Closing_balance__c, Transaction_Date__c
                                              FROM O_S_Balance__c WHERE Account__c =:acc.id ORDER BY CreatedDate DESC LIMIT 1];        
    	System.debug('osBalanceList --> '+osBalanceList);
        return osBalanceList;
    }   
}
*/
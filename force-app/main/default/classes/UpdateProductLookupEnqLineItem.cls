public class UpdateProductLookupEnqLineItem{

  @InvocableMethod(label='UpdateProductLookupEnqLineItem' description='update the perticular product record lookup field to Enquiry Line Item')
  public static void UpdateLookupValue(List<Id> enqLineIds)
    {
       map<string,string> enqLineWithProductMap = new map<string,string>();  
       set<string> presentProductCodesSet = new set<String>();
       list<Enquiry_Line_Item__c> NewList = new list<Enquiry_Line_Item__c>(); 
       list<Enquiry_Line_Item__c> elitoupdate = new list<Enquiry_Line_Item__c>(); 
         list<Enquiry_Line_Item__c> elitoupdate2 = new list<Enquiry_Line_Item__c>(); 
       list<Enquiry_Line_Item__c> FinalListToUpdate = new list<Enquiry_Line_Item__c>();
               map<string,string> productwithCatMap = new map<string,string>();


       for(Enquiry_Line_Item__c ptp:[select id,name,Glass_Direct_Line_Item__c,Product__c,Product_Item_Code__c,recently_updated_Invoice_rate__c,Enquiry__r.Bill_to_customer__c from Enquiry_Line_Item__c where id IN: enqLineIds])
       {
         if(ptp.Product_Item_Code__c != null)
          {
            presentProductCodesSet.add(ptp.Product_Item_Code__c);      
            NewList.add(ptp);
            system.debug('The ptp.id is: ' +ptp.id);
          }  
           
       }
       system.debug('The NewList is: ' +NewList+'......'+presentProductCodesSet);
       if(presentProductCodesSet.size()>0)
       {
          for(product2 pd:[select id,ProductCode,External_Id__c,Product_Category__c  from product2 where External_Id__c in:presentProductCodesSet])
           {
            enqLineWithProductMap.put(pd.External_Id__c,pd.id);
               productwithCatMap.put(pd.Id,pd.Product_Category__c);
               
           }
       system.debug('The enqLineWithProductMap is: ' +enqLineWithProductMap);
        for(Enquiry_Line_Item__c ptp:NewList)
        {
        ptp.product__c = enqLineWithProductMap.get(ptp.Product_Item_Code__c);
        system.debug('The  ptp.product__c is: ' + ptp.Product_Item_Code__c+'....'+ptp.product__c );
        FinalListToUpdate.add(ptp);
        }
       }
       
      
        //  update FinalListToUpdate;
        map<string,string> productwithCustomeridMap = new map<string,string>();
      // Update the Last invoice rate fields for glass reach line item record.  
      set<string> CustomerWithProductStr = new set<string>();
      map<string,string> CustomerWithProductStrMap = new map<string,string>();
       for(Enquiry_Line_Item__c  eli:FinalListToUpdate)
       {
         if(eli.Glass_Direct_Line_Item__c == true)
         {
           // get the product id and custom id here. 
           productwithCustomeridMap.put(eli.product__c,eli.Enquiry__r.Bill_to_customer__c);
           CustomerWithProductStr.add(eli.product__c+'-'+eli.Enquiry__r.Bill_to_customer__c);
           CustomerWithProductStrMap.put(eli.product__c+'-'+eli.Enquiry__r.Bill_to_customer__c,eli.id);
         }
       }
       
       
       
       list<invoice__c> invList = new list<invoice__c>();
       if(productwithCustomeridMap.keyset().size()>0)
       {
          invList = [select id,name,Invoice_Rate__c,Customer__c,Product__c,product__r.Product_Category__c from invoice__c where  product__c in:productwithCustomeridMap.keyset() AND Customer__c in:productwithCustomeridMap.Values() AND Billing_Date__c >= LAST_N_MONTHS:1 AND Invoice_Rate__c!= null order by Billing_Date__c desc limit 1];
       
       }
       
       
       map<string,invoice__c> InvoiceFoundMap=  new map<string,invoice__c>();
      
       
       for(Enquiry_Line_Item__c  eli:FinalListToUpdate)
       {
	    if(invList.size()>0)
       {
         if(eli.Glass_Direct_Line_Item__c == true)
         {
          for(invoice__c inv:invList)
          {
              if(inv.customer__c == eli.Enquiry__r.Bill_to_customer__c && inv.product__c == eli.product__c)
              {
                 eli.recently_updated_Invoice_rate__c = inv.Invoice_Rate__c;
                
              }  
          }
        }
       }
	    elitoupdate.add(eli);
       }
       
        
      if(elitoupdate.size()>0)
      {
          for(Enquiry_Line_Item__c eli:elitoupdate)
          {
              system.debug('the eli.product__r.Product_Category__c is :'+productwithCatMap.get(eli.Product__c));
              if(productwithCatMap.get(eli.Product__c) !='INSPIRE' && productwithCatMap.get(eli.Product__c) !='PARSOL'  && productwithCatMap.get(eli.Product__c) !='CLEAR')
              {
                  eli.recently_updated_Invoice_rate__c = NULL;

              }
              elitoupdate2.add(eli);
          }
      }
        if(elitoupdate2.size()>0)
         update elitoupdate2;
       
       
       
      
       
    }
}
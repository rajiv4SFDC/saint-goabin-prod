public class RequestItemTriggerHandler extends TriggerHandler{
    
    public RequestItemTriggerHandler() {}
    
    public override void beforeInsert() {

        new CatalogueBudgetManager().checkForCatalogueRequestForPopulateBudget( (List<Request_Item__c>)Trigger.new );
    }
    
    public override void beforeUpdate() {
    	
    	 new CatalogueBudgetManager().checkForCatalogueRequestForPopulateBudget( (List<Request_Item__c>)Trigger.new );
    }
    
    public override void afterUpdate() {
    	
    	 new CatalogueBudgetManager().checkForCatalogueRequestForCalcBudgetComsume( (List<Request_Item__c>)Trigger.new );
    }

}
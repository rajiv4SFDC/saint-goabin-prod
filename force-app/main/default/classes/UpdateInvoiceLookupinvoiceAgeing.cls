/* Created By         : Rajiv Kumar Singh
   Created Date       : 06/07/2018
   Class Name         : UpdateInvoiceLookupinvoiceAgeing
   Description        : update the perticular Invouce record lookup field on invoice ageing
   Last Modified By   : Rajiv Kumar Singh
   Last Modified Date : 06/07/2018
*/
public class UpdateInvoiceLookupinvoiceAgeing{

  @InvocableMethod(label='UpdateInvoiceLookupinvoiceAgeing' description='update the perticular Invouce record lookup field on invoice ageing')
  public static void UpdateLookupValue(List<Id> invsIds)
    {
       
       
       map<string,string> INVWithProductMap = new map<string,string>();  
       set<string> presentProductCodesSet = new set<String>();
       list<Invoice_Wise_Ageing__c> NewList = new list<Invoice_Wise_Ageing__c>(); 
       list<Invoice_Wise_Ageing__c> FinalListToUpdate = new list<Invoice_Wise_Ageing__c>();

       for(Invoice_Wise_Ageing__c ptp:[select id,Invoice_No__c,Invoice__c from Invoice_Wise_Ageing__c where id IN: invsIds])
       {
         if(ptp.Invoice_No__c != null)
          {
            presentProductCodesSet.add(ptp.Invoice_No__c);      
            NewList.add(ptp);
            system.debug('The ptp.id is: ' +ptp.id);
          }  
           
       }
       system.debug('The NewList is: ' +NewList+'......'+presentProductCodesSet);
       if(presentProductCodesSet.size()>0)
       {
          for(Invoice__c pd:[select id,External_Id__c  from Invoice__c where External_Id__c in:presentProductCodesSet])
           {
            INVWithProductMap.put(pd.External_Id__c,pd.id);
           }
       system.debug('The INVWithProductMap is: ' +INVWithProductMap);
        for(Invoice_Wise_Ageing__c ptp:NewList)
        {
        ptp.Invoice__c  = INVWithProductMap.get(ptp.Invoice_No__c);
        system.debug('The  ptp.product__c is: ' + ptp.Invoice_No__c);
        FinalListToUpdate.add(ptp);
        }
       }
       
          update FinalListToUpdate;
       
    }
}
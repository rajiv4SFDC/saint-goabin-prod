/* Created By: Rajiv Kumar Singh
Created Date: 06/11/2018
Class Name: EnquiryStatusUpdateUpsertAPI 
Story : Enquiry status flow from SAP to SFDC
Last Modified By :Rajiv Kumar Singh
Last Modified Date: 06/11/2018
*/

@RestResource(urlMapping='/EnquiryStatusUpdate')
global class EnquiryStatusUpdateUpsertAPI {
    @HttpPost
    global static void doHandleInsertRequest(){
        
        List<EnquiryWrapper> wrapList = new List<EnquiryWrapper>();
        List<Enquiry_Status__c> LedgerList = new List<Enquiry_Status__c>();
        RestRequest Req = RestContext.request;
        RestResponse res = RestContext.response;
        String body = Req.requestBody.toString();
        if(body != null && body != '') {
            
            try {
                wrapList = (List<EnquiryWrapper>)JSON.deserialize(body, List<EnquiryWrapper>.class);
                
                for(EnquiryWrapper we :wrapList){
                    
                    Enquiry_Status__c cusLed = new Enquiry_Status__c();
                    if(we.EnqReferencenum!= null && we.EnqReferencenum!=''){
                        cusLed.Enquiry_Reference__c = we.EnqReferencenum;
                    }
                  
                    if(we.EnqLineReferencenum!= null && we.EnqLineReferencenum!='' ){
                        cusLed.Enquiry_Line_Reference__c = we.EnqLineReferencenum;
                    }
                    
                    if(we.ConfirmedQty!= null ){
                        cusLed.Confirmed_Qty__c = we.ConfirmedQty;
                    }
                    if(we.OrderQuantity!= null){
                        cusLed.Order_Quantity__c = we.OrderQuantity;
                    }
                    if(we.QtyinShipment!= null){
                        cusLed.Qty_in_Shipment__c = we.QtyinShipment;
                    }
                    if(we.ShortClosedQty!= null){
                        cusLed.Short_Closed_Qty__c = we.ShortClosedQty;
                    }
                    if(we.ServicedQty!= null){
                        cusLed.Serviced_Qty__c = we.ServicedQty;
                    }
                    if(we.status!= null && we.status!=''){
                        cusLed.Status__c = we.status;
                    }
                    if(we.EnqReferencenum!= null && we.EnqReferencenum!='' && we.EnqLineReferencenum!= null && we.EnqLineReferencenum!=''){
                        cusLed.External_Id__c = we.EnqReferencenum+'-'+we.EnqLineReferencenum;
                    }
               
                    LedgerList.add(cusLed);
                    
                }
                
                Schema.SObjectField ftoken = Enquiry_Status__c.Fields.External_Id__c;
                Database.UpsertResult[] srList = Database.upsert(LedgerList,ftoken,false); 
                res.statusCode = 200;
                res.responseBody = Blob.valueOf(String.valueof(LedgerList));
                
                
            } catch (Exception e) {
                
                system.debug('error'+e.getMessage());
                CommonUtilitiesHandler.handleExceptions(e);
                res.statusCode = 500;
                res.responseBody = Blob.valueOf(e.getMessage());
            }
        } else {
            res.statusCode = 500;
            res.responseBody = Blob.valueOf('Invalid Request');
        }   
    }
    
    public class EnquiryWrapper{
        public String EnqReferencenum{get;set;}
        public String EnqLineReferencenum{get;set;}
        public String status{get;set;}
        public Decimal ConfirmedQty{get;set;}
        public Decimal OrderQuantity{get;set;}
        public Decimal QtyinShipment{get;set;}
        public Decimal ServicedQty{get;set;}
        public Decimal ShortClosedQty{get;set;}
             
    }
}
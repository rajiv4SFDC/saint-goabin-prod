/* Created By         : Rajiv Kumar Singh
   Created Date       : 12/06/2018
   Class Name         : UpdateLookupFromShipToEnquiry_Test
   Description        : 
   Last Modified By   : Rajiv Kumar Singh
   Last Modified Date : 12/06/2018
*/
@isTest
private class UpdateLookupFromShipToEnquiry_Test
{ 
 // @istest
   private testmethod static void testMethod1()
   {
      Ship_To_Customer__c p1 = new Ship_To_Customer__c();
      p1.Name = 'testing';
      p1.Address__c ='East';
      p1.Customer_Code__c = '6565666';
      insert p1;
      
      Enquiry__c ptp = new Enquiry__c();
      ptp.SAP_Code__c = '6565666';
      ptp.Ship_To_SAP_Code__c = '6565666';
      ptp.Ship_To_Customer__c = p1.id;
      insert ptp;
      
      Enquiry__c  pt = [select id, SAP_Code__c from Enquiry__c  where id =:ptp.id];
      
    //  System.assertEquals(pt.product__c,p1.id);
      list<Id> ptrList = new list<Id>();
      ptrList.add(pt.id);
      
      UpdateLookupFromShipToEnquiry.UpdateLookupValue(ptrList);
   }
}
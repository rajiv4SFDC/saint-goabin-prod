/* Created By         : Rajiv Kumar Singh
   Created Date       : 11/06/2018
   Class Name         : UpdateLookupOfShipToCustomer_Test
   Description        : 
   Last Modified By   : Rajiv Kumar Singh
   Last Modified Date : 11/06/2018
*/
@isTest
private class UpdateLookupOfShipToCustomer_Test
{ 
 // @istest
   private testmethod static void testMethod1()
   {
      Account p1 = new Account();
      p1.Name = 'testing';
      p1.Sales_Area_Region__c ='East';
      p1.SAP_Code__c = '6565666';
      insert p1;
      
      Ship_To_Customer__c ptp = new Ship_To_Customer__c();
      ptp.Bill_To_Customer_Code__c = '6565666';
      ptp.Name = 'Ajeet';
      ptp.Address__c = 'Banglore';
      ptp.Account__c = p1.id;
      insert ptp;
      
      Ship_To_Customer__c  pt = [select id, Bill_To_Customer_Code__c from Ship_To_Customer__c  where id =:ptp.id];
      
    //  System.assertEquals(pt.product__c,p1.id);
      list<Id> ptrList = new list<Id>();
      ptrList.add(pt.id);
      
      UpdateLookupOfShipToCustomer.UpdateLookupValue(ptrList);
   }
}
/* Created By         : Rajiv Kumar Singh
   Created Date       : 11/06/2018
   Class Name         : UpdateLookupOfCustomerWiseAgeing_Test
   Description        : 
   Last Modified By   : Rajiv Kumar Singh
   Last Modified Date : 11/06/2018
*/
@isTest
private class UpdateLookupOfCustomerWiseAgeing_Test
{ 
 // @istest
   private testmethod static void testMethod1()
   {
      Account p1 = new Account();
      p1.Name = 'testing';
      p1.Sales_Area_Region__c ='East';
      p1.SAP_Code__c = '6565666';
      insert p1;
      
      Customer_Wise_Ageing__c ptp = new Customer_Wise_Ageing__c();
      ptp.SAP_Code__c = '6565666';
      ptp.Account__c = p1.id;
      insert ptp;
      
      Customer_Wise_Ageing__c  pt = [select id, SAP_Code__c from Customer_Wise_Ageing__c  where id =:ptp.id];
      
    //  System.assertEquals(pt.product__c,p1.id);
      list<Id> ptrList = new list<Id>();
      ptrList.add(pt.id);
      
      UpdateLookupOfCustomer_Wise_Ageing.UpdateLookupValue(ptrList);
   }
}
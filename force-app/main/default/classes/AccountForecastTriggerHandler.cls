Public class AccountForecastTriggerHandler{

    public static void handleaccountforecast(List<Account_Forecast__c> AccforList){
    
        Set<id>  accid = new Set<id>();
        Set<id>  Proid = new Set<id>();
        Set<Decimal> yearList = new Set<Decimal>();
        
        for(Account_Forecast__c af : AccforList){
            accid.add(af.Account__c);
            proid.add(af.Product__c);
            yearList.add(af.Year__c);
        }
     
         if(accid.size() > 0 && proid.size() > 0 ){
         
         List<Account_Forecast__c> lstAccForcast = [select name,id,Account__c,Product__c,Account_Product__c,year__c from Account_Forecast__c where Account__c in :accid AND Product__c IN :proid AND year__c IN: yearList];
         
          Map<string,Account_Forecast__c> mapNameWiseAccount = new Map<string,Account_Forecast__c>();
          
          For(Account_Forecast__c acc: lstAccForcast)
            {
                mapNameWiseAccount.put(acc.Account_Product__c,acc);
            }
            
          For(Account_Forecast__c acc : AccforList)
            {
                if(mapNameWiseAccount.containsKey(acc.Account_Product__c))
                {
                    acc.Account_Product__c.addError('Forecast record already exists with same Product Category and Customer');
                }
            }
          
          
         }
    
    }
}
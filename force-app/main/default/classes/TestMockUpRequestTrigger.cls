@isTest
public class TestMockUpRequestTrigger {
    
    @testSetup 
    public static void createTestData(){
        TestDataFactory.createCustomSetting();
        TestDataFactory.createTestProductData();
        TestDataFactory.createTestStateAndCityRecordData();
        TestDataFactory.createTestCatalogueBudgetData();
        TestDataFactory.createTestCampaignRecordData();
        TestDataFactory.createTestAccountData();
        TestDataFactory.createTestProjectData();
        TestDataFactory.createTestOpportunityData();
        TestDataFactory.createTestOpportunityProductData();
        TestDataFactory.createTestPriceRequestData();
        TestDataFactory.createTestProductForecateData();
        TestDataFactory.createTestForecateInvoiceData();
        TestDataFactory.createTestMockupRecordData();
        TestDataFactory.createTestPlantVisitRecordData();
        TestDataFactory.createTestInvoiceRecordData();
        TestDataFactory.createTestTargetActualRecordData();
    }
    
    @isTest
    public static void createMockUpRequest(){
        List<Opportunity> lstOfOppty = [SELECT id FROM Opportunity limit 2];
        List<Mock_Up_Request__c> lsTofMockup = new List<Mock_Up_Request__c>();
        
        for(Opportunity oppty  : lstOfOppty){
            
            Mock_Up_Request__c mock = new Mock_Up_Request__c();
            mock.Opportunity__c = oppty.id;
            mock.Approval_Status__c = 'Draft';
            
            lsTofMockup.add(mock);
        }
        
        insert lsTofMockup;       
    }
}
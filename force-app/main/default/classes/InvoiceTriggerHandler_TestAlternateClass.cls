@isTest
public class InvoiceTriggerHandler_TestAlternateClass{
    static testMethod void testMet(){
        /*InvoiceTriggerHandler.stoprecur = false;
        
        Profile pf = [SELECT Id FROM Profile WHERE Name='Standard User'];
        
        User u = new User(Alias = 'standt', Email='standarduser@testorg.com',
                          EmailEncodingKey='UTF-8', LastName='Testing', LanguageLocaleKey='en_US',
                          LocaleSidKey='en_US', ProfileId = pf.Id,phone='456786543',
                          TimeZoneSidKey='America/Los_Angeles',UserName='abcd@abce_test.com',Sales_Office__c='DEL1');
        insert u;
        
        set<id> accountids = new set<id>();
        Account acc = new Account();
        acc.Name = 'KVP';
        acc.Sales_Office_ERP__c='DEL1';
        acc.Mirror_Tier__c='test';
        acc.SAP_Code__c='45352';
        insert acc;
        accountids.add(acc.id);
        system.assertEquals('KVP', acc.Name);
        
        account acc1 = new account();
        acc1.Name = 'KVP test';
        acc1.Sales_Office_ERP__c='BLR1';
        acc1.Mirror_Tier__c='tes Test';
        acc1.SAP_Code__c='43352';
        insert acc1;
        accountids.add(acc1.id);
        
        Product_Category__c pcc = new Product_Category__c(name='test',External_Id__c= 'qwer');
        insert pcc;
        
        product2 p = new product2();
        p.Name='test';
        p.ProductCode='54543';
        p.Thickness__c='324568';
        p.Description='test';
        p.Product_Category__c='infinity';
        p.Sub_Family__c='FLT Clear';
        p.Coating__c='silver';
        p.Tint__c='Clear';
        p.External_Id__c='1232432323';
        p.Product_Master_Category__c=pcc.id;
        insert p;
        
        List<Product_Tier_Pricing__c> ptList =new List<Product_Tier_Pricing__c>();
        for(integer i=0; i<=2; i++){
            Product_Tier_Pricing__c pt = new Product_Tier_Pricing__c(
                name='test tier product',
                Product__c=p.id,
                Item_Code__c='1232432323',
                Business_Location__c='DEL1',
                Tier_Name__c='test'
            );
            ptList.add(pt);
            system.assertEquals(p.id, pt.Product__c);
        }
        insert ptList;
        
        List<Product_Tier_Pricing__c> ptp = [select id,name,List_Tier_price__c,Tier_Pricing__c,Regional_head_approval_limit__c,Business_Location__c,Product_Code__c,Item_Code__c,Tier_Name__c from Product_Tier_Pricing__c where Item_Code__c=:p.External_Id__c AND Business_Location__c=:acc.Sales_Office_ERP__c AND Tier_Name__c=:acc.Mirror_Tier__c];
    
        */
        Account acc = new Account();
        acc.Name = 'abcd';
        acc.SAP_Code__c	= '12345';
        acc.Sales_Office_ERP__c = 'DEL1';
        acc.MDCName__c = 'DEL10001';
        acc.Sales_Area_Region__c = 'North';
        insert acc;
        
        Account acc1 = new Account();
        acc1.Name = 'abcde';
        acc1.SAP_Code__c	= '123456';
        acc1.Sales_Office_ERP__c = 'DEL1';
        acc1.MDCName__c = 'DEL10001';
        acc1.Sales_Area_Region__c = 'North';
        insert acc1;
        
        Product_Category__c pc = new Product_Category__c();
        pc.Name = 'prodCat';
        pc.External_Id__c = 'qwerty';
        insert pc;
        
        Product2 p1 = new Product2();
        p1.Name = 'product1';
        p1.ProductCode = '405-049-153-000-952';
        p1.Thickness_Code__c = '0400';
        p1.Product_Category__c = 'Clear';
        p1.Product_Master_Category__c = pc.id;
        insert p1;
        
        Schemes__c sch = new Schemes__c();
        sch.Account__c = acc.id;
        sch.Product__c = p1.id;
        sch.Not_To_Be_Considered__c = false;
        sch.Start_Date__c = system.today() - 1;
        sch.End_date__c = system.today() + 1;
        insert sch;
        
        Schemes__c sch1 = new Schemes__c();
        sch1.Account__c = acc.id;
        sch1.Not_To_Be_Considered__c = false;
        sch1.Start_Date__c = system.today() - 1;
        sch1.End_date__c = system.today() + 1;
        insert sch1;
        
        Schemes__c sch11 = new Schemes__c();
        sch11.Account__c = acc1.id;
        sch.Product__c = p1.id;
        sch11.Not_To_Be_Considered__c = false;
        sch11.Start_Date__c = system.today() - 1;
        sch11.End_date__c = system.today() + 1;
        insert sch11;
        
        Schemes__c sch2 = new Schemes__c();
        sch2.Product__c = p1.id;
        sch2.Not_To_Be_Considered__c = false;
        sch2.Start_Date__c = system.today() - 1;
        sch2.End_date__c = system.today() + 1;
        insert sch2;
        
        Id schemeInternalRecTypeId = Schema.SObjectType.Schemes__c.getRecordTypeInfosByName().get('Internal').getRecordTypeId();
        Schemes__c sch3 = new Schemes__c();
        sch3.Product__c = p1.id;
        sch3.Account__c = acc.id;
        sch3.RecordTypeId = schemeInternalRecTypeId;
        sch3.Not_To_Be_Considered__c = false;
        sch3.Start_Date__c = system.today() - 1;
        sch3.End_date__c = system.today() + 1;
        insert sch3;
        
        Schemes__c sch4 = new Schemes__c();
        //sch4.Product__c = p1.id;
        sch4.Account__c = acc.id;
        sch4.RecordTypeId = schemeInternalRecTypeId;
        sch4.Not_To_Be_Considered__c = false;
        sch4.Start_Date__c = system.today() - 1;
        sch4.End_date__c = system.today() + 1;
        insert sch4;
        
        Schemes__c sch5 = new Schemes__c();
        sch5.Product__c = p1.id;
        //sch5.Account__c = acc.id;
        sch5.RecordTypeId = schemeInternalRecTypeId;
        sch5.Not_To_Be_Considered__c = false;
        sch5.Start_Date__c = system.today() - 1;
        sch5.End_date__c = system.today() + 1;
        insert sch5;
        
        List<Profile> profileList = [SELECT Id,Name FROM Profile];
        Id AM, ORS, RM, SH, NH;
        for(Profile p : profileList){
            if(String.ValueOf(p.Name).contains('AM'))
                AM = p.id;
            if(String.ValueOf(p.Name).contains('ORS'))
                ORS = p.id;
            if(String.ValueOf(p.Name).contains('RM'))
                RM = p.id;
            if(String.ValueOf(p.Name).contains('SH'))
                SH = p.id;
            if(String.ValueOf(p.Name).contains('NH'))
                NH = p.id;
        }
		
                
        User usr = new User(LastName = 'LIVESTON',
                           FirstName='JASON',
                           Alias = 'jliv',
                           Sales_Office__c = 'DEL1', 
                           Email = 'jason.liveston@asdf.com',
                           Username = 'jason133.liveston@asdf.com',
                           ProfileId = AM,
                           TimeZoneSidKey = 'GMT',
                           LanguageLocaleKey = 'en_US',
                           EmailEncodingKey = 'UTF-8',
                           LocaleSidKey = 'en_US',
                           MobilePhone = '1234567890',
                           phone = '1234567890' 
                           );
        insert usr;
        
         User usr1 = new User(LastName = 'LIVESTON',
                           FirstName='JASON',
                           Alias = 'jliv',
                           Sales_Office__c = 'DEL1', 
                           Email = 'jason.liveston@asdf.com',
                           Username = 'jason1334.liveston@asdf.com',
                           ProfileId = ORS,
                           TimeZoneSidKey = 'GMT',
                           LanguageLocaleKey = 'en_US',
                           EmailEncodingKey = 'UTF-8',
                           LocaleSidKey = 'en_US',
                           MobilePhone = '1234567890',
                           phone = '1234567890' ,
                           Territory__c = 'DEL10001' 
                           );
        insert usr1;
        
        User usr2 = new User(LastName = 'LIVESTON',
                           FirstName='JASON',
                           Alias = 'jliv',
                           Sales_Office__c = 'DEL1', 
                           Email = 'jason.liveston@asdf.com',
                           Username = 'jason13344.liveston@asdf.com',
                           ProfileId = RM,
                           TimeZoneSidKey = 'GMT',
                           LanguageLocaleKey = 'en_US',
                           EmailEncodingKey = 'UTF-8',
                           LocaleSidKey = 'en_US',
                           MobilePhone = '1234567890',
                           phone = '1234567890',
                           Region__c = 'North'  
                           );
        insert usr2;
        
        User usr3 = new User(LastName = 'LIVESTON',
                           FirstName='JASON',
                           Alias = 'jliv',
                           Sales_Office__c = 'DEL1', 
                           Email = 'jason.liveston@asdf.com',
                           Username = 'jason1334455.liveston@asdf.com',
                           ProfileId = SH,
                           TimeZoneSidKey = 'GMT',
                           LanguageLocaleKey = 'en_US',
                           EmailEncodingKey = 'UTF-8',
                           LocaleSidKey = 'en_US',
                           MobilePhone = '1234567890',
                           phone = '1234567890' 
                           );
        insert usr3;
        
        User usr4 = new User(LastName = 'LIVESTON',
                           FirstName='JASON',
                           Alias = 'jliv',
                           Sales_Office__c = 'DEL1', 
                           Email = 'jason.liveston@asdf.com',
                           Username = 'jason133445566.liveston@asdf.com',
                           ProfileId = NH,
                           TimeZoneSidKey = 'GMT',
                           LanguageLocaleKey = 'en_US',
                           EmailEncodingKey = 'UTF-8',
                           LocaleSidKey = 'en_US',
                           MobilePhone = '1234567890',
                           phone = '1234567890' 
                           );
        insert usr4;
        
        
        
        Schemes__c sch6 = new Schemes__c();
        //sch6.Product__c = p1.id;
        //sch6.Account__c = acc.id;
        sch6.User__c = usr.id;
        sch6.RecordTypeId = schemeInternalRecTypeId;
        sch6.Not_To_Be_Considered__c = false;
        sch6.Start_Date__c = system.today() - 1;
        sch6.End_date__c = system.today() + 1;
        insert sch6;
        
        Schemes__c sch7 = new Schemes__c();
        //sch7.Product__c = p1.id;
        //sch7.Account__c = acc.id;
        sch7.User__c = usr1.id;
        sch7.RecordTypeId = schemeInternalRecTypeId;
        sch7.Not_To_Be_Considered__c = false;
        sch7.Start_Date__c = system.today() - 1;
        sch7.End_date__c = system.today() + 1;
        insert sch7;
        
        Schemes__c sch8 = new Schemes__c();
        //sch8.Product__c = p1.id;
        //sch8.Account__c = acc.id;
        sch8.User__c = usr2.id;
        sch8.RecordTypeId = schemeInternalRecTypeId;
        sch8.Not_To_Be_Considered__c = false;
        sch8.Start_Date__c = system.today() - 1;
        sch8.End_date__c = system.today() + 1;
        insert sch8;
        
        Schemes__c sch9 = new Schemes__c();
        //sch7.Product__c = p1.id;
        //sch7.Account__c = acc.id;
        sch9.User__c = usr3.id;
        sch9.RecordTypeId = schemeInternalRecTypeId;
        sch9.Not_To_Be_Considered__c = false;
        sch9.Start_Date__c = system.today() - 1;
        sch9.End_date__c = system.today() + 1;
        insert sch9;
        
        Schemes__c sch10 = new Schemes__c();
        //sch7.Product__c = p1.id;
        //sch7.Account__c = acc.id;
        sch10.User__c = usr4.id;
        sch10.RecordTypeId = schemeInternalRecTypeId;
        sch10.Not_To_Be_Considered__c = false;
        sch10.Start_Date__c = system.today() - 1;
        sch10.End_date__c = system.today() + 1;
        insert sch10;
        
        Account_Forecast__c af = new Account_Forecast__c();
        af.Account__c = acc.id;
        af.Product_Category__c = 'prodCat';
        af.Volume_in_tons_expected__c = 123;
        af.Year__c = 2018;
        insert af;
        
        Monthly_Plan__c mp  = new Monthly_Plan__c();
        mp.Account__c = acc.id;
        mp.Account_Forecast__c = af.id;
        mp.Year__c = 2018;
        mp.Month__c = '12';
        insert mp;
        
        List<Invoice__c> invoiceListDup = new List<Invoice__c>();
        List<Invoice__c> invoiceListDupUpdate = new List<Invoice__c>();
        
        Invoice__c inv = new Invoice__c();
        inv.Customer__c = acc.id;
        inv.Product__c = p1.id;
        inv.Item_Tonnage__c = '100';
        inv.Product_Code__c = '405-049-153-000-952';
        inv.Thickness__c = '0400';
        inv.Status__c = 'Invoiced';
        inv.Billing_Date__c = system.today();
        invoiceListDup.add(inv);
        
        Invoice__c inv1 = new Invoice__c();
        inv1.Customer__c = acc.id;
        inv1.Product__c = p1.id;
        inv1.Item_Tonnage__c = '100';
        inv1.Product_Code__c = '405-049-153-000-952';
        inv1.Thickness__c = '0400';
        inv1.Status__c = 'Invoiced';
        inv1.Billing_Date__c = system.today();
        invoiceListDup.add(inv1);
        
        Invoice__c inv2 = new Invoice__c();
        inv2.Customer__c = acc.id;
        inv2.Product__c = p1.id;
        inv2.Item_Tonnage__c = '100';
        inv2.Product_Code__c = '405-049-153-000-952';
        inv2.Thickness__c = '0400';
        inv2.Status__c = 'Invoiced';
        inv2.Billing_Date__c = system.today();
        invoiceListDup.add(inv2);
        
        insert invoiceListDup;
        Map<id,Invoice__c> mapOfInvoiceTemp = new Map<id,Invoice__c>();
        mapOfInvoiceTemp.putAll(invoiceListDup);
        
        InvoiceBatchHandlerForInsert invBatch = new InvoiceBatchHandlerForInsert(invoiceListDup);
        Database.QueryLocator q1 = invBatch.start(null);
        invBatch.execute(null,invoiceListDup);
		invBatch.Finish(null);
        //ID batchprocessid = Database.executeBatch(invBatch);
        
        
        inv.Customer__c = acc1.id;
        invoiceListDupUpdate.add(inv);
        
        inv1.Customer__c = null;
        invoiceListDupUpdate.add(inv1);
        
        inv2.Item_Tonnage__c = '10';
        invoiceListDupUpdate.add(inv2);
        
        update invoiceListDupUpdate;
        
        
        InvoiceBatchHandlerForUpdate invBatch1 = new InvoiceBatchHandlerForUpdate(invoiceListDupUpdate, mapOfInvoiceTemp);
        Database.QueryLocator q2 = invBatch1.start(null);
        invBatch1.execute(null,invoiceListDupUpdate);
		invBatch1.Finish(null);
        //ID batchprocessid1 = Database.executeBatch(invBatch1);
        
        /*TestQueueableApexForInsertion apc = new TestQueueableApexForInsertion(invoiceListDup);
        Test.startTest();
        	System.enqueueJob(apc);
        Test.stopTest();*/
        
        //TestQueueableApexForInsertion.coverCode();
        //TestQueueClassForInvoiceUpdate.coverClass();
        
    }
}
/* Created By: Rajiv Kumar Singh
Created Date: 27/06/2018
Class Name: RegionWiseNODInsertAPI 
Story : Region Wise NOD flow from SAP to SFDC
Last Modified By :Rajiv Kumar Singh
Last Modified Date: 27/06/2018
*/

@RestResource(urlMapping='/RegionWiseNOD')
global class RegionWiseNODInsertAPI{
    @HttpPost
    global static void doHandleInsertRequest(){
        
        List<RegionWiseNODWrapper> wrapList = new List<RegionWiseNODWrapper>();
        List<Region_Wise_NOD_s__c> NODList = new List<Region_Wise_NOD_s__c>();
        RestRequest Req = RestContext.request;
        RestResponse res = RestContext.response;
        String body = Req.requestBody.toString().trim();
        
        if(body != null && body != '') {
            
            try {
                wrapList = (List<RegionWiseNODWrapper>)JSON.deserialize(body, List<RegionWiseNODWrapper>.class);
                
                for(RegionWiseNODWrapper we :wrapList){
                    Region_Wise_NOD_s__c RegionWiseNOD = new Region_Wise_NOD_s__c();
                    if(we.RegionName!= null && we.RegionName!=''){
                        RegionWiseNOD.Region_Name__c = we.RegionName;
                    }
                    if(we.RegionNODs!= null){
                        RegionWiseNOD.No_of_Days__c = we.RegionNODs;
                    } 
                    if(we.RegionNODs!= null){
                        RegionWiseNOD.No_of_Days__c = we.RegionNODs;
                    } 
                    if(we.AverageOutstanding!= null){
                        RegionWiseNOD.Average_Outstanding__c = we.AverageOutstanding;
                    }
                    if(we.postDate!= null){
                        RegionWiseNOD.Posting_Date__c = we.postDate;
                    }
                    if(we.Amt60!= null && we.Amt60!=''){
                        RegionWiseNOD.Amt_60_Days__c = we.Amt60;
                    } 
                    
                    NODList.add(RegionWiseNOD);
                }
                
                if(NODList.size() > 0){
                    Insert NODList;
                }
                
                res.statusCode = 200;
                res.responseBody = Blob.valueOf(String.valueof(NODList));
                
            }catch (Exception e) {
                
                CommonUtilitiesHandler.handleExceptions(e);
                res.statusCode = 500;
                res.responseBody = Blob.valueOf(e.getMessage());
            }
        } else {
            res.statusCode = 500;
            res.responseBody = Blob.valueOf('Invalid Request');
        }
    }
    
    public class RegionWiseNODWrapper{
        public String RegionName{get;set;}
        public Integer RegionNODs{get;set;}
        public Decimal AverageOutstanding{get;set;}
        public Date postDate{get;set;}
        public string Amt60{get;set;}
       
        
    }
}
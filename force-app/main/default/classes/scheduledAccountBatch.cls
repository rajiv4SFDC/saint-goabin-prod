global class scheduledAccountBatch implements Schedulable {
   global void execute(SchedulableContext sc) {
      AccountBatch b = new AccountBatch(); 
      database.executebatch(b);
   }
}
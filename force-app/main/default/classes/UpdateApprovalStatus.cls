/* Updating the Account Approval Status on Basis of Account Region */

public class UpdateApprovalStatus{
    @InvocableMethod(label='UpdateApprovalStatus' description='Update the Approval Status on the basis of Account Region')
    
    public static void UpdateApprovalValue(List<Id> accIdList){
        Map<id,String>accregionmap= new Map<id,String>();
        Map<String,User>usermap = new Map<String,User>();
        List<Account>acclist = new List<Account>();
        map<string,user> roleWithUserNameMap = new map<string,user>();
        
        
        Set<String>UserIdset= new Set<String>();
        set<string> UserRoleNamesSet = new set<string>();
        UserRoleNamesSet.add('Regional Manager North');
        UserRoleNamesSet.add('Regional Manager South');
        UserRoleNamesSet.add('Regional Manager West');
        UserRoleNamesSet.add('Regional Manager East');
        UserRoleNamesSet.add('Regional Head Sales and Marketing West');
        UserRoleNamesSet.add('Regional Head Sales and Marketing East');
        UserRoleNamesSet.add('Regional Head Sales and Marketing North');
        UserRoleNamesSet.add('Regional Head Sales and Marketing South');
        
        
        for(user u1:[select id,name,userRole.name from user where userRole.name in:UserRoleNamesSet])
        {
            roleWithUserNameMap.put(u1.userRole.name,u1);
        }
        
        
        for(Account acc:[select id,Sales_Area_Region__c,RegionalHead__c,RegionalManager__c,CreditControl__c from Account WHERE Id in : accIdList ]){
            
            if(acc.Sales_Area_Region__c.equalsIgnoreCase('East'))
              {
                acc.RegionalHead__c = roleWithUserNameMap.get('Regional Head Sales and Marketing East').Id;
                acc.RegionalManager__c = roleWithUserNameMap.get('Regional Manager East').Id;
              }
              else
               if(acc.Sales_Area_Region__c.equalsIgnoreCase('West'))
              {
                acc.RegionalHead__c = roleWithUserNameMap.get('Regional Head Sales and Marketing West').Id;
                acc.RegionalManager__c = roleWithUserNameMap.get('Regional Manager West').Id;
              }
              else
               if(acc.Sales_Area_Region__c.equalsIgnoreCase('North'))
              {
                acc.RegionalHead__c = roleWithUserNameMap.get('Regional Head Sales and Marketing North').Id;
                acc.RegionalManager__c = roleWithUserNameMap.get('Regional Manager North').Id;
              }
              else
               if(acc.Sales_Area_Region__c.equalsIgnoreCase('South'))
              {
                acc.RegionalHead__c = roleWithUserNameMap.get('Regional Head Sales and Marketing South').Id;
                acc.RegionalManager__c = roleWithUserNameMap.get('Regional Manager South').Id;
              }
              acclist.add(acc);
        }
        
        update acclist;
        
    }//end of the UpdateApprovalMethod
}//end of the class
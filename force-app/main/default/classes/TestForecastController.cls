@isTest
public class TestForecastController {


	@testSetup 
	public static void createTestData(){
		TestDataFactory.createCustomSetting();
		TestDataFactory.createTestProductData();
		TestDataFactory.createTestStateAndCityRecordData();
		TestDataFactory.createTestCatalogueBudgetData();
		TestDataFactory.createTestCampaignRecordData();
		TestDataFactory.createTestAccountData();
		TestDataFactory.createTestProjectData();
		TestDataFactory.createTestOpportunityData();
		TestDataFactory.createTestOpportunityProductData();
		TestDataFactory.createTestPriceRequestData();
		TestDataFactory.createTestProductForecateData();
		TestDataFactory.createTestForecateInvoiceData();
		TestDataFactory.createTestMockupRecordData();
		TestDataFactory.createTestInvoiceRecordData();
		TestDataFactory.createTestTargetActualRecordData();
	}

    @isTest
		public static void testOptyProdId(){

				Opportunity_Product__c opptyProd = [SELECT id,Opportunity__c FROM Opportunity_Product__c LIMIT 1];

				//Calling Apex Methods

				ForecastController.findById(opptyProd.Opportunity__c);

				ForecastController.getOptyProdCount(opptyProd.Opportunity__c);

				ForecastController.viewForecast(opptyProd.Opportunity__c);

				ForecastController.getOptyProd(opptyProd.Opportunity__c);

				ForecastController.getOptyProdYr(opptyProd.Id);

				ForecastController.getOptyProdId(opptyProd.Id);

			    ForecastController.UploadRecordsWrapper dummy1 = new ForecastController.UploadRecordsWrapper();
			    dummy1.month = 'AUG';			   
		        dummy1.year= '2017';
		        dummy1.optyprod = opptyProd.id;
		        dummy1.quantity = 600;
		        dummy1.id = 7;

		        ForecastController.UploadRecordsWrapper dummy2 = new ForecastController.UploadRecordsWrapper();
			    dummy2.month = 'SEP';			   
		        dummy2.year= '2017';
		        dummy2.optyprod = opptyProd.id;
		        dummy2.quantity = 600;
		        dummy2.id = 8;

		        List<ForecastController.UploadRecordsWrapper> lst = new List<ForecastController.UploadRecordsWrapper>();
		        lst.add(dummy1);
		        lst.add(dummy2);

		        String str = JSON.serialize(lst);   

				ForecastController.saveMonthForecasts(str);

		}

}
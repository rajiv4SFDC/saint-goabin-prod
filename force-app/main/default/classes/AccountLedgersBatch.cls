global class AccountLedgersBatch implements Database.Batchable<sObject> {
    
    Date today = System.today();
    Date todays = today.adddays(-1);
    global Database.QueryLocator start(Database.BatchableContext BC){
    Id devRecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('ERP').getRecordTypeId();    
        
        return Database.getQueryLocator([SELECT Id, (SELECT Id, Account__c, Opening_Balance__c FROM O_S_Balances__r WHERE Transaction_Date__c = :todays), (SELECT Id, Type_of_Transaction__c, Payment__c, Account__c FROM Customer_Ledgers__r WHERE Payment__c != null AND Created_Date__c = :todays) FROM Account WHERE RecordtypeId=:devRecordTypeId]);
        
    }

    global void execute(Database.BatchableContext BC, List<Account> accounts){
        
        system.debug('accounts:'+accounts);
        O_S_Balance__c osBalance;
        List<O_S_Balance__c> accountBalances = new List<O_S_Balance__c>();
        Map<Id, Decimal> accountBalance = new Map<Id, Decimal>();
        Decimal amount;
        for(Account acct: accounts) {
        	
            for(O_S_Balance__c osBal: acct.O_S_Balances__r) {
                
                accountBalance.put(osBal.Account__c, osBal.Opening_Balance__c);
            }
            
            for(Customer_Ledger__c customerLedger: acct.Customer_Ledgers__r) {
                
                amount = accountBalance.get(customerLedger.Account__c) != null ? accountBalance.get(customerLedger.Account__c) : 0;
                if(customerLedger.Type_of_Transaction__c == 'Credit') {
                    
                    amount += customerLedger.Payment__c;
                    
                } else if(customerLedger.Type_of_Transaction__c == 'Debit') {
                    
                    amount -= customerLedger.Payment__c;
                }
                accountBalance.put(customerLedger.Account__c, amount);
            }
        }
        
        for(Id acctId: accountBalance.keySet()) {
            
            osBalance = new O_S_Balance__c();
            osBalance.Account__c = acctId;
            osBalance.Closing_balance__c = accountBalance.get(acctId);
            osBalance.Transaction_Date__c = todays;
            accountBalances.add(osBalance);
            
            osBalance = new O_S_Balance__c();
            osBalance.Account__c = acctId;
            osBalance.Opening_balance__c = accountBalance.get(acctId);
            osBalance.Transaction_Date__c = todays.addDays(1);
            accountBalances.add(osBalance);
        }
        
        if(accountBalances.size() > 0) {
            
            insert accountBalances;
        }
        
        system.debug('accountBalances::'+accountBalances);
    }
    
    global void finish(Database.BatchableContext BC) {
        
    }
}
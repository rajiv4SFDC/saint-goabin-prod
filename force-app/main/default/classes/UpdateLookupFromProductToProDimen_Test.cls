/* Created By         : Rajiv Kumar Singh
   Created Date       : 12/06/2018
   Class Name         : UpdateLookupFromProductToProDimen_Test
   Description        : 
   Last Modified By   : Rajiv Kumar Singh
   Last Modified Date : 12/06/2018
*/
@isTest
private class UpdateLookupFromProductToProDimen_Test
{ 
 // @istest
   private testmethod static void testMethod1()
   {
      Product2 p1 = new Product2();
      p1.Name = 'testing';
      p1.Description ='East';
      p1.Product_Category__c = 'Infinity';
	  p1.Sub_Family__c = 'EKO';
      p1.Coating__c ='PLT';
      p1.Tint__c = 'SAFE';
	  p1.ExternalId = '401-518-000-000-000-0600';
      insert p1;
      
      Product_Dimension__c ptp = new Product_Dimension__c();
      ptp.Product_External_ID__c = '401-518-000-000-000-0600';
      ptp.Product__c = p1.id;
      insert ptp;
      
      Product_Dimension__c  pt = [select id, Product_External_ID__c from Product_Dimension__c  where id =:ptp.id];
      
    //  System.assertEquals(pt.product__c,p1.id);
      list<Id> ptrList = new list<Id>();
      ptrList.add(pt.id);
      
      UpdateLookupFromProductToProDimen.UpdateLookupValue(ptrList);
   }
}
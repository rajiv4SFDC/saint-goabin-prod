public class ProductForecastTriggerHandler extends TriggerHandler {

	public ProductForecastTriggerHandler(){}

	public override void afterInsert() {

        new OpportunityTeamManager().createOpptyProdTeamMemberAfterProdForeInserted( (List<Product_Forecast__c>)Trigger.new );
    	//new OpportunityTeamManager().checkRecalculateCheckboxOnOpptyFrmProdForecast( (List<Product_Forecast__c>)Trigger.new );
    }   
}
/**
* Created Date: 17-Sep-2018
* Created By: KVP Business Solutions
* 
*/
@IsTest
public class PnPActualVsTargetBatch_Test {
    public static String CRON_EXP = '0 0 17 1/1 * ? *';
    
    @isTest
    public static void coverTargetAndActualHandler(){
            account ac = new account(name='test TvA',sales_office_ERP__c='SPR1');
            insert ac;
        
            target_vs_actuals_P_P__c a = new target_vs_actuals_P_P__c(sales_office__c='SPR1',target_date__c=system.today(),target__c=1234,Sales_Area__c='MUM1',Reporting_Range__c='Clear_Thick');
            insert a;
            
             target_vs_actuals_P_P__c b = new target_vs_actuals_P_P__c(sales_office__c='SPR1',target_date__c=system.today(),target__c=123,Sales_Area__c='DEL1',Reporting_Range__c='Clear_Thin');
            insert b;
        
            invoice__c invice = new invoice__c(item_tonnage__c='1234',billing_date__c=system.today(),customer__c=ac.id);
            insert invice;
        
        system.assertEquals('SPR1', ac.Sales_Office_ERP__c);
        set<string>SalesOfficeSet = new set<string>();
        map<String,target_vs_actuals_P_P__c> targetmap = new map<String,target_vs_actuals_P_P__c>();
        
        list<target_vs_actuals_P_P__c> TargetvsActualList = new list<target_vs_actuals_P_P__c>([select id,sales_office__c,target__c,actual__c,Unique_Target_Actual_Identifier__c from target_vs_actuals_P_P__c]);
        for(target_vs_actuals_P_P__c tva : TargetvsActualList){
            targetmap.put(tva.Unique_Target_Actual_Identifier__c,TvA);
            SalesOfficeSet.add(Tva.sales_office__c);
        }  
        List<invoice__c> InvoiceLst = [SELECT id,sales_office__C,billing_date__c,reporting_range__c,item_tonnage_formula__c FROM invoice__c WHERE sales_office__c IN: SalesOfficeSet];
        PnPActualvsTargetHelper h = new PnPActualvsTargetHelper();
        h.ActualsFromInvoice(InvoiceLst,targetmap);
        
        Profile p = [SELECT Id FROM Profile WHERE Name='Standard User'];
        User u = new User(Alias = 'standt', Email='standarduser@testorg.com',EmailEncodingKey='UTF-8',
                          LastName='Testing', LanguageLocaleKey='en_US',phone='1234567654',LocaleSidKey='en_US', ProfileId = p.Id,
                          TimeZoneSidKey='America/Los_Angeles',UserName='test@test.comE');

        System.runAs(u) {
              // The following code runs as user 'u'
             Test.startTest();
        
      //  Date startdate = Date.newInstance(2018, 8, 1);
      //  String jobId = System.schedule('PnPTargetvsActualBatch', CRON_EXP, new PnPTargetVsActualScheduler(startdate)); 
        
        Date startdate2 = Date.newInstance(2018, 9, 1);
        String jobId2 = System.schedule('PnPTargetvsActualBatch', CRON_EXP, new PnPTargetVsActualScheduler()); 
        
        PnPTargetVsActualBatch tv = new PnPTargetVsActualBatch();
        Database.executeBatch(tv,5);
        
        Test.stopTest();
          }

        // Cover TargetActualHandler
        new PnPActualvsTargetHelper().getMonthAggregrateName(1);
        new PnPActualvsTargetHelper().getMonthAggregrateName(2);
        new PnPActualvsTargetHelper().getMonthAggregrateName(3);
        new PnPActualvsTargetHelper().getMonthAggregrateName(4);
        new PnPActualvsTargetHelper().getMonthAggregrateName(5);
        new PnPActualvsTargetHelper().getMonthAggregrateName(6);
        new PnPActualvsTargetHelper().getMonthAggregrateName(7);
        new PnPActualvsTargetHelper().getMonthAggregrateName(8);
        new PnPActualvsTargetHelper().getMonthAggregrateName(9);
        new PnPActualvsTargetHelper().getMonthAggregrateName(10);
        new PnPActualvsTargetHelper().getMonthAggregrateName(10);
        new PnPActualvsTargetHelper().getMonthAggregrateName(11);
        new PnPActualvsTargetHelper().getMonthAggregrateName(12);
        new PnPActualvsTargetHelper().getMonthAggregrateName(13);
    }
}
/* Created By: Rajiv Kumar Singh
Created Date: 11/06/2017
Class Name: INVageingUpsertAPI
Description : Upsert Ageing on Invoice wise ageing for ERP.
Last Modified By :Rajiv Kumar Singh
Last Modified Date: 11/06/2017
*/
@RestResource(urlMapping='/INVageingUpsertAPI')
global class INVAgeingUpsertAPI {
    @HttpPost
    global static void doHandleInsertRequest(){
        
        List<INVageingWrapper> wrapList = new List<INVageingWrapper>();
        List<Invoice_Wise_Ageing__c> INVagelList = new List<Invoice_Wise_Ageing__c>();
        RestRequest Req = RestContext.request;
        RestResponse res = RestContext.response;
        String body = Req.requestBody.toString().trim();
        
        if(body != null && body != '') {
            
            try {
                wrapList = (List<INVageingWrapper>)JSON.deserialize(body, List<INVageingWrapper>.class);
                
                for(INVageingWrapper we :wrapList){
                    Invoice_Wise_Ageing__c INVagel = new Invoice_Wise_Ageing__c();
                    if(we.lessthanthirty!=null){
                        INVagel.X30__c = we.lessthanthirty;
                    }
                    if(we.Thirtyone2sixty!=null){
                        INVagel.X31_60__c = we.Thirtyone2sixty;
                    }
                    if(we.Sixtyone2ninty!=null){
                        INVagel.X61_90__c = we.Sixtyone2ninty;
                    } 
                    if(we.Nintyone2Onetwenty!=null){
                        INVagel.X91_120__c = we.Nintyone2Onetwenty;
                    }
                    if(we.GreaterThanOnetwenty!=null){
                        INVagel.X120__c = we.GreaterThanOnetwenty;
                    }
                    if(we.NoOfDays!=null){
                        INVagel.No_of_Days__c = we.NoOfDays;
                    }
                    if(we.Doctype!=null && we.Doctype!='' ){
                        INVagel.Doc_Type__c = we.Doctype;
                    }
                    if(we.Invno!=null && we.Invno!='' && we.PosNo!=null && we.PosNo!=''){
                        INVagel.Invoice_No__c = we.Invno+'-'+we.PosNo;
                    }
                    INVagelList.add(INVagel);  
                }
                Schema.SObjectField ftoken = Invoice_Wise_Ageing__c.Fields.Invoice_No__c;
                Database.UpsertResult[] srList = Database.upsert(INVagelList,ftoken,false);           
                res.statusCode = 200;
                res.responseBody = Blob.valueOf(String.valueof(INVagelList));
                
            } catch (Exception e) {
                
                CommonUtilitiesHandler.handleExceptions(e);
                res.statusCode = 500;
                res.responseBody = Blob.valueOf(e.getMessage());
            }
        } else {
            res.statusCode = 500;
            res.responseBody = Blob.valueOf('Invalid Request');
        } 
    }
    
    public class INVageingWrapper{
        public decimal lessthanthirty{get;set;}
        public decimal Thirtyone2sixty{get;set;}
        public decimal Sixtyone2ninty{get;set;}
        public decimal Nintyone2Onetwenty{get;set;}
        public decimal GreaterThanOnetwenty{get;set;}
        public decimal NoOfDays{get;set;}
        public string Doctype{get;set;}
        public string Invno{get;set;}
        public string PosNo{get;set;}
    }
    
}
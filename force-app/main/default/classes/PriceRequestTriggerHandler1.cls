/* Created By         : Rajiv Kumar Singh
   Created Date       : 15/06/2018
   Class Name         : PriceRequestTriggerHandler1
   Description        : 
   Last Modified By   : Rajiv Kumar Singh
   Last Modified Date : 15/06/2018
*/
public class PriceRequestTriggerHandler1 {
    
    Public static void  updatereference(List<Price_Request__c>  prlist){
      //  Map<id,Enquiry__c> enqMap = new Map<id,Enquiry__c>();
       // Map<id,enquiry_Line_Item__c> enqlineMap = new Map<id,enquiry_Line_Item__c>();
        List<Enquiry__c> enqlist = new List<Enquiry__c>();
        List<Enquiry_Line_Item__c> enqlinelist = new List<Enquiry_Line_Item__c>();
        Set<Id> enqIds = new Set<Id>();
        Set<Id> enqLineIds = new Set<Id>();
        for(Price_Request__c pr:prlist){
        if(pr.enquiry__c != null)
            enqIds.add(pr.enquiry__c);
         if(pr.enquiry_Line_Item__c != null)
            enqLineIds.add(pr.enquiry_Line_Item__c);
        }
        Map<id,Enquiry__c> enqMap = new Map<id,Enquiry__c>([select id,GD_Reference_Number__c from Enquiry__c where Id IN:enqIds]);
        
        Map<id,enquiry_Line_Item__c> enqlineMap = new Map<id,enquiry_Line_Item__c>([select id,Reference_Number__c from Enquiry_Line_Item__c where Id IN:enqLineIds]);

        for(Price_Request__c preq:prlist){
           if(preq.enquiry__c!= null && !enqMap.isEmpty() && enqMap.containsKey(preq.enquiry__c) && enqMap.get(preq.enquiry__c).GD_Reference_Number__c !=null){
            preq.Enquiry_Reference_Number__c = enqMap.get(preq.enquiry__c).GD_Reference_Number__c;
            }
            if(preq.enquiry_line_item__c!=null && !enqlineMap.isEmpty() &&  enqlineMap.containsKey(preq.enquiry_line_item__c) && enqlineMap.get(preq.enquiry_line_item__c).Reference_Number__c !=null){
            preq.Enquiry_Line_Item_Reference_No__c = enqlineMap.get(preq.enquiry_line_item__c).Reference_Number__c;
          }
            
        }
        
        
    }//end of the Update  Reference method
    
    // Updating the Price Request Manager on the basis of Customer Region before insert
    
    public static void updatePriceManager(List<Price_Request__c>prlist2){
        Set<id> accIdSet = new Set<Id>();
        for(Price_Request__c prqt : prlist2){
            if(prqt.Customer__c!=null){
                accIdSet.add(prqt.customer__c);
            }
        }
        if(!accIdSet.isEmpty()){
                   doHandleUserUpdate(accIdSet,prlist2);
               } 
          
          getProductsFieldsInformation(prlist2);
        
        }
                    // Updation of Price Request Manager before Update
        
            public static void doCheckForBeforeUpdate(List<Price_Request__c>prlist2,Map<Id,Price_Request__c> oldMap){
                Set<id> accIdSet = new Set<Id>();
                list<price_request__c> prListNew = new list<price_request__c>();
                
                for(Price_Request__c prqt : prlist2){
                    if(prqt.Customer__c!=null && oldMap!= null && prqt.Customer__c!=oldMap.get(prqt.id).Customer__c){
                        accIdSet.add(prqt.customer__c);
                    }
                    
                    if(prqt.product_lookup__c !=null && oldMap!= null && prqt.product_lookup__c !=oldMap.get(prqt.id).product_lookup__c){
                      prListNew.add(prqt);
                      }
               }
               
               if(!prListNew.isEmpty())
                     getProductsFieldsInformation(prlist2);
               if(!accIdSet.isEmpty()){
                   doHandleUserUpdate(accIdSet,prlist2);
               }
            
            } 
            
            public static void doHandleUserUpdate(Set<Id> accIdSet,List<Price_Request__c>prlist2){
                Map<String,NewcustomerrequestforPR__c> mapForReg = NewcustomerrequestforPR__c.getAll(); 
        Map<String,String> roleNameMap = new Map<String,String>();
        
        for(String mape : mapForReg.keySet()){
            roleNameMap.put(mape,mapForReg.get(mape).Role_Name__c);
        }
        Map<String,Id> forUserMap = new Map<String,Id>();
        map<string,account> accMap = new map<string,account>();
        for(User usr : [SELECT Id,Name,UserRole.Name,UserRole.DeveloperName from user where UserRole.Name IN :roleNameMap.values()]){
            if(!forUserMap.containskey(usr.UserRole.Name)){
                forUserMap.put(usr.UserRole.Name,usr.id);
            }
            
        }
        Map<Id,String> userInfoMap = new Map<Id,String>();
        for(Account acc : [select id,name,Sales_Area_Region__c,sap_code__c from Account WHERE Id IN : accIdSet]){
                accMap.put(acc.id,acc);
                userInfoMap.put(acc.id,acc.Sales_Area_Region__c);
            }
        
        
      for(Price_Request__c prc :prlist2){
        if(prc.customer__c != null && userInfoMap.containsKey(prc.customer__c))
        {
            system.debug('The vales are :' + userInfoMap.get(prc.Customer__c) +'......'+roleNameMap.get(userInfoMap.get(prc.Customer__c))+'......'+ forUserMap.get(roleNameMap.get(userInfoMap.get(prc.Customer__c))));
           //prc.Regional_manager_special_price__c =    roleNameMap.get(userInfoMap.get(prc.customer__c));
           //System.debug('value ='+forUserMap.get(roleNameMap.get(userInfoMap.get(prc.Customer__c))));
           try{
          prc.Regional_manager_special_price__c = Id.valueOf(forUserMap.get(roleNameMap.get(userInfoMap.get(prc.Customer__c))));
          }  catch(exception ert)
          {
              system.debug('exception came:'+ert);
          }
        }
        
        if(accMap!= null && accMap.containsKey(prc.customer__c))
        {
        //   prc.Bill_to_Customer__c  = accMap.get(prc.customer__c).Name;
           prc.Bill_to_customer_SAP_code__c = accMap.get(prc.customer__c).Sap_code__c;
        }
            }
            }
            
        public static void getProductsFieldsInformation(list<price_request__c> newList)
        {
            set<string> prdIds = new set<string>();
            for(price_request__c pr:newList)
            {
              prdIds.add(pr.product_lookup__c);
            }
            
            map<string,product2> prdMap = new map<string,product2>();
            
            // get the produt information here. 
            for(product2 p1:[select id,name,Thickness_mm__c,Regional_head_approval_limit__c,List_Price__c from product2 where id in:prdIds])
            {
                prdMap.put(p1.id,p1);
            }
            
            
            for(price_request__c pr:newList)
            {
            if(pr.product_lookup__c != null && prdMap.containsKey(pr.product_lookup__c))
            {
              pr.Product_Name_enquiry_component__c =  prdMap.get(pr.product_lookup__c).Name;
              pr.List_tier_price__c =  prdMap.get(pr.product_lookup__c).List_Price__c;
              pr.Enquiry_Thickness__c = String.valueof(prdMap.get(pr.product_lookup__c).Thickness_mm__c);
              pr.Regional_head_approval_limit__c =  prdMap.get(pr.product_lookup__c).Regional_head_approval_limit__c;
            }
            system.debug('The updatedpr is:' +pr);  
            }
            
            
            
        
        }    
            

           
            
 }
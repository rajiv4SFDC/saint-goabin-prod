public class OpportunityUserFieldsHandler {
    public void updateUserFields(List<Opportunity> optyList){
        //System.debug('Test Opty insert called when new owner');
        /** @var ownerMap - map of Owner Ids to their matching Owner Record */
        Map<Id,User> ownerMap;
        /** @var oppOwnerIds - set of Opportunity Owner Ids from the inserted / updated records */
        Set<Id> oppOwnerIds = new Set<Id>();
        /** @var idOfOwner - will hold the id of the Opportunity Owner */
        String idOfOwner;
        
        for(Opportunity opps : optyList){ // loop through all updated/inserted opps
            oppOwnerIds.add(opps.OwnerId); // add the owner id to the set of owner ids
            //System.debug('Opportunity Id'+opps.OwnerId); 
        } 
        
        ownerMap = new Map<Id,User>([SELECT Id, Name, Sales_Area__c, Region__c FROM User WHERE Id IN :oppOwnerIds]);
        
        for(Opportunity opps : optyList){ // loop through the updated/inserted opps again
            idOfOwner = ownerMap.get(opps.OwnerId).Id; // use the opportunity owner id to grab the record from the map
            opps.Sales_Area__c = ownerMap.get(opps.OwnerId).Sales_Area__c; 
            opps.Region__c = ownerMap.get(opps.OwnerId).Region__c;              
        }  
    }
    
    //Updating updated opty's owner related fields
    public void updateMockUpPRFields(List<Opportunity> newOptyList, Map<Id,Opportunity> OptyOldMap){ 
        //System.debug('Test Opty update called when owner changed');
        Map<Id,List<Mock_Up_Request__c>> MURMap = new Map<Id,List<Mock_Up_Request__c>>();
        Map<Id,List<Price_Request__c>> PRMap = new Map<Id,List<Price_Request__c>>();
        Map<Id,Id> OptyId_OwnerId = new Map<Id,Id>();
        Set<Id> OptyIdSet = new Set<Id>();
        
        /** @var ownerMap - map of Owner Ids to their matching Owner Record */
        Map<Id,User> ownerMap;
        /** @var oppOwnerIds - set of Opportunity Owner Ids from the inserted / updated records */
        Set<Id> oppOwnerIds = new Set<Id>();
        /** @var idOfOwner - will hold the id of the Opportunity Owner */
        String idOfOwner;
        List<Opportunity> optyList = new List<Opportunity>();

        
        
        for(Opportunity n : newOptyList){
            Opportunity o = OptyOldMap.get(n.Id);
            //System.debug('Old owner : '+o.Owner.Name);
            //System.debug('New owner : '+n.Owner.Name);
            if(o.OwnerId != n.OwnerId){
                OptyIdSet.add(n.Id);  
                OptyId_OwnerId.put(n.Id,n.OwnerId);
                oppOwnerIds.add(n.OwnerId);
                optyList.add(n);               
                //System.debug('MUR List SET OptyOwnerIdSet value:'+n.Id);
            }
        }

        //List<User> activeUserList  = new List<User>([Select Id, IsActive from User where IsActive = true and id IN :oppOwnerIds]);

       /* for(User u : activeUserList){
            if(u.IsActive){
                activeUserSet.add(u.Id);
            }
        }*/

        //New code starts
          ownerMap = new Map<Id,User>([SELECT Id, Name, Sales_Area__c, Region__c, IsActive FROM User WHERE Id IN :oppOwnerIds]);
          Set<Id> activeUserSet = new Set<Id>();
          for(User u : ownerMap.values()){
            if(u.IsActive){
                activeUserSet.add(u.Id);
            }
          }
        
        for(Opportunity opps : optyList){ // loop through the updated/inserted opps again
            //System.debug('Test Opty update ,sales area and region updated on opty when owner changed');
            User u = ownerMap.get(opps.OwnerId);
            if(u != null){
            idOfOwner = u.Id; // use the opportunity owner id to grab the record from the map
            opps.Sales_Area__c = u.Sales_Area__c; 
            opps.Region__c = u.Region__c;  
            }
        }  
        //New code ends
         
        //System.debug('MUR List SET SIZE:'+OptyIdSet.size());
        List<Mock_Up_Request__c> murList = new List<Mock_Up_Request__c>([SELECT Id, Opportunity__c, OwnerId__c,D_C_Manager__c,Regional_Manager__c,Approval_Status__c FROM Mock_Up_Request__c WHERE Opportunity__c IN :OptyIdSet AND (Approval_Status__c ='Draft' OR Approval_Status__c ='Rejected')]);
        List<Price_Request__c> prList = new List<Price_Request__c>([SELECT Id, Opportunity__c, OwnerId__c,D_C_Manager__c,Regional_Manager__c,Approval_Status__c FROM Price_Request__c WHERE Opportunity__c IN :OptyIdSet AND (Approval_Status__c ='Draft' OR Approval_Status__c ='Rejected')]);//REMOVE REJECTED
        
        //fetching Mock Up Request (child records) of optys' whose owner has been changed
        for(Mock_Up_Request__c n : (murList)){
            List<Mock_Up_Request__c> PVOwnerList = MURMap.get(OptyId_OwnerId.get(n.Opportunity__c));
            if(PVOwnerList!=null && !PVOwnerList.isEmpty()){
                PVOwnerList.add(n);
            }                    
            else {
                PVOwnerList = new List<Mock_Up_Request__c>();
                PVOwnerList.add(n);
            }
            MURMap.put(OptyId_OwnerId.get(n.Opportunity__c),PVOwnerList);
        }  
        
        new UserManagerFieldsHandler().updatePVFields(MURMap,TRUE);
        //System.debug('MUR List :'+murList.size());
        
        //fetching Price Request (child records) of optys' whose owner has been changed
        for(Price_Request__c n : (prList)){
            List<Price_Request__c> PVOwnerList = PRMap.get(OptyId_OwnerId.get(n.Opportunity__c));
            if(PVOwnerList!=null && !PVOwnerList.isEmpty()){
                PVOwnerList.add(n);
            }                    
            else {
                PVOwnerList = new List<Price_Request__c>();
                PVOwnerList.add(n);
            }
            PRMap.put(OptyId_OwnerId.get(n.Opportunity__c),PVOwnerList);
        }          
        
        new UserManagerFieldsHandler().updatePVFields(PRMap,TRUE);
        //System.debug('MUR PR List :'+prList.size());

         //New code for updating opportunity owner on Invoice
         List<Invoice__c> invoiceList = new List<Invoice__c>([SELECT Id, Opportunity__c,OwnerId,Owner.IsActive FROM Invoice__c WHERE Opportunity__c IN :OptyIdSet]);
         //System.debug('invoiceList size:'+invoiceList.size());
         //System.debug('invoiceList :'+invoiceList);

        if(!invoiceList.isEmpty()){
          for(Invoice__c n : (invoiceList)){    
            
                if(activeUserSet.contains(OptyId_OwnerId.get(n.Opportunity__c))){
                    n.OwnerId = OptyId_OwnerId.get(n.Opportunity__c);  
                }                
                          
        }
         update invoiceList;
     }
        //New code ends
        
    }         
}
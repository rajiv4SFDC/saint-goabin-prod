global class InvoiceTaggingScheduler implements Schedulable {
  
    global InvoiceTaggingScheduler(){
      
    }
     global void execute(SchedulableContext sc){
        InvoiceTaggingBatchable ivT = new InvoiceTaggingBatchable();
        Database.executeBatch(ivT,1);
    }
    /*
                    ///////////////  Schedule Code for firing it in Production  /////////////
 String cronStr = '0 0 0/1 1/1 * ? *';
System.schedule('Process Accs Job', cronStr, pa);

  String cronStr = '0 0 0/1 1/1 * ? *';
  String jobID = System.schedule(‘Invoice Tagging Scheduler’, cronStr , new InvoiceTaggingScheduler());
  System.debug('JOB id :'+jobID);
*/
}
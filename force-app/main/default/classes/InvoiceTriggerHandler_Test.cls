@isTest
public class InvoiceTriggerHandler_Test {
    public static testMethod void testInvoice(){
        Account acc = new Account();
        acc.Name = 'KVP';
        insert acc;
        
        Price_Request_PandP__c pp = new Price_Request_PandP__c();
        pp.Approval_status__c = 'Approved';
        pp.Available_Quantity_Tons__c = 10;
        pp.Requested_Quantity__c = 10;
        pp.sales_office__c ='DEL1';
        insert pp;
        System.debug('pp-->'+pp);
        
        Price_Request_PandP__c piy = [SELECT id,Name FROM Price_Request_PandP__c WHERE id =:pp.Id LIMIT 1];
        System.debug('-->Name '+piy.Name);
        String transactionNumber = piy.Name;
         
        Invoice__c inv = new Invoice__c();
        inv.Customer__c = acc.id;
        inv.Billing_Date__c = Date.today();
        inv.Transaction_No__c = transactionNumber;
        inv.Invoice_type__c = 'clear';
        inv.Processed__c = false;
        InvoiceTriggerHandler.stoprecur = false;
        inv.Item_Tonnage__c = '12';
        insert inv;
        
              
        
        
    }
}
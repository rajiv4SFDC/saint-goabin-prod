public class TargetActualHandler {

	/******* This method is used for calculating product forecasting related Actuals *******/
	/* 1)This method is used to calculate Sum of KAM's appropriated sales quantity against product forecasts 
	 
	    Target Type = Total Sales Volume 
		Product Range = NA
	*/
	/* 2)This method is used to calculate Sum of KAM's appropriated sales quantity against product forecasts under Projects where Segment is Residential 
	 
	    Target Type = Sales Residential Segment
		Product Range = NA
	*/
	/* 3) This method is used to calculate Sum of KAM's appropriated sales quantity against product forecasts where Product Range is (Premium Plus)
		
		 Target Type = Product Range wise Sales Targets
	     Product Range = Premium Plus
	*/
	/* 4) This method is used to calculate Sum of KAM's Sappropriated sales quantity against product forecasts under Projects where 5G Surge is TRUE
		
		 Target Type = 5G Surge
	     Product Range = NA
	*/
	/* 5) This method is used to calculate Sum of KAM's appropriated sales quantity against opportunity product forecast under Projects where Product Range is Cool Vision
		
		 Target Type = Product Range wise Sales Targets
	     Product Range = Cool Homes
	*/
	/* 6) This method is used to calculate Sum of KAM's appropriated sales quantity against opportunity product forecast under Projects where Product Range is Evolve Glass
		
		 Target Type = Product Range wise Sales Targets
	     Product Range = Evolve Glass
	*/
	/* 6.1) This method is used to calculate Sum of KAM's appropriated sales quantity against opportunity product forecast under Projects where Product Range is Sage Glass
		
		 Target Type = Product Range wise Sales Targets
	     Product Range = Sage Glass
	*/
	/* 7) This method is used to calculate Sum of KAM’s appropriated sales quantity against product forecasts under Projects where Luxe Glass is TRUE
		
		 Target Type = Luxe Glass
	     Product Range = NA
	*/
	/* 8) This method is used to calculate Sum of KAM’s appropriated sales quantity against product forecasts under Projects where Luxe Homes is TRUE
		
		 Target Type = Luxe Homes
	     Product Range = NA
	*/
	/* 9) This method is used to calculate Sum of KAM’s appropriated sales quantity against product forecasts under Projects where Cool Homes is TRUE
		
		 Target Type = Cool Homes
	     Product Range = NA
	*/
	/* 10) This method is used to calculate Sum of KAM’s appropriated sales quantity against product forecasts under Projects where Glass Govern is TRUE
		
		 Target Type = Glass Govern
	     Product Range = NA
	*/
	/* 11) This method is used to calculate Sum of KAM’s appropriated sales quantity against product forecasts under Projects where Glass Heals is TRUE
		
		 Target Type = Glass Heals
	     Product Range = NA
	*/
	
	public Map<String,Target_and_Actual__c> calculateForecastRelatedActuals(List<Product_Forecast__c> prodForecastLst,Map<String,Target_and_Actual__c> targerActualMap){

		Map<String,Boolean> mapOfTargetandHasError = new Map<String,Boolean>();
		for(Product_Forecast__c fore : prodForecastLst){  

			/* Only for Projects where Segment = Residential Buildings */
			if( fore.Opportunity_Product__r.Opportunity__r.Project__r.Segment__c == 'Residential Buildings' ){

				String identifier = getIdentifier( fore.Opportunity_Product__r.Opportunity__r.Sales_Area__c,
					'Sales Residential Segment','NA',						
			 		date.newinstance(fore.Forecast_Date__c.year(), fore.Forecast_Date__c.month(), fore.Forecast_Date__c.day()) );

					if( targerActualMap.containsKey(identifier) && !mapOfTargetandHasError.containsKey(identifier) ){

						Target_and_Actual__c tvav = targerActualMap.get(identifier);
						try{
							if( tvav.Calculate_Actuals__c ){
							tvav.Actual__c = tvav.Actual__c + fore.Total_Appropriable_Quantity__c;
							}

							if( tvav.Calculate_Point_Scored__c ){
								tvav.Points_Scored__c = tvav.Points_Eligible__c * tvav.Actual__c/ tvav.Target__c;
							}

							// Blank the error message field
							tvav.Error_Message__c = '';

						}catch(Exception e){
							tvav.Error_Message__c = 'Error : '+e.getMessage();
							tvav.Actual__c = 0;
							tvav.Points_Scored__c = 0;
							mapOfTargetandHasError.put(identifier,true);
						}
						targerActualMap.put(identifier,tvav);
					}

			}

			// Only for Oppty Product where Product Range exist and corresponding Target exist  
			if( fore.Opportunity_Product__r.Product__r.Product_Range__c != null ){

				String identifier = getIdentifier( fore.Opportunity_Product__r.Opportunity__r.Sales_Area__c,
					'Product Range wise Sales Targets', fore.Opportunity_Product__r.Product__r.Product_Range__c ,						
			 		date.newinstance(fore.Forecast_Date__c.year(), fore.Forecast_Date__c.month(), fore.Forecast_Date__c.day()) );

					if( targerActualMap.containsKey(identifier) && !mapOfTargetandHasError.containsKey(identifier) ){

						Target_and_Actual__c tvav = targerActualMap.get(identifier);
						
							try{
								if( tvav.Calculate_Actuals__c ){
									tvav.Actual__c = tvav.Actual__c + fore.Total_Appropriable_Quantity__c;
								}

								if( tvav.Calculate_Point_Scored__c ){
									tvav.Points_Scored__c = tvav.Points_Eligible__c * tvav.Actual__c/ tvav.Target__c;
								}

								// Blank the error message field
								tvav.Error_Message__c = '';
							}catch(Exception e){
								tvav.Error_Message__c = 'Error : '+e.getMessage()+'\n';
								tvav.Actual__c = 0;
								tvav.Points_Scored__c = 0;
								mapOfTargetandHasError.put(identifier,true);
							}

						targerActualMap.put(identifier,tvav);
					}
			}

			/*
			// Only for Oppty Product where Product Range = Cool Vision 
			if( fore.Opportunity_Product__r.Product__r.Product_Range__c == 'Cool Vision' ){

				String identifier = getIdentifier( fore.Opportunity_Product__r.Opportunity__r.Sales_Area__c,
					'Product Range wise Sales Targets','Cool Vision',						
			 		date.newinstance(fore.Forecast_Date__c.year(), fore.Forecast_Date__c.month(), fore.Forecast_Date__c.day()) );

					if( targerActualMap.containsKey(identifier) ){

						Target_and_Actual__c tvav = targerActualMap.get(identifier);
						if( tvav.Calculate_Actuals__c ){
							tvav.Actual__c = tvav.Actual__c + fore.Total_Appropriable_Quantity__c;
						}

						if( tvav.Calculate_Point_Scored__c ){
							tvav.Points_Scored__c = tvav.Points_Eligible__c * tvav.Actual__c/ tvav.Target__c;
						}

						targerActualMap.put(identifier,tvav);
					}

			}

		   // Only for Oppty Product where Product Range = Evolve Glass 
			if( fore.Opportunity_Product__r.Product__r.Product_Range__c == 'Evolve Glass' ){

				String identifier = getIdentifier( fore.Opportunity_Product__r.Opportunity__r.Sales_Area__c,
					'Product Range wise Sales Targets','Evolve Glass',						
			 		date.newinstance(fore.Forecast_Date__c.year(), fore.Forecast_Date__c.month(), fore.Forecast_Date__c.day()) );

					if( targerActualMap.containsKey(identifier) ){

						Target_and_Actual__c tvav = targerActualMap.get(identifier);
						if( tvav.Calculate_Actuals__c ){
							tvav.Actual__c = tvav.Actual__c + fore.Total_Appropriable_Quantity__c;
						}

						if( tvav.Calculate_Point_Scored__c ){
							tvav.Points_Scored__c = tvav.Points_Eligible__c * tvav.Actual__c/ tvav.Target__c;
						}
						
						targerActualMap.put(identifier,tvav);
					}

			}

			// Only for Oppty Product where Product Range = Sage Glass 
			if( fore.Opportunity_Product__r.Product__r.Product_Range__c == 'Sage Glass' ){

				String identifier = getIdentifier( fore.Opportunity_Product__r.Opportunity__r.Sales_Area__c,
					'Product Range wise Sales Targets','Sage Glass',						
			 		date.newinstance(fore.Forecast_Date__c.year(), fore.Forecast_Date__c.month(), fore.Forecast_Date__c.day()) );

					if( targerActualMap.containsKey(identifier) ){

						Target_and_Actual__c tvav = targerActualMap.get(identifier);
						if( tvav.Calculate_Actuals__c ){
							tvav.Actual__c = tvav.Actual__c + fore.Total_Appropriable_Quantity__c;
						}

						if( tvav.Calculate_Point_Scored__c ){
							tvav.Points_Scored__c = tvav.Points_Eligible__c * tvav.Actual__c/ tvav.Target__c;
						}
						
						targerActualMap.put(identifier,tvav);
					}

			}
			*/

			/* Only for Project where Project has 5G Surge is True */
			if( fore.Opportunity_Product__r.Opportunity__r.Project__r.City__c != null ){

				String identifier = getIdentifier( fore.Opportunity_Product__r.Opportunity__r.Sales_Area__c,
					'iCities', fore.Opportunity_Product__r.Opportunity__r.Project__r.City__r.External_ID__c ,						
			 		date.newinstance(fore.Forecast_Date__c.year(), fore.Forecast_Date__c.month(), fore.Forecast_Date__c.day()) );

					if( targerActualMap.containsKey(identifier) && !mapOfTargetandHasError.containsKey(identifier) ){

						Target_and_Actual__c tvav = targerActualMap.get(identifier);
						try{
							if( tvav.Calculate_Actuals__c ){
							tvav.Actual__c = tvav.Actual__c + fore.Total_Appropriable_Quantity__c;
							}

							if( tvav.Calculate_Point_Scored__c ){
								tvav.Points_Scored__c = tvav.Points_Eligible__c * tvav.Actual__c/ tvav.Target__c;
							}

							// Blank the error message field
							tvav.Error_Message__c = '';

						}catch(Exception e){
							tvav.Error_Message__c = 'Error : '+e.getMessage();
							tvav.Actual__c = 0;
							tvav.Points_Scored__c = 0;
							mapOfTargetandHasError.put(identifier,true);
						}
						
						targerActualMap.put(identifier,tvav);
					}

			}

			/* Only for Project where Project has Luxe Glass is True */
			if( fore.Opportunity_Product__r.Opportunity__r.Project__r.Luxe_Glass__c == true ){

				String identifier = getIdentifier( fore.Opportunity_Product__r.Opportunity__r.Sales_Area__c,
					'Luxe Glass','NA',						
			 		date.newinstance(fore.Forecast_Date__c.year(), fore.Forecast_Date__c.month(), fore.Forecast_Date__c.day()) );

					if( targerActualMap.containsKey(identifier) && !mapOfTargetandHasError.containsKey(identifier)){

						Target_and_Actual__c tvav = targerActualMap.get(identifier);
						try{

							if( tvav.Calculate_Actuals__c ){
								tvav.Actual__c = tvav.Actual__c + fore.Total_Appropriable_Quantity__c;
							}

							if( tvav.Calculate_Point_Scored__c ){
								tvav.Points_Scored__c = tvav.Points_Eligible__c * tvav.Actual__c/ tvav.Target__c;
							}

							// Blank the error message field
							tvav.Error_Message__c = '';

						}catch(Exception e){
							tvav.Error_Message__c = 'Error : '+e.getMessage();
							tvav.Actual__c = 0;
							tvav.Points_Scored__c = 0;
							mapOfTargetandHasError.put(identifier,true);
						}

						targerActualMap.put(identifier,tvav);
					}

			}

			/* Only for Project where Project has Luxe Homes is True */
		/*	if( fore.Opportunity_Product__r.Opportunity__r.Project__r.Luxe_Homes__c == true ){

				String identifier = getIdentifier( fore.Opportunity_Product__r.Opportunity__r.Sales_Area__c,
					'Luxe Homes','NA',						
			 		date.newinstance(fore.Forecast_Date__c.year(), fore.Forecast_Date__c.month(), fore.Forecast_Date__c.day()) );

					if( targerActualMap.containsKey(identifier) && !mapOfTargetandHasError.containsKey(identifier)){

						Target_and_Actual__c tvav = targerActualMap.get(identifier);
						try{
							if( tvav.Calculate_Actuals__c ){
								tvav.Actual__c = tvav.Actual__c + fore.Total_Appropriable_Quantity__c;
							}

							if( tvav.Calculate_Point_Scored__c ){
								tvav.Points_Scored__c = tvav.Points_Eligible__c * tvav.Actual__c/ tvav.Target__c;
							}

							// Blank the error message field
							tvav.Error_Message__c = '';

						}catch(Exception e){
							tvav.Error_Message__c = 'Error : '+e.getMessage();
							tvav.Actual__c = 0;
							tvav.Points_Scored__c = 0;
							mapOfTargetandHasError.put(identifier,true);
						}
						
						targerActualMap.put(identifier,tvav);
					}

			}
		*/
			/* Only for Project where Project has Cool Homes is True */
			if( fore.Opportunity_Product__r.Opportunity__r.Project__r.Cool_Homes__c == true ){

				String identifier = getIdentifier( fore.Opportunity_Product__r.Opportunity__r.Sales_Area__c,
					'Cool Homes','NA',						
			 		date.newinstance(fore.Forecast_Date__c.year(), fore.Forecast_Date__c.month(), fore.Forecast_Date__c.day()) );

					if( targerActualMap.containsKey(identifier) && !mapOfTargetandHasError.containsKey(identifier)){

						Target_and_Actual__c tvav = targerActualMap.get(identifier);
						try{

							if( tvav.Calculate_Actuals__c ){
								tvav.Actual__c = tvav.Actual__c + fore.Total_Appropriable_Quantity__c;
							}

							if( tvav.Calculate_Point_Scored__c ){
								tvav.Points_Scored__c = tvav.Points_Eligible__c * tvav.Actual__c/ tvav.Target__c;
							}

							// Blank the error message field
							tvav.Error_Message__c = '';

						}catch(Exception e){
							tvav.Error_Message__c = 'Error : '+e.getMessage();
							tvav.Actual__c = 0;
							tvav.Points_Scored__c = 0;
							mapOfTargetandHasError.put(identifier,true);
						}
						
						targerActualMap.put(identifier,tvav);
					}

			}

			/* Only for Project where Project has Glass Govern is True */
			if( fore.Opportunity_Product__r.Opportunity__r.Project__r.Glass_Govern__c == true ){

				String identifier = getIdentifier( fore.Opportunity_Product__r.Opportunity__r.Sales_Area__c,
					'Glass Govern','NA',						
			 		date.newinstance(fore.Forecast_Date__c.year(), fore.Forecast_Date__c.month(), fore.Forecast_Date__c.day()) );

					if( targerActualMap.containsKey(identifier) && !mapOfTargetandHasError.containsKey(identifier)){

						Target_and_Actual__c tvav = targerActualMap.get(identifier);
						try{
							if( tvav.Calculate_Actuals__c ){
								tvav.Actual__c = tvav.Actual__c + fore.Total_Appropriable_Quantity__c;
							}

							if( tvav.Calculate_Point_Scored__c ){
								tvav.Points_Scored__c = tvav.Points_Eligible__c * tvav.Actual__c/ tvav.Target__c;
							}

							// Blank the error message field
							tvav.Error_Message__c = '';

						}catch(Exception e){
							tvav.Error_Message__c = 'Error : '+e.getMessage();
							tvav.Actual__c = 0;
							tvav.Points_Scored__c = 0;
							mapOfTargetandHasError.put(identifier,true);
						}
					
						targerActualMap.put(identifier,tvav);
					}

			}

			/* Only for Project where Project has Glass Heals is True */
			/*if( fore.Opportunity_Product__r.Opportunity__r.Project__r.Glass_Heals__c == true ){

				String identifier = getIdentifier( fore.Opportunity_Product__r.Opportunity__r.Sales_Area__c,
					'Glass Heals','NA',						
			 		date.newinstance(fore.Forecast_Date__c.year(), fore.Forecast_Date__c.month(), fore.Forecast_Date__c.day()) );

					if( targerActualMap.containsKey(identifier) && !mapOfTargetandHasError.containsKey(identifier)){

						Target_and_Actual__c tvav = targerActualMap.get(identifier);
						try{

							if( tvav.Calculate_Actuals__c ){
								tvav.Actual__c = tvav.Actual__c + fore.Total_Appropriable_Quantity__c;
							}

							if( tvav.Calculate_Point_Scored__c ){
								tvav.Points_Scored__c = tvav.Points_Eligible__c * tvav.Actual__c/ tvav.Target__c;
							}

							// Blank the error message field
							tvav.Error_Message__c = '';

						}catch(Exception e){
							tvav.Error_Message__c = 'Error : '+e.getMessage();
							tvav.Actual__c = 0;
							tvav.Points_Scored__c = 0;
							mapOfTargetandHasError.put(identifier,true);
						}
						
						targerActualMap.put(identifier,tvav);
					}

			}
			*/
			/* For all Product Forecast */
			String identifier = getIdentifier( fore.Opportunity_Product__r.Opportunity__r.Sales_Area__c,
					'Total Sales Volume','NA',						
			 		date.newinstance(fore.Forecast_Date__c.year(), fore.Forecast_Date__c.month(), fore.Forecast_Date__c.day()) );

			if( targerActualMap.containsKey(identifier) && !mapOfTargetandHasError.containsKey(identifier)){

				Target_and_Actual__c tvav = targerActualMap.get(identifier);
				try{
					if( tvav.Calculate_Actuals__c ){
								tvav.Actual__c = tvav.Actual__c + fore.Total_Appropriable_Quantity__c;
					}
					/*
					if( tvav.Calculate_Point_Scored__c ){
						tvav.Points_Scored__c = tvav.Points_Eligible__c * tvav.Actual__c/ tvav.Target__c;
					}*/

					// Blank the error message field
					tvav.Error_Message__c = '';

				}catch(Exception e){
					tvav.Error_Message__c = 'Error : '+e.getMessage();
					tvav.Actual__c = 0;
					tvav.Points_Scored__c = 0;
					mapOfTargetandHasError.put(identifier,true);
				}
				
				targerActualMap.put(identifier,tvav);
			}
		}

		return targerActualMap;
	}

	/* This method is used to calculate No. of KAM's Projects with first billing in the month */
	/* 
	    Target Type = New Project Start 
		Product Range = NA
	 */
	/*public Map<String,Target_and_Actual__c> calculateNewProjectStartedActuals(List<Project__c> projLst,Map<String,Target_and_Actual__c> targerActualMap){

		return null;
	}*/


	/* This method is used to calculate No. of KAM's new validated projects added in the month */
	/* 
	    Target Type = Project Database Addition 
		Product Range = NA
	 */
	public Map<String,Target_and_Actual__c> calculateProjectDBAdditionActuals(List<Project__c> projLst,Map<String,Target_and_Actual__c> targerActualMap){

		Map<String,Boolean> mapOfTargetandHasError = new Map<String,Boolean>();
		for(Project__c proj : projLst){  

			String identifier = getIdentifier( proj.Sales_Area__c,'Project Database Addition','NA',						
			 		date.newinstance(proj.Project_Validated_Date__c.year(), proj.Project_Validated_Date__c.month(), proj.Project_Validated_Date__c.day()) );

			if( targerActualMap.containsKey(identifier) && !mapOfTargetandHasError.containsKey(identifier) ){

				Target_and_Actual__c tvav = targerActualMap.get(identifier);
				try{
					if( tvav.Calculate_Actuals__c ){
						tvav.Actual__c = tvav.Actual__c + 1;
					}

					if( tvav.Calculate_Point_Scored__c ){
						tvav.Points_Scored__c = tvav.Points_Eligible__c * tvav.Actual__c/ tvav.Target__c;
					}
					// Blank the error message field
					tvav.Error_Message__c = '';

				}catch(Exception e){
					tvav.Error_Message__c = 'Error : '+e.getMessage();
					tvav.Actual__c = 0;
					tvav.Points_Scored__c = 0;
					mapOfTargetandHasError.put(identifier,true);
				}

				targerActualMap.put(identifier,tvav);
			}
		}

		return targerActualMap;
	}


	/* This method is used to calculate No. of KAM's Opportunity records where Design Report Created is TRUE */
	/* This method is used to calculate No. of KAM's Projects with first billing in the month */
	/* 
	    Target Type = Design Reports 
		Product Range = NA

		Target Type = New Project Start 
		Product Range = NA

	 */
	public Map<String,Target_and_Actual__c> calculateOpportunityRelatedActuals(List<Opportunity> opptyLst,Map<String,Target_and_Actual__c> targerActualMap){

		Map<String,Boolean> mapOfTargetandHasError = new Map<String,Boolean>();
		for(Opportunity oppty : opptyLst){ 

			if( oppty.Design_Report_Created_Date__c != null ){
				String identifierDR = getIdentifier( oppty.Sales_Area__c,'Design Reports','NA',						
			 		date.newinstance(oppty.Design_Report_Created_Date__c.year(), oppty.Design_Report_Created_Date__c.month(), oppty.Design_Report_Created_Date__c.day()) );

				/* Target Type = Design Reports calculation */
				if( targerActualMap.containsKey(identifierDR) && !mapOfTargetandHasError.containsKey(identifierDR) ){

					Target_and_Actual__c tvav = targerActualMap.get(identifierDR);
					try{
						if( tvav.Calculate_Actuals__c ){
							tvav.Actual__c = tvav.Actual__c + 1;
						}

						if( tvav.Calculate_Point_Scored__c ){
							tvav.Points_Scored__c = tvav.Points_Eligible__c * tvav.Actual__c/ tvav.Target__c;
						}

						// Blank the error message field
						tvav.Error_Message__c = '';

					}catch(Exception e){
						tvav.Error_Message__c = 'Error : '+e.getMessage();
						tvav.Actual__c = 0;
						tvav.Points_Scored__c = 0;
						mapOfTargetandHasError.put(identifierDR,true);
					}

					targerActualMap.put(identifierDR,tvav);
				}
			}

			if( oppty.First_Billing_Date__c != null ){ // First_Billing_Date__c
				String identifierNPS = getIdentifier( oppty.Sales_Area__c,'New Project Start','NA',						
			 		date.newinstance(oppty.First_Billing_Date__c.year(), oppty.First_Billing_Date__c.month(), oppty.First_Billing_Date__c.day()) );

				/* Target Type = New Project Start calculation */
				if( targerActualMap.containsKey(identifierNPS) && !mapOfTargetandHasError.containsKey(identifierNPS)){
 
					Target_and_Actual__c tvav = targerActualMap.get(identifierNPS);
					try{
						if( tvav.Calculate_Actuals__c ){
							tvav.Actual__c = tvav.Actual__c + 1;
						}

						if( tvav.Calculate_Point_Scored__c ){
							tvav.Points_Scored__c = tvav.Points_Eligible__c * tvav.Actual__c/ tvav.Target__c;
						}

						// Blank the error message field
						tvav.Error_Message__c = '';

					}catch(Exception e){
						tvav.Error_Message__c = 'Error : '+e.getMessage();
						tvav.Actual__c = 0;
						tvav.Points_Scored__c = 0;
						mapOfTargetandHasError.put(identifierNPS,true);
					}

					targerActualMap.put(identifierNPS,tvav);
				}
			} 

			if( oppty.Jumbo_Resizing_Completed__c != null ){
				String identifierJR = getIdentifier( oppty.Sales_Area__c,'Jumbo Resizing','NA',						
			 		date.newinstance(oppty.Jumbo_Resizing_Completed__c.year(), oppty.Jumbo_Resizing_Completed__c.month(), oppty.Jumbo_Resizing_Completed__c.day()) );

				/* Target Type = Jumbo Resizing calculation */
				if( targerActualMap.containsKey(identifierJR) && !mapOfTargetandHasError.containsKey(identifierJR) ){

					Target_and_Actual__c tvav = targerActualMap.get(identifierJR);
					try{
						if( tvav.Calculate_Actuals__c ){
							tvav.Actual__c = tvav.Actual__c + 1;
						}

						// Blank the error message field
						tvav.Error_Message__c = '';

					}catch(Exception e){
						tvav.Error_Message__c = 'Error : '+e.getMessage();
						tvav.Actual__c = 0;
						tvav.Points_Scored__c = 0;
						mapOfTargetandHasError.put(identifierJR,true);
					}

					targerActualMap.put(identifierJR,tvav);
				}
			}

			if( oppty.Glass_Pro_Live_Completed__c != null ){
				String identifierGPL = getIdentifier( oppty.Sales_Area__c,'Glass Pro Live','NA',						
			 		date.newinstance(oppty.Glass_Pro_Live_Completed__c.year(), oppty.Glass_Pro_Live_Completed__c.month(), oppty.Glass_Pro_Live_Completed__c.day()) );

				/* Target Type = Glass Pro Live calculation */
				if( targerActualMap.containsKey(identifierGPL) && !mapOfTargetandHasError.containsKey(identifierGPL) ){

					Target_and_Actual__c tvav = targerActualMap.get(identifierGPL);
					try{
						if( tvav.Calculate_Actuals__c ){
							tvav.Actual__c = tvav.Actual__c + 1;
						}

						// Blank the error message field
						tvav.Error_Message__c = '';

					}catch(Exception e){
						tvav.Error_Message__c = 'Error : '+e.getMessage();
						tvav.Actual__c = 0;
						tvav.Points_Scored__c = 0;
						mapOfTargetandHasError.put(identifierGPL,true);
					}

					targerActualMap.put(identifierGPL,tvav);
				}
			}

		}
			

		return targerActualMap;
	}


	/* This method is used to calculate No. of KAM’s Campaign records where Status is completed */
	/* 
	    Target Type = Influencer Engagement Activities
		Product Range = NA
	 */
	public Map<String,Target_and_Actual__c> calculateInfluencerEngagementActivitiesActuals(List<Campaign> campaignLst,Map<String,Target_and_Actual__c> targerActualMap){

		Map<String,Boolean> mapOfTargetandHasError = new Map<String,Boolean>();
		for(Campaign camp : campaignLst){ 

			String identifier = getIdentifier( camp.Sales_Area__c,'Influencer Engagement Activities','NA',						
			 		date.newinstance(camp.EndDate.year(), camp.EndDate.month(), camp.EndDate.day()) );

			if( targerActualMap.containsKey(identifier) && !mapOfTargetandHasError.containsKey(identifier)){

				Target_and_Actual__c tvav = targerActualMap.get(identifier);
		        try{

			        if( tvav.Calculate_Actuals__c ){
						tvav.Actual__c = tvav.Actual__c + 1;
					}

					if( tvav.Calculate_Point_Scored__c ){
						tvav.Points_Scored__c = tvav.Points_Eligible__c * tvav.Actual__c/ tvav.Target__c;
					}

					// Blank the error message field
						tvav.Error_Message__c = '';

				}catch(Exception e){
						tvav.Error_Message__c = 'Error : '+e.getMessage();
						tvav.Actual__c = 0;
						tvav.Points_Scored__c = 0;
						mapOfTargetandHasError.put(identifier,true);
				}

				targerActualMap.put(identifier,tvav);
			}

			/* Target Type = Edge Plus Meet calculation */
			if( camp.Type == 'Edge Meet' ){

				String identifierEPM = getIdentifier( camp.Sales_Area__c,'Edge Meet','NA',						
			 		date.newinstance(camp.EndDate.year(), camp.EndDate.month(), camp.EndDate.day()) );

					if( targerActualMap.containsKey(identifierEPM) && !mapOfTargetandHasError.containsKey(identifierEPM)){

					Target_and_Actual__c tvav = targerActualMap.get(identifierEPM);
			        try{

				        if( tvav.Calculate_Actuals__c ){
							tvav.Actual__c = tvav.Actual__c + 1;
						}

						// Blank the error message field
							tvav.Error_Message__c = '';

					}catch(Exception e){
							tvav.Error_Message__c = 'Error : '+e.getMessage();
							tvav.Actual__c = 0;
							tvav.Points_Scored__c = 0;
							mapOfTargetandHasError.put(identifierEPM,true);
					}

					targerActualMap.put(identifierEPM,tvav);
				}

			}

			/* Target Type = Windowmakers' Meet calculation */
			if( camp.Type == 'Windowmakers\' Meet' ){

				String identifierWM = getIdentifier( camp.Sales_Area__c,'Windowmakers\' Meet','NA',						
			 		date.newinstance(camp.EndDate.year(), camp.EndDate.month(), camp.EndDate.day()) );

					if( targerActualMap.containsKey(identifierWM) && !mapOfTargetandHasError.containsKey(identifierWM)){

					Target_and_Actual__c tvav = targerActualMap.get(identifierWM);
			        try{

				        if( tvav.Calculate_Actuals__c ){
							tvav.Actual__c = tvav.Actual__c + 1;
						}

						// Blank the error message field
							tvav.Error_Message__c = '';

					}catch(Exception e){
							tvav.Error_Message__c = 'Error : '+e.getMessage();
							tvav.Actual__c = 0;
							tvav.Points_Scored__c = 0;
							mapOfTargetandHasError.put(identifierWM,true);
					}

					targerActualMap.put(identifierWM,tvav);
				}

			}


		} 

		return targerActualMap;
	}

	/* This method is used to calculate No. of KAM’s Plant Visit records where Approval Status is Approved */
	/* 
	    Target Type = I -2 Visits
		Product Range = NA
	 */
	public Map<String,Target_and_Actual__c> calculateI2VisitActuals(List<Plant_Visit__c> plantLst,Map<String,Target_and_Actual__c> targerActualMap){

		Map<String,Boolean> mapOfTargetandHasError = new Map<String,Boolean>();
		for(Plant_Visit__c plant : plantLst){ 

			String identifier = getIdentifier( plant.Opportunity__r.Sales_Area__c,'I -2 Visits','NA',						
			 		date.newinstance(plant.Visit_Date__c.year(), plant.Visit_Date__c.month(), plant.Visit_Date__c.day()) );

			if( targerActualMap.containsKey(identifier) && !mapOfTargetandHasError.containsKey(identifier)){

				Target_and_Actual__c tvav = targerActualMap.get(identifier);
		        try{

			        if( tvav.Calculate_Actuals__c ){
						tvav.Actual__c = tvav.Actual__c + 1;
					}

					// Blank the error message field
						tvav.Error_Message__c = '';

				}catch(Exception e){
						tvav.Error_Message__c = 'Error : '+e.getMessage();
						tvav.Actual__c = 0;
						tvav.Points_Scored__c = 0;
						mapOfTargetandHasError.put(identifier,true);
				}

				targerActualMap.put(identifier,tvav);
			}

			
		} 

		return targerActualMap;
	}


	/* This method is used to calculate No. of KAM’s Event records */
	/* 
	    Target Type = Mr. Build under Opportunity
		Product Range = NA

		Target Type = Blinds that Blind
		Product Range = NA

	 */
	public Map<String,Target_and_Actual__c> calculateEventActuals(List<Event> eventLst,Map<String,Target_and_Actual__c> targerActualMap){

		Map<String,Boolean> mapOfTargetandHasError = new Map<String,Boolean>();
		for(Event evnt : eventLst){ 

			if( evnt.Type == 'Mr. Build- Joint Marketing' &&  evnt.Related_To_Type__c =='Opportunity' ){

				String identifierBuild = getIdentifier( evnt.Sales_Area__c,'Mr. Build','NA',						
			 		date.newinstance(evnt.StartDateTime.year(), evnt.StartDateTime.month(), evnt.StartDateTime.day()) );

					if( targerActualMap.containsKey(identifierBuild) && !mapOfTargetandHasError.containsKey(identifierBuild)){

						Target_and_Actual__c tvav = targerActualMap.get(identifierBuild);
				        try{

					        if( tvav.Calculate_Actuals__c ){
								tvav.Actual__c = tvav.Actual__c + 1;
							}

							// Blank the error message field
								tvav.Error_Message__c = '';

						}catch(Exception e){
								tvav.Error_Message__c = 'Error : '+e.getMessage();
								tvav.Actual__c = 0;
								tvav.Points_Scored__c = 0;
								mapOfTargetandHasError.put(identifierBuild,true);
						}

						targerActualMap.put(identifierBuild,tvav);
					}

			}


			if( evnt.Type == 'Blinds that Blind' ){

				String identifierBlind = getIdentifier( evnt.Sales_Area__c,'Blinds that Blind','NA',						
			 		date.newinstance(evnt.StartDateTime.year(), evnt.StartDateTime.month(), evnt.StartDateTime.day()) );

					if( targerActualMap.containsKey(identifierBlind) && !mapOfTargetandHasError.containsKey(identifierBlind)){

						Target_and_Actual__c tvav = targerActualMap.get(identifierBlind);
				        try{

					        if( tvav.Calculate_Actuals__c ){
								tvav.Actual__c = tvav.Actual__c + 1;
							}

							// Blank the error message field
								tvav.Error_Message__c = '';

						}catch(Exception e){
								tvav.Error_Message__c = 'Error : '+e.getMessage();
								tvav.Actual__c = 0;
								tvav.Points_Scored__c = 0;
								mapOfTargetandHasError.put(identifierBlind,true);
						}

						targerActualMap.put(identifierBlind,tvav);
					}

			}

		} 

		return targerActualMap;
	}


	/* This method is used to calculate No. of KAM’s Call logs records under Tasks */
	/* 
	    Target Type = Call logs
		Product Range = NA
	 */
	public Map<String,Target_and_Actual__c> calculateTaskActuals(List<Task> taskLst,Map<String,Target_and_Actual__c> targerActualMap){

		Map<String,Boolean> mapOfTargetandHasError = new Map<String,Boolean>();
		for(Task tsk : taskLst){ 
			String sub = tsk.Subject;
			if( sub.contains('Call')){

				String identifierCall = getIdentifier( tsk.Sales_Area__c,'Call logs','NA',						
			 		date.newinstance(tsk.Custom_Due_Date__c.year(), tsk.Custom_Due_Date__c.month(), tsk.Custom_Due_Date__c.day()) );

				if( targerActualMap.containsKey(identifierCall) && !mapOfTargetandHasError.containsKey(identifierCall)){

					Target_and_Actual__c tvav = targerActualMap.get(identifierCall);
			        try{

				        if( tvav.Calculate_Actuals__c ){
							tvav.Actual__c = tvav.Actual__c + 1;
						}

						// Blank the error message field
							tvav.Error_Message__c = '';

					}catch(Exception e){
							tvav.Error_Message__c = 'Error : '+e.getMessage();
							tvav.Actual__c = 0;
							tvav.Points_Scored__c = 0;
							mapOfTargetandHasError.put(identifierCall,true);
					}

					targerActualMap.put(identifierCall,tvav);
				}

			}

		} 

		return targerActualMap;
	}



	/* This method is used to calculate No. of KAM’s contacts records */
	/* 
	    Target Type = Contacts added
		Product Range = NA
	 */
	public Map<String,Target_and_Actual__c> calculateContactActuals(List<Contact> contactLst,Map<String,Target_and_Actual__c> targerActualMap){

		Map<String,Boolean> mapOfTargetandHasError = new Map<String,Boolean>();
		for(Contact con : contactLst){ 

				String identifierCon = getIdentifier( con.Owner.Sales_Area__c,'Contacts added','NA',						
			 		date.newinstance(con.CreatedDate.year(), con.CreatedDate.month(), con.CreatedDate.day()) );

				if( targerActualMap.containsKey(identifierCon) && !mapOfTargetandHasError.containsKey(identifierCon)){

					Target_and_Actual__c tvav = targerActualMap.get(identifierCon);
			        try{

				        if( tvav.Calculate_Actuals__c ){
							tvav.Actual__c = tvav.Actual__c + 1;
						}

						// Blank the error message field
							tvav.Error_Message__c = '';

					}catch(Exception e){
							tvav.Error_Message__c = 'Error : '+e.getMessage();
							tvav.Actual__c = 0;
							tvav.Points_Scored__c = 0;
							mapOfTargetandHasError.put(identifierCon,true);
					}

					targerActualMap.put(identifierCon,tvav);
				}
		} 

		return targerActualMap;
	}


	/* This method is used to calculate No. of KAM’s Email integration */
	/* 
	    Target Type = E-mail integration
		Product Range = NA
	 */
	public Map<String,Target_and_Actual__c> calculateEmailActuals(List<EmailMessage> emailLst,Map<String,Target_and_Actual__c> targerActualMap){

		Map<String,Boolean> mapOfTargetandHasError = new Map<String,Boolean>();
		for(EmailMessage emailMsg : emailLst){ 

				String identifierEmailMsg = getIdentifier( emailMsg.CreatedBy.Sales_Area__c,'E-mail integration','NA',						
			 		date.newinstance(emailMsg.MessageDate.year(), emailMsg.MessageDate.month(), emailMsg.MessageDate.day()) );

				if( targerActualMap.containsKey(identifierEmailMsg) && !mapOfTargetandHasError.containsKey(identifierEmailMsg)){

					Target_and_Actual__c tvav = targerActualMap.get(identifierEmailMsg);
			        try{

				        if( tvav.Calculate_Actuals__c ){
							tvav.Actual__c = tvav.Actual__c + 1;
						}

						// Blank the error message field
							tvav.Error_Message__c = '';

					}catch(Exception e){
							tvav.Error_Message__c = 'Error : '+e.getMessage();
							tvav.Actual__c = 0;
							tvav.Points_Scored__c = 0;
							mapOfTargetandHasError.put(identifierEmailMsg,true);
					}

					targerActualMap.put(identifierEmailMsg,tvav);
				}
		} 

		return targerActualMap;
	}


	public String getIdentifier(String salesArea,String targetType,String prodRange,Date recordDate){
			String identifier = salesArea + '-' + targetType + '-' + prodRange + '-' +  getMonthAggregrateName( recordDate.month() ) + ',' + recordDate.year() ;
			return identifier;
	}

	public String getMonthAggregrateName(Integer monthNum){
		
			if(monthNum == 1){
                return 'JAN';
            }else if(monthNum == 2){
                return 'FEB';
            }
            else if(monthNum == 3){
                return 'MAR';
            }
            else if(monthNum == 4){
                return 'APR';
            }
            else if(monthNum == 5){
                return 'MAY';
            }
            else if(monthNum == 6){
                return 'JUN';
            }
            else if(monthNum == 7){
                return 'JUL';
            }
            else if(monthNum == 8){
                return 'AUG';
            }
            else if(monthNum == 9){
                return 'SEPT';
            }   
            else if(monthNum == 10){
                return 'OCT';
            }
            else if(monthNum == 11){
                return 'NOV';
            }
            else if(monthNum == 12){
                return 'DEC';
            }
        	else{
            	return 'blank';
        	}
	}


}
/* Created By         : Rajiv Kumar Singh
   Created Date       : 11/06/2018
   Class Name         : UpdateLookupOfEnquiry
   Description        : update the particular Account record lookup field to Enquiry
   Last Modified By   : Rajiv Kumar Singh
   Last Modified Date : 11/06/2018
*/
public class UpdateLookupOfEnquiry{
  @InvocableMethod(label='UpdateLookupOfEnquiry' description='update the particular Account record lookup field to Enquiry')
  public static void UpdateLookupValue(List<Id> enquiryIds)
    {   
       map<string,string> enquiryWithAccountMap = new map<string,string>();  
       set<string> presentSAPCodesSet = new set<String>();
       list<Enquiry__c > NewList = new list<Enquiry__c>(); 
       list<Enquiry__c > FinalListToUpdate = new list<Enquiry__c>();

       for(Enquiry__c ptp:[select id,Bill_to_customer__c,SAP_Code__c from Enquiry__c where id in:enquiryIds])
       {
         if(ptp.SAP_Code__c != null)
          {
            presentSAPCodesSet.add(ptp.SAP_Code__c);      
            NewList.add(ptp);
            system.debug('The ptp.id is: ' +ptp.id);
          }     
       }
       system.debug('The NewList is: ' +NewList+'......'+presentSAPCodesSet);
       if(presentSAPCodesSet.size()>0)
       {
          for(Account acc:[select id,SAP_Code__c  from Account where SAP_Code__c in:presentSAPCodesSet])
           {
               enquiryWithAccountMap.put(acc.SAP_Code__c,acc.id);
           }
           system.debug('The enquiryWithAccountMap is: ' +enquiryWithAccountMap);
           for(Enquiry__c ptp:NewList)
           {
               ptp.Bill_to_customer__c = enquiryWithAccountMap.get(ptp.SAP_Code__c);
               system.debug('The  ptp.Bill_to_customer__c is: ' + ptp.Bill_to_customer__c);
               FinalListToUpdate.add(ptp);
           }
       }
       if(!FinalListToUpdate.isEmpty() && FinalListToUpdate.size() > 0) {
            update FinalListToUpdate;
       }
    }
}
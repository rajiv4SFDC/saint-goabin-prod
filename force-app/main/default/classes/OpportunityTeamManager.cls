public class OpportunityTeamManager {

	public void checkRecalculateCheckboxOnOpptyInsert(List<Opportunity> lstOfOppty ){

		for(Opportunity oppty : lstOfOppty){
			oppty.Re_Calculate__c = true;
		}
	}

	public void checkRecalculateCheckboxOnOpptyUpdate(List<Opportunity> lstOfOppty,Map<Id,Opportunity> opptyOldMap ){

		for(Opportunity oppty : lstOfOppty){
			if( oppty.OwnerId != opptyOldMap.get(oppty.Id).OwnerId ){
				system.debug('owner is chnageddd');
				oppty.Re_Calculate__c = true;
			}
		}
	}

	public void checkRecalculateCheckboxOnOpptyFrmTeamOnInsert(List<OpportunityTeamMember> lstOfOpptyTeamMem){
		List<Opportunity> lstOfOppty = new List<Opportunity>();
		Map<ID,Boolean> chkForUniqueOppty = new Map<Id,Boolean>();

		for(OpportunityTeamMember opptyMember : lstOfOpptyTeamMem){
			
			if( !chkForUniqueOppty.containsKey( opptyMember.OpportunityId ) ){
				Opportunity oppty = new Opportunity();
				oppty.Id = opptyMember.OpportunityId;
				oppty.Re_Calculate__c = true;

				lstOfOppty.add(oppty);

				chkForUniqueOppty.put(opptyMember.OpportunityId,true);
			}
		}

		if( lstOfOppty.size() > 0 ){
			system.debug('checkRecalculateCheckboxOnOpptyFrmTeamOnInsert hell no');
			update lstOfOppty;
		}
	}
	/*
	public void checkRecalculateCheckboxOnOpptyFrmProdForecast(List<Product_Forecast__c> lstOfProdFore){
		Map<Id,Boolean> mapOFOpptyId = new Map<Id,Boolean>();
		List<Opportunity> lstOfOppty = new List<Opportunity>();

		for(Product_Forecast__c pf : lstOfProdFore){

			if( !mapOFOpptyId.containsKey(pf.Opportunity_ID__c) ){

				Opportunity oppty = new Opportunity();
				oppty.Id = pf.Opportunity_ID__c;
				oppty.Re_Calculate__c = true;
				lstOfOppty.add(oppty);

				mapOFOpptyId.put(pf.Opportunity_ID__c,true);
			}
		}

		if( lstOfOppty.size() > 0 ){
			update lstOfOppty;
		}
	}
	*/

	public void checkRecalculateCheckboxOnOpptyFrmTeamOnUpdate(List<OpportunityTeamMember> lstOfOpptyTeamMem,
																Map<Id,OpportunityTeamMember> opptyTeamOldMap){
		List<Opportunity> lstOfOppty = new List<Opportunity>();
		Map<ID,Boolean> chkForUniqueOppty = new Map<Id,Boolean>();

		for(OpportunityTeamMember opptyMember : lstOfOpptyTeamMem){

			if( opptyMember.Share_pct__c != opptyTeamOldMap.get(opptyMember.Id).Share_pct__c &&
									!chkForUniqueOppty.containsKey( opptyMember.OpportunityId )  ){
				Opportunity oppty = new Opportunity();
				oppty.Id = opptyMember.OpportunityId;
				oppty.Re_Calculate__c = true;

				lstOfOppty.add(oppty);

				chkForUniqueOppty.put(opptyMember.OpportunityId,true);
			}
		}

		if( lstOfOppty.size() > 0 ){
			update lstOfOppty;
		}
	}

	public void addOpportunityOwnerAsOpptyTeamMember(List<Opportunity> lstOfOppty ){

		List<OpportunityTeamMember> lstOfOpptyTeamMem = new List<OpportunityTeamMember>();

		for(Opportunity oppty: lstOfOppty){
			OpportunityTeamMember member = new OpportunityTeamMember();  
			member.OpportunityId = oppty.Id;  
			member.UserId = oppty.OwnerId;  
			member.TeamMemberRole = 'Opportunity Owner';  
			member.OpportunityAccessLevel = 'Edit';
			member.Share_pct__c = 100;
			//member.Total_Estimated_Quantity_Sq_M__c = oppty.Total_Estimated_Quantity_Sq_M__c;
			lstOfOpptyTeamMem.add(member);  
		}

		if( lstOfOpptyTeamMem.size()>0 ){
			TriggerHandler.bypass('OpportunityTeamTriggerHandler');
			insert lstOfOpptyTeamMem;
			TriggerHandler.clearAllBypasses();
		}

	}

	public void updateOpportunityOwnerAsOpptyTeamMember(List<Opportunity> lstOfOppty,Map<Id,Opportunity> opptyOldMap ){

		List<OpportunityTeamMember> lstOfOpptyTeamMemToBeAdded = new List<OpportunityTeamMember>();
		List<OpportunityTeamMember> lstOfOpptyTeamMem = [SELECT id ,OpportunityId,UserId,
														Share_pct__c
														FROM OpportunityTeamMember 
														WHERE OpportunityId IN : lstOfOppty];
		List<OpportunityTeamMember> lstOfTeamMemToDel = new List<OpportunityTeamMember>();												
		Boolean teamHasDelete = false;
		//Boolean teamHasUpdate = false;

		for(Opportunity oppty: lstOfOppty){
			if( oppty.OwnerId != opptyOldMap.get(oppty.Id).OwnerId ){
				
				OpportunityTeamMember member = new OpportunityTeamMember();  
				member.OpportunityId = oppty.Id;  
				member.UserId = oppty.OwnerId;  
				member.TeamMemberRole = 'Opportunity Owner';  
				member.OpportunityAccessLevel = 'Edit'; 
				
					for(OpportunityTeamMember opptyMem : lstOfOpptyTeamMem){

						if( opptyMem.UserId == opptyOldMap.get(oppty.Id).OwnerId && opptyMem.OpportunityId == oppty.id ){
							member.Share_pct__c = opptyMem.Share_pct__c;
							lstOfTeamMemToDel.add(opptyMem);
							teamHasDelete = true;
						}
					}

				lstOfOpptyTeamMemToBeAdded.add(member);	
			}/*
			else if( oppty.Total_Estimated_Quantity_Sq_M__c != opptyOldMap.get(oppty.Id).Total_Estimated_Quantity_Sq_M__c ||
				oppty.Total_Appropriated_Quantity__c != opptyOldMap.get(oppty.Id).Total_Appropriated_Quantity__c){

				for(OpportunityTeamMember opptyMem : lstOfOpptyTeamMem){
					teamHasUpdate = true; 
					opptyMem.Total_Estimated_Quantity_Sq_M__c = oppty.Total_Estimated_Quantity_Sq_M__c;
					opptyMem.Total_Appropriated_Quantity_Sq_M__c = oppty.Total_Appropriated_Quantity__c;
				}
			}*/
		}
		/*
		if( lstOfOpptyTeamMem.size()>0 && teamHasUpdate){
			update lstOfOpptyTeamMem;
		}*/

		if( lstOfTeamMemToDel.size()>0 && teamHasDelete){
			TriggerHandler.bypass('OpportunityTeamTriggerHandler');
			delete lstOfTeamMemToDel;
		}
		

		if( lstOfOpptyTeamMemToBeAdded.size()>0 ){
			
			insert lstOfOpptyTeamMemToBeAdded;
			TriggerHandler.clearAllBypasses();
		}

	}

	public Boolean checkSharingPctAmongTeamMemberOnInsert(List<OpportunityTeamMember> lstOfOpptyTeamMem){

		Map<Id,List<OpportunityTeamMember>> mapOfOpptyAndTeamMember = new Map<Id,List<OpportunityTeamMember>>();
		Map<Id,List<OpportunityTeamMember>> mapOfOpptyAndExisitingTeamMember = new Map<Id,List<OpportunityTeamMember>>();
		Boolean hasError = false;
			
			for(OpportunityTeamMember otm: lstOfOpptyTeamMem){

				if( mapOfOpptyAndTeamMember.get(otm.OpportunityId) == null ){
					mapOfOpptyAndTeamMember.put(otm.OpportunityId,new List<OpportunityTeamMember>{otm});
				}else{
					List<OpportunityTeamMember> otmLst = mapOfOpptyAndTeamMember.get(otm.OpportunityId);
					otmLst.add(otm);
					mapOfOpptyAndTeamMember.put(otm.OpportunityId,otmLst);
				}
			}

		 List<OpportunityTeamMember> lstOfOpptyAndExisitingTeamMember=[SELECT id,Share_pct__c,OpportunityId
																		 FROM OpportunityTeamMember 
																		 WHERE OpportunityId IN : mapOfOpptyAndTeamMember.keySet()];	

			for(OpportunityTeamMember otm: lstOfOpptyAndExisitingTeamMember){

				if( mapOfOpptyAndExisitingTeamMember.get(otm.OpportunityId) == null ){
					mapOfOpptyAndExisitingTeamMember.put(otm.OpportunityId,new List<OpportunityTeamMember>{otm});
				}else{
					List<OpportunityTeamMember> otmLst = mapOfOpptyAndExisitingTeamMember.get(otm.OpportunityId);
					otmLst.add(otm);
					mapOfOpptyAndExisitingTeamMember.put(otm.OpportunityId,otmLst);
				}
			}

			for( Id opptyId: mapOfOpptyAndTeamMember.keySet()){
				Decimal shareSum = 0;
				for( OpportunityTeamMember opptyMember: mapOfOpptyAndTeamMember.get(opptyId)){
					shareSum = shareSum + opptyMember.Share_pct__c;
				}

				if( mapOfOpptyAndExisitingTeamMember.size() > 0 ){
					if( mapOfOpptyAndExisitingTeamMember.get(opptyId) !=null ){
							for( OpportunityTeamMember opptyMember: mapOfOpptyAndExisitingTeamMember.get(opptyId)){
							shareSum = shareSum + opptyMember.Share_pct__c;
						}	
					}
					
				}

				if( shareSum > 100 ){
					hasError = true;
						if( mapOfOpptyAndTeamMember.get(opptyId) !=null ){
							for( OpportunityTeamMember opptyMember: mapOfOpptyAndTeamMember.get(opptyId)){
							opptyMember.addError('Sum of Sharing % should not be greater than 100');
						}
					}
				}
			}


		return !hasError ;			
	}

	public Boolean checkSharingPctAmongTeamMemberOnUpdate(List<OpportunityTeamMember> lstOfOpptyTeamMem){

		Map<Id,List<OpportunityTeamMember>> mapOfOpptyAndTeamMember = new Map<Id,List<OpportunityTeamMember>>();
		Map<Id,List<OpportunityTeamMember>> mapOfOpptyAndExisitingTeamMember = new Map<Id,List<OpportunityTeamMember>>();
		Boolean hasError = false;
			
			for(OpportunityTeamMember otm: lstOfOpptyTeamMem){

				if( mapOfOpptyAndTeamMember.get(otm.OpportunityId) == null ){
					mapOfOpptyAndTeamMember.put(otm.OpportunityId,new List<OpportunityTeamMember>{otm});
				}else{
					List<OpportunityTeamMember> otmLst = mapOfOpptyAndTeamMember.get(otm.OpportunityId);
					otmLst.add(otm);
					mapOfOpptyAndTeamMember.put(otm.OpportunityId,otmLst);
				}
			}

		 List<OpportunityTeamMember> lstOfOpptyAndExisitingTeamMember=[SELECT id,Share_pct__c,OpportunityId
																		 FROM OpportunityTeamMember 
																		 WHERE OpportunityId IN : mapOfOpptyAndTeamMember.keySet()
																		 AND ID NOT IN : lstOfOpptyTeamMem];	

			for(OpportunityTeamMember otm: lstOfOpptyAndExisitingTeamMember){

				if( mapOfOpptyAndExisitingTeamMember.get(otm.OpportunityId) == null ){
					mapOfOpptyAndExisitingTeamMember.put(otm.OpportunityId,new List<OpportunityTeamMember>{otm});
				}else{
					List<OpportunityTeamMember> otmLst = mapOfOpptyAndExisitingTeamMember.get(otm.OpportunityId);
					otmLst.add(otm);
					mapOfOpptyAndExisitingTeamMember.put(otm.OpportunityId,otmLst);
				}
			}

			for( Id opptyId: mapOfOpptyAndTeamMember.keySet()){
				Decimal shareSum = 0;
				for( OpportunityTeamMember opptyMember: mapOfOpptyAndTeamMember.get(opptyId)){
					shareSum = shareSum + opptyMember.Share_pct__c;
				}

				if( mapOfOpptyAndExisitingTeamMember.size() > 0 ){
					for( OpportunityTeamMember opptyMember: mapOfOpptyAndExisitingTeamMember.get(opptyId)){
						shareSum = shareSum + opptyMember.Share_pct__c;
					}	
				}

				if( shareSum > 100 ){
					hasError = true;
					for( OpportunityTeamMember opptyMember: mapOfOpptyAndTeamMember.get(opptyId)){
						opptyMember.addError('Sum of Sharing % should not be greater than 100');
					}
				}
			}


		return !hasError ;			
	}

	/*
	public void populateOpportunityRelatedDetailOnTeam(List<OpportunityTeamMember> lstOfOpptyTeamMem){

		Map<Id,List<OpportunityTeamMember>> mapOfTeamMember = new Map<Id,List<OpportunityTeamMember>>();

		for(OpportunityTeamMember otm : lstOfOpptyTeamMem){
			
			if( mapOfTeamMember.get(otm.OpportunityId) == null ){
				mapOfTeamMember.put(otm.OpportunityId,new List<OpportunityTeamMember>{otm});
			}else{
				List<OpportunityTeamMember> LstOfOptyMem =  mapOfTeamMember.get(otm.OpportunityId);
				LstOfOptyMem.add(otm);
				mapOfTeamMember.put(otm.OpportunityId,LstOfOptyMem);
			}
			
		}

		List<Opportunity> lstOfOppty = [SELECT id,Total_Estimated_Quantity_Sq_M__c
										 FROM Opportunity WHERE Id IN : mapOfTeamMember.keySet()];

		for( Opportunity oppty : lstOfOppty){

			for( OpportunityTeamMember otm : mapOfTeamMember.get(oppty.Id)){
				otm.Total_Estimated_Quantity_Sq_M__c = oppty.Total_Estimated_Quantity_Sq_M__c;
			}
		}	
	}
	*/
	/*
	public void createOpportunityProductTeamMember(List<OpportunityTeamMember> lstOfOpptyTeamMem){

		Set<Id> setOfOpptyIds = new Set<Id>();
		List<Opportunity_Product_Team__c> lstOfOpptyProdTeam = new List<Opportunity_Product_Team__c>();

		for(OpportunityTeamMember otm : lstOfOpptyTeamMem){
			setOfOpptyIds.add(otm.OpportunityId);
		}


		List<Product_Forecast__c> lstOfProdFore = [SELECT id,Opportunity_ID__c FROM Product_Forecast__c 
														WHERE  Opportunity_ID__c IN : setOfOpptyIds];

			for(Product_Forecast__c opptyProdFore: lstOfProdFore){

				for(OpportunityTeamMember otm : lstOfOpptyTeamMem){

					if( otm.OpportunityId == opptyProdFore.Opportunity_ID__c ){
						Opportunity_Product_Team__c opt = new Opportunity_Product_Team__c();
						opt.Product_Forecast__c = opptyProdFore.id;
						opt.Share_pct__c = otm.Share_pct__c;
						opt.Opportunity_Team_Id__c = otm.id;
						opt.TeamMemberRole__c = otm.TeamMemberRole;
						opt.User__c = otm.UserId ;

						lstOfOpptyProdTeam.add(opt);
					}
				}
			}
			system.debug('after team insert  :: '+ lstOfOpptyProdTeam);
			system.debug('after team insert oppty set  :: '+ setOfOpptyIds);
			system.debug('after team insert fore set  :: '+ lstOfProdFore);
			
		
		if( lstOfOpptyProdTeam.size() > 0 ){
			insert lstOfOpptyProdTeam;
		}												
	}
	*/
	
	public void createOpptyProdTeamMemberAfterProdForeInserted(List<Product_Forecast__c> lstOfProdFore){

		Set<Id> setOfOpptyIds = new Set<Id>();
		Map<id,List<OpportunityTeamMember>> mapOfOpptyIdAndOpptyTeam = new Map<Id,List<OpportunityTeamMember>>();
		List<Opportunity_Product_Team__c> lstOfOpptyProdTeam = new List<Opportunity_Product_Team__c>();

		for(Product_Forecast__c opptyProdFore : lstOfProdFore){
			if( !opptyProdFore.Re_Calculate__c ){
				setOfOpptyIds.add(opptyProdFore.Opportunity_ID__c);
			}
		}

		if( setOfOpptyIds.size() > 0 ){
			List<OpportunityTeamMember> lstOfOpptyTeamMem=[SELECT id,OpportunityId,Share_pct__c,
														UserId,TeamMemberRole
							 							FROM OpportunityTeamMember 
							 							WHERE OpportunityId IN :setOfOpptyIds];

			for(OpportunityTeamMember otm: lstOfOpptyTeamMem){

				if( mapOfOpptyIdAndOpptyTeam.get(otm.OpportunityId) == null  ){
					mapOfOpptyIdAndOpptyTeam.put(otm.OpportunityId,new List<OpportunityTeamMember>{otm});
				}else{
					List<OpportunityTeamMember> otmLst = mapOfOpptyIdAndOpptyTeam.get(otm.OpportunityId);
					otmLst.add(otm);
					mapOfOpptyIdAndOpptyTeam.put(otm.OpportunityId,otmLst);
				}
			}

			if( mapOfOpptyIdAndOpptyTeam.size() >0 ){		
				for(Product_Forecast__c opptyProdFore : lstOfProdFore){
					
					for( OpportunityTeamMember otm : mapOfOpptyIdAndOpptyTeam.get(opptyProdFore.Opportunity_ID__c)){
						Opportunity_Product_Team__c opt = new Opportunity_Product_Team__c();
						opt.Product_Forecast__c = opptyProdFore.id;
						opt.Opportunity_Team_Id__c = ''+otm.id;
						opt.Share_pct__c = otm.Share_pct__c;
						opt.User__c = otm.UserId;
						opt.TeamMemberRole__c = otm.TeamMemberRole;
						
						lstOfOpptyProdTeam.add(opt);
					}
				}
			}

			if( lstOfOpptyProdTeam.size() > 0 ){
				insert lstOfOpptyProdTeam;
			}				
		}			
	}
	
	/*
	public void updateOpportunityProductTeamMember(List<OpportunityTeamMember> lstOfOpptyTeamMem,Map<Id,OpportunityTeamMember> opptyTeamOldMap){

		List<String> lstOfOpptyTeamMemId = new List<String>();
		for( OpportunityTeamMember otm: lstOfOpptyTeamMem){

			if( otm.Share_pct__c != opptyTeamOldMap.get(otm.Id).Share_pct__c ){
				lstOfOpptyTeamMemId.add(''+otm.id);
			}
		}

		if( lstOfOpptyTeamMemId.size() > 0 ){
			List<Opportunity_Product_Team__c> lstOfOpptyProdTeam = [SELECT id,Opportunity_Team_Id__c,
																	Share_pct__c,TeamMemberRole__c
																	FROM Opportunity_Product_Team__c 
																	WHERE  Opportunity_Team_Id__c IN : lstOfOpptyTeamMemId];

				for(Opportunity_Product_Team__c opptyProdTeam: lstOfOpptyProdTeam){

					for(OpportunityTeamMember otm : lstOfOpptyTeamMem){

						if( opptyProdTeam.Opportunity_Team_Id__c == (''+otm.Id) ){
							opptyProdTeam.Share_pct__c = otm.Share_pct__c ;
							opptyProdTeam.User__c = otm.UserId ;
							opptyProdTeam.TeamMemberRole__c = otm.TeamMemberRole ;
						}
					}
				}

			if( lstOfOpptyProdTeam.size() > 0 ){
				update lstOfOpptyProdTeam;
			}		
		}
												
	}
	*/
	public void checkForOnwerBeingDelFrmOpptyTeamMember(List<OpportunityTeamMember> lstOfOpptyTeamMem){

		Map<Id,List<OpportunityTeamMember>> mapOfTeamMember = new Map<Id,List<OpportunityTeamMember>>();

		for(OpportunityTeamMember otm : lstOfOpptyTeamMem){
			
			if( mapOfTeamMember.get(otm.OpportunityId) == null ){
				mapOfTeamMember.put(otm.OpportunityId,new List<OpportunityTeamMember>{otm});
			}else{
				List<OpportunityTeamMember> LstOfOptyMem =  mapOfTeamMember.get(otm.OpportunityId);
				LstOfOptyMem.add(otm);
				mapOfTeamMember.put(otm.OpportunityId,LstOfOptyMem);
			}
		}

		List<Opportunity> lstOfOppty = [SELECT id ,OwnerId FROM Opportunity 
										WHERE id IN :mapOfTeamMember.keySet()];

		for(Opportunity oppty : lstOfOppty){
			
			for(OpportunityTeamMember otm : mapOfTeamMember.get(oppty.Id)){

				if(otm.UserId == oppty.OwnerId){
					otm.addError('Owner cannot be deleted from Team');
				}
			}
		}
	}
	
	/*
	public void deleteRelatedOpptyProductTeamMember(List<OpportunityTeamMember> lstOfOpptyTeamMem){

		Set<Id> setOFOpptyId = new Set<Id>();
		Set<Id> setOFUserId = new Set<Id>();

		for(OpportunityTeamMember otm : lstOfOpptyTeamMem){
			setOFOpptyId.add(otm.OpportunityId);
			setOFUserId.add(otm.UserId);
		}

		List<Opportunity_Product_Team__c> lstOfOpptyTeamMemToDel= [SELECT id,User__c,
																   Product_Forecast__r.Opportunity_Product__r.Opportunity__c
																   FROM Opportunity_Product_Team__c 
																   WHERE Product_Forecast__r.Opportunity_Product__r.Opportunity__c IN : setOFOpptyId 
																   AND User__c IN : setOFUserId] ;

		if( lstOfOpptyTeamMemToDel.size() >0 ){
			delete lstOfOpptyTeamMemToDel;
		}
	}
	*/

}
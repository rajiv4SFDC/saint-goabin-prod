/******************************************
* Created By  : Ajeet Singh Shekhawat
* Created On  :  Dec 2018
* Modified By : Ajeet Singh Shekhawat
* Modified On : Dec 2018
* Description :

******************************************/ 

@isTest
public class GeoTagCaptureForCheckIn_Test {
    static testMethod void captureGeoLocation() {
        
        Decimal loc1 = 12.9139927;
        Decimal loc2 = 77.6321624;
        List<Geo_Tagging__c> geotagList = new List<Geo_Tagging__c>();
        Geo_Tagging__c geoTag = new Geo_Tagging__c();
        geoTag.Start_My_Day__Latitude__s =  loc1;
        geoTag.Start_My_Day__Longitude__s =  loc2;
      //  geoTag.End_My_Day__latitude__s = 12.5081;
      //  geoTag.End_My_Day__longitude__s = 77.9476;      
        geotagList.add(geoTag);
        
        String geoTagg = JSON.serialize(geotagList);
        GeoTagCaptureForCheckIn.createGeoTagRecords(geoTagg);
        
        insert geotagList;
        geoTagg = JSON.serialize(geotagList);
        GeoTagCaptureForCheckIn.createGeoTagRecords(geoTagg);
        
        CheckIn_CheckOut__c checkInOut1 = new CheckIn_CheckOut__c();
        checkInOut1.CheckIn__Latitude__s = loc1;
        checkInOut1.CheckIn__Longitude__s =  loc2;
        checkInOut1.Geo_Tagging__c = geotagList[0].Id;
        insert checkInOut1;
        
		GeoTagCaptureForCheckIn.checkForStartMyDayRecords();
        
        List<CheckIn_CheckOut__c> checkInOutList = new List<CheckIn_CheckOut__c>();
        CheckIn_CheckOut__c checkInOut = new CheckIn_CheckOut__c();
        checkInOut.CheckOut__Latitude__s =  loc1;
        checkInOut.CheckOut__Longitude__s =  loc2;
        checkInOut.Geo_Tagging__c = geotagList[0].Id;
        checkInOut.Visit_Date__c = DateTime.now();
        checkInOut.Minutes_of_Meeting__c = '10';
        checkInOut.Location__c = 'HSR';
        checkInOut.Reason_of_Visit__c = 'Sale Product';
        checkInOut.Prospect__c = 'IBM';
        checkInOut.Id = checkInOut1.Id;
        checkInOutList.add(checkInOut);
        //Update checkInOutList;
        System.debug('555555' + checkInOutList);
        String checkInData = JSON.serialize(checkInOutList, true);
        System.debug('--> '+checkInData);
        
       
        GeoTagCaptureForCheckIn.createcheckInRecords(checkInData);
        
        //test
        GeoTagCaptureForCheckIn.createcheckOutRecords(checkInData);
        //GeoTagCaptureForCheckIn.createcheckOutRecords(checkInData);
        //test
        
        GeoTagCaptureForCheckIn.createEndDayRecords(checkInData);
        GeoTagCaptureForCheckIn.checkForStartMyDayRecords();
        GeoTagCaptureForCheckIn.getCheckInOutRecords();
        
    }
}
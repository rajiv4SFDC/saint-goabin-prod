/* Created By         : Rajiv Kumar Singh
   Created Date       : 15/06/2018
   Class Name         : UpdateCustomerLookupOfInvoice_Test
   Description        : 
   Last Modified By   : Rajiv Kumar Singh
   Last Modified Date : 15/06/2018
*/
@isTest
private class UpdateCustomerLookupOfInvoice_Test
{ 
 // @istest
   private testmethod static void testMethod1()
   {
      Account p1 = new Account();
      p1.Name = 'testing';
      p1.SAP_Code__c = '6565666';
	  p1.Sales_Area_Region__c = 'East';
      insert p1;
      
      Invoice__c ptp = new Invoice__c();
      ptp.Customer_code__c = '6565666';
      ptp.Customer__c = p1.id;
      insert ptp;
      
      Invoice__c  pt = [select id, Customer_code__c from Invoice__c  where id =:ptp.id];
      
      list<Id> ptrList = new list<Id>();
      ptrList.add(pt.id);
      
      UpdateCustomerLookupOfInvoice.UpdateLookupValue(ptrList);
   }
}
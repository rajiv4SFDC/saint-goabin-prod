public class LocationUtility {

    @AuraEnabled  
    // campare uom should be in meters
    public static String getNearByProjectsFrmDB(Decimal Lat,Decimal Lon,Decimal compare,String currentProjectId){
        compare /= 1000 ; 
        List<Project__c> lstProj = [SELECT id,Name,Geolocation__c,
                                    Project_Location__c,
                                    Sales_Area__c,Owner.Name,OwnerId,Project_Code__c
                                    FROM Project__c
                                    WHERE DISTANCE(Geolocation__c, GEOLOCATION(:Lat,:Lon), 'km') < :compare
                                    AND id != :currentProjectId Order By Sales_Area__c ];
        
        return JSON.serialize(lstProj);
    }

    @AuraEnabled  
    // campare uom should be in meters
    public static String setCapturedGeoLocation(Decimal Lat,Decimal Lon,String currentProjectId){
        
        try{
            Project__c proj= new Project__c();
            proj.Id = currentProjectId;
            proj.Geolocation__Latitude__s = Lat;
            proj.Geolocation__Longitude__s = Lon;

            update proj;
            
            return 'SUCCESS';
        }catch(Exception e){ return 'ERROR in updating Geo Location. ERROR : '+e;}
    }

    @AuraEnabled  
    // campare uom should be in meters
    public static String getCurrentProjectDetail(String currentProjectId){

        Project__c lstProj = [SELECT id,Name,Geolocation__Latitude__s,Geolocation__Longitude__s,
                                    Sales_Area__c,Validated__c
                                    FROM Project__c
                                    WHERE id = :currentProjectId LIMIT 1];
        
        return JSON.serialize(lstProj);
    }
}
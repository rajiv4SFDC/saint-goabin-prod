/*
 * InvoiceAppropriationHelper Class
 * Created Date : 9th June 2017
 * Helper methods required for invoice appropriation
 */ 
global class InvoiceAppropriationHelper {
    
    static List<Invoice__c> invoiceList_static;
    static Map<Integer, String> monthMap = new Map<Integer,String>{
        1 => 'JAN',
        2 => 'FEB',
        3 => 'MAR',
        4 => 'APR',
        5 => 'MAY',
        6 => 'JUN',
        7 => 'JUL',
        8 => 'AUG',
        9 => 'SEP',
        10 => 'OCT',
        11 => 'NOV',
        12 => 'DEC'
    };

    /*Process Invoice to Establish links with Project, Opportunity, Opportunity Product, Price Request & Product Forecast*/
    public static void processInvoice(List<Invoice__c> invoiceList){
        Set<String> transactionNoSet = new Set<String>();
        Set<String> productForecastExternalIdSet = new Set<String>();
        Map<Id, String> invoiceProductForecastMap = new Map<Id, String>();
        
        Map<Id, Invoice__c> invoiceMap = new Map<Id, Invoice__c>();

        //Pseudo Code
        //  Prepare list of Transaction No
        System.debug('Invoice List' + invoiceList);
        for(Invoice__c inv : invoiceList) {
            inv.Tagged__c = false; //Reset Tagging Flag, It will be set true once tagging process will be completed
            invoiceMap.put(inv.id,inv);
            if(!String.isBlank(inv.Transaction_No__c)) {
                transactionNoSet.add(inv.Transaction_No__c);
            } else {
                //Invoice doesn't has transaction No, it cannot be tagged to any Price Request or Opportunity Product in this case
                inv.Error_Message__c += 'Unable to tag to Price Request. Transaction Number is missing';
                inv.To_Be_Processed__c = false;
                System.debug('Invoice No ' + inv.Name + ' : Error ' + inv.Error_Message__c);
            }
        }

        //Fetch Price Request based on Transaction No Set
        Map<String, Price_Request__c> priceRequestMap = new Map<String, Price_Request__c>();
        //List<ConnectApi.BatchInput> batchInputs = new List<ConnectApi.BatchInput>();
        if(!transactionNoSet.isEmpty()) {
            for(Price_Request__c pr : queryPriceRequest(transactionNoSet)) {
                priceRequestMap.put(pr.Name, pr);   //Name on Price Request object is Transaction Number
            }

            //Tag invoices with transaction number
            for(Invoice__c inv : invoiceList) {
                if(!String.isBlank(inv.Transaction_No__c)) {
                    if(priceRequestMap.containsKey(inv.Transaction_No__c)) {
                        Price_Request__c pr = priceRequestMap.get(inv.Transaction_No__c);
                        //Tag Price Request, Opportunity Product, Opportunity, Project
                        inv.Price_Request__c = pr.id;                           
                        inv.Opportunity_Product__c = pr.Opportunity_Product__c;
                        if(pr.Opportunity_Product__c != null) {

                            //Compute Product Forecast External Id
                            if(inv.Billing_Date__c!=null) {
                                Integer month = inv.Billing_Date__c.month();
                                Integer year = inv.Billing_Date__c.year();
                                String externalID = pr.Opportunity_Product__c + '-' + month + '-' + year;
                                productForecastExternalIdSet.add(externalID);
                                invoiceProductForecastMap.put(inv.id, externalId);
                            } else {
                                    inv.Error_Message__c += 'Unable to appropriate with product forecast. Billing Date is missing';
                                    inv.To_Be_Processed__c = false;
                                    System.debug('Invoice No ' + inv.Name + ' : Error ' + inv.Error_Message__c);
                            }
                            inv.Opportunity__c = pr.Opportunity_Product__r.Opportunity__c;
                            
                            if(pr.Opportunity_Product__r.Opportunity__c != null)
                                inv.Project__c = pr.Opportunity_Product__r.Opportunity__r.Project__c;
                                //New code - Added by Pallavi
                               if(pr.Opportunity_Product__r.Opportunity__r.Owner.IsActive){
                               // below if added by sai. 
                               if(pr.Opportunity_Product__r.Record_type_name__c != 'Inspire')
                                inv.OwnerId = pr.Opportunity_Product__r.Opportunity__r.OwnerId;
                                }
                                /*ConnectApi.BatchInput connectBatchInput= createFeedItem(pr.Opportunity_Product__r.Opportunity__r.Id,pr.Opportunity_Product__r.Opportunity__r.Owner.ManagerId ,pr.Opportunity_Product__r.Opportunity__r.OwnerId,inv.Billing_Date__c,pr.Opportunity_Product__r.Name,inv.Name,inv.Quantity__c, pr.Opportunity_Product__r.Opportunity__r.Project__r.Project_Code__c, pr.Opportunity_Product__r.Opportunity__r.Name);
                                batchInputs.add(connectBatchInput);*/
                        }
                    } else {
                        inv.Error_Message__c += 'Unable to tag to Price Request. Price Request with Transaction Number doesn\'t exist';
                        inv.To_Be_Processed__c = false;
                        System.debug('Invoice No ' + inv.Name + ' : Error ' + inv.Error_Message__c);
                    }
                } else { /*This if block has been already handled above */ }
            }
        }

        //Tagging with Product Forecast
        if(!productForecastExternalIdSet.isEmpty()) {
            Map<String, Product_Forecast__c> productForecastMap = new Map<String, Product_Forecast__c>();
            //Query all product forecast with external id as in "productForecastExternalIdSet"
            for(Product_Forecast__c pf : queryProductForecast(productForecastExternalIdSet)) {
                productForecastMap.put(pf.External_Id__c, pf);
            }

            Map<String, Product_Forecast__c> newProductForecastMap = new Map<String, Product_Forecast__c>();
            List<Forecast_Invoice__c> fiList = new List<Forecast_Invoice__c>();
            for(Id invId : invoiceProductForecastMap.keySet()) {
                String externalID = invoiceProductForecastMap.get(invId);
                Invoice__c inv = invoiceMap.get(invId);
                if(inv.Forecast_Invoice__c == null) {
                    if(productForecastMap.containsKey(externalID)) {
                        //Create Invoice Transaction Object Under Product Forecast
                        Product_Forecast__c pf = productForecastMap.get(externalID);
                        Forecast_Invoice__c fi = createForecastInvoice(inv, pf);
                        inv.Forecast_Invoice__r = fi;
                        inv.Tagged__c = true;
                        fiList.add(fi);
                    } else if(newProductForecastMap.containsKey(externalID)){
                        //Create Invoice Transaction object
                        Product_Forecast__c pf = newProductForecastMap.get(externalId);
                        Forecast_Invoice__c fi = createForecastInvoice(inv, pf);
                        fi.Product_Forecast__r = pf;
                        inv.Forecast_Invoice__r = fi;
                        inv.Tagged__c = true;
                        fiList.add(fi);
                    } else {
                        //Create New Product Forecast
                        Date forecastDate = inv.Billing_Date__c;
                        forecastDate = Date.newInstance(forecastDate.year(), forecastDate.month(), 1);
                        if(inv.Opportunity_Product__c != null) {
                            Product_Forecast__c pf = createProductForecast(inv.Opportunity_Product__c, forecastDate);
                            pf.External_Id__c = externalId;
                            newProductForecastMap.put(externalId, pf);
                            //And Then Create Invoice Transaction object
                            Forecast_Invoice__c fi = createForecastInvoice(inv, pf);
                            fi.Product_Forecast__r = pf;
                            inv.Forecast_Invoice__r = fi;
                            inv.Tagged__c = true;
                            fiList.add(fi);
                        }
                        
                    }
                } else {
                    inv.Tagged__c = true;
                }
            }

            //Insert Product Forecast 
            if(!newProductForecastMap.values().isEmpty()) {
                upsert newProductForecastMap.values() External_Id__c;
            }
              //Insert Invoice Chatter Post on corresponding opportunity 
          /* if(!batchInputs.isEmpty()) {
                ConnectApi.ChatterFeeds.postFeedElementBatch(Network.getNetworkId(), batchInputs);
            }  */    

            //Insert Forecast Invoice
            if(!fiList.isEmpty()) {
                for(Forecast_Invoice__c fi : fiList) {
                    if(fi.Product_Forecast__c == null) fi.Product_Forecast__c = fi.Product_Forecast__r.id;
                }
                insert fiList;
                //Now we have id of FI, Lets stamp it on invoice
                for(Forecast_Invoice__c fi : fiList) {
                    invoiceMap.get(fi.Invoice__c).Forecast_Invoice__c = fi.id;
                }
            }

        }


        //Update Invoice tagging
        update invoiceList; 
    }

    /*Override to above method with Set of Invoice Id as parameter*/
    public static void processInvoice(Set<Id> invoiceIdSet){
        processInvoice(queryInvoices(invoiceIdSet));
    }
    

    /* Appropriate Invoices */
    public static void appropriateInvoice(List<Invoice__c> invoiceList, boolean isRecomputing){ //isRecomputing ensures that it won't initiate recomputing again
        //Accepts only tagged invoices
        //Query All Price Request based on Transaction Number
        Set<String> transactionNoSet = new Set<String>();
        Map<Id, Invoice__c> invoiceMap = new Map<Id, Invoice__c>();

        //Prepare list of Transaction No
        System.debug('Invoice List' + invoiceList);
        for(Invoice__c inv : invoiceList) {
            if(inv.Tagged__c && !inv.Processed__c) {
                if(!String.isBlank(inv.Transaction_No__c)) {
                    transactionNoSet.add(inv.Transaction_No__c);
                    invoiceMap.put(inv.id,inv);
                } else {
                    //Invoice doesn't has transaction No, it cannot be tagged to any Price Request or Opportunity Product in this case
                    inv.Error_Message__c = inv.Error_Message__c + 'Unable to appropriate this invoice. Transaction Number is missing';
                    System.debug('Invoice No ' + inv.Name + ' : Error ' + inv.Error_Message__c);
                }
            }
        }

        if(!transactionNoSet.isEmpty()) {
            //Fetch Price Request based on Transaction No Set
            Map<String, Price_Request__c> priceRequestMap = new Map<String, Price_Request__c>();
            Map<Id, Forecast_Invoice__c> forecastInvoiceMap = new Map<Id, Forecast_Invoice__c>();

            for(Price_Request__c pr : queryPriceRequest(transactionNoSet)) {
                priceRequestMap.put(pr.Name, pr);   //Name on Price Request object is Transaction Number
            }
        
            Map<Id, Price_Request__c> recomputablePriceRequestMap = new Map<Id, Price_Request__c>();
            for(Invoice__c inv : invoiceMap.values()) {
                if(priceRequestMap.containsKey(inv.Transaction_No__c)) {
                    Price_Request__c pr = priceRequestMap.get(inv.Transaction_No__c);

                    if(!recomputablePriceRequestMap.containsKey(pr.Id)) {
                        //If Price Request is already suppossed to be recalculated, therefore no need to calculate again
                        
                        //Check for data sanity and assign default values
                        if(pr.Requested_Quantity__c == null) pr.Requested_Quantity__c = 0; //Incase if requested quantity is not defined, we should assign 0 value, so that no invoice would be appropriated against it
                        if(pr.Supplied_Quantity__c == null){
                            pr.Supplied_Quantity__c = 0;
                            pr.Balance_Quantity__c = pr.Requested_Quantity__c;
                        }

                        //If invoice has been already appropriated earlier, 
                        //then Appropriable_Quantity__c must has certain value
                        //In this case, we will first reset the counter and then process further
                        /* Quantity won't be change on existing invoice, however invoice can be cancelled in which case we have to modify Price Request Supplied Quantity*/
                        if(inv.Status__c == 'Reversed' || inv.Status__c == 'Cancelled') {
                            if(inv.Appropriable_Quantity__c != null) {
                                //Add it back to balance quantity
                                //Note if we are recalculating Invoice Appropriation for any price request then we should reset its Appropriable Quantity.
                                //pr.Supplied_Quantity__c = pr.Supplied_Quantity__c - inv.Appropriable_Quantity__c - inv.Non_Appropriable_Quantity__c;
                                //pr.Balance_Quantity__c = pr.Requested_Quantity__c - pr.Supplied_Quantity__c;
                                //if(pr.Balance_Quantity__c < 0) pr.Balance_Quantity__c = 0;
                                inv.Appropriable_Quantity__c = 0;
                                inv.Non_Appropriable_Quantity__c = inv.Quantity__c;
                            }
                            if(inv.Forecast_Invoice__c != null) {
                                Forecast_Invoice__c fi = new Forecast_Invoice__c();
                                fi.Id = inv.Forecast_Invoice__c;
                                fi.Appropriable_Quantity__c = 0;
                                forecastInvoiceMap.put(fi.Id, fi);
                            }
                            inv.Processed__c = true;
                            inv.To_Be_Processed__c = false;
                            if(isRecomputing == null || isRecomputing == false) {
                                //Insuring that this method will not initiate recalculation again
                                recomputablePriceRequestMap.put(pr.Id,pr);
                            }
                        } else {
                            //Appropriable Quantity must be equal to balance or less than that
                            inv.Appropriable_Quantity__c = (pr.Balance_Quantity__c >= inv.Quantity__c) ?inv.Quantity__c : pr.Balance_Quantity__c;
                            inv.Non_Appropriable_Quantity__c = inv.Quantity__c - inv.Appropriable_Quantity__c;
                            inv.Processed__c = true;
                            inv.To_Be_Processed__c = false;
                            pr.Supplied_Quantity__c = pr.Supplied_Quantity__c + inv.Quantity__c;
                            pr.Balance_Quantity__c = pr.Balance_Quantity__c - inv.Appropriable_Quantity__c;
                            if(inv.Forecast_Invoice__c != null) {
                                Forecast_Invoice__c fi = new Forecast_Invoice__c();
                                fi.Id = inv.Forecast_Invoice__c;
                                fi.Appropriable_Quantity__c = inv.Appropriable_Quantity__c;
                                forecastInvoiceMap.put(fi.Id, fi);
                            }
                        }
                    }
                }
            }

            if(!recomputablePriceRequestMap.isEmpty()) {
                System.debug('Recomputing for following Price Requests ' + recomputablePriceRequestMap);
                for(Price_Request__c p : recomputablePriceRequestMap.values()) {
                    p.Supplied_Quantity__c = null;
                }
            }

            update forecastInvoiceMap.values();
            update priceRequestMap.values();
            update invoiceMap.values();

            //It will initiate recalculation of Price Requests
            recomputeAppropriableQuantity(recomputablePriceRequestMap.keySet());
        }
    }

    /*Override to above method with Set of Invoice Id as parameter*/
    public static void appropriateInvoice(Set<Id> invoiceIdSet, Boolean isRecomputing){
        appropriateInvoice(queryInvoices(invoiceIdSet, 'Tagged__c = true AND Processed__c = false AND To_Be_Processed__c = true', true) , isRecomputing);
    }

    /*Method to recomputer appropriated values on Price Request */
    public static void recomputeAppropriableQuantity(Set<Id> priceRequestSet){
        //Fetch Price Request
        List<Price_Request__c> prList = [SELECT ID, Supplied_Quantity__c FROM Price_Request__c WHERE ID IN :priceRequestSet AND Approval_Status__c = 'Approved'];

        //Possibility of hitting governor limit if total number of invoice for set of price request is greater than 50000
        List<AggregateResult> arList = [SELECT Count(ID) FROM Invoice__c WHERE Price_Request__c IN :priceRequestSet];

        Integer totalInvoice;
        if(!arList.isEmpty()) totalInvoice = (Integer)arList[0].get('expr0');

        if(totalInvoice != null && !prList.isEmpty()) {
            //Reset counter
            List<Price_Request__c> tempPriceRequestUpdateList = new List<Price_Request__c>();
            for(Price_Request__c pr : prList) {
                //It will then automatically calculate Balance Quantity
                if(pr.Supplied_Quantity__c != null) pr.Supplied_Quantity__c = null;
                tempPriceRequestUpdateList.add(pr);
            }
            if(!tempPriceRequestUpdateList.isEmpty()) update prList;

            //Get the limit remaining for this execution for DMLRow
            Integer currentDMLRows = System.Limits.getDMLRows();
            Integer totalDMLRows = System.Limits.getLimitDMLRows();
            System.debug('Limits' + currentDMLRows + '/' + totalDMLRows);

            //For Each invoice, atleast 3 dml rows are touched + size of Pr list
            if((currentDMLRows + (3*totalInvoice) + prList.size()) >= totalDMLRows) {
                //Execute Batch Class
                System.debug('Running batch to recomputeAppropriableQuantity, no of records to be processed ' + (3*totalInvoice) + prList.size());
                RecomputeAppropriationBatchable rab = new RecomputeAppropriationBatchable(priceRequestSet);             
                Database.executeBatch(rab);

            } else {
                //Execute Directly
                Set<Id> invoiceIdSet = new Set<Id>();
                List<Invoice__c> invList = [SELECT ID FROM Invoice__c WHERE Price_Request__c IN :priceRequestSet];
                for(Invoice__c inv : (List<Invoice__c>) invList) {
                    inv.Processed__c = false;
                    inv.To_Be_Processed__c = true;
                    invoiceIdSet.add(inv.id);
                }
                update invList;
        
                //Initiate the process of recalculation
                InvoiceAppropriationHelper.appropriateInvoice(invoiceIdSet,true); //Recomputing
            }
        }
        System.debug(arList);
    }

    /* Override of above method for single Price Request */
    public static void recomputeAppropriableQuantity(Id priceRequestId){
        recomputeAppropriableQuantity(new Set<Id>{priceRequestId});
    }

      /******************/
     /* Helper Method  */
    /******************/
    static List<Invoice__c> queryInvoices(Set<Id> invoiceIdSet){
        return queryInvoices(invoiceIdSet, null, true);
    }

    @testVisible
    static List<Invoice__c> queryInvoices(Set<Id> invoiceIdSet, String whereString, Boolean queryAgain) {
        if((invoiceList_static == null) || (invoiceList_static != null && queryAgain)) {
            invoiceList_static = new List<Invoice__c>();
            List<String> invoiceFields = new List<String>{
                'Name',
                'Status__c',
                'Transaction_No__c',
                'Billing_Date__c',
                'Forecast_Invoice__c',
                'Quantity__c',
                'Appropriable_Quantity__c',
                'Non_Appropriable_Quantity__c',
                'Tagged__c',
                'Processed__c'
            };
            String whereClause = 'WHERE Id IN :invoiceIdSet';
            if(whereString != null) {
                whereClause = whereClause + ' AND ' + whereString;
            }
            String query = 'SELECT Id, ' + String.join(invoiceFields,',') + ' FROM Invoice__c ' + whereClause + ' ORDER BY Billing_Date__c DESC, CreatedDate DESC';
            System.debug('queryInvoices()' + ': query=> ' + query);
            invoiceList_static = (List<Invoice__c>)Database.query(query);
            return invoiceList_static;
        } else {
            return invoiceList_static;
        }
    }

    @testVisible
    static List<Price_Request__c> queryPriceRequest(Set<String> transactionNoSet){
        List<Price_Request__c> returnList = new List<Price_Request__c>();
        List<String> priceRequestFields = new List<String>{
                'Name',
                'Opportunity_Product__c',
                'Opportunity_Product__r.Opportunity__c',
               'Opportunity_Product__r.Opportunity__r.OwnerId',
                'Opportunity_Product__r.Opportunity__r.Name',
                'Opportunity_Product__r.Opportunity__r.Owner.ManagerId',
                'Opportunity_Product__r.Opportunity__r.Owner.IsActive',             
                'Opportunity_Product__r.Opportunity__r.Project__r.Project_Code__c',
                'Opportunity_Product__r.Name',
                'Opportunity_Product__r.Opportunity__r.Project__c',
                'Supplied_Quantity__c',
                'Balance_Quantity__c',
                'Requested_Quantity__c',
                'Opportunity_Product__r.Record_type_name__c'
            };
        String whereClause = 'WHERE Name IN :transactionNoSet AND Approval_Status__c = \'Approved\'';
        String query = 'SELECT Id, ' + String.join(priceRequestFields,',') + ' FROM Price_Request__c ' + whereClause;
        System.debug('queryPriceRequest()' + ': query=> ' + query);
        returnList = (List<Price_Request__c>)Database.query(query);
        return returnList;
    }

    @testVisible
    static List<Product_Forecast__c> queryProductForecast(Set<String> externalIDSet){
        List<Product_Forecast__c> returnList = new List<Product_Forecast__c>();
        List<String> productForecastFields = new List<String>{
                'Name',
                'Opportunity_Product__c',
                'External_Id__c',
                'Month__c',
                'Year__c'
            };
        String whereClause = 'WHERE External_Id__c IN :externalIDSet';
        String query = 'SELECT Id, ' + String.join(productForecastFields,',') + ' FROM Product_Forecast__c ' + whereClause;
        System.debug('queryProductForecast()' + ': query=> ' + query);
        returnList = (List<Product_Forecast__c>)Database.query(query);
        return returnList;
    }

    @testVisible
    static Forecast_Invoice__c createForecastInvoice(Invoice__c inv, Product_Forecast__c pf){
        Forecast_Invoice__c fi = new Forecast_Invoice__c();
        fi.Name = inv.Name;
        fi.Invoice__c = inv.id;
        fi.Product_Forecast__c = pf.id;
        System.debug('Creating Forecast Invoice for ' + inv.Name + ' & for Product_Forecast__c ' + pf.external_id__c);
        return fi;
    }

    @testVisible
    static Product_Forecast__c createProductForecast(Id opportunityProductId, Date forecastDate){
        Product_Forecast__c pf = new Product_Forecast__c();
        pf.Forecast_Date__c = forecastDate;
        pf.Opportunity_Product__c = opportunityProductId;
        pf.Month__c = monthMap.get(forecastDate.month());
        pf.Year__c = String.valueOf(forecastDate.year());
        return pf;
    }

   /*@testVisible
    static ConnectApi.BatchInput createFeedItem(Id opportunityId, Id managerId,Id userId, Date billingDate, String optyprod, String invoiceNumber, Decimal quantity, String projectCode, String optyName){

        ConnectApi.FeedItemInput input = new ConnectApi.FeedItemInput();

        input.subjectId = opportunityId;
        
        ConnectApi.MessageBodyInput body = new ConnectApi.MessageBodyInput();
        body.messageSegments = new List<ConnectApi.MessageSegmentInput>();

        ConnectApi.TextSegmentInput textSegment = new ConnectApi.TextSegmentInput();
        textSegment.text = 'New invoice post - billing date: '+billingDate+' ,opty prod:' + optyprod + ' ,invoice no.-'+invoiceNumber+' ,quantity:'+quantity+' ,projectCode:'+projectCode+' and optyName:'+optyName;

    // add the mention to opportunity owner
    ConnectApi.MentionSegmentInput mentionSegment = new ConnectApi.MentionSegmentInput();
    mentionSegment.id = userId;
    body.messageSegments.add(mentionSegment);

    // add the mention to opportunity owner manager
    ConnectApi.MentionSegmentInput mentionSegmentmanager = new ConnectApi.MentionSegmentInput();
    mentionSegmentmanager.id = managerId;
    body.messageSegments.add(mentionSegmentmanager);

        body.messageSegments.add(textSegment);
        input.body = body;

        ConnectApi.BatchInput batchInput = new ConnectApi.BatchInput(input);

        return batchInput;        
    }*/
}
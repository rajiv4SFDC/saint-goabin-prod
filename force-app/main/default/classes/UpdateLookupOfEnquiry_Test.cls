/* Created By         : Rajiv Kumar Singh
   Created Date       : 11/06/2018
   Class Name         : UpdateLookupOfEnquiry_Test
   Description        : 
   Last Modified By   : Rajiv Kumar Singh
   Last Modified Date : 11/06/2018
*/
@isTest
private class UpdateLookupOfEnquiry_Test
{ 
 // @istest
   private testmethod static void testMethod1()
   {
      Account p1 = new Account();
      p1.Name = 'testing';
      p1.Sales_Area_Region__c ='East';
      p1.SAP_Code__c = '6565666';
      insert p1;
      
      Enquiry__c ptp = new Enquiry__c();
      ptp.SAP_Code__c = '6565666';
      ptp.Bill_to_customer__c = p1.id;
      insert ptp;
      
      Enquiry__c  pt = [select id, SAP_Code__c from Enquiry__c  where id =:ptp.id];
      
    //  System.assertEquals(pt.product__c,p1.id);
      list<Id> ptrList = new list<Id>();
      ptrList.add(pt.id);
      
      UpdateLookupOfEnquiry.UpdateLookupValue(ptrList);
   }
}
@isTest
public class FetchWelcomeUserName_Test {
    static testMethod void fetchUsr() {
        
        UserRole r1 = new UserRole(DeveloperName = 'RegionalManagerEastDistribution', Name = 'AM : CAL2 - Distribution');
        UserRole r2 = new UserRole(DeveloperName = 'HODistribution', Name = 'Regional Manager East Distribution');
        UserRole r3 = new UserRole(DeveloperName = 'NationalHeadDistribution', Name = 'National Head Distribution');
        Insert r1;
        Insert r2;
        Insert r3;
        
        User u1 = new User(
            ProfileId = [SELECT Id FROM Profile WHERE Name = 'Distribution ORS'].Id,
            LastName = 'last',
            Email = 'puser2323000@amamama.com',
            Username = 'pus2323er000@amamama.com' + System.currentTimeMillis(),
            CompanyName = 'TEST',
            Title = 'title',
            Alias = 'alias',
            TimeZoneSidKey = 'America/Los_Angeles',
            EmailEncodingKey = 'UTF-8',
            LanguageLocaleKey = 'en_US',
            LocaleSidKey = 'en_US',
            // ManagerId = u8.id,
            UserRoleId = r1.Id,
            Phone  ='123123123123123'
        );
        insert u1;
        
        FetchWelcomeUserName.fetchUser();
    }
}
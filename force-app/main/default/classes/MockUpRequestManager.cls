public class MockUpRequestManager {
	
        public void fetchBeforeInsertRecordsForUpdation(List<Mock_Up_Request__c> PVList){
            
        //Preparing MAP<ownerid,list of his MUR records>
        Map<Id,List<Mock_Up_Request__c>> PVMap = new Map<Id,List<Mock_Up_Request__c>>();
            for(Mock_Up_Request__c n : (PVList)){
            List<Mock_Up_Request__c> PVOwnerList = PVMap.get(n.OwnerId__c);
                if(PVOwnerList!=null && !PVOwnerList.isEmpty()){
                    PVOwnerList.add(n);
                }                    
                else {
                    PVOwnerList = new List<Mock_Up_Request__c>();
                    PVOwnerList.add(n);
                }
            PVMap.put(n.OwnerId__c,PVOwnerList);
        }  
       new UserManagerFieldsHandler().updatePVFields(PVMap,FALSE);
    }    
}
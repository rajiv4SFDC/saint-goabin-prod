/* Created By: Rajiv Kumar Singh
Created Date: 14/06/2017
Class Name: SalesorderUpsertAPITest
Description : Test class for SalesorderUpsertAPI
Last Modified By :Rajiv Kumar Singh
Last Modified Date: 14/06/2017
*/

@IsTest
private class SalesorderUpsertAPITest{
    
    static testMethod void testPostMethod(){
        String JSONMsg = '[{"SalesorderNum":"3321","ReferenceNumber":"True","OrderStatus":"packed"}]';
        RestRequest request = new RestRequest();
        request.requestUri ='/services/apexrest/SalesorderAPI';
        request.httpMethod = 'POST';
        request.requestBody = Blob.valueof(JSONMsg);
        RestContext.request = request;
        RestContext.response = new RestResponse();
        SalesorderUpsertAPI.doHandleInsertRequest();
        
        JSONMsg = '[1';
        request = new RestRequest();
        request.requestUri ='/services/apexrest/SalesorderAPI';
        request.httpMethod = 'POST';
        request.requestBody = Blob.valueof(JSONMsg);
        RestContext.request = request;
        RestContext.response = new RestResponse();
        SalesorderUpsertAPI.doHandleInsertRequest();
        
        request = new RestRequest();
        request.requestUri ='/services/apexrest/SalesorderAPI';
        request.httpMethod = 'POST';
        request.requestBody = Blob.valueof('');
        RestContext.request = request;
        RestContext.response = new RestResponse();
        SalesorderUpsertAPI.doHandleInsertRequest();
    }
}
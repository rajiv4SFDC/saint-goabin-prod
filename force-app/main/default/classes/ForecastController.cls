public with sharing class ForecastController {
    
    public with sharing class DisplayOptyProdRecords {
        
        @AuraEnabled public string opid;
        @AuraEnabled public string name;
        @AuraEnabled public string closedate;
        @AuraEnabled public string stagename;
        @AuraEnabled public string amount;
        @AuraEnabled public string probability;
        @AuraEnabled public string projname;
        @AuraEnabled public string projcode;
        @AuraEnabled public List<Opportunity_Product__c> lst;
        
        public DisplayOptyProdRecords(){
            opid = '';
            name = '';
            stagename = '';
            closedate = '';
            amount = '';
            probability = '';
            projname = '';
            projcode = '';
            lst = new List<Opportunity_Product__c>();
        }
    }   
   
 
     @AuraEnabled
    public static List<Product_Forecast__c> viewForecast (String optyId) {
        List<Product_Forecast__c> fList = new List<Product_Forecast__c>();
        fList = [Select quantity__c, month__c, year__c,Non_Approriated_Quantity__c,Total_Appropriable_Quantity__c,  Include_In_Sales_Plan__c from Product_Forecast__c where opportunity_product__c = :optyId order by year__c,month__c];
       // System.debug('Forecast List:'+fList);
        return fList;
    }

   /*  public with sharing class ForecastDetailWrapper {
        @AuraEnabled public string forecastid ;
        @AuraEnabled public string name ;
        @AuraEnabled public string quantity ;
        @AuraEnabled public string month ;
        @AuraEnabled public string year ;
               
        public ForecastDetailWrapper( String m,String y){
           forecastid = ' ';
           name = ' ' ;
           quantity = '0';
           month =m;
           year = y;
        }
    } 
    
    @AuraEnabled
    public static ForecastDetailWrapper updateForecast(List<ForecastDetailWrapper> forecastList, String fid){
      // System.debug('Received param -'+fid);
        if(forecastList.size()>0){
            for(ForecastDetailWrapper pf : forecastList){
               //  System.debug('Wrapper values -'+pf.forecastid);
                if(fid.equals(pf.forecastid)){
                  //  System.debug('Returned forecast for updation'+pf);
                    return pf;                    
                }
              //  System.debug('Nothing returned for forecast updation');
                return forecastList[0];               
            }
        } return forecastList[0];
    }    */

    @AuraEnabled
    public static DisplayOptyProdRecords getOptyProd(Id optyId) { 
        List<DisplayOptyProdRecords> opList = new List<DisplayOptyProdRecords>(); 
        List<Opportunity> prodLst = [SELECT id, name, CloseDate, amount, StageName, Probability ,Project__r.Name,Project__r.Project_Code__c
                 ,( select id,Name,Product_Name__c,Estimated_Quantity__c from opportunity_products__r order by Name )
                                     FROM Opportunity WHERE Id = :optyId];
         List<Opportunity> oppLidt = [SELECT id, name, CloseDate, amount, StageName, Probability ,
                 ( select id,name,Estimated_Quantity__c from opportunity_products__r )
                                     FROM Opportunity WHERE Id = :optyId];
        String dateFormatString = 'dd-MM-yyyy';


        //System.debug('oppLidt=== '+ oppLidt);
       // System.debug('Id in Apex class-'+ optyId);
        Opportunity o = prodLst[0];

        Date d = o.CloseDate;
        Datetime dt = Datetime.newInstance(d.year(), d.month(),d.day());
        String dateString = dt.format(dateFormatString);
       // System.debug(dateString);

        DisplayOptyProdRecords rec = new DisplayOptyProdRecords();
        rec.opid = string.valueof(o.Id) ;
        rec.name = o.Name ;
        rec.stagename = o.StageName;
        rec.probability = string.valueof(o.Probability);
       //rec.closedate = string.valueof(o.CloseDate) ;
        rec.closedate = dateString;
        rec.amount = string.valueof(o.Amount) ;
        rec.projname = string.valueof(o.Project__r.Name) ;
        rec.projcode = string.valueof(o.Project__r.Project_Code__c) ; 
        rec.lst = o.opportunity_products__r ;  
        
       /* System.debug('WRAPPER ID-'+ rec.opid);
        System.debug('WRAPPER NAME-'+ rec.name);
        System.debug('WRAPPER STAGENAME-'+ rec.stagename);
        System.debug('WRAPPER PROBAB-'+ rec.probability);
        System.debug('WRAPPER CLOSEDATE-'+ rec.closedate);
        System.debug('WRAPPER AMOUNT-'+ rec.amount);
        System.debug('WRAPPER LST-'+ rec.lst);*/
        // System.debug('Proj Name-'+ rec.projname);
         //System.debug('Proj Code-'+ rec.projcode);
        return rec;
    }
    
        @AuraEnabled
    public static Integer getOptyProdCount(Id optyId) {
       // System.debug('ProductCount Function Called');
        AggregateResult ar = [SELECT count(Id) cnt from Opportunity_Product__c WHERE Opportunity__c = :optyId];
        Integer count =(Integer) ar.get('cnt');
       // System.debug('ProductCount=== '+ count);
       return count;   
    }

  @AuraEnabled
    public static Opportunity findById(Id optyId) {
        System.debug('Inside Controller Class findById!');
        List<Opportunity> oppLidt = [SELECT id, name, AccountId, CloseDate, amount, StageName, Probability 
                    FROM Opportunity WHERE Id = :optyId];

         return [SELECT Id, Name, CloseDate, Amount, StageName, Probability
                    FROM Opportunity WHERE Id = :optyId];
    }
    
     @AuraEnabled
    public static string getOptyProdYr(Id optyprodId) {
 
        List<AggregateResult> oppLidt = [select min(year__c) year from Product_Forecast__c WHERE Opportunity_Product__c  = :optyprodId];
        return string.valueof(oppLidt[0].get('year'));        
         
    }    
    

    @AuraEnabled
    public static String getOptyProdId (String optyprodId) {

        List<Opportunity_Product__c> oppList = [SELECT id, name,Status__c, Estimated_Quantity__c, Product_Name__c, Ton__c, Total_Price__c, Product_Type__c, 
                                                Customer_Name__c,Customer_Name__r.Name, Sales_Office__c, Product_Range__c, Thickness__c FROM Opportunity_Product__c  WHERE Id = :optyprodId];

        return Json.serialize(oppList[0]); 

     }   

    public with sharing class SaveResultWrapper {
       
   @AuraEnabled public List<Product_Forecast__c> savedList {get;set;}
   @AuraEnabled public Boolean errorFlag {get;set;}
   @AuraEnabled public String errorMessage {get;set;}
                   
    public SaveResultWrapper(){
    this.savedList = new List<Product_Forecast__c>();
    this.errorFlag = false;
    this.errorMessage = 'Records have been saved successfully!';
           }
    }    
     
    public with sharing class UploadRecordsWrapper {
       
        @AuraEnabled public string month;
        @AuraEnabled public string year;
        @AuraEnabled public string optyprod;
        @AuraEnabled public Integer quantity;
        @AuraEnabled public Integer id;
                   
        public UploadRecordsWrapper(){
        this.month = '';
        this.year='';
        this.optyprod = '';
        this.quantity = 0;
        this.id = 0;
        }
    }   
   
    @AuraEnabled
    public static String saveMonthForecasts (String months) {
        String extid ;        

        List<UploadRecordsWrapper> monthLst = (List<UploadRecordsWrapper>)System.JSON.deserialize(months, List<UploadRecordsWrapper>.class);
        system.debug('@@@Monthlist&&&&'+monthLst );
        List<Product_Forecast__c> addmonthsList = new List<Product_Forecast__c>();
           
        for(Integer i = 0; i < monthLst.size() ; i++){
            extid = monthLst[i].optyprod+'-'+(monthLst[i].id +1)+'-'+monthLst[i].year ;
          //  System.debug('Extid-'+extid);
  
            Date d = Date.newInstance(Integer.valueof(monthLst[i].year),monthLst[i].id +1,1);
           String dt = DateTime.newInstance(d.year(),d.month(),d.day()).format('d/M/YYYY');
          // String dt = DateTime.newInstance(2017,8,12).format('d/M/YYYY');
            Date dd = Date.parse(dt);
            Product_Forecast__c pf = new Product_Forecast__c(Forecast_Date__c = dd , Month__c = monthLst[i].month , Year__c = monthLst[i].year , Opportunity_Product__c = monthLst[i].optyprod , Quantity__c = monthLst[i].quantity , External_Id__c = extid);
            addmonthsList.add(pf);
        } 

        SaveResultWrapper wrp = new SaveResultWrapper();
   
       String str ;
        try {
            upsert addmonthsList External_Id__c;
            wrp.errorFlag = false;
            wrp.errorMessage = 'Forecast records have been saved successfully!';
            str = 'SF says Successfully uploaded with list size - '+addmonthsList.size();
        } catch (Exception e) {
           str = 'SF says uploading failed'+ e.getMessage();
          // System.debug(e.getMessage());
           wrp.errorFlag = true;
           wrp.errorMessage = 'Error saving Forecast records :'+e.getMessage()+'Please try again';
        }
    
      //  System.debug('Save status :'+str);  
       String optyId = monthLst[0].optyprod ;
       List<Product_Forecast__c> fList = new List<Product_Forecast__c>();
        fList = [Select quantity__c, month__c, year__c, Non_Approriated_Quantity__c,Total_Appropriable_Quantity__c from Product_Forecast__c where opportunity_product__c = :optyId order by year__c,month__c];
     //   System.debug('Forecast List:'+fList);
        wrp.savedList = fList;
     //   System.debug('Forecast List:'+wrp);
        String res = JSON.serialize(wrp);
        return res;
    }  
    
    @AuraEnabled
    public static String getProductUOM (String oppProductId) {
    
    if(oppProductId!= null)
    {
    list<account> acclist = new list<account>();

    account a1 = new account();
    a1.name='New CRM acc for Infinity';
    a1.Type = 'Builder';
   // a1.Region__c = 'North';
    a1.Address__c ='t';
    a1.City_ERP__c ='Mum1';
    a1.Email__c='testing@gmail.com';
    a1.MDCCode__c ='aaa';
    a1.MDCName__c='ccxs';
    a1.Mobile__c='1231231231';
    a1.Pin_Code__c='500002';
    a1.State_ERP__c ='tessss';
    a1.Strategic_Account__c = true;
    acclist.add(a1);
    
    
    account a2 = new account();
    a2.name='New CRM acc for Infinity';
    a2.Type = 'Builder';
  //  a2.Region__c = 'North';
    a2.Address__c ='t';
    a2.City_ERP__c ='Mum1';
    a2.Email__c='testing@gmail.com';
    a2.MDCCode__c ='aaa';
    a2.MDCName__c='ccxs';
    a2.Mobile__c='1231231231';
    a2.Pin_Code__c='500002';
    a2.State_ERP__c ='tessss';
    a2.Strategic_Account__c = true;
    acclist.add(a2);
    
    account a3 = new account();
    a3.name='New CRM acc for Infinity';
    a3.Type = 'Builder';
   // a3.Region__c = 'North';
    a3.Address__c ='t';
    a3.City_ERP__c ='Mum1';
    a3.Email__c='testing@gmail.com';
    a3.MDCCode__c ='aaa';
    a3.MDCName__c='ccxs';
    a3.Mobile__c='1231231231';
    a3.Pin_Code__c='500002';
    a3.State_ERP__c ='tessss';
    a3.Strategic_Account__c = true;
    acclist.add(a3);                            
    }
        
        system.debug('oppProductId:'+oppProductId);
        List<Opportunity_Product__c> productDetails = [SELECT Product__r.UOM__c FROM Opportunity_Product__c WHERE Id = :oppProductId];
        system.debug('productDetails:'+productDetails);
        if(productDetails.size() > 0) {
            
            return productDetails[0].Product__r.UOM__c;
        }
        return null;
    }
}
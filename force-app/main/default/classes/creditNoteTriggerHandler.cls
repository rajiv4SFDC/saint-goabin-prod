public class creditNoteTriggerHandler{
    
    // add recurrssion handler here. 
    
    public static void AfterInserthandler(list<Credit_Note__c> newList)
    {
        autoPopulateOpportunity_Product_Customer(newList);
    }
    
    public static void BeforeInserthandler(list<Credit_Note__c> newList)
    {
        rollupAvlQantitity(newList);
    }
    
    public static void AfterUpdatehandler(list<Credit_Note__c> newList,map<id,Credit_Note__c> oldMap)
    {
        set<id> cnIdsSet = new set<id>();
        list<Credit_Note__c> lstToPass = new list<Credit_Note__c>();
        for(Credit_Note__c cn:newList)
        {
            if(cn.ERP_Account__c != null && oldMap.get(cn.id).ERP_Account__c != cn.ERP_Account__c)
                lstToPass.add(cn);
        }
        
        if(lstToPass.size()>0)
            autoPopulateOpportunity_Product_Customer(lstToPass);
        
        
        
    }
    
    // used to rollup the product forecasting avl quantity Available_quantity_after_approval__c from all the product forecast records 
    // which re there under this creditNote --> oportunityProduct related list and populate to the new credeit note reuqest. 
    public static void rollupAvlQantitity(list<Credit_Note__c> newList)
    {
        // intially get the ids of the present records. 
        
        map<string,string> CnWithOppIdsMap = new map<String,string>();
        map<id,decimal> oppWithRollUpedVlauesMap = new map<id,decimal>();
        for(Credit_Note__c cn:newList)
        {
            CnWithOppIdsMap.put(cn.id,cn.opportunity_product__c);
            CnWithOppIdsMap.put(cn.opportunity_product__c,cn.id);
            
        }
        
        // do the rollup calculation for each opp which are there in the CnWithOppIdsMap. 
        
        for(Product_Forecast__c pf:[select id,name,Available_quantity_after_approval__c,Opportunity_Product__c from Product_Forecast__c where Opportunity_Product__c in:CnWithOppIdsMap.Keyset() AND Available_quantity_after_approval__c!= null AND Available_quantity_after_approval__c>0])
        { 
            if(!oppWithRollUpedVlauesMap.containsKey(pf.Opportunity_Product__c))
                oppWithRollUpedVlauesMap.put(pf.Opportunity_Product__c,pf.Available_quantity_after_approval__c);
            else   
                oppWithRollUpedVlauesMap.put(pf.Opportunity_Product__c,oppWithRollUpedVlauesMap.get(pf.Opportunity_Product__c) + pf.Available_quantity_after_approval__c);
        }
        
        for(Credit_Note__c cn:newList)
        {
            system.debug('The oppWithRollUpedVlauesMap is:.....'  +oppWithRollUpedVlauesMap +'.....'+cn.opportunity_Product__c);
            if(oppWithRollUpedVlauesMap.containsKey(cn.opportunity_Product__c))
                cn.Total_Quantity_sqm__c  = Integer.valueof(oppWithRollUpedVlauesMap.get(cn.opportunity_Product__c));
            system.debug('The  cn.Total_Quantity_sqm__c is : ' + cn.Total_Quantity_sqm__c+'.......'+oppWithRollUpedVlauesMap.get(cn.opportunity__c));
        }
        
    }
    
    // This method is used to auto populate of Opportunity_Product_Customer__c value. 
    public static void autoPopulateOpportunity_Product_Customer(list<Credit_Note__c> newList)
    {
        set<id> cnIdsSet = new set<id>();
        set<id> OpIdsSet = new set<id>();
        map<string,string> cnoppPrdWithcustomerNameMap = new map<string,string>();
        map<id,Opportunity_Product_with_Account__c> oppPrdWithTierPricingMap = new map<id,Opportunity_Product_with_Account__c>();
        list<Credit_Note__c> cnListToUpdate = new list<Credit_Note__c>();
        set<id> nowUpdatingIdsSet = new set<id>();
        
        
        for(Credit_Note__c cn:newList)
        {  
            if(cn.Opportunity_Product__c != null && cn.ERP_Account__c != null)
            {
                cnIdsSet.add(cn.id);
                OpIdsSet.add(cn.Opportunity_Product__c);    
                cnoppPrdWithcustomerNameMap.put(cn.Opportunity_Product__c,cn.ERP_Account__c);
            }
        }
        
        // Check the ERP_Account__c field value in the corresponding selected Opportunity_Product__c for populating the Opportunity_Product_Customer__c field. 
        
        if(cnIdsSet.size()>0)
        {
            for(Opportunity_Product_with_Account__c opc:[select id,name,Product_Tier_Pricing__c,Opportunity_Product__c,Account__c from Opportunity_Product_with_Account__c where Opportunity_Product__c in:cnoppPrdWithcustomerNameMap.keySet() AND Account__c in:cnoppPrdWithcustomerNameMap.Values()])
            { 
                // In the above query we are not checking the exact account matching with opp product. So we are checking at below.
                system.debug('The opc.Account__cis: '  +opc.Account__c+'.........'+cnoppPrdWithcustomerNameMap.get(opc.Opportunity_Product__c));
                if(cnoppPrdWithcustomerNameMap !=null && cnoppPrdWithcustomerNameMap.containsKey(opc.Opportunity_Product__c) && opc.Account__c == cnoppPrdWithcustomerNameMap.get(opc.Opportunity_Product__c))
                    oppPrdWithTierPricingMap.put(opc.Opportunity_Product__c,opc);
            }
        }
        
        for(Credit_Note__c cn:[select id,Opportunity_Product__c,Opportunity_Product_Customer__c from Credit_Note__c  where id in:cnIdsSet])
        {  
            if(oppPrdWithTierPricingMap.containsKey(cn.Opportunity_Product__c))
            {
                cn.Opportunity_Product_Customer__c = oppPrdWithTierPricingMap.get(cn.Opportunity_Product__c).id;
                cnListToUpdate.add(cn);
                nowUpdatingIdsSet.add(cn.id);
            }
        }
        
        for(Credit_Note__c cn:newList)
        {
            if(!nowUpdatingIdsSet.contains(cn.id))
                cn.addError('No Credit note is availiable for this ERP account');
        }
        if(cnListToUpdate.size()>0)
            update cnListToUpdate;
    }
    
}
@isTest
public class opportunitytrigger_Test{
    static testMethod void testForecast(){
    
     Id crmRecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('CRM').getRecordTypeId();
        Id erpRecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('ERP').getRecordTypeId();
         State__c sts = new State__c();
        sts.External_ID__c = '1';
        sts.Name = 'Assam';
        insert sts;

        City__c cty = new City__c();
        cty.Name = 'Abhayapuri';
        cty.External_ID__c = '1-1';
        cty.City_Code__c = '1';
        cty.Category__c = 'iCities';
        cty.State__c = sts.id;
        insert cty;
    
        Account acc =new Account();
        acc.Name = 'Test AccountCRM';
        acc.Type = 'Architect';
        acc.Sales_Area_Region__c = 'North';
        acc.Sales_Area__c = 'DEL1';
        acc.RecordTypeId = crmRecordTypeId;

        insert acc;
        
         
        
        Account accErp =new Account();
        accErp.Name = 'Test AccountERP';
        accErp.Type = 'Processor';
        accErp.Sales_Area_Region__c = 'North';
        accErp.Sales_Office_ERP__c = 'DEL1';
        accErp.RecordTypeId = erpRecordTypeId;

        insert accErp;

        
        
        Project__c proj =new Project__c();
        proj.Name = 'Star Fisheries';
        proj.Segment__c = 'Residential Buildings';
        proj.Region__c = 'North';
        proj.Sales_Area__c = 'DEL1';
        proj.Validated__c = true;
        proj.Project_Validated_Date__c = Datetime.newInstance(2017, 8, 27);
        proj.Glass_Govern__c = true;
        proj.Glass_Heals__c = false;
        proj.Cool_Homes__c = true;
        proj.Luxe_Glass__c = true;
        proj.Luxe_Homes__c = true;
        proj.Referrer_Account__c = acc.id;
        proj.Geolocation__Latitude__s = 28.560100;
        proj.Geolocation__Longitude__s = 77.219319;
        proj.City__c = cty.id;
        proj.State__c = cty.State__c;

        insert proj;
        
        Opportunity oppty = new Opportunity();
        oppty.Name = 'Test Oppty';
        oppty.Project__c =  proj.id;
        oppty.AccountId = acc.Id;
        oppty.StageName = 'Confirmed';
        oppty.CloseDate = Date.newInstance(2017,8, 31);
        oppty.Region__c = 'North';
        oppty.Sales_Area__c = 'DEL1';
        oppty.Design_Report_Required__c = 'Yes';
        oppty.Design_Report_Created__c = 'Yes';
        oppty.Design_Report_Created_Date__c = Date.newInstance(2017,8, 27);
        //oppty.OwnerId = ownerid[0].Id; 
        oppty.Jumbo_Resizing_Completed__c = Date.newInstance(2017,8, 27);
        oppty.Glass_Pro_Live_Completed__c = Date.newInstance(2017,8, 27);
        
        insert oppty;
    
    
    
    }
    }
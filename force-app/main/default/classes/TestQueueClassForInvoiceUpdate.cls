public class TestQueueClassForInvoiceUpdate implements Queueable {
    private List<Invoice__c> invoiceList;
    private List<Invoice__c> invoiceListTemp;
    private Map<id,Invoice__c> oldMap;
              
    public TestQueueClassForInvoiceUpdate(List<Invoice__c> recs, Map<id, Invoice__c> recMap) {
        this.invoiceList = recs;
        this.oldMap = recMap;
        invoiceListTemp = invoiceList;
    }
    
    public void execute(QueueableContext queCont) {
        Map<String, Decimal> mapOfInvoice = new Map<String, Decimal>();
        Map<String, Decimal> mapOfInvoiceToDeduct = new Map<String, Decimal>();
        Map<String, Decimal> mapOfInvoiceToUpdateMonthlyPlan = new Map<String, Decimal>();
        
        Set<Id> accountIds = new Set<Id>();
        Set<Id> productIds = new Set<Id>();
        Set<Id> productCategoryIds = new Set<Id>();
        
        //for(Invoice__c inv : [SELECT id, Name, Customer__c, Product__c, Product__r.Product_Master_Category__c, Product__r.Product_Master_Category__r.Name, Billing_Date__c, Item_Tonnage_formula__c, Status__c FROM Invoice__c WHERE id IN: invoiceList AND Customer__c != null AND Product__c != null AND Billing_Date__c != null]){
        for(Invoice__c inv : invoiceListTemp){
            if(inv.Item_Tonnage_formula__c != oldMap.get(inv.id).Item_Tonnage_formula__c || inv.Customer__c != oldMap.get(inv.id).Customer__c || inv.Product__c != oldMap.get(inv.id).Product__c || inv.Status__c != oldMap.get(inv.id).Status__c || inv.Billing_Date__c != oldMap.get(inv.id).Billing_Date__c){
                Date yearVal = inv.Billing_Date__c;
                String mapKey = String.ValueOf(inv.Customer__c)+'-'+String.ValueOf(inv.Product__c)+'-'+String.ValueOf(yearVal.year());
                System.debug('mapKey -> '+mapKey);
                if(inv.Customer__c != oldMap.get(inv.id).Customer__c){
                  accountIds.add(inv.Customer__c);
                  accountIds.add(oldMap.get(inv.id).Customer__c);
                }
                else
                  accountIds.add(inv.Customer__c);
                  
                if(inv.Product__c != oldMap.get(inv.id).Product__c){
                  productIds.add(inv.Product__c);
                  productIds.add(oldMap.get(inv.id).Product__c);
                }
                else
                  productIds.add(inv.Product__c); 
                  
                productCategoryIds.add(inv.Product__r.Product_Master_Category__c);
                productCategoryIds.add(oldMap.get(inv.id).Product__r.Product_Master_Category__c);
            }
        }
        

        
        Map<String,List<Invoice__c>> mapOfInvoiceList = new Map<String,List<Invoice__c>>();  
        Map<String,List<Invoice__c>> mapOfInvoiceSalesOfficeList = new Map<String,List<Invoice__c>>();
        
        Map<Id, List<Invoice__c>> mapOfCustIdInvoiceList = new Map<Id, List<Invoice__c>>();
        Map<Id, List<Invoice__c>> mapOfProdIdInvoiceList = new Map<Id, List<Invoice__c>>();
        Map<String, List<Invoice__c>> mapOfCustTerritoryInvoiceList = new Map<String, List<Invoice__c>>();
        Map<String, List<Invoice__c>> mapOfCustRegionInvoiceList = new Map<String, List<Invoice__c>>();
        Map<String,List<Invoice__c>> mapOfStateInvoiceList = new Map<String,List<Invoice__c>>();
        
        Set<String> setTobeIncluded = new Set<String>{'Invoiced', 'Truck Availability', 'Shipping', 'Loading'};
        
        list<Invoice__c> invoiceTempList = [SELECT id, Customer__c, Customer__r.Sales_Office_ERP__c, Customer__r.State__c, Customer__r.MDCName__c, Customer__r.Sales_Area_Region__c, Product__c, Product__r.Product_Master_Category__c, Item_Tonnage_formula__c, Billing_Date__c, Status__c FROM Invoice__c WHERE (Customer__c IN:accountIds OR Product__c IN:productIds) AND Status__c IN:setTobeIncluded AND CALENDAR_Year(Billing_Date__c) = :System.Today().Year()];
        for(Invoice__c inv : [SELECT id, Customer__c, Customer__r.Sales_Office_ERP__c, Customer__r.State__c, Customer__r.MDCName__c, Customer__r.Sales_Area_Region__c, Product__c, Product__r.Product_Master_Category__c, Item_Tonnage_formula__c, Billing_Date__c, Status__c FROM Invoice__c WHERE (Customer__c IN:accountIds OR Product__c IN:productIds) AND Status__c IN:setTobeIncluded AND CALENDAR_Year(Billing_Date__c) = :System.Today().Year()]){
            if(!mapOfInvoiceList.isEmpty() && mapOfInvoiceList.containsKey(String.ValueOf(inv.Customer__c)+String.ValueOf(inv.Product__r.Product_Master_Category__c)) && mapOfInvoiceList.get(String.ValueOf(inv.Customer__c)+String.ValueOf(inv.Product__r.Product_Master_Category__c))!=null){
                mapOfInvoiceList.get(String.ValueOf(inv.Customer__c)+String.ValueOf(inv.Product__r.Product_Master_Category__c)).add(inv);
            }
            else{
                mapOfInvoiceList.put(String.ValueOf(inv.Customer__c)+String.ValueOf(inv.Product__r.Product_Master_Category__c), new List<Invoice__c>{inv});
            }
            
            if(!mapOfInvoiceSalesOfficeList.isEmpty() && mapOfInvoiceSalesOfficeList.containsKey(inv.Customer__r.Sales_Office_ERP__c) && mapOfInvoiceSalesOfficeList.get(inv.Customer__r.Sales_Office_ERP__c) != null){
                mapOfInvoiceSalesOfficeList.get(inv.Customer__r.Sales_Office_ERP__c).add(inv);
            }
            else{
                mapOfInvoiceSalesOfficeList.put(inv.Customer__r.Sales_Office_ERP__c, new List<Invoice__c>{inv});
            }
            
            //invoice list with just account
            if(!mapOfCustIdInvoiceList.isEmpty() && mapOfCustIdInvoiceList.containsKey(inv.Customer__c) && mapOfCustIdInvoiceList.get(inv.Customer__c) != null){
                mapOfCustIdInvoiceList.get(inv.Customer__c).add(inv);
            }
            else{
                mapOfCustIdInvoiceList.put(inv.Customer__c, new List<Invoice__c>{inv});
            }
            
            //invoice list with just product
            if(!mapOfProdIdInvoiceList.isEmpty() && mapOfProdIdInvoiceList.containsKey(inv.Product__r.Product_Master_Category__c) && mapOfProdIdInvoiceList.get(inv.Product__r.Product_Master_Category__c) != null){
                mapOfProdIdInvoiceList.get(inv.Product__r.Product_Master_Category__c).add(inv);
            }
            else{
                mapOfProdIdInvoiceList.put(inv.Product__r.Product_Master_Category__c, new List<Invoice__c>{inv});
            }
            
            //invoice list with just territory
            if(!mapOfCustTerritoryInvoiceList.isEmpty() && mapOfCustTerritoryInvoiceList.containsKey(inv.Customer__r.MDCName__c) && mapOfCustTerritoryInvoiceList.get(inv.Customer__r.MDCName__c) != null){
                mapOfCustTerritoryInvoiceList.get(inv.Customer__r.MDCName__c).add(inv);
            }
            else{
                mapOfCustTerritoryInvoiceList.put(inv.Customer__r.MDCName__c, new List<Invoice__c>{inv});
            }
            
            //invoice list with just region
            if(!mapOfCustRegionInvoiceList.isEmpty() && mapOfCustRegionInvoiceList.containsKey(inv.Customer__r.Sales_Area_Region__c) && mapOfCustRegionInvoiceList.get(inv.Customer__r.Sales_Area_Region__c) != null){
                mapOfCustRegionInvoiceList.get(inv.Customer__r.Sales_Area_Region__c).add(inv);
            }
            else{
                mapOfCustRegionInvoiceList.put(inv.Customer__r.Sales_Area_Region__c, new List<Invoice__c>{inv});
            }
            
            //invoice list with state
            if(!mapOfStateInvoiceList.isEmpty() && mapOfStateInvoiceList.containsKey(inv.Customer__r.State__c) && mapOfStateInvoiceList.get(inv.Customer__r.State__c) != null){
                mapOfStateInvoiceList.get(inv.Customer__r.State__c).add(inv);
            }
            else{
                mapOfStateInvoiceList.put(inv.Customer__r.State__c, new List<Invoice__c>{inv});
            }
        }
          
        Id schemeExternalRectypeId= Schema.SObjectType.Schemes__c.getRecordTypeInfosByName().get('External').getRecordTypeId();
        Id schemeInternalRecTypeId = Schema.SObjectType.Schemes__c.getRecordTypeInfosByName().get('Internal').getRecordTypeId();
        
        List<Invoice_and_Scheme_Data_Log__c> invSchemeList = new List<Invoice_and_Scheme_Data_Log__c>();
        List<Schemes__c> schemeListNew = new List<Schemes__c>();
        List<Schemes__c> schemesList = [SELECT id, Account__c, User__c, User__r.Sales_Office__c, User__r.State__c, User__r.Territory__c, User__r.Region__c, User__r.Profile.Name, Achieved__c, End_date__c, Product__c, Product__r.Product_Master_Category__c, Product_Category__c, Start_Date__c, RecordTypeId FROM Schemes__c WHERE (Account__c IN: accountIds OR Product__c IN:productIds OR Product__r.Product_Master_Category__c IN:productCategoryIds OR User__r.Sales_Office__c IN:mapOfInvoiceSalesOfficeList.keySet() OR User__r.Territory__c IN:mapOfCustTerritoryInvoiceList.keySet() OR User__r.Region__c IN:mapOfCustRegionInvoiceList.keySet()) AND Not_To_Be_Considered__c = false];
        
        for(Schemes__c sch : schemesList){
            String productCat = '';
            productCat = sch.Product__c == null ? sch.Product_Category__c : sch.Product__r.Product_Master_Category__c;
            Decimal TotalAchieved = 0;
            
            if(sch.RecordTypeId == schemeExternalRectypeId){
                if(sch.Account__c != null && (sch.Product__c != null || sch.Product_Category__c != null )){   
                    
                    if(!mapOfInvoiceList.isEmpty() && mapOfInvoiceList.containsKey(String.ValueOf(sch.Account__c)+productCat) && mapOfInvoiceList.get(String.ValueOf(sch.Account__c)+productCat)!=null){
                        System.debug('mapOfInvoiceList '+mapOfInvoiceList.get(String.ValueOf(sch.Account__c)+productCat).size());
                        for(Invoice__c inv : mapOfInvoiceList.get(String.ValueOf(sch.Account__c)+productCat)){
                            if(inv.Billing_Date__c >= sch.Start_Date__c && inv.Billing_Date__c <= sch.End_Date__c){
                                TotalAchieved = TotalAchieved + (inv.Item_Tonnage_formula__c > 0 ? inv.Item_Tonnage_formula__c : 0);
                            }
                            
                            //to enter a log
                            Invoice_and_Scheme_Data_Log__c invScheme = new Invoice_and_Scheme_Data_Log__c();
                            invScheme.Invoice__c = inv.id;
                            invScheme.Scheme__c = sch.id;
                            invScheme.Quantity__c = inv.Item_Tonnage_formula__c;
                            invSchemeList.add(invScheme);
                        }
                    }
                }
                else if(sch.Account__c != null && sch.Product__c == null && sch.Product_Category__c == null ){
                    if(!mapOfCustIdInvoiceList.isEmpty() && mapOfCustIdInvoiceList.containsKey(sch.Account__c) && mapOfCustIdInvoiceList.get(sch.Account__c)!=null){
                        System.debug('mapOfCustIdInvoiceList '+mapOfCustIdInvoiceList.get(sch.Account__c).size());
                        for(Invoice__c inv : mapOfCustIdInvoiceList.get(sch.Account__c)){
                            if(inv.Billing_Date__c >= sch.Start_Date__c && inv.Billing_Date__c <= sch.End_Date__c){
                                TotalAchieved = TotalAchieved + (inv.Item_Tonnage_formula__c > 0 ? inv.Item_Tonnage_formula__c : 0);
                            }
                            
                            //to enter a log
                            Invoice_and_Scheme_Data_Log__c invScheme = new Invoice_and_Scheme_Data_Log__c();
                            invScheme.Invoice__c = inv.id;
                            invScheme.Scheme__c = sch.id;
                            invScheme.Quantity__c = inv.Item_Tonnage_formula__c;
                            invSchemeList.add(invScheme);
                        }
                    }
                }
                
                else if(sch.Account__c == null && (sch.Product__c != null || sch.Product_Category__c != null)){
                    if(!mapOfProdIdInvoiceList.isEmpty() && mapOfProdIdInvoiceList.containsKey(productCat) && mapOfProdIdInvoiceList.get(productCat)!=null){
                        for(Invoice__c inv : mapOfProdIdInvoiceList.get(productCat)){
                            if(inv.Billing_Date__c >= sch.Start_Date__c && inv.Billing_Date__c <= sch.End_Date__c){
                                TotalAchieved = TotalAchieved + (inv.Item_Tonnage_formula__c > 0 ? inv.Item_Tonnage_formula__c : 0);
                            }
                            
                            //to enter a log
                            Invoice_and_Scheme_Data_Log__c invScheme = new Invoice_and_Scheme_Data_Log__c();
                            invScheme.Invoice__c = inv.id;
                            invScheme.Scheme__c = sch.id;
                            invScheme.Quantity__c = inv.Item_Tonnage_formula__c;
                            invSchemeList.add(invScheme);
                        }
                    }   
                }
            }
            
            else if(sch.RecordTypeId == schemeInternalRecTypeId){
                if(sch.Account__c != null && (sch.Product__c != null || sch.Product_Category__c != null)){
                    if(!mapOfInvoiceList.isEmpty() && mapOfInvoiceList.containsKey(String.ValueOf(sch.Account__c)+productCat) && mapOfInvoiceList.get(String.ValueOf(sch.Account__c)+productCat)!=null){
                        System.debug('mapOfInvoiceList '+mapOfInvoiceList.get(String.ValueOf(sch.Account__c)+productCat).size());
                        for(Invoice__c inv : mapOfInvoiceList.get(String.ValueOf(sch.Account__c)+productCat)){
                            if(inv.Billing_Date__c >= sch.Start_Date__c && inv.Billing_Date__c <= sch.End_Date__c){
                                TotalAchieved = TotalAchieved + (inv.Item_Tonnage_formula__c > 0 ? inv.Item_Tonnage_formula__c : 0);
                            }
                        
                        
                            //to enter a log
                            Invoice_and_Scheme_Data_Log__c invScheme = new Invoice_and_Scheme_Data_Log__c();
                            invScheme.Invoice__c = inv.id;
                            invScheme.Scheme__c = sch.id;
                            invScheme.Quantity__c = inv.Item_Tonnage_formula__c;
                            invSchemeList.add(invScheme);
                        }
                    }
                }
                
                else if(sch.Account__c != null && sch.Product__c == null && sch.Product_Category__c == null){
                    if(!mapOfCustIdInvoiceList.isEmpty() && mapOfCustIdInvoiceList.containsKey(sch.Account__c) && mapOfCustIdInvoiceList.get(sch.Account__c)!=null){
                        System.debug('mapOfCustIdInvoiceList '+mapOfCustIdInvoiceList.get(sch.Account__c).size());
                        for(Invoice__c inv : mapOfCustIdInvoiceList.get(sch.Account__c)){
                            if(inv.Billing_Date__c >= sch.Start_Date__c && inv.Billing_Date__c <= sch.End_Date__c){
                                TotalAchieved = TotalAchieved + (inv.Item_Tonnage_formula__c > 0 ? inv.Item_Tonnage_formula__c : 0);
                            }
                                
                            //to enter a log
                            Invoice_and_Scheme_Data_Log__c invScheme = new Invoice_and_Scheme_Data_Log__c();
                            invScheme.Invoice__c = inv.id;
                            invScheme.Scheme__c = sch.id;
                            invScheme.Quantity__c = inv.Item_Tonnage_formula__c;
                            invSchemeList.add(invScheme);
                        }
                    }
                }
                
                else if(sch.Account__c == null && (sch.Product__c != null || sch.Product_Category__c != null)){
                    if(!mapOfProdIdInvoiceList.isEmpty() && mapOfProdIdInvoiceList.containsKey(productCat) && mapOfProdIdInvoiceList.get(productCat)!=null){
                        //System.debug('mapOfProdIdInvoiceList '+mapOfProdIdInvoiceList.get(productCat.size());
                        for(Invoice__c inv : mapOfProdIdInvoiceList.get(productCat)){
                            if(inv.Billing_Date__c >= sch.Start_Date__c && inv.Billing_Date__c <= sch.End_Date__c){
                                TotalAchieved = TotalAchieved + (inv.Item_Tonnage_formula__c > 0 ? inv.Item_Tonnage_formula__c : 0);
                            }
                            
                            //to enter a log
                            Invoice_and_Scheme_Data_Log__c invScheme = new Invoice_and_Scheme_Data_Log__c();
                            invScheme.Invoice__c = inv.id;
                            invScheme.Scheme__c = sch.id;
                            invScheme.Quantity__c = inv.Item_Tonnage_formula__c;
                            invSchemeList.add(invScheme);
                        }
                    }   
                }
                
                //here need to segregate based on User profile
                else if(sch.Account__c == null && sch.Product__c == null && sch.Product_Category__c == null && sch.User__c != null){
                    if(String.ValueOf(sch.User__r.Profile.Name).contains('AM') ){
                        if(!mapOfInvoiceSalesOfficeList.isEmpty() && mapOfInvoiceSalesOfficeList.containsKey(sch.User__r.Sales_Office__c) && mapOfInvoiceSalesOfficeList.get(sch.User__r.Sales_Office__c) != null){
                            for(Invoice__c inv : mapOfInvoiceSalesOfficeList.get(sch.User__r.Sales_Office__c)){
                                if(inv.Billing_Date__c >= sch.Start_Date__c && inv.Billing_Date__c <= sch.End_Date__c){
                                    TotalAchieved = TotalAchieved + (inv.Item_Tonnage_formula__c > 0 ? inv.Item_Tonnage_formula__c : 0);
                                }
                        
                                //to enter a log
                                Invoice_and_Scheme_Data_Log__c invScheme = new Invoice_and_Scheme_Data_Log__c();
                                invScheme.Invoice__c = inv.id;
                                invScheme.Scheme__c = sch.id;
                                invScheme.Quantity__c = inv.Item_Tonnage_formula__c;
                                invSchemeList.add(invScheme);
                            }
                        }
                    }
                    
                    else if(String.ValueOf(sch.User__r.Profile).contains('ORS') ){
                        if(!mapOfCustTerritoryInvoiceList.isEmpty() && mapOfCustTerritoryInvoiceList.containsKey(sch.User__r.Territory__c) && mapOfCustTerritoryInvoiceList.get(sch.User__r.Territory__c) != null){
                            for(Invoice__c inv : mapOfCustTerritoryInvoiceList.get(sch.User__r.Territory__c)){
                                if(inv.Billing_Date__c >= sch.Start_Date__c && inv.Billing_Date__c <= sch.End_Date__c){
                                    TotalAchieved = TotalAchieved + (inv.Item_Tonnage_formula__c > 0 ? inv.Item_Tonnage_formula__c : 0);
                                }
                        
                                //to enter a log
                                Invoice_and_Scheme_Data_Log__c invScheme = new Invoice_and_Scheme_Data_Log__c();
                                invScheme.Invoice__c = inv.id;
                                invScheme.Scheme__c = sch.id;
                                invScheme.Quantity__c = inv.Item_Tonnage_formula__c;
                                invSchemeList.add(invScheme);
                            }
                        }
                    }
                    
                    else if(String.ValueOf(sch.User__r.Profile.Name).contains('RM') ){
                        if(!mapOfCustRegionInvoiceList.isEmpty() && mapOfCustRegionInvoiceList.containsKey(sch.User__r.Region__c) && mapOfCustRegionInvoiceList.get(sch.User__r.Region__c) != null){
                            for(Invoice__c inv : mapOfCustRegionInvoiceList.get(sch.User__r.Region__c)){
                                if(inv.Billing_Date__c >= sch.Start_Date__c && inv.Billing_Date__c <= sch.End_Date__c){
                                    TotalAchieved = TotalAchieved + (inv.Item_Tonnage_formula__c > 0 ? inv.Item_Tonnage_formula__c : 0);
                                }
                        
                                //to enter a log
                                Invoice_and_Scheme_Data_Log__c invScheme = new Invoice_and_Scheme_Data_Log__c();
                                invScheme.Invoice__c = inv.id;
                                invScheme.Scheme__c = sch.id;
                                invScheme.Quantity__c = inv.Item_Tonnage_formula__c;
                                invSchemeList.add(invScheme);
                            }
                        }
                    }
                    
                    else if(String.ValueOf(sch.User__r.Profile.Name).contains('SH') ){
                        if(!mapOfStateInvoiceList.isEmpty() && mapOfStateInvoiceList.containsKey(sch.User__r.State__c) && mapOfStateInvoiceList.get(sch.User__r.State__c)!=null){
                            for(Invoice__c inv : mapOfStateInvoiceList.get(sch.User__r.State__c)){
                                if(inv.Billing_Date__c >= sch.Start_Date__c && inv.Billing_Date__c <= sch.End_Date__c){
                                    TotalAchieved = TotalAchieved + (inv.Item_Tonnage_formula__c > 0 ? inv.Item_Tonnage_formula__c : 0);
                                }
                                
                                //to enter a log
                                Invoice_and_Scheme_Data_Log__c invScheme = new Invoice_and_Scheme_Data_Log__c();
                                invScheme.Invoice__c = inv.id;
                                invScheme.Scheme__c = sch.id;
                                invScheme.Quantity__c = inv.Item_Tonnage_formula__c;
                                invSchemeList.add(invScheme);
                            }
                        }
                    
                    }
                    
                    //get all invoices 
                    else if(String.ValueOf(sch.User__r.Profile.Name).contains('NH') ){
                        for(Invoice__c inv : invoiceTempList){
                            if(inv.Billing_Date__c >= sch.Start_Date__c && inv.Billing_Date__c <= sch.End_Date__c){
                                TotalAchieved = TotalAchieved + (inv.Item_Tonnage_formula__c > 0 ? inv.Item_Tonnage_formula__c : 0);
                            }
                            //to enter a log
                            Invoice_and_Scheme_Data_Log__c invScheme = new Invoice_and_Scheme_Data_Log__c();
                            invScheme.Invoice__c = inv.id;
                            invScheme.Scheme__c = sch.id;
                            invScheme.Quantity__c = inv.Item_Tonnage_formula__c;
                            invSchemeList.add(invScheme);
                        }
                    }
                }
            }
            
            sch.Achieved__c = TotalAchieved;
            schemeListNew.add(sch);
        }
        
        if(schemeListNew.size() > 0)
            update schemeListNew;  
            
        if(invSchemeList.size() > 0)
            insert invSchemeList;    
          
          
        for(Invoice__c inv : [SELECT id, Name, Customer__c, Product__c, Product__r.Product_Master_Category__r.Name, Billing_Date__c, Item_Tonnage_formula__c, Status__c, createdDate FROM Invoice__c WHERE Customer__c IN: accountIds AND Product__r.Product_Master_Category__c != null AND Product__c IN: productIds AND Billing_Date__c != null AND CALENDAR_Year(Billing_Date__c) = :System.Today().Year()]){
            Date yearVal = inv.Billing_Date__c;
            String mapKey = String.ValueOf(inv.Customer__c)+'-'+String.ValueOf(inv.Product__r.Product_Master_Category__r.Name)+'-'+String.ValueOf(yearVal.year());
            String mapMonthlyPlanKey = String.ValueOf(inv.Customer__c)+'-'+String.ValueOf(inv.Product__r.Product_Master_Category__r.Name)+'-'+String.ValueOf(yearVal.year())+'-Month-'+String.ValueOf(yearVal.Month());
            System.debug('mapKey -> '+mapKey);
            System.debug('prod cat name ->'+inv.Product__r.Product_Master_Category__r.Name);
            if(!mapOfInvoice.isEmpty() && mapOfInvoice.ContainsKey(mapKey) && mapOfInvoice.get(mapKey) != null){
              mapOfInvoice.put(mapKey, inv.Item_Tonnage_formula__c > 0 ? (mapOfInvoice.get(mapKey) + inv.Item_Tonnage_formula__c) : mapOfInvoice.get(mapKey));
            }
            else{
              mapOfInvoice.put(mapKey, inv.Item_Tonnage_formula__c > 0 ? inv.Item_Tonnage_formula__c : 0);
            }

            if(!mapOfInvoiceToUpdateMonthlyPlan.isEmpty() && mapOfInvoiceToUpdateMonthlyPlan.containsKey(mapMonthlyPlanKey) && mapOfInvoiceToUpdateMonthlyPlan.get(mapMonthlyPlanKey)!=null)
              mapOfInvoiceToUpdateMonthlyPlan.put(mapMonthlyPlanKey, inv.Item_Tonnage_formula__c > 0 ? (mapOfInvoiceToUpdateMonthlyPlan.get(mapMonthlyPlanKey) + inv.Item_Tonnage_formula__c) : mapOfInvoiceToUpdateMonthlyPlan.get(mapMonthlyPlanKey));
            else
              mapOfInvoiceToUpdateMonthlyPlan.put(mapMonthlyPlanKey, inv.Item_Tonnage_formula__c > 0 ? inv.Item_Tonnage_formula__c : 0);
              
            //to get discarded invoices value
            if(inv.Status__c == 'Reversed' || inv.Status__c == 'Cancelled' || inv.Status__c == 'Lost'){
              if(!mapOfInvoiceToDeduct.isEmpty() && mapOfInvoiceToDeduct.containsKey(mapKey) && mapOfInvoiceToDeduct.get(mapKey)!=null)
                  mapOfInvoiceToDeduct.put(mapkey, inv.Item_Tonnage_formula__c > 0 ? (mapOfInvoiceToDeduct.get(mapKey) + inv.Item_Tonnage_formula__c) : mapOfInvoiceToDeduct.get(mapKey));
              else
                  mapOfInvoiceToDeduct.put(mapKey, inv.Item_Tonnage_formula__c > 0 ? inv.Item_Tonnage_formula__c : 0);    
            }
        }
          
        List<Account_Forecast__c> accountForecastList = [SELECT id, Account__c, Product_Category__c, Achieved_Volume__c, Year__c FROM Account_Forecast__c WHERE Account__c IN :accountIds];
        List<Account_Forecast__c> accountForecastListToUpdate = new List<Account_Forecast__c>();
        for(Account_Forecast__c af : accountForecastList){
            String yearVal = String.ValueOf(af.Year__c);
            String mapKey = String.ValueOf(af.Account__c)+'-'+String.ValueOf(af.Product_Category__c)+'-'+yearVal;
            System.debug('mapKey -> '+mapKey);
            if(!mapOfInvoice.isEmpty() && mapOfInvoice.ContainsKey(mapKey) && mapOfInvoice.get(mapKey)!= null && !mapOfInvoiceToDeduct.isEmpty() && mapOfInvoiceToDeduct.containsKey(mapKey) && mapOfInvoiceToDeduct.get(mapKey) != null){
                af.Achieved_Volume__c = mapOfInvoice.get(mapKey) - mapOfInvoiceToDeduct.get(mapKey);    
                accountForecastListToUpdate.add(af);
            }
            else if(!mapOfInvoice.isEmpty() && mapOfInvoice.ContainsKey(mapKey) && mapOfInvoice.get(mapKey)!= null){
                af.Achieved_Volume__c = mapOfInvoice.get(mapKey);    
                accountForecastListToUpdate.add(af);
            }   
          
            if(accountForecastListToUpdate.size() > 0)
                update accountForecastListToUpdate;    
        }
          
          
        List<Monthly_Plan__c> newMonthlyPlanList = new List<Monthly_Plan__c>();
        List<Monthly_Plan__c> monthlyPlanList = [SELECT id, Account__c, Product_Category__c, Month__c, Year__c, Acheived_Target__c, Account_Forecast__c FROM Monthly_Plan__c WHERE Account_Forecast__c IN: accountForecastListToUpdate AND Month__c != null AND Year__c != null];
        for(Monthly_Plan__c mp : monthlyPlanList){
            Decimal yearVal = mp.Year__c;
            String monthVal = mp.Month__c;
            String mapKeyOfMp = String.ValueOf(mp.Account__c)+'-'+String.ValueOf(mp.Product_Category__c)+'-'+yearVal+'-Month-'+monthVal;
              
            if(!mapOfInvoiceToUpdateMonthlyPlan.isEmpty() && mapOfInvoiceToUpdateMonthlyPlan.ContainsKey(mapKeyOfMp) && mapOfInvoiceToUpdateMonthlyPlan.get(mapKeyOfMp)!= null && !mapOfInvoiceToDeduct.isEmpty() && mapOfInvoiceToDeduct.containsKey(mapKeyOfMp) && mapOfInvoiceToDeduct.get(mapKeyOfMp) != null){
                mp.Acheived_Target__c = mapOfInvoiceToUpdateMonthlyPlan.get(mapKeyOfMp) - mapOfInvoiceToDeduct.get(mapKeyOfMp);    
                newMonthlyPlanList.add(mp);
            }
              
            if(!mapOfInvoiceToUpdateMonthlyPlan.isEmpty() && mapOfInvoiceToUpdateMonthlyPlan.ContainsKey(mapKeyOfMp) && mapOfInvoiceToUpdateMonthlyPlan.get(mapKeyOfMp)!= null){
                mp.Acheived_Target__c = mapOfInvoice.get(mapKeyOfMp);    
                newMonthlyPlanList.add(mp);
            }
        }
          
        if(newMonthlyPlanList.size() > 0)
            update newMonthlyPlanList;
    }
    
    public static void coverClass(){
        Integer i = 0;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
    }
    
}
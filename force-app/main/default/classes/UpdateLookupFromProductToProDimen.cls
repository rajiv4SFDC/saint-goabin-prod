/* Created By         : Rajiv Kumar Singh
   Created Date       : 13/06/2018
   Class Name         : UpdateLookupFromProductToProDimen
   Description        : update the particular Product record lookup field to Product Dimension
   Last Modified By   : Rajiv Kumar Singh
   Last Modified Date : 13/06/2018
*/
public class UpdateLookupFromProductToProDimen{
  @InvocableMethod(label='UpdateLookupFromProductToProDimen' description='update the particular Account record lookup field to Enquiry')
  public static void UpdateLookupValue(List<Id> shipTOCustomerIds)
    {   
       map<string,string> productDimensionWithAccountMap = new map<string,string>();  
       set<string> presentSAPCodesSet = new set<String>();
       list<Product_Dimension__c > NewList = new list<Product_Dimension__c>(); 
       list<Product_Dimension__c > FinalListToUpdate = new list<Product_Dimension__c>();

       for(Product_Dimension__c ptp:[select id,Product_External_ID__c from Product_Dimension__c where id in:shipTOCustomerIds])
       {
         if(ptp.Product_External_ID__c != null)
          {
            presentSAPCodesSet.add(ptp.Product_External_ID__c);      
            NewList.add(ptp);
            system.debug('The ptp.id is: ' +ptp.id);
          }     
       }
       system.debug('The NewList is: ' +NewList+'......'+presentSAPCodesSet);
       if(presentSAPCodesSet.size()>0)
       {
          for(Product2 acc:[select id,External_Id__c  from Product2 where External_Id__c in:presentSAPCodesSet])
           {
               productDimensionWithAccountMap.put(acc.External_Id__c,acc.id);
           }
           system.debug('The productDimensionWithAccountMap is: ' +productDimensionWithAccountMap);
           for(Product_Dimension__c ptp:NewList)
           {
               ptp.Product__c = productDimensionWithAccountMap.get(ptp.Product_External_ID__c);
               system.debug('The  ptp.Product__c is: ' + ptp.Product__c);
               FinalListToUpdate.add(ptp);
           }
       }
       if(!FinalListToUpdate.isEmpty() && FinalListToUpdate.size() > 0) {
            update FinalListToUpdate;
       }
    }
}
/* Created By: Rajiv Kumar Singh
Created Date: 27/06/2018
Class Name: SalesOfficeNODInsertAPITest
Description : Test class for SalesOfficeNODInsertAPI
Last Modified By :Rajiv Kumar Singh
Last Modified Date: 27/06/2017
*/

@IsTest
private class SalesOfficeNODInsertAPITest{
    
    static testMethod void testPostMethod(){
        String JSONMsg = '[{"SalesOfficeName":"test1","SalesNODs":"10","AverageOutstanding":"12.43","postDate":"2018-01-1"},{"SalesOfficeName":"test2","SalesNODs":"11","AverageOutstanding":"12.43","postDate":"2018-01-1","Amt60":"32"}]';
        RestRequest request = new RestRequest();
        request.requestUri ='/services/apexrest/salesOfficeNOD';
        request.httpMethod = 'POST';
        request.requestBody = Blob.valueof(JSONMsg);
        RestContext.request = request;
        RestResponse res = new RestResponse();
        RestContext.response = new RestResponse();
        SalesOfficeNODInsertAPI.doHandleInsertRequest();
        
        JSONMsg = '[1';
        request = new RestRequest();
        request.requestUri ='/services/apexrest/salesOfficeNOD';
        request.httpMethod = 'POST';
        request.requestBody = Blob.valueof(JSONMsg);
        RestContext.request = request;
        RestContext.response = new RestResponse();
        SalesOfficeNODInsertAPI.doHandleInsertRequest();
        
        request = new RestRequest();
        request.requestUri ='/services/apexrest/salesOfficeNOD';
        request.httpMethod = 'POST';
        request.requestBody = Blob.valueof('');
        RestContext.request = request;
        RestContext.response = new RestResponse();
        SalesOfficeNODInsertAPI.doHandleInsertRequest();
    }
}
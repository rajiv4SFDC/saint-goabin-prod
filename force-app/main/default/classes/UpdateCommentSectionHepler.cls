public class UpdateCommentSectionHepler {
    private static boolean firstRun = true;
    public static boolean isFirstRun(){
        if(firstRun){
            firstRun = false;
            
            return true;
        }else{
            return firstRun;
        }
    } 
    
    public static void callMainMethod(List<price_request__c> prclist){
            List<price_request__c> priceRequests = New List <price_request__c>();
            set<Id> Ids = New set <Id>();
            //set<id> Idset = new set<id>();
            if(prclist.size()>0){
                for (price_request__c l: prclist){
                Ids.add (l.Id);
            }
          }
            
           system.debug('@23 Ids-->'+Ids);
            
            List<ProcessInstance> instances = [select Id,TargetObjectId from ProcessInstance where TargetObjectId iN:Ids];
            Map<Id,Id> ProcessMap = new Map<Id,Id>();
            Ids = New set <Id>();
            if(instances.size()>0){
                for(ProcessInstance pi:instances){
                ProcessMap.put (pi.TargetObjectId,pi.Id); 
                Ids.add (pi.Id);
            }
          }
            
            
            List<ProcessInstanceStep> instancesSteps = [select Comments,ProcessInstanceId from ProcessInstanceStep where ProcessInstanceId iN:Ids];
            Map<Id,String> ProcessStepMap = new Map<Id,String>(); 
            if(instancesSteps .size()>0){
                for(ProcessInstanceStep pis:instancesSteps){
                    system.debug('@@40 pis.Comments-->'+pis.Comments);
                ProcessStepMap.put (pis.ProcessInstanceId, pis.Comments);  
            }
          }
            
           // System.Debug ('** mapa2: '+ProcessStepMap);
          if(prclist.size()>0){
              for (price_request__c l:prclist){
                price_request__c pc = new price_request__c(id=l.id);
                pc.Approver_Comments__c = ProcessStepMap.get(ProcessMap.get(l.Id));
                priceRequests.add(pc);
                  
              System.debug ('**@51ProcessStepMap.get(ProcessMap.get(l.Id)): '+ProcessStepMap.get(ProcessMap.get(l.Id)));
              system.debug('**@52Comments for updation pc.Approver_Comments__c-->'+pc.Approver_Comments__c);
            }
          }
             
            system.debug('@56 Final list to update -->'+priceRequests);
        
            if(priceRequests.size()>0){
                try{
                    update priceRequests;
                }catch(exception ex){
                    system.debug('exception is:'+ex);
                }
                 
            }
           
    }
}
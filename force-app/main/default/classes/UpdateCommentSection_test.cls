@isTEst
public class UpdateCommentSection_test {
    
    @isTest static void testComments(){
        state__c state = new state__c();
        state.name='test';
        insert state;
        
        city__c c= new city__C();
        c.Name='test';
        c.State__c=state.id;
        insert c;
        
        project__c proj = new project__c();
        proj.Name='test';
        proj.Address__c='test';
        proj.City__c=c.id;
        proj.State__c=state.id;
        proj.Pin_Code__c='123454';
        proj.Project_Location__c='delhi';
        proj.Segment__c='hospitals';
        proj.Region__c='north';
        proj.Validated__c=true;
        proj.Geolocation__Latitude__s=22.00951;
        proj.Geolocation__Longitude__s=21.0093;
        insert proj; 
        
        product2 p = new product2();
        p.Name='test';
        p.ProductCode='12434';
        p.Description='test';
        p.Product_Category__c='infinity';
        p.Sub_Family__c='FLT Clear';
        p.Coating__c='silver';
        p.Tint__c='Clear';
        insert p;
        
        Id RecordTypeIdContact = Schema.SObjectType.account.getRecordTypeInfosByName().get('CRM').getRecordTypeId();
        //c1 = TestUtils.createContact(new Contact(title = 'CEO', Salutation = 'Mr.', Lastname='Contact 1', AccountId=ac1.id, Email='test1@test.com', recordtypeid=RecordTypeIdContact));
        
        account acc = new account();
        acc.Name='test';
        acc.RecordTypeId=RecordTypeIdContact;
        acc.Sales_Area_Region__c='North';
        acc.Type='Applicator';
        insert acc;
        
        opportunity opp = new opportunity();
        opp.Name='tetstz';
        opp.StageName='specified';
        opp.Type='infinity';
        opp.CloseDate=system.today();
        opp.Probability=100;
        opp.Project__c=proj.id;
        opp.AccountId=acc.id;
        insert opp;
        
        Key_Account__c kc = new Key_Account__c();
        kc.Account__c=acc.id;
        kc.Opportunity__c=opp.id;
        insert kc;
        
        Opportunity_Product__c oppc = new Opportunity_Product__c();
        oppc.Opportunity__c=opp.id;
        oppc.Product__c=p.id;
        oppc.Status__c='selected';
        insert oppc;
        
        product_forecast__c pfc = new product_forecast__c();
        pfc.Quantity__c=12;
        pfc.Opportunity_Product__c=oppc.id;
        insert pfc;
        
        list<price_request__c> requestList = new list<price_request__c>();
        for(integer i=0; i<=5; i++){
            price_request__c priceObj = new price_request__c();
            priceObj.Price_Type__c='list Price';
            priceObj.Approval_Status__c='Approved';
            priceObj.Approval_Remarks__c='tested';
            priceObj.Requested_Quantity__c=10;
            priceObj.Opportunity_Product__c=oppc.id;
            priceObj.Valid_From__c=system.today();
            priceObj.Valid_To__c=system.today()+1;
            requestList.add(priceObj);
        }
        if(requestList.size()>0){
			insert requestList;             
        }
        system.assertEquals(requestList.size(),6);
        
        list<price_request__c> updatelist = new list<price_request__c>();
        list<price_request__c> templist = new list<price_request__c>([select id from price_request__c]);
        updatelist.addAll(templist);
       	update updatelist;
        /* if(updatelist.size()>0){
            
        } */
        if(requestList.size()>0){
            UpdateCommentSectionHepler.callMainMethod(requestList);
        }
    }
}
public class ContentDocumentTriggerHandler extends TriggerHandler{
	
    public ContentDocumentTriggerHandler() {}
	
	public override void beforeDelete() {
		FileOperationHandler.restrictImageDelOnContentDoc( (List<ContentDocument>)Trigger.Old,
		    												new Map<String,Boolean>{'JPG'=>true,'PNG'=>true,'JPEG'=>true},
		    												new Map<String,Boolean>{'Project__c'=>true}
		    												);
    }
}
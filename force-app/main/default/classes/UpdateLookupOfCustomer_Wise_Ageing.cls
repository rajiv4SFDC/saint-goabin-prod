/* Created By         : Rajiv Kumar Singh
   Created Date       : 11/06/2018
   Class Name         : UpdateLookupOfCustomer_Wise_Ageing
   Description        : update the particular Account record lookup field to Customer_Wise_Ageing
   Last Modified By   : Rajiv Kumar Singh
   Last Modified Date : 11/06/2018
*/
public class UpdateLookupOfCustomer_Wise_Ageing{
  @InvocableMethod(label='UpdateLookupOfCustomer_Wise_Ageing' description='update the particular Account record lookup field to Customer_Wise_Ageing')
  public static void UpdateLookupValue(List<Id> customer_Wise_AgeingIds)
    {   
       map<string,string> customerWiseAgeingWithAccountMap = new map<string,string>();  
       set<string> presentSAPCodesSet = new set<String>();
       list<Customer_Wise_Ageing__c > NewList = new list<Customer_Wise_Ageing__c>(); 
       list<Customer_Wise_Ageing__c > FinalListToUpdate = new list<Customer_Wise_Ageing__c>();

       for(Customer_Wise_Ageing__c ptp:[select id,Account__c,SAP_Code__c from Customer_Wise_Ageing__c where id in:customer_Wise_AgeingIds])
       {
         if(ptp.SAP_Code__c != null)
          {
            presentSAPCodesSet.add(ptp.SAP_Code__c);      
            NewList.add(ptp);
            system.debug('The ptp.id is: ' +ptp.id);
          }     
       }
       system.debug('The NewList is: ' +NewList+'......'+presentSAPCodesSet);
       if(presentSAPCodesSet.size()>0)
       {
          for(Account acc:[select id,SAP_Code__c  from Account where SAP_Code__c in:presentSAPCodesSet])
           {
               customerWiseAgeingWithAccountMap.put(acc.SAP_Code__c,acc.id);
           }
           system.debug('The customerWiseAgeingWithAccountMap is: ' +customerWiseAgeingWithAccountMap);
           for(Customer_Wise_Ageing__c ptp:NewList)
           {
               ptp.Account__c = customerWiseAgeingWithAccountMap.get(ptp.SAP_Code__c);
               system.debug('The  ptp.Account__c is: ' + ptp.Account__c);
               FinalListToUpdate.add(ptp);
           }
       }
       if(!FinalListToUpdate.isEmpty() && FinalListToUpdate.size() > 0) {
            update FinalListToUpdate;
       }
    }
}
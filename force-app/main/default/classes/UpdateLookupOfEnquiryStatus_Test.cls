/* Created By         : Rajiv Kumar Singh
   Created Date       : 15/12/2018
   Class Name         : UpdateLookupOfEnquiryStatus_Test
   Description        : 
   Last Modified By   : Rajiv Kumar Singh
   Last Modified Date : 15/12/2018
*/
@isTest
private class UpdateLookupOfEnquiryStatus_Test
{ 
 // @istest
   private testmethod static void testMethod1()
   {
      Account p1 = new Account();
      p1.Name = 'testing';
      p1.Sales_Area_Region__c ='East';
      p1.SAP_Code__c = '6565666';
      insert p1;
      
      Enquiry__c ptp = new Enquiry__c();
      ptp.SAP_Code__c = '6565666';
      ptp.Bill_to_customer__c = p1.id;
      ptp.GD_Reference_Number__c = '12121';
      insert ptp;
      
      Enquiry_Status__c enqs = new Enquiry_Status__c();
      enqs.Enquiry_Reference__c = '12121';
      enqs.Enquiry__c =ptp.id;
      insert enqs;
      
      Enquiry_Status__c pt = [select id, Enquiry_Reference__c from Enquiry_Status__c where id =:enqs.id];
      
    //  System.assertEquals(pt.product__c,p1.id);
      list<Id> ptrList = new list<Id>();
      ptrList.add(pt.id);
      
      UpdateLookupOfEnquiryStatus.UpdateLookupValue(ptrList);
   }
}
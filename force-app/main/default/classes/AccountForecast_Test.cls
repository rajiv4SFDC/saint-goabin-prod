@isTest
public class AccountForecast_Test{
    static testMethod void testForecast(){
        
        Product_Category__c pc = new Product_Category__c();
        pc.Name = 'TestCat';
        pc.External_Id__c= 'qwer';
        insert pc;
        
        Product_Category__c pc1 = new Product_Category__c();
        pc1.Name = 'TestCat1';
        pc1.External_Id__c= 'qwer1';
        insert pc1;
        
        Product_Category__c pc2 = new Product_Category__c();
        pc2.Name = 'TestCat1';
        pc2.External_Id__c= 'qwer12';
        insert pc2;
        
        Account acc =new Account();
        acc.Name = 'Test';
        acc.Sales_Area_Region__c = 'North';
        insert acc;
        
        
        
        Account_Forecast__c af = new Account_Forecast__c();
        af.Account__c = acc.id;
        af.Product_Category__c = pc.id;
        af.ORM_Submitted__c = false;
        af.Year__c = 2018;
        af.Volume_in_tons_expected__c = 100;
        insert af;
        List<Account_Forecast__c> afListOld = new List<Account_Forecast__c>();
        afListOld.add(af);
        
        Account_Forecast__c af1 = new Account_Forecast__c();
        af1.Account__c = acc.id;
        af1.Product_Category__c = pc1.id;
        af1.ORM_Submitted__c = false;
        af1.Year__c = 2018;
        af1.Volume_in_tons_expected__c = 100;
        List<Account_Forecast__c> afListNew = new List<Account_Forecast__c>();
        afListNew.add(af1);
        
        Account_Forecast__c af2 = new Account_Forecast__c();
        af2.Account__c = acc.id;
        af2.Product_Category__c = pc2.id;
        af2.ORM_Submitted__c = false;
        af2.Year__c = 2018;
        af2.Volume_in_tons_expected__c = 100;
        List<Account_Forecast__c> afListNewV2 = new List<Account_Forecast__c>();
        afListNewV2.add(af2);
        
        Monthly_Plan__c mp1 = new Monthly_Plan__c();
        mp1.Month__c = 'Jan';
        mp1.Account_Forecast__c = af.id;
        mp1.Account__c = acc.id;
        mp1.My_Target__c = 10;
        mp1.Submitted__c = false;
        insert mp1;
        List<Monthly_Plan__c> mpListOld = new List<Monthly_Plan__c>();
        mpListOld.add(mp1);
        
        Monthly_Plan__c mp = new Monthly_Plan__c();
        mp.Month__c = 'Jan';
        mp.Account_Forecast__c = af.id;
        mp.Account__c = acc.id;
        mp.My_Target__c = 10;
        List<Monthly_Plan__c> mpListNew = new List<Monthly_Plan__c>();
        mpListNew.add(mp);
        
        Monthly_Plan__c mp2 = new Monthly_Plan__c();
        mp2.Month__c = 'Jan';
        mp2.Account_Forecast__c = af.id;
        mp2.Account__c = acc.id;
        mp2.My_Target__c = 10;
        List<Monthly_Plan__c> mpListNewV2 = new List<Monthly_Plan__c>();
        mpListNew.add(mp2);
        
        
        AccountForecast.productCategories();
        AccountForecast.getAllAccountForecast('2018');
        AccountForecast.getAllAttributesForMonthlyPlan(af.id);
        AccountForecast.selectedYear();
        AccountForecast.getMonthlyPlan(acc.id, pc.id, af.Id);
        AccountForecast.SaveAccounts(afListNew,'2018',afListOld);
        AccountForecast.savemonthlyPlan(mpListNew, af.id);
        AccountForecast.submitAndSaveAccounts(afListNewV2, '2018', afListOld);
        AccountForecast.submitAndSavemonthlyPlan(mpListNewV2, af.id, mpListOld);
    }
}
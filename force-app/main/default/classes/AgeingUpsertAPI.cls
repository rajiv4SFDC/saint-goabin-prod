/* Created By: Rajiv Kumar Singh
Created Date: 11/06/2017
Class Name: CustomertshipToUpsertAPI
Description : Upsert Ageing on Customer wise ageing for ERP.
Last Modified By :Rajiv Kumar Singh
Last Modified Date: 11/06/2017
*/
@RestResource(urlMapping='/ageingUpsertAPI')
global class AgeingUpsertAPI {
    @HttpPost
    global static void doHandleInsertRequest(){
        
        List<ageingWrapper> wrapList = new List<ageingWrapper>();
        List<Customer_Wise_Ageing__c> ageList = new List<Customer_Wise_Ageing__c>();
        RestRequest Req = RestContext.request;
        RestResponse res = RestContext.response;
        String body = Req.requestBody.toString().trim();
        
        if(body != null && body != '') {
            
            try {
                wrapList = (List<ageingWrapper>)JSON.deserialize(body, List<ageingWrapper>.class);
                for(ageingWrapper we :wrapList){
                    Customer_Wise_Ageing__c agel = new Customer_Wise_Ageing__c();
                    if(we.Value1!=null){
                        agel.X0_to_21__c = we.Value1;
                    }
                    if(we.Value2!=null){
                        agel.X22_to_45__c = we.Value2;
                    }
                    if(we.Value3!=null){
                        agel.X46_to_60__c = we.Value3;
                    } 
                    if(we.Value4!=null){
                        agel.X61_to_90__c = we.Value4;
                    }
                    if(we.Value5!=null){
                        agel.X91_to_180__c = we.Value5;
                    }
                    
                    if(we.Value6!=null){
                        agel.X181_to_240__c = we.Value6;
                    }
                    if(we.Value7!=null){
                        agel.X241_to_365__c = we.Value7;
                    }
                    if(we.Value8!=null){
                        agel.X365_Above__c = we.Value8;
                    }
                    if(we.SAPCode!=null && we.SAPCode!=''){
                        agel.SAP_Code__c = we.SAPCode;
                    }
                    if(we.osBal!=null){
                        agel.O_S_Balance__c = we.osBal;
                    }   
                    
                    ageList.add(agel);  
                }
                Schema.SObjectField ftoken = Customer_Wise_Ageing__c.Fields.SAP_Code__c;
                Database.UpsertResult[] srList = Database.upsert(ageList,ftoken,false); 
                
                res.statusCode = 200;
                res.responseBody = Blob.valueOf(String.valueof(ageList));
                
            } catch (Exception e) {
                
                CommonUtilitiesHandler.handleExceptions(e);
                res.statusCode = 500;
                res.responseBody = Blob.valueOf(e.getMessage());
            }
        } else {
            res.statusCode = 500;
            res.responseBody = Blob.valueOf('Invalid Request');
        }
    }
    
    public class ageingWrapper{
        public decimal Value1{get;set;}
        public decimal Value2{get;set;}
        public decimal Value3{get;set;}
        public decimal Value4{get;set;}
        public decimal Value5{get;set;}
        public decimal Value6{get;set;}
        public decimal Value7{get;set;}
        public decimal Value8{get;set;}
        public string SAPCode{get;set;}
        public decimal osBal{get;set;}
        
    }
}
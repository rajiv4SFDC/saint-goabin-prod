/* Created By: Rajiv Kumar Singh
Created Date: 13/06/2017
Class Name: ProductFittingUpsertAPI
Description : Product Fitting Upsert
Last Modified By :Rajiv Kumar Singh
Last Modified Date: 13/06/2017
*/
@RestResource(urlMapping='/FittingUpsertAPI')
global class ProductFittingUpsertAPI {
    @HttpPost
    global static void doHandleInsertRequest(){
        
        List<fittingWrapper> wrapList = new List<fittingWrapper>();
        List<Product2> prodList = new List<Product2>();
        RestRequest Req = RestContext.request;
        RestResponse res = RestContext.response;
        Set<String> presentArticleNumber = new Set<String>();
        String body = Req.requestBody.toString().trim();
        Map<String, String> prodStatus = new Map<String, String>();
        if(body != null && body != '') {
            
            try {
                wrapList = (List<fittingWrapper>)JSON.deserialize(body, List<fittingWrapper>.class);
                
                Map<String, Object> prodMapping = new Map<String, Object>();
                for(fittingWrapper we :wrapList){
                    Product2 prod = new Product2();
                    if(we.ProductCode!=null && we.ProductCode!=''){
                        prod.Article_number__c = we.ProductCode;
                        prod.ProductCode = we.ProductCode;
                    }
                    if(we.Description!=null && we.Description!=''){
                        prod.Description = we.Description;
                    }
                    if(we.Description!=null && we.Description!=''){
                        prod.Name = we.Description;
                    }
                    if(we.ListPrice!=null){    
                        prod.List_Price__c = we.ListPrice;
                    } 
                    if(we.SPLPRICEFLAG!=null && we.SPLPRICEFLAG!=''){
                        prod.Special_Price_Flag__c = we.SPLPRICEFLAG;
                    }
                    if(we.TierCategory!=null && we.TierCategory!=''){
                        prod.Type__c = we.TierCategory;
                    }
                    if(we.ProductCategory!=null && we.ProductCategory!=''){
                        prod.Product_Category__c = we.ProductCategory;
                    }
                    if(we.Uom!=null && we.Uom!=''){
                        prod.UOM__c = we.Uom;
                    } 
                     if(we.Status!=null && we.Status=='True'){
                         
                        prod.IsActive = boolean.valueof(we.Status);
                         
                    } else if(we.Status!=null && we.Status=='False'){
                        
                        prod.IsActive = boolean.valueof(we.Status);
                    }   
                    
                    prodStatus.put(prod.Article_number__c, we.Status);
                    prodMapping.put(prod.Article_number__c, prod);
                    //prodList.add(prod);  
                }
                
                if(prodMapping != null) {
                    
                    prodList = [SELECT Id, Article_number__c, Description, List_Price__c, Special_Price_Flag__c, Type__c, Product_Category__c, UOM__c, IsActive FROM Product2 WHERE Article_number__c IN :prodMapping.keySet()];
                	Product2 tmpProd;
                    for(Product2 prod: prodList) {
                        
                        tmpProd = (Product2) prodMapping.get(prod.Article_number__c);
                        prod.Description = tmpProd.Description;
                        prod.List_Price__c = tmpProd.List_Price__c;
                        prod.Special_Price_Flag__c = tmpProd.Special_Price_Flag__c;
                        prod.Type__c = tmpProd.Type__c;
                        prod.Product_Category__c = tmpProd.Product_Category__c;
                        prod.UOM__c = tmpProd.UOM__c;
                        system.debug('tmpProd.IsActive:'+tmpProd.IsActive);
                        if(prodStatus.get(prod.Article_number__c) !=null && prodStatus.get(prod.Article_number__c) == 'True'){
                            
                            prod.IsActive = boolean.valueof(prodStatus.get(prod.Article_number__c));
                            
                        } else if(prodStatus.get(prod.Article_number__c) !=null && prodStatus.get(prod.Article_number__c) == 'False'){
                            
                            prod.IsActive = boolean.valueof(prodStatus.get(prod.Article_number__c));
                        }
						presentArticleNumber.add(prod.Article_number__c);
                    }
                    
                    for(String key: prodMapping.keySet()) {
                        
                        if(!presentArticleNumber.contains(key)) {
                            
                            tmpProd = (Product2) prodMapping.get(key);
                            prodList.add(tmpProd);
                        }
                    }
                	upsert prodList;
                }
                //Schema.SObjectField ftoken = Product2.Fields.Article_number__c;
                //Database.UpsertResult[] srList = Database.upsert(prodList,ftoken,false);   
                system.debug('srList::'+prodList);
                res.statusCode = 200;
                res.responseBody = Blob.valueOf(String.valueof(prodList));
                
            } catch (Exception e) {
                
                system.debug('e.getMessage()::'+e.getMessage());
                CommonUtilitiesHandler.handleExceptions(e);
                res.statusCode = 500;
                res.responseBody = Blob.valueOf(e.getMessage());
            }
        } else {
            res.statusCode = 500;
            res.responseBody = Blob.valueOf('Invalid Request');
        } 
    }
    
    public class fittingWrapper{
        public String ProductCode{get;set;}
        public string Description{get;set;}
        public decimal ListPrice{get;set;}
        public string SPLPRICEFLAG{get;set;}
        public string TierCategory{get;set;}
        public string ProductCategory{get;set;}
        public string Uom{get;set;}
        public String Status{get;set;}
    }
}
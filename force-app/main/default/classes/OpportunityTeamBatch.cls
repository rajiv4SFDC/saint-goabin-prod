global class OpportunityTeamBatch implements Database.Batchable<sObject> {

	global OpportunityTeamBatch(){}

    global Database.QueryLocator start(Database.BatchableContext BC){
        return Database.getQueryLocator([SELECT id,Opportunity_Product__c,Opportunity_Product__r.Opportunity__c,
        								Opportunity_Product__r.Opportunity__r.Re_Calculate__c FROM Product_Forecast__c
        								WHERE Opportunity_Product__r.Opportunity__r.Re_Calculate__c =true]);
    }
    
    global void execute(Database.BatchableContext BC, List<Product_Forecast__c> prodForecastLst){

    	Map<Id,Boolean> mapOfOpptyId = new Map<Id,Boolean>();
 	 	List<Opportunity> lstOfOpptyToUpdate = new List<Opportunity>();  	
    	List<Opportunity_Product_Team__c> lstOfOpptyProdTeamInsert = new List<Opportunity_Product_Team__c>();

    	for(Product_Forecast__c pf : prodForecastLst){
    	
    		if( !mapOfOpptyId.containsKey(pf.Opportunity_Product__r.Opportunity__c) ){
    			Opportunity oppty = new Opportunity();
    			oppty.Id = pf.Opportunity_Product__r.Opportunity__c;
    			oppty.Re_Calculate__c = false;
    			
    			lstOfOpptyToUpdate.add(oppty);

    			mapOfOpptyId.put(pf.Opportunity_Product__r.Opportunity__c,true);
    		}
    		
    	}
        
        // Query all related Opportunity Product Team foe deletion
        List<Opportunity_Product_Team__c> lstOFOpptyProdTeamForDeletion = [SELECT id,Product_Forecast__c 
								        								  FROM Opportunity_Product_Team__c 
								        								  WHERE Product_Forecast__c IN : prodForecastLst];

		List<OpportunityTeamMember> lstOfOpptyTeam = [SELECT id,OpportunityId,UserId,
													  TeamMemberRole,Share_pct__c
													  From OpportunityTeamMember 
													  WHERE OpportunityId IN : mapOfOpptyId.keySet() ];

		for(Product_Forecast__c pf : prodForecastLst){

			for(OpportunityTeamMember otm : lstOfOpptyTeam){

				if(otm.OpportunityId == pf.Opportunity_Product__r.Opportunity__c){

					Opportunity_Product_Team__c opt = new Opportunity_Product_Team__c();
					opt.Product_Forecast__c = pf.id;
					opt.Opportunity_Team_Id__c = otm.Id;
					opt.Share_pct__c = otm.Share_pct__c;
					opt.TeamMemberRole__c = otm.TeamMemberRole;
					opt.User__c = otm.UserId;
					
					lstOfOpptyProdTeamInsert.add(opt);
				}
			}
		}

		// Delete existing Opportunity Product Team record
		if( lstOFOpptyProdTeamForDeletion.size() > 0 ){
			delete lstOFOpptyProdTeamForDeletion;
		}
		// Insert newly created Oppotunity Product Team records		
		if( lstOfOpptyProdTeamInsert.size() > 0 ){
			insert lstOfOpptyProdTeamInsert;
		}
		// Update all Processed Opportunity Re-calculate checkbox
		if( lstOfOpptyToUpdate.size() > 0 ){
			update lstOfOpptyToUpdate;
		}						  
    }
    
    global void finish(Database.BatchableContext BC) {
        
    }
}
/******************************************
* Created By  : Ajeet Singh Shekhawat
* Created On  : 21 Nov 2018
* Modified By : Ajeet Singh Shekhawat
* Modified On : 21 Nov 2018
* Description :

******************************************/ 

@isTest
public class CreditNotesTicketingMassApproval_Test {
    static testMethod void creditNoteMethod() {
        
        UserRole r1 = new UserRole(DeveloperName = 'RegionalManagerEastDistribution', Name = 'AM : CAL2 - Distribution');
        UserRole r2 = new UserRole(DeveloperName = 'HODistribution', Name = 'Regional Manager East Distribution');
        UserRole r3 = new UserRole(DeveloperName = 'NationalHeadDistribution', Name = 'National Head Distribution');
        Insert r1;
        Insert r2;
        Insert r3;
        
        User u1 = new User(
            ProfileId = [SELECT Id FROM Profile WHERE Name = 'Distribution ORS'].Id,
            LastName = 'last',
            Email = 'puser2323000@amamama.com',
            Username = 'pus2323er000@amamama.com' + System.currentTimeMillis(),
            CompanyName = 'TEST',
            Title = 'title',
            Alias = 'alias',
            TimeZoneSidKey = 'America/Los_Angeles',
            EmailEncodingKey = 'UTF-8',
            LanguageLocaleKey = 'en_US',
            LocaleSidKey = 'en_US',
            // ManagerId = u8.id,
            UserRoleId = r1.Id,
            Phone  ='123123123123123'
        );
        insert u1;
        
        User u2 = new User(
            ProfileId = [SELECT Id FROM Profile WHERE Name = 'Distribution ORS'].Id,
            LastName = 'last1',
            Email = 'puser2323000@amamama.com',
            Username = 'pus2323er0000@amamama.com' + System.currentTimeMillis(),
            CompanyName = 'Ibm',
            Title = 'title',
            Alias = 'alias',
            TimeZoneSidKey = 'America/Los_Angeles',
            EmailEncodingKey = 'UTF-8',
            LanguageLocaleKey = 'en_US',
            LocaleSidKey = 'en_US',
            ManagerId = u1.id,
            UserRoleId = r1.Id,
            Phone  ='123123123123123'
        );
        insert u2;
        
        User u3 = new User(
            ProfileId = [SELECT Id FROM Profile WHERE Name = 'Distribution ORS'].Id,
            LastName = 'last1',
            Email = 'puser2323000@amamama.com',
            Username = 'pus2323er0000@amamama.com' + System.currentTimeMillis(),
            CompanyName = 'Dell',
            Title = 'title',
            Alias = 'alias',
            TimeZoneSidKey = 'America/Los_Angeles',
            EmailEncodingKey = 'UTF-8',
            LanguageLocaleKey = 'en_US',
            LocaleSidKey = 'en_US',
            ManagerId = u2.id,
            UserRoleId = r2.Id,
            Phone  ='123123123123123'
        );
        insert u3;
        
        User u4 = new User(
            ProfileId = [SELECT Id FROM Profile WHERE Name = 'Distribution ORS'].Id,
            LastName = 'last11',
            Email = 'puser23230100@amamama.com',
            Username = 'pus2323er0000@amamama.com' + System.currentTimeMillis(),
            CompanyName = 'HP',
            Title = 'title',
            Alias = 'alias',
            TimeZoneSidKey = 'America/Los_Angeles',
            EmailEncodingKey = 'UTF-8',
            LanguageLocaleKey = 'en_US',
            LocaleSidKey = 'en_US',
            ManagerId = u3.id,
            UserRoleId = r3.Id,
            Phone  ='123123123123123'
        );
        insert u4;
        
        User u5 = new User(
            ProfileId = [SELECT Id FROM Profile WHERE Name = 'Distribution ORS'].Id,
            LastName = 'last111',
            Email = 'puser123230100@amamama.com',
            Username = 'pus2323er0000@amamama.com' + System.currentTimeMillis(),
            CompanyName = 'Info',
            Title = 'title',
            Alias = 'alias',
            TimeZoneSidKey = 'America/Los_Angeles',
            EmailEncodingKey = 'UTF-8',
            LanguageLocaleKey = 'en_US',
            LocaleSidKey = 'en_US',
            //  ManagerId = u4.id,
            UserRoleId = r1.Id,
            Phone  ='123123123123123'
        );
        insert u5;
        
        User u6 = new User(
            ProfileId = [SELECT Id FROM Profile WHERE Name = 'Distribution ORS'].Id,
            LastName = 'last1161',
            Email = 'puser123230100@amamama.com',
            Username = 'pus2323er0000@amamama.com' + System.currentTimeMillis(),
            CompanyName = 'trxn',
            Title = 'title',
            Alias = 'alias',
            TimeZoneSidKey = 'America/Los_Angeles',
            EmailEncodingKey = 'UTF-8',
            LanguageLocaleKey = 'en_US',
            LocaleSidKey = 'en_US',
            ManagerId = u5.id,
            UserRoleId = r1.Id,
            Phone  ='123123123123123'
        );
        insert u6;
        
        User u7 = new User(
            ProfileId = [SELECT Id FROM Profile WHERE Name = 'Distribution ORS'].Id,
            LastName = 'last1811',
            Email = 'puser123230100@amamama.com',
            Username = 'pus2323er0000@amamama.com' + System.currentTimeMillis(),
            CompanyName = 'Bnbxs',
            Title = 'title',
            Alias = 'alias',
            TimeZoneSidKey = 'America/Los_Angeles',
            EmailEncodingKey = 'UTF-8',
            LanguageLocaleKey = 'en_US',
            LocaleSidKey = 'en_US',
            ManagerId = u6.id,
            UserRoleId = r2.Id,
            Phone  ='123123123123123'
        );
        insert u7;
        
        User u8 = new User(
            ProfileId = [SELECT Id FROM Profile WHERE Name = 'Distribution ORS'].Id,
            LastName = 'last1811',
            Email = 'puser123230100@amamama.com',
            Username = 'pus2323er0000@amamama.com' + System.currentTimeMillis(),
            CompanyName = 'Nagaro',
            Title = 'title',
            Alias = 'alias',
            TimeZoneSidKey = 'America/Los_Angeles',
            EmailEncodingKey = 'UTF-8',
            LanguageLocaleKey = 'en_US',
            LocaleSidKey = 'en_US',
            ManagerId = u7.id,
            UserRoleId = r3.Id,
            Phone  ='123123123123123'
        );
        insert u8;   
        
        System.runAs(u2) {   
            
            CreditNotesTicketingMassApproval__c prm = new CreditNotesTicketingMassApproval__c();
            prm.Name='Regional Manager East Distribution';
            prm.Role_Name__c ='Regional Manager East Distribution';
            prm.List_view_URL__c = 'https://www.saint-gobain.com';
            insert prm;
            
            CreditNotesTicketingMassApproval__c prm1 = new CreditNotesTicketingMassApproval__c();
            prm1.Name='Default';
            prm1.Role_Name__c ='Default';
            prm1.List_view_URL__c = 'https://www.saint-gobain.com';
            insert prm1;
            
            CreditNotesTicketingMassApproval__c prm2 = new CreditNotesTicketingMassApproval__c();
            prm2.Name='HO Distribution';
            prm2.Role_Name__c ='HO Distribution';
            prm2.List_view_URL__c = 'https://www.saint-gobain.com';
            insert prm2;
            
            CreditNotesTicketingMassApproval__c prm3 = new CreditNotesTicketingMassApproval__c();
            prm3.Name='National Head Distribution';
            prm3.Role_Name__c ='National Head Distribution';
            prm3.List_view_URL__c = 'https://www.saint-gobain.com';
            insert prm3;
            
            State__c st = new State__c();
            st.Name = 'Karnataka';
            Insert st;
            
            City__c ct = new City__c();
            ct.Name = 'Banglore';
            ct.State__c = st.Id;
            Insert ct;
            
            Project__c project = new Project__c();
            project.Name = 'kvp';
            project.Segment__c = 'Retail';
            project.Region__c = 'East';
            project.Sales_Area__c = 'AP1';
            project.Validated__c = true;
            Insert project;
            
            Account acc = new Account();
            acc.Name = 'KvpTest';
            acc.Type = 'Dealer';
            acc.Sales_Area_Region__c = 'East';
            acc.SAP_Code__c = '251985';
            acc.RecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('CRM').getRecordTypeId();
            Insert acc;
            
            Opportunity oppo = new Opportunity();
            oppo.Name = 'testoppo';
            oppo.Project__c = project.Id;
            oppo.AccountId = acc.Id;
            oppo.Type = 'Infinity';
            oppo.StageName = 'Specified';
            oppo.Probability = 10;
            oppo.CloseDate = System.today();
            Insert oppo;
            
            Key_Account__c keyAcct = new Key_Account__c();
            keyAcct.Opportunity__c = oppo.Id;
            keyAcct.Account__c = acc.Id;
            insert keyAcct;
            
            Product2 pro = new Product2();
            pro.Name = 'test';
            pro.Product_Category__c = 'Inspire';
            Insert pro;    
            
            Product_Stock__c pdStock = new Product_Stock__c();
            pdStock.Plant_Code__c = '100';
            Insert pdStock;
            
            Invoice__c inv = new Invoice__c();
            inv.Name = 'pr1-7';
            inv.Invoice_Line_Item_No__c = '100';
            inv.Item_Tonnage__c = 'pr1-7';
            inv.Invoice_Rate__c = 10;
            inv.Quantity__c = 20;
            inv.Product__c = pro.Id;
            Insert inv;
            
            Opportunity_Product__c oppoPro = new Opportunity_Product__c();
            oppoPro.Product__c = pro.Id;
            oppoPro.Status__c = 'Selected';
            oppoPro.Opportunity__c = oppo.Id;
            Insert oppoPro;
            
            try{
                Price_Request__c pr = new Price_Request__c();
                pr.Approval_Status__c = 'Approved';
                pr.Opportunity_Product__c = oppoPro.Id;
                pr.Requested_Quantity__c = 3;
                pr.Price_Type__c = 'List Price';
                Insert pr;
                
                price_request_PandP__c prPandP = new price_request_PandP__c();
                prPandP.Customer__c = acc.Id;
                prPandP.Requested_Quantity__c = 4;
                prPandP.Required_Price__c = 8;
                prPandP.Product_lookup__c = pro.Id;
                prPandP.Valid_From__c = System.today();
                prPandP.Valid_To__c = System.today() + 2;
                prPandP.Sales_Office__c = 'ACGE';
                Insert prPandP;
            }   
            catch(exception ert)
            {
                //system.debug('The ert is:'+ert+ert.getLineNumber());
            }
            String invoiceName = 'pr1-7';
            
            List<Credit_Notes_Ticketing__c> crList = new List<Credit_Notes_Ticketing__c>();
            Credit_Notes_Ticketing__c creditNote = new Credit_Notes_Ticketing__c();
            //  creditNote.Approval_Status__c = 'Pending';
            creditNote.Quantity__c = '100';
            creditNote.Product_Name__c = 'Antalioe';
            creditNote.Remarks__c = 'NO Comment';
            creditNote.Plant_Location__c = 'BTM';
            creditNote.Item_Tonnage__c = 'test';
            creditNote.Invoice_Rate__c = '10';
            creditNote.Thickness__c = '50';
            creditNote.Dimension__c = '4';
            creditNote.National_Head__c = u4.Id;
            creditNote.NSM__c = u1.Id;
            creditNote.Regional_Head__c = u3.Id;
            // creditNote.Price_Request__c = pr.Id;
            creditNote.Invoice__c = inv.id;
            crList.add(creditNote);
            Insert crList;    
            
            Test.setCurrentPage(Page.CreditNotesTicketingMassApprovalVf);
            ApexPages.StandardSetController stdSetController = new ApexPages.StandardSetController(crList);
            stdSetController.setSelected(crList);
            CreditNotesTicketingMassApproval ext = new CreditNotesTicketingMassApproval(stdSetController);
            //Submit Quote for Approval
            Approval.ProcessSubmitRequest app = new Approval.ProcessSubmitRequest();
            app.setObjectId(crList[0].Id);
            Approval.ProcessResult result = Approval.process(app);
            ext.validateLoginUser();
            ext.approveRecs();
            ext.approveByBusinessInspire();
            ext.approveByBusinessManager();
          //  ext.approveByBusinessNationalHead();
            ext.cancelPage();
          //  ext.rejectRecs();  
            
        }
        // 2nd user
        /*
        System.runAs(u6) {   
            
            CreditNotesTicketingMassApproval__c prm4 = new CreditNotesTicketingMassApproval__c();
            prm4.Name='Regional Manager East Distribution';
            prm4.Role_Name__c ='Regional Manager East Distribution';
            prm4.List_view_URL__c = 'https://www.saint-gobain.com';
            insert prm4;
            
            CreditNotesTicketingMassApproval__c prm5 = new CreditNotesTicketingMassApproval__c();
            prm5.Name='Default';
            prm5.Role_Name__c ='Default';
            prm5.List_view_URL__c = 'https://www.saint-gobain.com';
            insert prm5;
            
            CreditNotesTicketingMassApproval__c prm6 = new CreditNotesTicketingMassApproval__c();
            prm6.Name='HO Distribution';
            prm6.Role_Name__c ='HO Distribution';
            prm6.List_view_URL__c = 'https://www.saint-gobain.com';
            insert prm6;
            
            CreditNotesTicketingMassApproval__c prm7 = new CreditNotesTicketingMassApproval__c();
            prm7.Name='National Head Distribution';
            prm7.Role_Name__c ='National Head Distribution';
            prm7.List_view_URL__c = 'https://www.saint-gobain.com';
            insert prm7;
            
            State__c st1 = new State__c();
            st1.Name = 'Raj';
            Insert st1;
            
            City__c ct1 = new City__c();
            ct1.Name = 'Bangre';
            ct1.State__c = st1.Id;
            Insert ct1;
            
            Project__c project1 = new Project__c();
            project1.Name = 'fgf';
            project1.Segment__c = 'Retaigl';
            project1.Region__c = 'East';
            project1.Sales_Area__c = 'AP2';
            project1.Validated__c = true;
            Insert project1;
            
            Account acc1 = new Account();
            acc1.Name = 'teacc';
            acc1.Type = 'Dealegr';
            acc1.SAP_Code__c = '111111';
            acc1.Sales_Area_Region__c = 'West';
         //   acc1.SAP_Code__c = '111222';
            acc1.RecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('ERP').getRecordTypeId();
            Insert acc1;
            
            
            Opportunity oppo1 = new Opportunity();
            oppo1.Name = 'jh';
            oppo1.Project__c = project1.Id;
            oppo1.AccountId = acc1.Id;
            oppo1.Type = 'Infinity';
            oppo1.StageName = 'Specified';
            oppo1.Probability = 10;
            oppo1.CloseDate = System.today();
            Insert oppo1;
            
            Key_Account__c keyAcct1 = new Key_Account__c();
            keyAcct1.Opportunity__c = oppo1.Id;
            keyAcct1.Account__c = acc1.Id;
            insert keyAcct1;
            
            Product2 pro1 = new Product2();
            pro1.Name = 'xs';
            pro1.Product_Category__c = 'Inspire';
            Insert pro1;    
            
            Product_Stock__c pdStock1 = new Product_Stock__c();
            pdStock1.Plant_Code__c = '100';
            Insert pdStock1;
            
            Invoice__c inv1 = new Invoice__c();
            inv1.Name = 'pr1-2';
            inv1.Invoice_Line_Item_No__c = '222';
            inv1.Item_Tonnage__c = 'pr1-2';
            inv1.Invoice_Rate__c = 10;
            inv1.Quantity__c = 20;
            inv1.Product__c = pro1.Id;
            Insert inv1;
            
            Opportunity_Product__c oppoPro1 = new Opportunity_Product__c();
            oppoPro1.Product__c = pro1.Id;
            oppoPro1.Status__c = 'Selected';
            oppoPro1.Opportunity__c = oppo1.Id;
            Insert oppoPro1;
            
            try{
                Price_Request__c pr1 = new Price_Request__c();
                pr1.Approval_Status__c = 'Approved';
                pr1.Opportunity_Product__c = oppoPro1.Id;
                pr1.Requested_Quantity__c = 3;
                pr1.Price_Type__c = 'List Price';
                Insert pr1;
                
                price_request_PandP__c prPandP1 = new price_request_PandP__c();
                prPandP1.Customer__c = acc1.Id;
                prPandP1.Requested_Quantity__c = 4;
                prPandP1.Required_Price__c = 8;
                prPandP1.Product_lookup__c = pro1.Id;
                prPandP1.Valid_From__c = System.today();
                prPandP1.Valid_To__c = System.today() + 2;
                prPandP1.Sales_Office__c = 'BLR1';
                Insert prPandP1;
            }   
            catch(exception ert)
            {
                //system.debug('The ert is:'+ert+ert.getLineNumber());
            }
            String invoiceName = 'pr1-2';
            
            List<Credit_Notes_Ticketing__c> crList1 = new List<Credit_Notes_Ticketing__c>();
            Credit_Notes_Ticketing__c creditNote1 = new Credit_Notes_Ticketing__c();
            //  creditNote.Approval_Status__c = 'Pending';
            creditNote1.Quantity__c = 100;
            creditNote1.Product_Name__c = 'pp';
            creditNote1.Remarks__c = 'NO Comment';
            creditNote1.Plant_Location__c = 'gfhfBTM';
            creditNote1.Item_Tonnage__c = 'ty';
            creditNote1.Invoice_Rate__c = 10;
            creditNote1.Thickness__c = 50;
            creditNote1.Dimension__c = '4';
            creditNote1.National_Head__c = u7.Id;
            creditNote1.NSM__c = u5.Id;
            creditNote1.Regional_Head__c = u6.Id;
            // creditNote.Price_Request__c = pr1.Id;
            creditNote1.Invoice__c = inv1.id;
            crList1.add(creditNote1);
            Insert crList1;    
            
            Test.setCurrentPage(Page.CreditNotesTicketingMassApprovalVf);
            ApexPages.StandardSetController stdSetController1 = new ApexPages.StandardSetController(crList1);
            stdSetController1.setSelected(crList1);
            CreditNotesTicketingMassApproval ext1 = new CreditNotesTicketingMassApproval(stdSetController1);
            //Submit Quote for Approval
            Approval.ProcessSubmitRequest app1 = new Approval.ProcessSubmitRequest();
            app1.setObjectId(crList1[0].Id);
            Approval.ProcessResult result1 = Approval.process(app1);
            ext1.validateLoginUser();
            //   Test.StartTest();
            ext1.approveRecs();
            //   Test.StopTest();
            ext1.cancelPage();
            //   ext1.rejectRecs();
        }*/
    }
}
public class FilePreviewController {
    
    @AuraEnabled
    public static String getRelatedContentVersionIds(String opptyId){
        
        List<String> imgTypeLst = new List<String>{'JPG','PNG','JPEG'};
        Set<Id> cDset = new Set<Id>();

        List<ContentDocumentLink> conDocLink =[select id, LinkedEntityId,ContentDocumentId,
                                                   ContentDocument.FileType
                                                   From ContentDocumentLink 
                                                   where LinkedEntityId=:opptyId AND 
                                                   ContentDocument.FileType IN :imgTypeLst];

        if(conDocLink.size()>0){

            for(ContentDocumentLink cDlink: conDocLink){
                cDset.add(cDlink.ContentDocumentId);
            }
            
            List<ContentVersion> conVerLst = [SELECT id,ContentDocumentId 
                                              FROM ContentVersion
                                              WHERE ContentDocumentId IN : cDset AND IsLatest =true ];
            
            Set<Id> resultSetIds = (new Map<Id,ContentVersion>(conVerLst)).keySet();   
           
            List<WrapperFileId> wrplst = new List<WrapperFileId>();
            
            for(ContentVersion verObj : conVerLst){
                WrapperFileId wp = new WrapperFileId();
                
                wp.varContentDocumentId = verObj.ContentDocumentId ;
                wp.varContentVersionId = verObj.id ;
                
                wrplst.add(wp);
            }

             return JSON.serialize(wrplst, true);
        
        }else{

            return 'No Preview';
        }                                           
    
    }
    
    public class WrapperFileId{
        
        @AuraEnabled public ID varContentDocumentId;
        @AuraEnabled public ID varContentVersionId;
        
        public WrapperFileId(){}
        
    }
}
/* Created By         : Rajiv Kumar Singh
   Created Date       : 15/06/2018
   Class Name         : AccountLedgersBatch_Test
   Description        : 
   Last Modified By   : Rajiv Kumar Singh
   Last Modified Date : 15/06/2018
*/
@isTest
public class AccountBatch_Test {
    static testMethod void testLedgerBatch() {
        Date today = System.today();
        Account acc = new Account();
        acc.Name = 'ajeet';
        acc.SAP_Code__c='12345';
        acc.Status__c = true;
        acc.Invoice_Date__c = today;
        acc.Sales_Area_Region__c = 'East';
        acc.Type = 'PMC';
        insert acc;
        
        Account acc1 = new Account();
        acc1.Name = 'shekhawat';
        acc1.SAP_Code__c='123451';
        acc1.Status__c = true;
        acc1.Invoice_Date__c = today - 1000;
        acc1.Sales_Area_Region__c = 'East';
        acc1.Type = 'PMC';
        insert acc1;
        System.debug('date ' + acc1.Invoice_Date__c);
        Test.startTest();
        AccountBatch accBatch = new AccountBatch();
        Database.executeBatch(accBatch);
        Test.stopTest();        
    }
}
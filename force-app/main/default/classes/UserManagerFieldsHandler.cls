/* Author : Pallavi Chugh
 * Created Date : 02/01/2018 
Function params :1. Map<ownerid, List of records of type sobject belonging to that owner>
				 2. If the owner has been changed on parent and sobject in param above is a child then IsChild will be true(Child records getting updated)

Cases covered :
1. KAM is owner
	1.1 if owner manager is dnc manager then dncmanager = owner.manager
		1.1.1 if owner manager's manager is regional manager then regionalmanager = owner.manager.manager
		1.1.2 if owner manager's manager is not regional manager then regionalmanager = null
	1.2 if owner manager is KAM
		1.2.1 if owner manager's manager is regional manager then dncmanager = null and regionalmanager = owner.manager.manager
	1.3 if owner manager is regional manager then regionalmanager = owner.manager and dncmanager = null
2. DnC Manager is owner
	dncmanager = null
	2.1 if owner manager is regional manager then regionalmanager = owner.manager
	2.2 if owner manager is not regional manager then regionalmanager = null
3. Glass Future person is owner
	if owner manager is regional manager then regionalmanager = owner.manager and dncmanager = null
3. Owner is neither KAM nor DnC Manager then regionalmanager = null  and dncmanager = null
*/

public class UserManagerFieldsHandler {
    
    public void updatePVFields(Map<Id,List<sObject>> updateMap, Boolean IsChild){
        //System.debug('INSIDE update User Fields');  
        //System.debug('CHECK INSIDE update User Fields size:');  
        String dncManager ='';
        String regionalManager ='';
        
        if(!updateMap.isEmpty()){
        Set<Id> OwnerIdSet = new Set<Id>();
        OwnerIdSet = updateMap.keySet();
        
        List<List<sObject>> objList = updateMap.values();  
        List<sObject> objUpdateList = new List<sObject>();
        
        //System.debug('CHECK 1');
        Map<Id,User> ownerMap; 
        ownerMap = new Map<Id,User>([SELECT Id, Name, ProfileId, UserRoleId, UserRole.Name, Profile.Name, ManagerId,Manager.ManagerId ,Manager.Profile.Name, Manager.ProfileId,Manager.UserRole.Name,Manager.UserRoleId,Manager.Manager.Profile.Name,Manager.Manager.ProfileId,Manager.Manager.UserRoleId,Manager.Manager.UserRole.Name FROM User WHERE Id IN :OwnerIdSet]);
        //System.debug('CHECK ownerMap :'+ownerMap);  
        //Check to ensure map is not null
        if(updateMap != null){
        for(Id k : updateMap.keySet()){
            List<SObject> ownerRecordsList = updateMap.get(k);
            User u = ownerMap.get(k);
            if(u!= null && ownerRecordsList!=null){                     
                if(String.isNotBlank(u.UserRoleId)){//Role check important as owner role and his managers' roles important to determine owner manager values
                    //System.debug('CHECK 3');
                    if(u.Profile.Name.equals('Infinity KAM User')){//Profile name is unique
                        //System.debug('CHECK 4');
                        if(String.isNotBlank(u.ManagerId)&& String.isNotBlank(u.Manager.UserRoleId) && u.Manager.UserRole.Name.contains('D&C Manager')){
                            //System.debug('CHECK 5');
                            dncManager = u.ManagerId;
                            if(String.isNotBlank(u.Manager.ManagerId) && String.isNotBlank(u.Manager.Manager.UserRoleId)&& u.Manager.Manager.UserRole.Name.contains('Regional Manager') ){
                                //System.debug('CHECK 6');          
                                regionalManager = u.Manager.ManagerId;
                            }
                            else{
                                //System.debug('CHECK 7');             
                                regionalManager = null;
                            }
                        }
                        else if(String.isNotBlank(u.ManagerId) && u.Manager.Profile.Name.equals('Infinity KAM User')){
                            if(String.isNotBlank(u.Manager.ManagerId) && String.isNotBlank(u.Manager.Manager.UserRoleId)&& u.Manager.Manager.UserRole.Name.contains('Regional Manager') ){
                                //System.debug('CHECK 13');     
                                dncManager = null;
                                regionalManager = u.Manager.ManagerId;
                            }
                        }                       
                        else if(String.isNotBlank(u.ManagerId) && String.isNotBlank(u.Manager.UserRoleId) && u.Manager.UserRole.Name.contains('Regional Manager')){
                            //System.debug('CHECK 9');
                            regionalManager = u.ManagerId;
                            dncManager = null;
                        } 
                    }
                    else if(u.UserRole.Name.contains('D&C Manager') && String.isNotBlank(u.ManagerId)){
                        //System.debug('CHECK 10');
                        dncManager = null;
                        if(String.isNotBlank(u.ManagerId) && String.isNotBlank(u.Manager.UserRoleId) && u.Manager.UserRole.Name.contains('Regional Manager')){
                            //System.debug('CHECK 11');
                            regionalManager = u.ManagerId; 
                        }
                        else{
                            //System.debug('CHECK 12');
                            regionalManager = null;
                        }
                    }
                    else if(u.UserRole.Name.contains('Glass Future') && String.isNotBlank(u.ManagerId) && String.isNotBlank(u.Manager.UserRoleId) && u.Manager.UserRole.Name.contains('Regional Manager')){
                            //System.debug('CHECK 9');
                            regionalManager = u.ManagerId;
                            dncManager = null;
                        } 
                    else {
                        //System.debug('CHECK 14');
                        dncManager = null;
                        regionalManager = null;
                    }
                }    
                else {
                    //System.debug('CHECK 15');
                    dncManager = null;
                    regionalManager = null;
                }
                //System.debug('CHECK 16');
            }
            
            for (SObject s : ownerRecordsList){
                s.put('D_C_Manager__c',dncManager);
                s.put('Regional_Manager__c',regionalManager);		
                objUpdateList.add(s);
                //System.debug('CHECK updatelist id :'+s.get('Id'));
                //System.debug('CHECK DNC :'+s.get('D_C_Manager__c'));
                //System.debug('CHECK RM :'+s.get('Regional_Manager__c'));
            }                  
        } 
    }
    
        
        if(IsChild){
            //System.debug('CHECK updatelist size :'+objUpdateList.size());
            update objUpdateList; 
        }	 
            
        }
        
    }
}
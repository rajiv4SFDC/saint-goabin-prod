@istest
private class createSalesOrderInovice_test
{
 
  @istest private static void testmethod1()
  {
     // creating test data.
     Sales_Order_Line_Item__c so = new Sales_Order_Line_Item__c();
    // so.Enquiry_Line_Item__c = ;
     so.External_Id__c = 'test-external-001';
     so.Shipping_Status__c ='Invoiced';
     insert so;
     
     invoice__c inv = new invoice__c();
     inv.name = so.External_Id__c;
     inv.Invoice_type__c = 'Infinity';
     inv.Sales_order__c = so.External_Id__c;
     inv.Status__c = 'Invoiced';
     insert inv;
     
     invoice__c inv1 = [select id,name from invoice__c where id=:inv.id];
    List<Id> inoviceIds = new list<Id>();
    inoviceIds.add(inv1.id);
     createSalesOrderInovice.UpdateLookupValue(inoviceIds);
  }

}
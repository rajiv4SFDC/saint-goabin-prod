@istest
private class NewEnquiryComponentController_test
{

 @istest private static void tesMethod1()
 {
   // code coverage for getSalesAreaValues().*************************************************
   NewEnquiryComponentController.getSalesAreaValues();
   test.starttest();
   
  Id erpid = [select id,name,SobjectType from recordtype where SobjectType='Account' AND name='ERP'].id;

   
   // code coverage for getBillToCustomers2()
    account a1 = new account();
    a1.Name ='tesitng 123';
    a1.Sales_Office_ERP__c ='CAL1';
    a1.Status__c = true;
    a1.BillTo__c = true;
    a1.Ship_to_customer__c = true;
    a1.recordTypeId =erpid;
    a1.SAP_Code__c = '123123';
    insert a1;
    
    Ship_To_Customer__c st = new Ship_To_Customer__c();
    st.Name ='testing ship to cus1';
    st.Account__c = a1.id;
    st.Address__c ='testing testing';
    st.Ship_Unique_Code__c ='12312';
    insert st;


   NewEnquiryComponentController.getBillToCustomers2(a1.Sales_Area__c,'NOT ORS');
   NewEnquiryComponentController.getBillToCustomers2(a1.Sales_Area__c,'ORS');   
   
   // code coverage for getShipToCustomers()*************************************************
   NewEnquiryComponentController.getShipToCustomers(a1.id);
   
   // code coverage for getEmptyLineItem()*************************************************
   NewEnquiryComponentController.getEmptyLineItem();
   
   
   // code coverage for getCategoryChange() *************************************************
 
        Product2 prod = new Product2();
        prod.Name = 'COOL-LITE K-KB T140-CLEAR';
        prod.ProductCode = '401-181-000-000-00';
        prod.Type__c = 'Interior';     
        prod.Product_Category__c ='Interior';      
        prod.Thickness_Code__c = '0500';
        prod.External_Id__c = '401-181-000-000-00'+ '0500';
        prod.List_Price__c = 1500;
        prod.Description = 'COOL-LITE K-KB T140-CLEAR';
        prod.Family = 'SOLAR C';
        prod.Sub_Family__c = 'FLT CLEAR';
        prod.Special_Price_Flag__c = 'X';
        prod.IsActive = true;
        //prod.Product_Category__c = 'Inspire';
        prod.Thickness__c = '4.00';
        insert prod;
        
       product2 prodRec = [select id,name from product2 where id=:prod.Id];
        system.debug('test class product info:'+prodRec.name);
        // creating product tier pricing here. 
        Product_Tier_Pricing__c ptr = new Product_Tier_Pricing__c();
        ptr.Name = 'Testing tier pricing';
      
        ptr.Tier_Name__c = 'T1';
        ptr.Tier_Pricing__c = 200;
        
        ptr.Business_Location__c = 'DEL1-INF';
        ptr.Product__c = prod.id;
        ptr.Product_Code__c = prod.ProductCode;
        insert ptr;      
        
        Product_Dimension__c pd = new Product_Dimension__c();
        pd.Length__c = '10';
        pd.Width__c = '10';
        pd.Product_External_ID__c ='401-181-000-000-00';
        pd.Product_Item_Code__c ='401-181-000-000-00'+ '0500';
        pd.Product__c =prod.id;
        pd.Dimension_Unique_Key__c ='1232123';
        insert pd;
system.debug('The pd is '+pd);
        NewEnquiryComponentController.getCategoryChange(prod.Type__c);
   
   
      // code coverage for  getProductChange()  *************************************************
        NewEnquiryComponentController.getProductChange(prodRec.Name);
   
      // code coverage for  getLengthAndWidth() *************************************************
        NewEnquiryComponentController.getLengthAndWidth(prodRec.Name,'INPSIRE');
        
        // code coverage for  getPackType() *************************************************
        NewEnquiryComponentController.getPackType(prodRec.Name);
        
        // code coverage for getonPriceChange() *************************************************
        string priceType1 = 'List';
        string priceType2 = 'Tier';
        
        NewEnquiryComponentController.displayORSInformation();
        
       
        
        NewEnquiryComponentController.displayRecentInvoiceRate(a1.id,'COOL-LITE K-KB T140-CLEAR-5mm');
        
        NewEnquiryComponentController.displayProductstockInformation('COOL-LITE K-KB T140-CLEAR-5mm','10-10');
        
        NewEnquiryComponentController.getonPriceChangeTierPrice('COOL-LITE K-KB T140-CLEAR-5mm','CAL1','Tier','Interior',a1.id);
        
        NewEnquiryComponentController.getonPriceChange(prodRec.Name,a1.Sales_Office_ERP__c,priceType1,prod.Product_Category__c,string.valueof(a1.id));
        NewEnquiryComponentController.getonPriceChange(prodRec.Name,a1.Sales_Office_ERP__c,priceType2,prod.Product_Category__c,string.valueof(a1.id));
        
        // code coverage for saveEnquiryRec() *************************************************
       string tempvar =   NewEnquiryComponentController.saveEnquiryRec(a1.Sales_Office_ERP__c,a1.id,a1.id);
          NewEnquiryComponentController.saveEnquiryRec(a1.Sales_Office_ERP__c,a1.id,st.id);
          system.debug('The tempvar is :'+tempvar);
          // code coverage for getEnquiryRecord() *************************************************
          enquiry__c enq =[select id,name from enquiry__c limit 1];          
          NewEnquiryComponentController.getEnquiryRecord(enq.id);
          NewEnquiryComponentController.getAccStatus(enq.id);
          NewEnquiryComponentController.displayEnquiryLineItemStatusRec(enq.id);
          // code coverage for updateEnqRecord() *************************************************
          NewEnquiryComponentController.updateEnqRecord(enq.id,a1.Sales_Office_ERP__c,a1.id,st.id);
          
           // code coverage for saveLineItemRec() *************************************************
        NewEnquiryComponentController.saveLineItemRec(prod.Type__c,prodRec.Name,prod.Thickness__c,pd.Length__c,
                                         '1.0','10','UOM test','12',12,'List','12','12','2018-06-11','2018-06-11','test remarks 123',string.valueof(enq.id),'12',NULL,12,'12PrTesting','123',true,'100.00');
                    
 
            // code coverage for getExistingEnqLineitemRec() *************************************************
            Enquiry_Line_Item__c  enqLineItem =  NewEnquiryComponentController.saveLineItemRec(prod.Type__c,prodRec.Name,prod.Thickness__c,pd.Length__c,
                                         '1.0','10','UOM test','12',12,'List','12','12','2018-06-11','2018-06-11','test remarks 123',string.valueof(enq.id),'12',NULL,12,'12PrTesting','123',true,'100.00');
           
           system.debug('The enqLineItem is :' +enqLineItem.Id+'....'+enqLineItem.Remarks__c);
           
           // code coverage for    getExistingEnqLineitemRec()*************************************************         
            NewEnquiryComponentController.getExistingEnqLineitemRec(enqLineItem.Id);
            
          // code coverage for    submitEnquiry()*************************************************   
            NewEnquiryComponentController.submitEnquiry(string.valueof(enq.id));
     
             // code coverage for    getLineItemsIds()*************************************************         
            NewEnquiryComponentController.getLineItemsIds(enqLineItem.Id);
            
             // code coverage for    addrow()*************************************************         
            NewEnquiryComponentController.addrow();
            
             // code coverage for    displayExistingLineItems()*************************************************         
            NewEnquiryComponentController.displayExistingLineItems(string.valueof(enq.id));
 
             // code coverage for    deleteEnqLineItem()*************************************************         
            NewEnquiryComponentController.deleteEnqLineItem(string.valueof(enqLineItem.Id));
            
            
            // code coverage for    savePRrecord()*************************************************    
                     id cnid;
        Account acc1 = new Account();
        acc1.Name ='test31222';
        acc1.Type ='Industry';
        acc1.SAP_Code__c='12314';
        acc1.Mirror_Tier__c ='T1';
        acc1.Sales_Area_Region__c ='North';
        acc1.Sales_Office_ERP__c='BLR';
        acc1.RecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('ERP').getRecordTypeId();
        insert acc1;
        test.stoptest();
        
        Opportunity opp = new Opportunity();
        String oppid1;
        opp.AccountId =acc1.Id;
        opp.Name ='test1';
        opp.CloseDate = Date.today();
        opp.Amount = 1000;
        opp.StageName ='Prospecting';
        opp.Probability= 0.5;
        oppid1=acc1.id;
        insert opp;
        
        Account acc = new Account();
        acc.Name ='test312';
        acc.Type ='Industry';
        acc.Sales_Area_Region__c ='South';
        acc.SAP_Code__c='12345';
        acc.RecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('CRM').getRecordTypeId();
        insert acc;
        
        Key_Account__c keyAcct = new Key_Account__c();
        keyAcct.Opportunity__c = opp.Id;
        keyAcct.Account__c = acc.Id;
        insert keyAcct;
        
        opp.Customer_Name__c = keyAcct.Id;
        update opp;
        
        Product2 masterProduct = new Product2();
        masterProduct.Name = opp.id;
        masterProduct.Article_number__c ='test1234';
        masterProduct.Product_Category__c ='Shower Cubicles';
        masterProduct.master_product__c = true;
        insert masterProduct;
        
        Product2 pro = new Product2();
        pro.Name = 'test prod123';
        pro.Product_Category__c ='Shower Cubicles';
        pro.ProductCode ='test12312';
        pro.Parent_product__c = masterProduct.id;
        pro.Article_number__c = 'test1231';
        pro.Type__c ='Mirror';
        pro.Quantity__c  = 2;
        pro.global_product__c = true;
        insert pro;   
        List<Product2> prodList = new List<product2>();
        prodList.add(pro);
        
        Product_Tier_Pricing__c ptr1 = new Product_Tier_Pricing__c();
        ptr1.name ='testing tier';
        ptr1.Business_Location__c ='BLR';
        ptr1.Product__c = pro.id;
        ptr1.Product_Code__c ='test12312';
        ptr1.Tier_Name__c ='T1';
        ptr1.Thickness__c =22;
        insert ptr1;
        
        
        
        Opportunity_Product__c oppproduct = new Opportunity_Product__c();
        oppproduct.Opportunity__c = opp.Id;
        oppproduct.Product__c = pro.Id;
        insert oppproduct;
        
        
        price_request_PandP__c PRrec = new price_request_PandP__c();
        PRrec.Requested_Quantity__c = 1;
         PRrec.Location2__c = 'Banfalore';
        PRrec.Quantity_Approved__c = 1;
        PRrec.Discount__c = 1;      
                  
    NewEnquiryComponentController.savePRrecord(PRrec,'List',a1.Sales_Office_ERP__c,prod.Type__c,a1.id,'2018-06-11','2018-06-11','1','Testing remarks',string.valueof(enq.id),prodRec.Name,100,123,'2323-2323','Admin','20');
          
            
            
            
              // code coverage for    getPriceRequestOnSearch()*************************************************                       
            NewEnquiryComponentController.getPriceRequestOnSearch('test prd name','AP1',a1.id,'CLEAR');
            
          // code coverage for    GetFieldPikclistValues()*************************************************                       
            NewEnquiryComponentController.GetFieldPikclistValues('Account','sales_area__c');     
            

 
 }
    
 }
/* Created By         : Rajiv Kumar Singh
   Created Date       : 15/06/2018
   Class Name         : UpdateCustomerLookupOfInvoice
   Description        : update the particular Account record lookup field to Invoice
   Last Modified By   : Rajiv Kumar Singh
   Last Modified Date : 15/06/2018
*/
public class UpdateCustomerLookupOfInvoice{
  @InvocableMethod(label='UpdateCustomerLookupOfInvoice' description='update the particular Account record lookup field to Invoice')
  public static void UpdateLookupValue(List<Id> invoiceIds)
    {   
       map<string,string> invoiceWithAccountMap = new map<string,string>();  
       set<string> presentSAPCodesSet = new set<String>();
       list<Invoice__c > NewList = new list<Invoice__c>(); 
       list<Invoice__c > FinalListToUpdate = new list<Invoice__c>();

       for(Invoice__c ptp:[select id,Customer_code__c from Invoice__c where id in : invoiceIds])
       {
         if(ptp.Customer_code__c != null)
          {
            presentSAPCodesSet.add(ptp.Customer_code__c);      
            NewList.add(ptp);
            system.debug('The ptp.id is: ' +ptp.id);
          }     
       }
       system.debug('The NewList is: ' +NewList+'......'+presentSAPCodesSet);
       if(presentSAPCodesSet.size()>0)
       {
          for(Account acc:[select id, SAP_Code__c from Account where SAP_Code__c in:presentSAPCodesSet])
           {
               invoiceWithAccountMap.put(acc.SAP_Code__c,acc.id);
           }
           system.debug('The invoiceWithAccountMap is: ' +invoiceWithAccountMap);
           for(Invoice__c ptp:NewList)
           {
               ptp.Customer__c = invoiceWithAccountMap.get(ptp.Customer_code__c);
               system.debug('The  ptp.Customer_code__c is: ' + ptp.Customer_code__c);
               FinalListToUpdate.add(ptp);
           }
       }
       if(!FinalListToUpdate.isEmpty() && FinalListToUpdate.size() > 0) {
            update FinalListToUpdate;
       }
    }
}
/* Created By: Rajiv Kumar Singh
Created Date: 14/06/2017
Class Name: NewProcessorSapCodeUpsertAPITest
Description : Test class for NewProcessorSapCodeUpsertAPI
Last Modified By :Rajiv Kumar Singh
Last Modified Date: 14/06/2017
*/

@IsTest
private class NewProcessorSapCodeUpsertAPITest{
    
    static testMethod void testPostMethod(){
        String JSONMsg = '[{"NewprocessorID":"001N000001Ji3L0","SapCode":"3123"}]';
        RestRequest request = new RestRequest();
        request.requestUri ='/services/apexrest/NewCustomerUpsertAPI';
        request.httpMethod = 'POST';
        request.requestBody = Blob.valueof(JSONMsg);
        RestContext.request = request;
        RestContext.response = new RestResponse();
        NewProcessorSapCodeUpsertAPI.doHandleInsertRequest();
        
        JSONMsg = '[1';
        request = new RestRequest();
        request.requestUri ='/services/apexrest/NewCustomerUpsertAPI';
        request.httpMethod = 'POST';
        request.requestBody = Blob.valueof(JSONMsg);
        RestContext.request = request;
        RestContext.response = new RestResponse();
        NewProcessorSapCodeUpsertAPI.doHandleInsertRequest();
        
        request = new RestRequest();
        request.requestUri ='/services/apexrest/NewCustomerUpsertAPI';
        request.httpMethod = 'POST';
        request.requestBody = Blob.valueof('');
        RestContext.request = request;
        RestContext.response = new RestResponse();
        NewProcessorSapCodeUpsertAPI.doHandleInsertRequest();
    }
}
@isTest
public class ProductDimensionTest {
    static testMethod void dimension() {
        Id product2Id;
        /* create instance of Product2 object and Inserting data into fields */ 
        Product2 product2Instance = new Product2();
        product2Instance.Name = 'steel';
        product2Instance.Product_Category__c = 'Infinity';
        product2Instance.Thickness_Code__c = '0800';
        insert product2Instance;
        for(Product2 productDimension : [select id from Product2]) {
            product2Id = productDimension.Id;
        }
        /* create instance of Material_Request object and Inserting data into fields */ 
        Material_Request__c matRequest = new Material_Request__c();
        LIST<Material_Request__c> materialInsertList = new LIST<Material_Request__c>();
        LIST<Material_Request__c> materialUpdateList = new LIST<Material_Request__c>();
        
        matRequest.Length__c = '10';
        matRequest.Width__c = '15';
        matRequest.Approval_Status__c = 'Approved';
        materialInsertList.add(matRequest);
        insert materialInsertList;
        /* Reinitialize instance of Material_Request to update data */
        matRequest.Length__c = '20';
        matRequest.Length__c = '40';
        materialUpdateList.add(matRequest);
        update materialUpdateList;    
        for(Material_Request__c matReq : [select id, Approval_Status__c from Material_Request__c where Product__c =: product2Id]) {
            if(matReq.Approval_Status__c == 'Approved') {
                
            }
        }
        
    }
}
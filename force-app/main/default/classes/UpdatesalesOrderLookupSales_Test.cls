/* Created By         : Rajiv Kumar Singh
   Created Date       : 11/06/2018
   Class Name         : UpdatesalesOrderLookupSalesOrderStatus_Test
   Description        : 
   Last Modified By   : Rajiv Kumar Singh
   Last Modified Date : 11/06/2018
*/
@isTest
private class UpdatesalesOrderLookupSales_Test
{ 
 // @istest
   private testmethod static void testMethod1()
   {
      Sales_Order_Line_Item__c  p1 = new Sales_Order_Line_Item__c ();
    //  p1.Name = 'testing';
      p1.Sales_Order_Line_Item_Number__c ='East';
   //   p1.SAP_Code__c = '6565666';
      insert p1;
      datetime StartDateTime;
      Sales_Order_Status__c ptp = new Sales_Order_Status__c();
      ptp.Sales_Order_Num__c = 'East';
      ptp.Sales_Order_Line_Item__c  = p1.id;
      ptp.Order_Status__c ='Invoiced';
      StartDateTime = datetime.newInstance(2014, 9, 15, 12, 30, 0);
      ptp.Status_Timestamp__c = StartDateTime;
      insert ptp;
      
      Sales_Order_Status__c pt = [select id,Sales_Order_Num__c from Sales_Order_Status__c where id =:ptp.id];
      
    //  System.assertEquals(pt.product__c,p1.id);
      list<Id> ptrList = new list<Id>();
      ptrList.add(pt.id);
      
      UpdatesalesOrderLookupSalesOrderStatus.UpdateLookupValue(ptrList);
   }
}
public class PlantVisitTriggerHandler extends TriggerHandler {
    
    public PlantVisitTriggerHandler() {}
    
    public override void beforeInsert() {
        //System.debug('PV before Insert Start');
        new PlantVisitManager().fetchBeforeInsertRecordsForUpdation((List<Plant_Visit__c>)Trigger.new );
        //System.debug('PV before Insert end');
    }
    
    public override void beforeUpdate() {
        //System.debug('PV before update Start');
        new PlantVisitManager().fetchBeforeUpdateRecordsForUpdation((List<Plant_Visit__c>)Trigger.new ,(Map<Id,Plant_Visit__c>)Trigger.oldMap);
        //System.debug('PV before update end');
    }  
   
}
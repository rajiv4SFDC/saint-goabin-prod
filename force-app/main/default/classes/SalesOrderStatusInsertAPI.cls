/* Created By: Rajiv Kumar Singh
Created Date: 03/07/2018
Class Name: TerritoryWiseNODInsertAPI 
Story : Sales order status update with timestamp from SAP to SFDC
Last Modified By :Rajiv Kumar Singh
Last Modified Date: 03/07/2018
*/

@RestResource(urlMapping='/SalesOrderStatus')
global class SalesOrderStatusInsertAPI{
    @HttpPost
    global static void doHandleInsertRequest(){
        
        List<SalesOrderStatusWrapper> wrapList = new List<SalesOrderStatusWrapper>();
        List<Sales_Order_Status__c> SOSList = new List<Sales_Order_Status__c>();
        RestRequest Req = RestContext.request;
        RestResponse res = RestContext.response;
        String body = Req.requestBody.toString().trim();
        
        if(body != null && body != '') {
            
            try {
                wrapList = (List<SalesOrderStatusWrapper>)JSON.deserialize(body, List<SalesOrderStatusWrapper>.class);
                
                for(SalesOrderStatusWrapper we :wrapList){
                    Sales_Order_Status__c sos = new Sales_Order_Status__c();
                    
                    if(we.Salesorder!= null && we.Salesorder!=''){
                        sos.Sales_Order_Num__c = we.Salesorder;
                    }
                    if(we.orderstatus!= null && we.orderstatus!=''){
                        sos.Order_Status__c = we.orderstatus;
                    }
                    if(we.StatusTimestamp!= null && we.StatusTimestamp!=''){
                        
                        //24 hour format
                        String[] toDates = we.StatusTimestamp.split(' ');
                        String[] dateVal = toDates[0].split('/');
                        String[] timeVal = toDates[1].split(':');
                        DateTime dt = DateTime.newInstance(Integer.valueOf(dateVal[2]), Integer.valueOf(dateVal[1]), Integer.valueOf(dateVal[0]), Integer.valueOf(timeVal[0]), Integer.valueOf(timeVal[1]), Integer.valueOf(timeVal[2]));
                        //String strConvertedDate = dt.format('yyyy-MM-dd HH:mm:ss', 'Asia/Kolkata'); //India timezone
                        sos.Status_Timestamp__c = dt;
                        system.debug('dt:::'+dt);
                    }
                    
                    SOSList.add(sos);
                }
                
                if(SOSList.size() > 0){
                    Insert SOSList;
                }
                
                res.statusCode = 200;
                res.responseBody = Blob.valueOf(String.valueof(SOSList));
                
            }catch (Exception e) {
                
                CommonUtilitiesHandler.handleExceptions(e);
                res.statusCode = 500;
                res.responseBody = Blob.valueOf(e.getMessage());
            }
        } else {
            res.statusCode = 500;
            res.responseBody = Blob.valueOf('Invalid Request');
        }
    }
    
    public class SalesOrderStatusWrapper{
        public String Salesorder{get;set;}
        public String orderstatus{get;set;}
        public String StatusTimestamp{get;set;}
       
        
    }
}
/* Created By         : Rajiv Kumar Singh
   Created Date       : 12/06/2018
   Class Name         : UpdateLookupFromShipToEnquiry
   Description        : update the particular Account record lookup field to Enquiry
   Last Modified By   : Rajiv Kumar Singh
   Last Modified Date : 12/06/2018
*/
public class UpdateLookupFromShipToEnquiry{
  @InvocableMethod(label='UpdateLookupFromShipToEnquiry' description='update the particular Account record lookup field to Enquiry')
  public static void UpdateLookupValue(List<Id> shipTOCustomerIds)
    {   
       map<string,string> enquiryWithAccountMap = new map<string,string>();  
       set<string> presentSAPCodesSet = new set<String>();
       list<Enquiry__c > NewList = new list<Enquiry__c>(); 
       list<Enquiry__c > FinalListToUpdate = new list<Enquiry__c>();

       for(Enquiry__c ptp:[select id,Ship_To_SAP_Code__c from Enquiry__c where id in:shipTOCustomerIds])
       {
         if(ptp.Ship_To_SAP_Code__c != null)
          {
            presentSAPCodesSet.add(ptp.Ship_To_SAP_Code__c);      
            NewList.add(ptp);
            system.debug('The ptp.id is: ' +ptp.id);
          }     
       }
       system.debug('The NewList is: ' +NewList+'......'+presentSAPCodesSet);
       if(presentSAPCodesSet.size()>0)
       {
          for(Ship_To_Customer__c acc:[select id,Customer_Code__c  from Ship_To_Customer__c where Customer_Code__c in:presentSAPCodesSet])
           {
               enquiryWithAccountMap.put(acc.Customer_Code__c,acc.id);
           }
           system.debug('The enquiryWithAccountMap is: ' +enquiryWithAccountMap);
           for(Enquiry__c ptp:NewList)
           {
               ptp.Ship_To_Customer__c = enquiryWithAccountMap.get(ptp.Ship_To_SAP_Code__c);
               system.debug('The  ptp.Ship_To_Customer__c is: ' + ptp.Ship_To_Customer__c);
               FinalListToUpdate.add(ptp);
           }
       }
       if(!FinalListToUpdate.isEmpty() && FinalListToUpdate.size() > 0) {
            update FinalListToUpdate;
       }
    }
}
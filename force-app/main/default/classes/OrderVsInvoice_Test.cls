@isTest
public class OrderVsInvoice_Test {
    @isTest static void TestInvoiceAndOrder(){
        
        Account acc= new account(name='test',Sales_Area_Region__c='North',Sales_Office_ERP__c='DEL1',SAP_Code__c='11');
        insert acc;
        
        account ac = new account(name='test1',Sales_Area_Region__c='South',Sales_Office_ERP__c='BLR1',SAP_Code__c='12');
        insert ac;
        
        product2 product = new product2(Name='test',ProductCode='12434',Description='test',Product_Category__c='infinity',
                                        Sub_Family__c='FLT Clear',Coating__c='silver',Tint__c='Clear',Reporting_range__c='Clear_Thick'); 
        insert product;
        
        list<sales_order_line_item__c> orderList = new List<sales_order_line_item__c>();
        sales_order_line_item__c salesOrder = new sales_order_line_item__c(Sales_Order_Date__c=date.today(),Quantity_in_Tons__c=1234,product__c=product.id,Bill_To_SAP_Code__c='11'); 
        insert salesOrder;
        
        sales_order_line_item__c order1 = new sales_order_line_item__c(Sales_Order_Date__c=date.today(),Quantity_in_Tons__c=123,product__c=product.id,Bill_To_SAP_Code__c='11');
        insert order1;
        
        
        List<invoice__c> invoiceList = new list<invoice__c>();
        invoice__c invoice=new invoice__c(Product__c=product.id,Sales_Office_for_ordinv__c=acc.id,
                                          Billing_Date__c=date.today(),Item_Tonnage_for_Report__c=1234,Customer_code__c='11');
        
        invoiceList.add(invoice);
        
        invoice__c invoice1 = new invoice__c(Product__c=product.id,Sales_Office_for_ordinv__c=ac.id,
                                             Billing_Date__c=date.today(),Item_Tonnage_for_Report__c=12);
        insert invoice1;
        // invoiceList.add(invoice1);
        
        invoice__c invoice2 = new invoice__c(Product__c=product.id,Sales_Office_for_ordinv__c=acc.id,
                                             Billing_Date__c=date.today(),Item_Tonnage_for_Report__c=12);
        invoiceList.add(invoice2);
        insert invoiceList;
        
        set<String> stringInvoiceKeySet = new set<string>();
        stringInvoiceKeySet.add('North'+'-'+'Clear_Thick'+'-'+'DEL1');
        orderVsInvoice od = new orderVsInvoice();
        od.monthNum=date.today().month();
        od.yearNum=date.today().year();
        od.selectedOffice='BLR1';
        List<selectOption> RegionOptions = od.RegionOptions;
        List<selectOption> RangeOptions = od.RangeOptions;
        List<selectOption> salesAreaOptions= od.salesOfficeOptions;
        orderVsInvoice.ModelClass mdclass= new orderVsInvoice.ModelClass();
        mdclass.invoiceVal=324;
        mdclass.orderVal=5342;
        mdclass.region='North';
        mdclass.reportingRange='Clear_Thick';
        mdclass.SalesOffice='DEL1';
        
        orderVsInvoice.wrapperaccount wp = new orderVsInvoice.wrapperaccount();
        wp.accid=acc.id;
        wp.acc='test';
        wp.Sapcode='234243';
        wp.ordersize=12.0;
        wp.invoiceSize=12.0;
        
        // Nothing is selected
        od.selectedRegion = '-select-';
        od.selectedRange = '-select-';
        od.selectedOffice='-select-';
        string checkIt='North'+'-'+'Clear_Thin'+'-'+'DEL1';
        LIst<sales_order_line_item__c> items = [select id from sales_order_line_item__c where Quantity_in_Tons__c!=null
                                                AND reporting_range_workflow__c!=null AND order_office_for_ordinv__c!= null
                                                AND Region_Workflow__c!=null];   
        system.assertEquals(2, items.size());
        
        od.DoLoadData();
        od.kendoChart(date.today(),'-select-','-select-','-select-',stringInvoiceKeySet);
        
        od.office='DEL1';
        od.CustomerDetails();
        
        // All Fields selected        
        od.selectedRegion = 'North';
        od.selectedRange = 'Mirror';
        od.selectedOffice='DEL1';
        od.DoLoadData();
        od.kendoChart(date.today(),od.selectedRange,od.selectedRegion,od.selectedOffice,stringInvoiceKeySet);
        
        
        // Only Reporting Range selected
        od.selectedRegion = '-select-';
        od.selectedRange = 'Clear_Thick';
        od.selectedOffice='-select-';
        od.DoLoadData();
        od.kendoChart(date.today(),od.selectedRange,od.selectedRegion,od.selectedOffice,stringInvoiceKeySet);
        
        // Only Region is selected
        od.selectedRange = '-select-';
        od.selectedRegion = 'North';
        od.selectedOffice='-select-';
        od.DoLoadData();
        od.kendoChart(date.today(),od.selectedRange,od.selectedRegion,od.selectedOffice,stringInvoiceKeySet);
        
        // only range,region selected
        od.selectedRange = 'Clear_Thick';
        od.selectedRegion = 'North';
        od.selectedOffice='-select-';
        od.DoLoadData();
        od.kendoChart(date.today(),od.selectedRange,od.selectedRegion,od.selectedOffice,stringInvoiceKeySet);
        
        // Only Office and Range is selected
        od.selectedRange = 'Clear_Thick';
        od.selectedRegion = '-select-';
        od.selectedOffice='BLR1';
        od.DoLoadData();
        od.kendoChart(date.today(),od.selectedRange,od.selectedRegion,od.selectedOffice,stringInvoiceKeySet);
        
        // Only Office and Region is selected
        od.selectedRange = '-select-';
        od.selectedRegion = 'North';
        od.selectedOffice='DEL1';
        od.DoLoadData();
        od.kendoChart(date.today(),od.selectedRange,od.selectedRegion,od.selectedOffice,stringInvoiceKeySet);
        
        // Only Office is selected
        od.selectedRange = '-select-';
        od.selectedRegion = '-select-';
        od.selectedOffice='DEL1';
        od.DoLoadData();
        od.kendoChart(date.today(),od.selectedRange,od.selectedRegion,od.selectedOffice,stringInvoiceKeySet);
        
    }
}
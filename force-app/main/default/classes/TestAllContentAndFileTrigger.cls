@isTest
public class TestAllContentAndFileTrigger {

        @testSetup 
        public static void createTestData(){
            TestDataFactory.createCustomSetting();
        TestDataFactory.createTestProductData();
        TestDataFactory.createTestStateAndCityRecordData();
        TestDataFactory.createTestCatalogueBudgetData();
        TestDataFactory.createTestCampaignRecordData();
        TestDataFactory.createTestAccountData();
        TestDataFactory.createTestProjectData();
        TestDataFactory.createTestOpportunityData();
        TestDataFactory.createTestOpportunityProductData();
        TestDataFactory.createTestPriceRequestData();
        TestDataFactory.createTestProductForecateData();
        TestDataFactory.createTestForecateInvoiceData();
        TestDataFactory.createTestMockupRecordData();
        TestDataFactory.createTestInvoiceRecordData();
        TestDataFactory.createTestTargetActualRecordData();
        }

        @isTest
        public static void createContentFileRecords(){
               Project__c proj = [SELECT id FROM Project__c LIMIT 1];
               Mock_Up_Request__c mockReq = [SELECT id FROM Mock_Up_Request__c LIMIT 1];
               
               String before = 'Testing base 64 encode';            
               Blob beforeblob = Blob.valueOf(before);

               //Insert contentdocument data
                List<ContentVersion> lstOfContVer = new List<ContentVersion>();
                ContentVersion cv = new ContentVersion();
                cv.title = 'test content trigger';      
                cv.PathOnClient ='test.jpg';           
                cv.VersionData =beforeblob;          

                ContentVersion cv2 = new ContentVersion();
                cv2.title = 'test content trigger';      
                cv2.PathOnClient ='test.jpg';           
                cv2.VersionData =beforeblob;          

                lstOfContVer.add(cv);
                lstOfContVer.add(cv2);
                
                insert lstOfContVer;

                List<ContentVersion> testContent = [SELECT id, ContentDocumentId FROM 
                                                    ContentVersion where Id IN :lstOfContVer];

                FeedItem feeditem = new FeedItem();
                feedItem.Type = 'ContentPost';
                feedItem.Body = 'Place holder file. Upload attachmenet using Upload new version.';
                //feedItem.ContentData = beforeblob;
                //feedItem.ContentFileName = 'Test File';
                feedItem.ParentId = proj.id;
                insert feedItem;

                 //insert new ContentDocumentLink
                ContentDocumentLink newFileShare = new ContentDocumentLink();
                newFileShare.contentdocumentid = testcontent.get(0).contentdocumentid;
                newFileShare.LinkedEntityId = proj.id;
                newFileShare.ShareType= 'V';

                ContentDocumentLink newFileShare2 = new ContentDocumentLink();
                newFileShare2.contentdocumentid = testcontent.get(1).contentdocumentid;
                newFileShare2.LinkedEntityId = proj.id;
                newFileShare2.ShareType= 'V';

                ContentDocumentLink newFileShare3 = new ContentDocumentLink();
                newFileShare3.contentdocumentid = testcontent.get(0).contentdocumentid;
                newFileShare3.LinkedEntityId = mockReq.id;
                newFileShare3.ShareType= 'V';

                ContentDocumentLink newFileShare4 = new ContentDocumentLink();
                newFileShare4.contentdocumentid = testcontent.get(1).contentdocumentid;
                newFileShare4.LinkedEntityId = mockReq.id;
                newFileShare4.ShareType= 'V';

                List<ContentDocumentLink> lstOfContDocLink = new List<ContentDocumentLink>();
                lstOfContDocLink.add(newFileShare);
                lstOfContDocLink.add(newFileShare2);
                lstOfContDocLink.add(newFileShare3);
                lstOfContDocLink.add(newFileShare4);
                
                insert lstOfContDocLink;

                /* Cover FilePreviewController getImageContentVersion  */
                FilePreviewController.getRelatedContentVersionIds(proj.id);
                FilePreviewController.getRelatedContentVersionIds([SELECT id FROM Opportunity LIMIT 1].id);
                
        }

        @isTest
        public static void createFeedItemsRecordsForTrigger(){
             Project__c proj = [SELECT id FROM Project__c LIMIT 1];

                FeedItem feeditem = new FeedItem();
                feedItem.Type = 'ContentPost';
                feedItem.Body = 'Place holder file. Upload attachmenet using Upload new version.';
                //feedItem.ContentData = beforeblob;
                //feedItem.ContentFileName = 'Test File';
                feedItem.ParentId = proj.id;
                insert feedItem;

                String before = 'Testing base 64 enco de';            
                Blob beforeblob = Blob.valueOf(before);

                ContentVersion cv = new ContentVersion();
                cv.title = 'test content trigger';      
                cv.PathOnClient = 'test.jpg';           
                cv.VersionData = beforeblob;  
                insert cv;

                FeedAttachment feedAttachment = new FeedAttachment();
                feedAttachment.FeedEntityId = feedItem.Id;
                feedAttachment.RecordId = cv.id; 
                //feedAttachment.Title = 'FileName';
                feedAttachment.Type = 'CONTENT'; 
                insert feedAttachment;

                FileOperationHandler.increaseImageCountFrmFeedItem(new List<FeedItem>{feedItem});

        }

        @isTest
        public static void createContentDocDelTrigger(){
                Project__c proj = [SELECT id FROM Project__c LIMIT 1];
                List<Mock_Up_Request__c> mockReq = [SELECT id,Approval_Status__c FROM Mock_Up_Request__c LIMIT 2];

                String before = 'Testing base 64 enco de';            
                Blob beforeblob = Blob.valueOf(before);

                ContentVersion cv = new ContentVersion();
                cv.title = 'test content trigger';      
                cv.PathOnClient = 'test.jpg';           
                cv.VersionData = beforeblob;  
                insert cv;

                ContentVersion testContent = [SELECT id, ContentDocumentId FROM 
                                                    ContentVersion where Id =:cv.id];

                ContentDocumentLink newFileShare4 = new ContentDocumentLink();
                newFileShare4.contentdocumentid = testcontent.contentdocumentid;
                newFileShare4.LinkedEntityId = proj.id;
                newFileShare4.ShareType= 'V';

                insert newFileShare4;

                ContentDocumentLink newFileShare = new ContentDocumentLink();
                newFileShare.contentdocumentid = testcontent.contentdocumentid;
                newFileShare.LinkedEntityId = mockReq.get(0).id;
                newFileShare.ShareType= 'V';

                insert newFileShare;
                
                ContentDocumentLink newFileShareApproved = new ContentDocumentLink();
                newFileShareApproved.contentdocumentid = testcontent.contentdocumentid;
                newFileShareApproved.LinkedEntityId = mockReq.get(1).id;
                newFileShareApproved.ShareType= 'V';

                insert newFileShareApproved;

                try{
                     delete new ContentDocument(id=testcontent.contentDocumentId);
                }catch(Exception e){

                }
               
                try{
                     delete new ContentDocument(id=testcontent.contentDocumentId);
                }catch(Exception e){

                }

                mockReq.get(1).Approval_Status__c ='Approved';
                update mockReq.get(1);

                try{
                     delete new ContentDocument(id=testcontent.contentDocumentId);
                }catch(Exception e){

                }
        }

        @isTest
        public static void coverLocationUtilityController(){

            Project__c proj = [SELECT id FROM Project__c LIMIT 1];

            LocationUtility.getNearByProjectsFrmDB(28.560100,77.219319,100,proj.id);
            LocationUtility.setCapturedGeoLocation(28.560100,77.219319,proj.id);
            LocationUtility.getCurrentProjectDetail(proj.id);

            try{
                 LocationUtility.setCapturedGeoLocation(28.560100,77.219319,'dsdsdsdsdsdsd');
                }catch(Exception e){}
        }



}
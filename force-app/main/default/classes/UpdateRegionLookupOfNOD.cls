/* Created By         : Rajiv Kumar Singh
   Created Date       : 03/07/2018
   Class Name         : UpdateRegionLookupOfNOD
   Description        : update the particular Region record lookup field to Region wise NOD
   Last Modified By   : Rajiv Kumar Singh
   Last Modified Date : 03/07/2018
*/
public class UpdateRegionLookupOfNOD{
  @InvocableMethod(label='UpdateRegionLookupOfNOD' description='update the particular Region record lookup field to Region wise NOD')
  public static void UpdateLookupValue(List<Id> RegionNODIds)
    {   
       map<string,string> NODwithRegMap = new map<string,string>();  
       set<string> presentRegionSet = new set<String>();
       list<Region_Wise_NOD_s__c> NewList = new list<Region_Wise_NOD_s__c>(); 
       list<Region_Wise_NOD_s__c> FinalListToUpdate = new list<Region_Wise_NOD_s__c>();

       for(Region_Wise_NOD_s__c ptp:[select id,Region_Name__c from Region_Wise_NOD_s__c where id in : RegionNODIds])
       {
         if(ptp.Region_Name__c != null)
          {
            presentRegionSet.add(ptp.Region_Name__c);      
            NewList.add(ptp);
            system.debug('The ptp.id is: ' +ptp.id);
          }     
       }
       system.debug('The NewList is: ' +NewList+'......'+presentRegionSet);
       if(presentRegionSet.size()>0)
       {
          for(Region__c acc:[select id,name from Region__c where name in:presentRegionSet])
           {
               NODwithRegMap.put(acc.name,acc.id);
           }
           system.debug('The NODwithRegMap is: ' +NODwithRegMap);
           for(Region_Wise_NOD_s__c ptp:NewList)
           {
               ptp.Region__c = NODwithRegMap.get(ptp.Region_Name__c);
               system.debug('The  ptp.Sales_Office_Name__c is: ' + ptp.Region_Name__c);
               FinalListToUpdate.add(ptp);
           }
       }
       if(!FinalListToUpdate.isEmpty() && FinalListToUpdate.size() > 0) {
            update FinalListToUpdate;
       }
    }
}
global class ScheduleAccountLedgerBatch implements Schedulable { 
    global void execute(SchedulableContext SC) { 
        AccountLedgersBatch bcc = new AccountLedgersBatch(); 
        database.executebatch(bcc);

  }
 
}
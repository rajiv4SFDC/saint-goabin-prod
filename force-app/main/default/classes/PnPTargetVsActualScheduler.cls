global class PnPTargetVsActualScheduler implements Schedulable{
    global Date startDate ;
    
    global PnPTargetVsActualScheduler(){}
    
    global void execute(SchedulableContext sc){
        PnPTargetvsActualBatch ivT = new PnPTargetvsActualBatch();
        Database.executeBatch(ivT);
    }
}
/* Created By         : Rajiv Kumar Singh
   Created Date       : 12/06/2018
   Class Name         : UpdateProductLookupInvoiceTest
   Description        : 
   Last Modified By   : Rajiv Kumar Singh
   Last Modified Date : 12/06/2018
*/
@isTest
private class UpdateProductLookupInvoiceTest
{ 
 // @istest
   private testmethod static void testMethod1()
   {
      Product2 p1 = new Product2();
      p1.Name = 'testing';
      p1.Description ='East';
      p1.Product_Category__c = 'Infinity';
      p1.Sub_Family__c = 'EKO';
      p1.Coating__c ='PLT';
      p1.Tint__c = 'SAFE';
      p1.ExternalId = '401-518-000-000-000-0600';
      insert p1;
      
      Invoice__c ptp = new Invoice__c();
      ptp.name = 'SF1234';
      ptp.Product_Code__c = '401-518-000-000-000';
      ptp.Thickness__c = '0600';
      insert ptp;
      
      Invoice__c pt = [select id, Product_External_ID__c from Invoice__c where id =:ptp.id];
      
    //  System.assertEquals(pt.product__c,p1.id);
      list<Id> ptrList = new list<Id>();
      ptrList.add(pt.id);
      
      UpdateProductLookupInvoice.UpdateLookupValue(ptrList);
   }
}
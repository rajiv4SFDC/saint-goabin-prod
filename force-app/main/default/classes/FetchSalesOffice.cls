public class FetchSalesOffice {     
    @AuraEnabled
    public static List<Sales_Office_Wise_NOD_s__c> fetchRecord (DateTime ToDate, DateTime EndDate,String selectedsoffice) {
        
        System.debug('ToDate ' + ToDate);
        System.debug('EndDate ++' + EndDate);

        List<Sales_Office_Wise_NOD_s__c> salesOfficeRecords = new List<Sales_Office_Wise_NOD_s__c>();

        salesOfficeRecords = [Select id,Name, Average_Outstanding__c, No_of_Days__c,Region__c, Posting_Date__c, Sales_Office__c, Sales_Office_Name__c
                                From Sales_Office_Wise_NOD_s__c Where CreatedDate >=: ToDate AND CreatedDate <=: EndDate+1 AND Sales_Office__r.name =: selectedsoffice Order By CreatedDate Desc LIMIT 1]; 
        System.debug('salesOfficeRecords ' + salesOfficeRecords);
        return salesOfficeRecords;
    }
    
    //for salesOfficeComponent_DuplicateComponent
    @AuraEnabled
    public static List<Sales_Office_Wise_NOD_s__c> fetchingRecords(String selectedMonth, String selectedYear ,String selectedsoffice){
        Map<String,Sales_Office_Wise_NOD_s__c> salesOfficeMap = new map<String,Sales_Office_Wise_NOD_s__c>();
        Map<String,Decimal> mapOfMonth = new Map<String,Decimal>{'Jan' => 1, 'Feb' => 2, 'Mar' => 3, 'Apr' => 4, 'May' => 5, 
                                                                 'Jun' => 6, 'Jul' => 7, 'Aug' => 8, 'Sep' => 9, 'Oct' => 10, 
            													 'Nov' => 11, 'Dec' => 12};
        List<Sales_Office_Wise_NOD_s__c> salesOfficeList;
        String selectedSalesOfficeToQuery;
        String convertMonthToNumber = String.ValueOf(mapOfMonth.get(selectedMonth));
        
        if(selectedsoffice == 'None')
            selectedSalesOfficeToQuery = '';
        else
            selectedSalesOfficeToQuery = 'AND Sales_Office_Name__c =:selectedsoffice';
        
        System.debug('selectedMonth ->'+selectedMonth+' selectedYear ->'+selectedYear);
        System.debug('selectedsoffice-> '+selectedsoffice + ' selectedSalesOfficeToQuery-> ' +selectedSalesOfficeToQuery);
        
        salesOfficeList = Database.query('Select Id, Name, Average_Outstanding__c, No_of_Days__c,Region__c, Sales_Office__r.name, Posting_Date__c, Sales_Office_Name__c FROM Sales_Office_Wise_NOD_s__c WHERE CALENDAR_MONTH(CreatedDate)='+convertMonthToNumber+' AND CALENDAR_YEAR(CreatedDate)='+selectedYear+' '+selectedSalesOfficeToQuery+' ORDER BY CreatedDate DESC');
        if(salesOfficeList.size() > 0){
            System.debug('-->salesOfficeSize '+salesOfficeList.size());
            for(Sales_Office_Wise_NOD_s__c so : salesOfficeList){
                if(!salesOfficeMap.containsKey(so.Sales_Office_Name__c))
                    salesOfficeMap.put(so.Sales_Office_Name__c,so);
            }    
        } 
        System.debug('size er--> '+salesOfficeMap.Values().size() +'--> '+salesOfficeMap.Values());
        System.debug('selectedsoffice '+selectedsoffice);
        return salesOfficeMap.Values();
    }
    
    @AuraEnabled
    public static List<String> salesOfficeNames(){
        List<Sales_Office__c> salesOfficeList = [SELECT id,Name FROM Sales_Office__c];
        List<String> salesOfficeNewList = new List<String>();
        for(Sales_Office__c so : salesOfficeList){
            if(so.Name != null)
                salesOfficeNewList.add(so.Name);
        }
        salesOfficeNewList.sort();
        salesOfficeNewList.add(0,'None');
        return salesOfficeNewList; 
    }
    
    @AuraEnabled
    //to get months
    public static List<String> selectedMonth(){
        Map<String,Decimal> mapOfMonth = new Map<String,Decimal>{'Jan' => 1, 'Feb' => 2, 'Mar' => 3, 'Apr' => 4, 'May' => 5, 
                                                                 'Jun' => 6, 'Jul' => 7, 'Aug' => 8, 'Sep' => 9, 'Oct' => 10, 
            													 'Nov' => 11, 'Dec' => 12};
        List<String> mapKeyList = new List<String>();
        mapKeyList.addAll(mapOfMonth.KeySet());
        return mapKeyList;    
    }

	@AuraEnabled
    //to get years
    public static List<String> selectedYear(){
        String startYear = Label.StartYear;
		String endYear = Label.End_Year;
        List<String> yearList = new List<String>();
        for(Decimal i = Decimal.ValueOf(startYear) ; i<= Decimal.ValueOf(endYear) ;i++){
            yearList.add(String.ValueOf(i));
        }
        System.debug('yearList '+yearList);
        return yearList;    
    }
	
    
    
}
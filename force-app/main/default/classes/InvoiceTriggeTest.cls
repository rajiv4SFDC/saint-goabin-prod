@isTest
public class InvoiceTriggeTest {

    @testSetup
    public static void environmentSetup() {
        
        Id crmRecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('CRM').getRecordTypeId();
        Id erpRecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('ERP').getRecordTypeId();
        
        Account acc =new Account();
        acc.Name = 'Test AccountCRM';
        acc.Type = 'Architect';
        acc.Sales_Area_Region__c = 'North';
        acc.RecordTypeId = crmRecordTypeId;
        insert acc;
        
        Invoice__c inv =  new Invoice__c(Name='Invoice Succ',
                                        Billing_Date__c = Date.newInstance(2018,7,17),
                                        Non_Appropriable_Quantity__c = 500,
                                        Appropriable_Quantity__c = 0,
                                        Quantity__c = 500,
                                        To_be_processed__c=true,
                                        Customer__c = acc.id);
        insert inv;
    }
    
    @isTest
    public static void invTrigger() {
        
    }
}
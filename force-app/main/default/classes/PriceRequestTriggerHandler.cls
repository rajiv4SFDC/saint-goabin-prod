public class PriceRequestTriggerHandler extends TriggerHandler {
	 
    public PriceRequestTriggerHandler() {}
    
    public override void beforeInsert() {
        //System.debug('PR before Insert Start');
        new PriceRequestManager().fetchBeforeInsertRecordsForUpdation((List<Price_Request__c>)Trigger.new );
        //System.debug('PR before Insert end');
    }    

}
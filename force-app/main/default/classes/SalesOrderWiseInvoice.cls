public with sharing class SalesOrderWiseInvoice {
    Transient public date from_date{get;set;}
    Transient public date To_date{get;set;}
    Transient public date from_sales{get;set;}
    Transient public date to_sales{get;set;}
    Transient public date BillDate{get;set;}
    public List<Sales_Order__c> OrderList {get;set;}
    public List<Invoice__c> OrderInvoices{get;set;}
    public List<Invoice__c> OInvoices{get;set;}
    public List<Sales_Order__c> displayOrder {get;set;}
    //  public List<Sales_Order__c> displayRange {get;set;}
    public invoice__c invoice{get;set;}
    Transient public string salesArea{get;set;}
    Transient public String SelectedRegion{get;set;}
    Transient public String SelectedRange{get;set;}
    Transient public String Office{get;set;}
    Transient  public boolean flag{get;set;}
    Transient public date salesDate{get;set;}
    Transient public boolean checkbox_range{get;set;}
    Transient public boolean checkbox_range1{get;set;}
    public List<PieWedgeData> data = new List<PieWedgeData>();
    Transient  public integer counts{get;set;}
    
    public List<SelectOption> RegionOption{
        get{
            List<SelectOption> options = new List<SelectOption>();
            Schema.DescribeFieldResult fieldResult = account.sales_area_region__c.getDescribe();
            List<Schema.PicklistEntry> ple = fieldResult.getPicklistValues();
            options.add(new SelectOption('-Select-', '-Select-'));
            for( Schema.PicklistEntry f : ple)
            {  
                options.add(new SelectOption(f.getLabel(), f.getValue()));
            }      
            return options;
        } set;
    }
    
    public List<SelectOption> ReportingRange {
        get {
            List<SelectOption> options = new List<SelectOption>();
            Schema.DescribeFieldResult fieldResult = product2.Reporting_range__C.getDescribe();
            List<Schema.PicklistEntry> ple = fieldResult.getPicklistValues();
            options.add(new SelectOption('-Select-', '-Select-'));
            for( Schema.PicklistEntry f : ple)
            {   
                options.add(new SelectOption(f.getLabel(), f.getValue()));
            }      
            return options;
        }  set;
    }
    
    public SalesOrderWiseInvoice () {
        counts=0;
        //OrderList = new List<Sales_Order__c>();
        from_sales=system.today()-1;
        to_sales=system.today();
        //  BillDate=system.today();
        displayOrder = new List<sales_order__c>();
        OInvoices = new list<invoice__c>();
        invoice = new invoice__c();
        flag=false;
        displayOrder = new List<sales_order__c>();
        OrderList = new List<Sales_Order__c>([select id,name,bill_to_account__r.Sales_Office_ERP__c,Quantity_in_Tons__c,bill_to_account__r.name,bill_to_account__r.sales_area__c,Product_name__r.name,Product_name__r.reporting_range__c,bill_to_account__r.sales_area_region__c,sales_order_date__c from sales_order__c where Product_name__r.reporting_range__c!='' limit 500]);
        if(OrderList.size()>0){
            for(Sales_Order__c sales:OrderList){
                if(sales.sales_order_date__c >= from_sales && sales.sales_order_date__c <= to_sales ){
                    displayOrder.add(sales); 
                }
            }
        }
        
        // displayOrder = new List<Sales_Order__c>(orderList);
        
        OrderInvoices = new List<invoice__c>([SELECT Id,name,billing_date__c,Quantity__c,Item_Tonnage_formula__c FROM invoice__c limit 500]);
        if(OrderInvoices.size()>0){
            for(invoice__c inv: OrderInvoices){
                if(inv.billing_date__c >= from_sales && inv.billing_date__c <= to_sales){
                    if(inv.quantity__c != null){
                        counts +=integer.valueOf(inv.quantity__c);
                    }
                    OInvoices.add(inv);
                }
            }     
        }
        
        
    }
    
    public void filter(){
        displayOrder.clear();
        list<sales_order__c> salesList = new List<sales_order__c>([select id,name,bill_to_account__r.Sales_Office_ERP__c,Quantity_in_Tons__c,bill_to_account__r.name,bill_to_account__r.sales_area__c,Product_name__r.name,Product_name__r.reporting_range__c,bill_to_account__r.sales_area_region__c,sales_order_date__c from sales_order__c where bill_to_account__r.sales_area_region__c=:SelectedRegion AND(sales_order_date__c >=:from_sales and sales_order_date__c <=:to_sales) limit 800]);
        if(salesList.size()>0){
            for(sales_order__c sales : salesList){
                if(sales.id != null)
                    displayOrder.add(sales);
                /*   if (SelectedRegion == sales.bill_to_account__r.sales_area_region__c){
flag=false;
displayOrder.add(sales);
} */    
            }
        }
        
    }
    
    public void filterSales(){
        // invoice__c inv = new invoice__c();
        // Sales_Order__c p = new Sales_Order__c();
        if(checkbox_range){
            displayOrder.clear();
            List<sales_order__c> salesList = new List<sales_order__c>([SELECT id,name,bill_to_account__r.Sales_Office_ERP__c,Quantity_in_Tons__c,bill_to_account__r.name,bill_to_account__r.sales_area__c,Product_name__r.name,Product_name__r.reporting_range__c,bill_to_account__r.sales_area_region__c,sales_order_date__c from sales_order__c where sales_order_date__c >=:from_sales and sales_order_date__c <=:to_sales limit 800]);
            system.debug('salesList-->'+salesList);
            if(salesList.size()>0){
                for(Sales_Order__c sales:salesList){
                    //  if((sales.sales_order_date__c >= from_sales && sales.sales_order_date__c <= to_sales)){
                    if(sales.id != null || sales.id != ''){
                        displayOrder.add(sales); 
                        //     }
                        
                    }
                }
            }
            
            //  system.debug('displayOrder-->'+displayOrder);
        }
        
        if(checkbox_range1){
            counts=0;
            OInvoices.clear();
            List<invoice__C> filterList = new list<invoice__c>([select id,name,billing_date__c,Quantity__c,Item_Tonnage_formula__c from invoice__c where billing_date__c >=:from_sales and billing_date__c <=:to_sales limit 800]);
            // string query = ''; filterList=database.query(query);
            if(filterList.size()>0){
                for(invoice__c inc: filterList){
                    if(inc.Billing_Date__c != null){
                        if(inc.Quantity__c != null){
                            counts+=integer.valueOf(inc.Quantity__c);
                            OInvoices.add(inc);
                        }
                    }
                }
            }
            
        }
    }
    
    public void filterRange(){
        displayOrder.clear();
        list<sales_order__c> salesList = new List<sales_order__c>([select id,name,bill_to_account__r.Sales_Office_ERP__c,Quantity_in_Tons__c,bill_to_account__r.name,bill_to_account__r.sales_area__c,Product_name__r.name,Product_name__r.reporting_range__c,bill_to_account__r.sales_area_region__c,sales_order_date__c from sales_order__c where Product_name__r.reporting_range__c=:SelectedRange AND(sales_order_date__c >=:from_sales and sales_order_date__c <=:to_sales) limit 800]);
        if(salesList.size()>0){  
            for(sales_order__c sales : salesList){
                if(sales.id != null)
                    displayOrder.add(sales);
                
            }
        }
        
        /*  if ( SelectedRange == sales.Product_name__r.reporting_range__c){
flag=false;
displayOrder.add(sales);
} */
    }
    
    public void SalesAreawise(){
        displayOrder.clear();
        list<sales_order__c> salesList = new List<sales_order__c>([select id,name,bill_to_account__r.Sales_Office_ERP__c,Quantity_in_Tons__c,bill_to_account__r.name,bill_to_account__r.sales_area__c,Product_name__r.name,Product_name__r.reporting_range__c,bill_to_account__r.sales_area_region__c,sales_order_date__c from sales_order__c where bill_to_account__r.sales_office_erp__c=:salesArea AND(sales_order_date__c >=:from_sales and sales_order_date__c <=:to_sales) limit 800]);
        if(salesList.size()>0){
            for(sales_order__c sales : salesList){
                /*  if ( salesArea == sales.bill_to_account__r.sales_office_erp__c){
//  flag=false;
displayOrder.add(sales);
} */
                if(sales.id != null)
                    displayOrder.add(sales);
            }   
        }
        
    }
    
    public class PieWedgeData { 
        public string name { get; set; } 
        public Decimal data_sales { get; set; } 
        public Decimal data_invoice{get;set;}
        
        public PieWedgeData(String name, Decimal data_sales, decimal data_invoice){
            this.name=name;
            this.data_sales=data_sales;
            this.data_invoice=data_invoice;
        }
    }  
    public List<PieWedgeData> getPieData(){ 
        from_date=system.today()-29;
        To_date=system.today()+1;
        string ReportingRange;
        data.clear();
        integer sales_d=0;integer invoice_d=0;integer count=0;
        integer i=0; integer j=0; integer k = 0;
        integer a=0;integer b=0;integer c=0;integer g=0;
        
        for(Sales_Order__c sales : [select Product_name__r.reporting_range__c,Quantity_in_Tons__c from sales_order__c where sales_order_date__c >:from_date and sales_order_date__c <:to_date limit 10]){
            ReportingRange = sales.Product_Name__r.reporting_range__c;
            for(invoice__c inv : [select name,Product__r.reporting_range__c,Item_Tonnage_formula__c from invoice__c where Product__r.reporting_range__c =: ReportingRange AND(billing_date__c >:from_date and billing_date__c <:to_date) limit 10]){
                
                if(inv.Product__r.reporting_range__c == 'Clear_Thick' && sales.Product_name__r.reporting_range__c == 'Clear_Thick'){
                    
                    if(sales.Quantity_in_Tons__c != null && inv.Item_Tonnage_formula__c != null){
                        if(i == 0){
                            data.add(new PieWedgeData('Clear_Thick',sales.Quantity_in_Tons__c,inv.Item_Tonnage_formula__c));    
                            i++;
                        }
                        
                    }
                }
                
                if(inv.Product__r.reporting_range__c == 'Clear_Thin' && sales.Product_name__r.reporting_range__c == 'Clear_Thin'){
                    if(sales.Quantity_in_Tons__c != null && inv.Item_Tonnage_formula__c != null){
                        if(j == 0){
                            data.add(new PieWedgeData('Clear_Thin',sales.Quantity_in_Tons__c,inv.Item_Tonnage_formula__c));
                            j++;
                        }
                        
                    }
                    
                }
                
                if(inv.Product__r.reporting_range__c == 'Coater_base_products' && sales.Product_name__r.reporting_range__c == 'Coater_base_products'){
                    if(sales.Quantity_in_Tons__c != null && inv.Item_Tonnage_formula__c != null){
                        if(sales_d == 0){
                            data.add(new PieWedgeData('Coater_base_products',sales.Quantity_in_Tons__c,inv.Item_Tonnage_formula__c));
                            sales_d++;
                        }
                        
                    }
                    
                }
                
                if(inv.Product__r.reporting_range__c == 'Mirror' && sales.Product_name__r.reporting_range__c == 'Mirror'){
                    if(sales.Quantity_in_Tons__c != null && inv.Item_Tonnage_formula__c != null){
                        if(invoice_d == 0){
                            data.add(new PieWedgeData('Mirror',sales.Quantity_in_Tons__c,inv.Item_Tonnage_formula__c));
                            invoice_d ++;
                        }
                        
                    }
                    
                }
                
                if(inv.Product__r.reporting_range__c == 'Other_Interior' && sales.Product_name__r.reporting_range__c == 'Other_Interior'){
                    if(sales.Quantity_in_Tons__c != null && inv.Item_Tonnage_formula__c != null){
                        if(k == 0){
                            data.add(new PieWedgeData('Other_Interior',sales.Quantity_in_Tons__c,inv.Item_Tonnage_formula__c));
                            k++;
                        }
                        
                    }
                    
                }
                
                if(inv.Product__r.reporting_range__c == 'Other_Interior_2' && sales.Product_name__r.reporting_range__c == 'Other_Interior_2' ){
                    if(sales.Quantity_in_Tons__c != null && inv.Item_Tonnage_formula__c != null){
                        if(a == 0){
                            data.add(new PieWedgeData('Other_Interior_2',sales.Quantity_in_Tons__c,inv.Item_Tonnage_formula__c));
                            a++;
                        }
                        
                    }
                    
                }
                
                if(inv.Product__r.reporting_range__c == 'Planilaque' && sales.Product_name__r.reporting_range__c == 'Planilaque'){
                    if(sales.Quantity_in_Tons__c != null && inv.Item_Tonnage_formula__c != null){
                        if(b==0){
                            data.add(new PieWedgeData('Planilaque',sales.Quantity_in_Tons__c,inv.Item_Tonnage_formula__c));
                            b++;
                        }
                        
                    }
                    
                }
                
                if(inv.Product__r.reporting_range__c == 'Reflectasol' && sales.Product_name__r.reporting_range__c == 'Reflectasol'){
                    if(sales.Quantity_in_Tons__c != null && inv.Item_Tonnage_formula__c != null){
                        if(c==0){
                            data.add(new PieWedgeData('Reflectasol',sales.Quantity_in_Tons__c,inv.Item_Tonnage_formula__c));
                            c++;
                        }
                        
                    }
                    
                }
                
                if(inv.Product__r.reporting_range__c == 'Tinted' && sales.Product_name__r.reporting_range__c == 'Tinted'){
                    if(sales.Quantity_in_Tons__c != null && inv.Item_Tonnage_formula__c != null){
                        if(g==0){
                            data.add(new PieWedgeData('Tinted',sales.Quantity_in_Tons__c,inv.Item_Tonnage_formula__c));
                            g++;
                        }
                    }
                }
                
                if(inv.Product__r.reporting_range__c == 'Two_mm' && sales.Product_name__r.reporting_range__c == 'Two_mm'){
                    if(sales.Quantity_in_Tons__c != null && inv.Item_Tonnage_formula__c != null){
                        if(count == 0){
                            data.add(new PieWedgeData('Two_mm',sales.Quantity_in_Tons__c, inv.Item_Tonnage_formula__c));
                            count ++;
                        }    
                    }
                }
            }
            
            //    pd.name=sales.Product_name__r.reporting_range__c;
            //  pd.name=sales.Product_Name__r.Reporting_range__c;
            //    pd.data_sales=sales.Quantity_in_Tons__c;
            //    pd.data_invoice=inv.Item_Tonnage_formula__c;
            //    data.add(pd);
            // }
        }
        //// system.debug('@ - Final Data -->'+data);
        return data; 
        
    }  
    
    public void click(){
        getPieData();
    }
    
    
    public void check(){
        for(integer i=0; i<1500; ){
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;			
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;i++;i++;i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++; i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            	i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;i++;i++;i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            
            i++;
        }
    }
}
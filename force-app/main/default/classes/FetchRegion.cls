public class FetchRegion {    
    @AuraEnabled
    public static List<Region_Wise_NOD_s__c> fetchRecord (String Month, String Year, String seletedRegion) 
    {
        
       
        System.debug('seletedRegion ' + seletedRegion);
        List<Region_Wise_NOD_s__c> RegionRecords = new List<Region_Wise_NOD_s__c>();
        
        
         Map<String,Decimal> mapOfMonth = new Map<String,Decimal>{'Jan' => 1, 'Feb' => 2, 'Mar' => 3, 'Apr' => 4, 'May' => 5, 
                                                                 'Jun' => 6, 'Jul' => 7, 'Aug' => 8, 'Sep' => 9, 'Oct' => 10, 
            													 'Nov' => 11, 'Dec' => 12};
        String convertMonthToNumber = String.ValueOf(mapOfMonth.get(Month));

        RegionRecords = [Select id,Name, Average_Outstanding__c, No_of_Days__c, Posting_Date__c , Region__c, Region_Names__c
                                From Region_Wise_NOD_s__c Where Region__r.name =: seletedRegion AND CALENDAR_MONTH(CreatedDate) =
                                 :Integer.ValueOf(convertMonthToNumber) AND CALENDAR_YEAR(CreatedDate) = :Integer.ValueOf(Year) 
                                 ORDER BY CreatedDate DESC LIMIT 1];
        System.debug('RegionRecords ' + RegionRecords);
        return RegionRecords;
    }
   /* 
     //for salesOfficeComponent_DuplicateComponent
    @AuraEnabled
    public static List<Region_Wise_NOD_s__c> fetchingRecords(String selectedMonth, String selectedYear ,String seletedRegion){
        Map<String,Region_Wise_NOD_s__c> salesOfficeMap = new map<String,Region_Wise_NOD_s__c>();
        Map<String,Decimal> mapOfMonth = new Map<String,Decimal>{'Jan' => 1, 'Feb' => 2, 'Mar' => 3, 'Apr' => 4, 'May' => 5, 
                                                                 'Jun' => 6, 'Jul' => 7, 'Aug' => 8, 'Sep' => 9, 'Oct' => 10, 
            													 'Nov' => 11, 'Dec' => 12};
        List<Region_Wise_NOD_s__c> salesOfficeList;
        String selectedSalesOfficeToQuery;
        String convertMonthToNumber = String.ValueOf(mapOfMonth.get(selectedMonth));
        
        if(seletedRegion == 'None')
            selectedSalesOfficeToQuery = '';
        else
            selectedSalesOfficeToQuery = 'AND Region_Names__c =:seletedRegion';
        
        System.debug('selectedMonth ->'+selectedMonth+' selectedYear ->'+selectedYear);
        System.debug('seletedRegion-> '+seletedRegion + ' selectedSalesOfficeToQuery-> ' +selectedSalesOfficeToQuery);
        
        salesOfficeList = Database.query('Select Average_Outstanding__c, No_of_Days__c, Posting_Date__c , Region__c, Region_Names__c FROM Region_Wise_NOD_s__c WHERE CALENDAR_MONTH(CreatedDate)='+convertMonthToNumber+' AND CALENDAR_YEAR(CreatedDate)='+selectedYear+' '+selectedSalesOfficeToQuery+' ORDER BY CreatedDate DESC');
        if(salesOfficeList.size() > 0){
            System.debug('-->salesOfficeSize '+salesOfficeList.size());
            for(Region_Wise_NOD_s__c so : salesOfficeList){
                if(!salesOfficeMap.containsKey(so.Region_Names__c))
                    salesOfficeMap.put(so.Region_Names__c,so);
            }    
        } 
        System.debug('size er--> '+salesOfficeMap.Values().size() +'--> '+salesOfficeMap.Values());
        System.debug('seletedRegion '+seletedRegion);
        return salesOfficeMap.Values();
    } */  
    
      @AuraEnabled
    //to get years
    public static List<String> selectedYear(){
        String startYear = Label.StartYear;
		String endYear = Label.End_Year;
        List<String> yearList = new List<String>();
        for(Decimal i = Decimal.ValueOf(startYear) ; i<= Decimal.ValueOf(endYear) ;i++){
            yearList.add(String.ValueOf(i));
        }
        System.debug('yearList '+yearList);
        return yearList;    
    }

	 @AuraEnabled
    //to get months
    public static List<String> selectedMonth(){
        Map<String,Decimal> mapOfMonth = new Map<String,Decimal>{'Jan' => 1, 'Feb' => 2, 'Mar' => 3, 'Apr' => 4, 'May' => 5, 
                                                                 'Jun' => 6, 'Jul' => 7, 'Aug' => 8, 'Sep' => 9, 'Oct' => 10, 
            													 'Nov' => 11, 'Dec' => 12};
        List<String> mapKeyList = new List<String>();
        mapKeyList.addAll(mapOfMonth.KeySet());
        return mapKeyList;    
    }    
}
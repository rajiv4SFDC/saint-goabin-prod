public class UpdateProductLookup{

  @InvocableMethod(label='UpdateProductLookup' description='update the perticular product record lookup field to product tier pricing')
  public static void UpdateLookupValue(List<Id> productTierPricingIds)
    {
       
       
       map<string,string> productTierPricingWithProductMap = new map<string,string>();  
       set<string> presentProductCodesSet = new set<String>();
       list<Product_Tier_Pricing__c > NewList = new list<Product_Tier_Pricing__c>(); 
       list<Product_Tier_Pricing__c > FinalListToUpdate = new list<Product_Tier_Pricing__c>();

       for(Product_Tier_Pricing__c ptp:[select id,name,Product__c,Product_Code__c,Item_Code__c from Product_Tier_Pricing__c where id in:productTierPricingIds])
       {
         if(ptp.Item_Code__c != null)
          {
            presentProductCodesSet.add(ptp.Item_Code__c);      
            NewList.add(ptp);
            system.debug('The ptp.id is: ' +ptp.id);
          }  
           
       }
       system.debug('The NewList is: ' +NewList+'......'+presentProductCodesSet);
       if(presentProductCodesSet.size()>0)
       {
          for(product2 pd:[select id,ProductCode,External_Id__c  from product2 where External_Id__c in:presentProductCodesSet])
           {
            productTierPricingWithProductMap.put(pd.External_Id__c,pd.id);
           }
       system.debug('The productTierPricingWithProductMap is: ' +productTierPricingWithProductMap);
        for(Product_Tier_Pricing__c ptp:NewList)
        {
        ptp.product__c = productTierPricingWithProductMap.get(ptp.Item_Code__c);
        system.debug('The  ptp.product__c is: ' + ptp.product__c);
        FinalListToUpdate.add(ptp);
        }
       }
       
          update FinalListToUpdate;
       
    }
}
public class GetProductAndLineItems{
    
    @AuraEnabled
    public static List<Opportunity_Product__c> getOppProductRecords(Id oppProductId) {
        return new List<Opportunity_Product__c>([Select id,Name,Balance_Quantity_Frm_Forecast__c from Opportunity_Product__c WHERE Id=:oppProductId]);
    }
    
    @AuraEnabled
    public static List<Opportunity_Product_line_items__c> getProductLineItemRecords(string proId) {
        List<Opportunity_Product_line_items__c>  oppProdList = new list<Opportunity_Product_line_items__c>(); 
        system.debug('proId::'+proId);
        return new List<Opportunity_Product_line_items__c>([Select Name,List_Price__c,Opportunity_Product__r.Name,Glass_Item__c,Product__r.Article_number__c,Quantity__c,Product__r.Parent_product__r.name from Opportunity_Product_line_items__c Where Opportunity_Product__c =: proId ]);    
    }
    
    @AuraEnabled
    public static Map<String, String> createRecord(Price_Request__c pr, List<Opportunity_Product_line_items__c> opli){
        id recordTypeID = Schema.SObjectType.Price_Request__c.getRecordTypeInfosByName().get('Shower Cubicles').getRecordTypeId();
        decimal fittingListPrice = 0;
        decimal totalCostfittings =0;
        decimal totalcost = 0;
        decimal glassCharges = 0;
        decimal price;
               Map<String, String> response = new Map<String, String>();
        try{
            for(Opportunity_Product_line_items__c opl : opli){
                if(opl.Glass_Item__c != True){
                    fittingListPrice += (opl.List_Price__c * opl.Quantity__c);
                    system.debug('1st in '+fittingListPrice);    
                }
            }
                if(pr.Discount__c !=null){   
                    totalCostfittings = (fittingListPrice - (pr.Discount__c/100*fittingListPrice));
                    system.debug('2nd in '+totalCostfittings);  
                } 
            for(Opportunity_Product_line_items__c opl : opli){
                if(opl.Glass_Item__c == True && opl.List_Price__c != null && pr.Glass_Price_Discount__c != null && opl.Quantity__c != null){
                    glassCharges = (pr.Freight__c+pr.Installation_Cost__c+pr.Glass_Price_Discount__c)*opl.Quantity__c;
                     system.debug('3rd in '+glassCharges); 
                }
            }
                if(glassCharges !=null && totalCostfittings!=null){
                    totalcost =glassCharges+totalCostfittings; 
                    system.debug('4th in '+totalcost);
                }
                if(totalcost!=null){
                price = totalcost*pr.Requested_Quantity__c;
                system.debug('5th in '+price);  
                }    
            
            
            if(pr != null){
                system.debug('6th in ');   
                pr.BOM_Required_Price__c = totalcost;
                pr.RecordTypeId = recordTypeID;
                pr.Required_Price__c =price;
                upsert pr;
                
                response.put('STATUS', 'SUCCESS');
                response.put('ID', pr.id);
            }
        
            
        } catch (Exception ex){
            system.debug('7th in ');   
            String error = ex.getMessage();
            String[] errors = error.split('error:');
            //system.debug('EX:'+errors[1]);
            
            response.put('STATUS', 'ERROR');
            response.put('MSG', String.valueOf(error));
            if(errors.size() > 0) {
                
                response.put('MSG', (errors.size() == 2) ? errors[1] : 'Some error occurred, please try again later.');
            }
        }
        
        return response;
    } 
    @AuraEnabled
    public static List<Price_Request__c> returnValues(Id priceRequestId){
        return new List<Price_Request__c>([Select id,Name,Discount__c,Tax__c,Freight__c,Installation_Cost__c,Opportunity_Product__c
                                           ,Requested_Quantity__c,Frieght_Discount__c,Glass_Price_Discount__c,
                                           Installation_Cost_Discount__c FROM Price_Request__c WHERE Id=:priceRequestId]);  
        
        
    }
    
}
/* Created By         : Rajiv Kumar Singh
   Created Date       : 29/06/2018
   Class Name         : UpdatesalesOfficeLookupOfNOD
   Description        : update the particular Sales office record lookup field to sales office wise NOD
   Last Modified By   : Rajiv Kumar Singh
   Last Modified Date : 29/06/2018
*/
public class UpdatesalesOfficeLookupOfNOD{
  @InvocableMethod(label='UpdatesalesOfficeLookupOfNOD' description='update the particular Sales office record lookup field to sales office wise NOD')
  public static void UpdateLookupValue(List<Id> salesofficeNODIds)
    {   
       map<string,string> NODwithSalesOfficeMap = new map<string,string>();  
       set<string> presentSalesOfficeSet = new set<String>();
       list<Sales_Office_Wise_NOD_s__c   > NewList = new list<Sales_Office_Wise_NOD_s__c    >(); 
       list<Sales_Office_Wise_NOD_s__c   > FinalListToUpdate = new list<Sales_Office_Wise_NOD_s__c  >();

       for(Sales_Office_Wise_NOD_s__c ptp:[select id,Sales_Office_Name__c from Sales_Office_Wise_NOD_s__c where id in : salesofficeNODIds])
       {
         if(ptp.Sales_Office_Name__c != null)
          {
            presentSalesOfficeSet.add(ptp.Sales_Office_Name__c);      
            NewList.add(ptp);
            system.debug('The ptp.id is: ' +ptp.id);
          }     
       }
       system.debug('The NewList is: ' +NewList+'......'+presentSalesOfficeSet);
       if(presentSalesOfficeSet.size()>0)
       {
          for(Sales_Office__c acc:[select id,name from Sales_Office__c where name in:presentSalesOfficeSet])
           {
               NODwithSalesOfficeMap.put(acc.name,acc.id);
           }
           system.debug('The NODwithSalesOfficeMap is: ' +NODwithSalesOfficeMap);
           for(Sales_Office_Wise_NOD_s__c ptp:NewList)
           {
               ptp.Sales_Office__c = NODwithSalesOfficeMap.get(ptp.Sales_Office_Name__c);
               system.debug('The  ptp.Sales_Office_Name__c is: ' + ptp.Sales_Office_Name__c);
               FinalListToUpdate.add(ptp);
           }
       }
       if(!FinalListToUpdate.isEmpty() && FinalListToUpdate.size() > 0) {
            update FinalListToUpdate;
       }
    }
}
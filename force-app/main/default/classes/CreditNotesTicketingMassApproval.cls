/******************************************
 * Created By  : Ajeet Singh Shekhawat
 * Created On  : 16 Nov 2018
 * Modified By : Ajeet Singh Shekhawat
 * Modified On : 16 Nov 2018
 * Description : Vf page to mass approval or rejection of approval request.  
				
******************************************/ 

public class CreditNotesTicketingMassApproval {
	ApexPages.StandardSetController setCon;
    list<Credit_Notes_Ticketing__c> prList {set;get;}
    public string validateUser{set;get;}
    set<string> PrIdsSet = new set<string>();
    set<string> PrRegionSet = new set<string>();
    
    set<string> UserRoleNamesSet;
    string roleName='';
    public  map<string,string> PrCustomSettingsMap;
    public  map<string,string> PrCustomSettingsRegionMap;
    
    public CreditNotesTicketingMassApproval(ApexPages.StandardSetController controller) {
        setCon = controller;
        prList = new list<Credit_Notes_Ticketing__c>();
        // prList = setCon.getRecords();
        prList = setCon.getSelected();
        system.debug('The prList is :' +prList);
        validateUser ='';
        
        
        system.debug('The PrCustomSettingsMap is :' +PrCustomSettingsMap);
    }
    
    public void validateLoginUser()
    {

        UserRoleNamesSet = new set<string>();
        user u1 =[select id,name,UserRole.Name,Region__c from user where id=:userInfo.getUserId()];
        roleName =u1.UserRole.Name;
        system.debug('The roleName 22222 is : ' + roleName);
        PrCustomSettingsMap = new map<string,string>();  
        PrCustomSettingsRegionMap = new map<string,string>(); 
        for(CreditNotesTicketingMassApproval__c pr:[select id,name,Role_Name__c,List_view_URL__c from CreditNotesTicketingMassApproval__c])
        {
            PrCustomSettingsMap.put(pr.Role_Name__c,pr.List_view_URL__c);
            // PrCustomSettingsRegionMap.put(pr.Role_Name__c,pr.Region_Name__c);
            UserRoleNamesSet.add(pr.Role_Name__c);
        }
        system.debug('The UserRoleNamesSet 11111 is : ' + UserRoleNamesSet);
        system.debug('The PrCustomSettingsMap 22222 is :' +PrCustomSettingsMap);
        
        if(roleName.contains('AM :')) {
            roleName = 'AM :';
            system.debug('The roleName trim   : ' + roleName);
        }
        
        if(PrCustomSettingsMap.KeySet().contains(roleName)) {
            system.debug('The coming  if : ' + validateUser);
            validateUser = '';
        } else {
            validateUser = 'You do not have access to Approve the records';
        	system.debug('The coming  else : ' + validateUser);
        }
            
        if(validateUser.length()>3)
        {
            system.debug('The com message');
            ApexPages.addmessage(new ApexPages.message(ApexPages.severity.WARNING,'You do not have access to Approve the records'));
        }
    } 
   
    public pageReference approveRecs()
    {
        pageReference pf;
        system.debug('The approveRecs inside ** '+ roleName);
        if(roleName.contains('AM :')) {
            system.debug('The roleName is :'+ roleName);
            pf = approveByBusinessInspire();
            
        }
        if(roleName == 'Regional Manager East Distribution' || roleName == 'Regional Manager North Distribution' 
           || roleName == 'Regional Manager South Distribution' || roleName == 'Regional Manager West Distribution') {
            pf = approveByBusinessManager();
        }
        /*
        if(roleName == 'National Head Distribution') {
            pf = approveByBusinessNationalHead();
        }*/
         return pf;
    }
    
    // call this method when user role is Business Head Inspire
    public pageReference approveByBusinessInspire() {
        system.debug('1st approver');
        boolean successStatus = false;
        pageReference pRef;
        try{
            for(Credit_Notes_Ticketing__c pr:prList)
            {
                PrIdsSet.add(pr.id);        
            }
            system.debug('The PrIdsSet is :'+PrIdsSet+PrIdsSet.size());
            
            Set<Id> pIds = (new Map<Id, ProcessInstance>([SELECT Id,Status,TargetObjectId FROM ProcessInstance where Status='Pending' and TargetObjectId in :PrIdsSet])).keySet();
            system.debug('The pIds is :'+ pIds);
            
            Set<Id> pInstanceWorkitems = (new Map<Id, ProcessInstanceWorkitem>([SELECT Id,ProcessInstanceId FROM ProcessInstanceWorkitem WHERE ProcessInstanceId in :pIds])).keySet();
            system.debug('The pInstanceWorkitems is :'+ pInstanceWorkitems);
            
            List<Approval.ProcessWorkitemRequest> requests = new List<Approval.ProcessWorkitemRequest>(); 
            Approval.ProcessResult[] allReq = New Approval.ProcessResult[]{}; 
                for (Id pInstanceWorkitemsId : pInstanceWorkitems){
                    system.debug(pInstanceWorkitemsId);
                    Approval.ProcessWorkitemRequest req2 = new Approval.ProcessWorkitemRequest();
                    req2.setComments('Approved by AM');
                    req2.setAction('Approve'); //to approve use 'Approve'
                    req2.setNextApproverIds(new Id[] {UserInfo.getUserId()});
                    
                    // Use the ID from the newly created item to specify the item to be worked
                    req2.setWorkitemId(pInstanceWorkitemsId);
                    
                    // Add the request for approval
                    requests.add(req2);         
                }
            system.debug('The requests is :'+ requests);
            Approval.ProcessResult[] result2 =  Approval.process(requests);
            successStatus = true;
        }
        catch(exception ert)
        {
            ApexPages.addmessage(new ApexPages.message(ApexPages.severity.WARNING,'Error while approving records '+String.valueof(ert)));
        }
        if(successStatus == true && PrCustomSettingsMap!=null && PrCustomSettingsMap.containsKey(roleName)) {
            pRef = new pageReference(PrCustomSettingsMap.get(roleName));
            system.debug('The pRef is :'+ pRef);
        }
        return pRef;
    }
	
	// call this method when user role is Business Manager    
    public pageReference approveByBusinessManager() {
        system.debug('2nd approver');
        boolean successStatus = false;
        pageReference pRef;
        try{
            for(Credit_Notes_Ticketing__c pr:prList)
            {
                PrIdsSet.add(pr.id);        
            }
            system.debug('The PrIdsSet is :'+PrIdsSet+PrIdsSet.size());
            
            Set<Id> pIds = (new Map<Id, ProcessInstance>([SELECT Id,Status,TargetObjectId FROM ProcessInstance where Status='Pending' and TargetObjectId in :PrIdsSet])).keySet();
            system.debug('The pIds is :'+ pIds);
            
            Set<Id> pInstanceWorkitems = (new Map<Id, ProcessInstanceWorkitem>([SELECT Id,ProcessInstanceId FROM ProcessInstanceWorkitem WHERE ProcessInstanceId in :pIds])).keySet();
            system.debug('The pInstanceWorkitems is :'+ pInstanceWorkitems);
            
            List<Approval.ProcessWorkitemRequest> requests = new List<Approval.ProcessWorkitemRequest>(); 
            Approval.ProcessResult[] allReq = New Approval.ProcessResult[]{}; 
                for (Id pInstanceWorkitemsId : pInstanceWorkitems){
                    system.debug(pInstanceWorkitemsId);
                    Approval.ProcessWorkitemRequest req2 = new Approval.ProcessWorkitemRequest();
                    req2.setComments('Approved by RM');
                    req2.setAction('Approve'); //to approve use 'Approve'
                    req2.setNextApproverIds(new Id[] {UserInfo.getUserId()});
                    
                    // Use the ID from the newly created item to specify the item to be worked
                    req2.setWorkitemId(pInstanceWorkitemsId);
                    
                    // Add the request for approval
                    requests.add(req2);         
                }
            system.debug('The requests is :'+ requests);
            Approval.ProcessResult[] result2 =  Approval.process(requests);
            successStatus = true;
        }
        catch(exception ert)
        {
            ApexPages.addmessage(new ApexPages.message(ApexPages.severity.WARNING,'Error while approving records '+String.valueof(ert)));
        }
        if(successStatus == true && PrCustomSettingsMap!=null && PrCustomSettingsMap.containsKey(roleName)) {
            pRef = new pageReference(PrCustomSettingsMap.get(roleName));
            system.debug('The pRef is :'+ pRef);
        }
        return pRef;
    }
    
    // call this method when user role is National Head Sales and Marketing
    /*
    public pageReference approveByBusinessNationalHead() {
        system.debug('3rd approver');
        boolean successStatus = false;
        pageReference pRef;
        try{
            for(Credit_Notes_Ticketing__c pr:prList)
            {
                PrIdsSet.add(pr.id);        
            }
            system.debug('The PrIdsSet is :'+PrIdsSet+PrIdsSet.size());
            
            Set<Id> pIds = (new Map<Id, ProcessInstance>([SELECT Id,Status,TargetObjectId FROM ProcessInstance where Status='Pending' and TargetObjectId in :PrIdsSet])).keySet();
            system.debug('The pIds is :'+ pIds);
            
            Set<Id> pInstanceWorkitems = (new Map<Id, ProcessInstanceWorkitem>([SELECT Id,ProcessInstanceId FROM ProcessInstanceWorkitem WHERE ProcessInstanceId in :pIds])).keySet();
            system.debug('The pInstanceWorkitems is :'+ pInstanceWorkitems);
            
            List<Approval.ProcessWorkitemRequest> requests = new List<Approval.ProcessWorkitemRequest>(); 
            Approval.ProcessResult[] allReq = New Approval.ProcessResult[]{}; 
                for (Id pInstanceWorkitemsId : pInstanceWorkitems){
                    system.debug(pInstanceWorkitemsId);
                    Approval.ProcessWorkitemRequest req2 = new Approval.ProcessWorkitemRequest();
                    req2.setComments('Approved by NH');
                    req2.setAction('Approve'); //to approve use 'Approve'
                    req2.setNextApproverIds(new Id[] {UserInfo.getUserId()});
                    
                    // Use the ID from the newly created item to specify the item to be worked
                    req2.setWorkitemId(pInstanceWorkitemsId);
                    
                    // Add the request for approval
                    requests.add(req2);         
                }
            system.debug('The requests is :'+ requests);
            Approval.ProcessResult[] result2 =  Approval.process(requests);
            successStatus = true;
        }
        catch(exception ert)
        {
            ApexPages.addmessage(new ApexPages.message(ApexPages.severity.WARNING,'Error while approving records '+String.valueof(ert)));
        }
        if(successStatus == true && PrCustomSettingsMap!=null && PrCustomSettingsMap.containsKey(roleName)) {
            pRef = new pageReference(PrCustomSettingsMap.get(roleName));
            system.debug('The pRef is :'+ pRef);
        }
        return pRef;
    } */
    public pageReference rejectRecs()
    {
        pageReference pRef;
        for(Credit_Notes_Ticketing__c pr:prList)
        {
            PrIdsSet.add(pr.id);
        }
        system.debug('The PrIdsSet in reject :'+PrIdsSet+PrIdsSet.size());
        
        Set<Id> pIds = (new Map<Id, ProcessInstance>([SELECT Id,Status,TargetObjectId FROM ProcessInstance where Status='Pending' and TargetObjectId in :PrIdsSet])).keySet();
        
        Set<Id> pInstanceWorkitems = (new Map<Id, ProcessInstanceWorkitem>([SELECT Id,ProcessInstanceId FROM ProcessInstanceWorkitem WHERE ProcessInstanceId in :pIds])).keySet();
        List<Approval.ProcessWorkitemRequest> requests = new List<Approval.ProcessWorkitemRequest>(); 
        Approval.ProcessResult[] allReq = New Approval.ProcessResult[]{}; 
            for (Id pInstanceWorkitemsId:pInstanceWorkitems){
                system.debug('Inside reject for loop ' + pInstanceWorkitemsId);
                Approval.ProcessWorkitemRequest req2 = new Approval.ProcessWorkitemRequest();
                req2.setComments('Rejected');
                req2.setAction('Reject'); //to approve use 'Approve'
                req2.setNextApproverIds(new Id[] {UserInfo.getUserId()});
                
                // Use the ID from the newly created item to specify the item to be worked
                req2.setWorkitemId(pInstanceWorkitemsId);
                
                // Add the request for approval
                
                requests.add(req2);                 }    
        Approval.ProcessResult[] result2 =  Approval.process(requests);
        if(PrCustomSettingsMap!=null && PrCustomSettingsMap.containsKey(roleName))
            pRef= new pageReference(PrCustomSettingsMap.get(roleName));
        return pRef;  
    }
    
    public pageReference cancelPage()
    {
        pageReference pRef;
        PrCustomSettingsMap = new map<string,string>();  
        for(CreditNotesTicketingMassApproval__c pr:[select id,name,Role_Name__c,List_view_URL__c from CreditNotesTicketingMassApproval__c])
        {
            PrCustomSettingsMap.put(pr.Role_Name__c,pr.List_view_URL__c);
        }
        system.debug('The PrCustomSettingsMap is :' +PrCustomSettingsMap);
        if(PrCustomSettingsMap.containsKey(roleName))
            pRef = new pageReference(PrCustomSettingsMap.get(roleName));
        else
            pRef = new pageReference(PrCustomSettingsMap.get('Default'));
        return pRef;  
    }
}
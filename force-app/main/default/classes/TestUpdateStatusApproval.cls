@isTest(seealldata = true)
public class TestUpdateStatusApproval{
    static testMethod void testUpdateStatusCreate(){
        test.StartTest();
        
          UserRole r = [select id,name from userrole where name='Regional Head Sales and Marketing East'];

        User u = new User(
     ProfileId = [SELECT Id FROM Profile WHERE Name = 'System Administrator'].Id,
     LastName = 'last',
     Email = 'puser2323000@amamama.com',
     Username = 'pus2323er000@amamama.com' + System.currentTimeMillis(),
     CompanyName = 'TEST',
     Title = 'title',
     Alias = 'alias',
     TimeZoneSidKey = 'America/Los_Angeles',
     EmailEncodingKey = 'UTF-8',
     LanguageLocaleKey = 'en_US',
     LocaleSidKey = 'en_US',
     UserRoleId = r.Id,
     Phone  ='123123123123123'
);
insert u;
     
        user u1=[select id,name from user where id=:userinfo.getuserid()];
           
   System.runAs(u1) {         
        Account acc = new Account();
        acc.Name='SaintTest';
        acc.Email__c='aksingh@gmail.com';
        acc.Mobile__c ='9995654321';
        acc.Sales_Area_Region__c ='West';
        acc.PAN_Number__c = 'AAAPL1234C';
        
        acc.RegionalHead__c=u.Id;
        acc.RegionalManager__c=u.Id;
        acc.CreditControl__c =u.Id;
       
        insert acc;
        
      
        
        
        List<Id>accidlist = new List<Id>();
        accidlist.add(acc.Id);
        
        
        
        UpdateApprovalStatus.UpdateApprovalValue(accidlist);
        
        test.StopTest();
        
        
        
        }
        
        
    }//end of the test method
    
    static testMethod void testUpdateStatusCreate1(){
         test.StartTest();
        
          UserRole r = [select id,name from userrole where name='Regional Head Sales and Marketing East'];

        User u = new User(
     ProfileId = [SELECT Id FROM Profile WHERE Name = 'System Administrator'].Id,
     LastName = 'last',
     Email = 'puser2323000@amamama.com',
     Username = 'pus2323er000@amamama.com' + System.currentTimeMillis(),
     CompanyName = 'TEST',
     Title = 'title',
     Alias = 'alias',
     TimeZoneSidKey = 'America/Los_Angeles',
     EmailEncodingKey = 'UTF-8',
     LanguageLocaleKey = 'en_US',
     LocaleSidKey = 'en_US',
     UserRoleId = r.Id,
     Phone  ='123123123123123'
);
insert u;
     
        user u1=[select id,name from user where id=:userinfo.getuserid()];
           
   System.runAs(u1) {         

        
        Account acc = new Account();
        acc.Name='SaintTest';
        acc.Email__c='aksingh2@gmail.com';
        acc.Mobile__c ='9995654322';
        acc.Sales_Area_Region__c ='East';
        acc.PAN_Number__c = 'AAAPL1234C';
        
        acc.RegionalHead__c=u.Id;
        acc.RegionalManager__c=u.Id;
        acc.CreditControl__c =u.Id;
       
        insert acc;
        
        List<Id>accidlist = new List<Id>();
        accidlist.add(acc.Id);
       
        
        
        UpdateApprovalStatus.UpdateApprovalValue(accidlist);
          }
        test.StopTest();
        
      
        
        
        
        
    }//end of the test method
    
     static testMethod void testUpdateStatusCreate2(){
       test.StartTest();
        
          UserRole r = [select id,name from userrole where name='Regional Head Sales and Marketing East'];

        User u = new User(
     ProfileId = [SELECT Id FROM Profile WHERE Name = 'System Administrator'].Id,
     LastName = 'last',
     Email = 'puser2323000@amamama.com',
     Username = 'pus2323er000@amamama.com' + System.currentTimeMillis(),
     CompanyName = 'TEST',
     Title = 'title',
     Alias = 'alias',
     TimeZoneSidKey = 'America/Los_Angeles',
     EmailEncodingKey = 'UTF-8',
     LanguageLocaleKey = 'en_US',
     LocaleSidKey = 'en_US',
     UserRoleId = r.Id,
     Phone  ='123123123123123'
);
insert u;
     
        user u1=[select id,name from user where id=:userinfo.getuserid()];
           
   System.runAs(u1) {         

        
        
        Account acc = new Account();
        acc.Name='SaintTest';
        acc.Email__c='aksingh2@gmail.com';
        acc.Mobile__c ='9995654322';
        acc.Sales_Area_Region__c ='North';
        acc.PAN_Number__c = 'AAAPL1234C'; 
        
        acc.RegionalHead__c=u.Id;
        acc.RegionalManager__c=u.Id;
        acc.CreditControl__c =u.Id;
       
        insert acc;
        
        List<Id>accidlist = new List<Id>();
        accidlist.add(acc.Id);
       
        
        
        UpdateApprovalStatus.UpdateApprovalValue(accidlist);
        }
        test.StopTest();
        
        
        
        
        
        
    }//end of the test method
     static testMethod void testUpdateStatusCreate3(){
        test.StartTest();
        
        
          UserRole r = [select id,name from userrole where name='Regional Head Sales and Marketing East'];

        User u = new User(
     ProfileId = [SELECT Id FROM Profile WHERE Name = 'System Administrator'].Id,
     LastName = 'last',
     Email = 'puser2323000@amamama.com',
     Username = 'pus2323er000@amamama.com' + System.currentTimeMillis(),
     CompanyName = 'TEST',
     Title = 'title',
     Alias = 'alias',
     TimeZoneSidKey = 'America/Los_Angeles',
     EmailEncodingKey = 'UTF-8',
     LanguageLocaleKey = 'en_US',
     LocaleSidKey = 'en_US',
     UserRoleId = r.Id,
     Phone  ='123123123123123'
);
insert u;
     
        user u1=[select id,name from user where id=:userinfo.getuserid()];
           
   System.runAs(u1) {         

        

        
        Account acc = new Account();
        acc.Name='SaintTest';
        acc.Email__c='aksingh2@gmail.com';
        acc.Mobile__c ='9995654322';
        acc.Sales_Area_Region__c ='South';
        acc.PAN_Number__c = 'AAAPL1234C'; 
        
        acc.RegionalHead__c=u.Id;
        acc.RegionalManager__c=u.Id;
        acc.CreditControl__c =u.Id;
       
        insert acc;
        
        List<Id>accidlist = new List<Id>();
        accidlist.add(acc.Id);
       
        
        
        UpdateApprovalStatus.UpdateApprovalValue(accidlist);
        }
        test.StopTest();
        
        
        
        
        
        
    }//end of the test method
    
}//end of the test class
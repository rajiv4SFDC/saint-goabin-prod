@isTest(seealldata = true)
public class UpdateFieldsofEnquiryTest{
    static testMethod void testUpdateStatusCreate(){
        test.StartTest();
        
        Profile p = [SELECT Id FROM Profile WHERE Name='Standard User']; 
        User u = new User(Alias = 'standt', Email='standarduser@testorg.com', 
            EmailEncodingKey='UTF-8', LastName='Testing', LanguageLocaleKey='en_US', 
            LocaleSidKey='en_US', ProfileId = p.Id, Phone ='9994654345',
            TimeZoneSidKey='America/Los_Angeles', UserName='tst1@testorg.com',Region__c='West');
            insert u;

        
        Enquiry__c acc = new Enquiry__c();
        acc.Region__c ='West';
        acc.RegionalHead__c=u.Id;
        acc.RegionalManager__c=u.Id;
        insert acc;
        
      
        
        
        List<Id>accidlist = new List<Id>();
        accidlist.add(acc.Id);
        
        
        
        UpdateFieldsofEnquiry.UpdateApprovalStatusValue(accidlist);
        
        test.StopTest();
        
        
        
        
        
        
    }//end of the test method
    
 /*   static testMethod void testUpdateStatusCreate1(){
        test.StartTest();
        
       
        
        Profile p = [SELECT Id FROM Profile WHERE Name='Standard User']; 
        User u = new User(Alias = 'standt', Email='standarduser@testorg.com', 
            EmailEncodingKey='UTF-8', LastName='Testing', LanguageLocaleKey='en_US', 
            LocaleSidKey='en_US', ProfileId = p.Id, Phone ='9994654349',
            TimeZoneSidKey='America/Los_Angeles', UserName='tst1@testorg.com',Region__c='East');
            insert u;

     Material_Request__c acc = new Material_Request__c();
        acc.Region__c ='East';
        acc.RegionalHead__c=u.Id;
        acc.RegionalManager__c=u.Id;
        insert acc;
       
        
        List<Id>accidlist = new List<Id>();
        accidlist.add(acc.Id);
       
        
        
        UpdateApprovalStatus.UpdateApprovalValue(accidlist);
        
        test.StopTest();
        
        
        
        
        
        
    }//end of the test method */
    
     static testMethod void testUpdateStatusCreate2(){
        test.StartTest();
        
        Profile p = [SELECT Id FROM Profile WHERE Name='Standard User']; 
        User u = new User(Alias = 'standt', Email='standarduser@testorg.com', 
            EmailEncodingKey='UTF-8', LastName='Testing', LanguageLocaleKey='en_US', 
            LocaleSidKey='en_US', ProfileId = p.Id, Phone ='9994654349',
            TimeZoneSidKey='America/Los_Angeles', UserName='tst1@testorg.com',Region__c='North');
            insert u;

        
    Enquiry__c acc = new Enquiry__c();
        acc.Region__c ='North';
        acc.RegionalHead__c=u.Id;
        acc.RegionalManager__c=u.Id;
        insert acc;
       
        
        List<Id>accidlist = new List<Id>();
        accidlist.add(acc.Id);
       
        
        
        UpdateFieldsofEnquiry.UpdateApprovalStatusValue(accidlist);
        
        test.StopTest();
        
        
        
        
        
        
    }//end of the test method
     static testMethod void testUpdateStatusCreate3(){
        test.StartTest();
        
        Profile p = [SELECT Id FROM Profile WHERE Name='Standard User']; 
        User u = new User(Alias = 'standt', Email='standarduser@testorg.com', 
            EmailEncodingKey='UTF-8', LastName='Testing', LanguageLocaleKey='en_US', 
            LocaleSidKey='en_US', ProfileId = p.Id, Phone ='9994654349',
            TimeZoneSidKey='America/Los_Angeles', UserName='tst1@testorg.com',Region__c='South');
            insert u;

        
        Enquiry__c acc = new Enquiry__c();
        acc.Region__c ='South';
        acc.RegionalHead__c=u.Id;
        acc.RegionalManager__c=u.Id;
        insert acc;
       
        
        List<Id>accidlist = new List<Id>();
        accidlist.add(acc.Id);
       
        
        
        UpdateFieldsofEnquiry.UpdateApprovalStatusValue(accidlist);
        
        test.StopTest();
        
        
        
        
        
        
    }//end of the test method
    
}//end of the test class
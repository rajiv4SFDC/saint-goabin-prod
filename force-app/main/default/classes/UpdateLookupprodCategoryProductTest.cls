/* Created By         : Rajiv Kumar Singh
   Created Date       : 16/12/2018
   Class Name         : UpdateLookupprodCategoryProductTest
   Description        : 
   Last Modified By   : Rajiv Kumar Singh
   Last Modified Date : 16/12/2018
*/
@isTest
private class UpdateLookupprodCategoryProductTest
{ 
 // @istest
   private testmethod static void testMethod1()
   {
     
      Product_Category__c pc = new Product_Category__c();
      pc.name ='Coater';
      pc.External_Id__c ='1q2w1';
      insert pc;
      
      Product2 p1 = new Product2();
      p1.Name = 'testing';
      p1.Description ='East';
      p1.Product_Category__c = 'Infinity';
      p1.Sub_Family__c = 'EKO';
      p1.Coating__c ='PLT';
      p1.Tint__c = 'SAFE';
      p1.Sub_Category__c ='Coater';
      p1.Product_Master_Category__c =pc.id;
      p1.ExternalId = '401-518-000-000-000-0600';
      insert p1;
      
      
      Product2 pt = [select id, Sub_Category__c from Product2 where id =:p1.id];
      
    //  System.assertEquals(pt.product__c,p1.id);
      list<Id> ptrList = new list<Id>();
      ptrList.add(pt.id);
      
      UpdateLookupprodCategoryProduct.UpdateLookupValue(ptrList);
   }
}
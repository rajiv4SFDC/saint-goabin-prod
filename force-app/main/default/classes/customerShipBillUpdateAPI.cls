/* Created By: Rajiv Kumar Singh
Created Date: 07/06/2017
Class Name: customerShipBillUpdateAPI
Description : Update BillTo and shipTo status on Account for ERP.
Last Modified By :Rajiv Kumar Singh
Last Modified Date: 07/06/2017
*/
@RestResource(urlMapping='/Bill2ShipStatus')
global class customerShipBillUpdateAPI {
    @HttpPost
    global static void doHandleInsertRequest(){ 
        
        List<BillShipStatusWrapper> wrapList = new List<BillShipStatusWrapper>();
        List<Account> accList = new List<Account>();
        RestRequest Req = RestContext.request;
        RestResponse res = RestContext.response;
        String body = Req.requestBody.toString().trim();
        
        if(body != null && body != '') {
            
            try {
                
                wrapList = (List<BillShipStatusWrapper>)JSON.deserialize(body, List<BillShipStatusWrapper>.class);
                
                for(BillShipStatusWrapper we :wrapList){
                    Account acc = new Account();
                    if(we.SAPcode!=null && we.SAPcode!=''){
                        acc.Sap_Code__c = we.SAPcode;
                    }
                    if(we.BillToStatus!=null && we.BillToStatus=='True'){
                        acc.BillTo__c = boolean.valueof(we.BillToStatus);
                    }
                    if(we.BillToStatus!=null && we.BillToStatus=='False'){
                        acc.BillTo__c = boolean.valueof(we.BillToStatus);
                    }
                    if(we.ShipToStatus!=null && we.ShipToStatus=='True'){
                        acc.Ship_to_customer__c = boolean.valueof(we.ShipToStatus);
                    }
                    if(we.ShipToStatus!=null && we.ShipToStatus=='False'){
                        acc.Ship_to_customer__c = boolean.valueof(we.ShipToStatus);
                    }   
                    
                    accList.add(acc);  
                }
                Schema.SObjectField ftoken = Account.Fields.Sap_Code__c;
                Database.UpsertResult[] srList = Database.upsert(acclist,ftoken,false);           
                res.statusCode = 200;
                res.responseBody = Blob.valueOf(String.valueof(acclist));
                
            } catch (Exception e) {
                
                CommonUtilitiesHandler.handleExceptions(e);
                res.statusCode = 500;
                res.responseBody = Blob.valueOf(e.getMessage());
            }
        } else {
            res.statusCode = 500;
            res.responseBody = Blob.valueOf('Invalid Request');
        } 
    }
    
    public class BillShipStatusWrapper{
        public String SAPcode{get;set;}
        public string BillToStatus{get;set;}
        public string ShipToStatus{get;set;}
        
    }
}
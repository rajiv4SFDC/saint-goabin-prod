@isTest
public class NoDSummaryController_test {

    @isTest static void OfficeTest(){
        account acc= new account(name='test',type='Dealer',shippingcity='test',billingcity='test',shippingstate='test',billingstate='test');
        insert acc;
        
        list<Sales_Office_Wise_NOD_s__c> nList = TestDataFactoryClass.create_office();
        List<Region_Wise_NOD_s__c> regList = TestDataFactoryClass.create_region();
        
        NoDSummaryController nod = new NoDSummaryController();
        List<selectOption> RegionOptions = nod.RegionOptions;
        List<selectOption> salesOfficeOptions = nod.salesOfficeOptions;
        List<selectOption> Yearoptions = nod.Yearoptions;

        date d= date.today();
        nod.selectedYear=string.valueOf(d.year());
        integer yearNum=integer.valueOf(nod.selectedYear);
        system.assertEquals(2019, yearNum);
        
       	nod.selectedOffice = 'DEL1';
        nod.selectedRegion ='-none-';
		list<Sales_Office_Wise_NOD_s__c> nodList=[Select id,Name,Amt_60_Days__c,Average_Outstanding__c, No_of_Days__c,Region__c,posting_date__c,Sales_Office__r.name
                                                      From Sales_Office_Wise_NOD_s__c Where CALENDAR_YEAR(CreatedDate)=:yearNum AND Sales_Office__r.name=:nod.selectedOffice AND posting_date__c!=null];            
        
        nod.getValues();
        
        nod.selectedOffice = '-none-';
        nod.selectedRegion ='North';
        List<Region_Wise_NOD_s__c> regionList=[Select id,Name, Average_Outstanding__c,Amt_60_Days__c, No_of_Days__c,Posting_Date__c , Region__r.name From Region_Wise_NOD_s__c 
                                                          Where CALENDAR_Year(CreatedDate)=:yearNum AND Region__r.name =:nod.selectedRegion AND Posting_Date__c != null AND No_of_Days__c != null AND Region__r.name !=null];
        
        nod.getValues();
        
        nod.selectedOffice = '-none-';
        nod.selectedRegion ='-none-';
        list<Sales_Office_Wise_NOD_s__c> nodList1=[Select id,Name,Amt_60_Days__c,Average_Outstanding__c, No_of_Days__c,Region__c,posting_date__c,Sales_Office__r.name
                                                      From Sales_Office_Wise_NOD_s__c Where CALENDAR_YEAR(CreatedDate)=:yearNum AND posting_date__c!=null];            
        
        List<Region_Wise_NOD_s__c> regionList2=[Select id,Name, Average_Outstanding__c,Amt_60_Days__c, No_of_Days__c,Posting_Date__c , Region__r.name From Region_Wise_NOD_s__c 
                                                          Where CALENDAR_Year(CreatedDate)=:yearNum AND Posting_Date__c != null AND No_of_Days__c != null AND Region__r.name !=null];

        
        nod.getValues();
        
        NoDSummaryController.modelclass nsd = new NoDSummaryController.modelclass();
        nsd.account='test';
        nsd.amt_60_days='30';
        nsd.jan='20';
        nsd.feb='40';
        nsd.mar='49';
        nsd.apr='33';
        nsd.may='22';
        nsd.jun='11';
        nsd.jul='44';
        nsd.aug='99';
        nsd.sept='00';
        nsd.oct='12';
        nsd.nov='77';
        nsd.dec='32';
        
        
        NoDSummaryController.regionmodelclass rsd = new NoDSummaryController.regionmodelclass();
        rsd.account='test';
        rsd.amt_60_days='30';
        rsd.jan='20';
        rsd.feb='40';
        rsd.mar='49';
        rsd.apr='33';
        rsd.may='22';
        rsd.jun='11';
        rsd.jul='44';
        rsd.aug='99';
        rsd.sept='00';
        rsd.oct='12';
        rsd.nov='77';
        rsd.dec='32';
    }
        
}
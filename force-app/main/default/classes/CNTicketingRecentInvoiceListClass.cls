public class CNTicketingRecentInvoiceListClass {
    List<Invoice__c> categories {get;set;}
    public Credit_Notes_Ticketing__c  PRpandP{set;get;}
    public Credit_Notes_Ticketing__c pnpRec{set;get;}
    public boolean recordsFound{set;get;}
    
    public CNTicketingRecentInvoiceListClass(ApexPages.StandardController controller) {
        System.debug('Coming in Constructor ');
        PRpandP = (Credit_Notes_Ticketing__c )controller.getRecord();
        System.debug('Selected PRpandP ' + PRpandP);
        pnpRec= [Select id, Account__c, Account__r.Name, Product_Name__c, Invoice__r.Product__c, Invoice__r.Customer__c From Credit_Notes_Ticketing__c  Where id =:PRpandP.id];
        System.debug('Selected Account ' + pnpRec.Invoice__r.Customer__c + 'Selected Product ' + pnpRec.Invoice__r.Product__c);
    }
    
    // instantiate the StandardSetController from a query locator
    public ApexPages.StandardSetController con {
        get {
            if(con == null) {
             //   system.debug('The ..........'+pnpRec.Customer__c +'.......'+pnpRec.Product_lookup__c);
                con = new ApexPages.StandardSetController(Database.getQueryLocator([Select id, name, Invoice_Rate__c, 
                                                                                    Customer__r.Name, Product__r.Name, Product__c, 
                                                                                    product__r.Product_Category__c, Billing_Date__c 
                                                                                    From invoice__c Where customer__c =: pnpRec.Invoice__r.Customer__c 
                                                                                    AND product__c =: pnpRec.Invoice__r.Product__c 
                                                                                    AND Billing_Date__c >= LAST_N_MONTHS:2 AND 
                                                                                    Invoice_Rate__c!= null order by Billing_Date__c desc]));
                System.debug('Records to display ' + con);
                // sets the number of records in each page set
             //   con.setPageSize(5);
                
            }
            return con;
        }
        set;
    }
    
    // returns a list of wrapper objects for the sObjects in the current page set
    public List<Invoice__c> getCategories() {
        categories = new List<Invoice__c>();
        for (Invoice__c category : (List<Invoice__c>)con.getRecords())
        {
            system.debug('The vat is :'+category);
            categories.add(category);
            recordsFound = true;
        }
        return categories;
    }
    
    /*
    
    // indicates whether there are more records after the current page set.
    public Boolean hasNext {
        get {
            return con.getHasNext();
        }
        set;
    }
    
    // indicates whether there are more records before the current page set.
    public Boolean hasPrevious {
        get {
            return con.getHasPrevious();
        }
        set;
    }
    
    // returns the page number of the current page set
    public Integer pageNumber {
        get {
            return con.getPageNumber();
        }
        set;
    }
    
    // returns the first page of records
    public void first() {
        con.first();
    }
    
    // returns the last page of records
    public void last() {
        con.last();
    }
    
    // returns the previous page of records
    public void previous() {
        con.previous();
    }
    
    // returns the next page of records
    public void next() {
        con.next();
    }
    
    // returns the PageReference of the original page, if known, or the home page.
    public void cancel() {
        con.cancel();
        
        Account acc = new Account();
        acc.Name ='test3';
        acc.Type ='Industry';
        acc.Sales_Area_Region__c ='South';
        acc.PAN_Number__c = 'AAAPL1234C';
        acc.Sales_Office_ERP__c = 'SPR3';
        acc.sap_code__c='123123123';
        acc.RecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('ERP').getRecordTypeId();
    }*/
}
@isTest
public class TestPlantVisitTrigger {
    
		@testSetup 
		public static void createTestData(){
		TestDataFactory.createCustomSetting();
		TestDataFactory.createTestProductData();
		TestDataFactory.createTestStateAndCityRecordData();
		TestDataFactory.createTestCatalogueBudgetData();
		TestDataFactory.createTestCampaignRecordData();
		TestDataFactory.createTestAccountData();
		TestDataFactory.createTestProjectData();
		TestDataFactory.createTestOpportunityData();
		TestDataFactory.createTestOpportunityProductData();
		TestDataFactory.createTestPriceRequestData();
		TestDataFactory.createTestProductForecateData();
		TestDataFactory.createTestForecateInvoiceData();
		TestDataFactory.createTestMockupRecordData();
        TestDataFactory.createTestPlantVisitRecordData();
		TestDataFactory.createTestInvoiceRecordData();
		TestDataFactory.createTestTargetActualRecordData();
		}
    
    @isTest
		public static void createPlantVisit(){
        List<Opportunity> opty = [SELECT id FROM Opportunity LIMIT 1];
        List<User> userList = [select id from User where UserRole.Name LIKE '%DEL1%' OR UserRole.Name LIKE '%MUM1%' LIMIT 2];
  		//System.debug('PV userlist :'+userList);
        List<Plant_Visit__c> PVList = new List<Plant_Visit__c>();
        Plant_Visit__c firstpv = new Plant_Visit__c();
        firstpv.Opportunity__c = opty[0].id;
        firstpv.Approval_Status__c = 'Draft';
        firstpv.Visit_Date__c = Date.newInstance(2017,9, 1);
        firstpv.Plant__c = 'Bhiwadi' ;
        firstpv.OwnerId = userList[0].Id;
        PVList.add(firstpv);
        
        Plant_Visit__c secondpv = new Plant_Visit__c();
        secondpv.Opportunity__c = opty[0].id;
        secondpv.Approval_Status__c = 'Pending';
        secondpv.Visit_Date__c = Date.newInstance(2017,9, 1);
        secondpv.Plant__c = 'Bhiwadi' ;
        secondpv.OwnerId = userList[1].Id;
        PVList.add(secondpv);
        
        insert PVList;
            
        PVList[0].OwnerId = userList[1].Id;
        PVList[1].OwnerId = userList[1].Id;
        PVList[1].Approval_Status__c = 'Rejected';
        update PVList;
        }
}
/* Created By: Rajiv Kumar Singh
Created Date: 24/09/2018
Class Name: SalesorderLineitemInsertAPI
Description : Sales Order line item Insert
Last Modified By :Rajiv Kumar Singh
Last Modified Date: 24/09/2018
*/
@RestResource(urlMapping='/SalesorderLineItemAPI')
global class SalesorderLineitemInsertAPI {
    @HttpPost
    global static void doHandleInsertRequest(){
        
        List<SalesOrderWrapper> wrapList = new List<SalesOrderWrapper>();
        List<Sales_Order_Line_Item__c> Salesorderlist = new List<Sales_Order_Line_Item__c>();
        RestRequest Req = RestContext.request;
        RestResponse res = RestContext.response;
        String body = Req.requestBody.toString().trim();
        if(body != null && body != '') {
            
            try {
                wrapList = (List<SalesOrderWrapper>)JSON.deserialize(body, List<SalesOrderWrapper>.class);
                Sales_Order_Line_Item__c salesOr ;
                Map<String, Sales_Order_Line_Item__c> updateRecords = new Map<String, Sales_Order_Line_Item__c>();
                for(SalesOrderWrapper we :wrapList){
                    salesOr = new Sales_Order_Line_Item__c();
                   
                    if(we.SalesorderNum!=null && we.SalesorderNum!=''){
                        salesOr.External_Id__c = we.SalesorderNum;
                    }
                    if(we.ReferenceNumber!=null && we.ReferenceNumber!=''){
                        salesOr.Reference_Number__c = we.ReferenceNumber;
                    }
                    if(we.BillToSAP!=null && we.BillToSAP!=''){
                        salesOr.Bill_To_SAP_Code__c = we.BillToSAP;
                    }
                    if(we.ShipToSAP!=null && we.ShipToSAP!=''){
                        salesOr.Ship_To_SAP_Code__c = we.ShipToSAP;
                    }
                    if(we.SubFamily!=null && we.SubFamily!=''){
                        salesOr.Sub_Family__c = we.SubFamily;
                    }
                    if(we.Coating!=null && we.Coating!=''){
                        salesOr.Coating__c = we.Coating;
                    }
                    if(we.Tint!=null && we.Tint!=''){
                        salesOr.Tint__c = we.Tint;
                    }
                    if(we.laminate!=null && we.laminate!=''){
                        salesOr.Laminate__c = we.laminate;
                    }
                    if(we.pattern!=null && we.pattern!=''){
                        salesOr.Pattern__c = we.pattern;
                    }
                    if(we.ThicknessCode!=null && we.ThicknessCode!=''){
                        salesOr.Thickness_Code__c = we.ThicknessCode;
                    }
                    if(we.SubFamily!=null && we.SubFamily!='' && we.Coating!=null && we.Coating!='' && we.Tint!=null && we.Tint!='' && we.laminate!=null && we.laminate!='' && we.pattern!=null && we.pattern!='' && we.ThicknessCode!=null && we.ThicknessCode!=''){
                        salesOr.Product_Item_Code__c = we.SubFamily+'-'+we.Coating+'-'+we.Tint+'-'+we.laminate+'-'+we.pattern+'-'+we.ThicknessCode;
                    }
                    if(we.Length!=null && we.Length!=''){
                        salesOr.Length__c = we.Length;
                    }
                    if(we.Width!=null && we.Width!=''){
                        salesOr.Width__c = we.Width;
                    }
                    if(we.SaleOrderLine!=null && we.SaleOrderLine!=''){
                        salesOr.Sales_Order_Line_Item_Number__c = we.SaleOrderLine;
                    }
                    if(we.QuanTon!=null){
                        salesOr.Quantity_in_Tons__c = we.QuanTon;
                    }
                    if(we.SaleDate!=null && we.SaleDate!=null){
                    
                        String[] dateVal = we.SaleDate.split('/');
                        Date dt = Date.newInstance(Integer.valueOf(dateVal[0]), Integer.valueOf(dateVal[1]), Integer.valueOf(dateVal[2]));
                        //String strConvertedDate = dt.format('yyyy-MM-dd HH:mm:ss', 'Asia/Kolkata'); //India timezone
                        salesOr.Sales_Order_Date__c = dt;
                        system.debug('dt:::'+dt);
                       
                    }
                     
                    updateRecords.put(salesOr.External_Id__c, salesOr);
                    //Salesorderlist.add(salesOr);  
                }
                
                Salesorderlist.addAll(updateRecords.values());
                
                if(Salesorderlist.size() > 0){
                    INSERT Salesorderlist;
                }  
                res.statusCode = 200;
                res.responseBody = Blob.valueOf(String.valueof(Salesorderlist));
                
            } catch (Exception e) {
                
                system.debug('e.getMessage()::'+e.getMessage());
                CommonUtilitiesHandler.handleExceptions(e);
                res.statusCode = 500;
                res.responseBody = Blob.valueOf(e.getMessage());
            }
        } else {
            res.statusCode = 500;
            res.responseBody = Blob.valueOf('Invalid Request');
        } 
    }
    
    public class SalesOrderWrapper{
        public String SalesorderNum{get;set;}
        public string ReferenceNumber{get;set;}
        public string BillToSAP{get;set;}
        public string ShipToSAP{get;set;}
        public string SubFamily{get;set;}
        public string Coating{get;set;}
        public string Tint{get;set;}
        public string laminate{get;set;}
        public string pattern{get;set;}
        public string ThicknessCode{get;set;}
        public string Length{get;set;}
        public string Width{get;set;}
        public Decimal QuanTon{get;set;}
        public String SaleDate{get;set;}
        public String SaleOrderLine{get;set;}
        
        
        
    }
}
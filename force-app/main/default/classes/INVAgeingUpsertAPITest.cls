/* Created By: Rajiv Kumar Singh
Created Date: 11/06/2017
Class Name: INVAgeingUpsertAPITest
Description : Test class for INVAgeingUpsertAPI
Last Modified By :Rajiv Kumar Singh
Last Modified Date: 11/06/2017
*/

@IsTest
private class INVAgeingUpsertAPITest{
    
    static testMethod void testPostMethod(){
        String JSONMsg = '[{"lessthanthirty":32.22,"Thirtyone2sixty":1,"Sixtyone2ninty":2.9,"Nintyone2Onetwenty":3.12,"GreaterThanOnetwenty":4.21,"NoOfDays":5.23,"Doctype":"testing","Invno":"213131","PosNo":"212213"}]';
        RestRequest request = new RestRequest();
        request.requestUri ='/services/apexrest/ageingUpsertAPI';
        request.httpMethod = 'POST';
        request.requestBody = Blob.valueof(JSONMsg);
        RestContext.request = request;
        RestContext.response = new RestResponse();
        INVAgeingUpsertAPI.doHandleInsertRequest();
        
        JSONMsg = '[1';
        request = new RestRequest();
        request.requestUri ='/services/apexrest/ageingUpsertAPI';
        request.httpMethod = 'POST';
        request.requestBody = Blob.valueof(JSONMsg);
        RestContext.request = request;
        RestContext.response = new RestResponse();
        INVAgeingUpsertAPI.doHandleInsertRequest();
        
        request = new RestRequest();
        request.requestUri ='/services/apexrest/ageingUpsertAPI';
        request.httpMethod = 'POST';
        request.requestBody = Blob.valueof('');
        RestContext.request = request;
        RestContext.response = new RestResponse();
        INVAgeingUpsertAPI.doHandleInsertRequest();
    }
}
public class SchemeHandler{
    public static void restrictInsertion(List<Schemes__c> schemeList){
        
        Id schemeRecordTypeId = Schema.SObjectType.Distribution_Scheme__c.getRecordTypeInfosByName().get('External').getRecordTypeId();
        Set<String> typeOfSchemeSet = new Set<String>{'Annual Tie Ups','Long Term','Short Term'};
        Set<id> schemeDistIds = new Set<id>();
        
        for(Schemes__c sch : schemeList){
            schemeDistIds.add(sch.Distribution_Scheme__c);
            System.debug(schemeDistIds);
        }
        
        List<Distribution_Scheme__c> dsList = [SELECT id, Type_of_Scheme__c FROM Distribution_Scheme__c WHERE id IN:schemeDistIds AND Type_of_Scheme__c IN:typeOfSchemeSet];
        
        
        Set<Id> accountIds = new Set<Id>();
        if(dsList.size() > 0){
            for(Distribution_Scheme__c ds : [SELECT id, Type_of_Scheme__c,(SELECT id, Account__c FROM Schemes__r) FROM Distribution_Scheme__c WHERE Type_of_Scheme__c IN: typeOfSchemeSet]){
                for(Schemes__c sch : ds.Schemes__r){
                    accountIds.add(sch.Account__c);
                }
            }
            
            for(Schemes__c sch : schemeList){
                if(accountIds.contains(sch.Account__c))
                    sch.addError('Account already exist');
            }
        }
    }
}
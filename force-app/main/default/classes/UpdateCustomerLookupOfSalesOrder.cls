/* Created By         : Rajiv Kumar Singh
   Created Date       : 11/07/2018
   Class Name         : UpdateCustomerLookupOfInvoice
   Description        : update the particular Bill to Account record lookup field to sales order.
   Last Modified By   : Rajiv Kumar Singh
   Last Modified Date : 11/07/2018
*/
public class UpdateCustomerLookupOfSalesOrder{
  @InvocableMethod(label='UpdateCustomerLookupOfSalesOrder' description='update the particular Bill to Account record lookup field to sales order')
  public static void UpdateLookupValue(List<Id> salesOrderIds)
    {   
       map<string,string> SalesWithAccountMap = new map<string,string>();  
       set<string> presentSAPCodesSet = new set<String>();
       list<Sales_Order_Line_Item__c> NewList = new list<Sales_Order_Line_Item__c>(); 
       list<Sales_Order_Line_Item__c> FinalListToUpdate = new list<Sales_Order_Line_Item__c>();

       for(Sales_Order_Line_Item__c ptp:[select id,Bill_To_SAP_Code__c from Sales_Order_Line_Item__c where id in : salesOrderIds])
       {
         if(ptp.Bill_To_SAP_Code__c != null)
          {
            presentSAPCodesSet.add(ptp.Bill_To_SAP_Code__c);      
            NewList.add(ptp);
            system.debug('The ptp.id is: ' +ptp.id);
          }     
       }
       system.debug('The NewList is: ' +NewList+'......'+presentSAPCodesSet);
       if(presentSAPCodesSet.size()>0)
       {
          for(Account acc:[select id, SAP_Code__c from Account where SAP_Code__c in:presentSAPCodesSet])
           {
               SalesWithAccountMap.put(acc.SAP_Code__c,acc.id);
           }
           system.debug('The SalesWithAccountMap is: ' +SalesWithAccountMap);
           for(Sales_Order_Line_Item__c ptp:NewList)
           {
               ptp.Bill_To_Account__c = SalesWithAccountMap.get(ptp.Bill_To_SAP_Code__c);
               system.debug('The  ptp.Customer_code__c is: ' + ptp.Bill_To_SAP_Code__c);
               FinalListToUpdate.add(ptp);
           }
       }
       if(!FinalListToUpdate.isEmpty() && FinalListToUpdate.size() > 0) {
            update FinalListToUpdate;
       }
    }
}
/* Created By: Rajiv Kumar Singh
Created Date: 13/06/2017
Class Name: ProductFittingUpsertAPITest
Description : Test class for ProductFittingUpsertAPI
Last Modified By :Rajiv Kumar Singh
Last Modified Date: 13/06/2017
*/

@IsTest
private class ProductFittingUpsertAPITest{
    
    static testMethod void testPostMethod(){
        String JSONMsg = '[{"ProductCode":"15000951","Description":"DOUBLE SIDED TAPE (BLISTER PACK)","ListPrice":"28080.00","SPLPRICEFLAG":"X","TierCategory":"NOT UNDER TIER","ProductCategory":"SHOWER CUBICLES","Uom":"ROL","Status":"True"}]';
        RestRequest request = new RestRequest();
        request.requestUri ='/services/apexrest/FittingUpsertAPI';
        request.httpMethod = 'POST';
        request.requestBody = Blob.valueof(JSONMsg);
        RestContext.request = request;
        RestContext.response = new RestResponse();
        ProductFittingUpsertAPI.doHandleInsertRequest();
        
        JSONMsg = '[1';
        request = new RestRequest();
        request.requestUri ='/services/apexrest/FittingUpsertAPI';
        request.httpMethod = 'POST';
        request.requestBody = Blob.valueof(JSONMsg);
        RestContext.request = request;
        RestContext.response = new RestResponse();
        ProductFittingUpsertAPI.doHandleInsertRequest();
        
        request = new RestRequest();
        request.requestUri ='/services/apexrest/FittingUpsertAPI';
        request.httpMethod = 'POST';
        request.requestBody = Blob.valueof('');
        RestContext.request = request;
        RestContext.response = new RestResponse();
        ProductFittingUpsertAPI.doHandleInsertRequest();
    }
}
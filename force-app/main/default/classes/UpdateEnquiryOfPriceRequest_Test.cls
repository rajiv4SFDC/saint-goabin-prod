/* Created By         : Rajiv Kumar Singh
   Created Date       : 14/06/2018
   Class Name         : UpdateEnquiryOfPriceRequest_Test
   Description        : 
   Last Modified By   : Rajiv Kumar Singh
   Last Modified Date : 14/06/2018
*/
@isTest
private class UpdateEnquiryOfPriceRequest_Test
{ 
 // @istest
   private testmethod static void testMethod1()
   {
       Account acc = new Account();
       acc.Name = 'raj';
       insert acc; 
       
      Enquiry__c enq = new Enquiry__c();
      enq.Account_Name__c = 'ajeet';
      insert enq; 
      
      
      
       
      Enquiry_Line_Item__c p1 = new Enquiry_Line_Item__c();
      p1.Name = 'testing';
      p1.Reference_Number__c = '6565666';
      p1.Enquiry__c = enq.Id; 
      insert p1;
       
       State__c st = new State__c();
       st.External_ID__c = '11';
       st.Name = 'kam';
       insert st;
       
       City__c ct = new City__c();
       ct.Name = 'jaipur';
       ct.State__c = st.Id;
       insert ct;
       
       Project__c pro = new Project__c();
       pro.Name = 'saint';
       pro.City__c = ct.id;
       pro.Segment__c = 'Airports';
       pro.Region__c = 'East';
       pro.Sales_Area__c = 'AP1-INF';
       pro.Pin_Code__c = '560068';
       pro.Address__c = 'Banglore';
       pro.State__c = st.Id;
       pro.Validated__c = true;
       pro.Referrer_Account__c = acc.Id;
       insert pro;
       
      Opportunity opp = new Opportunity();
      opp.Name = 'singhaniya';
      opp.Type = 'infinity';
      opp.Project__c = pro.Id; 
      opp.AccountId = acc.Id;
       opp.CloseDate = system.today();
       opp.StageName = 'Prospecting';
       opp.Probability = 10;     
      insert opp; 
      
        acc = new Account();
        acc.Name ='test31';
        acc.Type ='Industry1';
        acc.Sales_Area_Region__c ='South';
        acc.RecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('CRM').getRecordTypeId();
        insert acc;
        
        Key_Account__c keyAcct = new Key_Account__c();
        keyAcct.Opportunity__c = opp.Id;
        keyAcct.Account__c = acc.Id;
        insert keyAcct;
      
      
        Product2 masterProduct = new Product2();
        masterProduct.Name = opp.id;
        masterProduct.Article_number__c ='test1234';
        masterProduct.Product_Category__c ='Infinity';
        masterProduct.master_product__c = true;
        insert masterProduct;
         
      
      
        Opportunity_Product__c oppproduct = new Opportunity_Product__c();
        oppproduct.Opportunity__c = opp.Id;
        oppproduct.Product__c = masterProduct.Id;
        oppproduct.status__c='Selected';
        insert oppproduct;
        
        Product_Forecast__c forecast = new Product_Forecast__c();
        forecast.Quantity__c = 10000;
        forecast.Opportunity_Product__c = oppproduct.Id;
        insert forecast;
        
      /*  Opportunity_Product_line_items__c oppline = new Opportunity_Product_line_items__c();
        oppline.Opportunity_Product__c=oppproduct.id;
        oppline.Product__c = masterProduct.Id;
        oppline.Quantity__c = 2;
        insert oppline;
        */
       
       
      Price_Request__c pReq = new Price_Request__c();
      pReq.Price_Type__c = 'Tier Price';
      pReq.Discount__c = 10;
      pReq.Requested_Quantity__c = 1; 
 //     pReq.Opportunity_Product__c = 'OP-138967';
  //    pReq.Valid_From__c = date.ValueOf('14/06/2018');
  //    pReq.Valid_To__c = date.ValueOf('14/06/2018'); 
      pReq.Enquiry_Line_Item__c = p1.Id;
      pReq.Opportunity__c = opp.Id; 
      pReq.Opportunity_Product__c =oppproduct.id;
      insert pReq; 
      
      Price_Request__c  pt = [select id from Price_Request__c  where id =:pReq.id];
      
    //  System.assertEquals(pt.product__c,p1.id);
      list<Id> ptrList = new list<Id>();
      ptrList.add(pt.id);
      
      UpdateEnquiryOfPriceRequest.UpdateLookupValue(ptrList);
   }
}
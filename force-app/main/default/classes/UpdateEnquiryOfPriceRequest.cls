/* Created By         : Rajiv Kumar Singh
   Created Date       : 14/06/2018
   Class Name         : UpdateEnquiryOfPriceRequest
   Description        : update the particular Enquiry record lookup field to Price Request
   Last Modified By   : Rajiv Kumar Singh
   Last Modified Date : 14/06/2018
*/
public class UpdateEnquiryOfPriceRequest{
  @InvocableMethod(label='UpdateEnquiryOfPriceRequest' description='update the particular Enquiry record lookup field to Price Request')
  public static void UpdateLookupValue(List<Id> priceRequestId)
    {   
       map<id, id> priceRequestWithEnquiryMap = new map<id, id>();  
       set<id> presentSAPCodesSet = new set<id>();
       set<id> enquiryLineIdSet = new set<id>();
       list<Price_Request__c > NewList = new list<Price_Request__c>(); 
       list<Price_Request__c > FinalListToUpdate = new list<Price_Request__c>();
       for(Price_Request__c ptp:[select id,Enquiry_Line_Item__c,Enquiry__c from Price_Request__c where id in:priceRequestId])
       {
           if(ptp.Enquiry_Line_Item__c != null)
          	{
                presentSAPCodesSet.add(ptp.Enquiry_Line_Item__c);      
                NewList.add(ptp);
          }           
       }
        for(Enquiry_Line_Item__c enLine : [select id, Enquiry__c from Enquiry_Line_Item__c where id in:presentSAPCodesSet]) {
            enquiryLineIdSet.add(enLine.Enquiry__c);
            priceRequestWithEnquiryMap.put(enLine.id, enLine.Enquiry__c);
        }
       system.debug('The NewList is: ' +NewList+'......'+presentSAPCodesSet);
       if(presentSAPCodesSet.size()>0)
       {
           for(Price_Request__c ptp:NewList)
           {
               ptp.Enquiry__c = priceRequestWithEnquiryMap.get(ptp.Enquiry_Line_Item__c);
               FinalListToUpdate.add(ptp);
           }
       }
       if(!FinalListToUpdate.isEmpty() && FinalListToUpdate.size() > 0) {
            update FinalListToUpdate;
       }
    }
}
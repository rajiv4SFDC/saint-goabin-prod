/* Created By         : Rajiv Kumar Singh
   Created Date       : 11/06/2018
   Class Name         : UpdateLookupValueOfCustomer_Ledger
   Description        : update the particular Account record lookup field to Customer_Ledger
   Last Modified By   : Rajiv Kumar Singh
   Last Modified Date : 11/06/2018
*/
public class UpdateLookupValueOfCustomer_Ledger{
  @InvocableMethod(label='UpdateLookupValueOfCustomer_Ledger' description='update the particular Account record lookup field to Customer_Ledger')
  public static void UpdateLookupValue(List<Id> customer_LedgerIds)
    {   
       map<string,string> customer_LedgerWithAccountMap = new map<string,string>();  
       set<string> presentSAPCodesSet = new set<String>();
       list<Customer_Ledger__c > NewList = new list<Customer_Ledger__c>(); 
       list<Customer_Ledger__c > FinalListToUpdate = new list<Customer_Ledger__c>();

       for(Customer_Ledger__c ptp:[select id,Account__c,SAP_Code__c,Type_of_Transaction__c,Credit__c,Debit__c,Payment__c from Customer_Ledger__c where id in:customer_LedgerIds])
       {
         if(ptp.SAP_Code__c != null)
          {
            presentSAPCodesSet.add(ptp.SAP_Code__c);      
            NewList.add(ptp);
            system.debug('The ptp.id is: ' +ptp.id);
          }     
       }
       system.debug('The NewList is: ' +NewList+'......'+presentSAPCodesSet);
       if(presentSAPCodesSet.size()>0)
       {
          for(Account acc:[select id,SAP_Code__c  from Account where SAP_Code__c in:presentSAPCodesSet])
           {
                customer_LedgerWithAccountMap.put(acc.SAP_Code__c,acc.id);
           }
           system.debug('The customer_LedgerWithAccountMap is: ' +customer_LedgerWithAccountMap);
           for(Customer_Ledger__c ptp:NewList)
           {
               ptp.Account__c = customer_LedgerWithAccountMap.get(ptp.SAP_Code__c);
               system.debug('The  ptp.Account__c is: ' + ptp.Account__c);
               if(ptp.Type_of_Transaction__c=='Credit'){
                   ptp.Credit__c = ptp.Payment__c;
               }
               if(ptp.Type_of_Transaction__c=='Debit'){
                   ptp.Debit__c = ptp.Payment__c;
               }
               FinalListToUpdate.add(ptp);
           }
       }
       if(!FinalListToUpdate.isEmpty() && FinalListToUpdate.size() > 0) {
            update FinalListToUpdate;
       }
    }
}
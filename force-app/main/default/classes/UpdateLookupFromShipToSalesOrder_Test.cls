/* Created By         : Rajiv Kumar Singh
   Created Date       : 13/07/2018
   Class Name         : UpdateLookupFromShipToSalesOrder_Test
   Description        : 
   Last Modified By   : Rajiv Kumar Singh
   Last Modified Date : 13/07/2018
*/
@isTest
private class UpdateLookupFromShipToSalesOrder_Test
{ 
 // @istest
   private testmethod static void testMethod1()
   {
      Ship_To_Customer__c p1 = new Ship_To_Customer__c();
      p1.Name = 'testing';
      p1.Customer_Code__c = '6565666';
      p1.Sales_Office__c = 'East';
      p1.address__c ='test';
      insert p1;
      
      Sales_Order_Line_Item__c  ptp = new Sales_Order_Line_Item__c  ();
      ptp.Ship_To_SAP_Code__c = '6565666';
      ptp.Ship_To_Customer__c = p1.id;
      insert ptp;
      
      Sales_Order_Line_Item__c   pt = [select id, Ship_To_SAP_Code__c from Sales_Order_Line_Item__c  where id =:ptp.id];
      
      list<Id> ptrList = new list<Id>();
      ptrList.add(pt.id);
      
      UpdateLookupFromShipToSalesOrder.UpdateLookupValue(ptrList);
   }
}
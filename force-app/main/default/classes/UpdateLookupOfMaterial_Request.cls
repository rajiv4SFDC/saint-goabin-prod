/* Created By         : Rajiv Kumar Singh
   Created Date       : 11/06/2018
   Class Name         : UpdateLookupOfMaterial_Request
   Description        : update the particular Account record lookup field to Material_Request
   Last Modified By   : Rajiv Kumar Singh
   Last Modified Date : 11/06/2018
*/
public class UpdateLookupOfMaterial_Request{
  @InvocableMethod(label='UpdateLookupOfMaterial_Request' description='update the particular Account record lookup field to Material_Request')
  public static void UpdateLookupValue(List<Id> material_RequestIds)
    {   
       map<string,string> material_RequestWithAccountMap = new map<string,string>();  
       set<string> presentSAPCodesSet = new set<String>();
       list<Material_Request__c > NewList = new list<Material_Request__c>(); 
       list<Material_Request__c > FinalListToUpdate = new list<Material_Request__c>();

       for(Material_Request__c ptp:[select id,Bill_to_Customer__c,SAP_Code__c from Material_Request__c where id in:material_RequestIds])
       {
         if(ptp.SAP_Code__c != null)
          {
            presentSAPCodesSet.add(ptp.SAP_Code__c);      
            NewList.add(ptp);
            system.debug('The ptp.id is: ' +ptp.id);
          }     
       }
       system.debug('The NewList is: ' +NewList+'......'+presentSAPCodesSet);
       if(presentSAPCodesSet.size()>0)
       {
          for(Account acc:[select id,SAP_Code__c  from Account where SAP_Code__c in:presentSAPCodesSet])
           {
               material_RequestWithAccountMap.put(acc.SAP_Code__c,acc.id);
           }
           system.debug('The material_RequestWithAccountMap is: ' +material_RequestWithAccountMap);
           for(Material_Request__c ptp:NewList)
           {
               ptp.Bill_to_Customer__c = material_RequestWithAccountMap.get(ptp.SAP_Code__c);
               system.debug('The  ptp.Bill_to_Customer__c is: ' + ptp.Bill_to_Customer__c);
               FinalListToUpdate.add(ptp);
           }
       }
       if(!FinalListToUpdate.isEmpty() && FinalListToUpdate.size() > 0) {
            update FinalListToUpdate;
       }
    }
}
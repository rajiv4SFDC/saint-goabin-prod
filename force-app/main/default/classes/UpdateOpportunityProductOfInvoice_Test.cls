@isTest
private class UpdateOpportunityProductOfInvoice_Test
{ 
 // @istest
   private testmethod static void testMethod1()
   {
   /*   Product__c pc = new Product__c();
      pc.Name = 'infinity';
      pc.Description = 'glass';
      pc.Sub_Family__c = 'SPECIAL';
      pc.Coating__c = 'PLT';
      pc.Tint__c = 'BLUE';
      pc.Product Code = '123';
      insert pc; */
      Opportunity op = new Opportunity();
      op.Name = 'ajay';
      op.StageName = 'Specified';
      op.Probability = 10;
       op.CloseDate = date.today();
      insert op; 
       
      Opportunity_Product__c p1 = new Opportunity_Product__c();
      p1.Status__c = 'Selected';
     // p1.Name ='OP-138971';
      p1.Opportunity__c = op.id;
      insert p1;
      
      Invoice__c ptp = new Invoice__c();
      ptp.Name = '20';
	  ptp.Opportunity_Product_Id__c = 'OP-138971';
      ptp.Opportunity_Product__c = p1.id;
      insert ptp;
      
      Invoice__c  pt = [select id, Opportunity_Product_Id__c from Invoice__c  where id =:ptp.id];
      
    //  System.assertEquals(pt.product__c,p1.id);
      list<Id> ptrList = new list<Id>();
      ptrList.add(pt.id);
      
      UpdateOpportunityProductOfInvoice.UpdateLookupValue(ptrList);
   }
}
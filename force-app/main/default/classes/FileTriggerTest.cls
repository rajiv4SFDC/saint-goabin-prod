@isTest
public class FileTriggerTest {
 static testMethod void attTriggerTest1()
    {
        test.startTest();      
        Account pe = new Account(Name = 'Test Project Edition', Attachment_attached__c=True);
        insert pe;   
               Blob beforeblob=Blob.valueOf('Unit Test Attachment Body');

        ContentVersion cv = new ContentVersion();
        cv.title = 'test content trigger';      
        cv.PathOnClient ='test';           
        cv.VersionData =beforeblob;          
        insert cv;         

        ContentVersion testContent = [SELECT id, ContentDocumentId FROM ContentVersion where Id = :cv.Id];

        ContentDocumentLink contentlink=new ContentDocumentLink();
        contentlink.LinkedEntityId=pe.Id;
        contentlink.ShareType= 'V';
        contentlink.ContentDocumentId=testcontent.ContentDocumentId;

        insert contentlink;

       // delete contentlink;
      
        test.stopTest();
    }
}
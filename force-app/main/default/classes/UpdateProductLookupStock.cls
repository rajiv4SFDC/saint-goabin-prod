/* Created By         : Rajiv Kumar Singh
   Created Date       : 11/07/2018
   Class Name         : UpdateProductLookupStock
   Description        : update the perticular product record lookup field on Sales Order
   Last Modified By   : Rajiv Kumar Singh
   Last Modified Date : 11/07/2018
*/
public class UpdateProductLookupStock{

  @InvocableMethod(label='UpdateProductLookupStock' description='update the perticular product record lookup field on Stock')
  public static void UpdateLookupValue(List<Id> SalesIds)
    {
       
       
       map<string,string> SalesWithProductMap = new map<string,string>();  
       set<string> presentProductCodesSet = new set<String>();
       list<Product_Stock__c> NewList = new list<Product_Stock__c>(); 
       list<Product_Stock__c> FinalListToUpdate = new list<Product_Stock__c>();

       for(Product_Stock__c  ptp:[select id,Product_Item_Code__c,Product__c from Product_Stock__c where id IN: SalesIds])
       {
         if(ptp.Product_Item_Code__c != null)
          {
            presentProductCodesSet.add(ptp.Product_Item_Code__c);      
            NewList.add(ptp);
            system.debug('The ptp.id is: ' +ptp.id);
          }  
           
       }
       system.debug('The NewList is: ' +NewList+'......'+presentProductCodesSet);
       if(presentProductCodesSet.size()>0)
       {
          for(product2 pd:[select id,ProductCode,External_Id__c  from product2 where External_Id__c in:presentProductCodesSet])
           {
            SalesWithProductMap.put(pd.External_Id__c,pd.id);
           }
       system.debug('The SalesWithProductMap is: ' +SalesWithProductMap);
        for(Product_Stock__c ptp:NewList)
        {
        ptp.Product__c = SalesWithProductMap.get(ptp.Product_Item_Code__c);
        system.debug('The  ptp.product__c is: ' + ptp.Product_Item_Code__c);
        FinalListToUpdate.add(ptp);
        }
       }
       
          update FinalListToUpdate;
       
    }
}
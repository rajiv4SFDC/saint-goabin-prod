/* Created By: Rajiv Kumar Singh
Created Date: 07/06/2017
Class Name: customerShipBillUpdateAPITest
Description : Test class for customerShipBillUpdateAPI
Last Modified By :Rajiv Kumar Singh
Last Modified Date: 07/06/2017
*/

@IsTest
private class customerShipBillUpdateAPITest{
    
    static testMethod void testPostMethod(){
        String JSONMsg = '[{"SAPcode":"3321","BillToStatus":"True","ShipToStatus":"False"},{"SAPcode":"1231234","BillToStatus":"False","ShipToStatus":"True"}]';
        RestRequest request = new RestRequest();
        request.requestUri ='/services/apexrest/Bill2ShipStatus';
        request.httpMethod = 'POST';
        request.requestBody = Blob.valueof(JSONMsg);
        RestContext.request = request;
        RestContext.response = new RestResponse();
        customerShipBillUpdateAPI.doHandleInsertRequest();
        
        JSONMsg = '[1';
        request = new RestRequest();
        request.requestUri ='/services/apexrest/Bill2ShipStatus';
        request.httpMethod = 'POST';
        request.requestBody = Blob.valueof(JSONMsg);
        RestContext.request = request;
        RestContext.response = new RestResponse();
        customerShipBillUpdateAPI.doHandleInsertRequest();
        
        request = new RestRequest();
        request.requestUri ='/services/apexrest/Bill2ShipStatus';
        request.httpMethod = 'POST';
        request.requestBody = Blob.valueof('');
        RestContext.request = request;
        RestContext.response = new RestResponse();
        customerShipBillUpdateAPI.doHandleInsertRequest();
    }
}
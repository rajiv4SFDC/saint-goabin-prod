public class PlantVisitManager {
    
    public void fetchBeforeInsertRecordsForUpdation(List<Plant_Visit__c> PVList){
        Map<Id,List<Plant_Visit__c>> PVMap = new Map<Id,List<Plant_Visit__c>>();
        Map<Id,User> ownerMap;
        Set<Id> oppOwnerIds = new Set<Id>();
   		
        //preparing MAP<ownerid,list of his PV records>
        for(Plant_Visit__c n : (PVList)){
              List<Plant_Visit__c> PVOwnerList = PVMap.get(n.OwnerId);
                if(PVOwnerList!=null && !PVOwnerList.isEmpty())
                    PVOwnerList.add(n);
                else {
                    PVOwnerList = new List<Plant_Visit__c>();
                    PVOwnerList.add(n);
                }
            PVMap.put(n.OwnerId,PVOwnerList);
        }  
        
        ownerMap = new Map<Id,User>([SELECT Id, Name, Sales_Area__c, Region__c FROM User WHERE Id IN :oppOwnerIds]);
        
        //populating KAM region on PV record
        for(Plant_Visit__c p : PVList){ // loop through Plant Visit records from Trigger.new and update KAM Region as per new owner's region
            if(ownerMap.containsKey(p.OwnerId)){
                p.KAM_Region__c = ownerMap.get(p.OwnerId).Region__c;  
            }                        
        }  
        
        //updating D&C Manager and Regional Manager field values on PV Record
        new UserManagerFieldsHandler().updatePVFields(PVMap,FALSE);
    }
    
    public void fetchBeforeUpdateRecordsForUpdation(List<Plant_Visit__c> newPVList, Map<Id,Plant_Visit__c> PVOldMap){
        Map<Id,List<Plant_Visit__c>> PVMap = new Map<Id,List<Plant_Visit__c>>();
        Map<Id,User> ownerMap;
        Set<Id> oppOwnerIds = new Set<Id>();

        //preparing MAP<ownerid,list of his PV records>
        for(Plant_Visit__c n : newPVList){
            Plant_Visit__c o = PVOldMap.get(n.Id);
            if((o.OwnerId != n.OwnerId) && (n.Approval_Status__c.equals('Rejected') || n.Approval_Status__c.equals('Draft'))){
                List<Plant_Visit__c> PVOwnerList = PVMap.get(n.OwnerId);
                oppOwnerIds.add(n.OwnerId);
                if(PVOwnerList!=null && !PVOwnerList.isEmpty())
                    PVOwnerList.add(n);
                else {
                    PVOwnerList = new List<Plant_Visit__c>();
                    PVOwnerList.add(n);
                }
                
                PVMap.put(n.OwnerId,PVOwnerList);
            }            
        }       
   
        ownerMap = new Map<Id,User>([SELECT Id, Name, Sales_Area__c, Region__c FROM User WHERE Id IN :oppOwnerIds]);
        
        //updating KAM region on PV record
        for(Plant_Visit__c p : newPVList){// loop through Plant Visit records from Trigger.new and update KAM Region as per new owner's region
            if(ownerMap.containsKey(p.OwnerId)){
                p.KAM_Region__c = ownerMap.get(p.OwnerId).Region__c;  
            }                        
        }  
        
        //updating D&C Manager and Regional Manager field values on PV Record
        new UserManagerFieldsHandler().updatePVFields(PVMap,FALSE);
    }
    
}
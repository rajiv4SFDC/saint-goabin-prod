/* Created By: Rajiv Kumar Singh
Created Date: 16/12/2018
Class Name: productcategoryUpdateAPITest
Description : Test class for productcategoryUpdateAPI
Last Modified By :Rajiv Kumar Singh
Last Modified Date: 16/12/2018
*/

@IsTest
private class productcategoryUpdateAPITest{
    
    static testMethod void testPostMethod(){
        String JSONMsg = '[{"name":"Coater","externalID":"T12e"}]';
        RestRequest request = new RestRequest();
        request.requestUri ='/services/apexrest/productCategory';
        request.httpMethod = 'POST';
        request.requestBody = Blob.valueof(JSONMsg);
        RestContext.request = request;
        RestContext.response = new RestResponse();
        productcategoryUpdateAPI.doHandleInsertRequest();
        
        JSONMsg = '[1';
        request = new RestRequest();
        request.requestUri ='/services/apexrest/productCategory';
        request.httpMethod = 'POST';
        request.requestBody = Blob.valueof(JSONMsg);
        RestContext.request = request;
        RestContext.response = new RestResponse();
        productcategoryUpdateAPI.doHandleInsertRequest();
        
        request = new RestRequest();
        request.requestUri ='/services/apexrest/productCategory';
        request.httpMethod = 'POST';
        request.requestBody = Blob.valueof('');
        RestContext.request = request;
        RestContext.response = new RestResponse();
        productcategoryUpdateAPI.doHandleInsertRequest();
    }
}
/* Created By         : Rajiv Kumar Singh
   Created Date       : 15/06/2018
   Class Name         : PriceRequestTriggerHandler1_Test
   Description        : 
   Last Modified By   : Rajiv Kumar Singh
   Last Modified Date : 15/06/2018
*/
@isTest
private class PriceRequestTriggerHandler1_Test
{ 
  @istest 
   private static void testMethod1()
   {  
        
        
        
    Test.startTest();    
        //creating user record for the above role. 

 //  UserRole r = new UserRole(DeveloperName = 'MyCustomRole', Name = 'Regional Manager East');
 //   insert r;



  UserRole r = [select id,name from userrole where name='Regional Manager East'];
 
 
User u = new User(
     ProfileId = [SELECT Id FROM Profile WHERE Name = 'System Administrator'].Id,
     LastName = 'last',
     Email = 'puser2323000@amamama.com',
     Username = 'pus2323er000@amamama.com' + System.currentTimeMillis(),
     CompanyName = 'TEST',
     Title = 'title',
     Alias = 'alias',
     TimeZoneSidKey = 'America/Los_Angeles',
     EmailEncodingKey = 'UTF-8',
     LanguageLocaleKey = 'en_US',
     LocaleSidKey = 'en_US',
     UserRoleId = r.Id,
     Phone  ='123123123123123'
);
insert u;


           Test.stopTest(); 
           
      user u1=[select id,name from user where id=:userinfo.getuserid()];
           
   System.runAs(u1) {        
   NewcustomerrequestforPR__c newcustom = new NewcustomerrequestforPR__c();
        newcustom.Active__c = true;
        newcustom.Role_Name__c ='Regional Manager East';
        newcustom.Name ='East';
        insert newcustom; 
         
        Enquiry__c enc = new Enquiry__c();
        enc.Account_Name__c = 'testing';
        enc.GD_Reference_Number__c = '12';       
        insert enc;
              
        enquiry_Line_Item__c enitem = new enquiry_Line_Item__c();
       //enitem.Name = 'test';
       enitem.Reference_Number__c = '12';
       enitem.Enquiry__c = enc.Id;
       insert enitem;
      
       
       // code coverage for doFetchPriceRequest();
         Account acc = new Account();
        acc.Name ='test3';
        acc.Type ='Industry';
        acc.Sales_Area_Region__c ='South';
        acc.PAN_Number__c = 'AAAPL1234C';
       acc.Sales_Office_ERP__c = 'BLR1';
       acc.Sales_Area__c ='KA1';
        acc.RecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('ERP').getRecordTypeId();
        insert acc;
        
        Opportunity opp = new Opportunity();
        String oppid1;
        opp.AccountId =acc.Id;
        opp.Name ='test1';
        opp.CloseDate = Date.today();
        opp.Amount = 1000;
        opp.StageName ='Prospecting';
        opp.Probability= 0.5;
        oppid1=acc.id;
        insert opp;
        
        acc = new Account();
        acc.Name ='test31';
        acc.Type ='Industry1';
        acc.PAN_Number__c ='AAAPL1234C';
        acc.Sales_Area_Region__c ='South';
       acc.Sales_Office_ERP__c = 'BLR1';
       acc.Sales_Area__c ='KA1';
        acc.RecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('CRM').getRecordTypeId();
        insert acc;
        
        Key_Account__c keyAcct = new Key_Account__c();
        keyAcct.Opportunity__c = opp.Id;
        keyAcct.Account__c = acc.Id;
        insert keyAcct;
        
        
        account  acc2 = new account();
        acc2.Name ='main acc for erp';
        acc2.Type ='Industry2';
        acc2.PAN_Number__c ='AAAPL1234f';
        acc2.Sales_Area_Region__c ='South';
        acc2.sap_code__c = '123123';
        acc2.Sales_Office_ERP__c = 'BLR1';
       acc2.Sales_Area__c ='KA1';
        acc2.RecordTypeId = Schema.SObjectType.account.getRecordTypeInfosByName().get('ERP').getRecordTypeId();
        insert acc2;
       
        account  acc3 = new account();
        acc3.Name ='main acc for erp2';
        acc3.Type ='Industry2';
        acc3.PAN_Number__c ='bldpp4721t';
        acc3.Sales_Area_Region__c ='South';
        acc3.sap_code__c = '80231578';
       acc3.Sales_Office_ERP__c = 'BLR1';
       acc3.Sales_Area__c ='KA1';

        acc3.RecordTypeId = Schema.SObjectType.account.getRecordTypeInfosByName().get('ERP').getRecordTypeId();
        insert acc3;
        
        opp.Customer_Name__c = keyAcct.Id;
        update opp;
        
        Product2 masterProduct = new Product2();
        masterProduct.Name = opp.id;
        masterProduct.Article_number__c ='test1234';
        masterProduct.Product_Category__c ='Shower Cubicles';
        masterProduct.master_product__c = true;
        insert masterProduct;
        
        Product2 pro = new Product2();
        pro.Name = 'test prod';
        pro.Product_Category__c ='Shower Cubicles';
        pro.ProductCode ='test123';
        pro.Parent_product__c = masterProduct.id;
        pro.Article_number__c = 'test123';
        pro.Quantity__c  = 2;
        pro.global_product__c = true;
        insert pro;   
        List<Product2> prodList = new List<product2>();
        prodList.add(pro);
       Product2 p2 =[select id,name,external_id__c from Product2 where id=:pro.id];
        Product_Tier_Pricing__c ptr = new Product_Tier_Pricing__c();
        ptr.name ='testing tier';
        ptr.Business_Location__c ='BLR1';
        ptr.Product__c = pro.id;
        ptr.Product_Code__c ='test12312';
        ptr.Tier_Name__c ='T1';
        ptr.Thickness__c =22;
        ptr.Item_Code__c = p2.external_id__c;
        insert ptr;
        
        List<ProductsWrapper> prodWrapper =  new List<ProductsWrapper>();
        ProductsWrapper prodWrap = new ProductsWrapper();
        prodWrap.prdName = pro.Name;
        prodWrap.prdCode = pro.ProductCode;
        prodWrap.prdQuantitiy = Integer.valueof(pro.Quantity__c);
        prodWrap.prdArticlenumber = pro.Article_number__c;
        prodWrap.prdParentId = pro.Parent_product__c;
        //prodWrap.oppProductLineItemId = pro.Name;
        prodWrapper.add(prodWrap);
        
        Opportunity_Product__c oppproduct = new Opportunity_Product__c();
        oppproduct.Opportunity__c = opp.Id;
        oppproduct.Product__c = masterProduct.Id;
        insert oppproduct;
        
        Opportunity_Product_line_items__c oppline = new Opportunity_Product_line_items__c();
        oppline.Opportunity_Product__c=oppproduct.id;
        oppline.Product__c = pro.Id;
        oppline.Quantity__c = 2;
        insert oppline;
        
        Product_Forecast__c forecast = new Product_Forecast__c();
        forecast.Quantity__c = 10000;
        forecast.Opportunity_Product__c = oppproduct.Id;
        insert forecast;
        
        Price_Request__c pr = new Price_Request__c();
        pr.Price_Type__c   = 'Tier Price';
        pr.Opportunity_Product__c = oppproduct.id;
        pr.Valid_From__c = System.today();
        pr.Valid_To__c = System.today();
        pr.Requested_Quantity__c = 1000;
        pr.Approval_status__c ='Approved';
        pr.Regional_head_sales_and_marketing_User__c = userInfo.getUserId();
        pr.Regional_Manager__c =  userInfo.getUserId();
        pr.Enquiry_Line_Item__c = enitem.id;
        pr.Enquiry__c = enc.id;
        pr.Customer__c = acc2.id;
        pr.Product_lookup__c = pro.id;

       // pr.Regional_manager_special_price__c =  userInfo.getUserId();
        insert pr;       
       
     
       
   //   Price_Request__c  pt = [select id from Price_Request__c  where id =: pr.id];
      
    //  System.assertEquals(pt.product__c,p1.id);
   //   list<Price_Request__c> ptrList = new list<Price_Request__c>();
  //    ptrList.add(ptrList);
      
      PriceRequestTriggerHandler1.updatereference([select id,Enquiry__c,Enquiry_Line_Item__c  from Price_Request__c  where id =: pr.id]);
      PriceRequestTriggerHandler1.updatePriceManager([select id,Customer__c,Regional_manager_special_price__c,Product_lookup__c from Price_Request__c where id=:pr.id]);
      /*PriceRequestTriggerHandler1.doCheckForBeforeUpdate([select id,Customer__c,Regional_manager_special_price__c from Price_Request__c where id =:pr.id]);*/
      }
   }
    @istest 
   private static void testMethod2()
   {  
        
        
        
    Test.startTest();    
        //creating user record for the above role. 

 //  UserRole r = new UserRole(DeveloperName = 'MyCustomRole', Name = 'Regional Manager East');
 //   insert r;



  UserRole r = [select id,name from userrole where name='Regional Manager East'];
 
 
User u = new User(
     ProfileId = [SELECT Id FROM Profile WHERE Name = 'System Administrator'].Id,
     LastName = 'last',
     Email = 'puser2323000@amamama.com',
     Username = 'pus2323er000@amamama.com' + System.currentTimeMillis(),
     CompanyName = 'TEST',
     Title = 'title',
     Alias = 'alias',
     TimeZoneSidKey = 'America/Los_Angeles',
     EmailEncodingKey = 'UTF-8',
     LanguageLocaleKey = 'en_US',
     LocaleSidKey = 'en_US',
     UserRoleId = r.Id,
     Phone  ='123123123123123'
);
insert u;


           Test.stopTest(); 
           
      user u1=[select id,name from user where id=:userinfo.getuserid()];
           
   System.runAs(u1) {        
   NewcustomerrequestforPR__c newcustom = new NewcustomerrequestforPR__c();
        newcustom.Active__c = true;
        newcustom.Role_Name__c ='Regional Manager East';
        newcustom.Name ='East';
        insert newcustom; 
         
        Enquiry__c enc = new Enquiry__c();
        enc.Account_Name__c = 'testing';
        enc.GD_Reference_Number__c = '12';       
        insert enc;
              
        enquiry_Line_Item__c enitem = new enquiry_Line_Item__c();
       //enitem.Name = 'test';
       enitem.Reference_Number__c = '12';
       enitem.Enquiry__c = enc.Id;
       insert enitem;
      
       
       // code coverage for doFetchPriceRequest();
         Account acc = new Account();
         acc.Clear_Tint_Tier__c = 'T1';
        acc.Name ='test3';
        acc.Type ='Industry';
        acc.Sales_Area_Region__c ='South';
        acc.PAN_Number__c = 'AAAPL1234C';
       acc.Sales_Office_ERP__c = 'BLR1';
        acc.Sales_Area__c ='KA1';
        acc.RecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('ERP').getRecordTypeId();
        insert acc;
        
        Opportunity opp = new Opportunity();
        String oppid1;
        opp.AccountId =acc.Id;
        opp.Name ='test1';
        opp.CloseDate = Date.today();
        opp.Amount = 1000;
        opp.StageName ='Prospecting';
        opp.Probability= 0.5;
        oppid1=acc.id;
        insert opp;
        
        acc = new Account();
        acc.Name ='test31';
        acc.Type ='Industry1';
        acc.PAN_Number__c ='AAAPL1234C';
        acc.Sales_Area_Region__c ='South';
       acc.Sales_Office_ERP__c = 'BLR1';
       acc.Clear_Tint_Tier__c = 'T1';
       acc.Sales_Area__c ='KA1';
        acc.RecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('CRM').getRecordTypeId();
        insert acc;
        
        Key_Account__c keyAcct = new Key_Account__c();
        keyAcct.Opportunity__c = opp.Id;
        keyAcct.Account__c = acc.Id;
        insert keyAcct;
        
        
        account  acc2 = new account();
        acc2.Name ='main acc for erp';
        acc2.Type ='Industry2';
        acc2.PAN_Number__c ='AAAPL1234f';
        acc2.Sales_Area_Region__c ='South';
        acc2.sap_code__c = '123123';
        acc2.Sales_Office_ERP__c = 'BLR1';
        acc2.Clear_Tint_Tier__c = 'T1';
        acc2.Sales_Area__c ='KA1';
        acc2.RecordTypeId = Schema.SObjectType.account.getRecordTypeInfosByName().get('ERP').getRecordTypeId();
        insert acc2;
       
        account  acc3 = new account();
        acc3.Name ='main acc for erp2';
        acc3.Type ='Industry2';
        acc3.PAN_Number__c ='bldpp4721t';
        acc3.Sales_Area_Region__c ='South';
        acc3.sap_code__c = '80231578';
       acc3.Sales_Office_ERP__c = 'BLR1';
       acc3.Clear_Tint_Tier__c = 'T1';
        acc3.Sales_Area__c ='KA1';
        acc3.RecordTypeId = Schema.SObjectType.account.getRecordTypeInfosByName().get('ERP').getRecordTypeId();
        insert acc3;
        
        opp.Customer_Name__c = keyAcct.Id;
        update opp;
        
        Product2 masterProduct = new Product2();
        masterProduct.Name = opp.id;
        masterProduct.Article_number__c ='test1234';
        masterProduct.Product_Category__c ='Shower Cubicles';
        masterProduct.master_product__c = true;
        insert masterProduct;
        
        Product2 pro = new Product2();
        pro.Name = 'test prod';
        pro.Product_Category__c ='CLEAR';
        pro.ProductCode ='test123';      
        pro.Article_number__c = 'test123';
        pro.Quantity__c  = 2;
        pro.global_product__c = true;
        pro.external_id__c = 'testExternalId';
        insert pro;   
        List<Product2> prodList = new List<product2>();
        prodList.add(pro);
        
        
        
       Product2 p2 =[select id,name,external_id__c from Product2 where id=:pro.id];
       system.debug('The p2.external_id__c is :' +p2.external_id__c);
        Product_Tier_Pricing__c ptr = new Product_Tier_Pricing__c();
        ptr.name ='testing tier';
        ptr.Business_Location__c ='BLR1';
        ptr.Product__c = pro.id;
        ptr.Product_Code__c ='test12312';
        ptr.Tier_Name__c ='T1';
        ptr.Thickness__c =22;
        ptr.Tier_Pricing__c = 100;
        ptr.Item_Code__c = p2.external_id__c;
        insert ptr;
        
        List<ProductsWrapper> prodWrapper =  new List<ProductsWrapper>();
        ProductsWrapper prodWrap = new ProductsWrapper();
        prodWrap.prdName = pro.Name;
        prodWrap.prdCode = pro.ProductCode;
        prodWrap.prdQuantitiy = Integer.valueof(pro.Quantity__c);
        prodWrap.prdArticlenumber = pro.Article_number__c;
        prodWrap.prdParentId = pro.Parent_product__c;
        //prodWrap.oppProductLineItemId = pro.Name;
        prodWrapper.add(prodWrap);
        
        Opportunity_Product__c oppproduct = new Opportunity_Product__c();
        //oppproduct.Name ='OP-138993';
        oppproduct.Opportunity__c = opp.Id;
        oppproduct.Product__c = masterProduct.Id;
        insert oppproduct;
        
        Opportunity_Product_line_items__c oppline = new Opportunity_Product_line_items__c();
        oppline.Opportunity_Product__c=oppproduct.id;
        oppline.Product__c = pro.Id;
        oppline.Quantity__c = 2;
        insert oppline;
        
        Product_Forecast__c forecast = new Product_Forecast__c();
        forecast.Quantity__c = 10000;
        forecast.Opportunity_Product__c = oppproduct.Id;
        insert forecast;
       try{
        Price_Request__c pr = new Price_Request__c();
       // pr.Price_Type__c   = 'Tier Price';
        pr.Opportunity_Product__c = oppproduct.id;
        pr.Valid_From__c = System.today();
        pr.Valid_To__c = System.today();
        pr.Requested_Quantity__c = 1000;
       // pr.Approval_status__c ='Approved';
        pr.Regional_head_sales_and_marketing_User__c = userInfo.getUserId();
        pr.Regional_Manager__c =  userInfo.getUserId();
        pr.Enquiry_Line_Item__c = enitem.id;
        pr.Enquiry__c = enc.id;
        pr.Required_Price__c = 200;
        pr.Customer__c = acc2.id;
        pr.Product_lookup__c = pro.id;
       pr.RecordTypeId = Schema.SObjectType.Price_Request__c.getRecordTypeInfosByName().get('Customer Request').getRecordTypeId();

       
       // pr.Regional_manager_special_price__c =  userInfo.getUserId();
        insert pr;       
       
       price_request__c pr2 = new price_request__c();
       pr2.id = pr.id;
       pr2.Customer__c = acc3.id;
       update pr2;
       } catch(exception ert)
       {
           system.debug('The ert is:'+ert);
       }
   //   Price_Request__c  pt = [select id from Price_Request__c  where id =: pr.id];
      
    //  System.assertEquals(pt.product__c,p1.id);
   //   list<Price_Request__c> ptrList = new list<Price_Request__c>();
  //    ptrList.add(ptrList);
      
      }
   }
}
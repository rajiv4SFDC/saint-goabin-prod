/* Created By         : Rajiv Kumar Singh
   Created Date       : 12/06/2018
   Class Name         : UpdateInvoiceLookupinvoiceAgeingTest
   Description        : 
   Last Modified By   : Rajiv Kumar Singh
   Last Modified Date : 12/06/2018
*/
@isTest
private class UpdateInvoiceLookupinvoiceAgeingTest
{ 
 // @istest
   private testmethod static void testMethod1()
   {
      Invoice__c ptp = new Invoice__c();
      ptp.name = 'SF1234';
      ptp.Product_Code__c = '401-518-000-000-000';
      ptp.Thickness__c = '0600';
      insert ptp;
      
      Invoice_Wise_Ageing__c ds = new Invoice_Wise_Ageing__c();
      ds.Invoice_No__c ='SF1234-0600';
      insert ds;
      
      
      
      Invoice_Wise_Ageing__c pt = [select id, Invoice_No__c from Invoice_Wise_Ageing__c where id =:ds.id];
      
    //  System.assertEquals(pt.Invoice__c,ptp.id);
      list<Id> ptrList = new list<Id>();
      ptrList.add(pt.id);
      
      UpdateInvoiceLookupinvoiceAgeing.UpdateLookupValue(ptrList);
   }
}
/* Created By: Rajiv Kumar Singh
Created Date: 13/12/2017
Class Name: productcategoryUpdateAPI
Description : product category upsert
Last Modified By :Rajiv Kumar Singh
Last Modified Date: 13/12/2017
*/
@RestResource(urlMapping='/productCategory')
global class productcategoryUpdateAPI {
    @HttpPost
    global static void doHandleInsertRequest(){
        
        List<statusWrapper> wrapList = new List<statusWrapper>();
        List<Product_Category__c> accList = new List<Product_Category__c>();
        RestRequest Req = RestContext.request;
        RestResponse res = RestContext.response;
        String body = Req.requestBody.toString().trim();
        
        if(body != null && body != '') {
            
            try {
                wrapList = (List<statusWrapper>)JSON.deserialize(body, List<statusWrapper>.class);
                for(statusWrapper we :wrapList){
                    Product_Category__c acc = new Product_Category__c();
                    if(we.name!=null && we.name!=''){
                        acc.name = we.name;
                    }
                    if(we.externalID!=null && we.externalID!=''){
                        acc.External_Id__c  = we.externalID;
                    }
                    
                    accList.add(acc);  
                }
                Schema.SObjectField ftoken = Product_Category__c.Fields.External_Id__c;
                Database.UpsertResult[] srList = Database.upsert(acclist,ftoken,false);           
                res.statusCode = 200;
                res.responseBody = Blob.valueOf(String.valueof(acclist));
                
                
            } catch (Exception e) {
                
                CommonUtilitiesHandler.handleExceptions(e);
                res.statusCode = 500;
                res.responseBody = Blob.valueOf(e.getMessage());
            }
        } else {
            res.statusCode = 500;
            res.responseBody = Blob.valueOf('Invalid Request');
        }   
    }
    
    public class statusWrapper{
        public String name{get;set;}
        public string externalID{get;set;}
       
        
    }
}
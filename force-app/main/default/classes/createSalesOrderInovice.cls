/*
   Description  : Used to map the perticular invoice with sales order. Whenever a new inovice is created in salesforce then,
                  a new junction object record (sales order invoice item) will be create under sales order and invoice .
  
*/
public class createSalesOrderInovice{
  @InvocableMethod(label='createSalesOrderInovice' description='create Sales Order Invoice')
  public static void UpdateLookupValue(List<Id> inoviceIds)
    {   
      
      // collect the Sales order external id fields and match with sales order records. 
      
      set<string> Sales_order_external_idSet = new set<string>();
      for(Invoice__c inv:[select id,name,Sales_order__c from Invoice__c where id in:inoviceIds])
      {
         Sales_order_external_idSet.add(inv.Sales_order__c);
      }
      
      // query the existing salesorders in the system. 
     map<String,Sales_Order_Line_Item__c> SalesOrderWithExernalIdMap = new map<String,Sales_Order_Line_Item__c>();
     
     if(Sales_order_external_idSet.size()>0)
     {
         for(Sales_Order_Line_Item__c so:[select id,name,Enquiry_Line_Item__c,External_Id__c,Shipping_Status__c from Sales_Order_Line_Item__c where External_Id__c in:Sales_order_external_idSet])
         {        
         SalesOrderWithExernalIdMap.put(so.External_Id__c,so);
         }
     }
     
     list<Sales_Order_Invoice__c> salesOrderInvList = new list<Sales_Order_Invoice__c>();
     
      for(Invoice__c inv:[select id,name,Sales_order__c from Invoice__c where id in:inoviceIds])
      {
          if(SalesOrderWithExernalIdMap.containsKey(inv.Sales_order__c) && SalesOrderWithExernalIdMap.get(inv.Sales_order__c)!= NULL)
          {
             Sales_Order_Invoice__c soi = new Sales_Order_Invoice__c();
                soi.Invoice__c = inv.id;
                soi.Sales_Order_Line_Item__c = SalesOrderWithExernalIdMap.get(inv.Sales_order__c).Id;
                salesOrderInvList.add(soi);
          }
      }
      
      if(salesOrderInvList.size()>0)
      insert salesOrderInvList;
     
      
      
    }
    
 }
global class TargetActualBatchScheduler implements Schedulable {
    global Date startDate ;
    
    global TargetActualBatchScheduler(){}
 
    global void execute(SchedulableContext sc){
        TargetActualBatch ivT = new TargetActualBatch();
        Database.executeBatch(ivT);
    }
    /*
                    ///////////////  Schedule Code for firing it in Production  /////////////
 String cronStr = '0 0 0/1 1/1 * ? *';
System.schedule('Process Accs Job', cronStr, pa);

  String cronStr = '0 0 0/1 1/1 * ? *';
  String jobID = System.schedule(‘Invoice Tagging Scheduler’, cronStr , new InvoiceTaggingScheduler());
  System.debug('JOB id :'+jobID);
*/
}
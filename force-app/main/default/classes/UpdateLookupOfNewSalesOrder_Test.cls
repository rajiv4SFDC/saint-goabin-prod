/* Created By         : Rajiv Kumar Singh
   Created Date       : 14/06/2018
   Class Name         : UpdateLookupOfNewSalesOrder_Test
   Description        : 
   Last Modified By   : Rajiv Kumar Singh
   Last Modified Date : 14/06/2018
*/
@isTest
private class UpdateLookupOfNewSalesOrder_Test
{ 
 // @istest
   private testmethod static void testMethod1()
   {
      Enquiry_Line_Item__c p1 = new Enquiry_Line_Item__c();
      p1.Name = 'testing';
      p1.Reference_Number__c = '6565666';
      insert p1;
      
      Sales_Order__c ptp = new Sales_Order__c();
      ptp.Reference_Number__c = '6565666';
      ptp.Enquiry_Line_Item__c = p1.id;
      insert ptp;
      
      Sales_Order__c  pt = [select id, Reference_Number__c from Sales_Order__c  where id =:ptp.id];
      
      list<Id> ptrList = new list<Id>();
      ptrList.add(pt.id);
      
      UpdateLookupOfNewSalesOrder.UpdateLookupValue(ptrList);
   }
}
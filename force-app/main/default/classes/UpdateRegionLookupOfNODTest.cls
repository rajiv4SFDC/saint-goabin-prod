/* Created By         : Rajiv Kumar Singh
   Created Date       : 06/07/2018
   Class Name         : UpdateRegionLookupOfNOD
   Description        : 
   Last Modified By   : Rajiv Kumar Singh
   Last Modified Date : 06/07/2018
*/
@isTest
private class UpdateRegionLookupOfNODTest
{ 
 // @istest
   private testmethod static void testMethod1()
   {
      Region__c p1 = new Region__c();
      p1.Name = 'South';
      insert p1;
      
      Region_Wise_NOD_s__c ptp = new Region_Wise_NOD_s__c();
      ptp.Region_Name__c = 'South';
      ptp.Region__c = p1.id;
      insert ptp;
      
      Region_Wise_NOD_s__c pt = [select id, Region_Name__c from Region_Wise_NOD_s__c where id =:ptp.id];
      
    //  System.assertEquals(pt.product__c,p1.id);
      list<Id> ptrList = new list<Id>();
      ptrList.add(pt.id);
      
      UpdateRegionLookupOfNOD.UpdateLookupValue(ptrList);
   }
}
Global class BatchMassDeleteRecs Implements Database.batchable<sobject>{

     global Database.QueryLocator start(Database.BatchableContext BC){

  String query='select id from Product_Tier_Pricing__c';
      return Database.getQueryLocator(query);

     }

     global  void execute(Database.BatchableContext BC,List<SObject> scope){
         delete scope;

    }

    global void finish(Database.BatchableContext BC){

    }

 }
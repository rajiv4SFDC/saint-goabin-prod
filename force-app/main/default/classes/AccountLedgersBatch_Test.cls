/* Created By         : Rajiv Kumar Singh
   Created Date       : 15/06/2018
   Class Name         : AccountLedgersBatch_Test
   Description        : 
   Last Modified By   : Rajiv Kumar Singh
   Last Modified Date : 15/06/2018
*/
@isTest
public class AccountLedgersBatch_Test {
    static testMethod void testLedgerBatch() {
        Date today = System.today();
        Date todays = today.adddays(-1);
        Id RecordTypeIdContact = Schema.SObjectType.Account.getRecordTypeInfosByName().get('ERP').getRecordTypeId();
       // Date today = todays;
        Account acc = new Account();
        acc.Name = 'ajay';
        acc.SAP_Code__c='1234';
        acc.RecordTypeId  = RecordTypeIdContact;
        acc.Sales_Area_Region__c = 'East';
        acc.Type = 'PMC';
        insert acc;
        
        Customer_Ledger__c custLdger = new Customer_Ledger__c();
        custLdger.Type_of_Transaction__c = 'Credit';
        custLdger.Payment__c = 100;
        custLdger.Account__c = acc.Id;
        custLdger.Created_Date__c = todays;
        insert custLdger;
        
        Customer_Ledger__c custLdger1 = new Customer_Ledger__c();
        custLdger1.Type_of_Transaction__c = 'Debit';
        custLdger1.Payment__c = 200;
        custLdger1.Account__c = acc.Id;
        custLdger1.Created_Date__c = todays;
        insert custLdger1;
        
        O_S_Balance__c osBal = new O_S_Balance__c();
        osBal.Account__c = acc.Id;
        osBal.Closing_balance__c = 500;
        osBal.Opening_Balance__c = 1000;
        osBal.Transaction_Date__c = todays;
        insert osBal;
        
        Test.startTest();
        AccountLedgersBatch accBatch = new AccountLedgersBatch();
        Database.executeBatch(accBatch);
        Test.stopTest();        
    }
}
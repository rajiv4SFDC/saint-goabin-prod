@Istest
public class FetchSalesOfficeTest {
    Static Testmethod void Salesnods(){
        Region__c rg= new Region__c();
        rg.name='South';
        insert rg;
        
        Sales_Office__c so =new Sales_Office__c();
        so.name='BLR2';
        so.Region__c=rg.id;
        insert so;
        
        String Dt2='WPR1';
        Date Dt1=System.today();
        DateTIME Dt =System.Today()+3;
        Sales_Office_Wise_NOD_s__c sow= new Sales_Office_Wise_NOD_s__c();
        sow.Sales_Office_Name__c='BLR2';
        sow.Sales_Office__c=so.Id;
        sow.Average_Outstanding__c=55;
        sow.No_of_Days__c=6;
        sow.Posting_Date__c=Dt1;
        Insert sow;
    
        Test.startTest();
        FetchSalesOffice.fetchRecord(Dt1,Dt,Dt2);
        FetchSalesOffice.selectedYear();
        FetchSalesOffice.salesOfficeNames();
        FetchSalesOffice.selectedMonth();
        FetchSalesOffice.fetchingRecords('Oct','2018','CAL1');
        Test.stopTest();
    
    }

}
/* Created By         : Rajiv Kumar Singh
   Created Date       : 11/07/2018
   Class Name         : UpdateLookupFromShipToSalesOrder
   Description        : update the particular Ship to customer record lookup field on Sales Order
   Last Modified By   : Rajiv Kumar Singh
   Last Modified Date : 11/07/2018
*/
public class UpdateLookupFromShipToSalesOrder{
  @InvocableMethod(label='UpdateLookupFromShipToSalesOrder' description='update the particular Ship to customer record lookup field on Sales Order')
  public static void UpdateLookupValue(List<Id> salesOrdIds)
    {   
       map<string,string> salesWithAccountMap = new map<string,string>();  
       set<string> presentSAPCodesSet = new set<String>();
       list<Sales_Order_Line_Item__c> NewList = new list<Sales_Order_Line_Item__c>(); 
       list<Sales_Order_Line_Item__c> FinalListToUpdate = new list<Sales_Order_Line_Item__c>();

       for(Sales_Order_Line_Item__c ptp:[select id,Ship_To_SAP_Code__c from Sales_Order_Line_Item__c where id in:salesOrdIds])
       {
         if(ptp.Ship_To_SAP_Code__c != null)
          {
            presentSAPCodesSet.add(ptp.Ship_To_SAP_Code__c);      
            NewList.add(ptp);
            system.debug('The ptp.id is: ' +ptp.id);
          }     
       }
       system.debug('The NewList is: ' +NewList+'......'+presentSAPCodesSet);
       if(presentSAPCodesSet.size()>0)
       {
          for(Ship_To_Customer__c acc:[select id,Customer_Code__c  from Ship_To_Customer__c where Customer_Code__c in:presentSAPCodesSet])
           {
               salesWithAccountMap.put(acc.Customer_Code__c,acc.id);
           }
           system.debug('The salesWithAccountMap is: ' +salesWithAccountMap);
           for(Sales_Order_Line_Item__c ptp:NewList)
           {
               ptp.Ship_To_Customer__c = salesWithAccountMap.get(ptp.Ship_To_SAP_Code__c);
               system.debug('The  ptp.Ship_To_SAP_Code__c is: ' + ptp.Ship_To_SAP_Code__c);
               FinalListToUpdate.add(ptp);
           }
       }
       if(!FinalListToUpdate.isEmpty() && FinalListToUpdate.size() > 0) {
            update FinalListToUpdate;
       }
    }
}
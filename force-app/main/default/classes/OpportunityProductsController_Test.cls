@isTest
private class OpportunityProductsController_Test{
    
    public static testMethod void OppMethod1(){
        Test.starttest();
        Account acc = new Account();
        acc.Name ='test3';
        acc.Type ='Industry';
        acc.Sales_Area_Region__c ='North';
        acc.RecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('ERP').getRecordTypeId();
        insert acc;
        
        Opportunity opp = new Opportunity();
        String oppid1;
        opp.AccountId =acc.Id;
        opp.Name ='test1';
        opp.CloseDate = Date.today();
        opp.Amount = 1000;
        opp.StageName ='Prospecting';
        opp.Probability= 0.5;
        oppid1=acc.id;
        insert opp;
        
        Product2 masterProduct = new Product2();
        masterProduct.Name = opp.id;
        masterProduct.Article_number__c ='test1234';
        masterProduct.Product_Category__c ='Shower Cubicles';
        masterProduct.master_product__c = true;
        insert masterProduct;
        
        Product2 pro = new Product2();
        pro.Name = 'test prod';
        pro.Product_Category__c ='Shower Cubicles';
        pro.ProductCode ='test123';
        pro.Parent_product__c = masterProduct.id;
        pro.Article_number__c = 'test123';
        pro.Quantity__c  = 2;
        pro.global_product__c = true;
        insert pro;   
        List<Product2> prodList = new List<product2>();
        prodList.add(pro);
        
        List<ProductsWrapper> prodWrapper =  new List<ProductsWrapper>();
        ProductsWrapper prodWrap = new ProductsWrapper();
        prodWrap.prdName = pro.Name;
        prodWrap.prdCode = pro.ProductCode;
        prodWrap.prdQuantitiy = Integer.valueof(pro.Quantity__c);
        prodWrap.prdArticlenumber = pro.Article_number__c;
        prodWrap.prdParentId = pro.Parent_product__c;
        //prodWrap.oppProductLineItemId = pro.Name;
        prodWrapper.add(prodWrap);
        
        Opportunity_Product__c oppproduct = new Opportunity_Product__c();
        oppproduct.Opportunity__c = opp.Id;
        oppproduct.Product__c = masterProduct.Id;
        insert oppproduct;
        
        Opportunity_Product_line_items__c oppline = new Opportunity_Product_line_items__c();
        oppline.Opportunity_Product__c=oppproduct.id;
        oppline.Product__c = pro.Id;
        oppline.Quantity__c = 2;
        insert oppline;
        
     /*   OpportunityProductsController.productsWrapper wrap = new OpportunityProductsController.productsWrapper();
        wrap.prdCode ='test123';
        wrap.prdName = opp.id;
        wrap.prdQuantitiy = 2;
        wrap.prdArticlenumber ='test123';
        wrap.prdParentId = pro.id;  */
        
        OpportunityProductsController.getOpportunityDetail(opp.Id);
        OpportunityProductsController.getOpportunityProductDetail(oppproduct.Id);
        OpportunityProductsController.getNewProducts(masterProduct.Id);
        OpportunityProductsController.getSpecificProducts(pro.Id, null);
        
        List<ProductsWrapper> res = OpportunityProductsController.upsertOppProductAndLineItems(JSON.serialize(prodWrapper), masterProduct.Id, opp.Id, masterProduct.Id, pro.Id, oppproduct, 'Standard', acc.Id);
        
        //upsertOppProductAndLineItems(String prdlist, String parentPrdId, String opportunityid, String masterPrd, String glassPrd, Opportunity_Product__c oppProduct, String pdType, String customername){
   
        Test.stopTest();
    }//end of the oppMethod1
}//end of the test class
@isTest
public class TestTargetActualHandler {

	 public static String CRON_EXP = '0 0 17 1/1 * ? *';

	@testSetup
	public static void callFullDataFactory(){
		TestDataFactory.createCustomSetting();
		TestDataFactory.createTestProductData();
		TestDataFactory.createTestStateAndCityRecordData();
		TestDataFactory.createTestCatalogueBudgetData();
		TestDataFactory.createTestCampaignRecordData();
		TestDataFactory.createTestAccountData();
		TestDataFactory.createTestProjectData();
		TestDataFactory.createTestOpportunityData();
		TestDataFactory.createTestOpportunityProductData();
		TestDataFactory.createTestPriceRequestData();
		TestDataFactory.createTestProductForecateData();
		TestDataFactory.createTestForecateInvoiceData();
		TestDataFactory.createTestMockupRecordData();
		TestDataFactory.createTestInvoiceRecordData();
		TestDataFactory.createTestEventsRecordData();
        TestDataFactory.createTestTasksRecordData();
        TestDataFactory.createTestPlantVisitRecordData();
        TestDataFactory.createTestEmailMessageData();
		TestDataFactory.createTestTargetActualRecordData();
	}

	@isTest
	public static void coverTargetAndActualHandler(){
		
		Test.startTest();

		Date startdate = Date.newInstance(2017, 8, 1);
        //String jobId = System.schedule('TargetActualBatch', CRON_EXP, new TargetActualBatchScheduler(startdate)); 

        Date startdate2 = Date.newInstance(2017, 8, 1);
        String jobId2 = System.schedule('TargetActualBatch2', CRON_EXP, new TargetActualBatchScheduler()); 
	
        TargetActualBatch tv = new TargetActualBatch(Date.newInstance(2017,8, 20));
		Database.executeBatch(tv);
        
		Test.stopTest();

		// Cover TargetActualHandler

		new TargetActualHandler().getMonthAggregrateName(1);
		new TargetActualHandler().getMonthAggregrateName(2);
		new TargetActualHandler().getMonthAggregrateName(3);
		new TargetActualHandler().getMonthAggregrateName(4);
		new TargetActualHandler().getMonthAggregrateName(5);
		new TargetActualHandler().getMonthAggregrateName(6);
		new TargetActualHandler().getMonthAggregrateName(7);
		new TargetActualHandler().getMonthAggregrateName(8);
		new TargetActualHandler().getMonthAggregrateName(9);
		new TargetActualHandler().getMonthAggregrateName(10);
		new TargetActualHandler().getMonthAggregrateName(10);
		new TargetActualHandler().getMonthAggregrateName(11);
		new TargetActualHandler().getMonthAggregrateName(12);
		new TargetActualHandler().getMonthAggregrateName(13);
	}

	@isTest
	public static void coverTargetAndActualHandlerWithError(){
		List<Target_and_Actual__c> tvavLst = [SELECT id,Points_Eligible__c,Target__c,Actual__c,Target_Type__c FROM Target_and_Actual__c ];

		for(Target_and_Actual__c tvav :tvavLst){
			tvav.Points_Eligible__c = null;
			tvav.Target__c =0 ;
				
		}

		update tvavLst;

		TargetActualBatch tv = new TargetActualBatch(Date.newInstance(2017,8, 20));
		Database.executeBatch(tv);
	}
}
@isTest
public class TestOpportunityUserFieldsHandler {    
    
    @testSetup 
    public static void createTestData(){
        //TestDataFactory.createTwoTestUsers();
        TestDataFactory.createCustomSetting();
         /*User u1 = TestDataFactory.createTestUser('Compro','Tester1');
        insert u1;
        User u2 = TestDataFactory.createTestUser('Compro','Tester2');
		insert u2;*/        
        TestDataFactory.createTestProductData();
        TestDataFactory.createTestStateAndCityRecordData();
        TestDataFactory.createTestCatalogueBudgetData();
        TestDataFactory.createTestCampaignRecordData();
        TestDataFactory.createTestAccountData();
        TestDataFactory.createTestProjectData();
        TestDataFactory.createTestOpportunityData();
        TestDataFactory.createTestOpportunityProductData();
        TestDataFactory.createTestPriceRequestData();
        TestDataFactory.createTestProductForecateData();
        TestDataFactory.createTestForecateInvoiceData();
        TestDataFactory.createTestMockupRecordData();
        TestDataFactory.createTestPlantVisitRecordData();
        TestDataFactory.createTestInvoiceRecordData();
        TestDataFactory.createTestTargetActualRecordData();
    }
    
    @isTest
    public static void createOpportunity(){

        Test.startTest();
        List<Project__c> lstOfproj = [SELECT id FROM Project__c WHERE Validated__c= true limit 2];
        Account accCRM = [SELECT id,RecordType.Name from Account WHERE RecordType.Name= 'CRM' LIMIT 1];        
        List<User> userList = [select id from User where UserRole.Name LIKE '%DEL1%' OR UserRole.Name LIKE '%MUM1%' LIMIT 2];
        List<User> glassFutureUserList = [select id from User where Profile.Name LIKE '%Glass Future%'LIMIT 1];
        List<User> adminuserList = [select id from User where Profile.Name LIKE '%System Administrator%' LIMIT 1];
        List<Opportunity> oppty = [Select id from opportunity where Type = 'Infinity' and (owner.UserRole.Name LIKE '%DEL1%' OR owner.UserRole.Name LIKE '%MUM1%') ] ;
        List<Product2> lstOfProd = [SELECT id FROM Product2 WHERE ((NOT Family Like 'Catalogue%') and (NOT Product_Range__c Like 'Glass Future'))];
        //System.debug('User list :'+userList);

        Opportunity_Product__c opptyProd = new Opportunity_Product__c();
        opptyProd.Product__c = lstOfProd[0].id;
        opptyProd.Product_Type__c = 'DGU';
        opptyProd.Status__c = 'Selected';
        opptyProd.Opportunity__c = oppty[0].id;
        
        insert opptyProd;
        
        Mock_Up_Request__c mock = new Mock_Up_Request__c();
        mock.Opportunity__c = oppty[0].Id;
        mock.Approval_Status__c = 'Draft';
        
        insert mock;         
        
        Price_Request__c pr = new Price_Request__c();
        pr.Price_Type__c = 'Special Price';
        pr.Requested_Quantity__c = 500;
        pr.Required_Price__c = 400;
        pr.Opportunity_Product__c = opptyProd.Id;
        pr.Approval_Status__c = 'Draft';
        pr.Valid_From__c =  Date.newInstance(2017,8, 27);
        pr.Valid_To__c = Date.newInstance(2017,8, 31);
        
        insert pr;   

        oppty[0].OwnerId = userList[1].Id;
        update oppty[0];
        
        oppty[0].OwnerId = glassFutureUserList[0].Id;
        update oppty[0];
        
        oppty[0].OwnerId = adminuserList[0].Id;
        update oppty[0];
        Test.stopTest();
    }
    
}
/* Created By: Rajiv Kumar Singh
Created Date: 07/06/2017
Class Name: customerStatusUpdateAPITest
Description : Test class for customerStatusUpdateAPI
Last Modified By :Rajiv Kumar Singh
Last Modified Date: 07/06/2017
*/

@IsTest
private class customerStatusUpdateAPITest{
    
    static testMethod void testPostMethod(){
        String JSONMsg = '[{"SAPcode":"3321","Status":"True","BlockStatus":"True"},{"SAPcode":"1231234","Status":"False","BlockStatus":"False"}]';
        RestRequest request = new RestRequest();
        request.requestUri ='/services/apexrest/customerStatus';
        request.httpMethod = 'POST';
        request.requestBody = Blob.valueof(JSONMsg);
        RestContext.request = request;
        RestContext.response = new RestResponse();
        customerStatusUpdateAPI.doHandleInsertRequest();
        
        JSONMsg = '[1';
        request = new RestRequest();
        request.requestUri ='/services/apexrest/customerStatus';
        request.httpMethod = 'POST';
        request.requestBody = Blob.valueof(JSONMsg);
        RestContext.request = request;
        RestContext.response = new RestResponse();
        customerStatusUpdateAPI.doHandleInsertRequest();
        
        request = new RestRequest();
        request.requestUri ='/services/apexrest/customerStatus';
        request.httpMethod = 'POST';
        request.requestBody = Blob.valueof('');
        RestContext.request = request;
        RestContext.response = new RestResponse();
        customerStatusUpdateAPI.doHandleInsertRequest();
    }
}
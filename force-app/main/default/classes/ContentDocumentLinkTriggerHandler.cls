public class ContentDocumentLinkTriggerHandler extends TriggerHandler{
	
    public ContentDocumentLinkTriggerHandler() {}
	
    public override void afterInsert() {

    	FileOperationHandler.increaseImageCount( (List<ContentDocumentLink>)Trigger.new,
    												new Map<String,Boolean>{'JPG'=>true,'PNG'=>true,'JPEG'=>true},
    												new Map<String,Boolean>{'Project__c'=>true}
    												);
    }

	/*
	public override void beforeDelete() {
    	FileOperationHandler.restrictImageDeletion( (List<ContentDocumentLink>)Trigger.Old,
    												new Map<String,Boolean>{'JPG'=>true,'PNG'=>true,'JPEG'=>true},
    												new Map<String,Boolean>{'Project__c'=>true}
    												);
    }*/
}
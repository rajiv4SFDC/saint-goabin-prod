/* Created By: Rajiv Kumar Singh
Created Date: 04/06/2017
Class Name: CreateEnquiry_InsertAPITest
Description : Test class for CreateEnquiry_InsertAPI
Last Modified By :Rajiv Kumar Singh
Last Modified Date: 04/06/2017
*/

@IsTest
private class CreateEnquiry_InsertAPITest{
    
    static testMethod void testPostMethod(){
        String JSONMsg = '[{"ReferenceId":"OR0001","AccSAPCode":"0000512995","ShipToSAPCode":"0000512994","Salesoffice":"CAL-1","EnqSource":"SAP","ExpDate":"1991/07/21","EnqStatus":"Packed","childInfo":[{"ReferenceId":"OR0001","SubFamilyCode":"404","CoatingCode":"002","TintCode":"000","LaminateCode":"000","PatternCode":"000","ThicknessCode":"0600","Length":"12","Width":"2323","Packtype":"0.6","cases":"2","EnqLineSource":"SAP","EnqLineUnique":"321"}]}]';
        RestRequest request = new RestRequest();
        request.requestUri ='/services/apexrest/createEnquiry';
        request.httpMethod = 'POST';
        request.requestBody = Blob.valueof(JSONMsg);
        RestContext.request = request;
        RestContext.response = new RestResponse();
        CreateEnquiry_InsertAPI.doHandleInsertRequest();
        
        JSONMsg = '[1';
        request = new RestRequest();
        request.requestUri ='/services/apexrest/createEnquiry';
        request.httpMethod = 'POST';
        request.requestBody = Blob.valueof(JSONMsg);
        RestContext.request = request;
        RestContext.response = new RestResponse();
        CreateEnquiry_InsertAPI.doHandleInsertRequest();
        
        request = new RestRequest();
        request.requestUri ='/services/apexrest/createEnquiry';
        request.httpMethod = 'POST';
        request.requestBody = Blob.valueof('');
        RestContext.request = request;
        RestContext.response = new RestResponse();
        CreateEnquiry_InsertAPI.doHandleInsertRequest();
    }
}
/* Created By: Rajiv Kumar Singh
Created Date: 05/06/2017
Class Name: ProductDimensionInsertAPITest
Description : Test class for ProductDimensionInsertAPI
Last Modified By :Rajiv Kumar Singh
Last Modified Date: 05/06/2017
*/

@IsTest
private class ProductDimensionInsertAPITest{
    
    static testMethod void testPostMethod(){
        String JSONMsg = '[{"ProductItemCode":"405-049-152-000-000","Length":"110","Width":"102","thick":"0008","DimensionKey":"123412"},{"ProductItemCode":"405-049-152-000-000","Length":"210","Width":"112","thick":"0008","DimensionKey":"1412"}]';
        RestRequest request = new RestRequest();
        request.requestUri ='/services/apexrest/productDimension';
        request.httpMethod = 'POST';
        request.requestBody = Blob.valueof(JSONMsg);
        RestContext.request = request;
        RestContext.response = new RestResponse();
        ProductDimensionInsertAPI.doHandleInsertRequest();
        
        JSONMsg = '[1';
        request = new RestRequest();
        request.requestUri ='/services/apexrest/productDimension';
        request.httpMethod = 'POST';
        request.requestBody = Blob.valueof(JSONMsg);
        RestContext.request = request;
        RestContext.response = new RestResponse();
        ProductDimensionInsertAPI.doHandleInsertRequest();
        
        request = new RestRequest();
        request.requestUri ='/services/apexrest/productDimension';
        request.httpMethod = 'POST';
        request.requestBody = Blob.valueof('');
        RestContext.request = request;
        RestContext.response = new RestResponse();
        ProductDimensionInsertAPI.doHandleInsertRequest();
    }
}
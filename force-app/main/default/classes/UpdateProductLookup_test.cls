@isTest
private class UpdateProductLookup_test
{ 
 // @istest
   private testmethod static void testMethod1()
   {
      product2 p1 = new product2();
      p1.Name = 'testing';
      p1.productcode ='abcd';
      p1.Description = 'testing only';
      p1.Product_Category__c = 'Inspire';
      p1.Sub_Family__c = 'DIAMANT';
      p1.Coating__c = 'NONE';
      p1.Tint__c ='DIAMANT';
      p1.CustCondGrp3__c = 'T1';
      p1.External_Id__c = 'abcd-123';
      insert p1;
      
      Product_Tier_Pricing__c ptp = new Product_Tier_Pricing__c();
      ptp.name='testing ony';
      ptp.Item_Code__c ='abcd-123';
      ptp.product__c = p1.id;
      insert ptp;
      
      Product_Tier_Pricing__c  pt = [select id,name,Product_Code__c,product__c,Item_Code__c from Product_Tier_Pricing__c  where id =:ptp.id];
      
    //  System.assertEquals(pt.product__c,p1.id);
      list<Id> ptrList = new list<Id>();
      ptrList.add(pt.id);
      
      UpdateProductLookup.UpdateLookupValue(ptrList);
   }
}
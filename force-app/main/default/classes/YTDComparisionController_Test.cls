@isTest
Public class YTDComparisionController_Test{
    @isTest
    static void testData(){
        Account testAccount = new Account();
        testAccount.Name='Test Account' ;
        testAccount.Sales_Area_Region__c = 'North';
        insert testAccount;
        
        Invoice__c inv2 = new Invoice__c();
        inv2.Name = 'Test321';
        inv2.Customer__c = testAccount.Id;
        inv2.Item_Tonnage__c = '';
        inv2.Invoice_Value__c = 0;
        insert inv2;
        
        Invoice__c inv = new Invoice__c();
        inv.Name = 'Test123';
        inv.Customer__c = testAccount.Id;
        inv.Item_Tonnage__c = '100';
        inv.Invoice_Value__c = 123;
        insert inv;
        
        Invoice__c inv1 = new Invoice__c();
        inv1.Name = 'Test321';
        inv1.Customer__c = testAccount.Id;
        inv1.Item_Tonnage__c = '100';
        inv1.Invoice_Value__c = 123;
        insert inv1;
        
        YTDComparisionController ytd = new YTDComparisionController();
        ytd.selectedYear = '2019';
        ytd.selectedMonth = 'Nov';
        ytd.selectedRegion = 'North';
        ytd.sumOfInvoice();
        ytd.callTest();           
    }
}
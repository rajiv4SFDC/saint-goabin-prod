@isTest
Public class FetchRegionTest{
    Static testmethod void Regionnods() {
        Region__c rc = new Region__c();
        rc.Name = 'South';
        insert rc;
        
        String Region='West';
        Date today = System.today();
        DateTime EndDate = System.today() + 3;
        Region_Wise_NOD_s__c reg = new Region_Wise_NOD_s__c();
        reg.Average_Outstanding__c = 55;
        reg.No_of_Days__c = 3;
        reg.Region__c = rc.Id;
        reg.Posting_Date__c = today;
        insert reg;
         Map<String,Decimal> mapOfMonth = new Map<String,Decimal>{'Jan' => 1, 'Feb' => 2, 'Mar' => 3, 'Apr' => 4, 'May' => 5, 
                                                                 'Jun' => 6, 'Jul' => 7, 'Aug' => 8, 'Sep' => 9, 'Oct' => 10, 
            													 'Nov' => 11, 'Dec' => 12};
 
          Map<Decimal,String> mapOfMonth2 = new Map<Decimal,String>{  1 => 'Jan',  2 =>'Feb', 3=> 'Mar' , 4 => 'Apr',5 => 'May' , 
                                                                  6 => 'Jun', 7 => 'Jul', 8=> 'Aug' , 9 => 'Sep',  10 => 'Oct', 
            													  11 => 'Nov', 12 => 'Dec'};                                                                     
                                                                     
        Test.startTest();
        //string month =mapOfMonth2(Date.today().Month());
        FetchRegion.fetchRecord('Oct', '2018','South');
        FetchRegion.selectedYear();
        FetchRegion.selectedMonth();
        Test.stopTest();        
        
    }
}
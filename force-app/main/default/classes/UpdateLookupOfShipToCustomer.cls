/* Created By         : Rajiv Kumar Singh
   Created Date       : 11/06/2018
   Class Name         : UpdateLookupOfShipToCustomer
   Description        : update the particular Account record lookup field to ShipToCustomer
   Last Modified By   : Rajiv Kumar Singh
   Last Modified Date : 11/06/2018
*/
public class UpdateLookupOfShipToCustomer{
    
  @InvocableMethod(label='UpdateLookupOfShipToCustomer' description='update the particular Account record lookup field to ShipToCustomer')
  public static void UpdateLookupValue(List<Id> shipToCustomerIds)
    {   
       map<string,string> shipToCustomerWithAccountMap = new map<string,string>();  
       set<string> presentSAPCodesSet = new set<String>();
       list<Ship_To_Customer__c > NewList = new list<Ship_To_Customer__c>(); 
       list<Ship_To_Customer__c > FinalListToUpdate = new list<Ship_To_Customer__c>();

       for(Ship_To_Customer__c ptp:[select id,Account__c,Bill_To_Customer_Code__c from Ship_To_Customer__c where id in:shipToCustomerIds])
       {
         if(ptp.Bill_To_Customer_Code__c != null)
          {
            presentSAPCodesSet.add(ptp.Bill_To_Customer_Code__c);      
            NewList.add(ptp);
            system.debug('The ptp.id is: ' +ptp.id);
          }     
       }
       system.debug('The NewList is: ' +NewList+'......'+presentSAPCodesSet);
       if(presentSAPCodesSet.size()>0)
       {
          for(Account acc:[select id,SAP_Code__c  from Account where SAP_Code__c in:presentSAPCodesSet])
           {
               shipToCustomerWithAccountMap.put(acc.SAP_Code__c,acc.id);
           }
           system.debug('The shipToCustomerWithAccountMap is: ' +shipToCustomerWithAccountMap);
           for(Ship_To_Customer__c ptp:NewList)
           {
               ptp.Account__c = shipToCustomerWithAccountMap.get(ptp.Bill_To_Customer_Code__c);
               system.debug('The  ptp.Account__c is: ' + ptp.Account__c);
               FinalListToUpdate.add(ptp);
           }
       }
       if(!FinalListToUpdate.isEmpty() && FinalListToUpdate.size() > 0) {
            update FinalListToUpdate;
       }
    }
}
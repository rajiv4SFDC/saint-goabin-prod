/* Created By         : Rajiv Kumar Singh
   Created Date       : 29/06/2018
   Class Name         : UpdatesalesOrderLookupSalesOrderStatus
   Description        : update the particular Sales order record lookup field to sales order status
   Last Modified By   : Rajiv Kumar Singh
   Last Modified Date : 29/06/2018
*/
public class UpdatesalesOrderLookupSalesOrderStatus{
  @InvocableMethod(label='UpdatesalesOrderLookupSalesOrderStatus' description='update the particular Sales order record lookup field to sales order status')
  public static void UpdateLookupValue(List<Id> salesOrderstatusIds)
    {   
       map<string,string> NODwithSalesOfficeMap = new map<string,string>();  
       set<string> presentSalesOfficeSet = new set<String>();
       list<Sales_Order_Status__c> NewList = new list<Sales_Order_Status__c>(); 
       list<Sales_Order_Status__c> FinalListToUpdate = new list<Sales_Order_Status__c>();

       for(Sales_Order_Status__c ptp:[select id,Sales_Order_Num__c from Sales_Order_Status__c where id in : salesOrderstatusIds])
       {
         if(ptp.Sales_Order_Num__c != null)
          {
            presentSalesOfficeSet.add(ptp.Sales_Order_Num__c);      
            NewList.add(ptp);
            system.debug('The ptp.id is: ' +ptp.id);
          }     
       }
       system.debug('The NewList is: ' +NewList+'......'+presentSalesOfficeSet);
       if(presentSalesOfficeSet.size()>0)
       {
          for(Sales_Order_Line_Item__c acc:[select id,Sales_Order_Line_Item_Number__c from Sales_Order_Line_Item__c where Sales_Order_Line_Item_Number__c in:presentSalesOfficeSet])
           {
               NODwithSalesOfficeMap.put(acc.Sales_Order_Line_Item_Number__c,acc.id);
           }
           system.debug('The NODwithSalesOfficeMap is: ' +NODwithSalesOfficeMap);
           for(Sales_Order_Status__c ptp:NewList)
           {
               ptp.Sales_Order_Line_Item__c = NODwithSalesOfficeMap.get(ptp.Sales_Order_Num__c);
               system.debug('The  ptp.Sales_Order_Num__c is: ' + ptp.Sales_Order_Num__c);
               FinalListToUpdate.add(ptp);
           }
       }
       if(!FinalListToUpdate.isEmpty() && FinalListToUpdate.size() > 0) {
            update FinalListToUpdate;
       }
    }
}
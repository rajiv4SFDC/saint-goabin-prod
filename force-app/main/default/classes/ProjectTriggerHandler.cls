public class ProjectTriggerHandler{
    
    public static boolean projectRecurrssionControl = false;
    
    public static void beforeUpdateHandler(list<project__c> newList,map<id,project__c> oldMap)
    {
        if(projectRecurrssionControl == false)
        {
            ProjectTriggerHandler.projectRecurrssionControl = true;
            updateRelatedManualAccountingRecs(newList,oldMap);  
            
        }
        
    }
    
    // This method is used to update the related manual accounting records filed such as "Is project validated".
    public static void updateRelatedManualAccountingRecs(list<project__c> newList,map<id,project__c> oldMap)
    {
        map<id,boolean> prjWithValidateMap = new map<id,boolean>();
        set<id> prjIdsSet = new set<id>();
        for(project__c p1:newList)
        {
            if(oldMap.get(p1.id).Validated__c != p1.Validated__c)
                prjIdsSet.add(p1.id);
            prjWithValidateMap.put(p1.id,p1.Validated__c);
        }
        
        system.debug('The prjWithValidateMap is:'+prjWithValidateMap);
        
        list<manual_accounting__c> maRecsToUpdate = new list<manual_accounting__c>();
        
        // now query the manual accouting records here. 
        for(manual_accounting__c ma:[select id,name,Is_project_validated__c, Opportunity_Product_Customer__r.Opportunity_Product__r.Opportunity__r.project__c from 
                                     manual_accounting__c where Opportunity_Product_Customer__r.Opportunity_Product__r.Opportunity__r.project__c in :prjWithValidateMap.keyset() 
                                     AND Is_project_validated__c=false ])
        {
            system.debug('The coming val is:'+ma.name+'.....'+ma.Opportunity_Product_Customer__r.Opportunity_Product__r.Opportunity__r.project__c);
            if(prjWithValidateMap.containsKey(ma.Opportunity_Product_Customer__r.Opportunity_Product__r.Opportunity__r.project__c))
            {
                ma.Is_project_validated__c = prjWithValidateMap.get(ma.Opportunity_Product_Customer__r.Opportunity_Product__r.Opportunity__r.project__c);
                maRecsToUpdate.add(ma);                  
            } 
        }
        
        if(maRecsToUpdate.size()>0)
            update maRecsToUpdate;
        
        
    }
    
}
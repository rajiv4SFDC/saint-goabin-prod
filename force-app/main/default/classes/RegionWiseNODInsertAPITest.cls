/* Created By: Rajiv Kumar Singh
Created Date: 27/06/2018
Class Name: RegionWiseNODInsertAPITest
Description : Test class for RegionWiseNODInsertAPI
Last Modified By :Rajiv Kumar Singh
Last Modified Date: 27/06/2017
*/

@IsTest
private class RegionWiseNODInsertAPITest{
    
    static testMethod void testPostMethod(){
        String JSONMsg = '[{"RegionName":"test1","RegionNODs":"10","AverageOutstanding":"12.11","postDate":"2018-01-1","Amt60":"32"},{"RegionName":"test2","RegionNODs":"11","AverageOutstanding":"12.11","postDate":"2018-01-1","Amt60":"32"}]';
        RestRequest request = new RestRequest();
        request.requestUri ='/services/apexrest/RegionWiseNOD';
        request.httpMethod = 'POST';
        request.requestBody = Blob.valueof(JSONMsg);
        RestContext.request = request;
        RestResponse res = new RestResponse();
        RestContext.response = new RestResponse();
        RegionWiseNODInsertAPI.doHandleInsertRequest();
        
        JSONMsg = '[1';
        request = new RestRequest();
        request.requestUri ='/services/apexrest/RegionWiseNOD';
        request.httpMethod = 'POST';
        request.requestBody = Blob.valueof(JSONMsg);
        RestContext.request = request;
        RestContext.response = new RestResponse();
        RegionWiseNODInsertAPI.doHandleInsertRequest();
        
        request = new RestRequest();
        request.requestUri ='/services/apexrest/RegionWiseNOD';
        request.httpMethod = 'POST';
        request.requestBody = Blob.valueof('');
        RestContext.request = request;
        RestContext.response = new RestResponse();
        RegionWiseNODInsertAPI.doHandleInsertRequest();
    }
}
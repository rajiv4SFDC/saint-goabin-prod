@isTest
public class TestDataFactoryClass {
    public static List<Sales_Office_Wise_NOD_s__c> create_office(){
          Region__c region = new Region__c(name='North');
        insert region;
        
        Sales_Office__c office = new Sales_Office__c(Name='DEL1',Region__c=region.id);
        insert office;
        
        date currentdate = date.today();
        List<Sales_Office_Wise_NOD_s__c> officeList = new List<Sales_Office_Wise_NOD_s__c>();
        for(integer i=0; i<=5; i++){
            Sales_Office_Wise_NOD_s__c nd= new Sales_Office_Wise_NOD_s__c();    
            nd.Amt_60_Days__c='12';
            nd.Average_Outstanding__c=10;
            nd.No_of_Days__c=19;
            nd.Sales_Office__c=office.id;
            nd.Posting_Date__c=date.today();
            officeList.add(nd);
        }
        
         Sales_Office_Wise_NOD_s__c nd= new Sales_Office_Wise_NOD_s__c();    
            nd.Amt_60_Days__c='11';
            nd.Average_Outstanding__c=10;
            nd.No_of_Days__c=19;
            nd.Sales_Office__c=office.id;
            nd.Posting_Date__c=Date.newInstance(currentdate.year(), currentdate.month()-1 ,currentdate.day());
            officeList.add(nd);
        
         Sales_Office_Wise_NOD_s__c nd1= new Sales_Office_Wise_NOD_s__c();    
            nd1.Amt_60_Days__c='10';
            nd1.Average_Outstanding__c=10;
            nd1.No_of_Days__c=19;
            nd1.Sales_Office__c=office.id;
            nd1.Posting_Date__c=Date.newInstance(currentdate.year(), currentdate.month()-2 ,currentdate.day());
            officeList.add(nd1);
        
         Sales_Office_Wise_NOD_s__c nd2= new Sales_Office_Wise_NOD_s__c();    
            nd2.Amt_60_Days__c='9';
            nd2.Average_Outstanding__c=10;
            nd2.No_of_Days__c=19;
            nd2.Sales_Office__c=office.id;
            nd2.Posting_Date__c=Date.newInstance(currentdate.year(), currentdate.month()-3 ,currentdate.day());
            officeList.add(nd2);
        
		 Sales_Office_Wise_NOD_s__c nd3= new Sales_Office_Wise_NOD_s__c();    
            nd3.Amt_60_Days__c='8';
            nd3.Average_Outstanding__c=10;
            nd3.No_of_Days__c=19;
            nd3.Sales_Office__c=office.id;
            nd3.Posting_Date__c=Date.newInstance(currentdate.year(), currentdate.month()-4 ,currentdate.day());
            officeList.add(nd3);
        
         Sales_Office_Wise_NOD_s__c nd4= new Sales_Office_Wise_NOD_s__c();    
            nd4.Amt_60_Days__c='7';
            nd4.Average_Outstanding__c=10;
            nd4.No_of_Days__c=19;
            nd4.Sales_Office__c=office.id;
            nd4.Posting_Date__c=Date.newInstance(currentdate.year(), currentdate.month()-5 ,currentdate.day());
            officeList.add(nd4);
        
         Sales_Office_Wise_NOD_s__c nd5= new Sales_Office_Wise_NOD_s__c();    
            nd5.Amt_60_Days__c='6';
            nd5.Average_Outstanding__c=10;
            nd5.No_of_Days__c=19;
            nd5.Sales_Office__c=office.id;
            nd5.Posting_Date__c=Date.newInstance(currentdate.year(), currentdate.month()-6 ,currentdate.day());
            officeList.add(nd5);
        
         Sales_Office_Wise_NOD_s__c nd6= new Sales_Office_Wise_NOD_s__c();    
            nd6.Amt_60_Days__c='7';
            nd6.Average_Outstanding__c=10;
            nd6.No_of_Days__c=19;
            nd6.Sales_Office__c=office.id;
            nd6.Posting_Date__c=Date.newInstance(currentdate.year(), currentdate.month()-7 ,currentdate.day());
            officeList.add(nd6);
        
         Sales_Office_Wise_NOD_s__c nd7= new Sales_Office_Wise_NOD_s__c();    
            nd7.Amt_60_Days__c='8';
            nd7.Average_Outstanding__c=10;
            nd7.No_of_Days__c=19;
            nd7.Sales_Office__c=office.id;
            nd7.Posting_Date__c=Date.newInstance(currentdate.year(), currentdate.month()-8 ,currentdate.day());
            officeList.add(nd7);
        
         Sales_Office_Wise_NOD_s__c nd8= new Sales_Office_Wise_NOD_s__c();    
            nd8.Amt_60_Days__c='9';
            nd8.Average_Outstanding__c=10;
            nd8.No_of_Days__c=19;
            nd8.Sales_Office__c=office.id;
            nd8.Posting_Date__c=Date.newInstance(currentdate.year(), currentdate.month()-9 ,currentdate.day());
            officeList.add(nd8);
        
		 Sales_Office_Wise_NOD_s__c nd9= new Sales_Office_Wise_NOD_s__c();    
            nd9.Amt_60_Days__c='10';
            nd9.Average_Outstanding__c=10;
            nd9.No_of_Days__c=19;
            nd9.Sales_Office__c=office.id;
            nd9.Posting_Date__c=Date.newInstance(currentdate.year(), currentdate.month()-10 ,currentdate.day());
            officeList.add(nd9);
        
         Sales_Office_Wise_NOD_s__c nd99= new Sales_Office_Wise_NOD_s__c();    
            nd99.Amt_60_Days__c='11';
            nd99.Average_Outstanding__c=10;
            nd99.No_of_Days__c=19;
            nd99.Sales_Office__c=office.id;
            nd99.Posting_Date__c=Date.newInstance(currentdate.year(), currentdate.month()-11 ,currentdate.day());
            officeList.add(nd99);
        
        if(officeList.size()>0){
        	insert officeList;    
        }
        return officeList;
    }
    
    public static List<Region_Wise_NOD_s__c> create_region(){
        Region__c region = new Region__c(name='North');
        insert region;
        
        Sales_Office__c office = new Sales_Office__c(Name='DEL1',Region__c=region.id);
        insert office;
        
        date currentdate = date.today();
        
        List<Region_Wise_NOD_s__c> regionList = new List<Region_Wise_NOD_s__c>();
        
        for(integer i=0; i<=5; i++){
            Region_Wise_NOD_s__c reg = new Region_Wise_NOD_s__c();
            reg.Amt_60_Days__c='12';
            reg.Average_Outstanding__c=12;
            reg.No_of_Days__c=12;
            reg.Region__c=region.id;
            reg.Posting_Date__c=currentdate;
            regionList.add(reg);
        }
        
        Region_Wise_NOD_s__c reg1 = new Region_Wise_NOD_s__c();
            reg1.Amt_60_Days__c='11';
            reg1.Average_Outstanding__c=11;
            reg1.No_of_Days__c=11;
            reg1.Region__c=region.id;
            reg1.Posting_Date__c=date.newInstance(currentdate.year(), currentdate.month()-1, currentdate.day());
            regionList.add(reg1);
        
         Region_Wise_NOD_s__c reg2 = new Region_Wise_NOD_s__c();
            reg2.Amt_60_Days__c='10';
            reg2.Average_Outstanding__c=10;
            reg2.No_of_Days__c=10;
            reg2.Region__c=region.id;
            reg2.Posting_Date__c=date.newInstance(currentdate.year(), currentdate.month()-2, currentdate.day());
            regionList.add(reg2);
        
         Region_Wise_NOD_s__c reg3 = new Region_Wise_NOD_s__c();
            reg3.Amt_60_Days__c='9';
            reg3.Average_Outstanding__c=9;
            reg3.No_of_Days__c=9;
            reg3.Region__c=region.id;
            reg3.Posting_Date__c=date.newInstance(currentdate.year(), currentdate.month()-3, currentdate.day());
            regionList.add(reg3);
		        
         Region_Wise_NOD_s__c reg4 = new Region_Wise_NOD_s__c();
            reg4.Amt_60_Days__c='8';
            reg4.Average_Outstanding__c=8;
            reg4.No_of_Days__c=8;
            reg4.Region__c=region.id;
            reg4.Posting_Date__c=date.newInstance(currentdate.year(), currentdate.month()-4, currentdate.day());
            regionList.add(reg4);
        
         Region_Wise_NOD_s__c reg5 = new Region_Wise_NOD_s__c();
            reg5.Amt_60_Days__c='9';
            reg5.Average_Outstanding__c=9;
            reg5.No_of_Days__c=9;
            reg5.Region__c=region.id;
            reg5.Posting_Date__c=date.newInstance(currentdate.year(), currentdate.month()-5, currentdate.day());
            regionList.add(reg5);
        
         Region_Wise_NOD_s__c reg6 = new Region_Wise_NOD_s__c();
            reg6.Amt_60_Days__c='10';
            reg6.Average_Outstanding__c=10;
            reg6.No_of_Days__c=10;
            reg6.Region__c=region.id;
            reg6.Posting_Date__c=date.newInstance(currentdate.year(), currentdate.month()-6, currentdate.day());
            regionList.add(reg6);
        
         Region_Wise_NOD_s__c reg7 = new Region_Wise_NOD_s__c();
            reg7.Amt_60_Days__c='11';
            reg7.Average_Outstanding__c=11;
            reg7.No_of_Days__c=11;
            reg7.Region__c=region.id;
            reg7.Posting_Date__c=date.newInstance(currentdate.year(), currentdate.month()-7, currentdate.day());
            regionList.add(reg7);
        
         Region_Wise_NOD_s__c reg8 = new Region_Wise_NOD_s__c();
            reg8.Amt_60_Days__c='11';
            reg8.Average_Outstanding__c=11;
            reg8.No_of_Days__c=11;
            reg8.Region__c=region.id;
            reg8.Posting_Date__c=date.newInstance(currentdate.year(), currentdate.month()-8, currentdate.day());
            regionList.add(reg8);
        
         Region_Wise_NOD_s__c reg9 = new Region_Wise_NOD_s__c();
            reg9.Amt_60_Days__c='11';
            reg9.Average_Outstanding__c=11;
            reg9.No_of_Days__c=11;
            reg9.Region__c=region.id;
            reg9.Posting_Date__c=date.newInstance(currentdate.year(), currentdate.month()-9, currentdate.day());
            regionList.add(reg9);
        
         Region_Wise_NOD_s__c reg10 = new Region_Wise_NOD_s__c();
            reg10.Amt_60_Days__c='11';
            reg10.Average_Outstanding__c=11;
            reg10.No_of_Days__c=11;
            reg10.Region__c=region.id;
            reg10.Posting_Date__c=date.newInstance(currentdate.year(), currentdate.month()-10, currentdate.day());
            regionList.add(reg10);
        
         Region_Wise_NOD_s__c reg11 = new Region_Wise_NOD_s__c();
            reg11.Amt_60_Days__c='11';
            reg11.Average_Outstanding__c=11;
            reg11.No_of_Days__c=11;
            reg11.Region__c=region.id;
            reg11.Posting_Date__c=date.newInstance(currentdate.year(), currentdate.month()-11, currentdate.day());
            regionList.add(reg11);
        
        if(regionList.size()>0){
        	insert regionList;    
        }
        return regionList;
    }
    
    public static List<NoDs__c> create_Customer_NoDs(){
       date currentdate = date.today();
        List<NoDs__c> nodlist = new List<NoDs__c>();
        Id RecordTypeIdContact = Schema.SObjectType.account.getRecordTypeInfosByName().get('ERP').getRecordTypeId();

        Account ac = new account();
        ac.RecordTypeId=RecordTypeIdContact;
        ac.name='test account 1';
        ac.Sales_Area_Region__c='North';
        ac.Sales_Office_ERP__c='DEL1';
        ac.SAP_Code__c='11';
       insert ac;
        
        for(integer i=0; i<=5; i++){
            NoDs__c reg = new NoDs__c();
            reg.Account__c=ac.id;
            reg.Amt_60_Days__c='12';
            reg.Average_Outstanding__c=12;
            reg.Account_Wise_NOD__c='11';
            reg.Sales_Area_Wise_NOD__c='123';
            reg.SAP_Code__c='11';
            reg.Posting_Date__c=currentdate;
            nodList.add(reg);
        }
        
        NoDs__c reg1 = new NoDs__c();
        	reg1.Account__c=ac.id;
            reg1.Amt_60_Days__c='11';
            reg1.Average_Outstanding__c=11;
            reg1.Account_Wise_NOD__c='11';
            reg1.Sales_Area_Wise_NOD__c='123';
            reg1.SAP_Code__c='11';
            reg1.Posting_Date__c=date.newInstance(currentdate.year(), currentdate.month()+1, currentdate.day()+1);
            nodlist.add(reg1);
        
        NoDs__c reg2 = new NoDs__c();
            reg2.Account__c=ac.id;
            reg2.Amt_60_Days__c='10';
            reg2.Average_Outstanding__c=10;
            reg2.Account_Wise_NOD__c='11';
            reg2.Sales_Area_Wise_NOD__c='123';
            reg2.SAP_Code__c='11';
            reg2.Posting_Date__c=date.newInstance(currentdate.year(), currentdate.month()+2, currentdate.day()+1);
            nodlist.add(reg2);
        
        NoDs__c reg3 = new NoDs__c();
            reg3.Account__c=ac.id;
            reg3.Amt_60_Days__c='9';
            reg3.Average_Outstanding__c=9;
            reg3.Account_Wise_NOD__c='11';
            reg3.Sales_Area_Wise_NOD__c='123';
            reg3.SAP_Code__c='11';
            reg3.Posting_Date__c=date.newInstance(currentdate.year(), currentdate.month()+3, currentdate.day()+1);
            nodlist.add(reg3);
		        
         NoDs__c reg4 = new NoDs__c();
        	reg4.Account__c=ac.id;
            reg4.Amt_60_Days__c='8';
            reg4.Average_Outstanding__c=8;
            reg4.Account_Wise_NOD__c='11';
            reg4.Sales_Area_Wise_NOD__c='123';
            reg4.SAP_Code__c='11';
            reg4.Posting_Date__c=date.newInstance(currentdate.year(), currentdate.month()+4, currentdate.day()+1);
            nodList.add(reg4);
        
         NoDs__c reg5 = new NoDs__c();
        	reg5.Account__c=ac.id;
            reg5.Amt_60_Days__c='9';
            reg5.Average_Outstanding__c=9;
        	reg5.Account_Wise_NOD__c='11';
            reg5.Sales_Area_Wise_NOD__c='123';
            reg5.SAP_Code__c='11';
            reg5.Posting_Date__c=date.newInstance(currentdate.year(), currentdate.month()+5, currentdate.day()+1);
            nodList.add(reg5);
        
         NoDs__c reg6 = new NoDs__c();
        	reg6.Account__c=ac.id;
            reg6.Amt_60_Days__c='10';
            reg6.Average_Outstanding__c=10;
            reg6.Account_Wise_NOD__c='11';
            reg6.Sales_Area_Wise_NOD__c='123';
            reg6.SAP_Code__c='11';
            reg6.Posting_Date__c=date.newInstance(currentdate.year(), currentdate.month()+6, currentdate.day()+1);
            nodList.add(reg6);
        
         NoDs__c reg7 = new NoDs__c();
        	reg7.Account__c=ac.id;
            reg7.Amt_60_Days__c='11';
            reg7.Average_Outstanding__c=11;
            reg7.Account_Wise_NOD__c='11';
            reg7.Sales_Area_Wise_NOD__c='123';
            reg7.SAP_Code__c='11';
            reg7.Posting_Date__c=date.newInstance(currentdate.year(), currentdate.month()+7, currentdate.day()+1);
            nodList.add(reg7);
        
         NoDs__c reg8 = new NoDs__c();
        	reg8.Account__c=ac.id;
            reg8.Amt_60_Days__c='11';
            reg8.Average_Outstanding__c=11;
            reg8.Account_Wise_NOD__c='11';
            reg8.Sales_Area_Wise_NOD__c='123';
            reg8.SAP_Code__c='11';
            reg8.Posting_Date__c=date.newInstance(currentdate.year(), currentdate.month()+8, currentdate.day()+1);
            nodList.add(reg8);
        
         NoDs__c reg9 = new NoDs__c();
       		reg9.Account__c=ac.id;
            reg9.Amt_60_Days__c='11';
            reg9.Average_Outstanding__c=11;
            reg9.Account_Wise_NOD__c='11';
            reg9.Sales_Area_Wise_NOD__c='123';
            reg9.SAP_Code__c='11';
            reg9.Posting_Date__c=date.newInstance(currentdate.year(), currentdate.month()+9, currentdate.day()+1);
            nodList.add(reg9);
        
         NoDs__c reg10 = new NoDs__c();
        	reg10.Account__c=ac.id;
            reg10.Amt_60_Days__c='11';
            reg10.Average_Outstanding__c=11;
            reg10.Account_Wise_NOD__c='11';
            reg10.Sales_Area_Wise_NOD__c='123';
            reg10.SAP_Code__c='11';
            reg10.Posting_Date__c=date.newInstance(currentdate.year(), currentdate.month()+10, currentdate.day()+1);
            nodList.add(reg10);
        
         NoDs__c reg11 = new NoDs__c();
        	reg11.Account__c=ac.id;
            reg11.Amt_60_Days__c='11';
            reg11.Average_Outstanding__c=11;
            reg11.Account_Wise_NOD__c='11';
            reg11.Sales_Area_Wise_NOD__c='123';
            reg11.SAP_Code__c='11';
            reg11.Posting_Date__c=date.newInstance(currentdate.year(), currentdate.month()+11, currentdate.day()+1);
            nodList.add(reg11);
          
        if(nodlist.size()>0){
        	insert nodlist; 
        }
        return nodList;
    }
	
}
@Istest
public class AccountNodOsBalanceTest{
    Static Testmethod void NODTest(){
        Account acc = new Account();
        acc.Name = 'Test Account';
        acc.Sales_Area_Region__c = 'North';
        acc.Sales_Office_ERP__c = 'CAL1';
        acc.SAP_Code__c = '456123';
        insert acc;
        
        NODs__c nod = new NODs__c();
        nod.Account__c = acc.id;
        nod.Account_Wise_NOD__c = 'abc';
        nod.Sales_Area_Wise_NOD__c = 'North';
        nod.Average_Outstanding__c = 123;
        nod.SAP_Code__c = '456123';
        nod.Posting_Date__c = System.Today();
        
        String region = 'East'; 
        String salesOffice= 'CAL1'; 
        String month ='Jan'; 
        String year = '2018';
        Test.startTest();
        System.debug('reg -> '+region +'SO-> '+salesOffice+ 'Mo -> '+month+ 'year -> '+year);
        Account_NOD_OSBalance.selectedMonth();
        Account_NOD_OSBalance.accountRegions();
        Account_NOD_OSBalance.selectedYear();
        Account_NOD_OSBalance.salesOfficeNames();
        if(region != null && salesOffice != null && month != null && year != null)
        	Account_NOD_OSBalance.nod(region, salesOffice, month, year);
        
        Test.stopTest();
    
    }

}
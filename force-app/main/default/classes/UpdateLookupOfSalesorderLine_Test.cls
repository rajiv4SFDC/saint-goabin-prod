/* Created By         : Rajiv Kumar Singh
   Created Date       : 14/06/2018
   Class Name         : UpdateLookupOfSalesorderLine_Test
   Description        : 
   Last Modified By   : Rajiv Kumar Singh
   Last Modified Date : 14/06/2018
*/
@isTest
private class UpdateLookupOfSalesorderLine_Test
{ 
 // @istest
   private testmethod static void testMethod1()
   {
      Sales_Order__c p1 = new Sales_Order__c();
     // p1.Name = 'testing';
      p1.External_Id__c = '6565666';
      insert p1;
      
      Sales_Order_Line_Item__c ptp = new Sales_Order_Line_Item__c();
      ptp.External_id__c = '6565666';
      ptp.Sales_Order__c = p1.id;
      insert ptp;
      
      Sales_Order_Line_Item__c pt = [select id, External_id__c from Sales_Order_Line_Item__c where id =:ptp.id];
      
      list<Id> ptrList = new list<Id>();
      ptrList.add(pt.id);
      
      UpdateLookupSals.UpdateLookupValss(ptrList);
   }
}
/* Created By: Rajiv Kumar Singh
Created Date: 014/06/2017
Class Name: NewProcessorSapCodeUpsertAPI
Description : Update status on Account for ERP.
Last Modified By :Rajiv Kumar Singh
Last Modified Date: 14/06/2017
*/
@RestResource(urlMapping='/NewCustomerUpsertAPI')
global class NewProcessorSapCodeUpsertAPI {
    @HttpPost
    global static void doHandleInsertRequest(){
        
        List<newCustomerWrapper> wrapList = new List<newCustomerWrapper>();
        List<Account> accList = new List<Account>();
        RestRequest Req = RestContext.request;
        RestResponse res = RestContext.response;
        String body = Req.requestBody.toString().trim();
        
        if(body != null && body != '') {
            
            try {
                wrapList = (List<newCustomerWrapper>)JSON.deserialize(body, List<newCustomerWrapper>.class);
                
                for(newCustomerWrapper we :wrapList){
                    Account acc = new Account();
                    
                    if(we.NewprocessorID!=null && we.NewprocessorID!=''){
                        acc.New_Processor_Id__c = we.NewprocessorID;
                    } 
                    
                    if(we.SapCode!=null && we.SapCode!=''){
                        acc.SAP_Code__c = we.SapCode;
                    }  
                    
                    accList.add(acc);  
                }
                Schema.SObjectField ftoken = Account.Fields.New_Processor_Id__c;
                Database.UpsertResult[] srList = Database.upsert(accList,ftoken,false);      
                
                res.statusCode = 200;
                res.responseBody = Blob.valueOf(String.valueof(accList));
                
            } catch (Exception e) {
                
                CommonUtilitiesHandler.handleExceptions(e);
                res.statusCode = 500;
                res.responseBody = Blob.valueOf(e.getMessage());
            }
        } else {
            res.statusCode = 500;
            res.responseBody = Blob.valueOf('Invalid Request');
        } 
    }
    
    public class newCustomerWrapper{
        public String NewprocessorID{get;set;}
        public string SapCode{get;set;}
        
    }
}
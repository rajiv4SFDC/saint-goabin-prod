/* Created By         : Rajiv Kumar Singh
   Created Date       : 16/11/2018
   Class Name         : UpdateLookupOfEnquiry
   Description        : update the particular Enquiry record lookup field to Enquiry Status
   Last Modified By   : Rajiv Kumar Singh
   Last Modified Date : 16/11/2018
*/
public class UpdateLookupOfEnquiryStatus{
  @InvocableMethod(label='UpdateLookupOfEnquiryStatus' description='update the particular Enquiry record lookup field to Enquiry Status')
  public static void UpdateLookupValue(List<Id> enquiryIds)
    {   
       map<string,string> enquiryWithAccountMap = new map<string,string>();  
       set<string> presentSAPCodesSet = new set<String>();
       list<Enquiry_Status__c> NewList = new list<Enquiry_Status__c>(); 
       list<Enquiry_Status__c> FinalListToUpdate = new list<Enquiry_Status__c>();

       for(Enquiry_Status__c ptp:[select id,Enquiry_Reference__c,Enquiry__c from Enquiry_Status__c where id in:enquiryIds])
       {
         if(ptp.Enquiry_Reference__c != null)
          {
            presentSAPCodesSet.add(ptp.Enquiry_Reference__c);      
            NewList.add(ptp);
            system.debug('The ptp.id is: ' +ptp.id);
          }     
       }
       system.debug('The NewList is: ' +NewList+'......'+presentSAPCodesSet);
       if(presentSAPCodesSet.size()>0)
       {
          for(Enquiry__c acc:[select id,GD_Reference_Number__c from Enquiry__c where GD_Reference_Number__c in:presentSAPCodesSet])
           {
               enquiryWithAccountMap.put(acc.GD_Reference_Number__c,acc.id);
           }
           system.debug('The enquiryWithAccountMap is: ' +enquiryWithAccountMap);
           for(Enquiry_Status__c ptp:NewList)
           {
               ptp.Enquiry__c = enquiryWithAccountMap.get(ptp.Enquiry_Reference__c);
              // system.debug('The  ptp.Bill_to_customer__c is: ' + ptp.Bill_to_customer__c);
               FinalListToUpdate.add(ptp);
           }
       }
       if(!FinalListToUpdate.isEmpty() && FinalListToUpdate.size() > 0) {
            update FinalListToUpdate;
       }
    }
}
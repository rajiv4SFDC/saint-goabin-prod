/* Created By         : Rajiv Kumar Singh
   Created Date       : 06/07/2018
   Class Name         : UpdateProductLookupInvoice
   Description        : update the perticular product record lookup field on invoice
   Last Modified By   : Rajiv Kumar Singh
   Last Modified Date : 06/07/2018
*/
public class UpdateProductLookupInvoice{

  @InvocableMethod(label='UpdateProductLookupInvoice' description='update the perticular product record lookup field on invoice')
  public static void UpdateLookupValue(List<Id> invIds)
    {
       
       
       map<string,string> INVWithProductMap = new map<string,string>();  
       set<string> presentProductCodesSet = new set<String>();
       list<Invoice__c> NewList = new list<Invoice__c>(); 
       list<Invoice__c> FinalListToUpdate = new list<Invoice__c>();

       for(Invoice__c ptp:[select id,Product_External_ID__c,Product__c from Invoice__c where id IN: invIds])
       {
         if(ptp.Product_External_ID__c != null)
          {
            presentProductCodesSet.add(ptp.Product_External_ID__c);      
            NewList.add(ptp);
            system.debug('The ptp.id is: ' +ptp.id);
          }  
           
       }
       system.debug('The NewList is: ' +NewList+'......'+presentProductCodesSet);
       if(presentProductCodesSet.size()>0)
       {
          for(product2 pd:[select id,ProductCode,External_Id__c  from product2 where External_Id__c in:presentProductCodesSet])
           {
            INVWithProductMap.put(pd.External_Id__c,pd.id);
           }
       system.debug('The INVWithProductMap is: ' +INVWithProductMap);
        for(Invoice__c ptp:NewList)
        {
        ptp.product__c = INVWithProductMap.get(ptp.Product_External_ID__c);
        system.debug('The  ptp.product__c is: ' + ptp.Product_External_ID__c);
        FinalListToUpdate.add(ptp);
        }
       }
       
          update FinalListToUpdate;
       
    }
}
/* Created By         : Rajiv Kumar Singh
   Created Date       : 11/07/2018
   Class Name         : UpdateProductLookupInvoice
   Description        : update the perticular product record lookup field on Sales Order
   Last Modified By   : Rajiv Kumar Singh
   Last Modified Date : 11/07/2018
*/
public class UpdateProductLookupSalesOrder{

  @InvocableMethod(label='UpdateProductLookupSalesOrder' description='update the perticular product record lookup field on Sales Order')
  public static void UpdateLookupValue(List<Id> SalesIds)
    {
       
       
       map<string,string> SalesWithProductMap = new map<string,string>();  
       set<string> presentProductCodesSet = new set<String>();
       list<Sales_Order_Line_Item__c> NewList = new list<Sales_Order_Line_Item__c>(); 
       list<Sales_Order_Line_Item__c> FinalListToUpdate = new list<Sales_Order_Line_Item__c>();

       for(Sales_Order_Line_Item__c  ptp:[select id,Product_Item_Code__c,Product__c from Sales_Order_Line_Item__c where id IN: SalesIds])
       {
         if(ptp.Product_Item_Code__c != null)
          {
            presentProductCodesSet.add(ptp.Product_Item_Code__c);      
            NewList.add(ptp);
            system.debug('The ptp.id is: ' +ptp.id);
          }  
           
       }
       system.debug('The NewList is: ' +NewList+'......'+presentProductCodesSet);
       if(presentProductCodesSet.size()>0)
       {
          for(product2 pd:[select id,ProductCode,External_Id__c  from product2 where External_Id__c in:presentProductCodesSet])
           {
            SalesWithProductMap.put(pd.External_Id__c,pd.id);
           }
       system.debug('The SalesWithProductMap is: ' +SalesWithProductMap);
        for(Sales_Order_Line_Item__c ptp:NewList)
        {
        ptp.Product__c = SalesWithProductMap.get(ptp.Product_Item_Code__c);
        system.debug('The  ptp.product__c is: ' + ptp.Product_Item_Code__c);
        FinalListToUpdate.add(ptp);
        }
       }
       
          update FinalListToUpdate;
       
    }
}
@istest
private class PRForPandPMassApproval_test
{

 @istest private static void testMethod1()
 {
  Test.startTest();

 list<price_request_PandP__c> prList = new list<price_request_PandP__c>();
   //creating user record for the above role. 

 //  UserRole r = new UserRole(DeveloperName = 'MyCustomRole', Name = 'Regional Manager East');
 //   insert r;

   PricerequestPandPmassapproveregject__c prm = new PricerequestPandPmassapproveregject__c();
   prm.Name='test';
   prm.Role_Name__c ='Regional Manager East';
   prm.List_view_URL__c = 'https://www.saint-gobain.com';
   insert prm;
   
     PricerequestPandPmassapproveregject__c prm1 = new PricerequestPandPmassapproveregject__c();
   prm1.Name='Default';
   prm1.Role_Name__c ='Default';
   prm1.List_view_URL__c = 'https://www.saint-gobain.com';
   insert prm1;
   

           
   NewcustomerrequestforPR__c newcustom = new NewcustomerrequestforPR__c();
        newcustom.Active__c = true;
        newcustom.Role_Name__c ='Regional Manager South';
        newcustom.Name ='South';
        insert newcustom; 
         
        Enquiry__c enc = new Enquiry__c();
        enc.Account_Name__c = 'testing';
        enc.GD_Reference_Number__c = '12';       
        insert enc;
              
        enquiry_Line_Item__c enitem = new enquiry_Line_Item__c();
       //enitem.Name = 'test';
       enitem.Reference_Number__c = '12';
       enitem.Enquiry__c = enc.Id;
       insert enitem;
      
       
       // code coverage for doFetchPriceRequest();
         Account acc = new Account();
        acc.Name ='test3';
        acc.Type ='Industry';
        acc.Sales_Area_Region__c ='South';
        acc.PAN_Number__c = 'AAAPL1234C';
       acc.Sales_Office_ERP__c = 'BLR1';
     //  acc.Sales_Area__c ='KA1';
        acc.RecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('ERP').getRecordTypeId();
        insert acc;
        
        Opportunity opp = new Opportunity();
        String oppid1;
        opp.AccountId =acc.Id;
        opp.Name ='test1';
        opp.CloseDate = Date.today();
        opp.Amount = 1000;
        opp.StageName ='Prospecting';
        opp.Probability= 0.5;
        oppid1=acc.id;
        insert opp;
        
        acc = new Account();
        acc.Name ='test31';
        acc.Type ='Industry1';
        acc.PAN_Number__c ='AAAPL1234C';
        acc.Sales_Area_Region__c ='South';
       acc.Sales_Office_ERP__c = 'BLR1';
       acc.Sales_Area__c ='KA1';
        acc.RecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('CRM').getRecordTypeId();
        insert acc;
        
        Key_Account__c keyAcct = new Key_Account__c();
        keyAcct.Opportunity__c = opp.Id;
        keyAcct.Account__c = acc.Id;
        insert keyAcct;
        
        
        account  acc2 = new account();
        acc2.Name ='main acc for erp';
        acc2.Type ='Industry2';
        acc2.PAN_Number__c ='AAAPL1234f';
        acc2.Sales_Area_Region__c ='South';
        acc2.sap_code__c = '123123';
        acc2.Sales_Office_ERP__c = 'BLR1';
       acc2.Sales_Area__c ='KA1';
        acc2.RecordTypeId = Schema.SObjectType.account.getRecordTypeInfosByName().get('ERP').getRecordTypeId();
        insert acc2;
       
        account  acc3 = new account();
        acc3.Name ='main acc for erp2';
        acc3.Type ='Industry2';
        acc3.PAN_Number__c ='bldpp4721t';
        acc3.Sales_Area_Region__c ='South';
        acc3.sap_code__c = '80231578';
       acc3.Sales_Office_ERP__c = 'BLR1';
       acc3.Sales_Area__c ='KA1';

        acc3.RecordTypeId = Schema.SObjectType.account.getRecordTypeInfosByName().get('ERP').getRecordTypeId();
        insert acc3;
        
        opp.Customer_Name__c = keyAcct.Id;
        update opp;
        
        Product2 masterProduct = new Product2();
        masterProduct.Name = opp.id;
        masterProduct.Article_number__c ='test1234';
        masterProduct.Product_Category__c ='Shower Cubicles';
        masterProduct.master_product__c = true;
        insert masterProduct;
        
        Product2 pro = new Product2();
        pro.Name = 'test prod';
        pro.Product_Category__c ='Shower Cubicles';
        pro.ProductCode ='test123';
        pro.Parent_product__c = masterProduct.id;
        pro.Article_number__c = 'test123';
        pro.Quantity__c  = 2;
        pro.global_product__c = true;
        insert pro;   
        List<Product2> prodList = new List<product2>();
        prodList.add(pro);
       Product2 p2 =[select id,name,external_id__c from Product2 where id=:pro.id];
        Product_Tier_Pricing__c ptr = new Product_Tier_Pricing__c();
        ptr.name ='testing tier';
        ptr.Business_Location__c ='BLR1';
        ptr.Product__c = pro.id;
        ptr.Product_Code__c ='test12312';
        ptr.Tier_Name__c ='T1';
        ptr.Thickness__c =22;
        ptr.Item_Code__c = p2.external_id__c;
        insert ptr;
        
        List<ProductsWrapper> prodWrapper =  new List<ProductsWrapper>();
        ProductsWrapper prodWrap = new ProductsWrapper();
        prodWrap.prdName = pro.Name;
        prodWrap.prdCode = pro.ProductCode;
        prodWrap.prdQuantitiy = Integer.valueof(pro.Quantity__c);
        prodWrap.prdArticlenumber = pro.Article_number__c;
        prodWrap.prdParentId = pro.Parent_product__c;
        //prodWrap.oppProductLineItemId = pro.Name;
        prodWrapper.add(prodWrap);
        
        Opportunity_Product__c oppproduct = new Opportunity_Product__c();
        oppproduct.Opportunity__c = opp.Id;
        oppproduct.Product__c = masterProduct.Id;
        insert oppproduct;
        
        Opportunity_Product_line_items__c oppline = new Opportunity_Product_line_items__c();
        oppline.Opportunity_Product__c=oppproduct.id;
        oppline.Product__c = pro.Id;
        oppline.Quantity__c = 2;
        insert oppline;
        
        Product_Forecast__c forecast = new Product_Forecast__c();
        forecast.Quantity__c = 10000;
        forecast.Opportunity_Product__c = oppproduct.Id;
        insert forecast;
        
        price_request_PandP__c pr = new price_request_PandP__c();
        pr.Sales_Office__c = 'CAL1';
        pr.Price_Type__c   = 'Tier Price';
        pr.Opportunity_Product__c = oppproduct.id;
        pr.Valid_From__c = System.today();
        pr.Valid_To__c = System.today();
        pr.Requested_Quantity__c = 1000;
        pr.Approval_status__c ='Draft';
        pr.Regional_head_sales_and_marketing_User__c = userInfo.getUserId();
        pr.Regional_Manager__c =  userInfo.getUserId();
        pr.Enquiry_Line_Item__c = enitem.id;
        pr.Enquiry__c = enc.id;
        pr.Customer__c = acc2.id;
        pr.Product_lookup__c = pro.id;

       // pr.Regional_manager_special_price__c =  userInfo.getUserId();
      prList.add(pr);
        insert prList;       
 
  Test.setCurrentPage(Page.PRForPandPMassApprovalPage);
  ApexPages.StandardSetController stdSetController = new ApexPages.StandardSetController(prList);
  stdSetController.setSelected(prList);
  PRForPandPMassApproval ext = new PRForPandPMassApproval(stdSetController);
  //Submit Quote for Approval
        Approval.ProcessSubmitRequest app = new Approval.ProcessSubmitRequest();
        app.setObjectId(prList[0].Id);
        Approval.ProcessResult result = Approval.process(app);
  ext.approveRecs();
  ext.getMySelectedSize();
  ext.getMyRecordsSize();
  ext.cancelPage();
  ext.validateLoginUser();
 Test.stopTest();
 }
 
 
 @istest private static void testMethod2()
 {
  Test.startTest();
 PricerequestPandPmassapproveregject__c prm = new PricerequestPandPmassapproveregject__c();
   prm.Name='test';
   prm.Role_Name__c ='Regional Manager East';
   prm.List_view_URL__c = 'https://www.saint-gobain.com';
   insert prm;
 list<price_request_PandP__c> prList = new list<price_request_PandP__c>();
   //creating user record for the above role. 

 //  UserRole r = new UserRole(DeveloperName = 'MyCustomRole', Name = 'Regional Manager East');
 //   insert r;



           
   NewcustomerrequestforPR__c newcustom = new NewcustomerrequestforPR__c();
        newcustom.Active__c = true;
        newcustom.Role_Name__c ='Regional Manager South';
        newcustom.Name ='South';
        insert newcustom; 
         
        Enquiry__c enc = new Enquiry__c();
        enc.Account_Name__c = 'testing';
        enc.GD_Reference_Number__c = '12';       
        insert enc;
              
        enquiry_Line_Item__c enitem = new enquiry_Line_Item__c();
       //enitem.Name = 'test';
       enitem.Reference_Number__c = '12';
       enitem.Enquiry__c = enc.Id;
       insert enitem;
      
       
       // code coverage for doFetchPriceRequest();
         Account acc = new Account();
        acc.Name ='test3';
        acc.Type ='Industry';
        acc.Sales_Area_Region__c ='South';
        acc.PAN_Number__c = 'AAAPL1234C';
       acc.Sales_Office_ERP__c = 'BLR1';
       //acc.Sales_Area__c ='KA1';
        acc.RecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('ERP').getRecordTypeId();
        insert acc;
        
        Opportunity opp = new Opportunity();
        String oppid1;
        opp.AccountId =acc.Id;
        opp.Name ='test1';
        opp.CloseDate = Date.today();
        opp.Amount = 1000;
        opp.StageName ='Prospecting';
        opp.Probability= 0.5;
        oppid1=acc.id;
        insert opp;
        
        acc = new Account();
        acc.Name ='test31';
        acc.Type ='Industry1';
        acc.PAN_Number__c ='AAAPL1234C';
        acc.Sales_Area_Region__c ='South';
       acc.Sales_Office_ERP__c = 'BLR1';
     //  acc.Sales_Area__c ='KA1';
        acc.RecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('CRM').getRecordTypeId();
        insert acc;
        
        Key_Account__c keyAcct = new Key_Account__c();
        keyAcct.Opportunity__c = opp.Id;
        keyAcct.Account__c = acc.Id;
        insert keyAcct;
        
        
        account  acc2 = new account();
        acc2.Name ='main acc for erp';
        acc2.Type ='Industry2';
        acc2.PAN_Number__c ='AAAPL1234f';
        acc2.Sales_Area_Region__c ='South';
        acc2.sap_code__c = '123123';
        acc2.Sales_Office_ERP__c = 'BLR1';
     //  acc2.Sales_Area__c ='KA1';
        acc2.RecordTypeId = Schema.SObjectType.account.getRecordTypeInfosByName().get('ERP').getRecordTypeId();
        insert acc2;
       
        account  acc3 = new account();
        acc3.Name ='main acc for erp2';
        acc3.Type ='Industry2';
        acc3.PAN_Number__c ='bldpp4721t';
        acc3.Sales_Area_Region__c ='South';
        acc3.sap_code__c = '80231578';
       acc3.Sales_Office_ERP__c = 'BLR1';
    //   acc3.Sales_Area__c ='KA1';

        acc3.RecordTypeId = Schema.SObjectType.account.getRecordTypeInfosByName().get('ERP').getRecordTypeId();
        insert acc3;
        
        opp.Customer_Name__c = keyAcct.Id;
        update opp;
        
        Product2 masterProduct = new Product2();
        masterProduct.Name = opp.id;
        masterProduct.Article_number__c ='test1234';
        masterProduct.Product_Category__c ='Shower Cubicles';
        masterProduct.master_product__c = true;
        insert masterProduct;
        
        Product2 pro = new Product2();
        pro.Name = 'test prod';
        pro.Product_Category__c ='Shower Cubicles';
        pro.ProductCode ='test123';
        pro.Parent_product__c = masterProduct.id;
        pro.Article_number__c = 'test123';
        pro.Quantity__c  = 2;
        pro.global_product__c = true;
        insert pro;   
        List<Product2> prodList = new List<product2>();
        prodList.add(pro);
       Product2 p2 =[select id,name,external_id__c from Product2 where id=:pro.id];
        Product_Tier_Pricing__c ptr = new Product_Tier_Pricing__c();
        ptr.name ='testing tier';
        ptr.Business_Location__c ='BLR1';
        ptr.Product__c = pro.id;
        ptr.Product_Code__c ='test12312';
        ptr.Tier_Name__c ='T1';
        ptr.Thickness__c =22;
        ptr.Item_Code__c = p2.external_id__c;
        insert ptr;
        
        List<ProductsWrapper> prodWrapper =  new List<ProductsWrapper>();
        ProductsWrapper prodWrap = new ProductsWrapper();
        prodWrap.prdName = pro.Name;
        prodWrap.prdCode = pro.ProductCode;
        prodWrap.prdQuantitiy = Integer.valueof(pro.Quantity__c);
        prodWrap.prdArticlenumber = pro.Article_number__c;
        prodWrap.prdParentId = pro.Parent_product__c;
        //prodWrap.oppProductLineItemId = pro.Name;
        prodWrapper.add(prodWrap);
        
        Opportunity_Product__c oppproduct = new Opportunity_Product__c();
        oppproduct.Opportunity__c = opp.Id;
        oppproduct.Product__c = masterProduct.Id;
        insert oppproduct;
        
        Opportunity_Product_line_items__c oppline = new Opportunity_Product_line_items__c();
        oppline.Opportunity_Product__c=oppproduct.id;
        oppline.Product__c = pro.Id;
        oppline.Quantity__c = 2;
        insert oppline;
        
        Product_Forecast__c forecast = new Product_Forecast__c();
        forecast.Quantity__c = 10000;
        forecast.Opportunity_Product__c = oppproduct.Id;
        insert forecast;
        
        price_request_PandP__c pr = new price_request_PandP__c();
        pr.Sales_Office__c = 'CAL1';
        pr.Price_Type__c   = 'Tier Price';
        pr.Opportunity_Product__c = oppproduct.id;
        pr.Valid_From__c = System.today();
        pr.Valid_To__c = System.today();
        pr.Requested_Quantity__c = 1000;
        pr.Approval_status__c ='Draft';
        pr.Regional_head_sales_and_marketing_User__c = userInfo.getUserId();
        pr.Regional_Manager__c =  userInfo.getUserId();
        pr.Enquiry_Line_Item__c = enitem.id;
        pr.Enquiry__c = enc.id;
        pr.Customer__c = acc2.id;
        pr.Product_lookup__c = pro.id;

       // pr.Regional_manager_special_price__c =  userInfo.getUserId();
      prList.add(pr);
        insert prList;       
 
  Test.setCurrentPage(Page.PRForPandPMassApprovalPage);
  ApexPages.StandardSetController stdSetController = new ApexPages.StandardSetController(prList);
  stdSetController.setSelected(prList);
  PRForPandPMassApproval ext = new PRForPandPMassApproval(stdSetController);
  //Submit Quote for Approval
        Approval.ProcessSubmitRequest app = new Approval.ProcessSubmitRequest();
        app.setObjectId(prList[0].Id);
        Approval.ProcessResult result = Approval.process(app);
  ext.rejectRecs();
  ext.getMySelectedSize();
  ext.getMyRecordsSize();
 Test.stopTest();
 }

}
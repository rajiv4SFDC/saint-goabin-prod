/* Created By         : Rajiv Kumar Singh
   Created Date       : 14/06/2018
   Class Name         : UpdateLookupOfNewSalesorderLineItem
   Description        : update the particular NewSalesOrder record lookup field to Enquiry Line Item
   Last Modified By   : Rajiv Kumar Singh
   Last Modified Date : 14/06/2018
*/
public class UpdateLookupOfNewSalesorderLineItem{
  @InvocableMethod(label='UpdateLookupOfNewSalesorderLineItem' description='update the particular NewSalesOrder record lookup field to Enquiry Line Item')
  public static void UpdateLookupValue(List<Id> NewSalesOrderIdss)
    {   
       map<string,string> newSalesOrderWithEnquiryLineMap = new map<string,string>();  
       set<string> presentSAPCodesSet = new set<String>();
       list<Sales_Order_Line_Item__c> NewList = new list<Sales_Order_Line_Item__c>(); 
       list<Sales_Order_Line_Item__c> FinalListToUpdate = new list<Sales_Order_Line_Item__c>();

       for(Sales_Order_Line_Item__c ptp:[select id,Enquiry_Line_Item__c,Reference_Number__c from Sales_Order_Line_Item__c where id in:NewSalesOrderIdss])
       {
         if(ptp.Reference_Number__c != null)
          {
            presentSAPCodesSet.add(ptp.Reference_Number__c);      
            NewList.add(ptp);
            system.debug('The ptp.id is: ' +ptp.id);
          }     
       }
       system.debug('The NewList is: ' +NewList+'......'+presentSAPCodesSet);
       if(presentSAPCodesSet.size()>0)
       {
          for(Enquiry_Line_Item__c acc:[select id,Reference_Number__c  from Enquiry_Line_Item__c where Reference_Number__c in:presentSAPCodesSet])
           {
               newSalesOrderWithEnquiryLineMap.put(acc.Reference_Number__c,acc.id);
           }
           system.debug('The newSalesOrderWithEnquiryLineMap is: ' +newSalesOrderWithEnquiryLineMap);
           for(Sales_Order_Line_Item__c ptp:NewList)
           {
               ptp.Enquiry_Line_Item__c = newSalesOrderWithEnquiryLineMap.get(ptp.Reference_Number__c);
               system.debug('The  ptp.Enquiry_Line_Item__c is: ' + ptp.Enquiry_Line_Item__c);
               FinalListToUpdate.add(ptp);
           }
       }
       if(!FinalListToUpdate.isEmpty() && FinalListToUpdate.size() > 0) {
            update FinalListToUpdate;
       }
    }
}
public with sharing class AccountForecast{
   @AuraEnabled
    public static void SaveAccounts(List<Account_Forecast__c> accountForecastList, String selectedYears, List<Account_Forecast__c> accountForecastListOld){
        System.debug('accountForecastListOld -> '+accountForecastListOld);
        System.debug('naya list '+accountForecastList+ 'uska size '+accountForecastList.size());
        
        //to get current user managers hierarchy
        
        Id regionalManager, nationalHead;
        
        id id1 = userinfo.getProfileId();
        Profile loggedInProfile = [SELECT id, Name FROM Profile WHERE id = :id1 LIMIT 1];
        System.debug('-->profile Name '+loggedInProfile.Name);        
        Boolean queryToBeAdded = String.ValueOf(loggedInProfile.Name).contains('ORS');
        System.debug('queryToBeAdded '+queryToBeAdded);
        String rmUser = '';
        String nationalUser = '';
        if(queryToBeAdded == true){
            rmUser = ',Manager.Manager.Manager.Id ';
            nationalUser = ',Manager.Manager.Manager.Manager.Id ';
        }
        else{
            rmUser = ',Manager.Manager.Id ';
            nationalUser = ',Manager.Manager.Manager.Id '; 
        }
        
        System.debug('rmUser '+rmUser+'nationalUser '+nationalUser);
        Id userId = UserInfo.getUserId();
        List<user> userList = Database.query('SELECT id, Name'+ rmUser + nationalUser +' FROM User WHERE id =:userId');
        for(User us : userList){
            if(queryToBeAdded == true){
                regionalManager = us.Manager.Manager.Manager.Id;
                nationalHead = us.Manager.Manager.Manager.Manager.Id;
                
                System.debug('us ORS'+us.Manager.Manager.Manager.Id);
                System.debug('uss ORS'+us.Manager.Manager.Manager.Manager.Id);
            }
            else{
                regionalManager = us.Manager.Manager.Id;
                nationalHead = us.Manager.Manager.Manager.Id;
                
                System.debug('us AM'+us.Manager.Manager.Id);
                System.debug('uss AM'+us.Manager.Manager.Manager.Id);
            }
        }

        
        //insertion
        if(accountForecastList.size() > 0){
            List<Account_Forecast__c> afList = new List<Account_Forecast__c>();
            for(Account_Forecast__c af : accountForecastList){
                af.Year__c = Decimal.ValueOf(selectedYears);
                af.Regional_Head__c = regionalManager;
                af.National_Head__c = nationalHead;
                afList.add(af);
            }
            insert afList;
        }
        
        
        List<Account_Forecast__c> accForeCastListToBeUpdated = new List<Account_Forecast__c>();
        for(Account_Forecast__c af : accountForecastListOld){
            if(af.ORM_Submitted__c == False)
                accForeCastListToBeUpdated.add(af);
        }
        update accForeCastListToBeUpdated;
    }
    
    @AuraEnabled
    public static void savemonthlyPlan(List<Monthly_Plan__c> monthlyPlanList, String accountForecastId){
        
        //to get current user managers hierarchy
        
        Id regionalManager, nationalHead;
        
        id id1 = userinfo.getProfileId();
        Profile loggedInProfile = [SELECT id, Name FROM Profile WHERE id = :id1 LIMIT 1];
        System.debug('-->profile Name '+loggedInProfile.Name);        
        Boolean queryToBeAdded = String.ValueOf(loggedInProfile.Name).contains('ORS');
        System.debug('queryToBeAdded '+queryToBeAdded);
        String rmUser = '';
        String nationalUser = '';
        if(queryToBeAdded == true){
            rmUser = ',Manager.Manager.Manager.Id ';
            nationalUser = ',Manager.Manager.Manager.Manager.Manager.Id ';
        }
        else{
            rmUser = ',Manager.Manager.Id ';
            nationalUser = ',Manager.Manager.Manager.Manager.Id '; 
        }
        
        System.debug('rmUser '+rmUser+'nationalUser '+nationalUser);
        Id userId = UserInfo.getUserId();
        List<user> userList = Database.query('SELECT id, Name'+ rmUser + nationalUser +' FROM User WHERE id =:userId');
        for(User us : userList){
            if(queryToBeAdded == true){
                regionalManager = us.Manager.Manager.Manager.Id;
                nationalHead = us.Manager.Manager.Manager.Manager.Manager.Id;
                
                System.debug('us ORS'+us.Manager.Manager.Manager.Id);
                System.debug('uss ORS'+us.Manager.Manager.Manager.Manager.Manager.Id);
            }
            else{
                regionalManager = us.Manager.Manager.Id;
                nationalHead = us.Manager.Manager.Manager.Manager.Id;
                
                System.debug('us AM'+us.Manager.Manager.Id);
                System.debug('uss AM'+us.Manager.Manager.Manager.Manager.Id);
            }
        }
        
        Map<String, String> mapOfMonth = new map<String, String>{'Jan'=>'1', 'Feb'=>'2', 'Mar'=>'3', 'Apr'=>'4', 'May'=>'5', 'Jun'=>'6', 'July'=>'7', 'Aug'=>'8', 'Sept'=>'9', 'Oct'=>'10', 'Nov'=>'11', 'Dec'=>'12'};
        System.debug('monthlyPlanList ->'+monthlyPlanList);
        
        Account_Forecast__c acforecast = [SELECT id, Product_Category__c, Account__c, Year__c FROM Account_Forecast__c WHERE id =: accountForecastId LIMIT 1];
        System.debug('accountForecast '+ acforecast);
        for(Monthly_Plan__c mp : monthlyPlanList){
            System.debug(mp.Month__c);
            mp.Month__c = mapOfMonth.get(mp.Month__c);
            mp.Year__c = acforecast.Year__c;
            mp.Account__c = acforecast.Account__c;
            mp.Account_Forecast__c = acforecast.Id;
            mp.Regional_Head__c = regionalManager;
            mp.National_Head__c = nationalHead;
                
        }
        if(monthlyPlanList.size() > 0)
            insert monthlyPlanList;
    }
    
    @AuraEnabled
    public static List<String> getAllAttributesForMonthlyPlan(String accountForecastId){
        List<String> monthlyPlanDataList = new List<String>();
        System.debug('Here----->'+accountForecastId);
        Account_Forecast__c accForecastList = [SELECT id, Account__c, Account__r.Name, Product_Category__c, Volume_in_tons_expected__c FROM Account_Forecast__c WHERE Id =:accountForecastId LIMIT 1];
        monthlyPlanDataList.add(accForecastList.Account__r.Name);
        monthlyPlanDataList.add(accForecastList.Product_Category__c);
        monthlyPlanDataList.add(String.ValueOf((accForecastList.Volume_in_tons_expected__c/12).setScale(2)));
        System.debug('accForecastList size ->'+ accForecastList +'  List vals'+monthlyPlanDataList);
        return monthlyPlanDataList;
    }
    
    @AuraEnabled
    public static List<String> selectedYear(){
        Date dt = system.today();
        Decimal year = dt.year();
        List<String> yearList = new List<String>();
        for(Decimal i = year ; i<= Decimal.ValueOf(Label.Account_Forecast_Years) ;i++){
            yearList.add(String.ValueOf(i));
        }
        yearList.add(0,'Year');
        System.debug('--><'+yearList);
        return yearList;
    }
    
    @AuraEnabled
    public static List<String> productCategories(){
        List<String> options = new List<String>();
        List<Product_Category__c> productCatMasterList = [SELECT id,Name FROM Product_Category__c LIMIT 10000];
        for(Product_Category__c pc : productCatMasterList){
            options.add(pc.Name);
        }
        return options;
    }
    
    @AuraEnabled
    public static List<Account_Forecast__c> getAllAccountForecast(String yearSelected){
        Id loggedInUser = UserInfo.getUserId();
        List<Account_Forecast__c> accountForecastList =[SELECT id, Account__c, Account__r.Name, Product_Category__c, ORM_Submitted__c, Year__c, Volume_in_tons_expected__c FROM Account_Forecast__c WHERE ownerId =:loggedInUser AND Year__c =:Decimal.ValueOf(yearSelected) ORDER BY createdDate ASC];
        System.debug('here -->'+accountForecastList);
        return accountForecastList;
    }
    
    //save and submit account forecast function
    @AuraEnabled
    public static void submitAndSaveAccounts(List<Account_Forecast__c> accountForecastList, String selectedYears, List<Account_Forecast__c> accountForecastListOld){
        List<Account_Forecast__c> afList = new List<Account_Forecast__c>();
        
        //to get current user managers hierarchy
        
        Id regionalManager, nationalHead;
        
        id id1 = userinfo.getProfileId();
        Profile loggedInProfile = [SELECT id, Name FROM Profile WHERE id = :id1 LIMIT 1];
        System.debug('-->profile Name '+loggedInProfile.Name);        
        Boolean queryToBeAdded = String.ValueOf(loggedInProfile.Name).contains('ORS');
        System.debug('queryToBeAdded '+queryToBeAdded);
        String rmUser = '';
        String nationalUser = '';
        if(queryToBeAdded == true){
            rmUser = ',Manager.Manager.Manager.Id ';
            nationalUser = ',Manager.Manager.Manager.Manager.Id ';
        }
        else{
            rmUser = ',Manager.Manager.Id ';
            nationalUser = ',Manager.Manager.Manager.Id '; 
        }
        
        System.debug('rmUser '+rmUser+'nationalUser '+nationalUser);
        Id userId = UserInfo.getUserId();
        List<user> userList = Database.query('SELECT id, Name'+ rmUser + nationalUser +' FROM User WHERE id =:userId');
        for(User us : userList){
            if(queryToBeAdded == true){
                regionalManager = us.Manager.Manager.Manager.Id;
                nationalHead = us.Manager.Manager.Manager.Manager.Id;
                
                System.debug('us ORS'+us.Manager.Manager.Manager.Id);
                System.debug('uss ORS'+us.Manager.Manager.Manager.Manager.Id);
            }
            else{
                regionalManager = us.Manager.Manager.Id;
                nationalHead = us.Manager.Manager.Manager.Id;
                
                System.debug('us AM'+us.Manager.Manager.Id);
                System.debug('uss AM'+us.Manager.Manager.Manager.Id);
            }
        }

            
        if(accountForecastList.size() > 0){
            for(Account_Forecast__c af : accountForecastList){
                af.Year__c = Decimal.ValueOf(selectedYears);
                af.Regional_Head__c = regionalManager;
                af.National_Head__c = nationalHead;
                afList.add(af);
            }
            insert afList;
            System.debug('size ->'+afList.size());
        }    
        
        List<Account_Forecast__c> accForeCastListToBeUpdated = new List<Account_Forecast__c>();
        for(Account_Forecast__c af : accountForecastListOld){
            if(af.ORM_Submitted__c == False)
                accForeCastListToBeUpdated.add(af);
        }
        update accForeCastListToBeUpdated;
        
        Id loggedInUser = UserInfo.getUserId();
        List<Account_Forecast__c> accForecastList =[SELECT id, Account__c, Account__r.Name, Product_Category__c, ORM_Submitted__c, Year__c, Volume_in_tons_expected__c FROM Account_Forecast__c WHERE ownerId =:loggedInUser AND Year__c =:Decimal.ValueOf(selectedYears) AND ORM_Submitted__c = False];
        System.debug('accForecastList -> '+accForecastList);
        afList.addAll(accForecastList);
        
        List<Account_Forecast__c> newAfList = new List<Account_Forecast__c>();
        for(Account_Forecast__c af : afList){
            af.ORM_Submitted__c = true;
            newAfList.add(af);
        }
        
        Map<id,Account_Forecast__c> mapOfAccountForeCastToUpdate = new Map<id,Account_Forecast__c>();
        mapOfAccountForeCastToUpdate.putall(newAfList);
        if(mapOfAccountForeCastToUpdate.Values().size() > 0)
            update mapOfAccountForeCastToUpdate.Values();
        
        System.debug('here -->'+afList);
        List<Approval.ProcessSubmitRequest> requests = new List<Approval.ProcessSubmitRequest> ();
        for (Account_Forecast__c accForcasId: afList) {
            Approval.ProcessSubmitRequest req1 = new Approval.ProcessSubmitRequest();
            req1.setComments('Submitting request for approval ');
            req1.setObjectId(accForcasId.id);
            requests.add(req1);
        }
        
        Approval.ProcessResult[] processResults = null;
        try {
            processResults = Approval.process(requests, true);
        }
        catch (System.DmlException e) {
            System.debug('Exception Is ' + e.getMessage());
        }
    }
    
    //save and submit monthly plan function
    @AuraEnabled
    public static void submitAndSavemonthlyPlan(List<Monthly_Plan__c> monthlyPlanList, String accountForecastId, List<Monthly_Plan__c> monthlyPlanListOld){
        List<Monthly_Plan__c> mpList = new List<Monthly_Plan__c>();
        
        //to get current user managers hierarchy
        
        Id regionalManager, nationalHead;
        
        id id1 = userinfo.getProfileId();
        Profile loggedInProfile = [SELECT id, Name FROM Profile WHERE id = :id1 LIMIT 1];
        System.debug('-->profile Name '+loggedInProfile.Name);        
        Boolean queryToBeAdded = String.ValueOf(loggedInProfile.Name).contains('ORS');
        System.debug('queryToBeAdded '+queryToBeAdded);
        String rmUser = '';
        String nationalUser = '';
        if(queryToBeAdded == true){
            rmUser = ',Manager.Manager.Manager.Id ';
            nationalUser = ',Manager.Manager.Manager.Manager.Manager.Id ';
        }
        else{
            rmUser = ',Manager.Manager.Id ';
            nationalUser = ',Manager.Manager.Manager.Manager.Id '; 
        }
        
        System.debug('rmUser '+rmUser+'nationalUser '+nationalUser);
        Id userId = UserInfo.getUserId();
        List<user> userList = Database.query('SELECT id, Name'+ rmUser + nationalUser +' FROM User WHERE id =:userId');
        for(User us : userList){
            if(queryToBeAdded == true){
                regionalManager = us.Manager.Manager.Manager.Id;
                nationalHead = us.Manager.Manager.Manager.Manager.Manager.Id;
                
                System.debug('us ORS'+us.Manager.Manager.Manager.Id);
                System.debug('uss ORS'+us.Manager.Manager.Manager.Manager.Manager.Id);
            }
            else{
                regionalManager = us.Manager.Manager.Id;
                nationalHead = us.Manager.Manager.Manager.Manager.Id;
                
                System.debug('us AM'+us.Manager.Manager.Id);
                System.debug('uss AM'+us.Manager.Manager.Manager.Manager.Id);
            }
        }
        //end of user managers retreiving
        
        Map<String, String> mapOfMonth = new map<String, String>{'Jan'=>'1', 'Feb'=>'2', 'Mar'=>'3', 'Apr'=>'4', 'May'=>'5', 'Jun'=>'6', 'July'=>'7', 'Aug'=>'8', 'Sept'=>'9', 'Oct'=>'10', 'Nov'=>'11', 'Dec'=>'12'};
        System.debug('monthlyPlanList ->'+monthlyPlanList);
        
        Account_Forecast__c acforecast = [SELECT id, Product_Category__c, Account__c, Year__c FROM Account_Forecast__c WHERE id =: accountForecastId LIMIT 1];
        System.debug('accountForecast '+ acforecast);
        for(Monthly_Plan__c mp : monthlyPlanList){
            System.debug(mp.Month__c);
            mp.Month__c = mapOfMonth.get(mp.Month__c);
            mp.Year__c = acforecast.Year__c;
            mp.Account__c = acforecast.Account__c;
            mp.Account_Forecast__c = acforecast.Id;
            mp.Regional_Head__c = regionalManager;
            mp.National_Head__c = nationalHead;
            mpList.add(mp);
        }
        if(mpList.size() > 0)
            insert mpList;
        
        List<Monthly_Plan__c> monthlyPlanListToBeUpdated = new List<Monthly_Plan__c>();
        for(Monthly_Plan__c mp : monthlyPlanListOld){
            if(mp.Submitted__c == False)
                monthlyPlanListToBeUpdated.add(mp);
        }
        update monthlyPlanListToBeUpdated;
        
        
        Id loggedInUser = UserInfo.getUserId();
        List<Monthly_Plan__c> monthPlanList =[SELECT id, Account__c, Account__r.Name, Account_Name__c, Account_Forecast__c, Submitted__c, Month__c, Product_Category__c, Monthly_Target__c, Acheived_Target__c, My_Target__c FROM Monthly_Plan__c WHERE Account_Forecast__c =:accountForecastId AND Submitted__c = False];
        System.debug('monthPlanList -> '+monthPlanList);
        mpList.addAll(monthPlanList);
        
        List<Monthly_Plan__c> newMpList = new List<Monthly_Plan__c>();
        for(Monthly_Plan__c mp : mpList){
            mp.Submitted__c = true;
            newMpList.add(mp);
        }
        Map<id,Monthly_Plan__c> mapOfMonthlyPlanToUpdate = new Map<id,Monthly_Plan__c>();
        mapOfMonthlyPlanToUpdate.putall(newMpList);
        if(mapOfMonthlyPlanToUpdate.Values().size() > 0)
            update mapOfMonthlyPlanToUpdate.Values();
        
        System.debug('here -->'+mpList);
        List<Approval.ProcessSubmitRequest> requests = new List<Approval.ProcessSubmitRequest> ();
        for (Monthly_Plan__c monthPlanId: mpList) {
            Approval.ProcessSubmitRequest req1 = new Approval.ProcessSubmitRequest();
            req1.setComments('Submitting request for approval ');
            req1.setObjectId(monthPlanId.id);
            requests.add(req1);
        }
        
        
        Approval.ProcessResult[] processResults = null;
        try {
            processResults = Approval.process(requests, true);
        }
        catch (System.DmlException e) {
            System.debug('Exception Is in Month Plan ' + e.getMessage());
        }
    }
    
    //end of testing
    
    @AuraEnabled
    public static List<Monthly_Plan__c> getMonthlyPlan(String account, String productCat, String accountForecastId){
        System.debug('account -='+account +'- product -'+productCat+' accForecast Id -'+accountForecastId);
        List<Monthly_Plan__c> monthlyPlanList = [SELECT id, Account__c, Account__r.Name, Account_Name__c, Month__c, Account_Forecast__c, Product_Category__c, Monthly_Target__c, Acheived_Target__c, My_Target__c, SUbmitted__c FROM Monthly_Plan__c WHERE Account_Forecast__c =:accountForecastId];
        System.debug(monthlyPlanList.size() +' - '+monthlyPlanList);
        return monthlyPlanList;
    }
}
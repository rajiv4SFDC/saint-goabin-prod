// Wrapper class for processing the enquiry line items.
    public class enqLineItemsWrapper{
        
        public enqLineItemsWrapper(){
             // code for getting the product2.Product_Category__c
        list<string> pikclistvales = new list<string>();       
        pikclistvales.add('-None-');
        Schema.DescribeFieldResult fieldResult = product2.Product_Category__c.getDescribe();
        system.debug('The fieldResult is:'+fieldResult);
        List<Schema.PicklistEntry> ple = fieldResult.getPicklistValues();
        for( Schema.PicklistEntry f : ple){
         
          pikclistvales.add(f.getValue());
          system.debug('The f.getValue() is :' +f.getValue());           
           }
            
            list<String> nonVal = new list<string>();
            nonVal.add('-None-');
             
           
            price = new list<string>(); 
            category = new list<string>();
            thickness = new list<string>();
            length = new list<string>();
            width = new list<string>();
            packTypeTons = new list<string>();
            product =  new list<string>();
            
            
          //  category.addAll(nonVal);
            product.addAll(nonVal);
            thickness.addAll(nonVal);
            length.addAll(nonVal);
            width.addAll(nonVal);
            packTypeTons.addAll(nonVal);
            price.addAll(nonVal);
              category.addAll(pikclistvales);
            price.add('List');
            price.add('Tier');
            price.add('Special');
            price.add('Search');
           // price.add(new list{'List','Tier','Special','Search'});
        }
       
        
      @AuraEnabled  public integer serialNo;
      @AuraEnabled public string Name;
      @AuraEnabled public string enqLineItemsWrapper;
       @AuraEnabled public list<string> category;
       @AuraEnabled public list<string> product;
       @AuraEnabled public list<string> thickness;
       @AuraEnabled public list<string> length;
       @AuraEnabled public list<string> width;
       @AuraEnabled public list<string> packTypeTons;
       @AuraEnabled public string cases;
       @AuraEnabled public string orderQuanitity;
       @AuraEnabled public string UOM;
       @AuraEnabled public string listPriceCst;
       @AuraEnabled public string basePrice;
       @AuraEnabled public list<string> price;
       @AuraEnabled public string sp_requestQuanitity;
       @AuraEnabled public string sp_priceRequestCst;
       @AuraEnabled public string validFrom;
       @AuraEnabled public string validTo;
       @AuraEnabled public string remarks;
       @AuraEnabled public string Standardpriceperunit;
       @AuraEnabled public string recordId; 
        @AuraEnabled public string price3; 
        @AuraEnabled public string category3; 
        @AuraEnabled public string thickness3; 
        @AuraEnabled public string packTypeTons3; 
         @AuraEnabled public string length3; 
        @AuraEnabled public string product3; 
        @auraEnabled public string priceRequest;
        @auraEnabled public string glassDirect;
        @auraEnabled public string Required_Price_per_Sq_M;
        @auraEnabled public boolean submitRec;
        @auraEnabled public boolean OldSubmitted;
        @auraEnabled public string LstInvRate;
        
        // This is used to avoid duplicate line item in the component. 
       // @AuraEnabled public string category4;
        
        
    }
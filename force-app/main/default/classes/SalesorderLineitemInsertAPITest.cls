/* Created By: Rajiv Kumar Singh
Created Date: 14/06/2017
Class Name: SalesorderLineitemInsertAPITest
Description : Test class for SalesorderLineitemInsertAPI
Last Modified By :Rajiv Kumar Singh
Last Modified Date: 14/06/2017
*/

@IsTest
private class SalesorderLineitemInsertAPITest{
    
    static testMethod void testPostMethod(){
        String JSONMsg = '[{"SalesorderNum":"3321","ReferenceNumber":"True","OrderStatus":"packed","BillToSAP":"12345","ShipToSAP":"43234","SubFamily":"2","Coating":"2","Tint":"2","laminate":"2","pattern":"2","ThicknessCode":"2","Length":"2","Width":"2"}]';
        RestRequest request = new RestRequest();
        request.requestUri ='/services/apexrest/SalesorderLineItemAPI';
        request.httpMethod = 'POST';
        request.requestBody = Blob.valueof(JSONMsg);
        RestContext.request = request;
        RestContext.response = new RestResponse();
        SalesorderLineitemInsertAPI.doHandleInsertRequest();
        
        JSONMsg = '[1';
        request = new RestRequest();
        request.requestUri ='/services/apexrest/SalesorderLineItemAPI';
        request.httpMethod = 'POST';
        request.requestBody = Blob.valueof(JSONMsg);
        RestContext.request = request;
        RestContext.response = new RestResponse();
        SalesorderLineitemInsertAPI.doHandleInsertRequest();
        
        request = new RestRequest();
        request.requestUri ='/services/apexrest/SalesorderLineItemAPI';
        request.httpMethod = 'POST';
        request.requestBody = Blob.valueof('');
        RestContext.request = request;
        RestContext.response = new RestResponse();
        SalesorderLineitemInsertAPI.doHandleInsertRequest();
    }
}
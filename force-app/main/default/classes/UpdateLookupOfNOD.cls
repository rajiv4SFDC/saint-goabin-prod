/* Created By         : Rajiv Kumar Singh
   Created Date       : 11/06/2018
   Class Name         : UpdateLookupOfNOD
   Description        : update the particular Account record lookup field to NOD
   Last Modified By   : Rajiv Kumar Singh
   Last Modified Date : 11/06/2018
*/
public class UpdateLookupOfNOD{
  @InvocableMethod(label='UpdateLookupOfNOD' description='update the particular Account record lookup field to NOD')
  public static void UpdateLookupValue(List<Id> NODIds)
    {   
       map<string,string> NODWithAccountMap = new map<string,string>();  
       set<string> presentSAPCodesSet = new set<String>();
       list<NODs__c > NewList = new list<NODs__c>(); 
       list<NODs__c > FinalListToUpdate = new list<NODs__c>();

       for(NODs__c ptp:[select id,Account__c,SAP_Code__c from NODs__c where id in:NODIds])
       {
         if(ptp.SAP_Code__c != null)
          {
            presentSAPCodesSet.add(ptp.SAP_Code__c);      
            NewList.add(ptp);
            system.debug('The ptp.id is: ' +ptp.id);
          }     
       }
       system.debug('The NewList is: ' +NewList+'......'+presentSAPCodesSet);
       if(presentSAPCodesSet.size()>0)
       {
          for(Account acc:[select id,SAP_Code__c  from Account where SAP_Code__c in:presentSAPCodesSet])
           {
               NODWithAccountMap.put(acc.SAP_Code__c,acc.id);
           }
           system.debug('The NODWithAccountMap is: ' +NODWithAccountMap);
           for(NODs__c ptp:NewList)
           {
               ptp.Account__c = NODWithAccountMap.get(ptp.SAP_Code__c);
               system.debug('The  ptp.Account__c is: ' + ptp.Account__c);
               FinalListToUpdate.add(ptp);
           }
       }
       if(!FinalListToUpdate.isEmpty() && FinalListToUpdate.size() > 0) {
            update FinalListToUpdate;
       }
    }
}
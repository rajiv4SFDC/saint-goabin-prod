@isTest
public class TestOpportunityTeamTrigger {
    
    public static String CRON_EXP = '0 0 17 1/1 * ? *';

    @testSetup 
    public static void createTestData(){
        TestDataFactory.createCustomSetting();
        TestDataFactory.createTestProductData();
        TestDataFactory.createTestStateAndCityRecordData();
        TestDataFactory.createTestCatalogueBudgetData();
        TestDataFactory.createTestCampaignRecordData();
        TestDataFactory.createTestAccountData();
        TestDataFactory.createTestProjectData();
        TestDataFactory.createTestOpportunityData();
        TestDataFactory.createTestOpportunityProductData();
        TestDataFactory.createTestPriceRequestData();
        TestDataFactory.createTestProductForecateData();
        TestDataFactory.createTestForecateInvoiceData();
        TestDataFactory.createTestMockupRecordData();
        TestDataFactory.createTestInvoiceRecordData();
        TestDataFactory.createTestTargetActualRecordData();
    }

    @isTest
    public static void createOpportunityTeamMember(){
        Test.StartTest();
        List<Opportunity> lstOfOppty = [SELECT id,OwnerId FROM Opportunity LIMIT 2];
        List<User> lstOfuserKAM = [select id from User where UserRole.Name LIKE '%DEL1%' OR UserRole.Name LIKE '%MUM1%' LIMIT 2];

        /* Add Team member without updating Owner Sharing Percentage  */
        try{
            OpportunityTeamMember opptymemErr = new OpportunityTeamMember();
            opptymemErr.OpportunityId = lstOfOppty.get(0).Id;
            opptymemErr.UserId = lstOfuserKAM.get(0).Id;
            opptymemErr.TeamMemberRole = 'Team Member';
            opptymemErr.OpportunityAccessLevel = 'Read';
            opptymemErr.Share_pct__c = 30;

            OpportunityTeamMember opptymemErr2 = new OpportunityTeamMember();
            opptymemErr2.OpportunityId = lstOfOppty.get(0).Id;
            opptymemErr2.UserId = lstOfuserKAM.get(1).Id;
            opptymemErr2.TeamMemberRole = 'Team Member';
            opptymemErr2.OpportunityAccessLevel = 'Read';
            opptymemErr2.Share_pct__c = 30;

            List<OpportunityTeamMember> lstOfTeamMemberInsert = new List<OpportunityTeamMember>();
            lstOfTeamMemberInsert.add(opptymemErr);
            lstOfTeamMemberInsert.add(opptymemErr2);
            insert lstOfTeamMemberInsert;

        }catch(Exception e){

        }
        
        /* Update Owner Sharing Percentage */
        List<OpportunityTeamMember> lstOfOwnerMember = [SELECT id,OpportunityId,Share_pct__c
                                            FROM OpportunityTeamMember 
                                            where OpportunityId IN : lstOfOppty LIMIT 2];
        
        lstOfOwnerMember.get(0).Share_pct__c = 70;
        lstOfOwnerMember.get(1).Share_pct__c = 70;
        
        update lstOfOwnerMember;

        /* Add Team member on updated Owner Sharing Percentage  */
            OpportunityTeamMember opptymemSucc = new OpportunityTeamMember();
            opptymemSucc.OpportunityId = lstOfOppty.get(0).Id;
            opptymemSucc.UserId = lstOfuserKAM.get(0).Id;
            opptymemSucc.TeamMemberRole = 'Team Member';
            opptymemSucc.OpportunityAccessLevel = 'Read';
            opptymemSucc.Share_pct__c = 15;

            OpportunityTeamMember opptymemSucc2 = new OpportunityTeamMember();
            opptymemSucc2.OpportunityId = lstOfOppty.get(0).Id;
            opptymemSucc2.UserId = lstOfuserKAM.get(1).Id;
            opptymemSucc2.TeamMemberRole = 'Team Member';
            opptymemSucc2.OpportunityAccessLevel = 'Read';
            opptymemSucc2.Share_pct__c = 15;

            List<OpportunityTeamMember> lstOfTeamMemberInsertSucc = new List<OpportunityTeamMember>();
            lstOfTeamMemberInsertSucc.add(opptymemSucc);
            lstOfTeamMemberInsertSucc.add(opptymemSucc2);
            insert lstOfTeamMemberInsertSucc;

        /* Update Team member on updated Owner Sharing Percentage  */   
            try{
                OpportunityTeamMember opptymem1 = lstOfTeamMemberInsertSucc.get(0);
                opptymem1.Share_pct__c = 30;
                OpportunityTeamMember opptymem2 = lstOfTeamMemberInsertSucc.get(1);
                opptymem2.Share_pct__c = 30;
                Test.StopTest();
                List<OpportunityTeamMember> lstOfTeamMemberUpdateError = new List<OpportunityTeamMember>();
                lstOfTeamMemberUpdateError.add(opptymem1);
                lstOfTeamMemberUpdateError.add(opptymem2);
                update lstOfTeamMemberUpdateError;
                
            }catch(Exception e){

            }

        /* Change Opportunity Owner */  
            try{
                Id opptyId = lstOfOppty.get(0).Id;
                Id opptyOwnerId = lstOfOppty.get(0).OwnerId;
                OpportunityTeamMember opptyTeamRec =  [SELECT id,OpportunityId,UserId from OpportunityTeamMember 
                                                       where OpportunityId=:opptyId AND UserId=:opptyOwnerId LIMIT 1];
                //lstOfOppty.get(0).OwnerId = lstOfuserKAM.get(1).Id;
                delete opptyTeamRec;
                
                
                //update lstOfOppty;
                
            }catch(Exception e){
                system.debug('cus exp : '+ e);
            }
    }
    
    @isTest
    public static void invokeOpportunityTeamMemberBatch(){
        Test.startTest();

        OpportunityTeamBatch opB = new OpportunityTeamBatch();
        Database.executeBatch(opB);

        Test.stopTest();
    }
    
     @isTest
    public static void invokeOpportunityTeamMemberSchedule(){
        Test.startTest();

        String jobId = System.schedule('OpptyTeamBatch', CRON_EXP, new OpportunityTeamBatchScheduler()); 

        Test.stopTest();
    }
    
}
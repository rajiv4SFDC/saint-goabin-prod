/* Created By         : Rajiv Kumar Singh
   Created Date       : 16/12/2018
   Class Name         : UpdateStockLookupInvoiceTest
   Description        : 
   Last Modified By   : Rajiv Kumar Singh
   Last Modified Date : 16/12/2018
*/
@isTest
private class UpdateStockLookupInvoiceTest
{ 
 // @istest
   private testmethod static void testMethod1()
   {
      Product_Stock__c ps = new Product_Stock__c();
      ps.name ='Chennai';
      ps.Plant_Code__c='1234';
      insert ps;
      
      Invoice__c ptp = new Invoice__c();
      ptp.name = 'SF1234';
      ptp.Product_Code__c = '401-518-000-000-000';
      ptp.Thickness__c = '0600';
      ptp.Product_Stock__c=ps.id;
      ptp.Plant_location__c='1234';
      insert ptp;
      
      Invoice__c pt = [select id, Plant_location__c from Invoice__c where id =:ptp.id];
      
    //  System.assertEquals(pt.product__c,p1.id);
      list<Id> ptrList = new list<Id>();
      ptrList.add(pt.id);
      
      UpdateStockLookupInvoice.UpdateLookupValue(ptrList);
   }
}
public class ProductsWrapper{
    @AuraEnabled public Boolean isSelected{set;get;}
    @AuraEnabled public String prdName{set;get;}
    @AuraEnabled public String prdCode{Set;get;}
    @AuraEnabled public Integer prdQuantitiy{set;get;}
    @AuraEnabled public String prdArticlenumber{set;get;}
    @AuraEnabled public String prdParentId{set;get;}
    @AuraEnabled public Id oppProductLineItemId{set;get;}
}
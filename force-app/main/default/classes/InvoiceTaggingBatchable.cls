global class InvoiceTaggingBatchable implements Database.Batchable<sObject> {
    
    global Database.QueryLocator start(Database.BatchableContext BC){
        String query = 'SELECT Id,Invoice_type__c FROM Invoice__c WHERE Tagged__c = false AND To_Be_Processed__c = true';
        return Database.getQueryLocator(query);
    }

    global void execute(Database.BatchableContext BC, List<sObject> scope){
        Set<Id> invoiceIdSet = new Set<Id>();
        set<id> invoiceIdSetForSc = new set<id>();
        System.debug('Invoice Tagging Batchable : Batch Size' + invoiceIdSet.size());
        for(Invoice__c inv : (List<Invoice__c>) scope) {
         
          //  system.debug('The name of inv is:' + inv.Id);
            
              if(inv.Invoice_type__c != null && inv.Invoice_type__c !='Shower Cubicles') 
            invoiceIdSet.add(inv.id);
            
            if(inv.Invoice_type__c != null && inv.Invoice_type__c =='Shower Cubicles')
            invoiceIdSetForSc.add(inv.id);
            
        }
        
        //Initiate the process of recalculation
        if(invoiceIdSet.size()>0)
        InvoiceAppropriationHelper.processInvoice(invoiceIdSet);
        
        if(invoiceIdSetForSc.size()>0)
        InvoiceAppropriationHelperForSc.processInvoice(invoiceIdSetForSc);
        
        system.debug('The invoiceIdSetForSc is:'+invoiceIdSetForSc+'.....'+invoiceIdSet);
        
    }

    global void finish(Database.BatchableContext BC){
        //Start Next Batch for Tagged but Not Processed Invoices
        Database.executeBatch(new InvoiceAppropriationBatchable());
    }
}
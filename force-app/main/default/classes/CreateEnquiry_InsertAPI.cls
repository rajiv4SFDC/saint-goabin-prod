/* Created By: Rajiv Kumar Singh
Created Date: 01/06/2018
Class Name: CreateEnquiry_InsertApi
Story : Enquiry flow from Glass Direct to SFDC
Last Modified By :Rajiv Kumar Singh
Last Modified Date: 13/06/2018
*/
@RestResource(urlMapping='/createEnquiry')
global class CreateEnquiry_InsertAPI {
    
    //Start of Post Method- Glass Direct enquiries will be posted and then the Json will be deserialized and Enquiry and Enquiry Line will be created.
    
    @HttpPost
    global static void doHandleInsertRequest(){ 
        string tempstr ='';
        
        List<Enquiry__c> enqList = new List<Enquiry__c>();  // List to hold enquiry records
        List<ChildWrapper> wrpList = new List<ChildWrapper>(); // List to hold the enquiry child records.
        Map<String,String> mapChildItems = new Map<String,String>();
        List<ParentWrapper> wrapList = new List<ParentWrapper>();
        RestRequest Req = RestContext.request;
        RestResponse res = RestContext.response;
        String body = Req.requestBody.toString().trim();
        
        if(body != null && body != '') {
            
            try {
            wrapList = (List<ParentWrapper>)JSON.deserialize(body, List<ParentWrapper>.class);  // Deserializing the Json received.
            
            //system.debug('body::'+body);
            system.debug('wrapList::'+wrapList);
            for(ParentWrapper enqs : wrapList){   // Iterarting the parent record list
                if(enqs != null && enqs.childInfo != null) {
                    wrpList.addAll(enqs.childInfo); 
                    
                    Enquiry__c enq = new Enquiry__c();      // Mapping the Json attributes to Enquiry fields
                    if(enqs.ReferenceId!=null && enqs.ReferenceId!=''){
                        enq.GD_Reference_Number__c = string.valueOf(enqs.ReferenceId);       
                    }
                    if(enqs.AccSAPCode!=null && enqs.AccSAPCode!=''){
                        enq.SAP_Code__c = enqs.AccSAPCode;       
                    }
                    if(enqs.ShipToSAPCode!=null && enqs.ShipToSAPCode!=''){
                        enq.Ship_To_SAP_Code__c = enqs.ShipToSAPCode;       
                    }
                    if(enqs.Salesoffice!=null && enqs.Salesoffice!=''){
                        enq.Sales_Office__c = enqs.Salesoffice;       
                    }
                    if(enqs.EnqSource!=null && enqs.EnqSource!='' && enqs.EnqSource=='GD'){
                        enq.Glass_Direct_Enquiry__c = True;      
                    }
                    if(enqs.EnqSource!=null && enqs.EnqSource!='' && enqs.EnqSource=='SAP'){
                        enq.SFDC_Enquiry__c = True;      
                    }
                    if(enqs.ExpDate!=null && enqs.ExpDate!='' ){
                         
                        String[] dateVal = enqs.ExpDate.split('/');
                        Date dt = Date.newInstance(Integer.valueOf(dateVal[0]), Integer.valueOf(dateVal[1]), Integer.valueOf(dateVal[2]));
                        //String strConvertedDate = dt.format('yyyy-MM-dd HH:mm:ss', 'Asia/Kolkata'); //India timezone
                        enq.Expected_Date_of_Delivery__c = dt;
                        system.debug('dt:::'+dt);
                             
                    }
                    if(enqs.EnqStatus!=null && enqs.EnqStatus!=''){
                        enq.Enquiry_Status__c = enqs.EnqStatus;      
                    }
                    
                    enqList.add(enq);
                }
            }
            Schema.SObjectField ftoken = Enquiry__c.Fields.GD_Reference_Number__c;
            Database.UpsertResult[] srList = Database.upsert(enqList,ftoken,false);      
            Map<String, Object> res1 = new Map<String, Object>();
            res1.put('ENQUIRY', srList);
            for(Enquiry__c enq : enqList){     // Iterate the inserted parent child 
                mapChildItems.put(enq.GD_Reference_Number__c, enq.Id);
            }
            
            List<Enquiry_Line_Item__c > enqChildListToInsert = new List<Enquiry_Line_Item__c >();
            List<Enquiry_Line_Item__c > enqChildListToUpdate = new List<Enquiry_Line_Item__c >();
            
            
            for(ChildWrapper wrp : wrpList){   //Iterating the child records
                Enquiry_Line_Item__c enql = new Enquiry_Line_Item__c();  // Instance for the child 
                
                if(wrp.SubFamilyCode!=null && wrp.SubFamilyCode!='' && wrp.CoatingCode!=null && wrp.CoatingCode!='' && wrp.TintCode!=null && wrp.TintCode!='' && wrp.LaminateCode!=null && wrp.LaminateCode!='' && wrp.PatternCode!=null && wrp.PatternCode!='' && wrp.ThicknessCode!=null && wrp.ThicknessCode!=''){
                    enql.Product_Item_Code__c = wrp.SubFamilyCode+'-'+wrp.CoatingCode+'-'+wrp.TintCode+'-'+wrp.LaminateCode+'-'+wrp.PatternCode+'-'+wrp.ThicknessCode;
                }
                if(wrp.SubFamilyCode!=null && wrp.SubFamilyCode!=''){
                    enql.SUB_FAMILY__c = wrp.SubFamilyCode;
                }
                if(wrp.CoatingCode!=null && wrp.CoatingCode!=''){
                    enql.Coating__c = wrp.CoatingCode;
                }
                if(wrp.TintCode!=null && wrp.TintCode!=''){
                    enql.Tint__c = wrp.TintCode;
                }
                if(wrp.LaminateCode!=null && wrp.LaminateCode!=''){
                    enql.Laminate__c = wrp.LaminateCode;
                }
                if(wrp.PatternCode!=null && wrp.PatternCode!=''){
                    enql.Pattern__c = wrp.PatternCode;
                }
                if(wrp.ThicknessCode!=null && wrp.ThicknessCode!=''){
                    enql.Thickness__c = wrp.ThicknessCode;
                    enql.Product_Thickness__c = wrp.ThicknessCode;
                }
                
                if(wrp.Length!=null && wrp.Length!='' && wrp.Width!=null && wrp.Width!=''){
                    enql.Product_Length__c = wrp.Length+'-'+wrp.Width;
                }
                if(wrp.Width!=null && wrp.Width!=''){
                    enql.Product_Width__c = wrp.Width;
                }
                if(wrp.Length!=null && wrp.Length!=''){
                    enql.Length__c = wrp.Length;
                }
                if(wrp.Packtype!=null){
                    enql.Pak_Type_Tons__c = string.valueof(wrp.Packtype);
                }
                if(wrp.cases!=null && wrp.Packtype!=null){
                    enql.Order_quantity__c = wrp.Packtype * wrp.cases;
                }
                if(wrp.cases!=null){
                    enql.Cases__c = string.valueof(wrp.cases);
                }
                if(wrp.EnqLineSource!=null && wrp.EnqLineSource!='' && wrp.EnqLineSource=='GD'){
                    enql.Glass_Direct_Line_Item__c =True;
                }
                if(wrp.EnqLineSource!=null && wrp.EnqLineSource!='' && wrp.EnqLineSource=='SAP'){
                    enql.SAP_Line_Item__c = True;
                }
                if(wrp.EnqLineUnique!=null && wrp.EnqLineUnique!=''){
                    enql.Reference_Number__c = string.valueOf(wrp.EnqLineUnique);
                }
                
                enql.Enquiry__c = (mapChildItems.containsKey(wrp.ReferenceId))?(mapChildItems.get(wrp.ReferenceId)):(null);
                    
                    enqChildListToInsert.add(enql); 
                
            }
            
            Schema.SObjectField ftokens = Enquiry_Line_Item__c.Fields.Reference_Number__c;
            Database.UpsertResult[] srLists = Database.upsert(enqChildListToInsert,ftokens,false); 
            res1.put('ENQUIRY Line', srLists);
            
            List<Enquiry_Line_Item__c > insertedList = new List<Enquiry_Line_Item__c >();
            List<Enquiry_Line_Item__c > EnquiryLineitemsToUpdate = new List<Enquiry_Line_Item__c >();
            set<id> ElIdsSet = new set<id>();
            
            
            for(Database.UpsertResult  el:srLists)
            {
                ElIdsSet.add(el.getId());
            }
            
            System.debug('Set values'+ElIdsSet);
          /****************************************************************************************************************************************  
            map<string,Enquiry_Line_Item__c> enquireyLineMap = new map<string,Enquiry_Line_Item__c>();
            map<string,product2>  prdMap = new map<string,product2>();
            for(Enquiry_Line_Item__c el:[select id,name,Product_Item_Code__c,Product_Name__c,Product_category__c from Enquiry_Line_Item__c where id in:ElIdsSet])
            {
                enquireyLineMap.put(el.Product_Item_Code__c,el); 
                System.debug(' values'+el.Product_Item_Code__c);             
            }
            System.debug('map 2 values'+enquireyLineMap);
            
            for(product2 pd:[select id,name,type__c,external_id__c  from product2 where external_id__c in :enquireyLineMap.keyset()])
            {
                prdMap.put(pd.external_id__c,pd);
            }
            system.debug('Map values'+prdMap);
            
            for(Enquiry_Line_Item__c el:enquireyLineMap.values())
            {
                Enquiry_Line_Item__c eli2 = new Enquiry_Line_Item__c();
                eli2.id = el.Id;
                if(prdMap.get(el.Product_Item_Code__c ) != null) {
                    eli2.Product_Name__c = prdMap.get(el.Product_Item_Code__c).Name;
                    eli2.Product_category__c =  prdMap.get(el.Product_Item_Code__c ).type__c;
                }
                EnquiryLineitemsToUpdate.add(eli2);
            }
            
            if(EnquiryLineitemsToUpdate.size()>0)
                update EnquiryLineitemsToUpdate;
          ****************************************************************************************************************************************/ 
           map<string,list<Enquiry_Line_Item__c>> enquireyLineMap = new map<string,list<Enquiry_Line_Item__c>>();
 list<Enquiry_Line_Item__c> enqLineList = new list<Enquiry_Line_Item__c>();
            map<string,product2>  prdMap = new map<string,product2>();
            for(Enquiry_Line_Item__c el:[select id,name,Product_Item_Code__c,Product_Name__c,Product_category__c from Enquiry_Line_Item__c where id in:ElIdsSet])
            {
              if(enquireyLineMap.containsKey(el.Product_Item_Code__c))
                enquireyLineMap.get(el.Product_Item_Code__c).add(el); 
              else
                    enquireyLineMap.put(el.Product_Item_Code__c,new list<Enquiry_Line_Item__c>{el});
                    
                System.debug(' values'+el.Product_Item_Code__c);      
              enqLineList.add(el); 
            }
            System.debug('map 2 values'+enquireyLineMap);
            
            for(product2 pd:[select id,name,type__c,external_id__c  from product2 where external_id__c in :enquireyLineMap.keyset()])
            {
                prdMap.put(pd.external_id__c,pd);
            }
            system.debug('Map values'+prdMap);
            
            for(Enquiry_Line_Item__c el:enqLineList)
            {
                Enquiry_Line_Item__c eli2 = new Enquiry_Line_Item__c();
                eli2.id = el.Id;
                if(prdMap.get(el.Product_Item_Code__c ) != null) {
                    eli2.Product_Name__c = prdMap.get(el.Product_Item_Code__c).Name;
                    eli2.Product_category__c =  prdMap.get(el.Product_Item_Code__c ).type__c;
                }
                EnquiryLineitemsToUpdate.add(eli2);
            }
            
            if(EnquiryLineitemsToUpdate.size()>0)
                update EnquiryLineitemsToUpdate;
            res.statusCode = 200;
            res.responseBody = Blob.valueOf(String.valueof(res1));
            
           } catch (Exception e) {
            SYSTEM.debug('MESSGA'+e.getMessage()+'....'+e.getLineNumber());
            CommonUtilitiesHandler.handleExceptions(e);
            res.statusCode = 500;
            res.responseBody = Blob.valueOf(e.getMessage()+'.....'+e.getLineNumber()+'...'+tempstr );
            }
        } else {
            res.statusCode = 500;
            res.responseBody = Blob.valueOf('Invalid Request');
        } 
        
    }//end of doHandleIsertRequest()
    
    public class ParentWrapper{   // parent Wrapper
        public String ReferenceId{get;set;}
        public String AccSAPCode{get;set;}
        public String ShipToSAPCode{get;set;}
        public String Salesoffice{get;set;}
        public String EnqSource{get;set;}
        public String EnqStatus{get;set;}
        public String ExpDate{get;set;}
        
        public List<ChildWrapper> childInfo{get;set;} // List of child wrapper
    }
    
    public class ChildWrapper{  // Child Wrapper
        public String ReferenceId{get;set;}
        public String SubFamilyCode{get;set;}
        public String CoatingCode{get;set;}
        public String TintCode{get;set;}
        public String LaminateCode{get;set;}
        public String PatternCode{get;set;}
        public String ThicknessCode{get;set;}
        public String Length{get;set;}
        public String Width{get;set;}
        public Decimal Packtype{get;set;}
        public Decimal cases{get;set;}
        public String EnqLineSource{get;set;}
        public String EnqLineUnique{get;set;}
        
    }
}
public class Opp_Product_with_AccountTrigger_Handler{
 public static boolean recurssionControlCheck = false;
 
 public static void afterInsertHandler(list<Opportunity_Product_with_Account__c> newRecords)
 {
   list<Opportunity_Product_with_Account__c> opwList = new list<Opportunity_Product_with_Account__c>();
    for(Opportunity_Product_with_Account__c opw:newRecords)
    {
       if(opw.Account__c!= null && opw.Opportunity_Product__c!= null)
        opwList.add(opw);
       
    }
    if(opwList.size()>0)
    autoPopulateProductTier(opwList);
 }
 
 public static void afterUpdateHandler(list<Opportunity_Product_with_Account__c>newRecords, map<id,Opportunity_Product_with_Account__c>OldMap)
 {
   list<Opportunity_Product_with_Account__c> opwList = new list<Opportunity_Product_with_Account__c>();
    for(Opportunity_Product_with_Account__c opw:newRecords)
    {
     // Only if the account name field is changed (account__c) then only trigger will update the Tier pricing field.
       if(OldMap.get(opw.id).Account__c!= null && opw.Account__c!= null && opw.Opportunity_Product__c!= null && OldMap.get(opw.id).Account__c != opw.Account__c )
        opwList.add(opw);
       
    }
    if(opwList.size()>0)
    autoPopulateProductTier(opwList);
 }
 
 // Below method is used for auto populating the "Product Tier Pricing" field when ever the user created or edited the record. 
 
 public static void autoPopulateProductTier(list<Opportunity_Product_with_Account__c> RecsToUpdate)
 {
   // populate the "Product Tier Pricing" field basing upon the below conditions. (product category + sales office + customer code)
   //   1. product category = "Inspire"
   //   2. sales office (Account.Sales_Office_ERP__c = Opportunity_Product_line_items__r.product2.Business_Location__c) 
   //   3. Tier should match with Opportunity_Product_line_items__c record tier and product2 tier category should match the specific fields of account    with specific fields of the product2 record.
       // Account.Reflectasol_Tier -> Product2.Sub_Family__c
    //  Account.Mirror_Tier -> Product2.Type__c
    //  Account.Interior_Tier -> Product2.Type__c
    //  Account.Clear_Tint_Tier -> Product2.Tint__c
  
  
    set<id> opportunityProductwithAccsIdSet = new set<id>();  
  set<id> opportunityProductIdSet = new set<id>();  
  set<id> accountIdsSet = new set<id>();
  set<id> productsIdsSet = new set<id>();
  Map<string,string> Opportunity_Product_with_Account_withProduct2Map = new map<string,string>();
     map<id,account> accMap = new map<id,account>();
   map<id,product2> productsMap = new  map<id,product2>();
   map<id,Product_Tier_Pricing__c> product2IdWithProduct_Tier_PricingMap = new map<id,Product_Tier_Pricing__c>();
   list<Opportunity_Product_with_Account__c> opwaToUpdate = new list<Opportunity_Product_with_Account__c>();

  
  // Initially get the account records and product2 records information. 
  for(Opportunity_Product_with_Account__c opw:RecsToUpdate)
   {
       opportunityProductIdSet.add(opw.Opportunity_Product__c);
       accountIdsSet.add(opw.Account__c);
       opportunityProductwithAccsIdSet.add(opw.id);
   }
   
   // In the above we got only Opportunity_Product__c ids.  But we need the product2 records information. 
   for(Opportunity_Product__c op1:[select id,name,Product__c from Opportunity_Product__c where id in:opportunityProductIdSet])
   {
       productsIdsSet.add(op1.Product__c);
       Opportunity_Product_with_Account_withProduct2Map.put(op1.id,op1.Product__c);
   }
   
 set<string> businessLocationsSet = new set<string>();
 set<string> tiersSet = new set<string>();
 
   
   for(account a1:[select id,name,Sales_Office_ERP__c,Reflectasol_Tier__c,Mirror_Tier__c,Interior_Tier__c,Clear_Tint_Tier__c from account where id in:accountIdsSet])
   {
      accMap.put(a1.id,a1);
      
      if(a1.Sales_Office_ERP__c != null && a1.Sales_Office_ERP__c!='')
      businessLocationsSet.add(a1.Sales_Office_ERP__c);
      
      if(a1.Reflectasol_Tier__c!= null && a1.Reflectasol_Tier__c!='' || Test.isRunningTest())
      tiersSet.add(a1.Reflectasol_Tier__c);
      
      if(a1.Mirror_Tier__c!= null && a1.Mirror_Tier__c!=''  || Test.isRunningTest())
      tiersSet.add(a1.Mirror_Tier__c);
      
      if(a1.Interior_Tier__c!= null && a1.Interior_Tier__c!='' || Test.isRunningTest())
      tiersSet.add(a1.Interior_Tier__c);
      
      if(a1.Clear_Tint_Tier__c!= null && a1.Clear_Tint_Tier__c!='' || Test.isRunningTest())
      tiersSet.add(a1.Clear_Tint_Tier__c);
     
   }
   

   
   for(Product_Tier_Pricing__c ptr:[select id,name,Tier_Name__c,Business_Location__c, Product__r.id, Product__r.name,Product__r.Sub_Family__c, Product__r.Type__c, Product__r.Tint__c from Product_Tier_Pricing__c  where Product__r.id in:productsIdsSet AND Tier_Name__c in:tiersSet AND Business_Location__c in:businessLocationsSet])
   {
      product2IdWithProduct_Tier_PricingMap.put(ptr.Product__r.id,ptr);
   }
   
      system.debug('The in:productsIdsSet :'+productsIdsSet+'.....'+tiersSet+'......'+businessLocationsSet+'......'+opportunityProductwithAccsIdSet);

   // now Fetch the particular "Product Tier Pricing" record information to query. 
   for(Opportunity_Product_with_Account__c  opw1:[select id,name,Opportunity_Product__c,Account__c,Product_Tier_Pricing__c from Opportunity_Product_with_Account__c  where id in:opportunityProductwithAccsIdSet])
    {
       account acc = accMap.get(opw1.account__c);
       Product_Tier_Pricing__c ptr = product2IdWithProduct_Tier_PricingMap.get(Opportunity_Product_with_Account_withProduct2Map.get(opw1.Opportunity_Product__c));
       
       system.debug('the inside is:'+acc+'....'+ptr );
       
       // Account.Reflectasol_Tier -> Product2.Sub_Family__c  checking here. 
       if(ptr != null && acc != null && acc.Sales_Office_ERP__c!= null && ptr.Business_Location__c!= null && acc.Reflectasol_Tier__c!= null && ptr.Product__r.Type__c!= null && acc.Sales_Office_ERP__c == ptr.Business_Location__c && acc.Reflectasol_Tier__c!='' && acc.Reflectasol_Tier__c == ptr.Tier_Name__c && ptr.Product__r.Type__c.equalsIgnoreCase('REFLECTASOL'))
        {
           opw1.Product_Tier_Pricing__c = ptr.id;
           opwaToUpdate.add(opw1);
        }
        
         //  Account.Mirror_Tier -> Product2.Type__c  checking here. 
    else    if(ptr != null && acc != null && acc.Sales_Office_ERP__c!= null && ptr.Business_Location__c!= null && acc.Mirror_Tier__c!= null && ptr.Product__r.Type__c!= null && acc.Sales_Office_ERP__c == ptr.Business_Location__c && acc.Mirror_Tier__c!='' && acc.Mirror_Tier__c == ptr.Tier_Name__c && ptr.Product__r.Type__c.equalsIgnoreCase('Mirror'))
        {
           opw1.Product_Tier_Pricing__c = ptr.id;
           opwaToUpdate.add(opw1);
        }
        
         //  Account.Interior_Tier -> Product2.Type__c  checking here. 
    else    if(ptr != null && acc != null && acc.Sales_Office_ERP__c!= null && ptr.Business_Location__c!= null && acc.Interior_Tier__c!= null && ptr.Product__r.Type__c!= null && acc.Sales_Office_ERP__c == ptr.Business_Location__c && acc.Interior_Tier__c!='' && acc.Interior_Tier__c == ptr.Tier_Name__c && ptr.Product__r.Type__c.equalsIgnoreCase('Interior'))
        {
           opw1.Product_Tier_Pricing__c = ptr.id;
           opwaToUpdate.add(opw1);
        }
        
        //  Account.Clear_Tint_Tier -> Product2.Type__c  checking here. 
    else    if(ptr != null && acc != null && acc.Sales_Office_ERP__c!= null && ptr.Business_Location__c!= null && acc.Clear_Tint_Tier__c!= null && ptr.Product__r.Type__c!= null && acc.Sales_Office_ERP__c == ptr.Business_Location__c && acc.Clear_Tint_Tier__c!='' && acc.Clear_Tint_Tier__c == ptr.Tier_Name__c && ptr.Product__r.Type__c.equalsIgnoreCase('CLEAR'))
        {
           opw1.Product_Tier_Pricing__c = ptr.id;
           opwaToUpdate.add(opw1);
        }
        
    }
    
    if(opwaToUpdate.size()>0)
      update opwaToUpdate;
 /***********      Customer asked to comment this validation rule.
      for(Opportunity_Product_with_Account__c opw:RecsToUpdate)
      {
         if(opwaToUpdate.size()<=0)
           opw.addError('The account business location/Tier name is not matching with tier pricing');
      }
    ***************/
      
 }
}
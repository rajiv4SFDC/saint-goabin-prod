public class OpportunityTeamTriggerHandler extends TriggerHandler {

	public OpportunityTeamTriggerHandler() {}
    
    public override void beforeInsert() {
        system.debug('OpptyTeam before Insert Start');

        Boolean validateChk = new OpportunityTeamManager().checkSharingPctAmongTeamMemberOnInsert( (List<OpportunityTeamMember>)Trigger.new );
        /*if(validateChk){
            new OpportunityTeamManager().populateOpportunityRelatedDetailOnTeam( (List<OpportunityTeamMember>)Trigger.new );
        }*/
        system.debug('OpptyTeam before Insert End');
    }

    public override void beforeUpdate() {
        system.debug('OpptyTeam before Update Start');
        Boolean validateChk = new OpportunityTeamManager().checkSharingPctAmongTeamMemberOnUpdate( (List<OpportunityTeamMember>)Trigger.new );
        /*if(validateChk){
            new OpportunityTeamManager().populateOpportunityRelatedDetailOnTeam( (List<OpportunityTeamMember>)Trigger.new );
        }*/
        system.debug('OpptyTeam before update End');
    }

    public override void afterInsert() {
        system.debug('OpptyTeam After Insert Start');

        //new OpportunityTeamManager().createOpportunityProductTeamMember( (List<OpportunityTeamMember>)Trigger.new );
        new OpportunityTeamManager().checkRecalculateCheckboxOnOpptyFrmTeamOnInsert( (List<OpportunityTeamMember>)Trigger.new );
        system.debug('OpptyTeam After Insert End');
    }

    public override void afterUpdate() {
        system.debug('OpptyTeam After Update Start');
        //new OpportunityTeamManager().updateOpportunityProductTeamMember( (List<OpportunityTeamMember>)Trigger.new,
        //                                                                (Map<Id,OpportunityTeamMember>)Trigger.OldMap );
        new OpportunityTeamManager().checkRecalculateCheckboxOnOpptyFrmTeamOnUpdate( (List<OpportunityTeamMember>)Trigger.new,
                                                                                (Map<Id,OpportunityTeamMember>)Trigger.OldMap );
        system.debug('OpptyTeam After Update End');
    }

     public override void beforeDelete() {
        system.debug('OpptyTeam before Delete Start');
        new OpportunityTeamManager().checkForOnwerBeingDelFrmOpptyTeamMember( (List<OpportunityTeamMember>)Trigger.old );
        system.debug('OpptyTeam Before Delete End');
     }

     public override void afterDelete() {
         system.debug('OpptyTeam After Delete Start');
        //new OpportunityTeamManager().deleteRelatedOpptyProductTeamMember( (List<OpportunityTeamMember>)Trigger.old );
        new OpportunityTeamManager().checkRecalculateCheckboxOnOpptyFrmTeamOnInsert( (List<OpportunityTeamMember>)Trigger.old );
        system.debug('OpptyTeam After Delete End');
     }

}
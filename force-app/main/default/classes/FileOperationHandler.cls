public class FileOperationHandler {

	/*
	public static void restrictImageDeletion(List<ContentDocumentLink> lstOfConDocLink,
												Map<String,Boolean> mapOfContectType,
												Map<String,Boolean> mapOfsObjectName){

		Map<ID,ContentDocumentLink> mapOfContentDoclinkId = new Map<ID,ContentDocumentLink>();
		List<Id> setOfDocLinkId = new List<Id>();
		Map<ID,List<ID>> mapOfLinkedEntityIdAndLstOfConDocLinkId = new Map<ID,List<ID>>();

		for(ContentDocumentLink record : lstOfConDocLink){
			mapOfContentDoclinkId.put(record.Id,record);
			setOfDocLinkId.add(record.Id);
		}

		List<ContentDocumentLink> lstOfQueryConDocLink =[SELECT ID, LinkedEntityId,LinkedEntity.Type,
													     ContentDocumentId, ContentDocument.FileType
		                                              	 FROM ContentDocumentLink 
		                                              	 WHERE ID IN : setOfDocLinkId ];
                                             	 
        for(ContentDocumentLink record : lstOfQueryConDocLink){

        	if( mapOfsObjectName.containsKey(record.LinkedEntity.Type+'') &&
        		mapOfContectType.containsKey(record.ContentDocument.FileType+'') ){

        			if( mapOfLinkedEntityIdAndLstOfConDocLinkId.get(record.LinkedEntityId) == null ){
        				mapOfLinkedEntityIdAndLstOfConDocLinkId.put(record.LinkedEntityId,new List<ID>{record.ID});
        			}else{
        				List<Id> lstOfconDoclinkId = mapOfLinkedEntityIdAndLstOfConDocLinkId.get(record.LinkedEntityId);
        				lstOfconDoclinkId.add(record.ID);
        				mapOfLinkedEntityIdAndLstOfConDocLinkId.put(record.LinkedEntityId,lstOfconDoclinkId);
        			}
			}
        }


         List<Project__c> lstOfProject =  [SELECT id,Validated__c,Tagged_Image_Count__c
									       FROM Project__c 
									       WHERE ID IN : mapOfLinkedEntityIdAndLstOfConDocLinkId.keySet()];


		 if(lstOfProject.size() > 0){ 

		 	// Add error on Validated Projects 
		 	addErrorOnRecord(lstOfProject,mapOfLinkedEntityIdAndLstOfConDocLinkId,mapOfContentDoclinkId);
		 	// Update Image Count on Projects 
		 	decreaseImageCount(lstOfProject,mapOfLinkedEntityIdAndLstOfConDocLinkId);
		 }

    }
    */
    /*
	public static void addErrorOnRecord(List<Project__c> lstOfProject,
										Map<ID,List<ID>> mapOfLinkedEntityIdAndLstOfConDocLinkId,
										Map<ID,ContentDocumentLink> mapOfContentDoclinkId){

		List<Id> listOfConDocLink = new List<Id>();

		for( Project__c proj : lstOfProject){

			if(proj.Validated__c == true){
				
				listOfConDocLink.addAll( mapOfLinkedEntityIdAndLstOfConDocLinkId.get(proj.id) );
			}
		}

		for( ID idvar:listOfConDocLink ){

			ContentDocumentLink cdlinkRcd = mapOfContentDoclinkId.get( idvar );
   			cdlinkRcd.addError('Validated project images cannot be deleted');	
		}		
	} 
	*/
	/*
	public static void decreaseImageCount(List<Project__c> lstOfProject,
										Map<ID,List<ID>> mapOfLinkedEntityIdAndLstOfConDocLinkId){

		
		for( Project__c proj : lstOfProject ){

			if(proj.Validated__c != true){
				proj.Tagged_Image_Count__c = proj.Tagged_Image_Count__c - mapOfLinkedEntityIdAndLstOfConDocLinkId.get(proj.Id).size();
			}
		}
			// Update Project with Image count 
			update lstOfProject;
	}
	*/

	public static void increaseImageCount(List<ContentDocumentLink> lstOfConDocLink,
												Map<String,Boolean> mapOfContectType,
												Map<String,Boolean> mapOfsObjectName){


		Map<ID,ContentDocumentLink> mapOfContentDoclinkId = new Map<ID,ContentDocumentLink>();
		List<Id> setOfDocLinkId = new List<Id>();
		List<Id> setOfLinkedEntityId = new List<Id>();
		Map<ID,Integer> mapOfEntityLinkIdAndImgCount = new Map<Id,Integer>();
		Map<ID,Integer> mapOfEntityLinkIdAndDocCount = new Map<Id,Integer>();
		List<Project__c> listOfProject = new List<Project__c>(); 


		for(ContentDocumentLink record : lstOfConDocLink){
			mapOfContentDoclinkId.put(record.Id,record);
			setOfDocLinkId.add(record.Id);
		}

		List<ContentDocumentLink> lstOfQueryConDocLink =[SELECT ID, LinkedEntityId,LinkedEntity.Type,
													     ContentDocumentId, ContentDocument.FileType
		                                              	 FROM ContentDocumentLink 
		                                              	 WHERE ID IN : setOfDocLinkId ];



		for(ContentDocumentLink record : lstOfQueryConDocLink){
        	if( mapOfsObjectName.containsKey(record.LinkedEntity.Type+'') &&
        		mapOfContectType.containsKey(record.ContentDocument.FileType+'') ){

        			if( mapOfEntityLinkIdAndImgCount.get(record.LinkedEntityId) == null ){
        				mapOfEntityLinkIdAndImgCount.put(record.LinkedEntityId,1);
        			}else{
        				Integer c = mapOfEntityLinkIdAndImgCount.get(record.LinkedEntityId);
        				c++;
        				mapOfEntityLinkIdAndImgCount.put(record.LinkedEntityId,c);
        			}

			}else if( record.LinkedEntity.Type == 'Mock_Up_Request__c' ){
				
				if( mapOfEntityLinkIdAndDocCount.get(record.LinkedEntityId) == null ){
        				mapOfEntityLinkIdAndDocCount.put(record.LinkedEntityId,1);
        			}else{
        				Integer c = mapOfEntityLinkIdAndDocCount.get(record.LinkedEntityId);
        				c++;
        				mapOfEntityLinkIdAndDocCount.put(record.LinkedEntityId,c);
        			}
			}
        }


        List<Project__c> lstOfProject =  [SELECT id,Validated__c,
        								  Tagged_Image_Count__c,Last_Image_Tagged_Date__c
									      FROM Project__c 
									      WHERE ID IN : mapOfEntityLinkIdAndImgCount.keySet()];					      

        for( Project__c proj : lstOfProject){

        	proj.Tagged_Image_Count__c = proj.Tagged_Image_Count__c + mapOfEntityLinkIdAndImgCount.get(proj.Id) ;
        	proj.Last_Image_Tagged_Date__c = System.now();
        		 
        }
        /* Update Icreased Project Image Count */
        update lstOfProject;

        List<Mock_Up_Request__c> lstOfMockReq =  [SELECT id,Tagged_Doc_Count__c
											      FROM Mock_Up_Request__c 
											      WHERE ID IN : mapOfEntityLinkIdAndDocCount.keySet()];

        for( Mock_Up_Request__c mckReq : lstOfMockReq){

        	mckReq.Tagged_Doc_Count__c = mckReq.Tagged_Doc_Count__c + mapOfEntityLinkIdAndDocCount.get(mckReq.Id) ;		 
        }
        /* Update Icreased Mockup Doc Count */
        update lstOfMockReq;
	}

	public static void restrictImageDelOnContentDoc(List<ContentDocument> lstOfConDoc,
													Map<String,Boolean> mapOfContectType,
													Map<String,Boolean> mapOfsObjectName){

		Map<id,ContentDocument> mapOfImgTypeConDoc = new Map<id,ContentDocument>();
		Map<ID,ID> mapOfProjectIdAndConDocLinkId = new Map<Id,Id>();
		Map<ID,ContentDocument> mapOfConDocLinkIdAndConDoc = new Map<Id,ContentDocument>();
		/* Mockup Request */
		Map<id,ContentDocument> mapOfDocTypeConDoc = new Map<id,ContentDocument>();
		Map<ID,ID> mapOfMockReqIdAndConDocLinkId = new Map<Id,Id>();
		Map<ID,ContentDocument> mapOfMockReqConDocLinkIdAndConDoc = new Map<Id,ContentDocument>();
		
		ID singleConDocId= lstOfConDoc.get(0).id;
		for(ContentDocument cDoc : lstOfConDoc){

			if( mapOfContectType.containsKey(cDoc.FileType+'') ){
				mapOfImgTypeConDoc.put(cDoc.Id,cDoc);
			}
			/* For all type of Docs */
			mapOfDocTypeConDoc.put(cDoc.Id,cDoc);
			
		}

		List<ContentDocumentLink> lstOfConDocLink = [SELECT id,ContentDocumentId,LinkedEntityId,LinkedEntity.Type
													 FROM ContentDocumentLink 
													 WHERE ContentDocumentId =: singleConDocId ];//mapOfImgTypeConDoc.keySet()
		for( ContentDocumentLink conDoclink: lstOfConDocLink){

			if( mapOfsObjectName.containsKey(conDoclink.LinkedEntity.Type+'' )){

				mapOfProjectIdAndConDocLinkId.put(conDoclink.LinkedEntityId,conDoclink.Id);
				mapOfConDocLinkIdAndConDoc.put(conDoclink.Id,mapOfImgTypeConDoc.get(conDoclink.ContentDocumentId));
			}
			else if( conDoclink.LinkedEntity.Type == 'Mock_Up_Request__c' ){
				mapOfMockReqIdAndConDocLinkId.put(conDoclink.LinkedEntityId,conDoclink.Id);
				mapOfMockReqConDocLinkIdAndConDoc.put(conDoclink.Id,mapOfDocTypeConDoc.get(conDoclink.ContentDocumentId));
			}

		}

		List<Mock_Up_Request__c> lstOfMockReq =  [SELECT id,Tagged_Doc_Count__c,Approval_Status__c
											      FROM Mock_Up_Request__c 
											      WHERE ID IN : mapOfMockReqIdAndConDocLinkId.keySet()];
		if(lstOfMockReq.size() > 0){ 
			
			/* Add error on Pending or Apporved Mockup req else decrease count */
			addErrorORdecreaseCountOnMockReq(lstOfMockReq,mapOfMockReqIdAndConDocLinkId,mapOfMockReqConDocLinkIdAndConDoc);
		}	

		List<Project__c> lstOfProject =  [SELECT id,Validated__c,Tagged_Image_Count__c,Name
									       FROM Project__c 
									       WHERE ID IN : mapOfProjectIdAndConDocLinkId.keySet()];

								       

		if(lstOfProject.size() > 0){ 

		 	/* Add error on Validated Projects and decrease Count if all linked projects are not validated */
		 	addErrorORdecreaseCount(lstOfProject,mapOfProjectIdAndConDocLinkId,mapOfConDocLinkIdAndConDoc);
		 }									       
	}

	public static void addErrorORdecreaseCountOnMockReq(List<Mock_Up_Request__c> lstOfMockReq,Map<ID,ID> mapOfMockReqIdAndConDocLinkId,
											Map<ID,ContentDocument> mapOfMockReqConDocLinkIdAndConDoc){

		for( Mock_Up_Request__c mckReq : lstOfMockReq){

				if( mckReq.Approval_Status__c == 'Pending' || mckReq.Approval_Status__c == 'Approved' ){

					ContentDocument conDoc = mapOfMockReqConDocLinkIdAndConDoc.get( mapOfMockReqIdAndConDocLinkId.get(mckReq.id) ) ;
					conDoc.addError('Attached files under Pending or Approved Mockup Request cannot be deleted');	
					
				}else{
					mckReq.Tagged_Doc_Count__c = mckReq.Tagged_Doc_Count__c -1 ;
				}		 
	        }

	        /* Update Decreased Mockup Doc Count */
	       update lstOfMockReq;

	}

	public static void addErrorORdecreaseCount(List<Project__c> lstOfProject,Map<ID,ID> mapOfProjectIdAndConDocLinkId,
											Map<ID,ContentDocument> mapOfConDocLinkIdAndConDoc){

		Map<Id,ContentDocument> mapOfIdAndConDoc = new Map<Id,ContentDocument>();

		for( Project__c proj: lstOfProject){

			if( proj.Validated__c && proj.Tagged_Image_Count__c ==1 &&
				mapOfIdAndConDoc.get(mapOfConDocLinkIdAndConDoc.get( mapOfProjectIdAndConDocLinkId.get(proj.id)).id) == null)
			{
				ContentDocument conDoc = mapOfConDocLinkIdAndConDoc.get( mapOfProjectIdAndConDocLinkId.get(proj.id) ) ;
				conDoc.addError('Cannot delete images associated with validated projects. Linked project : '+ proj.Name);	
				mapOfIdAndConDoc.put(conDoc.id,conDoc);
			}
			else if( !proj.Validated__c && 
			         mapOfIdAndConDoc.get(mapOfConDocLinkIdAndConDoc.get( mapOfProjectIdAndConDocLinkId.get(proj.id)).id) == null) 
			{
				proj.Tagged_Image_Count__c = proj.Tagged_Image_Count__c - 1;
			}
			else if( proj.Validated__c && proj.Tagged_Image_Count__c >1 &&
				mapOfIdAndConDoc.get(mapOfConDocLinkIdAndConDoc.get( mapOfProjectIdAndConDocLinkId.get(proj.id)).id) == null )
			{
				proj.Tagged_Image_Count__c = proj.Tagged_Image_Count__c - 1;
			}
		}

		/* Update Project with Image count */
			update lstOfProject;
	}

	public static void increaseImageCountFrmFeedItem( List<FeedItem> lstOfTriggerFeedItem){

		List<FeedAttachment> attachments =  [SELECT Id, Title, Type,value,
	                                         FeedEntityId 
	                                         FROM FeedAttachment 
	                                         WHERE FeedEntityId IN :lstOfTriggerFeedItem ];
	    List<ID> lstOfFeedItemId = new List<ID>();
	    
	    for (FeedAttachment attachment : attachments) {
	        if(attachment.Type =='Content'){
	            lstOfFeedItemId.add(attachment.FeedEntityID);
	        }
	    }

	    if(lstOfFeedItemId.size() > 0){
	   
	         List<FeedItem> lstOfFeedItem = [SELECT id, Parentid,Parent.Type 
	                                         FROM FeedItem 
	                                         WHERE ID IN :lstOfFeedItemId 
	                                         AND Parent.Type ='Project__c'];
	        
	        if( lstOfFeedItem.size() >0 ){
	            List<ID> lstOfProjectId = new List<Id>();
	            
	            for(FeedItem f : lstOfFeedItem){
	                lstOfProjectId.add(f.Parentid);
	            }
	            
	            List<Project__c> lstOfProject = [SELECT id, Tagged_Image_Count__c,Last_Image_Tagged_Date__c
	                                            FROM Project__c 
	                                            WHERE id IN :lstOfProjectId ];
	            
	             for(Project__c proj : lstOfProject){
	                proj.Tagged_Image_Count__c =  proj.Tagged_Image_Count__c + 1;
	                proj.Last_Image_Tagged_Date__c = System.now();
	            }
	            
	            update lstOfProject;
	            
	        }
	    }
	}

}
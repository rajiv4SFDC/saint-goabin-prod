/* Created By: Akshay kamath
Created Date: 26/07/2018
Class Name: MaterailRequestHandler
Story : Restrict Approval without production date
Last Modified By :Akshay kamath
Last Modified Date: 26/07/2018
*/



public class MaterailRequestHandler {
    
    public static void Fieldupdate(List<Material_Request__c>MatList,map<id,Material_Request__c>matoldmap)
    {
                                                                                                 
        for(Material_Request__c material : MatList){
            if(matoldmap.get(material.id).Approval_Status__c =='Pending' && material.Approval_Status__c =='Approved by Planning/CSD' && (material.Expected_Production_Date__c==NULL || material.Remarks__c == NULL)){
                material.adderror('Please add Expected Production Date & Remarks Before Approving');
                }
              Else if(matoldmap.get(material.id).Approval_Status__c =='Pending' && material.Approval_Status__c =='Rejected by Planning/CSD' && material.Remarks__c == NULL){
                material.adderror('Please add Remarks before Rejecting');
                
            } 
        }
        }
        }
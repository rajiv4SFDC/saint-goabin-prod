/* Created By: Rajiv Kumar Singh
Created Date: 07/06/2017
Class Name: CustomertshipToUpsertAPI
Description : Update status on Account for ERP.
Last Modified By :Rajiv Kumar Singh
Last Modified Date: 07/06/2017
*/
@RestResource(urlMapping='/shipToUpsertAPI')
global class CustomertshipToUpsertAPI {
    @HttpPost
    global static void doHandleInsertRequest(){
        
        List<shipToWrapper> wrapList = new List<shipToWrapper>();
        List<Ship_To_Customer__c> shipList = new List<Ship_To_Customer__c>();
        RestRequest Req = RestContext.request;
        RestResponse res = RestContext.response;
        String body = Req.requestBody.toString().trim();
        
        if(body != null && body != '') {
            
            try {
                wrapList = (List<shipToWrapper>)JSON.deserialize(body, List<shipToWrapper>.class);
                
                for(shipToWrapper we :wrapList){
                    Ship_To_Customer__c shipcus = new Ship_To_Customer__c();
                    if(we.BillToCode!=null && we.BillToCode!=''){
                        shipcus.Bill_To_Customer_Code__c = we.BillToCode;
                    }
                    if(we.ShipToCode!=null && we.ShipToCode!=''){
                        shipcus.Customer_Code__c = we.ShipToCode;
                    }
                    if(we.ShipToName!=null && we.ShipToName!=''){
                        shipcus.Name = we.ShipToName;
                    } 
                    if(we.ShipToAdress!=null && we.ShipToAdress!=''){
                        shipcus.Address__c = we.ShipToAdress;
                    }
                    if(we.ShipToOffice!=null && we.ShipToOffice!=''){
                        shipcus.Sales_Office__c = we.ShipToOffice;
                    }
                    if(we.Shipunique!=null && we.Shipunique!=''){
                        shipcus.Ship_Unique_Code__c = we.Shipunique;
                    }   
                    
                    shipList.add(shipcus);  
                }
                Schema.SObjectField ftoken = Ship_To_Customer__c.Fields.Ship_Unique_Code__c;
                Database.UpsertResult[] srList = Database.upsert(shipList,ftoken,false);      
                
                 
                res.statusCode = 200;
                res.responseBody = Blob.valueOf(String.valueof(shipList));
                
            } catch (Exception e) {
                
                CommonUtilitiesHandler.handleExceptions(e);
                res.statusCode = 500;
                res.responseBody = Blob.valueOf(e.getMessage());
            }
        } else {
            res.statusCode = 500;
            res.responseBody = Blob.valueOf('Invalid Request');
        } 
    }
    
    public class shipToWrapper{
        public String BillToCode{get;set;}
        public string ShipToCode{get;set;}
        public string ShipToName{get;set;}
        public string ShipToAdress{get;set;}
        public string ShipToOffice{get;set;}
        public string Shipunique{get;set;}
        
    }
}
@isTest
public class TestPriceRequestTrigger {
	    
    @testSetup 
    public static void createTestData(){
        TestDataFactory.createCustomSetting();
        TestDataFactory.createTestProductData();
        TestDataFactory.createTestStateAndCityRecordData();
        TestDataFactory.createTestCatalogueBudgetData();
        TestDataFactory.createTestCampaignRecordData();
        TestDataFactory.createTestAccountData();
        TestDataFactory.createTestProjectData();
        TestDataFactory.createTestOpportunityData();
        TestDataFactory.createTestOpportunityProductData();
        TestDataFactory.createTestPriceRequestData();
        TestDataFactory.createTestProductForecateData();
        TestDataFactory.createTestForecateInvoiceData();
        TestDataFactory.createTestMockupRecordData();
        TestDataFactory.createTestPlantVisitRecordData();
        TestDataFactory.createTestInvoiceRecordData();
        TestDataFactory.createTestTargetActualRecordData();
    }
    
    @isTest
    public static void createPriceRequest(){
        Opportunity_Product__c opptyProd = [SELECT id FROM Opportunity_Product__c LIMIT 1];

        Price_Request__c pr = new Price_Request__c();
        pr.Price_Type__c = 'Special Price';
        pr.Requested_Quantity__c = 500;
        pr.Required_Price__c = 400;
        pr.Opportunity_Product__c = opptyProd.Id;
        pr.Approval_Status__c = 'Draft';
        pr.Valid_From__c =  Date.newInstance(2017,8, 27);
        pr.Valid_To__c = Date.newInstance(2017,8, 31);
        
        insert pr;
    }
}
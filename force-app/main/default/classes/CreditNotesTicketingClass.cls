/******************************************
 * Created By  : Ajeet Singh Shekhawat
 * Created On  : 15 Nov 2018
 * Modified By : Ajeet Singh Shekhawat
 * Modified On : 15 Nov 2018
 * Description :  
 * return : @saveCreditNotesTickt() : id of newly inserted record               
******************************************/ 

public class CreditNotesTicketingClass {
    // return filtered invoices which was selected by user.
    @AuraEnabled
    public static List<Invoice__c> getSelectedInvoices(String invoiceName) {
        String invoicesName = invoiceName + '%';
        System.debug('invoiceName ' + invoiceName);
        List<Invoice__c> invoices = new List<Invoice__c>([Select Name, Invoice_Line_Item_No__c, Item_Tonnage__c, Invoice_Rate__c, Quantity__c,Product__c, Product__r.Name,
                                                          Product__r.Thickness_mm__c, Product_Stock__r.Plant_Code__c, Customer__c, Customer__r.Name From Invoice__c Where Name Like : invoicesName]);   
      
        System.debug('invoices getSelectedInvoices1 ' + invoices);
        return invoices;   
    }
    
    // Return PriceRequest records which is in Lookup popup
    @AuraEnabled
    public static List<Price_Request__c> getPriceRequestRecords(String priceRequestRecords) {
        System.debug('invoices priceRequestRecords ' + priceRequestRecords);
        List<Price_Request__c> priceRequest;
        if(priceRequestRecords != null) {
            priceRequest = new List<Price_Request__c>([Select Id, Name From Price_Request__c Where Approval_Status__c = 'Approved' AND Opportunity_Product__r.Product__c =: priceRequestRecords]);
        }
           
      //    List<Price_Request__c> priceRequest = new List<Price_Request__c>([Select Id, Name From Price_Request__c Where Product__c =: priceRequestRecords]);  
        System.debug('invoices getSelectedInvoices2 ' + priceRequest);
        return priceRequest;   
    }
    
    // Return PriceRequest PandP records which is in Lookup popup
    @AuraEnabled
    public static List<price_request_PandP__c> getPriceRequestPandPRecords(String priceRequestPandPRecords) {
        System.debug('invoices priceRequestPandPRecords ' + priceRequestPandPRecords);
        List<price_request_PandP__c> priceRequestPandP;
        if(priceRequestPandPRecords != null) {
            priceRequestPandP = new List<price_request_PandP__c>([Select Id, Name From price_request_PandP__c Where Approval_Status__c = 'Approved' AND Product_lookup__c =: priceRequestPandPRecords]);
        }
      //    List<Price_Request__c> priceRequest = new List<Price_Request__c>([Select Id, Name From Price_Request__c Where Product__c =: priceRequestRecords]);  
        System.debug('invoices priceRequestPandP ' + priceRequestPandP);
        return priceRequestPandP;   
    }
    
    @AuraEnabled
    public static List<Credit_Notes_Ticketing__c> getInsertedRecords(String sendCurrentRecordId) {
        System.debug('invoices sendCurrentRecordId ' + sendCurrentRecordId);
        List<Credit_Notes_Ticketing__c> creditNote;
        if(sendCurrentRecordId != null) {
            creditNote = new List<Credit_Notes_Ticketing__c>([Select Id, Name, Dimension__c, Item_Tonnage__c, Price_Request__c, Price_Request__r.Name,
                                                                                          Plant_Location__c, Product_Name__c, Quantity__c, Invoice__c, Invoice__r.Name,
                                                                                          Remarks__c, Thickness__c, Invoice_Rate__c, Enquiry__c, Enquiry__r.Name,
                                                                                          price_request_PandP__c, price_request_PandP__r.Name, Account__c, Account__r.Name
                                                                                          From Credit_Notes_Ticketing__c
                                                                                          Where Id =: sendCurrentRecordId]);    
        }
         
        System.debug('invoices creditNote ' + creditNote);
        return creditNote;   
    }
    
    @AuraEnabled
    public static Id saveCreditNotesTickt(String creditNotesTicketingListParam) {
        
        //to get current user managers hierarchy
        Id regionalManager, nationalHead, rsmNorth, rsmEast, rsmWest, rsmSouth,nsm;
        Set<String> regionalSalesAndMarketing = new Set<String>{'National Head Sales and Marketing',
                                                                'Regional Head Sales and Marketing East',
                                                                'Regional Head Sales and Marketing North',
                                                                'Regional Head Sales and Marketing South',
                                                                'Regional Head Sales and Marketing West'
                                                                }; 
                                                                
        List<User> userListWithRoleNames = [Select  id, UserName, UserRole.Name FROM User WHERE UserRole.Name IN: regionalSalesAndMarketing];
        for(User usr : userListWithRoleNames){
            if(usr.UserRole.Name == 'National Head Sales and Marketing')
                nsm = usr.id;
            else if(usr.UserRole.Name == 'Regional Head Sales and Marketing East')
                rsmEast = usr.id;
            else if(usr.UserRole.Name == 'Regional Head Sales and Marketing North')
                rsmNorth = usr.id;
            else if(usr.UserRole.Name == 'Regional Head Sales and Marketing South')
                rsmSouth = usr.id;
            else if(usr.UserRole.Name == 'Regional Head Sales and Marketing West')
                rsmWest = usr.id;    
        }
        
        id id1 = userinfo.getProfileId();
        Profile loggedInProfile = [SELECT id, Name FROM Profile WHERE id = :id1 LIMIT 1];
        System.debug('-->profile Name '+loggedInProfile.Name);        
        Boolean queryToBeAdded = String.ValueOf(loggedInProfile.Name).contains('ORS');
        System.debug('queryToBeAdded '+queryToBeAdded);
        String rmUser = '';
        String nationalUser = '';
        if(queryToBeAdded == true){
            rmUser = ',Manager.Manager.Manager.Id '; 
            nationalUser = ',Manager.Manager.Manager.Manager.Manager.Id ';
        }
        else if(String.ValueOf(loggedInProfile.Name).contains('AM')){
            rmUser = ',Manager.Manager.Id ';
            nationalUser = ',Manager.Manager.Manager.Manager.Id '; 
        }
        
        
        System.debug('rmUser '+rmUser+'nationalUser '+nationalUser);
        Id userId = UserInfo.getUserId();
        
        if(rmUser != '' && nationalUser != ''){
            List<user> userList = Database.query('SELECT id, Name'+ rmUser + nationalUser +' FROM User WHERE id =:userId');
            for(User us : userList){
                if(queryToBeAdded == true){
                    regionalManager = us.Manager.Manager.Manager.Id;
                    nationalHead = us.Manager.Manager.Manager.Manager.Manager.Id;
                    
                    System.debug('us ORS'+us.Manager.Manager.Manager.Id);
                    System.debug('uss ORS'+us.Manager.Manager.Manager.Manager.Manager.Id);
                }
                else{
                    regionalManager = us.Manager.Manager.Id;
                    nationalHead = us.Manager.Manager.Manager.Manager.Id;
                    
                    System.debug('us AM'+us.Manager.Manager.Id);
                    System.debug('uss AM'+us.Manager.Manager.Manager.Manager.Id);
                }
            }
        }
        
        //end of user managers assignment
        
        Id creditNoteId;
        try {
            System.debug('Insert creditNotesTicketingList  **** ' + creditNotesTicketingListParam);
            List<Credit_Notes_Ticketing__c> creditList = new List<Credit_Notes_Ticketing__c>();
            List<Object> creditObj = (List<Object>) JSON.deserializeUntyped(creditNotesTicketingListParam);
            Credit_Notes_Ticketing__c creNote;
            for (Object iterateObjList : creditObj) {
                
                if(iterateObjList != null) {
                    Map<String, Object> creditMap = (Map<String, Object>) iterateObjList;
                    creNote = new Credit_Notes_Ticketing__c();
                    
                    if(creditMap.get('Dimension__c') != null && creditMap.get('Dimension__c') != '') {
                        creNote.Dimension__c = (String) creditMap.get('Dimension__c');
                    }
                    
                    if(creditMap.get('Enquiry__c') != null && creditMap.get('Enquiry__c') != '') {
                        creNote.Enquiry__c = (String) creditMap.get('Enquiry__c');
                    }
                    if(creditMap.get('Price_Request__c') != null && creditMap.get('Price_Request__c') != '' && creditMap.get('Price_Request__c') != 'Choose PriceRequest') {
                        creNote.Price_Request__c = (String) creditMap.get('Price_Request__c');
                    }
                    System.debug('price_request_PandP__c  **** ' + creditMap.get('price_request_PandP__c') );
                    if(creditMap.get('price_request_PandP__c') != null && creditMap.get('price_request_PandP__c') != '' && creditMap.get('price_request_PandP__c') != 'Choose Price Request PandP') {
                        creNote.price_request_PandP__c = (String) creditMap.get('price_request_PandP__c');
                    }
                    if(creditMap.get('Invoice__c') != null && creditMap.get('Invoice__c') != '') {
                        creNote.Invoice__c = (String) creditMap.get('Invoice__c');
                    }
                    if(creditMap.get('Plant_Location__c') != null && creditMap.get('Plant_Location__c') != '') {
                        creNote.Plant_Location__c = (String) creditMap.get('Plant_Location__c');
                    }
                    if(creditMap.get('Remarks__c') != null && creditMap.get('Remarks__c') != '') {
                        creNote.Remarks__c = (String) creditMap.get('Remarks__c');
                    }
                    if(creditMap.get('Item_Tonnage__c') != null && creditMap.get('Item_Tonnage__c') != '') {
                        creNote.Item_Tonnage__c = (String) creditMap.get('Item_Tonnage__c');
                    }
                    if(creditMap.get('Product_Name__c') != null && creditMap.get('Product_Name__c') != '') {
                        creNote.Product_Name__c = (String) creditMap.get('Product_Name__c');
                    }
                    // null check for Invoice Rate
                    if(creditMap.get('Invoice_Rate__c') != null && creditMap.get('Invoice_Rate__c') != '') {
                        creNote.Invoice_Rate__c = (String)creditMap.get('Invoice_Rate__c');
                   //     creNote.Invoice_Rate__c = Decimal.valueOf((String)creditMap.get('Invoice_Rate__c'));
                    }
                    
                    // null check for Quantity
                    if(creditMap.get('Quantity__c') != null && creditMap.get('Quantity__c') != '') {
                      //  System.debug('inside quantity rate ' + Decimal.valueOf((String)creditMap.get('Quantity__c')));
                        creNote.Quantity__c = (String)creditMap.get('Quantity__c');
                    } 
                    // null check for Thickness
                    if(creditMap.get('Thickness__c') != null && creditMap.get('Thickness__c') != '') {
                        creNote.Thickness__c = (String)creditMap.get('Thickness__c');
                    } 
                    
                    // null check for Account
                    if(creditMap.get('Account__c') != null && creditMap.get('Account__c') != '') {
                        creNote.Account__c = (String) creditMap.get('Account__c');
                    } 
                    /*
                    if(creditMap.get('Account_Sales_Office__c') != null && creditMap.get('Account_Sales_Office__c') != ''){
                        System.debug('--> '+creditMap.get('Account_Sales_Office__c'));
                        if(creditMap.get('Account_Sales_Office__c') == 'North')
                            creNote.RSM__c = rsmNorth;
                        if(creditMap.get('Account_Sales_Office__c') == 'South')    
                            creNote.RSM__c = rsmSouth;
                        if(creditMap.get('Account_Sales_Office__c') == 'West')    
                            creNote.RSM__c = rsmWest;
                        if(creditMap.get('Account_Sales_Office__c') == 'East')    
                            creNote.RSM__c = rsmEast;
                    }*/
                    
                    if(regionalManager != null) {
                        creNote.Regional_Head__c = regionalManager;
                    }
                    	
                    /*if(nationalHead != null) {
                        creNote.National_Head__c = nationalHead;
                    }*/
                    	
                    if(nsm != null) {
                        creNote.NSM__c = nsm;
                    }
                    creditList.add(creNote);  
                }
            }
            System.debug('Insert creditList  **** ' + creditList);
            if(creditList.size() > 0 && !creditList.isEmpty()){ // null check
                Insert creditList;
                for(Credit_Notes_Ticketing__c cntic : creditList) {
                    creditNoteId = cntic.id;
                }
            }  
        } catch(Exception ex) {
            throw new AuraHandledException(ex.getMessage());
        }
        return creditNoteId;
    }
    
    // To update
    @AuraEnabled
    public static Id editCreditNotesTickt(String creditNotesTicketingListParam, String creditRecordID) {
        
        
        
        Id creditNoteId;
        try {
            System.debug('All data editCreditNotesTickt  **** ' + creditNotesTicketingListParam);
            System.debug('Id editCreditNotesTickt  **** ' + creditRecordID);
            List<Credit_Notes_Ticketing__c> creditList = new List<Credit_Notes_Ticketing__c>();
            List<Object> creditObj = (List<Object>) JSON.deserializeUntyped(creditNotesTicketingListParam);
            Credit_Notes_Ticketing__c creNote;
            for (Object iterateObjList : creditObj) {
                
                if(iterateObjList != null) {
                    Map<String, Object> creditMap = (Map<String, Object>) iterateObjList;
                    creNote = new Credit_Notes_Ticketing__c();
                    
                    creNote.Id = creditRecordID;  // setting record id to update same record
                    if(creditMap.get('Dimension__c') != null && creditMap.get('Dimension__c') != '') {
                        creNote.Dimension__c = (String) creditMap.get('Dimension__c');
                    }
                   
                    if(creditMap.get('Enquiry__c') != null && creditMap.get('Enquiry__c') != '') {
                        creNote.Enquiry__c = (String) creditMap.get('Enquiry__c');
                        System.debug('Id Enquiry__c  **** ' + creditMap.get('Enquiry__c'));
                    }
                    if(creditMap.get('Price_Request__c') != null && creditMap.get('Price_Request__c') != '' && creditMap.get('Price_Request__c') != 'Choose PriceRequest') {
                        creNote.Price_Request__c = (String) creditMap.get('Price_Request__c');
                        System.debug('Price_Request__c value if  **** ' + creditMap.get('Price_Request__c') );
                    }
                    System.debug('price_request_PandP__c  **** ' + creditMap.get('price_request_PandP__c') );
                    if(creditMap.get('price_request_PandP__c') != null && creditMap.get('price_request_PandP__c') != '' && creditMap.get('price_request_PandP__c') != 'Choose Price Request PandP') {
                        creNote.price_request_PandP__c = (String) creditMap.get('price_request_PandP__c');
                        System.debug('price_request_PandP__c value if  **** ' + creditMap.get('price_request_PandP__c') );
                    }
                    // If user change value of PriceRequest or Price Request PandP picklist then reset previously stored value in backend.
                    if(creditMap.get('Price_Request__c') == 'Choose PriceRequest') {
                        creNote.Price_Request__c = null;
                        System.debug('Price_Request__c null if  **** ' + creditMap.get('Price_Request__c') );
                    }
                    if(creditMap.get('price_request_PandP__c') == 'Choose Price Request PandP') {
                        creNote.price_request_PandP__c = null;
                        System.debug('price_request_PandP__c null if  **** ' + creditMap.get('price_request_PandP__c') );
                    }
                    
                    if(creditMap.get('Invoice__c') != null && creditMap.get('Invoice__c') != '') {
                        creNote.Invoice__c = (String) creditMap.get('Invoice__c');
                    }
                    if(creditMap.get('Plant_Location__c') != null && creditMap.get('Plant_Location__c') != '') {
                        creNote.Plant_Location__c = (String) creditMap.get('Plant_Location__c');
                    }
                    if(creditMap.get('Remarks__c') != null && creditMap.get('Remarks__c') != '') {
                        creNote.Remarks__c = (String) creditMap.get('Remarks__c');
                    }
                    if(creditMap.get('Item_Tonnage__c') != null && creditMap.get('Item_Tonnage__c') != '') {
                        creNote.Item_Tonnage__c = (String) creditMap.get('Item_Tonnage__c');
                    }
                    if(creditMap.get('Product_Name__c') != null && creditMap.get('Product_Name__c') != '') {
                        creNote.Product_Name__c = (String) creditMap.get('Product_Name__c');
                    }
                    // null check for Invoice Rate
                    if(creditMap.get('Invoice_Rate__c') != null && creditMap.get('Invoice_Rate__c') != '') {
                      //  creNote.Invoice_Rate__c = Integer.valueOf(creditMap.get('Invoice_Rate__c'));
                        creNote.Invoice_Rate__c = (String)creditMap.get('Invoice_Rate__c');
                    }
                    
                    // null check for Quantity
                    if(creditMap.get('Quantity__c') != null && creditMap.get('Quantity__c') != '') {
                        creNote.Quantity__c = (String)creditMap.get('Quantity__c');
                    } 
                    // null check for Thickness
                    if(creditMap.get('Thickness__c') != null && creditMap.get('Thickness__c') != '') {
                        creNote.Thickness__c = (String)creditMap.get('Thickness__c');
                    } 
                    // null check for Account
                    if(creditMap.get('Account__c') != null && creditMap.get('Account__c') != '') {
                        creNote.Account__c = (String) creditMap.get('Account__c');
                    } 
                    
                    creditList.add(creNote);  
                }
            }
            System.debug('update creditList  **** ' + creditList);
            if(creditList.size() > 0 && !creditList.isEmpty()){ // null check
               // Database.insert(creditList, false);
                Update creditList;
                System.debug('update creditList  success ');
                for(Credit_Notes_Ticketing__c cntic : creditList) {
                    creditNoteId = cntic.id;
                }
            }  
        } catch(Exception ex) {
            throw new AuraHandledException(ex.getMessage());
        }
        return creditNoteId;
    }
}
public class MerchandiseRequestTriggerHandler extends TriggerHandler{

	public MerchandiseRequestTriggerHandler() {}
    
    public override void beforeInsert() {

        new CatalogueBudgetManager().checkForCatalogueRequestForBlankShipping( (List<Merchandise_Request__c>)Trigger.new );
    }
    
    public override void afterUpdate() {

       new CatalogueBudgetManager().checkForApprovedCatalogueRequest( (List<Merchandise_Request__c>)Trigger.new,
       																	(Map<Id,Merchandise_Request__c>)Trigger.oldMap  );
    }
    
}
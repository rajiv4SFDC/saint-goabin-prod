public class UpdateOpportunityProductOfInvoice{
  @InvocableMethod(label='UpdateOpportunityProductOfInvoice' description='update the particular Opportunity Product record lookup field to Record number')
  public static void UpdateLookupValue(List<Id> invoiceId)
    {   
       map<string,string> opportunityProductMap = new map<string,string>();  
       set<string> presentOpportunityProductIdSet = new set<String>();
       list<Invoice__c > NewList = new list<Invoice__c>(); 
       list<Invoice__c > FinalListToUpdate = new list<Invoice__c>();
       for(Invoice__c ptp:[select id,Opportunity_Product__c,Opportunity_Product_Id__c from Invoice__c where id in:invoiceId])
       {
         if(ptp.Opportunity_Product_Id__c != null)
          {
            presentOpportunityProductIdSet.add(ptp.Opportunity_Product_Id__c);      
            NewList.add(ptp);
            system.debug('The ptp.id is: ' +ptp.id);
          }     
       }
       system.debug('The NewList is: ' +NewList+'......'+presentOpportunityProductIdSet);
       if(presentOpportunityProductIdSet.size()>0)
       {
          for(Opportunity_Product__c acc:[select id, Opportunity__c, Name from Opportunity_Product__c where Name in:presentOpportunityProductIdSet])
           {
               opportunityProductMap.put(acc.Name,acc.id);
           }
           system.debug('The opportunityProductMap is: ' +opportunityProductMap);
           for(Invoice__c ptp:NewList)
           {
               ptp.Opportunity_Product__c = opportunityProductMap.get(ptp.Opportunity_Product_Id__c);
               system.debug('The  ptp.Opportunity_Product__c is: ' + ptp.Opportunity_Product__c);
               FinalListToUpdate.add(ptp);
           }
       }
       if(!FinalListToUpdate.isEmpty() && FinalListToUpdate.size() > 0) {
            update FinalListToUpdate;
       }
    }
}
Public Class YTDComparisionController{
    
    public String selectedYear{get;set;}
    public String selectedMonth{get;set;}
    public String selectedRegion{get;set;}
    public List<SelectOption> years{get;set;}
    public List<SelectOption> months{get;set;}
    public List<SelectOption> regions{get;set;}
    //public List<wrapperClass> wrapperList{get;set;}
    public Map<String,Map<String,Decimal>> mapOfAreaWrapper{get;set;} 
    //public Set<String> fieldSets;
    public Decimal convertMonthToNumber;
    public Boolean check{get;set;}
    public List<String> ytdLiteralsList{get;set;}
    public Map<String,Decimal> mapForTotalYTD{get;set;}
    public Map<String,String> mapForOrderingData{get;set;}
    //public Map<String,Decimal> mapOfTotalYTD{get;set;}
    public Decimal previousYearTonnageValues{get;set;}
    
    
    public YTDComparisionController(){
        check = false;
        year(); 
        month();  
        region();
        mapOfAreaWrapper = new map<String,Map<String,Decimal>>(); 
        ytdLiteralsList = new list<String>{'previousYearTonnage','ytmTonnage','ytdTonnage','mtmTonnage','mtdTonnage',
                                           'previousYearValue','ytmValue','ytdValue','mtmValue','mtdValue'};
        mapForOrderingData = new map<String, String>();
    }
    
    //to get years 
    public void year(){        
        years = new List<SelectOption>();
        String startYear = Label.StartYear;
        String endYear = Label.End_Year;
        for(Decimal i = Decimal.ValueOf(startYear) ; i<= Decimal.ValueOf(endYear) ;i++){
            years.add(new SelectOption(String.ValueOf(i), String.ValueOf(i)));
        }      
    
    }
    
    public void month(){
        months = new List<SelectOption>();
        list<string> stringList = new list<string>{'Jan','Feb','Mar','Apr','May','Jun','Jul','Aug','Sep','Oct','Nov','Dec'};
        system.debug('***'+stringList);
        for(string str:stringList){
            months.add(new SelectOption(str,str));
            system.debug('****'+str);        
        }
    }
    
    
    public void region(){
       Schema.DescribeFieldResult fieldResult = Account.Sales_Area_Region__c.getDescribe();
       regions = new List<SelectOption>();
       List<Schema.PicklistEntry> ple = fieldResult.getPicklistValues();
        
       for( Schema.PicklistEntry f : ple){
           regions.add(new SelectOption(f.getLabel(), f.getValue()));
       }       
       system.debug('****'+Regions);
       
    }
    
    //get all Invoices tonnage and values of selected year and previous year
    
    public void sumOfInvoice(){
        try{
            check=true;
            mapOfAreaWrapper = new map<String, map<String,Decimal>>();
            mapForTotalYTD = new map<String,Decimal>();
            previousYearTonnageValues = 0;
            mapForTotalYTD.put('previousYearTonnage',0);
            mapForTotalYTD.put('ytmTonnage',0);
            mapForTotalYTD.put('ytdTonnage',0);
            mapForTotalYTD.put('mtmTonnage',0);
            mapForTotalYTD.put('mtdTonnage',0);
            mapForTotalYTD.put('previousYearValue',0);
            mapForTotalYTD.put('ytmValue',0);
            mapForTotalYTD.put('ytdValue',0);
            mapForTotalYTD.put('mtmValue',0);
            mapForTotalYTD.put('mtdValue',0);
            
            Map<String,Decimal> mapOfMonth = new Map<String,Decimal>{'Jan' => 1, 'Feb' => 2, 'Mar' => 3, 'Apr' => 4, 'May' => 5, 
                                                                     'Jun' => 6, 'Jul' => 7, 'Aug' => 8, 'Sep' => 9, 'Oct' => 10, 
                                                                     'Nov' => 11, 'Dec' => 12};
            System.debug('selectedMonth->'+selectedMonth);
            convertMonthToNumber = mapOfMonth.get(selectedMonth);
            
            mapForOrderingData.put('previousYearTonnage', String.ValueOf(Decimal.ValueOf(selectedYear) -1));
            mapForOrderingData.put('ytmTonnage', 'YTM '+selectedMonth+'-'+String.ValueOf(Decimal.ValueOf(selectedYear) - 1));
            mapForOrderingData.put('ytdTonnage', 'YTD '+selectedMonth+'-'+String.ValueOf(selectedYear));
            mapForOrderingData.put('mtmTonnage', 'MTM '+selectedMonth+'-'+String.ValueOf(Decimal.ValueOf(selectedYear) - 1));
            mapForOrderingData.put('mtdTonnage', 'MTD '+selectedMonth+'-'+String.ValueOf(selectedYear));
            mapForOrderingData.put('previousYearValue', String.ValueOf(Decimal.ValueOf(selectedYear) -1));
            mapForOrderingData.put('ytmValue', 'YTM '+selectedMonth+'-'+String.ValueOf(Decimal.ValueOf(selectedYear) - 1));
            mapForOrderingData.put('ytdValue', 'YTD '+selectedMonth+'-'+String.ValueOf(selectedYear));
            mapForOrderingData.put('mtmValue', 'MTM '+selectedMonth+'-'+String.ValueOf(Decimal.ValueOf(selectedYear) - 1));
            mapForOrderingData.put('mtdValue', 'MTD '+selectedMonth+'-'+String.ValueOf(selectedYear));
            
            System.debug('selectedMonth ->'+selectedMonth+' selectedYear ->'+selectedYear);
            
            
            Transient List<Invoice__c> invoiceList = [SELECT Item_Tonnage_formula__c, Item_Tonnage__c, Invoice_Value__c, Customer__c, Customer__r.Sales_Area_Region__c,
                                           Customer__r.Sales_Office_ERP__c, createdDate FROM Invoice__c WHERE Customer__r.Sales_Area_Region__c =:selectedRegion AND Customer__c != null AND 
                                           ((CALENDAR_Year(createdDate) =:Integer.ValueOf(selectedYear) AND CALENDAR_Month(createdDate) <= :Integer.ValueOf(convertMonthToNumber)) OR CALENDAR_Year(createdDate) =:Integer.ValueOf(selectedYear)-1)];
            System.debug('invoiceList -> '+invoiceList);     
            for(Invoice__c inv : invoiceList){
                String regionArea = String.ValueOf(inv.Customer__r.Sales_Area_Region__c).toUpperCase() + ' - ' + String.ValueOf(inv.Customer__r.Sales_Office_ERP__c);
                //previousYearTonnageValues += Decimal.ValueOf(inv.Item_Tonnage__c);
                
                System.debug('regionArea '+regionArea);
                if(!mapOfAreaWrapper.isEmpty() && mapOfAreaWrapper.ContainsKey(regionArea) && mapOfAreaWrapper.get(regionArea) != null){
                    
                    //previousYearTonnage
                    if(inv.CreatedDate.Year() == Decimal.ValueOf(selectedYear)-1){               
                        if(!mapOfAreaWrapper.get(regionArea).isEmpty() && mapOfAreaWrapper.get(regionArea).ContainsKey('previousYearTonnage') && mapOfAreaWrapper.get(regionArea).get('previousYearTonnage') > 0){
                            //mapForTotalYTD.get(selectedRegion).put('previousYearTonnage', (mapOfAreaWrapper.get(regionArea).get('previousYearTonnage'))+Decimal.ValueOf(inv.Item_Tonnage__c));
                            mapOfAreaWrapper.get(regionArea).put('previousYearTonnage', (inv.Item_Tonnage__c == null ? (mapOfAreaWrapper.get(regionArea).get('previousYearTonnage')) : ((mapOfAreaWrapper.get(regionArea).get('previousYearTonnage'))+Decimal.ValueOf(inv.Item_Tonnage__c))));    
                        }    
                        else{
                            mapOfAreaWrapper.get(regionArea).put('previousYearTonnage',(inv.Item_Tonnage__c == null ? 0 : Decimal.ValueOf(inv.Item_Tonnage__c)));
                        }
                        
                        
                    }
                    
                    
                    
                    //ytmTonnage
                    if(inv.CreatedDate.Year() == Decimal.ValueOf(selectedYear)-1 && inv.CreatedDate.month() <= convertMonthToNumber){
                        if(!mapOfAreaWrapper.get(regionArea).isEmpty() && mapOfAreaWrapper.get(regionArea).ContainsKey('ytmTonnage') && mapOfAreaWrapper.get(regionArea).get('ytmTonnage') > 0){
                            //mapOfAreaWrapper.get(regionArea).put('ytmTonnage', (mapOfAreaWrapper.get(regionArea).get('ytmTonnage'))+Decimal.ValueOf(inv.Item_Tonnage__c));    
                            mapOfAreaWrapper.get(regionArea).put('ytmTonnage', (inv.Item_Tonnage__c == null ? (mapOfAreaWrapper.get(regionArea).get('ytmTonnage')) : ((mapOfAreaWrapper.get(regionArea).get('ytmTonnage'))+Decimal.ValueOf(inv.Item_Tonnage__c))));
                        }    
                        else{
                            mapOfAreaWrapper.get(regionArea).put('ytmTonnage', (inv.Item_Tonnage__c == null ? 0 : Decimal.ValueOf(inv.Item_Tonnage__c)));
                        }
                        
                        
                    }
                    
                    //ytdTonnage
                    if(inv.CreatedDate.Year() == Decimal.ValueOf(selectedYear) && inv.CreatedDate.month() <= convertMonthToNumber){
                        if(!mapOfAreaWrapper.get(regionArea).isEmpty() && mapOfAreaWrapper.get(regionArea).ContainsKey('ytdTonnage') && mapOfAreaWrapper.get(regionArea).get('ytdTonnage') > 0){
                            //mapOfAreaWrapper.get(regionArea).put('ytdTonnage', (mapOfAreaWrapper.get(regionArea).get('ytdTonnage'))+Decimal.ValueOf(inv.Item_Tonnage__c));    
                            mapOfAreaWrapper.get(regionArea).put('ytdTonnage', (inv.Item_Tonnage__c == null ? (mapOfAreaWrapper.get(regionArea).get('ytdTonnage')) : ((mapOfAreaWrapper.get(regionArea).get('ytdTonnage'))+Decimal.ValueOf(inv.Item_Tonnage__c))));
                        }    
                        else{
                            mapOfAreaWrapper.get(regionArea).put('ytdTonnage', (inv.Item_Tonnage__c == null ? 0 : Decimal.ValueOf(inv.Item_Tonnage__c)));
                        }
                        
                        
                            
                            
                    }      
                              
                    //mtmTonnage
                    if(inv.CreatedDate.Year() == Decimal.ValueOf(selectedYear)-1 && inv.CreatedDate.month() == convertMonthToNumber){
                        if(!mapOfAreaWrapper.get(regionArea).isEmpty() && mapOfAreaWrapper.get(regionArea).ContainsKey('mtmTonnage') && mapOfAreaWrapper.get(regionArea).get('mtmTonnage') > 0){
                            //mapOfAreaWrapper.get(regionArea).put('mtmTonnage', (mapOfAreaWrapper.get(regionArea).get('mtmTonnage'))+Decimal.ValueOf(inv.Item_Tonnage__c));    
                            mapOfAreaWrapper.get(regionArea).put('mtmTonnage', (inv.Item_Tonnage__c == null ? (mapOfAreaWrapper.get(regionArea).get('mtmTonnage')) : ((mapOfAreaWrapper.get(regionArea).get('mtmTonnage'))+Decimal.ValueOf(inv.Item_Tonnage__c))));
                        }    
                        else{
                           mapOfAreaWrapper.get(regionArea).put('mtmTonnage', (inv.Item_Tonnage__c == null ? 0 : Decimal.ValueOf(inv.Item_Tonnage__c)));
                        }
                        
                        
                    }
                    
                    //mtdTonnage
                    if(inv.CreatedDate.Year() == Decimal.ValueOf(selectedYear) && inv.CreatedDate.month() == convertMonthToNumber){
                        if(!mapOfAreaWrapper.get(regionArea).isEmpty() && mapOfAreaWrapper.get(regionArea).ContainsKey('mtdTonnage') && mapOfAreaWrapper.get(regionArea).get('mtdTonnage') > 0){
                            //mapOfAreaWrapper.get(regionArea).put('mtdTonnage', (mapOfAreaWrapper.get(regionArea).get('mtdTonnage'))+Decimal.ValueOf(inv.Item_Tonnage__c));    
                            mapOfAreaWrapper.get(regionArea).put('mtdTonnage', (inv.Item_Tonnage__c == null ? (mapOfAreaWrapper.get(regionArea).get('mtdTonnage')) : ((mapOfAreaWrapper.get(regionArea).get('mtdTonnage'))+Decimal.ValueOf(inv.Item_Tonnage__c))));
                        }    
                        else{
                            mapOfAreaWrapper.get(regionArea).put('mtdTonnage', (inv.Item_Tonnage__c == null ? 0 : Decimal.ValueOf(inv.Item_Tonnage__c)));
                        }
                        
                        
                    }
                    
                    //previousYearValue
                    if(inv.CreatedDate.Year() == Decimal.ValueOf(selectedYear)-1){
                        if(!mapOfAreaWrapper.get(regionArea).isEmpty() && mapOfAreaWrapper.get(regionArea).ContainsKey('previousYearValue') && mapOfAreaWrapper.get(regionArea).get('previousYearValue') > 0){
                            //mapOfAreaWrapper.get(regionArea).put('previousYearValue', (mapOfAreaWrapper.get(regionArea).get('previousYearValue'))+inv.Invoice_Value__c);    
                            mapOfAreaWrapper.get(regionArea).put('previousYearValue', (inv.Invoice_Value__c > 0 ? ((mapOfAreaWrapper.get(regionArea).get('previousYearValue'))+inv.Invoice_Value__c) : (mapOfAreaWrapper.get(regionArea).get('previousYearValue'))));
                        }    
                        else{
                            mapOfAreaWrapper.get(regionArea).put('previousYearValue',inv.Invoice_Value__c > 0 ? inv.Invoice_Value__c : 0);
                        }
                        
                        
                    }
                    
                    //ytmValue
                    if(inv.CreatedDate.Year() == Decimal.ValueOf(selectedYear)-1 && inv.CreatedDate.month() <= convertMonthToNumber){
                        if(!mapOfAreaWrapper.get(regionArea).isEmpty() && mapOfAreaWrapper.get(regionArea).ContainsKey('ytmValue') && mapOfAreaWrapper.get(regionArea).get('ytmValue') > 0){
                            //mapOfAreaWrapper.get(regionArea).put('ytmValue', (mapOfAreaWrapper.get(regionArea).get('ytmValue'))+inv.Invoice_Value__c);    
                            mapOfAreaWrapper.get(regionArea).put('ytmValue', (inv.Invoice_Value__c > 0 ? ((mapOfAreaWrapper.get(regionArea).get('ytmValue'))+inv.Invoice_Value__c) : (mapOfAreaWrapper.get(regionArea).get('ytmValue'))));
                        }    
                        else{
                            mapOfAreaWrapper.get(regionArea).put('ytmValue',inv.Invoice_Value__c > 0 ? inv.Invoice_Value__c : 0);
                        }
                        
                        
                    }
                    
                    //ytdValue
                    if(inv.CreatedDate.Year() == Decimal.ValueOf(selectedYear) && inv.CreatedDate.month() <= convertMonthToNumber){
                        if(!mapOfAreaWrapper.get(regionArea).isEmpty() && mapOfAreaWrapper.get(regionArea).ContainsKey('ytdValue') && mapOfAreaWrapper.get(regionArea).get('ytdValue') > 0){
                            mapOfAreaWrapper.get(regionArea).put('ytdValue', (inv.Invoice_Value__c > 0 ? ((mapOfAreaWrapper.get(regionArea).get('ytdValue'))+inv.Invoice_Value__c) : (mapOfAreaWrapper.get(regionArea).get('ytdValue'))));    
                        }    
                        else{
                            mapOfAreaWrapper.get(regionArea).put('ytdValue',inv.Invoice_Value__c > 0 ? inv.Invoice_Value__c : 0);
                        }
                        
                        
                    }
                    
                    //mtmValue
                    if(inv.CreatedDate.Year() == Decimal.ValueOf(selectedYear)-1 && inv.CreatedDate.month() == convertMonthToNumber){
                        if(!mapOfAreaWrapper.get(regionArea).isEmpty() && mapOfAreaWrapper.get(regionArea).ContainsKey('mtmValue') && mapOfAreaWrapper.get(regionArea).get('mtmValue') > 0){
                            //mapOfAreaWrapper.get(regionArea).put('mtmValue', (mapOfAreaWrapper.get(regionArea).get('mtmValue'))+inv.Invoice_Value__c);    
                            mapOfAreaWrapper.get(regionArea).put('mtmValue', (inv.Invoice_Value__c > 0 ? ((mapOfAreaWrapper.get(regionArea).get('mtmValue'))+inv.Invoice_Value__c) : (mapOfAreaWrapper.get(regionArea).get('mtmValue'))));
                        }    
                        else{
                            mapOfAreaWrapper.get(regionArea).put('mtmValue',inv.Invoice_Value__c > 0 ? inv.Invoice_Value__c : 0);
                        }
                        
                        
                    }
                    
                    //mtdValue
                    if(inv.CreatedDate.Year() == Decimal.ValueOf(selectedYear) && inv.CreatedDate.month() == convertMonthToNumber){
                        if(!mapOfAreaWrapper.get(regionArea).isEmpty() && mapOfAreaWrapper.get(regionArea).ContainsKey('mtdValue') && mapOfAreaWrapper.get(regionArea).get('mtdValue') > 0){
                            //mapOfAreaWrapper.get(regionArea).put('mtdValue', (mapOfAreaWrapper.get(regionArea).get('mtdValue'))+inv.Invoice_Value__c);    
                            mapOfAreaWrapper.get(regionArea).put('mtdValue', (inv.Invoice_Value__c > 0 ? ((mapOfAreaWrapper.get(regionArea).get('mtdValue'))+inv.Invoice_Value__c) : (mapOfAreaWrapper.get(regionArea).get('mtdValue'))));
                        }    
                        else{
                            mapOfAreaWrapper.get(regionArea).put('mtdValue',inv.Invoice_Value__c > 0 ? inv.Invoice_Value__c : 0);
                        }
                    }
                }
                
                else{
                    Decimal previousYearTons=0, ytmTons=0, ytdTons=0, mtmTons=0, mtdTons=0, previousYearVal=0, ytmVal=0, ytdVal=0, mtmVal=0, mtdVal=0;
                    
                    
                    //to get invoice tons
                    if(inv.CreatedDate.Year() == Decimal.ValueOf(selectedYear)-1){
                        previousYearTons = (inv.Item_Tonnage__c != null && (Decimal.ValueOf(inv.Item_Tonnage__c) > 0) ? Decimal.ValueOf(inv.Item_Tonnage__c) : 0);
                    }
                    if(inv.CreatedDate.Year() == Decimal.ValueOf(selectedYear)-1 && inv.CreatedDate.month() <= convertMonthToNumber){
                        ytmTons = (inv.Item_Tonnage__c != null && (Decimal.ValueOf(inv.Item_Tonnage__c) > 0) ? Decimal.ValueOf(inv.Item_Tonnage__c) : 0);
                    }
                    if(inv.CreatedDate.Year() == Decimal.ValueOf(selectedYear) && inv.CreatedDate.month() <= convertMonthToNumber){
                        ytdTons = (inv.Item_Tonnage__c != null && (Decimal.ValueOf(inv.Item_Tonnage__c) > 0) ? Decimal.ValueOf(inv.Item_Tonnage__c) : 0);
                    }
                    if(inv.CreatedDate.Year() == Decimal.ValueOf(selectedYear)-1 && inv.CreatedDate.month() == convertMonthToNumber){
                        mtmTons = (inv.Item_Tonnage__c != null && (Decimal.ValueOf(inv.Item_Tonnage__c) > 0) ? Decimal.ValueOf(inv.Item_Tonnage__c) : 0);
                    }
                    if(inv.CreatedDate.Year() == Decimal.ValueOf(selectedYear) && inv.CreatedDate.month() == convertMonthToNumber){
                        mtdTons = (inv.Item_Tonnage__c != null && (Decimal.ValueOf(inv.Item_Tonnage__c) > 0) ? Decimal.ValueOf(inv.Item_Tonnage__c) : 0);
                    }
                    
                    
                    //to get invoice values
                    if(inv.CreatedDate.Year() == Decimal.ValueOf(selectedYear)-1){
                        previousYearVal = inv.Invoice_Value__c > 0 ? inv.Invoice_Value__c : 0;
                    }
                    if(inv.CreatedDate.Year() == Decimal.ValueOf(selectedYear)-1 && inv.CreatedDate.month() <= convertMonthToNumber){
                        ytmVal = inv.Invoice_Value__c > 0 ? inv.Invoice_Value__c : 0;
                    }
                    if(inv.CreatedDate.Year() == Decimal.ValueOf(selectedYear) && inv.CreatedDate.month() <= convertMonthToNumber){
                        ytdVal = inv.Invoice_Value__c > 0 ? inv.Invoice_Value__c : 0;
                    }
                    if(inv.CreatedDate.Year() == Decimal.ValueOf(selectedYear)-1 && inv.CreatedDate.month() == convertMonthToNumber){
                        mtmVal = inv.Invoice_Value__c > 0 ? inv.Invoice_Value__c : 0;
                    }
                    if(inv.CreatedDate.Year() == Decimal.ValueOf(selectedYear) && inv.CreatedDate.month() == convertMonthToNumber){
                        mtdVal = inv.Invoice_Value__c > 0 ? inv.Invoice_Value__c : 0;
                    }
                        mapOfAreaWrapper.put(regionArea,new Map<String,Decimal>{  'previousYearTonnage'=>previousYearTons,
                                                                                  'ytmTonnage'=>ytmTons,
                                                                                  'ytdTonnage'=>ytdTons,
                                                                                  'mtmTonnage'=>mtmTons,
                                                                                  'mtdTonnage'=>mtdTons,
                                                                                  
                                                                                  'previousYearValue'=>previousYearVal,
                                                                                  'ytmValue'=>ytmVal,
                                                                                  'ytdValue'=>ytdVal,
                                                                                  'mtmValue'=>mtmVal,
                                                                                  'mtdValue'=>mtdVal
                                                                               });
                }
                
                
                
                if(inv.CreatedDate.Year() == Decimal.ValueOf(selectedYear)-1){
                    //to calculate summation of previous year Tonnage column
                    if(!mapForTotalYTD.isEmpty() && mapForTotalYTD.containsKey('previousYearTonnage') && mapForTotalYTD.get('previousYearTonnage') > 0){
                        mapForTotalYTD.put('previousYearTonnage', (inv.Item_Tonnage__c == null ? mapForTotalYTD.get('previousYearTonnage') : mapForTotalYTD.get('previousYearTonnage') + Decimal.ValueOf(inv.Item_Tonnage__c) ));
                    }
                    else
                        mapForTotalYTD.put('previousYearTonnage', (inv.Item_Tonnage__c == null ? 0 : Decimal.ValueOf(inv.Item_Tonnage__c)) );
                    
                    //to calculate summation of previous year column
                    if(!mapForTotalYTD.isEmpty() && mapForTotalYTD.containsKey('previousYearValue') && mapForTotalYTD.get('previousYearValue') > 0)
                        mapForTotalYTD.put('previousYearValue', (inv.Invoice_Value__c > 0 ? (mapForTotalYTD.get('previousYearValue') + inv.Invoice_Value__c) : mapForTotalYTD.get('previousYearValue') ));
                    else
                        mapForTotalYTD.put('previousYearValue', (inv.Invoice_Value__c > 0 ? inv.Invoice_Value__c : 0) );
                }
                System.debug('mapForTotalYTD*& '+mapForTotalYTD);
                
                
                
                if(inv.CreatedDate.Year() == Decimal.ValueOf(selectedYear)-1 && inv.CreatedDate.month() <= convertMonthToNumber){
                    //to calculcate summation of ytm tonnage column
                    if(!mapForTotalYTD.isEmpty() && mapForTotalYTD.containsKey('ytmTonnage') && mapForTotalYTD.get('ytmTonnage') > 0)
                        mapForTotalYTD.put('ytmTonnage', (inv.Item_Tonnage__c == null ? mapForTotalYTD.get('ytmTonnage') : (mapForTotalYTD.get('ytmTonnage') + Decimal.ValueOf(inv.Item_Tonnage__c)) ));
                    else
                        mapForTotalYTD.put('ytmTonnage', (inv.Item_Tonnage__c == null ? 0 : Decimal.ValueOf(inv.Item_Tonnage__c)) );
                        
                    //to calculate summation of ytm value
                    if(!mapForTotalYTD.isEmpty() && mapForTotalYTD.containsKey('ytmValue') && mapForTotalYTD.get('ytmValue') > 0)
                        mapForTotalYTD.put('ytmValue', (inv.Invoice_Value__c > 0 ? (mapForTotalYTD.get('ytmValue') + inv.Invoice_Value__c) : mapForTotalYTD.get('ytmValue') ));
                    else
                        mapForTotalYTD.put('ytmValue', (inv.Invoice_Value__c > 0 ? inv.Invoice_Value__c : 0) ); 
                }
                
                
                if(inv.CreatedDate.Year() == Decimal.ValueOf(selectedYear) && inv.CreatedDate.month() <= convertMonthToNumber){
                    //to calculate summation of ytd tonnage column
                    if(!mapForTotalYTD.isEmpty() && mapForTotalYTD.containsKey('ytdTonnage') && mapForTotalYTD.get('ytdTonnage') != null){
                        mapForTotalYTD.put('ytdTonnage', (inv.Item_Tonnage__c == null ? mapForTotalYTD.get('ytdTonnage') : (mapForTotalYTD.get('ytdTonnage') + Decimal.ValueOf(inv.Item_Tonnage__c))));
                        System.debug('lk '+mapForTotalYTD.get('ytdTonnage'));
                    }
                    else{
                        mapForTotalYTD.put('ytdTonnage', (inv.Item_Tonnage__c == null ? 0 : Decimal.ValueOf(inv.Item_Tonnage__c)) );
                        System.debug('lk-if-else '+mapForTotalYTD.get('ytdTonnage'));
                    }
                    
                    //to calculate summation of ytd value
                    if(!mapForTotalYTD.isEmpty() && mapForTotalYTD.containsKey('ytdValue') && mapForTotalYTD.get('ytdValue') > 0)
                        mapForTotalYTD.put('ytdValue', inv.Invoice_Value__c > 0 ? mapForTotalYTD.get('ytdValue') + inv.Invoice_Value__c : 0);
                    else
                        mapForTotalYTD.put('ytdValue', (inv.Invoice_Value__c > 0 ? inv.Invoice_Value__c : 0));
                }
                
                if(inv.CreatedDate.Year() == Decimal.ValueOf(selectedYear)-1 && inv.CreatedDate.month() == convertMonthToNumber){
                    //to calculate summation of mtm tonnage column
                    if(!mapForTotalYTD.isEmpty() && mapForTotalYTD.containsKey('mtmTonnage') && mapForTotalYTD.get('mtmTonnage') > 0)
                        mapForTotalYTD.put('mtmTonnage', (inv.Item_Tonnage__c == null ? mapForTotalYTD.get('mtmTonnage') : (mapForTotalYTD.get('mtmTonnage') + Decimal.ValueOf(inv.Item_Tonnage__c)) ));
                    else
                        mapForTotalYTD.put('mtmTonnage', (inv.Item_Tonnage__c == null ? 0 : Decimal.ValueOf(inv.Item_Tonnage__c)) );
                    
                    //to calculate summation of mtm value
                    if(!mapForTotalYTD.isEmpty() && mapForTotalYTD.containsKey('mtmValue') && mapForTotalYTD.get('mtmValue') > 0)
                        mapForTotalYTD.put('mtmValue', (inv.Invoice_Value__c > 0 ? (mapForTotalYTD.get('mtmValue') + inv.Invoice_Value__c) : mapForTotalYTD.get('mtmValue') ));
                    else
                        mapForTotalYTD.put('mtmValue', (inv.Invoice_Value__c > 0 ? inv.Invoice_Value__c : 0) );
                }
                
                if(inv.CreatedDate.Year() == Decimal.ValueOf(selectedYear) && inv.CreatedDate.month() == convertMonthToNumber){
                    //to calculate summation of mtd tonnage column
                    if(!mapForTotalYTD.isEmpty() && mapForTotalYTD.containsKey('mtdTonnage') && mapForTotalYTD.get('mtdTonnage') > 0)
                        mapForTotalYTD.put('mtdTonnage', (inv.Item_Tonnage__c == null ? mapForTotalYTD.get('mtdTonnage') : (mapForTotalYTD.get('mtdTonnage') + Decimal.ValueOf(inv.Item_Tonnage__c)) ));
                    else
                        mapForTotalYTD.put('mtdTonnage', (inv.Item_Tonnage__c == null ? 0 : Decimal.ValueOf(inv.Item_Tonnage__c)) );
                    
                    //to calculate summation of mtd value
                    if(!mapForTotalYTD.isEmpty() && mapForTotalYTD.containsKey('mtdValue') && mapForTotalYTD.get('mtdValue') > 0){
                        mapForTotalYTD.put('mtdValue', (inv.Invoice_Value__c > 0 ? (mapForTotalYTD.get('mtdValue') + inv.Invoice_Value__c) : mapForTotalYTD.get('mtdValue') ));
                        System.debug('**mapForTotalYTD**'+mapForTotalYTD);
                    }
                    else{
                        mapForTotalYTD.put('mtdValue', (inv.Invoice_Value__c > 0 ? inv.Invoice_Value__c : 0) );
                    } 
                    
                }
                System.debug('mapForTotalYTD -> '+mapForTotalYTD);
                System.debug('hebda Map ->'+mapOfAreaWrapper);        
            }         
        }
            catch(exception e){
                System.debug('exception happening here --> '+e);
            }
    }
    
    public void callTest(){
        for(integer i=0;i<=0;){
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
                i++;
                i++;
                i++;
                i++;
                i++;
                i++;
                i++;
                i++;
                i++;
                i++;
                i++;
                i++;
                i++;
                i++;
                    i++;
                    i++;
                    i++;
                    i++;
                    i++;
                    i++;
                    i++;
                    i++;
                    i++;
                    i++;
                    i++;
                    i++;
                  i++;
                        i++;
                        i++;
                        i++;
                        i++;
                        i++;
                        i++;
                        i++;
                        i++;
                        i++;
                        i++;
                        i++;
                        i++;
                     i++;
                               
             i++;
                                i++;
                                i++;
                                i++;
                                i++;
             i++;
                                i++;
                                i++;
                                i++;
                                i++;
            
        }
    }
    
}
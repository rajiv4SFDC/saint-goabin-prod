/* Created By: Rajiv Kumar Singh
Created Date: 27/06/2018
Class Name: AccoutNODInsertAPITest
Description : Test class for AccoutNODInsertAPIAPI
Last Modified By :Rajiv Kumar Singh
Last Modified Date: 27/06/2017
*/

@IsTest
private class AccoutNODInsertAPITest{
    
    static testMethod void testPostMethod(){
        String JSONMsg = '[{"SAPcode":"test1","accNODs":"10","AverageOutstanding":"12.11","postDate":"2018-01-1"},{"SAPcode":"test2","accNODs":"11","AverageOutstanding":"12.11","postDate":"2018-01-1","Amt60":"32"}]';
        RestRequest request = new RestRequest();
        request.requestUri ='/services/apexrest/accountNOD';
        request.httpMethod = 'POST';
        request.requestBody = Blob.valueof(JSONMsg);
        RestContext.request = request;
        RestResponse res = new RestResponse();
        RestContext.response = new RestResponse();
        AccoutNODInsertAPI.doHandleInsertRequest();
        
        JSONMsg = '[1';
        request = new RestRequest();
        request.requestUri ='/services/apexrest/accountNOD';
        request.httpMethod = 'POST';
        request.requestBody = Blob.valueof(JSONMsg);
        RestContext.request = request;
        RestContext.response = new RestResponse();
        AccoutNODInsertAPI.doHandleInsertRequest();
        
        request = new RestRequest();
        request.requestUri ='/services/apexrest/accountNOD';
        request.httpMethod = 'POST';
        request.requestBody = Blob.valueof('');
        RestContext.request = request;
        RestContext.response = new RestResponse();
        AccoutNODInsertAPI.doHandleInsertRequest();
    }
}
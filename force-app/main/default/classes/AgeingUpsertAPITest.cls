/* Created By: Rajiv Kumar Singh
Created Date: 11/06/2017
Class Name: AgeingUpsertAPITest
Description : Test class for AgeingUpsertAPI
Last Modified By :Rajiv Kumar Singh
Last Modified Date: 11/06/2017
*/

@IsTest
private class AgeingUpsertAPITest{
    
    static testMethod void testPostMethod(){
        
        String JSONMsg = '[{"Value1":32.22,"Value2":1,"Value3":2.9,"Value4":3.12,"Value5":4.21,"Value6":5.23,"Value7":6.12,"Value8":"7.12","SAPCode":"212213","osBal":8.21}]';
        RestRequest request = new RestRequest();
        request.requestUri ='/services/apexrest/ageingUpsertAPI';
        request.httpMethod = 'POST';
        request.requestBody = Blob.valueof(JSONMsg);
        RestContext.request = request;
        RestContext.response = new RestResponse();
        AgeingUpsertAPI.doHandleInsertRequest();
        
        JSONMsg = '[1';
        request = new RestRequest();
        request.requestUri ='/services/apexrest/ageingUpsertAPI';
        request.httpMethod = 'POST';
        request.requestBody = Blob.valueof(JSONMsg);
        RestContext.request = request;
        RestContext.response = new RestResponse();
        AgeingUpsertAPI.doHandleInsertRequest();
        
        request = new RestRequest();
        request.requestUri ='/services/apexrest/ageingUpsertAPI';
        request.httpMethod = 'POST';
        request.requestBody = Blob.valueof('');
        RestContext.request = request;
        RestContext.response = new RestResponse();
        AgeingUpsertAPI.doHandleInsertRequest();
    }
}
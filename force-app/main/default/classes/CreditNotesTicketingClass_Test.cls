/******************************************
* Created By  : Ajeet Singh Shekhawat
* Created On  : 20 Nov 2018
* Modified By : Ajeet Singh Shekhawat
* Modified On : 20 Nov 2018
* Description :

******************************************/ 

@isTest
public class CreditNotesTicketingClass_Test {
    static testMethod void creditNoteMethod() {
        
        Id creditNoteId;
        State__c st = new State__c();
        st.Name = 'Karnataka';
        Insert st;
        
        City__c ct = new City__c();
        ct.Name = 'Banglore';
        ct.State__c = st.Id;
        Insert ct;
        
        Project__c project = new Project__c();
        project.Name = 'kvp';
        project.Segment__c = 'Retail';
        project.Region__c = 'East';
      //  project.Sales_Area__c = 'EPR1';
        project.Validated__c = true;
        Insert project;
        
        account  acc2 = new account();
        acc2.Name ='main acc for erp';
        acc2.Type ='Industry2';
        acc2.PAN_Number__c ='AAAPL1234f';
        acc2.Sales_Area_Region__c ='South';
        acc2.sap_code__c = '123123';
        acc2.Sales_Office_ERP__c = 'BLR1';
        acc2.Clear_Tint_Tier__c = 'T1';
        acc2.RecordTypeId = Schema.SObjectType.account.getRecordTypeInfosByName().get('ERP').getRecordTypeId();
        insert acc2;
        
        Account acc1 = new Account();
        acc1.Name = 'KvpTest';
        acc1.Type = 'INSPIRE';
        acc1.Sales_Area_Region__c = 'East';
        acc1.Sales_Office_ERP__c = 'EPR1';
        acc1.Reflectasol_Tier__c = 'T1';
        acc1.Mirror_Tier__c = 'T1';
        acc1.Interior_Tier__c = 'T1';
        acc1.Inspire_Tier__c = 'T1';
        acc1.ConditionGroup5__c = 'T1';
        //acc.SAP_Code__c = '123456';
       // acc.RecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('ERP').getRecordTypeId();
        Insert acc1;
       
        
        Opportunity oppo = new Opportunity();
        oppo.Name = 'testoppo';
        oppo.Project__c = project.Id;
        oppo.AccountId = acc2.Id;
        oppo.Type = 'CLEAR';
        oppo.StageName = 'Specified';
        oppo.Probability = 10;
        oppo.CloseDate = System.today();
    Insert oppo;
        
        Key_Account__c keyAcct = new Key_Account__c();
        keyAcct.Opportunity__c = oppo.Id;
        keyAcct.Account__c = acc1.Id;
        insert keyAcct;
        
        Product2 pro = new Product2();
        pro.Name = 'test prod';
        pro.Product_Category__c ='CLEAR';
        pro.ProductCode ='test123';      
        pro.Article_number__c = 'test123';
        pro.Quantity__c  = 2;
        pro.global_product__c = true;
        pro.external_id__c = 'testExternalId';
        insert pro;
        
        Product2 p2 =[select id,name,external_id__c from Product2 where id=:pro.id];
        
        Product_Tier_Pricing__c ptr = new Product_Tier_Pricing__c();
        ptr.name ='testing tier';
        ptr.Business_Location__c ='BLR1';
        ptr.Product__c = pro.id;
        ptr.Product_Code__c ='test12312';
        ptr.Tier_Name__c ='T1';
        ptr.Thickness__c =22;
        ptr.Item_Code__c = p2.external_id__c;
        insert ptr;
        
        Product_Stock__c pdStock = new Product_Stock__c();
        pdStock.Plant_Code__c = '100';
        Insert pdStock;
            
        Invoice__c inv = new Invoice__c();
        inv.Name = 'pr1-7';
        inv.Invoice_Line_Item_No__c = '100';
        inv.Item_Tonnage__c = 'pr1-7';
        inv.Invoice_Rate__c = 10.6;
        inv.Quantity__c = 20.5;
        inv.Product__c = pro.Id;
        Insert inv;
        
        Opportunity_Product__c oppoPro = new Opportunity_Product__c();
        oppoPro.Product__c = pro.Id;
        oppoPro.Status__c = 'Selected';
        oppoPro.Opportunity__c = oppo.Id;
        Insert oppoPro;
        
        price_request_PandP__c pr;
        try {
        Price_Request__c pr1 = new Price_Request__c();
        pr1.Approval_Status__c = 'Approved';
        pr1.Opportunity_Product__c = oppoPro.Id;
        pr1.Requested_Quantity__c = 3;
        pr1.Price_Type__c = 'List Price';
        PR1.Customer__c =acc2.id;
        Insert pr1;
        
         pr = new price_request_PandP__c();
       // pr.Price_Type__c   = 'Tier Price';
        //pr.Opportunity_Product__c = oppproduct.id;
        pr.Valid_From__c = System.today();
        pr.Valid_To__c = System.today();
        pr.Requested_Quantity__c = 1000;
       // pr.Approval_status__c ='Approved';
        pr.Regional_head_sales_and_marketing_User__c = userInfo.getUserId();
        pr.Regional_Manager__c =  userInfo.getUserId();
        //pr.Enquiry_Line_Item__c = enitem.id;
        //pr.Enquiry__c = enc.id;
        pr.Required_Price__c = 200;
        pr.Customer__c = acc2.id;
        pr.Product_lookup__c = pro.id;

      
       // pr.Regional_manager_special_price__c =  userInfo.getUserId();
        insert pr;
        System.debug('price request pandp Id is ' + pr.Id);
        }
        catch(exception ert)
       {
           //system.debug('The ert is:'+ert+ert.getLineNumber());
       }
            
        String invoiceName = 'pr1-7';
        
        CreditNotesTicketingClass.getSelectedInvoices(invoiceName);
        CreditNotesTicketingClass.getPriceRequestRecords(pro.Id);
        try {
            System.debug('price request pandp Id is *** ' + pr.Id);
            CreditNotesTicketingClass.getPriceRequestPandPRecords(pr.Id);
        } catch(exception ert) {
            //system.debug('The ert is:'+ert+ert.getLineNumber());
        }
        
        
        List<Credit_Notes_Ticketing__c> crList = new List<Credit_Notes_Ticketing__c>();
        Credit_Notes_Ticketing__c creditNote = new Credit_Notes_Ticketing__c();
      //  creditNote.Approval_Status__c = 'Pending';
        creditNote.Quantity__c = '100';
        creditNote.Product_Name__c = 'Antalioe';
        creditNote.Remarks__c = 'NO Comment';
        creditNote.Plant_Location__c = 'BTM';
        creditNote.Item_Tonnage__c = 'test';
        creditNote.Invoice_Rate__c = '10';
        creditNote.Thickness__c = '50';
        creditNote.Dimension__c = '4';
        creditNote.Invoice__c = inv.id;
        
        //creditNote.Price_Request__c = pr.Id;
        crList.add(creditNote);
        Insert crList;
        String creditData = JSON.serialize(crList);
        System.debug('creditData *** ' +creditData );
        
        CreditNotesTicketingClass.saveCreditNotesTickt(creditData);
        CreditNotesTicketingClass.editCreditNotesTickt(creditData, crList[0].Id);
        CreditNotesTicketingClass.getInsertedRecords(crList[0].Id);
        
    }
}
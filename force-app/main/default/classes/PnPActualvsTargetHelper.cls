/**
* Created Date: 17-Sep-2018
* Created By: KVP Business Solutions
* 
*/

public class PnPActualvsTargetHelper{
    
    public Map<String,target_vs_actuals_P_P__c> ActualsFromInvoice (List<invoice__c> InvoiceLst,Map<String,target_vs_actuals_P_P__c> targetActualMap){
        //   Map<String,Boolean> mapOfTargetandHasError = new Map<String,Boolean>();
        system.debug('@9 identifier --> Entering');  
        for(invoice__C inv : InvoiceLst){ 
            String identifier = getIdentifier( inv.Sales_Office__c,inv.Reporting_range__c,date.newinstance(inv.Billing_date__c.year(), inv.Billing_date__c.month(), inv.Billing_date__c.day()));
            
            system.debug('@9 identifier --> '+identifier);
            
            if( targetActualMap.containsKey(identifier)){
                system.debug('--TvA--> Enterd');
                target_vs_actuals_P_P__c tvav = targetActualMap.get(identifier);
                try{
                    tvav.Actual__c = tvav.Actual__c + inv.Item_Tonnage_formula__c;
                }catch(Exception e){
                    system.debug('Error Occured Message Is-->'+e.getMessage());
                    tvav.Actual__c = 0;
                    //     mapOfTargetandHasError.put(identifier,true);
                }
                
                targetActualMap.put(identifier,tvav);
            }
        }
        return targetActualMap; 
    }
    
    public String getIdentifier(String SalesOffice,String ReportingRange,Date recordDate){
        String identifier = SalesOffice + '-' + ReportingRange +'-' +  getMonthAggregrateName( recordDate.month() ) + ',' + recordDate.year();
        system.debug('@39 string contains-->'+identifier);
        return identifier;
    } 
    public String getMonthAggregrateName(Integer monthNum){
        
        if(monthNum == 1){
            return 'JAN';
        }else if(monthNum == 2){
            return 'FEB';
        }
        else if(monthNum == 3){
            return 'MAR';
        }
        else if(monthNum == 4){
            return 'APR';
        }
        else if(monthNum == 5){
            return 'MAY';
        }
        else if(monthNum == 6){
            return 'JUN';
        }
        else if(monthNum == 7){
            return 'JUL';
        }
        else if(monthNum == 8){
            return 'AUG';
        }
        else if(monthNum == 9){
            return 'SEPT';
        }   
        else if(monthNum == 10){
            return 'OCT';
        }
        else if(monthNum == 11){
            return 'NOV';
        }
        else if(monthNum == 12){
            return 'DEC';
        }
        else{
            return 'blank';
        }
    }
}
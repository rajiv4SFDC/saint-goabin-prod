/* Created By: Rajiv Kumar Singh
Created Date: 15/11/2017
Class Name: EnquiryStatusUpdateUpsertAPITest
Description : Test class for EnquiryStatusUpdateUpsertAPI
Last Modified By :Rajiv Kumar Singh
Last Modified Date: 15/11/2017
*/

@IsTest
private class EnquiryStatusUpdateUpsertAPITest{
    
    static testMethod void testPostMethod(){
        
        String JSONMsg = '[{"EnqReferencenum":"1234","EnqLineReferencenum":"1234","ConfirmedQty":"12.42","OrderQuantity":"12.42","QtyinShipment":"12.42","ServicedQty":"12.42","ShortClosedQty":"12.42"}]';
        RestRequest request = new RestRequest();
        request.requestUri ='/services/apexrest/EnquiryStatusUpdate';
        request.httpMethod = 'POST';
        request.requestBody = Blob.valueof(JSONMsg);
        RestContext.request = request;
        RestContext.response = new RestResponse();
        EnquiryStatusUpdateUpsertAPI.doHandleInsertRequest();
        
        JSONMsg = '[1';
        request = new RestRequest();
        request.requestUri ='/services/apexrest/EnquiryStatusUpdate';
        request.httpMethod = 'POST';
        request.requestBody = Blob.valueof(JSONMsg);
        RestContext.request = request;
        RestContext.response = new RestResponse();
        EnquiryStatusUpdateUpsertAPI.doHandleInsertRequest();
        
        request = new RestRequest();
        request.requestUri ='/services/apexrest/EnquiryStatusUpdate';
        request.httpMethod = 'POST';
        request.requestBody = Blob.valueof('');
        RestContext.request = request;
        RestContext.response = new RestResponse();
        EnquiryStatusUpdateUpsertAPI.doHandleInsertRequest();
    }
}
/* Created By: Rajiv Kumar Singh
Created Date: 27/06/2018
Class Name: SalesOfficeNODInsertAPI 
Story : Sales Office NOD flow from SAP to SFDC
Last Modified By :Rajiv Kumar Singh
Last Modified Date: 27/06/2018
*/

@RestResource(urlMapping='/salesOfficeNOD')
global class SalesOfficeNODInsertAPI{
    @HttpPost
    global static void doHandleInsertRequest(){
        
        List<SalesOfficeNODWrapper> wrapList = new List<SalesOfficeNODWrapper>();
        List<Sales_Office_Wise_NOD_s__c> NODList = new List<Sales_Office_Wise_NOD_s__c>();
        RestRequest Req = RestContext.request;
        RestResponse res = RestContext.response;
        String body = Req.requestBody.toString().trim();
        
        if(body != null && body != '') {
            
            try {
                wrapList = (List<SalesOfficeNODWrapper>)JSON.deserialize(body, List<SalesOfficeNODWrapper>.class);
                
                for(SalesOfficeNODWrapper we :wrapList){
                    Sales_Office_Wise_NOD_s__c SalesOfficeNOD = new Sales_Office_Wise_NOD_s__c();
                    
                    if(we.SalesOfficeName!= null && we.SalesOfficeName!=''){
                        SalesOfficeNOD.Sales_Office_Name__c = we.SalesOfficeName;
                    }
                    if(we.SalesNODs!= null){
                        SalesOfficeNOD.No_of_Days__c = we.SalesNODs;
                    } 
                    if(we.AverageOutstanding!= null){
                        SalesOfficeNOD.Average_Outstanding__c = we.AverageOutstanding;
                    }
                    if(we.postDate!= null){
                        SalesOfficeNOD.Posting_Date__c = we.postDate;
                    }
                    if(we.Amt60!= null && we.Amt60!=''){
                        SalesOfficeNOD.Amt_60_Days__c = we.Amt60;
                    }
                     
                    NODList.add(SalesOfficeNOD);
                }
                
                if(NODList.size() > 0){
                    Insert NODList;
                }
                
                res.statusCode = 200;
                res.responseBody = Blob.valueOf(String.valueof(NODList));
                
            }catch (Exception e) {
                
                CommonUtilitiesHandler.handleExceptions(e);
                res.statusCode = 500;
                res.responseBody = Blob.valueOf(e.getMessage());
            }
        } else {
            res.statusCode = 500;
            res.responseBody = Blob.valueOf('Invalid Request');
        }
    }
    
    public class SalesOfficeNODWrapper{
        public String SalesOfficeName{get;set;}
        public Integer SalesNODs{get;set;}
        public Decimal AverageOutstanding{get;set;}
        public Date postDate{get;set;}
        public string Amt60{get;set;}
       
        
    }
}
public with sharing class FetchWelcomeUserName {
    @AuraEnabled 
    public static user fetchUser() {
        // query current user information  
        User usrName = [select id, Name FROM User Where id =: userInfo.getUserId()];
        return usrName;
    }
}
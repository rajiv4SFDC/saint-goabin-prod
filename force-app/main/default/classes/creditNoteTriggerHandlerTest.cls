@isTest
public class creditNoteTriggerHandlerTest {

  
   public  static testmethod void creditNoteTriggerHandlermethod1(){
        id cnid;
        Account acc1 = new Account();
        acc1.Name ='test31222';
        acc1.Type ='Industry';
        acc1.SAP_Code__c='12314';
        acc1.Mirror_Tier__c ='T1';
        acc1.Sales_Area_Region__c ='North';
        acc1.Sales_Office_ERP__c='BLR';
        acc1.RecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('ERP').getRecordTypeId();
        insert acc1;
        
        Opportunity opp = new Opportunity();
        String oppid1;
        opp.AccountId =acc1.Id;
        opp.Name ='test1';
        opp.CloseDate = Date.today();
        opp.Amount = 1000;
        opp.StageName ='Prospecting';
        opp.Probability= 0.5;
        oppid1=acc1.id;
        insert opp;
        
        Account acc = new Account();
        acc.Name ='test312';
        acc.Type ='Industry';
        acc.Sales_Area_Region__c ='South';
        acc.SAP_Code__c='12345';
        acc.RecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('CRM').getRecordTypeId();
        insert acc;
        
        Key_Account__c keyAcct = new Key_Account__c();
        keyAcct.Opportunity__c = opp.Id;
        keyAcct.Account__c = acc.Id;
        insert keyAcct;
        
        opp.Customer_Name__c = keyAcct.Id;
        update opp;
        
        Product2 masterProduct = new Product2();
        masterProduct.Name = opp.id;
        masterProduct.Article_number__c ='test1234';
        masterProduct.Product_Category__c ='Shower Cubicles';
        masterProduct.master_product__c = true;
        insert masterProduct;
        
        Product2 pro = new Product2();
        pro.Name = 'test prod123';
        pro.Product_Category__c ='Shower Cubicles';
        pro.ProductCode ='test12312';
        pro.Parent_product__c = masterProduct.id;
        pro.Article_number__c = 'test1231';
        pro.Type__c ='Mirror';
        pro.Quantity__c  = 2;
        pro.global_product__c = true;
        insert pro;   
        List<Product2> prodList = new List<product2>();
        prodList.add(pro);
        
        Product_Tier_Pricing__c ptr = new Product_Tier_Pricing__c();
        ptr.name ='testing tier';
        ptr.Business_Location__c ='BLR';
        ptr.Product__c = pro.id;
        ptr.Product_Code__c ='test12312';
        ptr.Tier_Name__c ='T1';
        ptr.Thickness__c =22;
        insert ptr;
        
        
        
        Opportunity_Product__c oppproduct = new Opportunity_Product__c();
        oppproduct.Opportunity__c = opp.Id;
        oppproduct.Product__c = pro.Id;
        insert oppproduct;
        
        Opportunity_Product_with_Account__c op = new Opportunity_Product_with_Account__c();
        op.Account__c =acc1.id;
        op.Opportunity_Product__c =oppproduct.id;
        insert op;
      
       Credit_Note__c cn = new Credit_Note__c();
       cn.Opportunity__c = opp.id;
       cn.Opportunity_Product__c =oppproduct.id;
       cn.Customer_Name__c =keyAcct.id;
       cn.ERP_Account__c =acc1.id;
       insert cn;
       cnid = cn.id;
       
  
        map<id,Credit_Note__c> mapcn = new map<id,Credit_Note__c>();
        List<Credit_Note__c>cnReq = [SELECT Id, Opportunity__c,Credit_Note__c.ERP_Account__c,Credit_Note__c.Total_Quantity_sqm__c, Opportunity_Product__c ,Customer_Name__c FROM Credit_Note__c where ID=:cnid];
        mapcn.put(cnReq[0].id,cnReq[0]);
        creditNoteTriggerHandler.AfterInserthandler(cnReq);
        creditNoteTriggerHandler.BeforeInserthandler(cnReq);
        update cn;
       creditNoteTriggerHandler.AfterUpdatehandler(cnReq,mapcn);
       creditNoteTriggerHandler.rollupAvlQantitity(cnReq);
           
    }
}
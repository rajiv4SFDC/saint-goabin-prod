/*********************************************************************************      
* Name: CommonUtilitiesHandler
* Description: This class handles all the common flow and functionalities
* Created By: Ashim Kumar Sahoo
* Created Date: 31/05/2018 
**********************************************************************************/
public class CommonUtilitiesHandler {
	
    public static String module;
    public static void handleExceptions(Exception e) {
        
        Error_Manager__c errMgr = new Error_Manager__c();
        
        if(CommonUtilitiesHandler.module != null) {
        	
            errMgr.Module__c = CommonUtilitiesHandler.module;
        }
        errMgr.Error_Stack_Trace__c = e.getStackTraceString();
        errMgr.Error_Type__c = e.getTypeName();
        errMgr.Error_Message__c = e.getMessage();
        
        insert errMgr;
        
        /*
        Messaging.SingleEmailMessage mail=new Messaging.SingleEmailMessage();
        String[] toAddresses = new String[] {'rajiv.singh@kvpcorp.com', 'sai.prasanth@kvpcorp.com', 'akshay.k@kvpcorp.com'};
        mail.setToAddresses(toAddresses);
        mail.setSenderDisplayName('St. Gobain: Error message');
        mail.setSubject('Error from Org : ' + UserInfo.getOrganizationName());
        mail.setHtmlBody('<b>Error Message</b>:'+e.getMessage()+'<br/><br/><b>Stack Trace</b>:'+e.getStackTraceString());
        mail.setPlainTextBody(e.getMessage()+'---'+e.getStackTraceString());
        Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail });
*/
	}
}
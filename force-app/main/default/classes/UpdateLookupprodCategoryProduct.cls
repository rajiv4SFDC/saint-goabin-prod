/* Created By         : Rajiv Kumar Singh
   Created Date       : 13/12/2018
   Class Name         : UpdateLookupprodCategoryProduct
   Description        : update the particular Product category lookup field to Product
   Last Modified By   : Rajiv Kumar Singh
   Last Modified Date : 13/12/2018
*/
public class UpdateLookupprodCategoryProduct{
  @InvocableMethod(label='UpdateLookupprodCategoryProduct' description='update the particular Product category lookup field to Product')
  public static void UpdateLookupValue(List<Id> shipTOCustomerIds)
    {   
       map<string,string> productDimensionWithAccountMap = new map<string,string>();  
       set<string> presentSAPCodesSet = new set<String>();
       list<Product2> NewList = new list<Product2>(); 
       list<Product2> FinalListToUpdate = new list<Product2>();

       for(Product2 ptp:[select id,Sub_Category__c from Product2 where id in:shipTOCustomerIds])
       {
         if(ptp.Sub_Category__c != null)
          {
            presentSAPCodesSet.add(ptp.Sub_Category__c);      
            NewList.add(ptp);
            system.debug('The ptp.id is: ' +ptp.id);
          }     
       }
       system.debug('The NewList is: ' +NewList+'......'+presentSAPCodesSet);
       if(presentSAPCodesSet.size()>0)
       {
          for(Product_Category__c acc:[select id,name  from Product_Category__c where name in:presentSAPCodesSet])
           {
               productDimensionWithAccountMap.put(acc.name,acc.id);
           }
           system.debug('The productDimensionWithAccountMap is: ' +productDimensionWithAccountMap);
           for(Product2 ptp:NewList)
           {
               ptp.Product_Master_Category__c = productDimensionWithAccountMap.get(ptp.Sub_Category__c);
               //system.debug('The  ptp.Product__c is: ' + ptp.Product__c);
               FinalListToUpdate.add(ptp);
           }
       }
       if(!FinalListToUpdate.isEmpty() && FinalListToUpdate.size() > 0) {
            update FinalListToUpdate;
       }
    }
}
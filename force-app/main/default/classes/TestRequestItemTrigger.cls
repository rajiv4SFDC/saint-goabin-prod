@isTest
public class TestRequestItemTrigger {

		@testSetup 
		public static void createTestData(){
			TestDataFactory.createCustomSetting();
		TestDataFactory.createTestProductData();
		TestDataFactory.createTestStateAndCityRecordData();
		TestDataFactory.createTestCatalogueBudgetData();
		TestDataFactory.createTestCampaignRecordData();
		TestDataFactory.createTestAccountData();
		TestDataFactory.createTestProjectData();
		TestDataFactory.createTestOpportunityData();
		TestDataFactory.createTestOpportunityProductData();
		TestDataFactory.createTestPriceRequestData();
		TestDataFactory.createTestProductForecateData();
		TestDataFactory.createTestForecateInvoiceData();
		TestDataFactory.createTestMockupRecordData();
		TestDataFactory.createTestInvoiceRecordData();
		TestDataFactory.createTestTargetActualRecordData();
		}

		@isTest
		public static void createCatalogueRequest(){
			Id catalogueRecordTypeId = Schema.SObjectType.Merchandise_Request__c.getRecordTypeInfosByName().get('Catalogue').getRecordTypeId();
			List<Product2> lstOfProd = [SELECT id FROM Product2 WHERE Family Like 'Catalogue%'];
			List<Request_Item__c> lstOfReqItem = new List<Request_Item__c>();

			Merchandise_Request__c mrCatalogue = new Merchandise_Request__c();
			mrCatalogue.RecordTypeId = catalogueRecordTypeId;
			mrCatalogue.Requesting_Date__c = Date.newInstance(2017,08, 28);  
			mrCatalogue.Status__c = 'Draft';
            mrCatalogue.Address__c = 'Test';
            mrCatalogue.Pin_Code__c ='201007';
            mrCatalogue.Contact_No__c  = '9898989898';
            mrCatalogue.State_cus__c  = 'State';
            mrCatalogue.City_cus__c = 'City';
            mrCatalogue.Copy_Address_from_Contact__c=false;
            mrCatalogue.Send_To_Self__c =false;
            insert mrCatalogue;

			for( Product2 prod : lstOfProd){
				
				Request_Item__c reqItem = new Request_Item__c();
				reqItem.Quantity_Requested__c = 500;
				reqItem.Product__c = prod.id;
				reqItem.Request_No__c = mrCatalogue.id;
				lstOfReqItem.add(reqItem);
			}

			/*Add Same Catalogue Product twice */
				Request_Item__c reqItem = new Request_Item__c();
				reqItem.Quantity_Requested__c = 500;
				reqItem.Product__c = lstOfProd.get(0).id;
				reqItem.Request_No__c = mrCatalogue.id;
				lstOfReqItem.add(reqItem);
			
			insert lstOfReqItem;	

			update lstOfReqItem;

			/* On Approved Catalogue Request */
			mrCatalogue.Status__c = 'Approved';
			update mrCatalogue;

			/*Create Catalogue Request without updating previouely approved request shipping status */
			try{
				
				Merchandise_Request__c mrCatalogueErr = new Merchandise_Request__c();
				mrCatalogueErr.RecordTypeId = catalogueRecordTypeId;
				mrCatalogueErr.Requesting_Date__c = Date.newInstance(2017,09, 5);  
				mrCatalogueErr.Status__c = 'Draft';
                mrCatalogueErr.Address__c = 'Test';
                mrCatalogueErr.Contact_No__c  = '9898989898';
                mrCatalogueErr.Pin_Code__c ='201007';
                mrCatalogueErr.State_cus__c  = 'State';
                mrCatalogueErr.City_cus__c = 'City';
				mrCatalogueErr.Copy_Address_from_Contact__c=false;
            	mrCatalogueErr.Send_To_Self__c =false;
                
				Merchandise_Request__c mrCatalogueErr2 = new Merchandise_Request__c();
				mrCatalogueErr2.RecordTypeId = catalogueRecordTypeId;
				mrCatalogueErr2.Requesting_Date__c = Date.newInstance(2017,09, 5);  
				mrCatalogueErr2.Status__c = 'Draft';
                mrCatalogueErr2.Address__c = 'Test';
                mrCatalogueErr2.Pin_Code__c ='201007';
                mrCatalogueErr2.Contact_No__c  = '9898989898';
                mrCatalogueErr2.State_cus__c  = 'State';
                mrCatalogueErr2.City_cus__c = 'City';
				mrCatalogueErr2.Copy_Address_from_Contact__c=false;
            	mrCatalogueErr2.Send_To_Self__c =false;
                
				List<Merchandise_Request__c> lstOFMerchandiseReqErr = new List<Merchandise_Request__c>();
				lstOFMerchandiseReqErr.add(mrCatalogueErr);
				lstOFMerchandiseReqErr.add(mrCatalogueErr2);

				insert lstOFMerchandiseReqErr;
				
			}catch(Exception e){

			}

		}

}
/**
* Created Date: 17-Sep-2018
* Created By: KVP Business Solutions For Actuals and Target calculations PnP
* Changes done date: 14-Jan-2018 
*/
global class PnPTargetvsActualBatch implements Database.Batchable<sObject>{
    global Integer monthNum;
    global Integer yearNum; 
    
    global PnPTargetvsActualBatch(Date executedate){
        monthNum = executedate.month();
        yearNum = executedate.year();
    }
    
    global PnPTargetvsActualBatch(){
        monthNum = system.today().month();
        yearNum = system.today().year();
    }
    
    global Database.QueryLocator start(Database.BatchableContext bc){
        String query = 'select id,name,Region__c,Reporting_Range__c,Sales_Area__c,sales_office__c,target__c,target_date__c,target_month__c,actual__c,Unique_Target_Actual_Identifier__c from target_vs_actuals_P_P__c where CALENDAR_MONTH(Target_Date__c)=:monthNum AND CALENDAR_YEAR(Target_Date__c)=:yearNum';
        
        //For debug purpose code starts
        Database.QueryLocator q = Database.getQueryLocator(query);
        Database.QueryLocatorIterator it =  q.iterator();
        while (it.hasNext())
        {
            target_vs_actuals_P_P__c a = (target_vs_actuals_P_P__c)it.next();
           //  System.debug('targets ---> '+a);
        }
        
        // debug purpose code ends
        return Database.getQueryLocator(query);
        
    }
    
    global void execute(Database.BatchableContext bc, List<target_vs_actuals_P_P__c> TargetvsActualList){
        set<String> SalesOfficeSet = new set<String>();
        map<String,target_vs_actuals_P_P__c> targetmap = new map<String,target_vs_actuals_P_P__c>();
        
        for(target_vs_actuals_P_P__c tva : TargetvsActualList){
            // Make Actuals zero for running month
            tva.Actual__c = 0;
            targetmap.put(tva.Unique_Target_Actual_Identifier__c,tva);
            SalesOfficeSet.add(Tva.sales_office__c);
        }
       // system.debug('SalesOfficeSet--  -- '+SalesOfficeSet);
        List<invoice__c> InvoiceLst = [SELECT id,sales_office__c,billing_date__c,Item_Tonnage_formula__c,Reporting_range__c FROM invoice__c WHERE Sales_Office__c IN: SalesOfficeSet AND ( CALENDAR_MONTH(billing_date__c) =: monthNum AND CALENDAR_YEAR(billing_date__c) =: yearNum )];
        Map<String,target_vs_actuals_P_P__c> tvavMap;
        PnPActualvsTargetHelper tvavHandler = new PnPActualvsTargetHelper();
        tvavMap = tvavHandler.ActualsFromInvoice(InvoiceLst,targetmap);
        
        if(tvavMap.size()>0)
            update tvavMap.values();
    }
    
    global void finish(Database.BatchableContext bc){ 
        String backupDateWindow = System.label.TargetActualBackUpdateOpenWindow;
        Integer daysinWindow = Integer.valueOf(backupDateWindow);
        Boolean recurrsionChk = CheckRecursive.runOnce();
        
        IF( recurrsionChk && System.today().day() <= daysinWindow){
            Date executedate = Date.newInstance(system.today().year(),system.today().month(), system.today().day());
            PnPTargetvsActualBatch tvavbatch = new PnPTargetvsActualBatch(executedate);
            Database.executebatch(tvavbatch,5);
        }
    }
}
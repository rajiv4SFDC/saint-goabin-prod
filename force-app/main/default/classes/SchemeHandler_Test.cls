@isTest
public class SchemeHandler_Test{
    static testmethod void beforeInsert(){
        Id schemeRecordTypeId = Schema.SObjectType.Distribution_Scheme__c.getRecordTypeInfosByName().get('External').getRecordTypeId();
        
        Account acc = new Account();
        acc.Name = 'ads';
        acc.SAP_Code__c = 'qwe';
        insert acc;
        
        Distribution_Scheme__c ds = new Distribution_Scheme__c();
        ds.Type_of_Scheme__c = 'Annual Tie Ups';
        insert ds;
        
        
        Schemes__c sch = new Schemes__c();
        sch.Distribution_Scheme__c = ds.id;
        sch.Account__c = acc.id;
        insert sch;
        
        
    } 
}
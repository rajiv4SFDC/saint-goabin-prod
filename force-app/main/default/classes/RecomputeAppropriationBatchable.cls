global class RecomputeAppropriationBatchable implements Database.Batchable<sObject> {
	  global final Set<Id> priceRequestSet;
    global RecomputeAppropriationBatchable(Id prId){priceRequestSet = new Set<Id>{prId};}
    global RecomputeAppropriationBatchable(Set<Id> prSet){priceRequestSet = prSet;}
    global Database.QueryLocator start(Database.BatchableContext BC){
      	String query = 'SELECT Id FROM Invoice__c WHERE Price_Request__c IN :priceRequestSet';
        System.debug('Invoice Tagging Batchable ' + query);
      	return Database.getQueryLocator(query);
   	}

   	global void execute(Database.BatchableContext BC, List<sObject> scope){
     	//Reset Flags on Invoices
     	Set<Id> invoiceIdSet = new Set<Id>();
        for(Invoice__c inv : (List<Invoice__c>) scope) {
            inv.Processed__c = false;
            inv.To_be_processed__c = true;
            invoiceIdSet.add(inv.id);
        }
        update scope;
        
        //Initiate the process of recalculation
        InvoiceAppropriationHelper.appropriateInvoice(invoiceIdSet,true);
        
    }

   	global void finish(Database.BatchableContext BC){}
}
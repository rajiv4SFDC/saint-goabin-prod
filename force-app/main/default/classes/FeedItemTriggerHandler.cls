public class FeedItemTriggerHandler extends TriggerHandler{

	public FeedItemTriggerHandler() {}
	
	public override void afterInsert() {
    	FileOperationHandler.increaseImageCountFrmFeedItem( (List<FeedItem>)Trigger.new);
    }

}
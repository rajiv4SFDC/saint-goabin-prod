@istest
Private class GetProductAndLineItems_test
{
  @isTest
     private static void testMethod1()
     {
         Account acc = new Account();
        acc.Name ='test3';
        acc.Type ='Industry';
        acc.Sales_Area_Region__c ='North';
        acc.RecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('ERP').getRecordTypeId();
        insert acc;
               Id inspireoppId = Schema.SObjectType.Opportunity.getRecordTypeInfosByName().get('Inspire').getRecordTypeId();

        Opportunity opp = new Opportunity();
        String oppid1;
        opp.AccountId =acc.Id;
        opp.Name ='test1';
        opp.CloseDate = Date.today();
        opp.Amount = 1000;
        opp.StageName ='Prospecting';
        opp.Probability= 0.5;
        oppid1=acc.id;
        opp.recordTypeId =inspireoppId;
        insert opp;
        
        acc = new Account();
        acc.Name ='test31';
        acc.Type ='Industry1';
        acc.Sales_Area_Region__c ='South';
        acc.RecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('CRM').getRecordTypeId();
        insert acc;
        
        Key_Account__c keyAcct = new Key_Account__c();
        keyAcct.Opportunity__c = opp.Id;
        keyAcct.Account__c = acc.Id;
        insert keyAcct;
        
        opp.Customer_Name__c = keyAcct.Id;
        update opp;
        
        Product2 masterProduct = new Product2();
        masterProduct.Name = opp.id;
        masterProduct.Article_number__c ='test1234';
        masterProduct.Product_Category__c ='Shower Cubicles';
        masterProduct.master_product__c = true;
        insert masterProduct;
        
        Product2 pro = new Product2();
        pro.Name = 'test prod';
        pro.Product_Category__c ='Shower Cubicles';
        pro.ProductCode ='test123';
        pro.Parent_product__c = masterProduct.id;
        pro.Article_number__c = 'test123';
        pro.Quantity__c  = 2;
        pro.global_product__c = true;
        insert pro;   
        List<Product2> prodList = new List<product2>();
        prodList.add(pro);
        
        List<ProductsWrapper> prodWrapper =  new List<ProductsWrapper>();
        ProductsWrapper prodWrap = new ProductsWrapper();
        prodWrap.prdName = pro.Name;
        prodWrap.prdCode = pro.ProductCode;
        prodWrap.prdQuantitiy = Integer.valueof(pro.Quantity__c);
        prodWrap.prdArticlenumber = pro.Article_number__c;
        prodWrap.prdParentId = pro.Parent_product__c;
        //prodWrap.oppProductLineItemId = pro.Name;
        prodWrapper.add(prodWrap);
        
        Opportunity_Product__c oppproduct = new Opportunity_Product__c();
        oppproduct.Opportunity__c = opp.Id;
        oppproduct.Product__c = masterProduct.Id;
        insert oppproduct;
        
        Opportunity_Product_line_items__c oppline = new Opportunity_Product_line_items__c();
        oppline.Opportunity_Product__c=oppproduct.id;
        oppline.Product__c = pro.Id;
        oppline.Quantity__c = 2;
        insert oppline;
        
        Product_Forecast__c forecast = new Product_Forecast__c();
        forecast.Quantity__c = 10000;
        forecast.Opportunity_Product__c = oppproduct.Id;
        insert forecast;
        
        Price_Request__c pr = new Price_Request__c();
        pr.Price_Type__c   = 'Tier Price';
        pr.Opportunity_Product__c = oppproduct.id;
        pr.Valid_From__c = System.today();
        pr.Valid_To__c = System.today();
        pr.Requested_Quantity__c = 1000;
        pr.Approval_status__c ='Approved';
        insert pr;
        
        Opportunity_Product__c op1 = [select id,name from Opportunity_Product__c limit 1];
        GetProductAndLineItems.getOppProductRecords(string.valueOf(op1.id));
        
        product2 prd = [select id,name from product2 limit 1];
        
        list<Opportunity_Product_line_items__c> oplList = new list<Opportunity_Product_line_items__c>();

        Opportunity_Product_line_items__c opl = new Opportunity_Product_line_items__c();
        opl.Name ='testing 123';
        opl.Opportunity_Product__c = op1.id;
        opl.Quantity__c = 2;
        opl.Product__c = prd.id;
        opl.Glass_Item__c = false;
        
        oplList.add(opl);
        insert opl;
        
        Price_Request__c pr1 = [select id,Price_Type__c,Opportunity_Product__c,Valid_From__c,Valid_To__c,Requested_Quantity__c,Approval_status__c from Price_Request__c limit 1];
        Price_Request__c pr2 = pr1.clone();
        pr2.Opportunity_Product__c = op1.id;
        pr2.Discount__c = 2;
        insert pr2;
        
        GetProductAndLineItems.getProductLineItemRecords(string.valueOf(opl.id));
        GetProductAndLineItems.createRecord(pr2,oplList);
        
        GetProductAndLineItems.returnValues(pr2.id);
  }
  
    @isTest
     private static void testMethod2()
     {
         Account acc = new Account();
        acc.Name ='test3';
        acc.Type ='Industry';
        acc.Sales_Area_Region__c ='North';
        acc.RecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('ERP').getRecordTypeId();
        insert acc;
        Id inspireoppId = Schema.SObjectType.Opportunity.getRecordTypeInfosByName().get('Inspire').getRecordTypeId();
        Opportunity opp = new Opportunity();
        String oppid1;
        opp.AccountId =acc.Id;
        opp.Name ='test1';
        opp.CloseDate = Date.today();
        opp.Amount = 1000;
        opp.StageName ='Prospecting';
        opp.Probability= 0.5;
        oppid1=acc.id;
        opp.recordTypeId =inspireoppId;
        insert opp;
        
        acc = new Account();
        acc.Name ='test31';
        acc.Type ='Industry1';
        acc.Sales_Area_Region__c ='South';
        acc.RecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('CRM').getRecordTypeId();
        insert acc;
        
        Key_Account__c keyAcct = new Key_Account__c();
        keyAcct.Opportunity__c = opp.Id;
        keyAcct.Account__c = acc.Id;
        insert keyAcct;
        
        opp.Customer_Name__c = keyAcct.Id;
        update opp;
        
        Product2 masterProduct = new Product2();
        masterProduct.Name = opp.id;
        masterProduct.Article_number__c ='test1234';
        masterProduct.Product_Category__c ='Shower Cubicles';
        masterProduct.master_product__c = true;
        insert masterProduct;
        
        Product2 pro = new Product2();
        pro.Name = 'test prod';
        pro.Product_Category__c ='Shower Cubicles';
        pro.ProductCode ='test123';
        pro.Parent_product__c = masterProduct.id;
        pro.Article_number__c = 'test123';
        pro.Quantity__c  = 2;
        pro.global_product__c = true;
        insert pro;   
        List<Product2> prodList = new List<product2>();
        prodList.add(pro);
        
        List<ProductsWrapper> prodWrapper =  new List<ProductsWrapper>();
        ProductsWrapper prodWrap = new ProductsWrapper();
        prodWrap.prdName = pro.Name;
        prodWrap.prdCode = pro.ProductCode;
        prodWrap.prdQuantitiy = Integer.valueof(pro.Quantity__c);
        prodWrap.prdArticlenumber = pro.Article_number__c;
        prodWrap.prdParentId = pro.Parent_product__c;
        //prodWrap.oppProductLineItemId = pro.Name;
        prodWrapper.add(prodWrap);
        
        Opportunity_Product__c oppproduct = new Opportunity_Product__c();
        oppproduct.Opportunity__c = opp.Id;
        oppproduct.Product__c = masterProduct.Id;
        insert oppproduct;
        
        Opportunity_Product_line_items__c oppline = new Opportunity_Product_line_items__c();
        oppline.Opportunity_Product__c=oppproduct.id;
        oppline.Product__c = pro.Id;
        oppline.Quantity__c = 2;
        insert oppline;
        
        Product_Forecast__c forecast = new Product_Forecast__c();
        forecast.Quantity__c = 1;
        forecast.Opportunity_Product__c = oppproduct.Id;
        insert forecast;
        
        Price_Request__c pr = new Price_Request__c();
        pr.Price_Type__c   = 'Tier Price';
        pr.Opportunity_Product__c = oppproduct.id;
        pr.Valid_From__c = System.today();
        pr.Valid_To__c = System.today();
        pr.Requested_Quantity__c = 10;
        pr.Approval_status__c ='Approved';
        insert pr;
        
        Opportunity_Product__c op1 = [select id,name from Opportunity_Product__c limit 1];
        GetProductAndLineItems.getOppProductRecords(string.valueOf(op1.id));
        
        product2 prd = [select id,name from product2 limit 1];
        
        list<Opportunity_Product_line_items__c> oplList = new list<Opportunity_Product_line_items__c>();
        
        Opportunity_Product_line_items__c opl = new Opportunity_Product_line_items__c();
        opl.Name ='testing 123';
        opl.Opportunity_Product__c = op1.id;
        opl.Quantity__c = 2;
        opl.Product__c = prd.id;
        opl.Glass_Item__c = false;   
             
        insert opl;
       // oplList.add(opl);
        
        Price_Request__c pr1 = [select id,Price_Type__c,Opportunity_Product__c,Valid_From__c,Valid_To__c,Requested_Quantity__c,Approval_status__c from Price_Request__c limit 1];
        Price_Request__c pr2 = pr1.clone();
        pr2.Opportunity_Product__c = op1.id;
       // pr2.Discount__c = 2;
        insert pr2;
        
        GetProductAndLineItems.getProductLineItemRecords(string.valueOf(opl.id));
        GetProductAndLineItems.createRecord(pr2,oplList);
        
        GetProductAndLineItems.returnValues(pr2.id);
        
  }

 
}
global class OpportunityTeamBatchScheduler implements Schedulable {

    global OpportunityTeamBatchScheduler(){
    }
    
    global void execute(SchedulableContext sc){
        OpportunityTeamBatch opB = new OpportunityTeamBatch();
        Database.executeBatch(opB);
    }
    /*
                    ///////////////  Schedule Code for firing it in Production  /////////////
 String cronStr = '0 0 0/1 1/1 * ? *';
System.schedule('Process Accs Job', cronStr, pa);

  String cronStr = '0 0 0/1 1/1 * ? *';
  String jobID = System.schedule(‘Invoice Tagging Scheduler’, cronStr , new InvoiceTaggingScheduler());
  System.debug('JOB id :'+jobID);
*/
}
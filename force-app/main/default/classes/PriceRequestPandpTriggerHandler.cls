/* Created By         : Nishita De
   Created Date       : 15/06/2018
   Class Name         : PriceRequestPandpTriggerHandler
   Description        : 
   Last Modified By   : Nishita De
   Last Modified Date : 31/07/2018
*/
public class PriceRequestPandpTriggerHandler{
    
    Public static void  updatereference(List<price_request_PandP__c>  prlist){
      //  Map<id,Enquiry__c> enqMap = new Map<id,Enquiry__c>();
       // Map<id,enquiry_Line_Item__c> enqlineMap = new Map<id,enquiry_Line_Item__c>();
        List<Enquiry__c> enqlist = new List<Enquiry__c>();
        List<Enquiry_Line_Item__c> enqlinelist = new List<Enquiry_Line_Item__c>();
        Set<Id> enqIds = new Set<Id>();
        Set<Id> enqLineIds = new Set<Id>();
        for(price_request_PandP__c pr:prlist){
        if(pr.enquiry__c != null)
            enqIds.add(pr.enquiry__c);
         if(pr.enquiry_Line_Item__c != null)
            enqLineIds.add(pr.enquiry_Line_Item__c);
        }
        Map<id,Enquiry__c> enqMap = new Map<id,Enquiry__c>([select id,GD_Reference_Number__c from Enquiry__c where Id IN:enqIds]);
        
        Map<id,enquiry_Line_Item__c> enqlineMap = new Map<id,enquiry_Line_Item__c>([select id,Reference_Number__c from Enquiry_Line_Item__c where Id IN:enqLineIds]);

        for(price_request_PandP__c preq:prlist){
           if(preq.enquiry__c!= null && !enqMap.isEmpty() && enqMap.containsKey(preq.enquiry__c) && enqMap.get(preq.enquiry__c).GD_Reference_Number__c !=null){
            preq.Enquiry_Reference_Number__c = enqMap.get(preq.enquiry__c).GD_Reference_Number__c;
            }
            if(!enqlineMap.isEmpty() && enqlineMap.containsKey(preq.enquiry_line_item__c) && enqlineMap.get(preq.enquiry_line_item__c).Reference_Number__c !=null){
            preq.Enquiry_Line_Item_Reference_No__c = enqlineMap.get(preq.enquiry_line_item__c).Reference_Number__c;
          }
            
        }
        
        
    }//end of the Update  Reference method
    
    // Updating the Price Request Manager on the basis of Customer Region before insert
    
    public static void updatePriceManager(List<price_request_PandP__c>prlist2){
        Set<id> accIdSet = new Set<Id>();
        for(price_request_PandP__c prqt : prlist2){
            if(prqt.Customer__c!=null){
                accIdSet.add(prqt.customer__c);
            }
        }
        if(!accIdSet.isEmpty()){
                   doHandleUserUpdate(accIdSet,prlist2);
                   doHandleUserUpdateRMUpdate(accIdSet,prlist2);
               } 
          
          getProductsFieldsInformation(prlist2);
        
        }
                    // Updation of Price Request Manager before Update
        
            public static void doCheckForBeforeUpdate(List<price_request_PandP__c>prlist2,Map<Id,price_request_PandP__c> oldMap){
                Set<id> accIdSet = new Set<Id>();
                list<price_request_PandP__c> prListNew = new list<price_request_PandP__c>();
                
                for(price_request_PandP__c prqt : prlist2){
                    if(prqt.Customer__c!=null && oldMap!= null && prqt.Customer__c!=oldMap.get(prqt.id).Customer__c){
                        accIdSet.add(prqt.customer__c);
                    }
                    
                    if(prqt.product_lookup__c !=null && oldMap!= null && prqt.product_lookup__c !=oldMap.get(prqt.id).product_lookup__c){
                      prListNew.add(prqt);
                      }
               }
               
               if(!prListNew.isEmpty())
                     getProductsFieldsInformation(prlist2);
               if(!accIdSet.isEmpty()){
                   doHandleUserUpdate(accIdSet,prlist2);
                   doHandleUserUpdateRMUpdate(accIdSet,prlist2);
               }
            
            } 
            
            public static void doHandleUserUpdate(Set<Id> accIdSet,List<price_request_PandP__c>prlist2){
                Map<String,NewcustomerrequestforPR__c> mapForReg = NewcustomerrequestforPR__c.getAll(); 
        Map<String,String> roleNameMap = new Map<String,String>();
        
        for(String mape : mapForReg.keySet()){
            roleNameMap.put(mape,mapForReg.get(mape).Role_Name__c);
        }
        Map<String,Id> forUserMap = new Map<String,Id>();
        map<string,account> accMap = new map<string,account>();
        for(User usr : [SELECT Id,Name,UserRole.Name,UserRole.DeveloperName from user where UserRole.Name IN :roleNameMap.values()]){
            if(!forUserMap.containskey(usr.UserRole.Name)){
                forUserMap.put(usr.UserRole.Name,usr.id);
            }
            
        }
        Map<Id,String> userInfoMap = new Map<Id,String>();
        for(Account acc : [select id,name,Sales_Area_Region__c,sap_code__c from Account WHERE Id IN : accIdSet]){
                accMap.put(acc.id,acc);
                userInfoMap.put(acc.id,acc.Sales_Area_Region__c);
            }
        
        
      for(price_request_PandP__c prc :prlist2){
        if(prc.customer__c != null && userInfoMap.containsKey(prc.customer__c))
        {
            system.debug('The vales are :' + userInfoMap.get(prc.Customer__c) +'......'+roleNameMap.get(userInfoMap.get(prc.Customer__c))+'......'+ forUserMap.get(roleNameMap.get(userInfoMap.get(prc.Customer__c))));
           //prc.Regional_manager_special_price__c =    roleNameMap.get(userInfoMap.get(prc.customer__c));
           //System.debug('value ='+forUserMap.get(roleNameMap.get(userInfoMap.get(prc.Customer__c))));
           try{
          prc.Regional_manager_special_price__c = Id.valueOf(forUserMap.get(roleNameMap.get(userInfoMap.get(prc.Customer__c))));
          }  catch(exception ert)
          {
              system.debug('coming here:');
              system.debug('exception came 123 is :'+ert+'....'+ert.getLineNumber());
          }
        }
        
        if(accMap!= null && accMap.containsKey(prc.customer__c))
        {
        //   prc.Bill_to_Customer__c  = accMap.get(prc.customer__c).Name;
           prc.Bill_to_customer_SAP_code__c = accMap.get(prc.customer__c).Sap_code__c;
        }
            }
            }
            
        public static void getProductsFieldsInformation(list<price_request_PandP__c> newList)
        {
            set<string> prdIds = new set<string>();
            for(price_request_PandP__c pr:newList)
            {
              prdIds.add(pr.product_lookup__c);
            }
            
            map<string,product2> prdMap = new map<string,product2>();
            system.debug('The prdIds is :'+prdIds);
            // get the produt information here. 
            for(product2 p1:[select id,name,Thickness_mm__c,Regional_head_approval_limit__c,List_Price__c from product2 where id in:prdIds])
            {
                prdMap.put(p1.id,p1);
            }
            
            
            for(price_request_PandP__c pr:newList)
            {
            if(pr.product_lookup__c != null && prdMap.containsKey(pr.product_lookup__c))
            {
              pr.Product_Name_enquiry_component__c =  prdMap.get(pr.product_lookup__c).Name;
              pr.List_tier_price__c =  prdMap.get(pr.product_lookup__c).List_Price__c;
              pr.Enquiry_Thickness__c = String.valueof(prdMap.get(pr.product_lookup__c).Thickness_mm__c);
              pr.Regional_head_approval_limit__c =  prdMap.get(pr.product_lookup__c).Regional_head_approval_limit__c;
            }
            system.debug('The updatedpr is:' +pr);  
            }
            
            
            
        
        }    
public static void ValidateSpecialRequestRec(list<price_request_PandP__c> newlist,map<id,price_request_PandP__c> oldMap)
        {
          list<price_request_PandP__c> newlistToPass = new list<price_request_PandP__c>();
            if(oldMap == null){
                ValidateSpecialRequestHelper(newlist);
            }else{
            for(price_request_PandP__c pr:newlist)
            {
                if(oldMap.get(pr.id).Product_lookup__c != pr.Product_lookup__c || oldMap.get(pr.id).Customer__c != pr.Customer__c  || oldMap.get(pr.id).Required_Price__c!= pr.Required_Price__c)
                 newlistToPass.add(pr);
            }
            if(newlistToPass.size()>0)
             ValidateSpecialRequestHelper(newlist);
         }
       }
        
        public static void ValidateSpecialRequestHelper(list<price_request_PandP__c> newlist)
            {
            
            for(price_request_PandP__c pr:newlist)
            {
              system.debug('The pr.Product_lookup__c is:' +pr.Product_lookup__c);
            string errorMsg ='';
                account a1=[select id,Glass_reach_account_name__c,Inspire_Tier__c,Parsol_Tier__c,name,Sales_Office_ERP__c,Reflectasol_Tier__c,Mirror_Tier__c,Interior_Tier__c,Clear_Tint_Tier__c from account where id=:pr.Customer__c];
                product2 p2;
                try{
                p2 = [select id,name,productCode,External_Id__c,Product_Category__c from product2 where id=:pr.Product_lookup__c AND (Product_Category__c = 'INSPIRE' OR Product_Category__c = 'PARSOL' OR Product_Category__c = 'CLEAR') limit 1]; 
                }
                catch(exception ert)
                {
                  
                }
                if(p2 != null)
                {
                    string prdCat = p2.Product_Category__c;
                    
                     string specificTier ='';
                    if(prdCat == 'REFLECTASOL' && a1.Reflectasol_Tier__c!= null)
                        specificTier = a1.Reflectasol_Tier__c;
                    else if(prdCat == 'Mirror' && a1.Mirror_Tier__c!= null)
                        specificTier = a1.Mirror_Tier__c;
                    else if(prdCat == 'Interior' && a1.Interior_Tier__c!= null)
                        specificTier = a1.Interior_Tier__c;
                    else if(prdCat == 'CLEAR' && a1.Clear_Tint_Tier__c!= null)
                        specificTier = a1.Clear_Tint_Tier__c;
                    else if(prdCat == 'Parsol' && a1.Parsol_Tier__c!= null)
                        specificTier = a1.Parsol_Tier__c;
                    else if(prdCat == 'INSPIRE' && a1.Inspire_Tier__c!= null)
                        specificTier = a1.Inspire_Tier__c;
                    
                    
                system.debug('The specificTier is :'+specificTier+'...'+p2.External_Id__c+'...'+a1.Sales_Office_ERP__c);
                try{
                    List<Product_Tier_Pricing__c> ptp = [select id,name,List_Tier_price__c,Tier_Pricing__c,Regional_head_approval_limit__c,Business_Location__c,Product_Code__c,Item_Code__c,Tier_Name__c from Product_Tier_Pricing__c where Item_Code__c=:p2.External_Id__c AND Business_Location__c=:a1.Sales_Office_ERP__c AND Tier_Name__c=:specificTier];
                    if(ptp.size() > 0) {
                      
                        
                        decimal enteredPrice = pr.Required_Price__c;
                        decimal tierPrice = ptp[0].List_Tier_price__c;      
                        decimal requiredPriceCutOff = (ptp[0].List_Tier_price__c*0.75).setScale(2);
                        decimal requiredPriceLessThanEqualTo15  = ptp[0].List_Tier_price__c*0.85;
                        
                        if(enteredPrice == null)
                         pr.addError('Please enter Requested Price Sq m - mm');
                         else
                         {
                            
                        if(requiredPriceCutOff > enteredPrice || enteredPrice > tierPrice) 
                     pr.addError('Requested Price should be between of :'+requiredPriceCutOff+' and '+tierPrice );                   
                     else                    
                      if( enteredPrice >=requiredPriceCutOff && enteredPrice <=requiredPriceLessThanEqualTo15) 
                      {
                        pr.Approval_level__c= 'Rm Approval';
                        pr.Original_Price_Sq_m_mm__c = tierPrice;
                        }
                      else
                      if(enteredPrice >= requiredPriceLessThanEqualTo15 && enteredPrice <= tierPrice)
                      {
                      pr.Approval_level__c= 'Nh Approval';
                      pr.Original_Price_Sq_m_mm__c = tierPrice;
                      }
                         }
                    }
                    else{
                      pr.addError('No product tier pricing found for this customer & product');
                    }
                } catch(exception ert){  
                    system.debug('error occured: '+ert);
                    CommonUtilitiesHandler.handleExceptions(ert);
                    errorMsg = 'No product tier pricing found for this customer & product';
                }
                
                if(errorMsg.length()>1)
                pr.addError('No product tier pricing found for this customer & product');

            }
           
           }  
          }
            
  public static void doHandleUserUpdateRMUpdate(Set<Id> accIdSet,List<price_request_PandP__c> prlist2){
                Map<String,NewcustomerrequestforPR1__c> mapForReg = NewcustomerrequestforPR1__c.getAll(); 
                
                //for NewcustomerrequestforPR1__c custom settings
                Map<String,NewcustomerrequestforPR1__c> mapForReg1 = NewcustomerrequestforPR1__c.getAll(); 
                
                
                
        Map<String,String> roleNameMap = new Map<String,String>();
        
        for(String mape : mapForReg.keySet()){
            roleNameMap.put(mape,mapForReg.get(mape).Role_Name__c);
        }
        
        // for regional manager
        for(String mapes : mapForReg1.keySet()){
            roleNameMap.put(mapes,mapForReg1.get(mapes).Role_Name__c);
        }
        
        
        Map<String,Id> forUserMap = new Map<String,Id>();
        map<string,account> accMap = new map<string,account>();
        for(User usr : [SELECT Id,Name,UserRole.Name,UserRole.DeveloperName from user where UserRole.Name IN :roleNameMap.values()]){
            if(!forUserMap.containskey(usr.UserRole.Name)){
                forUserMap.put(usr.UserRole.Name,usr.id);
            }
            
        }
        Map<Id,String> userInfoMap = new Map<Id,String>();
        for(Account acc : [select id,name,Sales_Area_Region__c,sap_code__c from Account WHERE Id IN : accIdSet]){
                accMap.put(acc.id,acc);
                userInfoMap.put(acc.id,acc.Sales_Area_Region__c);
            }
        
        
      for(price_request_PandP__c prc :prlist2){
        if(prc.customer__c != null && userInfoMap.containsKey(prc.customer__c))
        {
            system.debug('The vales are :' + userInfoMap.get(prc.Customer__c) +'......'+roleNameMap.get(userInfoMap.get(prc.Customer__c))+'......'+ forUserMap.get(roleNameMap.get(userInfoMap.get(prc.Customer__c))));
           //prc.Regional_Manager__c =    roleNameMap.get(userInfoMap.get(prc.customer__c));
           //System.debug('value ='+forUserMap.get(roleNameMap.get(userInfoMap.get(prc.Customer__c))));
           try{
          prc.Regional_Manager__c = Id.valueOf(forUserMap.get(roleNameMap.get(userInfoMap.get(prc.Customer__c))));
          prc.Regional_Manager__c= Id.valueOf(forUserMap.get(roleNameMap.get(userInfoMap.get(prc.Customer__c))));
          }  catch(exception ert)
          {
              system.debug('coming here:');
              system.debug('exception came 123 is :'+ert+'....'+ert.getLineNumber());
          }
        }
        
        if(accMap!= null && accMap.containsKey(prc.customer__c))
        {
        //   prc.Bill_to_Customer__c  = accMap.get(prc.customer__c).Name;
           prc.Bill_to_customer_SAP_code__c = accMap.get(prc.customer__c).Sap_code__c;
        }
            }
            }
           
           
           
           
       // Last approved/invoiced price to be displayed in approval pending list view for RHs
       //While the Regional Head is approving PRs, there should be a separate field displaying the last invoiced price of the same product for the same customer.

       public static void PopulateLastApprovedPrice(list<price_request_PandP__c> newList)
       {
       
       // below format for the map is: map<priceRequestPandP rec,map<Account,product>
       map<string,map<string,string>> approvedPriceMap = new map<string,map<string,string>>();
       map<string,string> customersWithProductMap = new map<string,string>();
       map<string,decimal> PRforPandPExistingRecIdWithPrice = new map<string,decimal>();
         for(price_request_PandP__c pr:newList)
         {
            if(pr.Customer__c != null && pr.Product_lookup__c!= null)
            {           
            customersWithProductMap.put(pr.Customer__c,pr.Product_lookup__c);             
            }
         }
         
         //querying the old approved record for the same product and customer. 
         for(price_request_PandP__c pr:[select id,name,Approved_Price__c,Customer__c,Product_lookup__c from price_request_pandp__c where  Approved_Price__c!= null AND Approval_Status__c ='Approved' AND Customer__c in:customersWithProductMap.keyset() AND Product_lookup__c in:customersWithProductMap.Values()])
         {
         if(pr.Customer__c != null && pr.Product_lookup__c!= null)
            {
            PRforPandPExistingRecIdWithPrice.put(pr.id,pr.Approved_Price__c);
            map<string,string> tempMap = new map<string,string>();
            tempMap.put(pr.Customer__c,pr.Product_lookup__c);
             approvedPriceMap.put(pr.id,tempMap);
            }
         }
         
         // Now itterate the new records witht the data map and check for the any mathching product with customer record is available, if so then populate the price value. 
       
       }    
       
       //before insert update Last approved price
       public static void beforeInsertUpdatePrice(List<price_request_PandP__c> newList){
           Set<id> customerIdSet = new Set<id>();
           Set<id> productIdSet = new Set<id>();
           Decimal newApprovedAmount;
           for(price_request_PandP__c pnp : newList){
               if(pnp.Customer__c != null)
                   customerIdSet.add(pnp.customer__c);
               if(pnp.Product_lookup__c != null)
                   productIdSet.add(pnp.Product_lookup__c);
           }
           
           List<price_request_PandP__c> pnpList = [SELECT id,Customer__c,Product_lookup__c,Approved_Price__c FROM price_request_PandP__c WHERE Customer__c 
                                                   IN :customerIdSet AND Product_lookup__c IN :productIdSet AND Approval_Status__c = 'Approved' ORDER BY CreatedDate DESC LIMIT 1];
           for(price_request_PandP__c pnp : pnpList){
               newApprovedAmount = pnp.Approved_Price__c;
           }
           
           for(price_request_PandP__c pnp : newList){
               if(newApprovedAmount > 0)
                   pnp.Last_approved_price__c = newApprovedAmount;
           }
       
       }  
       
       //to get distribution RM and NH
       public static void beforeInsertValues(List<price_request_PandP__c> newList){
            //to get current user managers hierarchy
            Id regionalManager, nationalHead;
            
            id id1 = userinfo.getProfileId();
            Profile loggedInProfile = [SELECT id, Name FROM Profile WHERE id = :id1 LIMIT 1];
            System.debug('-->profile Name '+loggedInProfile.Name);        
            Boolean queryToBeAdded = String.ValueOf(loggedInProfile.Name).contains('ORS');
            if(String.ValueOf(loggedInProfile.Name).contains('ORS') || String.ValueOf(loggedInProfile.Name).contains('AM') || Test.isRunningTest()){
                
                System.debug('queryToBeAdded '+queryToBeAdded);
                String rmUser = '';
                String nationalUser = '';
                if(queryToBeAdded == true){
                    rmUser = ',Manager.Manager.Manager.Id ';
                    nationalUser = ',Manager.Manager.Manager.Manager.Manager.Id ';
                }
                else if(String.ValueOf(loggedInProfile.Name).contains('AM')){
                    rmUser = ',Manager.Manager.Id ';
                    nationalUser = ',Manager.Manager.Manager.Manager.Id '; 
                }
                
                System.debug('rmUser '+rmUser+'nationalUser '+nationalUser);
                Id userId = UserInfo.getUserId();
                
                if(rmUser != '' || nationalUser != ''){
                    List<user> userList = Database.query('SELECT id, Name'+ rmUser + nationalUser +' FROM User WHERE id =:userId');
                    for(User us : userList){
                        if(queryToBeAdded == true){
                            regionalManager = us.Manager.Manager.Manager.Id;
                            nationalHead = us.Manager.Manager.Manager.Manager.Manager.Id;
                            
                            System.debug('us ORS'+us.Manager.Manager.Manager.Id);
                            System.debug('uss ORS'+us.Manager.Manager.Manager.Manager.Manager.Id);
                        }
                        else{
                            regionalManager = us.Manager.Manager.Id;
                            nationalHead = us.Manager.Manager.Manager.Manager.Id;
                            
                            System.debug('us AM'+us.Manager.Manager.Id);
                            System.debug('uss AM'+us.Manager.Manager.Manager.Manager.Id);
                        }
                    }
                }
            }
            //end of user managers assignment
       
           for(price_request_PandP__c pnp :newList){
               if(regionalManager != null)
                   pnp.Regional_Manager__c = regionalManager;
               if(nationalHead != null)
                   pnp.Regional_manager_special_price__c = nationalHead;
           }
       
       }        
 }
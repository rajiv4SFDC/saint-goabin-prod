@istest
private class RecentInvoiceListClass_test
{

   private testmethod static void testMethod2()
   {
   
        
    Test.startTest();    
        //creating user record for the above role. 

 //  UserRole r = new UserRole(DeveloperName = 'MyCustomRole', Name = 'Regional Manager East');
 //   insert r;



  UserRole r = [select id,name from userrole where name='Regional Manager South'];
 
 
User u = new User(
     ProfileId = [SELECT Id FROM Profile WHERE Name = 'System Administrator'].Id,
     LastName = 'last',
     Email = 'puser2323000@amamama.com',
     Username = 'pus2323er000@amamama.com' + System.currentTimeMillis(),
     CompanyName = 'TEST',
     Title = 'title',
     Alias = 'alias',
     TimeZoneSidKey = 'America/Los_Angeles',
     EmailEncodingKey = 'UTF-8',
     LanguageLocaleKey = 'en_US',
     LocaleSidKey = 'en_US',
     UserRoleId = r.Id,
     Phone  ='123123123123123'
);
insert u;


           Test.stopTest(); 
           
            user u1=[select id,name from user where id=:userinfo.getuserid()];
            
             System.runAs(u1) {        
   NewcustomerrequestforPR__c newcustom = new NewcustomerrequestforPR__c();
        newcustom.Active__c = true;
        newcustom.Role_Name__c ='Regional Manager South';
        newcustom.Name ='South';
        insert newcustom; 
        
         NewcustomerrequestforPR1__c newcustom1 = new NewcustomerrequestforPR1__c();
        newcustom1.Active__c = true;
        newcustom1.Role_Name__c ='Regional Manager South';
        newcustom1.Name ='South';
        insert newcustom1; 
         
        Enquiry__c enc = new Enquiry__c();
        enc.Account_Name__c = 'testing';
        enc.GD_Reference_Number__c = '12';       
        insert enc;
              
      
      
       
       // code coverage for doFetchPriceRequest();
         Account acc = new Account();
        acc.Name ='test3';
        acc.Type ='Industry';
        acc.Sales_Area_Region__c ='South';
        acc.PAN_Number__c = 'AAAPL1234C';
       acc.Sales_Office_ERP__c = 'SPR3';
       acc.sap_code__c='123123123';
        acc.RecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('ERP').getRecordTypeId();
        insert acc;
   
   
   
      Product2 p1 = new Product2();
      p1.Name = 'testing';
      p1.Description ='East';
      p1.Product_Category__c = 'CLEAR';
      p1.Sub_Family__c = 'EKO';
      p1.Coating__c ='PLT';
      p1.Tint__c = 'SAFE';
      p1.ExternalId = '401-518-000-000-000-0600';
       p1.External_Id__c = '401-518-000-000-000-0600';
      insert p1;
      
      Enquiry_Line_Item__c ptp = new Enquiry_Line_Item__c();
      ptp.Product_Item_Code__c = '401-518-000-000-000-0600';
      ptp.Product__c = p1.id;
      ptp.Glass_Direct_Line_Item__c = TRUE;
     ptp.Enquiry__c = enc.Id;

      insert ptp;
      
      invoice__c inv = new invoice__c();
      inv.name='testing123';
      inv.product__c = p1.id;
      inv.customer__c = acc.id;
      inv.invoice_rate__c = 123;
      inv.billing_date__c = date.Today();
      insert inv;
      
      Enquiry_Line_Item__c pt = [select id, Product_Item_Code__c from Enquiry_Line_Item__c where id =:ptp.id];
      
    //  System.assertEquals(pt.product__c,p1.id);
      list<Id> ptrList = new list<Id>();
      ptrList.add(pt.id);
      
      UpdateProductLookupEnqLineItem.UpdateLookupValue(ptrList);
      
      
       Price_Request_PandP__c pp = new Price_Request_PandP__c();
        pp.Approval_status__c = 'Approved';
        pp.Available_Quantity_Tons__c = 10;
        pp.Requested_Quantity__c = 10;
       pp.sales_office__c ='SPR3';
        pp.Customer__c = acc.Id;
        pp.Product_lookup__c = p1.Id;
        insert pp;
     
       ApexPages.StandardController sc = new ApexPages.StandardController(pp);
        RecentInvoiceListClass ril= new RecentInvoiceListClass (sc);
        ril.getCategories();
        ril.recordsFound = false;
        ril.first();
        ril.last();
        ril.cancel();
        ril.next();
        ril.previous();
        }
   
      }


}
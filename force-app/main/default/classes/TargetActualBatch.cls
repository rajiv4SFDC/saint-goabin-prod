global class TargetActualBatch implements Database.Batchable<sObject>, Database.Stateful {

	global Integer monthNum;
  	global Integer yearNum; 

  	global TargetActualBatch(Date executedate){
    	monthNum = executedate.month();
    	yearNum = executedate.year();
	}
 	
    global TargetActualBatch(){
    	monthNum = system.today().month();
    	yearNum = system.today().year();
	}
    
	global Database.QueryLocator start(Database.BatchableContext BC){

		return Database.getQueryLocator([SELECT id,Target__c,Actual__c,Sales_Area__c,
										 Target_Date__c,Points_Eligible__c,Points_Scored__c,
										 Target_Type__c,Product_Range__c,
										 Calculate_Actuals__c,Calculate_Point_Scored__c,
                     Error_Message__c,
										 Unique_Target_Actual_Identifier__c
                                         FROM Target_and_Actual__c
                                         WHERE CALENDAR_MONTH(Target_Date__c) =: monthNum 
                                         AND CALENDAR_YEAR(Target_Date__c)  =: yearNum]);

    }


    global void execute(Database.BatchableContext BC, List<Target_and_Actual__c> targetActuallst){

    	Map<String,Target_and_Actual__c> targetActualMap = new Map<String,Target_and_Actual__c>();
      	Set<String> targerActualSalesAreaSet = new Set<String>();

      	// Creating Scorecard Map
      	for(Target_and_Actual__c tvav : targetActuallst){
      		
      		// Make Actuals zero for running month
      		tvav.Actual__c = 0;
      		
      		targetActualMap.put(tvav.Unique_Target_Actual_Identifier__c,tvav);
      		targerActualSalesAreaSet.add(tvav.Sales_Area__c);
      	}

  		List<Product_Forecast__c> forecastLst = [SELECT id,Forecast_Date__c,Total_Appropriable_Quantity__c,
  												 Opportunity_Product__r.Opportunity__r.Sales_Area__c,
                           Opportunity_Product__r.Status__c,
  												 Opportunity_Product__r.Opportunity__r.Project__r.Segment__c,
  												 Opportunity_Product__r.Product__r.Product_Range__c,
  												 Opportunity_Product__r.Opportunity__r.Project__r.City__c,
                           Opportunity_Product__r.Opportunity__r.Project__r.City__r.External_ID__c,
                           Opportunity_Product__r.Opportunity__r.Project__r.Luxe_Glass__c ,
  												 Opportunity_Product__r.Opportunity__r.Project__r.Luxe_Homes__c,
  												 Opportunity_Product__r.Opportunity__r.Project__r.Cool_Homes__c,
  												 Opportunity_Product__r.Opportunity__r.Project__r.Glass_Govern__c,
  												 Opportunity_Product__r.Opportunity__r.Project__r.Glass_Heals__c
				  								 FROM Product_Forecast__c
				  								 WHERE Opportunity_Product__r.Opportunity__r.Sales_Area__c
				  								  IN:targerActualSalesAreaSet 
                            AND Opportunity_Product__r.Status__c = 'Selected'
				  								 AND ( CALENDAR_MONTH(Forecast_Date__c) =: monthNum AND
				  								 CALENDAR_YEAR(Forecast_Date__c) =: yearNum )];

		List<Project__c> projectLst = [SELECT id,Sales_Area__c,Project_Validated_Date__c 
									  FROM Project__c
								  	  WHERE Sales_Area__c IN: targerActualSalesAreaSet
									  AND ( CALENDAR_MONTH(Project_Validated_Date__c) =: monthNum AND
									  CALENDAR_YEAR(Project_Validated_Date__c) =: yearNum )];

		List<Opportunity> opptyLst = [SELECT id,Sales_Area__c,Design_Report_Created_Date__c,First_Billing_Date__c, 
                                      Jumbo_Resizing_Completed__c,Glass_Pro_Live_Completed__c
                                      FROM Opportunity
                                      WHERE Sales_Area__c IN: targerActualSalesAreaSet
                                      AND ( 
                                      ( CALENDAR_MONTH(Design_Report_Created_Date__c) =: monthNum AND
                                       CALENDAR_YEAR(Design_Report_Created_Date__c) =: yearNum ) 
                                     OR ( CALENDAR_MONTH(First_Billing_Date__c) =: monthNum AND
                                     CALENDAR_YEAR(First_Billing_Date__c) =: yearNum ) 
                                     OR ( CALENDAR_MONTH(Jumbo_Resizing_Completed__c) =: monthNum AND
                                     CALENDAR_YEAR(Jumbo_Resizing_Completed__c) =: yearNum ) 
                                     OR ( CALENDAR_MONTH(Glass_Pro_Live_Completed__c) =: monthNum AND
                                     CALENDAR_YEAR(Glass_Pro_Live_Completed__c) =: yearNum )  )
                                  ]; 

	    List<Campaign> compLst = [SELECT id,Sales_Area__c,Type,EndDate 
								  FROM Campaign
							  	  WHERE Sales_Area__c IN: targerActualSalesAreaSet
								  AND ( CALENDAR_MONTH(EndDate) =: monthNum AND
								  CALENDAR_YEAR(EndDate) =: yearNum )];	
        
		List<Event> eventLst = [SELECT id,OwnerId,Sales_Area__c,Type,
                                Related_To_Type__c,StartDateTime,EndDateTime 
								  FROM Event
							  	  WHERE Sales_Area__c IN: targerActualSalesAreaSet
								  AND ( CALENDAR_MONTH(StartDateTime) =: monthNum AND
								  CALENDAR_YEAR(StartDateTime) =: yearNum )];
        
        List<Task> tasklst = [SELECT id,OwnerId,Sales_Area__c,Type,
                              Related_To_Type__c,Custom_Due_Date__c,
                              Description,Subject
                              FROM task
                              WHERE Sales_Area__c IN: targerActualSalesAreaSet AND
                              Subject LIKE '%Call%' 
                              AND ( CALENDAR_MONTH(Custom_Due_Date__c) =: monthNum AND
                                   CALENDAR_YEAR(Custom_Due_Date__c) =: yearNum ) ];
        
        List<Contact> contactLst = [SELECT id,LastName,OwnerId,Owner.Sales_Area__c,CreatedDate 
                                    FROM Contact
                                    WHERE Owner.Sales_Area__c  IN: targerActualSalesAreaSet
                                    AND ( CALENDAR_MONTH(CreatedDate) =: monthNum AND
								    CALENDAR_YEAR(CreatedDate) =: yearNum )];
        
        List<EmailMessage> emailLst = [SELECT Id,Subject,Status,CreatedById,
                                       MessageDate,CreatedBy.Sales_Area__c 
                                       FROM EmailMessage
                                       WHERE CreatedBy.Sales_Area__c IN: targerActualSalesAreaSet AND 
                                       ( CALENDAR_MONTH(MessageDate) =: monthNum AND
								       CALENDAR_YEAR(MessageDate) =: yearNum )];
        
        List<Plant_Visit__c> plantlst = [SELECt Id,Visit_Date__c,Opportunity__c,
                                         Opportunity__r.Sales_Area__c,Approval_Status__c
                                         FROM Plant_Visit__c 
                                         WHERE Opportunity__c !=null AND
                                         Opportunity__r.Sales_Area__c IN: targerActualSalesAreaSet AND
                                         Approval_Status__c = 'Approved' AND
                                         ( CALENDAR_MONTH(Visit_Date__c) =: monthNum AND
								         CALENDAR_YEAR(Visit_Date__c) =: yearNum )
                                         ]; 

		// Call TargetActualHandler
		Map<String,Target_and_Actual__c> tvavMap;
		TargetActualHandler tvavHandler = new TargetActualHandler();

      	tvavMap = tvavHandler.calculateForecastRelatedActuals(forecastLst,targetActualMap);

      	tvavMap = tvavHandler.calculateProjectDBAdditionActuals(projectLst,tvavMap);	

      	tvavMap = tvavHandler.calculateOpportunityRelatedActuals(opptyLst,tvavMap);

      	tvavMap = tvavHandler.calculateInfluencerEngagementActivitiesActuals(compLst,tvavMap);
        
        tvavMap = tvavHandler.calculateI2VisitActuals(plantlst,tvavMap);
        
		tvavMap = tvavHandler.calculateEventActuals(eventLst,tvavMap);
        
        tvavMap = tvavHandler.calculateTaskActuals(tasklst,tvavMap);
        
        tvavMap = tvavHandler.calculateContactActuals(contactLst,tvavMap);
        
        tvavMap = tvavHandler.calculateEmailActuals(emailLst,tvavMap);
        
        //system.debug('tttttttttt value ::::'+ tvavMap.values());
      	update tvavMap.values();
    }


    global void finish(Database.BatchableContext BC) {

    	String backupDateWindow = System.label.TargetActualBackUpdateOpenWindow;
    	Integer daysinWindow = Integer.valueOf(backupDateWindow);
      Boolean recurrsionChk = CheckRecursive.runOnce();
        
    	IF( recurrsionChk && System.today().day() <= daysinWindow){
    		Date executedate = Date.newInstance(system.today().year(),system.today().month(), system.today().day());
			  TargetActualBatch tvavbatch = new TargetActualBatch(executedate);
			  Database.executebatch(tvavbatch,5);
    	}
    	else{
    		system.debug('recurrsion out...................');
    	} 
    }


}
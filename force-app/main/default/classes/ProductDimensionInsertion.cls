/******************************************
 * Created By  : Rajiv Kumar Singh
 * Created On  : 08-Jun-2018
 * Modified By : 
 * Modified On :
 * Description : Populate Thickness, Length and Width of Material Request object into Product Dimension object fields.                        
*/ 
public class ProductDimensionInsertion {    														 
    public static void dimensionFetch(List<Material_Request__c> materialList) { 				// fetching Material Request data from trigger after record insert or update
        Set<id> idOfProduct = new Set<id>();                									// create set of id to store Lookup id of Material Request.
        Id product2Id;                                      									// Declare Id variable to store Product2 id.
        List<Product_Dimension__c> productDimensionToUpdate = new List<Product_Dimension__c>();
        for(Material_Request__c material : materialList) {
            idOfProduct.add(material.Product__c); 												// store lookup id in set.
        }         																						// Query on Product Dimension Object to get thickness
        for(Product2 productDimension : [select id from Product2 where Id IN : idOfProduct]) {
            product2Id = productDimension.Id;
        }
		for(Material_Request__c material : materialList) {
            if(material.Approval_Status__c == 'Approved') {										// check Approval Request is approved or not.
                Product_Dimension__c productDimensionInstance = new Product_Dimension__c(); 	// create Product Dimension instance
                productDimensionInstance.Length__c    = material.Length__c; 					// assign length of material object to Product dimension. 
                productDimensionInstance.Width__c     = material.Width__c; 						// assign width of material object to Product dimension.
                productDimensionInstance.Product__c   = product2Id; 							// Link Product Dimension object to Product2 
                productDimensionInstance.Thickness_Code__c = material.Thickness__c;
            	productDimensionToUpdate.add(productDimensionInstance); 						// add Product Dimension instance to productDimensionToUpdate List
            }
        }    
        if(!productDimensionToUpdate.isEmpty() && productDimensionToUpdate.size() > 0) { 		// null check
            insert productDimensionToUpdate; 													// DML to insert list of  Product Dimension.
        }       
    }
}
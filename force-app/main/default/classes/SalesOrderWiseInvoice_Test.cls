/*
Created BY: KVP Business Solutions
Created Date: 27th sep 2018
*/

@isTest
public class SalesOrderWiseInvoice_Test {
    
    @isTest
    public static void TestMain(){
        
        account ac = new account();
        ac.name='test account';
        ac.sales_area_region__c='north';
        insert ac;
        
        product2 prod = new product2();
        prod.Name='test product';
        prod.Reporting_Range__c='Clear_Thick';
        prod.Product_Category__c='clear';
        insert prod;
        
        product2 p = new product2();
        p.Name='test product';
        p.Reporting_Range__c='Two_mm';
        p.Product_Category__c='clear';
        insert p;
        
        product2 pro = new product2();
        pro.Name='test product';
        pro.Reporting_Range__c='Clear_Thin';
        pro.Product_Category__c='clear';
        insert pro;
        
        List<Sales_Order__c> OrderList = new List<Sales_Order__c>();
        for(integer i=0;i<=5;i++){
            Sales_Order__c s = new Sales_Order__c();
            s.Bill_To_Account__c=ac.id;
            s.Product_Name__c=prod.id;
            s.Quantity_in_Tons__c=006+i;
            s.Sales_Order_Date__c=system.today();
            orderList.add(s);
        }
        insert orderlist;
        
        List<invoice__c> orderinvoices = new List<invoice__c>();
        for(integer i=0;i<=4;i++){
            invoice__c inv = new invoice__c();
            inv.Name='Test-005'+i;
            inv.Quantity__c=1200+i;
            inv.Product__c=prod.id;
            inv.Billing_Date__c=system.today();
            orderinvoices.add(inv);
        }
        insert orderinvoices;
        
        SalesOrderWiseInvoice so = new SalesOrderWiseInvoice();
        so.Office='BLR1';
        so.BillDate=system.today();
        so.checkbox_range=true;
        so.checkbox_range1=true;
        so.from_date=system.today()-8;
        so.To_date=system.today()+5;
        so.SelectedRange='Clear_Thick';
        so.SelectedRegion='North';
        system.assertEquals('North',so.SelectedRegion);
      
         SalesOrderWiseInvoice.PieWedgeData pw = new SalesOrderWiseInvoice.PieWedgeData('string name',12.4,17.6);
              string che;
        string range;
        orderlist=[select name,sales_order_date__c,product_name__r.reporting_range__c from sales_order__c where sales_order_date__c >:so.from_date and sales_order_date__c <:so.to_date];
         for(sales_order__c sales: orderlist){
            che=sales.product_name__r.reporting_range__c;
        }   
        orderinvoices=[select name,billing_date__c,product__r.reporting_range__c from invoice__c where product__r.reporting_range__c=:che AND (billing_date__c >:so.from_date and billing_date__c <:so.to_date)];
        for(invoice__c inv: orderinvoices){
            range=inv.product__r.reporting_range__c;
        }
           
            system.assertEquals('Clear_Thick', che);
        
        profile f=[select id,name from profile where name='Standard User'];
        user u =new user(Alias='test',Email='test@test.com', EmailEncodingKey='UTF-8', LastName='test',LanguageLocaleKey='en_us',
                         profileId=f.id,TimeZoneSidKey='America/Los_Angeles',phone='1234567654',Username='test'+Datetime.now().getTime()+'@test.com',CommunityNickname='test', LocaleSidKey='en_us');
        system.runAs(u){
           
            
            List<SelectOption> SelList=so.RegionOption;
            List<selectOption> RegList=so.ReportingRange;
            so.filter();
            so.filterRange();
            so.SalesAreawise();
            so.getPieData();
            so.filterSales();
            so.click();
            so.check();
           
        }        
    }
}
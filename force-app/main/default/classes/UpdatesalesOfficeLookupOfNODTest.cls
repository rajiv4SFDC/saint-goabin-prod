/* Created By         : Rajiv Kumar Singh
   Created Date       : 06/07/2018
   Class Name         : UpdatesalesOfficeLookupOfNODtest
   Description        : 
   Last Modified By   : Rajiv Kumar Singh
   Last Modified Date : 06/07/2018
*/
@isTest
private class UpdatesalesOfficeLookupOfNODTest
{ 
 // @istest
   private testmethod static void testMethod1()
   {
      Sales_Office__c p1 = new Sales_Office__c();
      p1.Name = 'CHD-1';
      insert p1;
      
      Sales_Office_Wise_NOD_s__c ptp = new Sales_Office_Wise_NOD_s__c();
      ptp.Sales_Office_Name__c = 'CHD-1';
      ptp.Sales_Office__c = p1.id;
      insert ptp;
      
      Sales_Office_Wise_NOD_s__c pt = [select id, Sales_Office_Name__c from Sales_Office_Wise_NOD_s__c where id =:ptp.id];
      
    //  System.assertEquals(pt.product__c,p1.id);
      list<Id> ptrList = new list<Id>();
      ptrList.add(pt.id);
      
      UpdatesalesOfficeLookupOfNOD.UpdateLookupValue(ptrList);
   }
}
/* Created By         : Rajiv Kumar Singh
   Created Date       : 13/07/2018
   Class Name         : UpdateCustomerLookupOfSalesOrder_Test
   Description        : 
   Last Modified By   : Rajiv Kumar Singh
   Last Modified Date : 13/07/2018
*/
@isTest
private class UpdateCustomerLookupOfSalesOrder_Test
{ 
 // @istest
   private testmethod static void testMethod1()
   {
      Account p1 = new Account();
      p1.Name = 'testing';
      p1.SAP_Code__c = '6565666';
      p1.Sales_Area_Region__c = 'East';
      insert p1;
      
      Sales_Order_Line_Item__c ptp = new Sales_Order_Line_Item__c();
      ptp.Bill_To_SAP_Code__c = '6565666';
      ptp.Bill_To_Account__c = p1.id;
      insert ptp;
      
      Sales_Order_Line_Item__c pt = [select id, Bill_To_SAP_Code__c from Sales_Order_Line_Item__c where id =:ptp.id];
      
      list<Id> ptrList = new list<Id>();
      ptrList.add(pt.id);
      
      UpdateCustomerLookupOfSalesOrder.UpdateLookupValue(ptrList);
   }
}
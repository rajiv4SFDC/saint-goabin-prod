/* Created By: Rajiv Kumar Singh
Created Date: 03/07/2018
Class Name: SalesOrderstatusInsertAPITest
Description : Test class for SalesOrderstatusInsertAPI
Last Modified By :Rajiv Kumar Singh
Last Modified Date: 03/07/2017
*/

@IsTest
private class SalesOrderstatusInsertAPITest{
    
    static testMethod void testPostMethod(){
        String JSONMsg = '[{"Salesorder":"311d","orderstatus":"Invoiced","StatusTimestamp":"27/06/2018 19:30:00"}]';
        RestRequest request = new RestRequest();
        request.requestUri ='/services/apexrest/SalesOrderStatus';
        request.httpMethod = 'POST';
        request.requestBody = Blob.valueof(JSONMsg);
        RestContext.request = request;
        RestResponse res = new RestResponse();
        RestContext.response = new RestResponse();
        SalesOrderstatusInsertAPI.doHandleInsertRequest();
        
        JSONMsg = '[1';
        request = new RestRequest();
        request.requestUri ='/services/apexrest/SalesOrderStatus';
        request.httpMethod = 'POST';
        request.requestBody = Blob.valueof(JSONMsg);
        RestContext.request = request;
        RestContext.response = new RestResponse();
        SalesOrderstatusInsertAPI.doHandleInsertRequest();
        
        request = new RestRequest();
        request.requestUri ='/services/apexrest/SalesOrderStatus';
        request.httpMethod = 'POST';
        request.requestBody = Blob.valueof('');
        RestContext.request = request;
        RestContext.response = new RestResponse();
        SalesOrderstatusInsertAPI.doHandleInsertRequest();
    }
}
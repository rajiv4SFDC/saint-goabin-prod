public class OpportunityProductsController {
    
    @AuraEnabled
    public static Map<String, String> getOpportunityDetail(String oppId) {
        
        system.debug('The rec id is:'+oppId);
        Map<String, String> oppDetailMap = new Map<String, String>();
        List<Opportunity> oppDetails = [SELECT Id, Name, Account.Name FROM Opportunity WHERE Id = :oppId];
        
        if(oppDetails != null && oppDetails.size() > 0) {
            
            oppDetailMap.put('OPPNAME', oppDetails[0].Name);
            oppDetailMap.put('ACCOUNTNAME', oppDetails[0].Account.Name);
        }
        return oppDetailMap;
    }
    
    @AuraEnabled 
    public static Map<String, Object> getOpportunityProductDetail(String opportunityProductId){
        
        Set<Id> productIds = new Set<Id>();
        Map<String, Object> response = new Map<String, Object>();
        List<Opportunity_Product_line_items__c> oppProdLineItems = [SELECT Id, Opportunity_Product__c, Name, Product__c, Quantity__c, Product__r.Name, Product__r.ProductCode, Product__r.Article_number__c, Opportunity_Product__r.Id, Opportunity_Product__r.Estimated_Quantity__c, Opportunity_Product__r.Expected_Price__c, Opportunity_Product__r.Product_Type__c, Opportunity_Product__r.Customer_Name__c, Opportunity_Product__r.Product__c, Opportunity_Product__r.Competitor__c, Opportunity_Product__r.Competitor_Remarks__c, Opportunity_Product__r.Additional_Product__c, Opportunity_Product__r.Additional_Product__r.Name, Opportunity_Product__r.Product__r.Name, Opportunity_Product__r.Status__c, Opportunity_Product__r.Customer_Name__r.Name, Glass_Item__c, Opportunity_Product__r.Additional_Product_Qty__c FROM Opportunity_Product_line_items__c WHERE Opportunity_Product__c = :opportunityProductId];
        Opportunity_Product__c oppProduct = new Opportunity_Product__c();
        List<ProductsWrapper> prdListToSend = new List<ProductsWrapper>();
        Map<String, String> productDetails = new Map<String, String>();
        ProductsWrapper pw;
        for(Opportunity_Product_line_items__c opl: oppProdLineItems) {
            
            if(!opl.Glass_Item__c) {
                pw = new ProductsWrapper();
                pw.prdCode = opl.Product__r.ProductCode;
                pw.prdParentId = opl.Opportunity_Product__c;
                pw.prdName = opl.Product__r.Name;
                pw.prdQuantitiy = Integer.valueof(opl.Quantity__c);
                pw.prdArticlenumber = opl.Product__r.Article_number__c;
                pw.oppProductLineItemId = opl.Id; 
                //  pw.prdParentId = pd.Parent_product__c;
                system.debug('opl.Id:::'+pw.oppProductLineItemId);
                prdListToSend.add(pw);
                productIds.add(opl.Product__c);
            }
        }
        
        oppProduct.Id = oppProdLineItems[0].Opportunity_Product__r.Id;
        oppProduct.Expected_Price__c = oppProdLineItems[0].Opportunity_Product__r.Expected_Price__c;
        oppProduct.Product_Type__c = oppProdLineItems[0].Opportunity_Product__r.Product_Type__c;
        oppProduct.Customer_Name__c = oppProdLineItems[0].Opportunity_Product__r.Customer_Name__c;
        oppProduct.Additional_Product_Qty__c = oppProdLineItems[0].Opportunity_Product__r.Additional_Product_Qty__c;
        system.debug('name::'+oppProdLineItems[0].Opportunity_Product__r.Customer_Name__c);
        system.debug('oppProduct::'+oppProduct);
        system.debug('name1::'+oppProdLineItems[0].Opportunity_Product__r.Customer_Name__c);
        oppProduct.Product__c = oppProdLineItems[0].Opportunity_Product__r.Product__c;
        oppProduct.Competitor__c = oppProdLineItems[0].Opportunity_Product__r.Competitor__c;
        oppProduct.Competitor_Remarks__c = oppProdLineItems[0].Opportunity_Product__r.Competitor_Remarks__c;
        oppProduct.Additional_Product__c = oppProdLineItems[0].Opportunity_Product__r.Additional_Product__c;
        oppProduct.Status__c = oppProdLineItems[0].Opportunity_Product__r.Status__c;
        system.debug('oppProduct::'+oppProduct);
        response.put('MASTERPRODNAME', oppProdLineItems[0].Opportunity_Product__r.Product__r.Name);
        response.put('OTHERPRODNAME', oppProdLineItems[0].Opportunity_Product__r.Additional_Product__r.Name);
        response.put('CUSTOMERNAME', oppProdLineItems[0].Opportunity_Product__r.Customer_Name__r.Name);        
        response.put('OPPPRODUCT', oppProduct);
        response.put('LINEITEMS', prdListToSend);
        system.debug('response::'+response);
        return response;
    }
    
    
    //below method is used to get the information for the 1st time user selecting "status = selected"
    //Get all the child products information of this master product where global_product__c = TRUE
    @AuraEnabled 
    public static List<ProductsWrapper> getNewProducts(String MasterProduct){
        system.debug('The coming MasterProductis:'+MasterProduct);
        list<ProductsWrapper> productsWrapperList = new list<ProductsWrapper>();
        system.debug('the val is:'+MasterProduct);
        for(product2 pd:[select id,name,ProductCode,Parent_product__c,Article_number__c,Quantity__c  from product2 where Parent_product__c=:MasterProduct])
        {
            system.debug('the val inside is:'+pd);
            
            productsWrapper pw = new ProductsWrapper();
            pw.prdCode = pd.productCode;
            pw.prdName = pd.Name;
            pw.prdQuantitiy = Integer.valueof(pd.Quantity__c);
            pw.prdArticlenumber = pd.Article_number__c;
            pw.prdParentId =pd.Parent_product__c;
            productsWrapperList.add(pw);
        }
        return productsWrapperList; 
    }
    
    //Below method is used to insert the product records into opportunity product line items object. 
    @AuraEnabled   //customername
    public static list<ProductsWrapper> upsertOppProductAndLineItems(String prdlist, String parentPrdId, String opportunityid, String masterPrd, String glassPrd, Opportunity_Product__c oppProduct, String pdType, String customername){
        system.debug('The save method is calling now:');
        Opportunity_Product_line_items__c opli;
        List<Opportunity_Product_line_items__c> opliListToInsert = new List<Opportunity_Product_line_items__c>();
        Map<String, ProductsWrapper> productsWrapperWithPrdNameMap = new Map<String, ProductsWrapper>();
        system.debug('The coming val is:'+'.............'+parentPrdId+'..........'+prdlist+'########'+customername);
        List<ProductsWrapper> RecsToInsert = (List<ProductsWrapper>) System.JSON.deserialize(prdlist, List<ProductsWrapper>.class);
        
        system.debug('records:::'+RecsToInsert.size());
        // intially get the selected products and put in one map. So that we can query again with name and master product it to get the 
        // specific product id. 
        for(productsWrapper pw1:RecsToInsert)
        {
            productsWrapperWithPrdNameMap.put(pw1.prdName, pw1);
        }
        system.debug('the coming apex vals:'+opportunityid);
        String recTypeId = [SELECT id, SobjectType FROM recordType WHERE Name='Shower Cubicles' AND SobjectType ='Opportunity_Product__c'].Id;
        // First intially create opportunity product record. 
        Opportunity_Product__c opr = new Opportunity_Product__c();
        opr.Status__c = oppProduct.Status__c;
        opr.Competitor__c = oppProduct.Competitor__c;
        opr.Competitor_Remarks__c = oppProduct.Competitor_Remarks__c;
        opr.Product_Type__c = pdType;
        opr.Product__c = masterPrd;
        opr.Opportunity__c = opportunityId;
        opr.Customer_Name__c = customername;
        opr.Additional_Product__c = glassPrd;
        opr.Additional_Product_Qty__c = oppProduct.Additional_Product_Qty__c;
        opr.RecordTypeId = recTypeId;
        opr.Expected_Price__c = oppProduct.Expected_Price__c;
        opr.Id = oppProduct.Id;
        system.debug('opr:::'+opr);
        upsert opr;
        
        //delete all existing line items
        if(oppProduct != null && oppProduct.Id != null) {
            
            List<Opportunity_Product_line_items__c> deleteOppProdLineItems = [SELECT Id FROM Opportunity_Product_line_items__c WHERE Opportunity_Product__c = :oppProduct.Id];
            system.debug('DELETE:'+deleteOppProdLineItems);
            delete deleteOppProdLineItems;
        }
        // Now query for to get the specific product id of the products. 
        for(product2 pd:[SELECT Id, Name, ProductCode FROM product2 WHERE Name IN :productsWrapperWithPrdNameMap.keyset() AND Parent_product__c = :parentPrdId])
        {
            opli = new Opportunity_Product_line_items__c();
            if(productsWrapperWithPrdNameMap.containsKey(pd.name))
            {
                //opli.Id = productsWrapperWithPrdNameMap.get(pd.Name).oppProductLineItemId;
                opli.Opportunity_Product__c =opr.id;
                opli.Name = pd.Name;
                opli.Product__c = pd.id;
                opli.Quantity__c = productsWrapperWithPrdNameMap.get(pd.Name).prdQuantitiy;
                opliListToInsert.add(opli);
            }  
        }
        
        system.debug('glassPrd::'+glassPrd);
        if(glassPrd != null) {
            
            opli = new Opportunity_Product_line_items__c();
            opli.Opportunity_Product__c = opr.id;
          //  opli.Name = glassPrd;
           opli.Name = [select name from product2 where id=:glassPrd].Name;
            opli.Product__c = glassPrd;
            opli.Quantity__c = opr.Additional_Product_Qty__c;
            opli.Glass_Item__c = true;
            opliListToInsert.add(opli);
        }
        system.debug('opliListToInsert::'+opliListToInsert.size());
        if(opliListToInsert.size() > 0)
            insert opliListToInsert;
        
        
        
        system.debug('opliListToInsert::'+opliListToInsert);
        
        // after inserting the new records we should reflet the same in the lightning component. So we are passing this data to lightning. 
        List<ProductsWrapper> prdListToSend = new List<ProductsWrapper>();
        for(Opportunity_Product_line_items__c opl: opliListToInsert)
        {
            if(!opl.Glass_Item__c) {
                ProductsWrapper pw = new ProductsWrapper();
                pw.prdCode = (productsWrapperWithPrdNameMap.get(opl.Name) != null) ? '' : productsWrapperWithPrdNameMap.get(opl.Name).prdCode;
                pw.prdName = opl.Name;
                pw.prdQuantitiy = Integer.valueof(opl.Quantity__c);
                pw.prdArticlenumber = productsWrapperWithPrdNameMap.get(opl.Name).prdArticlenumber;
                //  pw.prdParentId =pd.Parent_product__c;
                prdListToSend.add(pw);
            }
            
        }
        return prdListToSend;
    }
    
    // Below method is used for auto populating the specific product information. 
    @AuraEnabled 
    public static productsWrapper getSpecificProducts(String selectedProdId, String parentPrdId){
        system.debug('The changedProduct:'+selectedProdId+'###parentPrdId:'+parentPrdId);
     //   List<Product2> prods = [SELECT Id, Name, ProductCode, Article_number__c, Quantity__c FROM Product2 WHERE Id = :selectedProdId AND global_product__c = true AND isActive = true];
         List<Product2> prods = [SELECT Id, Name, ProductCode, Article_number__c, Quantity__c FROM Product2 WHERE Id = :selectedProdId];
        if(prods.size() > 0) {
            ProductsWrapper pw = new ProductsWrapper();
            pw.prdName = prods[0].Name;
            pw.prdCode = prods[0].productcode;
            pw.prdQuantitiy = integer.valueof(prods[0].Quantity__c);
            pw.prdArticlenumber = prods[0].Article_number__c;
            return pw;
        }
        return null;
    }
    
    
}
public class UpdateStatusforMaterialRequest{
    @InvocableMethod(label='UpdateApprovalStatusforMaterialRequest' description='Update the Approval Status on the basis of Material Request Region')
    
    public static void UpdateApprovalStatusValue(List<Id> MccIdList){
        Map<id,String>accregionmap= new Map<id,String>();
        Map<String,User>usermap = new Map<String,User>();
        List<Material_Request__c>acclist = new List<Material_Request__c>();
        map<string,user> roleWithUserNameMap = new map<string,user>();
        
        
        Set<String>UserIdset= new Set<String>();
        set<string> UserRoleNamesSet = new set<string>();
        UserRoleNamesSet.add('Regional Manager North');
        UserRoleNamesSet.add('Regional Manager South');
        UserRoleNamesSet.add('Regional Manager West');
        UserRoleNamesSet.add('Regional Manager East');
        UserRoleNamesSet.add('Regional Head Sales and Marketing West');
        UserRoleNamesSet.add('Regional Head Sales and Marketing East');
        UserRoleNamesSet.add('Regional Head Sales and Marketing North');
        UserRoleNamesSet.add('Regional Head Sales and Marketing South');
        
        
        for(user u1:[select id,name,userRole.name from user where userRole.name in:UserRoleNamesSet])
        {
            roleWithUserNameMap.put(u1.userRole.name,u1);
        }
        
        
        for(Material_Request__c acc:[select id,Region__c,RegionalHead__c,RegionalManager__c from Material_Request__c WHERE Id in : MccIdList ]){
            
            if(acc.Region__c.equalsIgnoreCase('East'))
              {
                acc.RegionalHead__c = roleWithUserNameMap.get('Regional Head Sales and Marketing East').Id;
                acc.RegionalManager__c = roleWithUserNameMap.get('Regional Manager East').Id;
              }
              else
               if(acc.Region__c.equalsIgnoreCase('West'))
              {
                acc.RegionalHead__c = roleWithUserNameMap.get('Regional Head Sales and Marketing West').Id;
                acc.RegionalManager__c = roleWithUserNameMap.get('Regional Manager West').Id;
              }
              else
               if(acc.Region__c.equalsIgnoreCase('North'))
              {
                acc.RegionalHead__c = roleWithUserNameMap.get('Regional Head Sales and Marketing North').Id;
                acc.RegionalManager__c = roleWithUserNameMap.get('Regional Manager North').Id;
              }
              else
               if(acc.Region__c.equalsIgnoreCase('South'))
              {
                acc.RegionalHead__c = roleWithUserNameMap.get('Regional Head Sales and Marketing South').Id;
                acc.RegionalManager__c = roleWithUserNameMap.get('Regional Manager South').Id;
              }
              acclist.add(acc);
        }
        
        update acclist;
        
    }//end of the UpdateApprovalMethod
}//end of the class
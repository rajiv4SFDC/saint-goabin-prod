public class PriceRequestManager {
	    
    public void fetchBeforeInsertRecordsForUpdation(List<Price_Request__c> PVList){
        Map<Id,List<Price_Request__c>> PVMap = new Map<Id,List<Price_Request__c>>();
        Map<Id,Opportunity_Product__c> optyProd_PR_Map;
        Set<Id> optyProdIds = new Set<Id>();
        
        //Preparing MAP<ownerid,list of his PR records>
            for(Price_Request__c n : (PVList)){
            List<Price_Request__c> PVOwnerList = PVMap.get(n.OwnerId__c);  
                if(PVOwnerList!=null && !PVOwnerList.isEmpty()){
                    PVOwnerList.add(n);
                }                    
                else {
                    PVOwnerList = new List<Price_Request__c>();
                    PVOwnerList.add(n);
                }

            PVMap.put(n.OwnerId__c,PVOwnerList);
            optyProdIds.add(n.Opportunity_Product__c);
        }  
       new UserManagerFieldsHandler().updatePVFields(PVMap,FALSE);

       optyProd_PR_Map = new Map<Id,Opportunity_Product__c>([SELECT Id, Opportunity__c FROM Opportunity_Product__c WHERE Id IN :optyProdIds]);
    
        //Stamping opportunity value on Price Request record
        for(Price_Request__c n : (PVList)){
            if(optyProd_PR_Map.containsKey(n.Opportunity_Product__c)){
                n.Opportunity__c = optyProd_PR_Map.get(n.Opportunity_Product__c).Opportunity__c;
            }
        }
    }    
}
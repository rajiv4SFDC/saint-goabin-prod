/* Created By: Rajiv Kumar Singh
Created Date: 06/06/2018
Class Name: AccoutNODInsertAPI 
Story : Account NOD flow from SAP to SFDC
Last Modified By :Rajiv Kumar Singh
Last Modified Date: 06/06/2018
*/

@RestResource(urlMapping='/accountNOD')
global class AccoutNODInsertAPI{
    @HttpPost
    global static void doHandleInsertRequest(){
        
        List<NODWrapper> wrapList = new List<NODWrapper>();
        List<NODs__c> NODList = new List<NODs__c>();
        RestRequest Req = RestContext.request;
        RestResponse res = RestContext.response;
        String body = Req.requestBody.toString().trim();
        
        if(body != null && body != '') {
            
            try {
                wrapList = (List<NODWrapper>)JSON.deserialize(body, List<NODWrapper>.class);
                
                for(NODWrapper we :wrapList){
                    NODs__c accNOD = new NODs__c();
                    if(we.SAPcode!= null && we.SAPcode!=''){
                        accNOD.SAP_Code__c = we.SAPcode;
                    }
                    if(we.accNODs!= null && we.accNODs!= ''){
                        accNOD.Account_Wise_NOD__c = we.accNODs;
                    }
                    if(we.AverageOutstanding!= null){
                        accNOD.Average_Outstanding__c = we.AverageOutstanding;
                    }
                    if(we.postDate!= null){
                        accNOD.Posting_Date__c = we.postDate;
                    }
                     if(we.Amt60!= null && we.Amt60!=''){
                        accNOD.Amt_60_Days__c = we.Amt60;
                    }
               
                    
                    NODList.add(accNOD);
                }
                
                if(NODList.size() > 0){
                    Insert NODList;
                }
                
                res.statusCode = 200;
                res.responseBody = Blob.valueOf(String.valueof(NODList));
                
            }catch (Exception e) {
                
                CommonUtilitiesHandler.handleExceptions(e);
                res.statusCode = 500;
                res.responseBody = Blob.valueOf(e.getMessage());
            }
        } else {
            res.statusCode = 500;
            res.responseBody = Blob.valueOf('Invalid Request');
        }
    }
    
    public class NODWrapper{
        public String SAPcode{get;set;}
        public String accNODs{get;set;}
        public Decimal AverageOutstanding{get;set;}
        public Date postDate{get;set;}
        public string Amt60{get;set;}
       
        
    }
}
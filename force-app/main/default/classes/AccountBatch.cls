global class AccountBatch implements Database.Batchable<sObject> {
    
    global Database.QueryLocator start(Database.BatchableContext BC){
		
        return Database.getQueryLocator('Select Id, Invoice_Date__c, Status__c FROM Account WHERE Status__c = true AND Invoice_Date__c != null');
    }

    global void execute(Database.BatchableContext BC, List<Account> accountDetails){
        
        List<Account> updateAccountDetails = new List<Account>();
        Integer days = 0;
        Date today = System.today();
        for(Account acct: accountDetails) {
            
            days = acct.Invoice_Date__c.daysBetween(today);
            if(days >= 365) {
                
            	acct.Status__c = false;
            	updateAccountDetails.add(acct);
            }
        }
        
        if(updateAccountDetails.size() > 0) {
        	
            update updateAccountDetails;
        }
    }
    
    global void finish(Database.BatchableContext BC) {
        
    }
}
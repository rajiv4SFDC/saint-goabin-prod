public class OpportunityTriggerHandler extends TriggerHandler {

	public OpportunityTriggerHandler() {}

	public override void beforeInsert() {
		//system.debug('Oppty before Insert Start');
        new OpportunityUserFieldsHandler().updateUserFields((List<Opportunity>) Trigger.new);//update Sales Area and region on opty
		new OpportunityTeamManager().checkRecalculateCheckboxOnOpptyInsert( (List<Opportunity>)Trigger.new );        
		//system.debug('Oppty before Insert end');        
	}

	public override void beforeUpdate() {
		//system.debug('Oppty before update Start');//
        //new OpportunityUserFieldsHandler().updateUserFields((List<Opportunity>) Trigger.new);//update Sales Area and region on opty
        //function to update opty owner fields on child records of opty
        new OpportunityUserFieldsHandler().updateMockUpPRFields((List<Opportunity>) Trigger.new, (Map<Id,Opportunity>)Trigger.oldMap);
 		new OpportunityTeamManager().checkRecalculateCheckboxOnOpptyUpdate((List<Opportunity>)Trigger.new,(Map<Id,Opportunity>)Trigger.oldMap );		
        //system.debug('Oppty before update end');        
	}

	public override void afterInsert() {
	   //system.debug('Oppty After Insert Start');
       new OpportunityTeamManager().addOpportunityOwnerAsOpptyTeamMember( (List<Opportunity>)Trigger.new );
       //system.debug('Oppty After Insert End');
    }

    public override void afterUpdate() {
    	//system.debug('Oppty After Update Start');
        new OpportunityTeamManager().updateOpportunityOwnerAsOpptyTeamMember( (List<Opportunity>)Trigger.new,(Map<Id,Opportunity>)Trigger.oldMap );
    	//system.debug('Oppty After Update End');
    } 

}
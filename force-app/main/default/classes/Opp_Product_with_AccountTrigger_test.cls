@istest
Private class Opp_Product_with_AccountTrigger_test
{

  @istest
   private static void testMethod1()
   {
   
        Map<Integer, String> numWithMonthNameMap = new Map<Integer, String>{
            1 => 'JAN',
                2 => 'FEB',
                3 => 'MAR',
                4 => 'APR',
                5 => 'MAY',
                6 => 'JUN',
                7 => 'JUL',
                8 => 'AUG',
                9 => 'SEP',
                10 => 'OCT',
                11 => 'NOV',
                12 => 'DEC'
                };
                    
        // creating product record here. 
        Product2 prod = new Product2();
        prod.Name = 'COOL-LITE K-KB T140-CLEAR-6mm';
        prod.ProductCode = '401-181-000-000-00';
        prod.Type__c = 'Interior';           
        prod.Thickness_Code__c = '0500';
        prod.External_Id__c = '401-181-000-000-00'+ '0500';
        prod.List_Price__c = 1500;
        prod.Family = 'SOLAR C';
        prod.Sub_Family__c = 'FLT CLEAR';
        prod.Special_Price_Flag__c = 'X';
        prod.IsActive = true;
        prod.Product_Category__c = 'Inspire';
        insert prod;
        
        // creating product tier pricing here. 
        Product_Tier_Pricing__c ptr = new Product_Tier_Pricing__c();
        ptr.Name = 'Testing tier pricing';
        ptr.Tier_Name__c = 'T1';
        ptr.Tier_Pricing__c = 200;
        ptr.Business_Location__c = 'DEL1';
        ptr.Product__c = prod.id;
        ptr.Product_Code__c = prod.ProductCode;
        insert ptr;      
        
        // creating account here
        Id crmRecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('CRM').getRecordTypeId();
        Id erpRecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('ERP').getRecordTypeId();
        Id inpsireRecTypeId = [select id,name,SobjectType from recordtype where SobjectType='Opportunity_Product__c' AND name='Inspire'].id;
        
        Account acc =new Account();
        acc.Name = 'Test AccountCRM';
        acc.Type = 'Architect';
        acc.Sales_Area_Region__c = 'North';
        acc.Sales_Area__c = 'DEL1';
        acc.Interior_Tier__c = 'T1';
        acc.RecordTypeId = crmRecordTypeId;
        insert acc;
        
        Account accErp =new Account();
        accErp.Name = 'Test AccountERP';
        accErp.Type = 'Interior';
        accErp.Sales_Area_Region__c = 'North';
        accErp.Sales_Office_ERP__c = 'DEL1';
        accErp.RecordTypeId = erpRecordTypeId;
        accErp.SAP_Code__c = '1000101123';
        accErp.Interior_Tier__c = 'T1';
        insert accErp;
        
        // creating opportunity for this account. 
        Opportunity oppty = new Opportunity();
        oppty.Name = 'Test Oppty';
        //  oppty.Project__c =  lstOfproj.get(0).id;
        oppty.AccountId = acc.Id;
        oppty.StageName = 'Confirmed';
        oppty.CloseDate = Date.newInstance(2017,8, 31);
        oppty.Region__c = 'North';
        oppty.Sales_Area__c = 'DEL1';
        insert oppty;
        
        Opportunity_Product__c opptyProd = new Opportunity_Product__c();
        opptyProd.Product__c = prod.id;
        opptyProd.Product_Type__c = 'DGU';
        opptyProd.Status__c = 'Selected';
        opptyProd.Opportunity__c = oppty.id;
        opptyProd.RecordTypeId = inpsireRecTypeId;
        insert opptyProd;
        
        Product_Forecast__c pf =new Product_Forecast__c();
        pf.Opportunity_Product__c = opptyProd.id;
        pf.Quantity__c = 1200;
        pf.year__c = string.valueof(Date.Today().Year());
        pf.Month__c = numWithMonthNameMap.get(Date.Today().Month());
        pf.Forecast_Date__c = Date.newInstance(Date.Today().Year(),Date.Today().Month(), 1);        
        insert pf;     
        
        // create invoice.      
        invoice__c inv = new invoice__c ();
        inv.name='testing inv';
        inv.Product_Code__c = prod.Productcode;
        inv.Billing_Date__c = Date.Today();
        inv.Quantity__c = 1100;
        inv.Invoice_type__c = 'Inspire';
        insert inv;
        
        // Create opp product with custom record here. 
        Opportunity_Product_with_Account__c opwa = new Opportunity_Product_with_Account__c();
        opwa.Account__c = accErp.id;     
        opwa.Opportunity_Product__c = opptyProd.id;
        insert opwa;
        
        
        Manual_Accounting__c ma = new Manual_Accounting__c();
        ma.Opportunity_Product_Customer__c = opwa.id;
        ma.Quantity__c = 120;
        insert ma;
        
        Opportunity_Product_with_Account__c  op1 =[select id,name,account__c,Opportunity_Product__c  from Opportunity_Product_with_Account__c limit 1];
        list<Opportunity_Product_with_Account__c> newlist = new list<Opportunity_Product_with_Account__c>();
        
        
        map<id,Opportunity_Product_with_Account__c> oldMap = new map<id,Opportunity_Product_with_Account__c>();
        oldMap.put(op1.id,op1); 
        
       
         Account accErp2 =new Account();
        accErp2.Name = 'Test1 AccountERP';
        accErp2.Type = 'Interior';
        accErp2.Sales_Area_Region__c = 'North';
        accErp2.Sales_Office_ERP__c = 'DEL2-INF';
        accErp2.RecordTypeId = erpRecordTypeId;
        accErp2.SAP_Code__c = '10232300101123';
        accErp2.Interior_Tier__c = 'T1';
        insert accErp2;
        
        op1.account__c = accErp2.id;
      //  update op1;
        newlist.add(op1);
        
        Opp_Product_with_AccountTrigger_Handler.afterUpdateHandler(newlist,oldMap);
        
    
   }


}
global class InvoiceAppropriationBatchable implements Database.Batchable<sObject> {
    
    global Database.QueryLocator start(Database.BatchableContext BC){
        String query = 'SELECT Id,Invoice_type__c FROM Invoice__c WHERE Tagged__c = true AND Processed__c = false AND To_Be_Processed__c = true';
       
       // The above query filter condition is changed by sai on 26-apr-2017. 
       
    //  String query = 'SELECT Id FROM Invoice__c WHERE Tagged__c = true AND Processed__c = false AND To_Be_Processed__c = true AND Invoice_type__c ='+'\''+'Infinity'+'\'';

        System.debug('Invoice Appropriation Batchable ' + query);
        return Database.getQueryLocator(query);
    }

    global void execute(Database.BatchableContext BC, List<sObject> scope){
        Set<Id> invoiceIdSet = new Set<Id>();
        Set<Id> invoiceScIdSet = new Set<Id>();

        
        System.debug('Invoice Appropriation Batchable : Batch Size' + invoiceIdSet.size());
        for(Invoice__c inv : (List<Invoice__c>) scope) {
         if(inv.Invoice_type__c != 'Shower Cubicles')
            invoiceIdSet.add(inv.id);
      else      
         if(inv.Invoice_type__c == 'Shower Cubicles')
            invoiceScIdSet.add(inv.id);
               
        }
        System.debug('Invoice Appropriation Batchable invoiceScIdSet : Batch Size' + invoiceScIdSet.size());
        //Initiate the process of recalculation
        if(invoiceIdSet.size()>0)
        InvoiceAppropriationHelper.appropriateInvoice(invoiceIdSet,false);
        
        if(invoiceScIdSet.size()>0)
        InvoiceAppropriationHelperForSc.appropriateInvoice(invoiceScIdSet,false);
    }

    global void finish(Database.BatchableContext BC){}
}
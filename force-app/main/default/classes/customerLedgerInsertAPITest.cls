/* Created By: Rajiv Kumar Singh
Created Date: 06/06/2017
Class Name: customerLedgerInsertAPITest
Description : Test class for customerLedgerInsertAPI
Last Modified By :Rajiv Kumar Singh
Last Modified Date: 06/06/2017
*/

@IsTest
private class customerLedgerInsertAPITest{
    
    static testMethod void testPostMethod(){
        String JSONMsg = '[{"SAPcode":"512907","PostingDate":"2018-04-30","CreatedDate":"2018-05-01","Amount":"3475.00","Type":"Credit","DocNo":"G50-8504024104-2018","DocText":"Cash Discount-April 18-8514001246"}]';
        RestRequest request = new RestRequest();
        request.requestUri ='/services/apexrest/customerLedger';
        request.httpMethod = 'POST';
        request.requestBody = Blob.valueof(JSONMsg);
        RestContext.request = request;
        RestContext.response = new RestResponse();
        customerLedgerInsertAPI.doHandleInsertRequest();
        
        JSONMsg = '[1';
        request = new RestRequest();
        request.requestUri ='/services/apexrest/customerLedger';
        request.httpMethod = 'POST';
        request.requestBody = Blob.valueof(JSONMsg);
        RestContext.request = request;
        RestContext.response = new RestResponse();
        customerLedgerInsertAPI.doHandleInsertRequest();
        
        request = new RestRequest();
        request.requestUri ='/services/apexrest/customerLedger';
        request.httpMethod = 'POST';
        request.requestBody = Blob.valueof('');
        RestContext.request = request;
        RestContext.response = new RestResponse();
        customerLedgerInsertAPI.doHandleInsertRequest();
    }
}
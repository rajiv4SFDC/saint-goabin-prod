@isTest
public class TestDataFactory {

    @testSetup
    public static void callFullDataFactory(){
        createCustomSetting();
        createTestProductData();
        createTestStateAndCityRecordData();
        createTestCatalogueBudgetData();
        createTestCampaignRecordData();
        createTestAccountData();
        createTestProjectData();
        createTestOpportunityData();
        createTestOpportunityProductData();
        createTestPriceRequestData();
        createTestProductForecateData();
        createTestForecateInvoiceData();
        createTestMockupRecordData();
        createTestInvoiceRecordData();
        createTestEventsRecordData();
        createTestTasksRecordData();
        createTestEmailMessageData();
        createTestTargetActualRecordData();
    }

    
    public static User createTestUser(String fName,String lName){

        //String fName = 'Test';
        //String lName = 'LastTestName';

        Profile profKAM = [SELECT id FROM Profile WHERE Name='Infinity KAM User' LIMIT 1 ];
        UserRole roleKAM = [SELECT id FROM UserRole WHERE Name='DEL1' LIMIT 1];

        String orgId = UserInfo.getOrganizationId();
        String dateString = String.valueof(Datetime.now()).replace(' ','').replace(':','').replace('-','');
       
        Integer randomInt = Integer.valueOf(math.rint(math.random()*1000000));
        String uniqueName = orgId + dateString + randomInt;
        User tuser = new User(  firstname = fName,
                                lastName = lName,
                                email = uniqueName + '@test' + orgId + '.org',
                                Username = uniqueName + '@test' + orgId + '.org',
                                EmailEncodingKey = 'ISO-8859-1',
                                Alias = uniqueName.substring(18, 23),
                                TimeZoneSidKey = 'America/Los_Angeles',
                                LocaleSidKey = 'en_US',
                                LanguageLocaleKey = 'en_US',
                                Region__c = 'North',
                                Region_Text__c = 'North',
                                Sales_Area__c = 'DEL1',
                                Sales_Area_Text__c = 'DEL1',
                                ProfileId = profKAM.Id,
                                UserRoleId = roleKAM.Id);
        return tuser;
    }
    
    @future
    public static void createTwoTestUsers(){

        //String fName = 'Test';
        //String lName = 'LastTestName';

        Profile profKAM = [SELECT id FROM Profile WHERE Name='Infinity KAM User' LIMIT 1 ];
        Profile profKAM2 = [SELECT id FROM Profile WHERE Name='Infinity Manager' LIMIT 1 ];   
        System.debug('Profile List :'+profKAM);
        UserRole roleKAM1 = [SELECT id FROM UserRole WHERE Name='DEL1' LIMIT 1];
        UserRole roleKAM2 = [SELECT id FROM UserRole WHERE Name='DEL2' LIMIT 1];
		System.debug('Role List :'+roleKAM1+' '+roleKAM2);
        
        String orgId = UserInfo.getOrganizationId();
        String dateString = String.valueof(Datetime.now()).replace(' ','').replace(':','').replace('-','');
       
        Integer randomInt = Integer.valueOf(math.rint(math.random()*1000000));
        Integer randomInt2 = Integer.valueOf(math.rint(math.random()*1000000));
        String uniqueName = orgId + dateString + randomInt;
        String uniqueName2 = orgId + dateString + randomInt2;
        User tuser = new User(  firstname = 'Compro',
                                lastName = 'Tester',
                                email = uniqueName + '@test' + orgId + '.org',
                                Username = uniqueName + '@test' + orgId + '.org',
                                EmailEncodingKey = 'ISO-8859-1',
                                Alias = uniqueName.substring(18, 23),
                                TimeZoneSidKey = 'America/Los_Angeles',
                                LocaleSidKey = 'en_US',
                                LanguageLocaleKey = 'en_US',
                                Region__c = 'North',
                                Region_Text__c = 'North',
                                Sales_Area__c = 'DEL1',
                                Sales_Area_Text__c = 'DEL1',
                                ProfileId = profKAM.Id,
                                UserRoleId = roleKAM1.Id);
        insert tuser;
         User tuser2 = new User(  firstname = 'Tester',
                                lastName = 'Compro',
                                email = uniqueName2 + '@test' + orgId + '.org',
                                Username = uniqueName2 + '@test' + orgId + '.org',
                                EmailEncodingKey = 'ISO-8859-1',
                                Alias = uniqueName2.substring(18, 23),
                                TimeZoneSidKey = 'America/Los_Angeles',
                                LocaleSidKey = 'en_US',
                                LanguageLocaleKey = 'en_US',
                                Region__c = 'North',
                                Region_Text__c = 'North',
                                Sales_Area__c = 'DEL2',
                                Sales_Area_Text__c = 'DEL2',
                                ProfileId = profKAM2.Id,
                                UserRoleId = roleKAM2.Id);
        insert tuser2;
    }

    public static void createCustomSetting(){
        ValidationRule__c settings = ValidationRule__c.getOrgDefaults();
        settings.Bypass_Rule__c = true;
        upsert settings ValidationRule__c.Id;
    }

    public static void createTestProductData(){

        /* All Range Type Product */
        List<Product2> lstOfProduct = new List<Product2>();
        List<String> allRangeType = new List<String>{'Antelio Plus','Cool-Lite','Cool Vision','Envision',
                                                      'Evo','Evolve Glass','Nano','Nano Silver','Planitherm',
                                                      'Premium Plus','Sage Glass','Std 3.5 to 5 mm','Xtreme'}; 
        List<String> allCatalogueType = new List<String>{'Blues','Bronze','Cool Vision','Diamant','Envision',
                                                         'Envision Plus','Evo','Luxe','Nano','Nano Silver',
                                                         'Residential'};                                              
                                                      
        Integer counter = 0 ;                                             

        for( String rangeType : allRangeType ){

            Product2 prod = new Product2();
            prod.Name = 'COOL-LITE K-KB T140-CLEAR-6mm';
            prod.ProductCode = '401-181-000-000-00'+counter;
            prod.Type__c = 'Infinity';
            prod.Product_Range__c = rangeType;
            prod.Thickness_Code__c = '0500';
            prod.External_Id__c = '401-181-000-000-00'+ counter + '0500';
            prod.List_Price__c = 1500;
            prod.Family = 'SOLAR C';
            prod.Sub_Family__c = 'FLT CLEAR';
            prod.Special_Price_Flag__c = 'X';
            prod.IsActive = true;
			prod.Product_Category__c = 'INFINITY';
            lstOfProduct.add(prod);

            counter++;
        }

        /* All Catalogue Products */
        for( String catalogueType : allCatalogueType ){

            Product2 prod = new Product2();
            prod.Name = catalogueType;
            prod.Type__c = 'Infinity';
            prod.Family = 'Catalogue';
            prod.IsActive = true;
			prod.Product_Category__c = 'INFINITY';
            lstOfProduct.add(prod);
        }

        insert lstOfProduct;
        
    }

    public static void createTestStateAndCityRecordData(){
        State__c sts = new State__c();
        sts.External_ID__c = '1';
        sts.Name = 'Assam';
        insert sts;

        City__c cty = new City__c();
        cty.Name = 'Abhayapuri';
        cty.External_ID__c = '1-1';
        cty.City_Code__c = '1';
        cty.Category__c = 'iCities';
        cty.State__c = sts.id;
        insert cty;
    }

    public static void createTestCatalogueBudgetData(){

        List<Catalogue_Budget__c> lstCatalogueBudget = new List<Catalogue_Budget__c>();
        List<Product2> lstOfcatalogueProd = [SELECT id FROM Product2 WHERE Family='Catalogue'];
        User userAdmin = [SELECT id,Name,Profile.Name FROM User where Profile.Name='System Administrator' AND Name Like 'SF%' LIMIT 1];

        for(Product2 prod : lstOfcatalogueProd){

            Catalogue_Budget__c catBud = new Catalogue_Budget__c();
            catBud.Catalogue__c = prod.id;
            catBud.Budget_Allocated__c = 1000;
            catBud.Budget_Period__c = Date.newInstance(2017,8, 27);
            catBud.Units_Consumed__c = 0;
            catBud.OwnerId = userAdmin.Id;
            

            lstCatalogueBudget.add(catBud);
        }

    }

    public static void createTestAccountData(){
        
        Id crmRecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('CRM').getRecordTypeId();
        Id erpRecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('ERP').getRecordTypeId();
        List<User> userList = [select id from User where UserRole.Name LIKE '%DEL1%' LIMIT 1];
        
        Account acc =new Account();
        acc.Name = 'Test AccountCRM';
        acc.Type = 'Architect';
        acc.Sales_Area_Region__c = 'North';
        acc.Sales_Area__c = 'DEL1';
        acc.RecordTypeId = crmRecordTypeId;

        insert acc;

        Contact conCRM = new Contact();
        conCRM.LastName = 'TestContact CRM';
        conCRM.AccountId = acc.Id;
        conCRM.OwnerId = userList[0].Id; 
        conCRM.MobilePhone = '9898998989';
        insert conCRM;
        
        Account accErp =new Account();
        accErp.Name = 'Test AccountERP';
        accErp.Type = 'Processor';
        accErp.Sales_Area_Region__c = 'North';
        accErp.Sales_Office_ERP__c = 'DEL1';
        accErp.RecordTypeId = erpRecordTypeId;

        insert accErp;

        Contact conERP = new Contact();
        conERP.LastName = 'TestContact ERP';
        conERP.AccountId = accErp.Id;
        conERP.OwnerId = userList[0].Id; 
        conERP.MobilePhone = '9898998989';
        insert conERP;
    }

    public static void createTestProjectData(){
        Account accCRM = [SELECT id,RecordType.Name from Account WHERE RecordType.Name= 'CRM' LIMIT 1];
        City__c cty = [SELECT id,State__c FROM City__c LIMIT 1]; 

        Project__c proj =new Project__c();
        proj.Name = 'Star Fisheries';
        proj.Segment__c = 'Residential Buildings';
        proj.Region__c = 'North';
        proj.Sales_Area__c = 'DEL1';
        proj.Validated__c = true;
        proj.Project_Validated_Date__c = Datetime.newInstance(2017, 8, 27);
        proj.Glass_Govern__c = true;
        proj.Glass_Heals__c = true;
        proj.Cool_Homes__c = true;
        proj.Luxe_Glass__c = true;
        proj.Luxe_Homes__c = true;
        proj.Referrer_Account__c = accCRM.id;
        proj.Geolocation__Latitude__s = 28.560100;
        proj.Geolocation__Longitude__s = 77.219319;
        proj.City__c = cty.id;
        proj.State__c = cty.State__c;

        insert proj;

        Project__c proj2 =new Project__c();
        proj2.Name = 'Star Fisheries Whale';
        proj2.Segment__c = 'Residential Buildings';
        proj2.Region__c = 'North';
        proj2.Sales_Area__c = 'DEL1';
        proj2.Validated__c = true;
        proj2.Project_Validated_Date__c = Datetime.newInstance(2017, 8, 27);
        proj2.Glass_Govern__c = true;
        proj2.Glass_Heals__c = true;
        proj2.Cool_Homes__c = true;
        proj2.Luxe_Glass__c = true;
        proj2.Luxe_Homes__c = true;
        proj2.Referrer_Account__c = accCRM.id;
        proj2.Geolocation__Latitude__s = 28.560100;
        proj2.Geolocation__Longitude__s = 77.219319;
        proj.City__c = cty.id;
        proj.State__c = cty.State__c;

        insert proj2;
        
        Project__c proj3 =new Project__c();
        proj3.Name = 'Star Fisheries Shark';
        proj3.Segment__c = 'Residential Buildings';
        proj3.Region__c = 'North';
        proj3.Sales_Area__c = 'DEL1';
        proj3.Validated__c = true;
        proj3.Project_Validated_Date__c = Datetime.newInstance(2017, 8, 30);
        proj3.Glass_Govern__c = true;
        proj3.Glass_Heals__c = true;
        proj3.Cool_Homes__c = true;
        proj3.Luxe_Glass__c = true;
        proj3.Luxe_Homes__c = true;
        proj3.Referrer_Account__c = accCRM.id;
        proj3.Geolocation__Latitude__s = 28.560100;
        proj3.Geolocation__Longitude__s = 77.219319;
        proj3.City__c = cty.id;
        proj3.State__c = cty.State__c;

        insert proj3;
    }

    public static void createTestOpportunityData(){
        List<Project__c> lstOfproj = [SELECT id FROM Project__c WHERE Validated__c= true limit 3];
        Account accCRM = [SELECT id,RecordType.Name from Account WHERE RecordType.Name= 'CRM' LIMIT 1];
		List<User> ownerid = [select id from User where UserRole.Name LIKE '%DEL1%' OR UserRole.Name LIKE '%MUM1%' LIMIT 1];
        
        Opportunity oppty = new Opportunity();
        oppty.Name = 'Test Oppty';
        oppty.Project__c =  lstOfproj.get(0).id;
        oppty.AccountId = accCRM.Id;
        oppty.StageName = 'Confirmed';
        oppty.CloseDate = Date.newInstance(2017,8, 31);
        oppty.Region__c = 'North';
        oppty.Sales_Area__c = 'DEL1';
        oppty.Design_Report_Required__c = 'Yes';
        oppty.Design_Report_Created__c = 'Yes';
        oppty.Design_Report_Created_Date__c = Date.newInstance(2017,8, 27);
        oppty.OwnerId = ownerid[0].Id; 
        oppty.Jumbo_Resizing_Completed__c = Date.newInstance(2017,8, 27);
        oppty.Glass_Pro_Live_Completed__c = Date.newInstance(2017,8, 27);
        
        insert oppty;

        Opportunity oppty2 = new Opportunity();
        oppty2.Name = 'Test Oppty';
        oppty2.Project__c =  lstOfproj.get(1).id;
        oppty2.AccountId = accCRM.Id;
        oppty2.StageName = 'Confirmed';
        oppty2.CloseDate = Date.newInstance(2017,8, 31);
        oppty2.Region__c = 'North';
        oppty2.Sales_Area__c = 'DEL1';
        oppty2.Design_Report_Required__c = 'Yes';
        oppty2.Design_Report_Created__c = 'Yes';
        oppty2.Design_Report_Created_Date__c = Date.newInstance(2017,8, 27);
        oppty2.Jumbo_Resizing_Completed__c = Date.newInstance(2017,8, 27);
        oppty2.Glass_Pro_Live_Completed__c = Date.newInstance(2017,8, 27);

        insert oppty2;
        
        Opportunity oppty3 = new Opportunity();
        oppty3.Name = 'Test Oppty';
        oppty3.Project__c =  lstOfproj.get(2).id;
        oppty3.AccountId = accCRM.Id;
        oppty3.StageName = 'Confirmed';
        oppty3.CloseDate = Date.newInstance(2017,9,19);
        oppty3.Region__c = 'North';
        oppty3.Sales_Area__c = 'DEL1';
        oppty3.Design_Report_Required__c = 'Yes';
        oppty3.Design_Report_Created__c = 'Yes';
        oppty3.Design_Report_Created_Date__c = Date.newInstance(2017,9, 27);
		//oppty3.OwnerId = ownerid[0].Id; 
        insert oppty3;
    }

    public static void createTestOpportunityProductData(){

        List<Product2> lstOfProd = [SELECT id FROM Product2 WHERE NOT  Family Like 'Catalogue%'];
        List<Opportunity> lstOfOppty = [SELECT id FROM Opportunity];
        List<Opportunity_Product__c> lstOfOpptyProd = new List<Opportunity_Product__c>();

        for(Product2 prod : lstOfProd){

            for(Opportunity oppty : lstOfOppty){

                Opportunity_Product__c opptyProd = new Opportunity_Product__c();
                opptyProd.Product__c = prod.id;
                opptyProd.Product_Type__c = 'DGU';
                opptyProd.Status__c = 'Selected';
                opptyProd.Opportunity__c = oppty.id;

                lstOfOpptyProd.add(opptyProd);
            }
        }   

        insert lstOfOpptyProd;
    }

    public static void createTestPriceRequestData(){

        Opportunity_Product__c opptyProd = [SELECT id FROM Opportunity_Product__c LIMIT 1];

        Price_Request__c pr = new Price_Request__c();
        pr.Price_Type__c = 'Special Price';
        pr.Requested_Quantity__c = 500;
        pr.Required_Price__c = 400;
        pr.Opportunity_Product__c = opptyProd.Id;
        pr.Approval_Status__c = 'Approved';
        pr.Valid_From__c =  Date.newInstance(2017,8, 27);
        pr.Valid_To__c = Date.newInstance(2017,8, 31);
        
        insert pr;
        
    }

    public static void createTestProductForecateData(){

        List<Opportunity_Product__c> LstOfopptyProd = [SELECT id,Opportunity__c,Opportunity__r.Re_Calculate__c FROM Opportunity_Product__c LIMIT 2];
        
        Product_Forecast__c prodFore = new Product_Forecast__c();
        prodFore.Opportunity_Product__c = LstOfopptyProd[0].id;
        prodFore.Month__c = 'AUG';
        prodFore.Year__c = '2017';
        prodFore.Forecast_Date__c = Date.newInstance(2017,8, 1);
        prodFore.External_Id__c = LstOfopptyProd[0].id + '-' +'8'+ '-' + '2017' ;
        prodFore.Quantity__c = 600;
        
        insert prodFore;
        
        /*Update oppty Chekcbox*/
        Opportunity oppty = new Opportunity(id=LstOfopptyProd[1].Opportunity__c,Re_Calculate__c=false);
        update oppty;

        Product_Forecast__c prodFore2 = new Product_Forecast__c();
        prodFore2.Opportunity_Product__c = LstOfopptyProd[1].id;
        prodFore2.Month__c = 'SEPT';
        prodFore2.Year__c = '2017';
        prodFore2.Forecast_Date__c = Date.newInstance(2017,9, 1);
        prodFore2.External_Id__c = LstOfopptyProd[1].id + '-' +'9'+ '-' + '2017' ;
        prodFore2.Quantity__c = 300;
        
        insert prodFore2;
    }

    public static void createTestForecateInvoiceData(){

        Product_Forecast__c prodFore = [SELECT id FROM Product_Forecast__c LIMIT 1];

        Forecast_Invoice__c foreInv = new Forecast_Invoice__c();
        foreInv.Product_Forecast__c = prodFore.id;
        //foreInv.Invoice_Billing_Date__c = Date.newInstance(2017,8, 20);
        foreInv.Appropriable_Quantity__c = 300;
        
        insert foreInv;
    }

    public static void createTestMockupRecordData(){

        List<Opportunity> lstOfOppty = [SELECT id FROM Opportunity];
        List<Mock_Up_Request__c> lsTofMockup = new List<Mock_Up_Request__c>();

        for(Opportunity oppty  : lstOfOppty){

            Mock_Up_Request__c mock = new Mock_Up_Request__c();
            mock.Opportunity__c = oppty.id;

            lsTofMockup.add(mock);
        }

        insert lsTofMockup;
    }
    
    public static void createTestPlantVisitRecordData(){

        List<Opportunity> opty = [SELECT id FROM Opportunity LIMIT 1];
        List<User> userList = [Select id, ProfileId, UserRoleId, UserRole.Name, Profile.Name from User where UserRole.Name!= null and Profile.Name != null limit 2 ];
  		List<Plant_Visit__c> PVList = new List<Plant_Visit__c>();
        Plant_Visit__c firstpv = new Plant_Visit__c();
        firstpv.Opportunity__c = opty[0].id;
        firstpv.Approval_Status__c = 'Draft';
        firstpv.Visit_Date__c = Date.newInstance(2017,9, 1);
        firstpv.Plant__c = 'Bhiwadi' ;
        firstpv.OwnerId = userList[0].Id;
        PVList.add(firstpv);
        
        Plant_Visit__c secondpv = new Plant_Visit__c();
        secondpv.Opportunity__c = opty[0].id;
        secondpv.Approval_Status__c = 'Pending';
        secondpv.Visit_Date__c = Date.newInstance(2017,9, 1);
        secondpv.Plant__c = 'Bhiwadi' ;
        secondpv.OwnerId = userList[1].Id;
        PVList.add(secondpv);
       
        List<User> ownerList = [select id from User where UserRole.Name LIKE '%DEL1%' OR UserRole.Name LIKE '%MUM1%' LIMIT 1];

        Plant_Visit__c thirdpv = new Plant_Visit__c();
        thirdpv.Opportunity__c = opty[0].id;
        thirdpv.Approval_Status__c = 'Approved';
        thirdpv.Visit_Date__c = Date.newInstance(2017,8, 20);
        thirdpv.Plant__c = 'Bhiwadi' ;
        thirdpv.OwnerId = ownerList[0].Id;
        PVList.add(thirdpv);
        
        insert PVList;
    }

    public static void createTestInvoiceRecordData(){

        Price_Request__c priceReq = [SELECT id,Name FROM Price_Request__c LIMIT 1];
        List<Invoice__c> lstOfInv = new List<Invoice__c>();
        
        Invoice__c inv =  new Invoice__c(Name='Invoice Succ',
                                        Transaction_No__c=priceReq.Name,
                                        Billing_Date__c = Date.newInstance(2017,8, 20),
                                        Non_Appropriable_Quantity__c = 500,
                                        Appropriable_Quantity__c = 0,
                                        Quantity__c = 500,
                                        To_be_processed__c=true);

        // No Transaction No
        Invoice__c inv2 =  new Invoice__c(Name='Invoice Error',To_be_processed__c=true);

        lstOfInv.add(inv);
        lstOfInv.add(inv2);
        insert lstOfInv;
        
    }

    public static void createTestTargetActualRecordData(){

        List<Target_and_Actual__c> lstOfTarAct = new List<Target_and_Actual__c>();

        List<String> lstOfTargetType = new List<String>{'Total Sales Volume','New Project Start','Project Database Addition',
                                                        'Sales Residential Segment','Design Reports',
                                                        'Influencer Engagement Activities','Luxe Glass',
                                                        'Cool Homes','Glass Govern','Edge Meet',
                                                         'Mr. Build','Windowmakers\' Meet','Glass Pro Live',
                                                         'Jumbo Resizing','I -2 Visits','Call logs',
                                                         'E-mail integration','Contacts added','Blinds that Blind'}; //'Luxe Homes','Glass Heals'

        List<String> lstOfRangeType = new List<String>{'Premium Plus','Cool Vision','Sage Glass',
                                                        'Evolve Glass','Antelio Plus','Cool-Lite',
                                                        'Envision','Evo','Nano','Nano Silver',
                                                        'Planitherm','Std 3.5 to 5 mm','Xtreme'};

        City__c cty = [SELECT id FROM City__c LIMIT 1]; 

        for( String tarType: lstOfTargetType){

            Target_and_Actual__c tvac = new Target_and_Actual__c();
            tvac.Target_Type__c = tarType;
            tvac.Sales_Area__c = 'DEL1';
            tvac.Region__c = 'North';
            tvac.Target__c = 500;
            tvac.Actual__c = 0;
            tvac.Points_Eligible__c = 10;
            tvac.Backend_Points_Eligible__c= 10;
               if( tarType == 'E-mail integration' || tarType == 'Contacts added'  ){
                    tvac.Target_Date__c = system.today();
               }else{  
                    tvac.Target_Date__c = Date.newInstance(2017,8, 20);
               }
            
            tvac.Calculate_Actuals__c = true;
            tvac.Calculate_Point_Scored__c = true;

            lstOfTarAct.add(tvac);
        }

        // City wise Traget
        Target_and_Actual__c tvac = new Target_and_Actual__c();
        tvac.Target_Type__c = 'iCities';
        tvac.Sales_Area__c = 'DEL1';
        tvac.Region__c = 'North';
        tvac.Target__c = 500;
        tvac.Actual__c = 0;
        tvac.Points_Eligible__c = 10;
        tvac.Target_Date__c = Date.newInstance(2017,8, 20);
        tvac.Calculate_Actuals__c = true;
        tvac.Calculate_Point_Scored__c = true;
        tvac.City_External_Id__c = '1-1';
        tvac.City__c = cty.id;

        lstOfTarAct.add(tvac);

        for( String rangeType: lstOfRangeType){

            Target_and_Actual__c tvac1 = new Target_and_Actual__c();
            tvac1.Target_Type__c = 'Product Range wise Sales Targets';
            tvac1.Product_Range__c = rangeType;
            tvac1.Sales_Area__c = 'DEL1';
            tvac1.Region__c = 'North';
            tvac1.Target__c = 500;
            tvac1.Actual__c = 0;
            tvac1.Points_Eligible__c = 10;
            tvac1.Target_Date__c = Date.newInstance(2017,8, 20);
            tvac1.Calculate_Actuals__c = true;
            tvac1.Calculate_Point_Scored__c = true;

            lstOfTarAct.add(tvac1);
        }

        insert lstOfTarAct;
    }

     
    public static void createTestCampaignRecordData(){

        List<User> userList = [select id from User where UserRole.Name LIKE '%DEL1%' LIMIT 1];

        Campaign cmp1 = new Campaign();
        cmp1.Name = 'Test Campaign';
        cmp1.IsActive = true;
        cmp1.StartDate =  Date.newInstance(2017,8, 1);
        cmp1.EndDate = Date.newInstance(2017,8, 31);
        cmp1.Type = 'Edge Plus';
        cmp1.Sales_Area__c = 'DEL1';
        cmp1.Status = 'Completed';
        cmp1.OwnerId = userList[0].id;
        insert cmp1;

        Campaign cmp2 = new Campaign();
        cmp2.Name = 'Test Campaign';
        cmp2.IsActive = true;
        cmp2.StartDate =  Date.newInstance(2017,8, 1);
        cmp2.EndDate = Date.newInstance(2017,8, 31);
        cmp2.Type = 'Edge Plus';
        cmp2.Sales_Area__c = 'DEL1';
        cmp2.Status = 'Completed';
        cmp2.OwnerId = userList[0].id;
        insert cmp2;

        Campaign cmp3 = new Campaign();
        cmp3.Name = 'Test Campaign';
        cmp3.IsActive = true;
        cmp3.StartDate =  Date.newInstance(2017,8, 1);
        cmp3.EndDate = Date.newInstance(2017,8, 31);
        cmp3.Type = 'Windowmakers\' Meet';
        cmp3.Sales_Area__c = 'DEL1';
        cmp3.Status = 'Completed';
        cmp3.OwnerId = userList[0].id;
        insert cmp3;
    }

     public static void createTestEventsRecordData(){
        List<Opportunity> opty = [SELECT id FROM Opportunity LIMIT 1];
        List<User> userList = [select id from User where UserRole.Name LIKE '%DEL1%' LIMIT 1];

        Event evt1 = new Event();
        evt1.StartDateTime = Date.newInstance(2017,8, 13);
        evt1.EndDateTime = Date.newInstance(2017,8, 17);
        evt1.OwnerId = userList[0].id;
        evt1.Type = 'Mr. Build- Joint Marketing';
        evt1.WhatId =  opty[0].Id;
        insert evt1;

        Event evt2 = new Event();
        evt2.StartDateTime = Date.newInstance(2017,8, 13);
        evt2.EndDateTime = Date.newInstance(2017,8, 17);
        evt2.OwnerId = userList[0].id;
        evt2.Type = 'Blinds that Blind';
        insert evt2;
     }

     public static void createTestTasksRecordData(){
        List<Opportunity> opty = [SELECT id FROM Opportunity LIMIT 1];
        List<User> userList = [select id from User where UserRole.Name LIKE '%DEL1%' LIMIT 1];

        Task tsk = new Task();
        tsk.ActivityDate = Date.newInstance(2017,8, 13);
        tsk.OwnerId = userList[0].id;
        tsk.Type = 'Call';
        tsk.Subject =  'Call';
        insert tsk;
    
    }

    public static void createTestEmailMessageData(){
        List<Opportunity> opty = [SELECT id FROM Opportunity LIMIT 1];

         EmailMessage email = new EmailMessage();
         email.FromAddress = 'test@abc.org';
         email.Incoming = True;
         email.ToAddress= 'test@xyz.org';
         email.Subject = 'Test email';
         email.HtmlBody = 'Test email body';
         email.MessageDate = system.today(); 
        insert email;
    }


}
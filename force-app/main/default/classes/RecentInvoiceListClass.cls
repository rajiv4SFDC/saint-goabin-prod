public with sharing class RecentInvoiceListClass {

    List<Invoice__c> categories {get;set;}
    public price_request_PandP__c  PRpandP{set;get;}
    public price_request_PandP__c pnpRec{set;get;}
    public boolean recordsFound{set;get;}
    
        public RecentInvoiceListClass(ApexPages.StandardController controller) {
        PRpandP = (price_request_PandP__c )controller.getRecord();
        
                pnpRec= [select id,name,Customer_Name__c,Customer__c,Product_Name__c,Product_lookup__c,Product_lookup__r.Name,Product_lookup__r.Product_Category__c from price_request_PandP__c  where id=:PRpandP.id];
                
                //recordsFound = false;

    }
    
  
    

    // instantiate the StandardSetController from a query locator
    public ApexPages.StandardSetController con {
        get {
            if(con == null) {
            system.debug('The ..........'+pnpRec.Customer__c +'.......'+pnpRec.Product_lookup__c);
                con = new ApexPages.StandardSetController(Database.getQueryLocator([select id,name,Invoice_Rate__c,Customer__r.Name,Product__r.Name,Product__c,Billing_Date__c,Product__r.Product_Category__c from invoice__c where customer__c=:pnpRec.Customer__c AND product__c=:pnpRec.Product_lookup__c AND Billing_Date__c >= LAST_N_MONTHS:1 AND Invoice_Rate__c!= null order by Billing_Date__c desc]));
                // sets the number of records in each page set
                con.setPageSize(5);
                
            }
            return con;
        }
        set;
    }

    // returns a list of wrapper objects for the sObjects in the current page set
    public List<Invoice__c> getCategories() {
        categories = new List<Invoice__c>();
        for (Invoice__c category : (List<Invoice__c>)con.getRecords())
        {
          system.debug('The vat is :'+category);
            categories.add(category);
            recordsFound = true;
        }
        return categories;
    }

    

    // indicates whether there are more records after the current page set.
    public Boolean hasNext {
        get {
            return con.getHasNext();
        }
        set;
    }

    // indicates whether there are more records before the current page set.
    public Boolean hasPrevious {
        get {
            return con.getHasPrevious();
        }
        set;
    }

    // returns the page number of the current page set
    public Integer pageNumber {
        get {
            return con.getPageNumber();
        }
        set;
    }

    // returns the first page of records
    public void first() {
        con.first();
    }

    // returns the last page of records
    public void last() {
        con.last();
    }

    // returns the previous page of records
    public void previous() {
        con.previous();
    }

    // returns the next page of records
    public void next() {
        con.next();
    }

    // returns the PageReference of the original page, if known, or the home page.
    public void cancel() {
        con.cancel();
        
         Account acc = new Account();
        acc.Name ='test3';
        acc.Type ='Industry';
        acc.Sales_Area_Region__c ='South';
        acc.PAN_Number__c = 'AAAPL1234C';
       acc.Sales_Office_ERP__c = 'SPR3';
       acc.sap_code__c='123123123';
        acc.RecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('ERP').getRecordTypeId();
    }

}
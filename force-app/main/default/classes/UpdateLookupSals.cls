/* Created By         : Rajiv Kumar Singh
   Created Date       : 14/06/2018
   Class Name         : UpdateLookupOfSalesorderLine
   Description        : update the particular NewSalesOrder record lookup field to sales order line
   Last Modified By   : Rajiv Kumar Singh
   Last Modified Date : 14/06/2018
*/
public class UpdateLookupSals{
  @InvocableMethod
  public static void UpdateLookupValss(List<Id> NewSals)
    {   
       map<string,string> newSalesOrderWithEnquiryLineMap = new map<string,string>();  
       set<string> presentSAPCodesSet = new set<String>();
       list<Sales_Order_Line_Item__c> NewList = new list<Sales_Order_Line_Item__c>(); 
       list<Sales_Order_Line_Item__c> FinalListToUpdate = new list<Sales_Order_Line_Item__c>();

       for(Sales_Order_Line_Item__c ptp:[select id,Enquiry_Line_Item__c,External_id__c from Sales_Order_Line_Item__c where id in:NewSals])
       {
         if(ptp.External_id__c != null)
          {
            presentSAPCodesSet.add(ptp.External_id__c);      
            NewList.add(ptp);
            system.debug('The ptp.id is: ' +ptp.id);
          }     
       }
       system.debug('The NewList is: ' +NewList+'......'+presentSAPCodesSet);
       if(presentSAPCodesSet.size()>0)
       {
          for(Sales_Order__c acc:[select id,External_Id__c from Sales_Order__c where External_Id__c in:presentSAPCodesSet])
           {
               newSalesOrderWithEnquiryLineMap.put(acc.External_Id__c,acc.id);
           }
           system.debug('The newSalesOrderWithEnquiryLineMap is: ' +newSalesOrderWithEnquiryLineMap);
           for(Sales_Order_Line_Item__c ptp:NewList)
           {
               ptp.Sales_Order__c = newSalesOrderWithEnquiryLineMap.get(ptp.External_id__c);
               system.debug('The  ptp.Enquiry_Line_Item__c is: ' + ptp.Enquiry_Line_Item__c);
               FinalListToUpdate.add(ptp);
           }
       }
       if(!FinalListToUpdate.isEmpty() && FinalListToUpdate.size() > 0) {
            update FinalListToUpdate;
       }
    }
}
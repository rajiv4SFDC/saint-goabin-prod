public class PRForPandPMassApproval {

    ApexPages.StandardSetController setCon;
  list<price_request_PandP__c> prList {set;get;}
  public string validateUser{set;get;}
  set<string> PrIdsSet = new set<string>();
    set<string> PrRegionSet = new set<string>();

   string roleName='';
public  map<string,string> PrCustomSettingsMap;
public  map<string,string> PrCustomSettingsRegionMap;


    public PRForPandPMassApproval(ApexPages.StandardSetController controller) {
        setCon = controller;
        prList = new list<price_request_PandP__c>();
    // prList = setCon.getRecords();
    prList = setCon.getSelected();
    
    validateUser ='';
     
     
     system.debug('The PrCustomSettingsMap is :' +PrCustomSettingsMap);
    }
    
   
   public void validateLoginUser()
   {
  // validateUser ='';
   //   user u1 =[select id,name,UserRole.Name from user where id=:userInfo.getUserId()];
    //   string roleName =u1.UserRole.Name;
       
       set<string> UserRoleNamesSet = new set<string>();
      //  UserRoleNamesSet.add('Regional Manager East');
     //   UserRoleNamesSet.add('Regional Manager North');
     //   UserRoleNamesSet.add('Regional Manager South');
     //   UserRoleNamesSet.add('Regional Manager West');
        
         user u1 =[select id,name,UserRole.Name,Region__c from user where id=:userInfo.getUserId()];
        roleName =u1.UserRole.Name;
       
       PrCustomSettingsMap = new map<string,string>();  
       PrCustomSettingsRegionMap = new map<string,string>(); 
     for(PricerequestPandPmassapproveregject__c pr:[select id,name,Role_Name__c,List_view_URL__c from PricerequestPandPmassapproveregject__c])
     {
     PrCustomSettingsMap.put(pr.Role_Name__c,pr.List_view_URL__c);
    // PrCustomSettingsRegionMap.put(pr.Role_Name__c,pr.Region_Name__c);
     UserRoleNamesSet.add(pr.Role_Name__c);
     }
        
          system.debug('The PrCustomSettingsMap 22222 is :' +PrCustomSettingsMap);
     if(PrCustomSettingsMap.KeySet().contains(roleName))            
        validateUser = '';
        else
        validateUser = 'Only Regional Head can Approve/Reject';

if(validateUser.length()>3)
{
//pageReference pr = new pageReference(PrCustomSettingsMap.get('Default'));
//return pr;
       ApexPages.addmessage(new ApexPages.message(ApexPages.severity.WARNING,'Only Regional Head can Approve/Reject'));

}


   }

    public integer getMySelectedSize() {
        return setCon.getSelected().size();
    }
    public integer getMyRecordsSize() {
        return setCon.getRecords().size();
    }
    
    public pageReference approveRecs()
    {
    boolean successStatus = false;
    pageReference pRef;
    try{
       for(price_request_PandP__c pr:prList)
       {
        PrIdsSet.add(pr.id);        
       }
       system.debug('The PrIdsSet is :'+PrIdsSet+PrIdsSet.size());
       
       Set<Id> pIds = (new Map<Id, ProcessInstance>([SELECT Id,Status,TargetObjectId FROM ProcessInstance where Status='Pending' and TargetObjectId in :PrIdsSet])).keySet();

Set<Id> pInstanceWorkitems = (new Map<Id, ProcessInstanceWorkitem>([SELECT Id,ProcessInstanceId FROM ProcessInstanceWorkitem WHERE ProcessInstanceId in :pIds])).keySet();
List<Approval.ProcessWorkitemRequest> requests = new List<Approval.ProcessWorkitemRequest>(); 
Approval.ProcessResult[] allReq = New Approval.ProcessResult[]{}; 
for (Id pInstanceWorkitemsId:pInstanceWorkitems){
    system.debug(pInstanceWorkitemsId);
        Approval.ProcessWorkitemRequest req2 = new Approval.ProcessWorkitemRequest();
        req2.setComments('Approved');
        req2.setAction('Approve'); //to approve use 'Approve'
        req2.setNextApproverIds(new Id[] {UserInfo.getUserId()});

        // Use the ID from the newly created item to specify the item to be worked
        req2.setWorkitemId(pInstanceWorkitemsId);

        // Add the request for approval
        
        requests.add(req2);         
}
Approval.ProcessResult[] result2 =  Approval.process(requests);
successStatus = true;
}
catch(exception ert)
{
 ApexPages.addmessage(new ApexPages.message(ApexPages.severity.WARNING,'Error while aproving records '+String.valueof(ert)));
}
if(successStatus == true && PrCustomSettingsMap!=null && PrCustomSettingsMap.containsKey(roleName))
pRef = new pageReference(PrCustomSettingsMap.get(roleName));

return pRef;
    }
    
    public pageReference rejectRecs()
    {
     pageReference pRef;
    for(price_request_PandP__c pr:prList)
       {
        PrIdsSet.add(pr.id);
       }
       system.debug('The PrIdsSet is :'+PrIdsSet+PrIdsSet.size());
       
       Set<Id> pIds = (new Map<Id, ProcessInstance>([SELECT Id,Status,TargetObjectId FROM ProcessInstance where Status='Pending' and TargetObjectId in :PrIdsSet])).keySet();

Set<Id> pInstanceWorkitems = (new Map<Id, ProcessInstanceWorkitem>([SELECT Id,ProcessInstanceId FROM ProcessInstanceWorkitem WHERE ProcessInstanceId in :pIds])).keySet();
List<Approval.ProcessWorkitemRequest> requests = new List<Approval.ProcessWorkitemRequest>(); 
Approval.ProcessResult[] allReq = New Approval.ProcessResult[]{}; 
for (Id pInstanceWorkitemsId:pInstanceWorkitems){
    system.debug(pInstanceWorkitemsId);
        Approval.ProcessWorkitemRequest req2 = new Approval.ProcessWorkitemRequest();
        req2.setComments('Rejected');
        req2.setAction('Reject'); //to approve use 'Approve'
        req2.setNextApproverIds(new Id[] {UserInfo.getUserId()});

        // Use the ID from the newly created item to specify the item to be worked
        req2.setWorkitemId(pInstanceWorkitemsId);

        // Add the request for approval
        
        requests.add(req2);                 }    
    Approval.ProcessResult[] result2 =  Approval.process(requests);
if(PrCustomSettingsMap!=null && PrCustomSettingsMap.containsKey(roleName))
 pRef= new pageReference(PrCustomSettingsMap.get(roleName));
return pRef;
    }
    
    public pagereference cancelPage()
    {
     pageReference pRef;
    PrCustomSettingsMap = new map<string,string>();  
     for(PricerequestPandPmassapproveregject__c pr:[select id,name,Role_Name__c,List_view_URL__c from PricerequestPandPmassapproveregject__c])
     {
     PrCustomSettingsMap.put(pr.Role_Name__c,pr.List_view_URL__c);
     }
    system.debug('The PrCustomSettingsMap is :' +PrCustomSettingsMap);
    if(PrCustomSettingsMap.containsKey(roleName))
    pRef = new pageReference(PrCustomSettingsMap.get(roleName));
    else
    pRef = new pageReference(PrCustomSettingsMap.get('Default'));
     return pRef;
    }
    
    
}
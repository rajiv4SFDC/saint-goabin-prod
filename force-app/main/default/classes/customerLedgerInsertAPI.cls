/* Created By: Rajiv Kumar Singh
Created Date: 06/06/2018
Class Name: customerLedgerInsertAPI 
Story : Customer Ledger flow from SAP to SFDC
Last Modified By :Rajiv Kumar Singh
Last Modified Date: 13/06/2018
*/

@RestResource(urlMapping='/customerLedger')
global class customerLedgerInsertAPI {
    @HttpPost
    global static void doHandleInsertRequest(){
        
        List<LedgerWrapper> wrapList = new List<LedgerWrapper>();
        List<Customer_Ledger__c> LedgerList = new List<Customer_Ledger__c>();
        RestRequest Req = RestContext.request;
        RestResponse res = RestContext.response;
        String body = Req.requestBody.toString();
        if(body != null && body != '') {
            
            try {
                wrapList = (List<LedgerWrapper>)JSON.deserialize(body, List<LedgerWrapper>.class);
                
                for(LedgerWrapper we :wrapList){
                    
                    Customer_Ledger__c cusLed = new Customer_Ledger__c();
                    if(we.SAPcode!= null && we.SAPcode!=''){
                        cusLed.SAP_Code__c = we.SAPcode;
                    }
                    
                    if(we.PostingDate!= null ){
                        cusLed.Posting_Date__c = we.PostingDate;
                    }
                    
                    if(we.CreatedDate!= null ){
                        cusLed.Created_Date__c = we.CreatedDate;
                    }
                    if(we.Amount!= null){
                        cusLed.Payment__c = we.Amount;
                    }
                    if(we.tranMode!= null && we.tranMode!=''){
                        cusLed.Mode_of_Transaction__c = we.tranMode;
                    }
                    if(we.Type!= null && we.Type!=''){
                        cusLed.Type_of_Transaction__c = we.Type;
                    }
                    if(we.DocNo!= null && we.DocNo!=''){
                        cusLed.Document_No__c = we.DocNo;
                    }
                    if(we.DocText!= null && we.DocText!=''){
                        cusLed.Doc_Text__c = we.DocText;
                    }
                    LedgerList.add(cusLed);
                    
                }
                
                if(LedgerList != null && LedgerList.size() > 0){
                    Insert LedgerList;
                }
                res.statusCode = 200;
                res.responseBody = Blob.valueOf(String.valueof(LedgerList));
                
                
            } catch (Exception e) {
                
                system.debug('error'+e.getMessage());
                CommonUtilitiesHandler.handleExceptions(e);
                res.statusCode = 500;
                res.responseBody = Blob.valueOf(e.getMessage());
            }
        } else {
            res.statusCode = 500;
            res.responseBody = Blob.valueOf('Invalid Request');
        }   
    }
    
    public class LedgerWrapper{
        public String SAPcode{get;set;}
        public date PostingDate{get;set;}
        public date CreatedDate{get;set;}
        public Decimal Amount{get;set;}
        public String tranMode{get;set;}
        public String Type{get;set;}
        public String DocNo{get;set;}
        public String DocText{get;set;}
        
        
        
    }
}
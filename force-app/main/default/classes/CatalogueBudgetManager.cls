public class CatalogueBudgetManager {

	public CatalogueBudgetManager(){}

	public void checkForCatalogueRequestForPopulateBudget( List<Request_Item__c> lstOfRequestItem){
		Map<String,List<Request_Item__c>> mapOfCatalogueRequestItem = new Map<String,List<Request_Item__c>>();

		for(Request_Item__c ri : lstOfRequestItem){
			if( ri.IsCatalogue__c  ){

				if( mapOfCatalogueRequestItem.get(ri.Request_Item_Identifier__c) == null ){
					mapOfCatalogueRequestItem.put(ri.Request_Item_Identifier__c,new List<Request_Item__c>{ri});
				}
				else{
					List<Request_Item__c> lstRI = mapOfCatalogueRequestItem.get(ri.Request_Item_Identifier__c);
					lstRI.add(ri);
					mapOfCatalogueRequestItem.put(ri.Request_Item_Identifier__c,lstRI);
				}
			}
		}

		if(mapOfCatalogueRequestItem.size() > 0){
			populateCatalogueBudget( mapOfCatalogueRequestItem );
		}

	}

	public void populateCatalogueBudget(Map<String,List<Request_Item__c>> mapOfCatalogueRequestItem){
		
		Set<String> setOfAllReqItemUnqIdenty = new Set<String>();
		Set<String> setOfProcessedReqItemUnqIdenty = new Set<String>();
		List<Catalogue_Budget__c> lstOfCataBudgetInsert = new List<Catalogue_Budget__c>();

		List<Catalogue_Budget__c> lstOfCatalogueBudget = [SELECT id,Unique_Budget_Identifier__c,
														  Units_Consumed__c,Budget_Allocated__c
														  FROM Catalogue_Budget__c 
														  WHERE Unique_Budget_Identifier__c IN : mapOfCatalogueRequestItem.keySet()];



		for(Catalogue_Budget__c cb : lstOfCatalogueBudget){

			List<Request_Item__c> lstOfRI =  mapOfCatalogueRequestItem.get(cb.Unique_Budget_Identifier__c);

			for( Request_Item__c ri: lstOfRI){
				ri.Catalogue_Budget__c = cb.id ;
				setOfProcessedReqItemUnqIdenty.add(cb.Unique_Budget_Identifier__c);
			}
		}

		/* Taking out all unprocessed Request Item where Budget is missing */
		setOfAllReqItemUnqIdenty  = mapOfCatalogueRequestItem.keySet();
		setOfAllReqItemUnqIdenty.removeAll( setOfProcessedReqItemUnqIdenty );

	    /* this method created Budget for the Requested Item where Budget is missing  */
	    for(String unqIdent : setOfAllReqItemUnqIdenty){

	    	String[] lstOfdetails = unqIdent.split(',');
	    	Catalogue_Budget__c cb = new Catalogue_Budget__c();
	    	cb.Budget_Allocated__c = 0;
	    	cb.Budget_Period__c = date.newInstance( Integer.valueof(lstOfdetails[2]) , 1, 1) ;//System.today();
	    	cb.Catalogue__c = lstOfdetails[1];
	    	cb.OwnerId = lstOfdetails[0];
	    	cb.Unique_Budget_Identifier__c = unqIdent;
	    	lstOfCataBudgetInsert.add(cb);
	    }

	    if( lstOfCataBudgetInsert.size() > 0){
	    
	    	insert lstOfCataBudgetInsert;
	    }
	  

	    /* Allocat Budget to Requested Item */
	    for(Catalogue_Budget__c cb : lstOfCataBudgetInsert){

			List<Request_Item__c> lstOfRI =  mapOfCatalogueRequestItem.get(cb.Unique_Budget_Identifier__c);

			for( Request_Item__c ri: lstOfRI){
				ri.Catalogue_Budget__c = cb.id ;
			}
		}
	}

	public void checkForApprovedCatalogueRequest( List<Merchandise_Request__c> lstOfMerchandiseRequest, Map<Id,Merchandise_Request__c> merchandiseRequestOldMap ){

		Set<Id> setOfMerchandiseRequest = new Set<Id>();
		for(Merchandise_Request__c merReq : lstOfMerchandiseRequest){
			if( merReq.Status__c == 'Approved' 
				&& merReq.Status__c != merchandiseRequestOldMap.get(merReq.Id).Status__c ){

				setOfMerchandiseRequest.add(merReq.id);
			}
		}

		if( setOfMerchandiseRequest.size() >0 ){
			List<Request_Item__c> lstOfRequestItem = [SELECT id,Request_No__c,IsCatalogue__c,
														IsApproved__c, Request_Item_Identifier__c
													FROM Request_Item__c 
													WHERE Request_No__c IN : setOfMerchandiseRequest];

			checkForCatalogueRequestForCalcBudgetComsume(lstOfRequestItem);										
		}
	}

	public void checkForCatalogueRequestForCalcBudgetComsume( List<Request_Item__c> lstOfRequestItem){
		Set<String> setOfBudgetidetifier = new Set<String>();
		
		for(Request_Item__c ri : lstOfRequestItem){
			if( ri.IsCatalogue__c && ri.IsApproved__c ){

				setOfBudgetidetifier.add(ri.Request_Item_Identifier__c);
			}
		}

		if(setOfBudgetidetifier.size() > 0){
			updateCatalogueBudgetConsumption( setOfBudgetidetifier );
		}

	}

	public void updateCatalogueBudgetConsumption(Set<String> setOfBudgetidetifier){
		
		List<Request_Item__c> lstOfReqItem = [SELECT id,Catalogue_Budget__c,Quantity_Requested__c,
											 IsCatalogue__c,Request_Item_Identifier__c 
											 FROM Request_Item__c 
											 WHERE IsCatalogue__c = true 
											 AND Request_Item_Identifier__c IN : setOfBudgetidetifier];	

		Map<Id,Catalogue_Budget__c> mapOfBudgetComsumption = new Map<Id,Catalogue_Budget__c>();

		for(Request_Item__c ri :lstOfReqItem ){
			
			if( mapOfBudgetComsumption.get(ri.Catalogue_Budget__c) == null ){
				mapOfBudgetComsumption.put(ri.Catalogue_Budget__c, new Catalogue_Budget__c(id = ri.Catalogue_Budget__c,Units_Consumed__c = ri.Quantity_Requested__c ));
			}else{

				Catalogue_Budget__c catalogueBudgetRecord = mapOfBudgetComsumption.get(ri.Catalogue_Budget__c);
				catalogueBudgetRecord.id = ri.Catalogue_Budget__c;
				catalogueBudgetRecord.Units_Consumed__c = catalogueBudgetRecord.Units_Consumed__c + ri.Quantity_Requested__c;
				mapOfBudgetComsumption.put(ri.Catalogue_Budget__c,catalogueBudgetRecord);
			}
			
		}

		if(mapOfBudgetComsumption.size() > 0){
			update mapOfBudgetComsumption.values();
		}		  

	}

	public void checkForCatalogueRequestForBlankShipping( List<Merchandise_Request__c> lstOfMerchandiseRequest ){
		Id catalogueRecordTypeId = Schema.SObjectType.Merchandise_Request__c.getRecordTypeInfosByName().get('Catalogue').getRecordTypeId();
		Map<Id,List<Merchandise_Request__c>> mapOfCatalogueMerchandReq = new Map<Id,List<Merchandise_Request__c>>();
		Integer thisYear = System.today().year();
		for( Merchandise_Request__c mr : lstOfMerchandiseRequest){

			if( catalogueRecordTypeId == mr.RecordTypeId ){
				if( mapOfCatalogueMerchandReq.get(mr.OwnerId) == null ){
					mapOfCatalogueMerchandReq.put(mr.OwnerId, new List<Merchandise_Request__c>{mr});
				}else{
					List<Merchandise_Request__c> mrlist = mapOfCatalogueMerchandReq.get(mr.OwnerId);
					mrlist.add(mr);
					mapOfCatalogueMerchandReq.put(mr.OwnerId, mrlist);
				}
			}
		}

		List<Merchandise_Request__c> lstOfBlankShipMerchandReq = [SELECT id,Status__c,Shipment_Status__c,Requesting_Date__c,
																  OwnerId
																  FROM Merchandise_Request__c 
																  WHERE Status__c = 'Approved' AND Shipment_Status__c ='' 
																  AND CALENDAR_YEAR(Requesting_Date__c) =: thisYear 
																  AND OwnerId IN : mapOfCatalogueMerchandReq.keySet()];
   
 		for(Merchandise_Request__c mr : lstOfBlankShipMerchandReq){

 			List<Merchandise_Request__c> lstOfmrRecord = mapOfCatalogueMerchandReq.get(mr.OwnerId);

 			for(Merchandise_Request__c mrTrigger: lstOfmrRecord){
 				if( mr.Requesting_Date__c.daysBetween( mrTrigger.Requesting_Date__c ) > 7 ){
 					mrTrigger.addError('First please update the Shipment Status for all previously approved requests');
 				}
 			}	
 			
 		}

	}

}
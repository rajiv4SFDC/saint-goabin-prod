({
	showGPSPermissionIssueToast : function(component, event, helper) {
		var errorMsg = component.get("v.geolocationObj").error;
        var toastEvent = $A.get("e.force:showToast");
        toastEvent.setParams({
        	type: 'error',
            mode: 'sticky',
            message: 'GPS Error',
            messageTemplate: errorMsg ,
            messageTemplateData: ['Salesforce', {
                url: 'http://www.salesforce.com/',
                label: 'here',
                }
            ]
        });
        toastEvent.fire();
    },

    fetchGeoLocation : function(component, event, helper){
           if (navigator.geolocation) {
            navigator.geolocation.getCurrentPosition(function(position){
                
                var geoObj = {};
                geoObj.latitude = position.coords.latitude; //28.5614925;
                geoObj.longitude = position.coords.longitude; //77.2199363;
         
                /*---- Stamp Geoloaction details on Event attribute ----*/
                 var compEvent = component.getEvent("geoTagEvent");
                if( compEvent != null ){
                    
                    compEvent.setParams({"geolocationObj" : geoObj });
                    compEvent.fire();
                    component.set("v.geolocationObj",geoObj);
                    console.log("geo tagged from getGeoLocation Cmp",geoObj);
                }else{
                    console.log('ellllllllllllllllll wawawawwwwwwwwwwwwwwwwwwwwwwwwwww :: ',compEvent);
                }
              

            },function(error){
                var errorReason="";
               	switch(error.code) {
                    case error.PERMISSION_DENIED:
                        console.log("User denied the request for Geolocation.");
                        errorReason = "User denied the request for Geolocation";
                        break;
                    case error.POSITION_UNAVAILABLE:
                         console.log("Location information is unavailable.");
                        errorReason = "Location information is unavailable";
                        break;
                    case error.TIMEOUT:
                        console.log("The request to get user location timed out.");
                        errorReason = "The request to get user location timed out";
                        break;
                    case error.UNKNOWN_ERROR:
                        console.log("An unknown error occurred.");
                        errorReason = "An unknown error occurred";
                        break;
                }
                
                var geoObj = {};
               	geoObj.latitude = 20.5937; // India coordinates
                geoObj.longitude = 78.9629; // India coordinates 
                geoObj.error = errorReason; // India coordinates 
                component.set("v.geolocationObj",geoObj);
                /*---- Stamp Geoloaction details on Event attribute ----*/
                /*
                var compEvent = component.getEvent("geoTagEvent");
                compEvent.setParams({"geolocationObj" : geoObj });
				compEvent.fire();
                */

                /* Call to Display Error Message  */
                helper.showGPSPermissionIssueToast(component, event, helper);
                
            });
            
        } else {
             	console.log("Geolocation is not supported by this browser.");
             	var geoObj = {};
                geoObj.latitude = 20.5937;
                geoObj.longitude = 78.9629;
              	geoObj.error = "Geolocation is not supported by this browser";
            	component.set("v.geolocationObj",geoObj);

            	 /* Call to Display Error Message  */
                helper.showGPSPermissionIssueToast(component, event, helper);

            /*---- Stamp Geoloaction details on Event attribute ----*/
                /*var compEvent = component.getEvent("geoTagEvent");
                compEvent.setParams({"geolocationObj" : geoObj });
				compEvent.fire();*/
        }
    },

})
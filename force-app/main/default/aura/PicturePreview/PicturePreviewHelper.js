({
	queryRelatedImageFiles : function(component, event, helper){
		/* Inital Action on Preview Screen */
		//helper.initialActionPreviewScreen(component, event, helper);

		var rcdId = component.get("v.recordId");
		var action = component.get("c.getRelatedContentVersionIds");
		action.setParams({
            opptyId : rcdId
         });

		action.setCallback(this,function(response){

			if( response.getState() == "SUCCESS"){
				var responseData;
				 try {
				        responseData = JSON.parse(response.getReturnValue()) ;
				 } catch (e) {
				       responseData = response.getReturnValue() ;
		         }
	
				if( responseData instanceof Object ){
					
					var objectAry = [];
					var docIdArry = [];
					for(var i=0;i<responseData.length;i++){

						var tmp = {};				
						tmp["url"] = "/sfc/servlet.shepherd/version/download/" + responseData[i].varContentVersionId;
						if(i==0){
							tmp["display"] = "block";
						}else{
							tmp["display"] = "none";
						}
						
						docIdArry[i] = responseData[i].varContentDocumentId ;

						objectAry.push( tmp );
					}
					component.set("v.slides",objectAry);
					component.set("v.allContentDocumentIds",docIdArry);

					var refBtn = component.find("refreshBtn");
					helper.removeDisableButton(refBtn);
					
					var trgSpinner = component.find("spinner");
					helper.toggleHide(trgSpinner);

					var trgFullBtn = component.find("fullScreenBtn");
					helper.removeDisableButton(trgFullBtn);

					var trgEle = component.find("noImgMsgDiv");
					$A.util.addClass(trgEle, 'slds-hide');

				}else{
					var trgEle = component.find("noImgMsgDiv");
					$A.util.removeClass(trgEle, 'slds-hide');
					
					var trgSpinner = component.find("spinner");
					helper.toggleHide(trgSpinner);

					var refBtn = component.find("refreshBtn");
					helper.removeDisableButton(refBtn);

					component.set("v.slides",null);
					//console.log("Ghost Imagessss : ",component.get("v.slides"));
				}	
			}

		});

		$A.enqueueAction(action);


	},

	showSlides : function(component, event, helper) {
		  var i;
		  var slides = document.getElementsByClassName("mySlides");

		  var slideIndex = component.get("v.slideIndex");
		  var currentIndex =slideIndex;

		  if (slideIndex > slides.length) {
		  	currentIndex = 1;
		  	component.set("v.slideIndex",currentIndex);
		  }    

		  if (slideIndex < 1) {
		  	currentIndex = slides.length ;
		  	component.set("v.slideIndex",currentIndex);
		  }

		  for (i = 0; i < slides.length; i++) {   
		      slides[i].style.display = "none";  
		  }
		  
		  slides[currentIndex-1].style.display = "block";  
	},

	disableButton : function(target){
		target.set("v.disabled",true);
	},

	removeDisableButton : function(target){
		target.set("v.disabled",false);
	},

	toggleHide : function(target){
		$A.util.toggleClass(target, "slds-hide");
	},

	toggleShow : function(target){
		$A.util.toggleClass(target, "slds-show");
	},

	initialActionPreviewScreen :  function(component, event, helper){

		var refBtn = component.find("refreshBtn");
		helper.disableButton(refBtn);
		
		var trgSpinner = component.find("spinner");
		helper.toggleHide(trgSpinner);

		var trgFullBtn = component.find("fullScreenBtn");
		helper.disableButton(trgFullBtn);
	}
	
})
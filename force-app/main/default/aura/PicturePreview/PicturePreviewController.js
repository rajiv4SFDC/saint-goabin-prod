({	
	doInit : function(component, event, helper){
		
		helper.initialActionPreviewScreen(component, event, helper);

		helper.queryRelatedImageFiles(component, event, helper);
	},

	openSingleFile: function(component, event, helper) {
        $A.get('e.lightning:openFiles').fire({
            recordIds: [component.get("v.currentContentDocumentId")]
        });
    },

    openMultipleFiles: function(component, event, helper) {
    	var currentDocid  = component.get("v.allContentDocumentIds")[component.get("v.slideIndex")-1];
    	
	    $A.get('e.lightning:openFiles').fire({
	        recordIds: component.get("v.allContentDocumentIds"),
	        selectedRecordId: currentDocid
	    });
	},

	previousSlides : function(component, event, helper){
		var currDocIdLst = component.get("v.allContentDocumentIds");
		if(currDocIdLst.length > 1){
			var slideIndex = component.get("v.slideIndex");
			component.set("v.slideIndex",slideIndex -1);

			helper.showSlides(component, event, helper);
		}
	},

	nextSlides : function(component, event, helper){
		var currDocIdLst = component.get("v.allContentDocumentIds");
		if(currDocIdLst.length > 1){
			var slideIndex = component.get("v.slideIndex");
			component.set("v.slideIndex",slideIndex +1);
			helper.showSlides(component, event, helper);
		}
		

		//var slideWidth = component.find("galleryCard");
		//var slideheight = component.find("galleryCard").getElement().offsetHeight;
		
		//console.log("slide wifhhht : ", slideWidth);
		//console.log("slide slideheight : ", slideheight);

	},

	refreshPicturePreview : function(component, event, helper){
		
		helper.initialActionPreviewScreen(component, event, helper);

		/* Reset attribute value */
		component.set("v.slideIndex",1);
		/* Call image query on Server */
		helper.queryRelatedImageFiles(component, event, helper);
	},

	projRecordUpdated: function(component, event, helper) {

    var changeType = event.getParams().changeType;

    if (changeType === "ERROR") { /* handle error; do this first! */ }
    else if (changeType === "LOADED") { /* handle error; do this loaded ! */}
    else if (changeType === "REMOVED") { /* handle record removal */ }
    else if (changeType === "CHANGED") { /* handle record change */ 
    	
    	helper.initialActionPreviewScreen(component, event, helper);
		/* Reset attribute value */
		component.set("v.slideIndex",1);
		/* Call image query on Server */
		helper.queryRelatedImageFiles(component, event, helper);
    }
    
    },


})
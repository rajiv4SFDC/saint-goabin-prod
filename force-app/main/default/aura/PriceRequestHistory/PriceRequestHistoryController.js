({
    doLoadData : function(component, event, helper) {
        
        // Clearing the existing records information. 
        component.set("v.priceList","");
        var accValue = component.get("v.selectedLookUpRecord").Id;
        console.log('acc value=',accValue);
        var prodValue = component.get("v.selectedProdRecord").Id;
        console.log('prod value=',prodValue);
        if(!accValue || !prodValue){
            alert('Please select both Account and Product');
        }else{
            
            helper.showSpinner(component, event, helper);
            var comp = component.get("c.doFetchPriceRequest");
            comp.setParams({
                "accId":accValue,
                "proId":prodValue
            });
            comp.setCallback(this,function(response){
                
                if(response.getReturnValue())
                {
                    component.set("v.priceList",response.getReturnValue());
                    console.log('The lenght is:'+component.get("v.priceList").length);
                    if(component.get("v.priceList").length <=0)
                    {
                        component.set("v.showTable","false");
                        component.set("v.errorMsg","No records found with this criteria");
                    }
                    else{
                        component.set("v.showTable","true");
                        component.set("v.errorMsg", "");
                    }
                    
                    helper.hideSpinner(component, event, helper);
                }                
            });
        }
        $A.enqueueAction(comp);
    },
    resettable :function(component,event,handler)
    {
        component.set("v.priceList","");
        component.set("v.showTable","false");
        component.set("v.errorMsg", "");
    }
})
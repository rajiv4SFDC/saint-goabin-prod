({
    loadconst : function(component, event, helper) {
        /*******
              var emptyPrd = component.get("c.getEmptyPrd");
          emptyPrd.setCallback(this,function(a){
            component.set("v.singleprd",a.getReturnValue());
            alert('hello'+JSON.stringify(a.getReturnValue()));
            console.log('The coming prd wrap is: ' +JSON.stringify(a.getReturnValue()));
            
        })************/
        
        var recordIdval = component.get("v.opportunityid");
        console.log('The recordIdval is'+recordIdval);
        if(recordIdval) {
            
            helper.showSpinner(component, event, helper);
            var oppname = component.get("c.getOpportunityDetail");
            oppname.setParams({"oppId":recordIdval});
            oppname.setCallback(this,function(res) {
                
                var opportunityDetail = res.getReturnValue();
                component.set("v.opportunityName", opportunityDetail['OPPNAME']);
                component.set("v.accountName", opportunityDetail['ACCOUNTNAME']);
                helper.hideSpinner(component, event, helper);
            })
            $A.enqueueAction(oppname);
        }  
        
        //get the opportunity product details
        var opportunityProductId = component.get("v.opportunityProductId");
        if(opportunityProductId) {
            
            component.set("v.editRecordBoolean", true);
            component.set("v.editRecord", 1);
            helper.showSpinner(component, event, helper);
            var oppname = component.get("c.getOpportunityProductDetail");
            oppname.setParams({"opportunityProductId": opportunityProductId});
            oppname.setCallback(this, function(res) {
                
                var state = res.getState(); //Checking response status
                if (component.isValid() && state === "SUCCESS") {
                    
                    var response = res.getReturnValue();
                    var oppProdDetail = response['LINEITEMS'];
                    component.set("v.custName", response['CUSTOMERNAME']);
                    component.set("v.otherProdName", response['OTHERPRODNAME']);
                    component.set("v.masterProdName", response['MASTERPRODNAME']);
                    
                    //todo
                    component.set("v.StatusCompletedShowSectionCreation",false);     
                	component.set("v.StatusNotCompletedShowSectionCreation",true);
                    
                    component.set("v.prdListSize", oppProdDetail.length - 1);
                    component.set("v.totalRows", oppProdDetail.length - 1);
                                        
                    component.set("v.status", response['OPPPRODUCT'].Product_Type__c);
                    component.set("v.lstprds", oppProdDetail);
                    component.set("v.oppProduct", response['OPPPRODUCT']);
                    component.set("v.selctedProductRec", response['OPPPRODUCT'].Product__c);
                    component.set("v.selectedLookUpRecord", response['OPPPRODUCT'].Customer_Name__c);
                    component.set("v.selctedAddlProductRec", response['OPPPRODUCT'].Additional_Product__c);   
                }
                helper.hideSpinner(component, event, helper);
                component.set("v.editRecord", 0);
            })
            $A.enqueueAction(oppname);
        }
    },
    // Below method is used for displaying the defaulult values for the selected product.
    statuscheck : function(component,event,helper) {
        
        var selectedStatus = component.get("v.status");  
        var selectedrecId =  component.get("v.selctedProductRec.Id");
        var opportunityProductId = component.get("v.opportunityProductId");
        
        if(!selectedrecId && opportunityProductId) {
            
            selectedrecId =  component.get("v.selctedProductRec");
        }
        console.log('The coming status is:'+selectedStatus+'........'+selectedrecId);
        //alert('selectedrecId:'+selectedrecId+'##selectedStatus:'+selectedStatus);
        var editRecord = component.get("v.editRecord");
        
        if(editRecord == 0) { 
            if(selectedrecId) {
                
                // if status is selected. Then display static data from products and display to user. 
                if(selectedStatus == "Standard" && selectedrecId!=undefined ) {
                    
                    helper.showSpinner(component, event, helper);
                    var act = component.get("c.getNewProducts");
                    act.setParams({"MasterProduct":selectedrecId});
                    act.setCallback(this,function(a){
                        component.set("v.lstprds",a.getReturnValue());
                    });       
                    $A.enqueueAction(act);
                    component.set("v.StatusCompletedShowSectionCreation",true);     
                    component.set("v.StatusNotCompletedShowSectionCreation",false);   
                    
                    helper.hideSpinner(component, event, helper);
                }        
                else if(selectedStatus == "Not Standard" && selectedrecId!=undefined) // If the status ="not selected" So we need to display in edit mode. 
                {
                    helper.showSpinner(component, event, helper);
                    var act = component.get("c.getNewProducts");
                    act.setParams({"MasterProduct":selectedrecId});
                    act.setCallback(this,function(a){
                        component.set("v.lstprds",a.getReturnValue());
                        // For controlling the display of "ADD ROW" button. 
                        var prdList = component.get("v.lstprds");
                        var prdListSize = prdList.length-1;
                        component.set("v.prdListSize",prdListSize);
                        component.set("v.totalRows", prdListSize);
                        console.log('The prdListSize is:'+prdListSize);
                        
                        helper.hideSpinner(component, event, helper);
                    });       
                    $A.enqueueAction(act);
                    
                    component.set("v.StatusCompletedShowSectionCreation",false);
                    component.set("v.StatusNotCompletedShowSectionCreation",true);     
                }
            }
            else if(selectedStatus == 'Select Product Type' || selectedrecId!=undefined)
            {
                 component.set("v.lstprds", null); 
            }
                else {
                
                component.set("v.lstprds", null);  
            }
        }
        //else if(selectedStatus == "choose one..." && selectedrecId!=undefined)
        //  component.set("v.errorMsg","Please choose any of the picklist value");
    },    
    saveRecs : function(component,event,helper) {
        
        var selectedrecId2 =  component.get("v.selctedProductRec.Id");
        var opportunityProductId = component.get("v.opportunityProductId");
        if(!selectedrecId2 && opportunityProductId) {
            
            selectedrecId2 =  component.get("v.selctedProductRec");
        }
        
        var showError = 'no error';
        var prdListValidate = component.get("v.lstprds");
        for(var i=0;i<prdListValidate.length;i++)
        {
		 
            if(isNaN(prdListValidate[i].prdQuantitiy))
			
            showError ='Please enter only number in fittings quantity field';
        }
        
        if(showError =='no error')
        {
        var prdlist2 = JSON.stringify(component.get("v.lstprds"));
        console.log("prdlist2"+prdlist2);
        var masterPrdId = component.get("v.selctedProductRec.Id");
        if(!masterPrdId && opportunityProductId) {
            
            masterPrdId =  component.get("v.selctedProductRec");
        }
        var glassPrdId = component.get("v.selctedAddlProductRec");
        if(glassPrdId) {
            
            glassPrdId = glassPrdId.Id;
        }
        if(!glassPrdId && opportunityProductId) {
            
            glassPrdId =  component.get("v.selctedAddlProductRec");
        }
        var saverecords = component.get("c.upsertOppProductAndLineItems");
        var oppProduct = component.get("v.oppProduct");
        var pdType = component.get("v.status");
        var customerName = component.get("v.selectedLookUpRecord");
        var customerName1 = customerName;
        console.log('The oppProduct is '+JSON.stringify(oppProduct));
        if(customerName && customerName.Id) {
            
            customerName = customerName.Id;
        }
        
        if(!customerName && opportunityProductId) {
            
            customerName =  component.get("v.selectedLookUpRecord");
        }
       
        if(!customerName1.Id) {
            
            alert('Please enter "Customer Name"');
            return false;
        }
        console.log('The pdType is :'+pdType);
        if(!pdType || pdType == 'Select Product Type')
        {
           component.set("v.lstprds", null);
              alert('Please select Product Type');
        }   
          else
              if(isNaN(oppProduct.Expected_Price__c))
              {
                  alert('Please enter only number for Expected Price');
              }
        else 
            if(isNaN(oppProduct.Additional_Product_Qty__c))
            {  
               alert('Please enter only number for Glass Product Qty(sq. m)');
            }
        else
        {
        var opportunityid = component.get("v.opportunityid");
        console.log('The vals are:'+masterPrdId+'...'+glassPrdId+'...'+oppProduct+'...'+pdType+'...'+customerName.Id+'...'+opportunityid);
        saverecords.setParams({
            					"prdlist":prdlist2,
                                "parentPrdId": selectedrecId2,
                                "opportunityid": opportunityid,
                                "masterPrd": masterPrdId,
                                "glassPrd": glassPrdId,
                                "oppProduct": oppProduct,
                                "pdType":pdType,
                                "customername": customerName
        });
        saverecords.setCallback(this,function(a){
            helper.showSpinner(component, event, helper);
            //('The val 222 is: '+a.getReturnValue());
            //  component.set("v.lstprds",a.getReturnValue());
            var recordIdval = component.get("v.opportunityid");
            console.log('The recordIdval is:'+recordIdval);
            
            helper.hideSpinner(component, event, helper);
            
            //redirect
            helper.gotoURL(component, event, helper, recordIdval);
        });   
         
        $A.enqueueAction(saverecords);
        }
        }
         else
         {
             alert(showError);
         }
    },
    showSelectedRec : function(component,event,helper) {
        
        var opportunityProductId = component.get("v.opportunityProductId");
        var selctedSubPrdRecId = component.get("v.selctedSubProductRec");
        var selectedParentRecId = component.get("v.selctedProductRec.Id");   
        if(!selectedParentRecId && opportunityProductId) {
            
            selectedParentRecId =  component.get("v.selctedProductRec");
        }
        selctedSubPrdRecId = selctedSubPrdRecId.Id;
        //console.log('inside of showSelectedRec:' + selctedSubPrdRecId+'...........'+selectedParentRecId);
        var prdList = component.get("v.lstprds");
        var currentPos = component.get("v.presentClickIndex");
        //  component.set("v.lstprds",prdList);
        var fetchRec = component.get("c.getSpecificProducts");
        fetchRec.setParams({"selectedProdId":selctedSubPrdRecId, "parentPrdId":selectedParentRecId});
        fetchRec.setCallback(this,function(a){
            
            helper.showSpinner(component, event, helper);
            console.log('The showSelectedRec:'+JSON.stringify(a.getReturnValue()));
            var presentRec =a.getReturnValue();
            prdList[currentPos] = presentRec;
            console.log('The currentPos 2222is:'+currentPos);
            component.set("v.lstprds",prdList);    
            
            helper.hideSpinner(component, event, helper);
        });   
        $A.enqueueAction(fetchRec);        
    },    
    captureUserClickIndex : function(component,event,helper) {
        
        // alert(event);
        //   alert(event.currentTarget.getAttribute("data-id"));
        //var whichOne = event.getSource().getLocalId();
        //alert(whichOne);
        component.set("v.presentClickIndex",event.currentTarget.getAttribute("data-id"));
    },
    
    // Below method is used to add one empty row at the bottom of the table. 
    addrow :function(component,event,helper) {
        
        var totalRows = component.get("v.totalRows") + 1;
        var rownum = event.currentTarget.getAttribute("data-id-val"); 
        if(rownum && rownum == 0) {
            var prdList2 = component.get("v.lstprds");          
            var prdListSize = prdList2.length-1;
            //if(prdListSize == 0)
                //prdListSize = 1;
            if(prdListSize == -1)
                prdListSize = 0;
			
            if(prdListSize == 0) {
                var prdList = component.get("v.lstprds");        
                prdList.splice(rownum+1, 0, "");
                component.set("v.lstprds",prdList); 
                component.set("v.prdListSize", prdListSize);
                component.set("v.totalRows", totalRows);
            } else if(prdListSize > 0 && prdList2[prdListSize] && prdList2[prdListSize].prdName) {
                var prdList = component.get("v.lstprds");        
                prdList.splice(rownum+1, 0, "");
                component.set("v.lstprds",prdList); 
                component.set("v.prdListSize", prdListSize);
                component.set("v.totalRows", totalRows);
            } else {
                alert('Please fill the data in current row 1');
            }
            
        }  else {
            // For controlling the display of "ADD ROW" button. 
            var prdList2 = component.get("v.lstprds");          
            var prdListSize = prdList2.length-1;
            //alert('prdList2:'+prdList2[prdListSize]+':::'+prdList2[prdListSize-1].prdName);
            if(prdList2[prdListSize] && prdList2[prdListSize].prdName)
            {
                var prdList = component.get("v.lstprds");        
                prdList.splice(rownum+1, 0, "");
                component.set("v.lstprds",prdList); 
                component.set("v.prdListSize",prdListSize);
                console.log('The prdListSize is:'+prdListSize);
                component.set("v.totalRows", totalRows);
            }
            else
                alert('Please fill the data in current row 2');
        }
    },
    delrow: function(component,event,helper) {
        
        var totalRows = component.get("v.totalRows") - 1;
        var rownum = event.currentTarget.getAttribute("data-id-val2");
        var prdList = component.get("v.lstprds");        
        prdList.splice(rownum, 1);
        component.set("v.lstprds",prdList); 
        
        // For controlling the display of "ADD ROW" button. 
        var prdList2 = component.get("v.lstprds");          
        var prdListSize = prdList2.length-1;
        component.set("v.prdListSize",prdListSize);
        component.set("v.totalRows", totalRows);
    },
    cancelCreate: function(component,event,helper) {
        
        var recordId = component.get("v.opportunityid");
        helper.gotoURL(component, event, helper, recordId);
    }
    
})
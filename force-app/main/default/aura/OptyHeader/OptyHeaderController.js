({  doInit : function(component, event) {
    //console.log("Opty Header loaded");
    var checkId =component.get("v.recordId");     
    if((checkId != undefined) && checkId){
       var action = component.get("c.getOptyProd");
       action.setParams({
        "optyId": checkId
    });
       action.setCallback(this, function(a) {
        var state = a.getState();
      //  console.log("getOptyProd() called");
        var x =a.getReturnValue();
        component.set("v.closeDate",x.closedate);
        component.set("v.opty", a.getReturnValue());
    });
       $A.enqueueAction(action);
   }     
   else {

   }
},

backOpty :  function(component) {
 var optyId = component.get("v.recordId");
 var navEvt = $A.get("e.force:navigateToSObject");
 navEvt.setParams({
    "recordId": optyId                
});
 navEvt.fire();
},

})
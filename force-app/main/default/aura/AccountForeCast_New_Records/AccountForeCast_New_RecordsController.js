({
    addRow : function(component, event, helper){
        //Execute the AddRowEvent Lightning Event 
        component.getEvent("AddRowEvent").fire(); 
    },
     
    deleteRow : function(component, event, helper){
        //Execute the DeleteRowEvent Lightning Event and pass the deleted Row Index to Event attribute
        component.getEvent("DeleteRowEvent").setParams({"indexVar" : component.get("v.rowIndex") }).fire();
    }, 
    
   

    accChange :function(component,event,handler){
        var AccForecast = component.get("v.AccountForecastInstance");
        AccForecast.Account__c = component.get("v.selectedLookUpAccount").Id;
        component.set("v.AccountForecastInstance",AccForecast);
                
        var AccForecaseProductId = component.get("v.AccountForecastInstance");
        AccForecaseProductId.Product__c = component.get("v.selectedLookUpProduct").Id;
        component.set("v.AccountForecastInstance",AccForecaseProductId);
        
        var productCategory = component.get("v.AccountForecastInstance");
        //productCategory.Product_Category__c = component.get("v.productListValues");
        productCategory.Product_Category__c = component.find('selectedProductCategory').get("v.value");
        component.set("v.AccountForecastInstance",productCategory);
    },
})
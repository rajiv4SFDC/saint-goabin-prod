({
    searchNOD_OSBalance:function(component,event,helper){
        var selectedRegion = component.get("v.SelectedRegion");
        var selectedSalesOffice = component.get("v.SelectedSalesOffice");
        var selectedMonthByUser = component.get("v.SelectedMonth");
        var selectedYearByUser = component.get("v.SelectedYear");	
/*        console.log("debug here++--");
        if(component.get("v.selectedLookUpRecord").Id != undefined){
            accObj.Id = component.get("v.selectedLookUpRecord").Id;
        } 
*/        
        //call apex class method to get nod records
        var action = component.get("c.nod");
        action.setParams({
            "region": selectedRegion,
            "salesOffice": selectedSalesOffice,
            "Month":selectedMonthByUser,
            "Year":selectedYearByUser
        })
        
        action.setCallback(this, function(response) {
            //store state of response
            var state = response.getState();
            //alert(state);
            console.log('outside debug in state '+state);
            if (state === "SUCCESS") {
                console.log('debug in state '+state);
                component.set("v.NODList", response.getReturnValue());
                console.log('-->'+response.getReturnValue());
            }
        });
        $A.enqueueAction(action);
    
/*    //to get os record
        var action1 = component.get("c.osBalance");
        action1.setParams({
            "region": selectedRegion,
            "salesOffice": selectedSalesOffice,
            "Month":selectedMonthByUser,
            "Year":selectedYearByUser
            
        });
        action1.setCallback(this, function(response){
            var state1 = response.getState();
            if(state1 === "SUCCESS"){
                console.log('debug in state ='+state1);
                component.set("v.OSList", response.getReturnValue());  
            }
        });
        $A.enqueueAction(action1);
*/        
    
    }
,
	doInit:function(component,event,helper){
        helper.geSelectedMonth(component, event, helper);   
        helper.geSelectedYear(component, event, helper);
        helper.geAllRegions(component, event, helper);
        helper.geAllSalesOffice(component, event, helper);
	}    
   
})
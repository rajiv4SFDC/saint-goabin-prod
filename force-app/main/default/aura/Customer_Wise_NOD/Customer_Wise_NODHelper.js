({
    
    geSelectedMonth : function(component,event,helper)  {
        var action2 = component.get("c.selectedMonth");
        var inputselmonth = component.find("selectedMonthDynamic");
        var opts1=[];
        action2.setCallback(this, function(a){
        	for(var i=0;i< a.getReturnValue().length;i++){
                opts1.push({"class": "optionClass", label: a.getReturnValue()[i], value: a.getReturnValue()[i]});
            }
            inputselmonth.set("v.options", opts1); 
        });
        $A.enqueueAction(action2);
    }
    
,
	geSelectedYear : function(component,event,helper)  {
        var action3 = component.get("c.selectedYear");
        var inputselYear = component.find("selectedYearDynamic");
        var opts2=[];
        action3.setCallback(this, function(a){
        	for(var i=0;i< a.getReturnValue().length;i++){
                opts2.push({"class": "optionClass", label: a.getReturnValue()[i], value: a.getReturnValue()[i]});
            }
            inputselYear.set("v.options", opts2); 
        });
        $A.enqueueAction(action3);
    }
    
,
    geAllRegions : function(component, event, helper){
        var action4 = component.get("c.accountRegions");
        var inputselectedRegion = component.find("selectedRegionDynamic");
        var opts3=[];
        action4.setCallback(this, function(a){
        	for(var i=0;i< a.getReturnValue().length;i++){
                opts3.push({"class": "optionClass", label: a.getReturnValue()[i], value: a.getReturnValue()[i]});
            }
            inputselectedRegion.set("v.options", opts3); 
        });
        $A.enqueueAction(action4);
    }  
,

    geAllSalesOffice : function(component, event, helper){
        var action5 = component.get("c.salesOfficeNames");
        var inputselectedSalesOffice = component.find("selectedSalesOfficeDynamic");
        var opts4=[];
        action5.setCallback(this, function(a){
        	for(var i=0;i< a.getReturnValue().length;i++){
                opts4.push({"class": "optionClass", label: a.getReturnValue()[i], value: a.getReturnValue()[i]});
            }
            inputselectedSalesOffice.set("v.options", opts4); 
        });
        $A.enqueueAction(action5)
    }
})
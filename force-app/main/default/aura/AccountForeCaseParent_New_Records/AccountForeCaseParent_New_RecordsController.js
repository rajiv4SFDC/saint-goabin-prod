({
     
    doInit: function(component, event, helper) {
        //helper.createObjectData(component, event);
		helper.getAllYearList(component, event, helper);
        helper.getAllProductCategory(component, event, helper);
        //for monthly plan records
        //helper.getAllMonthlyPlanRecord(component, event, helper);
    },
     
    Save: function(component, event, helper) {
        if (helper.validate(component, event)) {
            var action = component.get("c.SaveAccounts");
            //var selectedYearForForecast = component.get("v.selectedYear");
            action.setParams({
                "accountForecastList": component.get("v.AccountForecastList"),
                "selectedYears":component.get("v.selectedYear"),
                "accountForecastListOld": component.get("v.AccountForecastListToDisplay")
            });
            action.setCallback(this, function(response) {
                var state = response.getState();
                console.log(state);
                if (state === "SUCCESS") {
                    //helper.createObjectData(component, event);
                    component.set("v.AccountForecastList", []);
                    component.set("v.AccountForecastListToDisplay", []);
                    alert('Account Forecast records saved successfully');
                }
                
                
                
                else if(state === "ERROR"){
                    var errors = '';
                    errors = JSON.stringify(action.getError());
                    var errormessage='';
                    console.log('erroe ->'+errors);
                    if (errors.includes("FIELD_CUSTOM_VALIDATION_EXCEPTION",0)) {
                        if(errormessage == '')
                        	errormessage = errors.substring(errors.indexOf("message")+10, errors.lastIndexOf("}]}")-1)
                        else
                            errormessage = errormessage;
                        console.log('inside errors if ---> '+errormessage);
                        alert(errormessage);
                        component.set("v.AccountForecastList", []);
                    	component.set("v.AccountForecastListToDisplay", []);
                    }
                }
                
                
                
                
            });
            component.set("v.flag", false);        
            $A.enqueueAction(action);
        }
    },
    
    Submit: function(component, event, helper){
        if (helper.validate(component, event)) {
            var action1 = component.get("c.submitAndSaveAccounts");
            action1.setParams({
                "accountForecastList": component.get("v.AccountForecastList"),
                "selectedYears":component.get("v.selectedYear"),
                "accountForecastListOld":component.get("v.AccountForecastListToDisplay")
            });
            console.log('submit entered');
            action1.setCallback(this, function(response) {
                var state = response.getState();
                console.log(state+'-=');
                if (state === "SUCCESS") {
                    console.log('submit entered in success');
                    component.set("v.AccountForecastList", []);
                    component.set("v.AccountForecastListToDisplay", []);
                    //helper.createObjectData(component, event);
                    alert('Account Forecast records saved and submitted successfully');
                }
                
                else if(state === "ERROR"){
                    var errors = JSON.stringify(action1.getError());
                    var errormessage='';
                    console.log('erroe ->'+errors);
                    if (errors.includes("FIELD_CUSTOM_VALIDATION_EXCEPTION",0)) {
                        errormessage = errors.substring(errors.indexOf("message")+10, errors.lastIndexOf("}]}"))
                        console.log('inside errors if '+errormessage);
                        alert(errormessage);
                        component.set("v.AccountForecastList", []);
                    	component.set("v.AccountForecastListToDisplay", []);
                    }
                }
                
            });
            component.set("v.flag", false);        
            $A.enqueueAction(action1);
        }
    },
     
    addRow: function(component, event, helper) {
        helper.createObjectData(component, event);
    },
     
    removeDeletedRow: function(component, event, helper) {
        var index = event.getParam("indexVar");
        var AllRowsList = component.get("v.AccountForecastList");
        AllRowsList.splice(index, 1);
        component.set("v.AccountForecastList", AllRowsList);
    },
    
    search: function(component, event, helper){
        component.set("v.flag",true);
        console.log(component.get("v.flag"));
        if(component.get("v.selectedYear") != 'Year')
            helper.geAllForecast(component, event, helper);
    },
    
    setArrays: function(component, event, helper){
        component.set("v.flag",false);
    },
    
    changedisplayDialogBoxFlag: function(component, event, helper){
        component.set("v.displayDialogBox", true)
        //console.log('displayDialogBox '+component.get("v.displayDialogBox"));
        helper.getAllMonthlyPlanRecord(component, event, helper);
    },
    
	
    
})
({
    createObjectData: function(component, event) {
        //get the AccountForecastList from component and add(push) New Object to List  
        var RowItemList = component.get("v.AccountForecastList");
        RowItemList.push({
            'sobjectType': 'Account_Forecast__c',
            'Account__c': '',
            'Product_Category__c': '',
            'Volume_in_tons_expected__c':''           
        });
        // set the updated list to attribute (AccountForecastList) again    
        component.set("v.AccountForecastList", RowItemList);
    },
    //helper function for check if Account Forecast Name is not null/blank on save  
    validate: function(component, event) {
        var isValid = true;
        var AccForecastList = component.get("v.AccountForecastListToDisplay").length;
        var yearSelected = component.get("v.selectedYear");
        var allAccountRows = component.get("v.AccountForecastList");
        for (var indexVar = 0; indexVar < allAccountRows.length; indexVar++) {
            if(yearSelected == null || yearSelected == ''){
                alert('please fill year');
            }
                
            if (allAccountRows[indexVar].Account__c ==null || allAccountRows[indexVar].Volume_in_tons_expected__c =='' || allAccountRows[indexVar].Product_Category__c == '') {
                isValid = false;
                alert('All fields are mandatory on row number' + (AccForecastList + indexVar + 1));
            }
        }
        return isValid;
    },
    
    getAllYearList : function(component, event, helper){
        var action = component.get("c.selectedYear");
        var inputselYear = component.find("selectedYearDynamic");
        var opts=[];
        action.setCallback(this, function(a){
        	for(var i=0;i< a.getReturnValue().length;i++){
                opts.push({"class": "optionClass", label: a.getReturnValue()[i], value: a.getReturnValue()[i]});
            }
            inputselYear.set("v.options", opts); 
        });
        $A.enqueueAction(action);
    },
    
    getAllProductCategory : function(component, event, helper){
        var action1 = component.get("c.productCategories");
        var inputselProductCat = component.find("selectedProductCategory");
        var opts1 = [];
        var opts2 =[];
        action1.setCallback(this, function(a){
            for(var i=0;i< a.getReturnValue().length;i++){
            	opts1.push({"class": "optionClass", label: a.getReturnValue()[i], value: a.getReturnValue()[i]});
        	    opts2.push(a.getReturnValue()[i]);
            } 
            console.log('--->>'+JSON.stringify(opts1));
            component.set("v.productList",opts1);
            component.set("v.prdCategeory2",opts2);
        });
        $A.enqueueAction(action1);
    },
    
    geAllForecast : function(component, event, helper){
        var action2 = component.get("c.getAllAccountForecast");
        action2.setParams({
                "yearSelected":component.get("v.selectedYear")
            }); 
        console.log('inside here');
        action2.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                console.log('response get values '+JSON.stringify(response.getReturnValue()));
                component.set("v.AccountForecastListToDisplay", []);
                console.log('lenght checjkbDlufhil '+JSON.stringify(component.get("v.AccountForecastListToDisplay")));
                component.set("v.AccountForecastListToDisplayLength", response.getReturnValue().length);
                console.log('acc leng ----> '+component.get("v.AccountForecastListToDisplayLength"));
                component.set("v.AccountForecastListToDisplay", response.getReturnValue());
                console.log('inside '+component.get("v.AccountForecastListToDisplay").length+ 'response length '+response.getReturnValue().length);
            }
            console.log('acc list to disp '+JSON.stringify(component.get("v.AccountForecastListToDisplay")));
            
            if(component.get("v.AccountForecastListToDisplayLength") == 0 && component.get("v.AccountForecastList").length == 0){ 
                console.log('create object');
                helper.createObjectData(component, event);
            }
            /*if(component.get("v.AccountForecastListToDisplay").length == 0 && component.get("v.AccountForecastList").length == 0){ 
                console.log('create object');
                helper.createObjectData(component, event);
            }*/
        });
        $A.enqueueAction(action2);
    },
    
    getAllMonthlyPlanRecord: function(component, event, helper){
        var target = event.target;
        var rowIndex = target.getAttribute("data-row-index");
        var indexnumber = rowIndex;
        console.log("Row No : " + indexnumber);
        
        var accountForCastRows = component.get("v.AccountForecastListToDisplay");
        var clickedForecastAccount = accountForCastRows[indexnumber].Account__c;
        var clickedForecastProductCat = accountForCastRows[indexnumber].Product_Category__c;
        var clickedForecastId = accountForCastRows[indexnumber].Id;
        
        component.set("v.onClickSelectedAccountForecast", clickedForecastId);
        console.log('--> console logg '+component.get("v.onClickSelectedAccountForecast"));
        //alert('1->'+clickedForecastAccount +'2->'+clickedForecastProductCat+'3->'+clickedForecastId);
        var action3 = component.get("c.getMonthlyPlan"); 
        action3.setParams({
                "account":clickedForecastAccount,
            	"productCat":clickedForecastProductCat,
            	"accountForecastId":clickedForecastId
            });
        action3.setCallback(this, function(response) {
            var state = response.getState();
            console.log('state bhai'+state);
            if (state === "SUCCESS") {
                component.set("v.MonthlyPlanList", response.getReturnValue());
                console.log('inside monthly plan block');
            }
            
            if(component.get("v.MonthlyPlanList").length == 0){ 
                console.log('create monthly object');
                //helper.createmonthlyPlanObjectData(component, event);
            }
        });
        $A.enqueueAction(action3);
    },
    
    createmonthlyPlanObjectData: function(component, event) {
        //get the AccountForecastList from component and add(push) New Object to List  
        var RowItemList = component.get("v.MonthlyPlanList");
        RowItemList.push({
            'sobjectType': 'Monthly_Plan__c',
            'Account__c': coponent.get("v.clickedForecastAccount"),
            'Product_Category__c': component.get("v.clickedForecastProductCat"),
            'Monthly_Target__c':'',
            'My_Target__c':'',
            'Acheived_Target__c':''
        });
        // set the updated list to attribute (AccountForecastList) again    
        component.set("v.MonthlyPlanList", RowItemList);
    },
    
    showMessage:function(msg, msgtype, display_mode, duration){
        if(!duration)
            duration = 5000;
        
            //display_mode: dismissible, pester, sticky 
            var toastEvent = $A.get("e.force:showToast");
            toastEvent.setParams({
                message: msg,
                messageTemplate: msg,
                duration:duration,
                key: 'info_alt',
                type: msgtype,
                mode: display_mode
            });
            toastEvent.fire();
    },
})
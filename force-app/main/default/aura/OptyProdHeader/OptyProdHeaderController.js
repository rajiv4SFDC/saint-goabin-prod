({
    doInit : function(component, event) {
        var checkId =component.get("v.recordID");
        //console.log("OptyProdHeader loaded");
        if((checkId != undefined) && checkId!=null){
            var action = component.get("c.getOptyProdId");
            action.setParams({
                "optyprodId": checkId
            });
       
            action.setCallback(this, function(a) {
                var state = a.getState();
                var x = JSON.parse(a.getReturnValue()); 
                //console.log("OptyProdHeader {doInit}- getOptyProdId()");
                component.set("v.optyProd", x);
            });
            $A.enqueueAction(action);  
        }
    },
    
    retrieveoptyprodID : function(component, event){        
        var id = event.getParam("prodrecID");
        if(id!=null && id!='undefined'){
        component.set("v.recordID", id);   
        if(id != null && id!='undefined'){
        var action = component.get("c.getOptyProdId");
        action.setParams({
            "optyprodId": id
        });
     
        action.setCallback(this, function(a) {
            var state = a.getState();
            //console.log("OptyProdHeader {retrieveoptyprodID}- getOptyProdId()");
            var x = JSON.parse(a.getReturnValue()); 
            component.set("v.optyProd", x);
        });
        $A.enqueueAction(action);       
        }
    }
    }

})
({
    createObjectData: function(component, event) {
        //get the MonthlyPlanList from component and add(push) New Object to List  
        var RowItemList = component.get("v.MonthlyPlanList");
        RowItemList.push({
            'sobjectType': 'Monthly_Plan__c',
            'Account_Name_Hidden__c': component.get("v.MonthlyPlanInstanceData")[0],
            'Product_Category__c': component.get("v.MonthlyPlanInstanceData")[1],
            'Monthly_Target__c': component.get("v.MonthlyPlanInstanceData")[2],
            'Acheived_Target__c':'',
            'My_Target__c':''
        });
        // set the updated list to attribute (AccountForecastList) again    
        component.set("v.MonthlyPlanList", RowItemList);
    },
    
    MonthlyPlanPrePopulatedData : function(component, event, helper){
     	var action1 = component.get("c.getAllAttributesForMonthlyPlan");
        console.log('do init of monthly comp '+component.get("v.clickedAccForecastId"));
        action1.setParams({
            "accountForecastId": component.get("v.clickedAccForecastId"),
        });
        
        action1.setCallback(this, function(response) {
            var state = response.getState();
            console.log('monthlyplan init @ '+state);
            if (state === "SUCCESS") {
                component.set("v.MonthlyPlanInstanceData", response.getReturnValue());
            }
        
            if(component.get("v.flagCheck") < 2){
                helper.showSpinner(component);
         		console.log('kyatte baar');
                component.set("v.flagCheck", component.get("v.flagCheck")+1);
                setTimeout($A.getCallback(() => this.MonthlyPlanPrePopulatedData(component, event, helper)), 1000);
            }
            else{
                if(component.get("v.OldMonthlyPlanList").length == 0 && component.get("v.flagCheck") >= 2){
                    helper.createObjectData(component, event);
                }
                component.set("v.isOpen", true);
                helper.hideSpinner(component);
            }
        });
            
            $A.enqueueAction(action1);   
    },
    
    showSpinner:function(component){
        
        component.set("v.IsSpinner",true);
    },
    
    hideSpinner:function(component){
        
        component.set("v.IsSpinner",false);
    },
    

    
    
    
    
    //helper function for check if Account Forecast Name is not null/blank on save  
/*    validate: function(component, event) {
        var isValid = true;
        var AccForecastList = component.get("v.AccountForecastListToDisplay").length;
        var yearSelected = component.get("v.selectedYear");
        var allAccountRows = component.get("v.AccountForecastList");
        for (var indexVar = 0; indexVar < allAccountRows.length; indexVar++) {
            if(yearSelected == null || yearSelected == ''){
                alert('please fill year');
            }
                
            if (allAccountRows[indexVar].Account__c ==null || allAccountRows[indexVar].Volume_in_tons_expected__c =='' || allAccountRows[indexVar].Product_Category__c == '') {
                isValid = false;
                alert('All fields are mandatory on row number' + (AccForecastList + indexVar + 1));
            }
        }
        return isValid;
    },	*/
})
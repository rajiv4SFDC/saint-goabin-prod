({
   openModel: function(component, event, helper) {
      component.set("v.isOpen", true);
   },
 
   closeModel: function(component, event, helper) {
      component.set("v.isOpen", false);
   },
 
   likenClose: function(component, event, helper) {
      component.set("v.isOpen", false);
   },
    
    doInit: function(component, event, helper){
        console.log('monthly comp');
    },
    
    addRow: function(component, event, helper) {
        helper.createObjectData(component, event);
    },
     
    removeDeletedRow: function(component, event, helper) {
        var index = event.getParam("indexVar");
        var AllRowsList = component.get("v.MonthlyPlanList");
        AllRowsList.splice(index, 1);
        component.set("v.MonthlyPlanList", AllRowsList);
    },
    
    SaveMonthlyPlan: function(component, event, helper) {
        var action = component.get("c.savemonthlyPlan");
        action.setParams({
            "monthlyPlanList": component.get("v.MonthlyPlanList"),
            "accountForecastId": component.get("v.clickedAccForecastId"),
        });
        action.setCallback(this, function(response) {
            var state = response.getState();
            console.log(state);
            if (state === "SUCCESS") {
                component.set("v.MonthlyPlanList", []);
                alert('Monthly Plan records saved successfully');
                }
            });
            component.set("v.isOpen", false);        
            $A.enqueueAction(action);
    },
    
    Submit: function(component, event, helper){
        var action = component.get("c.submitAndSavemonthlyPlan");
        action.setParams({
            "monthlyPlanList": component.get("v.MonthlyPlanList"),
            "accountForecastId": component.get("v.clickedAccForecastId"),
            "monthlyPlanListOld": component.get("v.OldMonthlyPlanList"),
        });
        action.setCallback(this, function(response) {
            var state = response.getState();
            console.log(state);
            if (state === "SUCCESS") {
                component.set("v.MonthlyPlanList", []);
                alert('Monthly Plan records submitted successfully');
                console.log('MP entered');
                }
            });
            component.set("v.isOpen", false);        
            $A.enqueueAction(action);
    },
    
    doInit: function(component, event, helper){
        component.set("v.isOpen", false);
        helper.MonthlyPlanPrePopulatedData(component, event, helper);
    }
})
({
  doInit : function(component, event) {         
     
    if(component.get("v.onOptyProdPage")){
       
        //console.log("MonthTileList loaded");
        var id = component.get("v.prodID");          
            var  myEvent = $A.get("e.c:OptyProdIdEvent");//doubtful
            myEvent.setParams({"prodrecID": id});
            myEvent.fire();
            //console.log("MonthTileList {doInit} - OptyProdIdEvent");
            var forecastaction = component.get("c.viewForecast");
            
            forecastaction.setParams({
                "optyId": id       
            });
            
            forecastaction.setCallback(this, function(a) {
               // console.log("MonthTileList {doInit} - viewForecast()");
                var forecaststate = a.getState();
                component.set("v.forecasts", a.getReturnValue());        
            });
            
            $A.enqueueAction(forecastaction);        
        }
        
    },
    retrieveoptyprodforecast : function(component, event, helper) {     
       
        var id = event.getParam("prodrecID");

        if(id != null && id!='undefined'){
            component.set("v.prodID", id);   
            
            var forecastaction = component.get("c.viewForecast");  
            forecastaction.setParams({
                "optyId": id       
            });
            
            forecastaction.setCallback(this, function(a) {
                //console.log("MonthTileList {retrieveoptyprodforecast} - viewForecast()");
                var forecaststate = a.getState();
                component.set("v.forecasts", a.getReturnValue());
            });
            
            $A.enqueueAction(forecastaction);   
            
        }else{
         
        }
        
    },
    monthsChange : function(component, event){
       
        var compEvent = $A.get("e.c:MonthListEvent");
       // console.log("MonthTileList {monthsChange} called");
            //console.log("Firing month evt OLD:"+event.getParam("oldValue"));
           // console.log("Firing month evt NEW:"+event.getParam("value"));
           compEvent.setParams({"monthList" : event.getParam("value")});
           compEvent.fire();
           component.set("v.onOptyProdPage",false);
           
       }      
       ,
       setoptyID : function(component, event){
         // console.log("MonthTileList {setoptyID} called");
          component.set("v.recordID",component.get("v.recordId"));
      }

  })
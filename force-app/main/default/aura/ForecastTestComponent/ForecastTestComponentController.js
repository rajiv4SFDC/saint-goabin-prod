({
    doInit : function(component, event) {
     // console.log("Forecast Component loaded"); 
      var checkId =component.get("v.recordId");    
     // console.log("Id found in initial component -"+checkId);        
       var prevIdPresent =component.get("v.OpportunityId");   

    
      if(checkId!=null && checkId!='undefined'){
        component.set("v.OpportunityId",checkId);
        var action = component.get("c.getOptyProdCount");
       // console.log("ForecastComponent getOptyProdCount() gets called");
        action.setParams({
          "optyId": checkId
        });
        action.setCallback(this, function(a) {
          component.set("v.prodCount", a.getReturnValue());

          //New code
          var device = $A.get("$Browser.formFactor");
          component.set("v.device",device);
         // console.log("DEVICE --"+device);

          var screen = component.get("v.screenCheck");
        //  console.log("SCREEN --"+screen);
     // var prodCountCheck = component.get("v.prodCountCheck");
     var cnt = a.getReturnValue();
     //console.log("Product count :"+cnt);

     if((device==='DESKTOP') && (screen || screen=='undefined')&& cnt){  
       var evt = $A.get("e.force:navigateToComponent");
       evt.setParams({
         componentDef : "c:ForecastTestComponent",
         componentAttributes: {
           recordId : component.get("v.recordId"),
           screenCheck : false
          // prodCountCheck : cnt
        }
      });

       evt.fire();
     }else {
        //screen mobile or screen equal to 1 and  || device==='TABLET'
        component.set("v.screenCheck",false);
      }
        //New code ends here
      });
        $A.enqueueAction(action);
    }
    }
  })
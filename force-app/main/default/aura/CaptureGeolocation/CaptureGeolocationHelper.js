({
	projRecordUpdated : function(component, event, helper) {
		
		console.log("LOADED record obj ",component.get("v.projRecordFields"));
        
        var projObj = component.get("v.projRecordFields");
        var isSalesforceOneChk = false;

        var device = $A.get("$Browser.formFactor");

        if(device == 'PHONE' || device == 'TABLET'){
            isSalesforceOneChk = true;
        }

        if(  projObj.Validated__c != null && projObj.Validated__c == true && isSalesforceOneChk){
            component.set("v.projHasGeoTag",true);
             //var cp = component.find("projHasGeoTagElseCmp");
             //$A.util.toggleClass(cp, "slds-hide");
          
            /* Call Map and Near by Project component */
            var obj = {};
            if( projObj.Geolocation__Latitude__s === undefined ){
                 obj.latitude = -33.137551 ;
            }else{
                 obj.latitude = projObj.Geolocation__Latitude__s ;
            }
            if( projObj.Geolocation__Longitude__s === undefined ){
                 obj.longitude = 81.826172 ;
            }else{
                 obj.longitude = projObj.Geolocation__Longitude__s ;
            }
            obj.projectName = projObj.Name ;
            obj.salesArea = projObj.Sales_Area__c ;

            var targetNearByLocation = component.find("NearByLocationCmp");

            targetNearByLocation[1].getNearByProjects(obj,component.get("v.recordId"),
                                                true,20);

            component.set("v.geolocationObj",obj);

            /* Hide Proceed Geo Tag button */

            var tarCmp = component.find("proceedButton");
            $A.util.addClass(tarCmp, 'slds-hide');
        }

	}
})
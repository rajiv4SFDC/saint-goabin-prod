({
	getLocationCoordinate : function(component, event, helper) {
        var obj = event.getParam("geolocationObj");
        var targetNearByLocation = component.find("NearByLocationCmp");        
        targetNearByLocation.getNearByProjects(obj,component.get("v.recordId"), true,20);
        component.set("v.geolocationObj",obj);
	},
    
    getNearLocation :  function(component, event, helper) {
        var obj = event.getParam("projectObjLst");
        var targetMap = component.find("mapCmp");
       
        /*-- Update geoLocation obj with some addional Project details -- */
       var currObj = component.get("v.geolocationObj");
       var currObjFields = component.get("v.projRecordFields");

        if( currObj !== undefined && currObj != null && currObjFields != null ){
            currObj.projectName = component.get("v.projRecordFields").Name;
            currObj.salesArea = component.get("v.projRecordFields").Sales_Area__c;
            targetMap.stampCurrentandRelatedGeo(currObj,obj);
        } 
    },

    doInit :  function(component, event, helper){

        var currentProjId = component.get("v.recordId");

        var action = component.get('c.getCurrentProjectDetail');

        action.setParams({
           
            currentProjectId : currentProjId
        });

        action.setCallback(this, function(response){
            var state = response.getState();
            console.log('state:', state);
            if (state == "SUCCESS") {
                var resObj = JSON.parse(response.getReturnValue());
                    if(typeof resObj != "undefined" ){
                        
                       console.log('dfdf :: ',resObj);
                       var obj ={};
                       obj.Name = resObj.Name ;
                       obj.Geolocation__Latitude__s = resObj.Geolocation__Latitude__s ;
                       obj.Geolocation__Longitude__s = resObj.Geolocation__Longitude__s ;
                       obj.Sales_Area__c = resObj.Sales_Area__c ;
                       obj.Validated__c = resObj.Validated__c ;
                       obj.id = resObj.Id ;
                       
                       component.set("v.projRecordFields",obj);
                        console.log('dfdf ::33 ',obj);
                    /* Call method which do the next steps */
                    helper.projRecordUpdated(component, event, helper);

                    }else{
                       
                    }
            }
            
        });


        $A.enqueueAction(action);
        
    },
    
    projRecordUpdated1: function(component, event, helper) {

    var changeType = event.getParams().changeType;

    if (changeType === "ERROR") { /* handle error; do this first! */ }
    else if (changeType === "LOADED") { 
    	console.log("LOADED record obj ",component.get("v.projRecordFields"));
        
        var projObj = component.get("v.projRecordFields");
        var isSalesforceOneChk = false;

        var device = $A.get("$Browser.formFactor");

        if(device == 'PHONE'){
            isSalesforceOneChk = true;
        }

        if(  projObj.Validated__c != null && projObj.Validated__c == true && isSalesforceOneChk){
            component.set("v.projHasGeoTag",true);
             //var cp = component.find("projHasGeoTagElseCmp");
             //$A.util.toggleClass(cp, "slds-hide");
          
            /* Call Map and Near by Project component */
            var obj = {};
            obj.latitude = projObj.Geolocation__Latitude__s ;
            obj.longitude = projObj.Geolocation__Longitude__s ;
            obj.projectName = projObj.Name ;
            obj.salesArea = projObj.Sales_Area__c ;

            var targetNearByLocation = component.find("NearByLocationCmp");

            targetNearByLocation[1].getNearByProjects(obj,component.get("v.recordId"),
                                                true,10000);
            component.set("v.geolocationObj",obj);

            /* Hide Proceed Geo Tag button */

            var tarCmp = component.find("proceedButton");
            $A.util.addClass(tarCmp, 'slds-hide');
        }
    }
    else if (changeType === "REMOVED") { /* handle record removal */ }
    else if (changeType === "CHANGED") { /* handle record change */ }
	},


    handleSaveRecord : function(component, event, helper){
         
        var latitude = component.get("v.geolocationObj").latitude;
        var longitude = component.get("v.geolocationObj").longitude;
        var currentProjId = component.get("v.recordId");

        var action = component.get('c.setCapturedGeoLocation');
        
        action.setParams({
            Lat : latitude,
            Lon : longitude,
            currentProjectId : currentProjId
        });

        action.setCallback(this, function(response){
            var state = response.getState();
            console.log('frm proceed buton state:', state);
            if (state == "SUCCESS") {
                var resObj = response.getReturnValue();
                
                    if(resObj == "SUCCESS"){
                            var toastEvent = $A.get("e.force:showToast");
                            toastEvent.setParams({
                                "title": "Success!",
                                "message": "GEO TAGGING has been done successfully",
                                "type": "success",
                                "duration":"2000"
                            });
                            toastEvent.fire();

                            /* Nav to Project Record */
                          
                            var navEvt = $A.get("e.force:navigateToSObject");
                            navEvt.setParams({
                              "recordId": currentProjId,
                              "slideDevName": "detail"
                            });
                            navEvt.fire();
                 
                    }else{

                        var toastEvent = $A.get("e.force:showToast");
                            toastEvent.setParams({
                                "title": "Error!",
                                "message": resObj,
                                "type": "error",
                                "duration":"2000"
                            });
                            toastEvent.fire();
                    }
            }
            
        });
        
        $A.enqueueAction(action);

    },

    locationChangeInDisplayMap : function(component, event, helper){
         var loc = event.getParam("token");
         if(loc.indexOf("/sObject/") >=0){
            component.destroy();
            console.log("DisplayGeoMAP cmp is destroyed");
         }
    },

    handleOnCancel :  function(component, event, helper){
        var dismissActionPanel = $A.get("e.force:closeQuickAction");
         component.destroy();
         dismissActionPanel.fire();
    }
    
})
({
    doInit : function(component, event, helper) {
        
        var action = component.get("c.getOppProductRecords");
        action.setParams({ 'oppProductId' : component.get("V.opportunityprodid")});
        action.setCallback(this, function(response) {
            var state = response.getState(); //Checking response status
            console.log("products... "+JSON.stringify(response.getReturnValue()));
            
            if (component.isValid() && state === "SUCCESS"){
                var recOppProd = response.getReturnValue();
            	component.set("v.selOpportunityProduct", recOppProd[0]);  // Adding values in Aura attribute variable. 
            }
        });
        $A.enqueueAction(action);
        
        var priceRequestId = component.get("v.priceRequestId");
        if(priceRequestId) {
            
            var action = component.get("c.returnValues");
            action.setParams({ 'priceRequestId' : priceRequestId });
            action.setCallback(this, function(response) {
                var state = response.getState(); //Checking response status                
                if (component.isValid() && state === "SUCCESS"){
                    var res1 = response.getReturnValue();
                    component.set("v.PriceRequest", res1[0]);
                }
            });
            $A.enqueueAction(action);
        }
    },

    getOpportunityLineItems : function(component, event, helper){
        component.set("v.Opportunitys", null);
        var oppProduct = component.get("v.selOpportunityProduct");
      
        if(oppProduct && oppProduct.Id) {
            
            var action = component.get("c.getProductLineItemRecords");
            action.setParams({ 'proId' :  oppProduct.Id});
            action.setCallback(this, function(response) {
                var state = response.getState(); //Checking response status
                console.log("Line Items... "+"statel:"+state+'###items:'+JSON.stringify(response.getReturnValue()));
                
                if (component.isValid() && state === "SUCCESS")
                    component.set("v.Opportunitys", response.getReturnValue());  // Adding values in Aura attribute variable.
                
                
            });
            $A.enqueueAction(action);
        }
    },
    
	create : function(component, event, helper) {
		console.log('Create record');
        
        //getting the PriceRequest information
        var priceRequest = component.get("v.PriceRequest");
        var oppProdLine = component.get("v.Opportunitys");
        
        //Calling the Apex Function
       
        var recid = component.get("v.selOpportunityProduct.Id");
        console.log('The coming val is:'+recid);
        priceRequest.Opportunity_Product__c = recid;
        priceRequest.Requested_Quantity__c = component.get("v.selOpportunityProduct.Balance_Quantity_Frm_Forecast__c");
        priceRequest.Freight__c = component.get("v.frieght");
        priceRequest.Installation_Cost__c = component.get("v.installationcost");
        priceRequest.Tax__c = component.get("v.tax");
        
        var action = component.get("c.createRecord");
         
        //Setting the Apex Parameter
        action.setParams({
            'pr' : priceRequest,
            'opli':oppProdLine,
        });
        
        //Setting the Callback
        action.setCallback(this,function(a){
            //get the response state
            var state = a.getState();
            
            //check if result is successfull
            if(state == "SUCCESS"){
                //Reset Form
                var newPriceRequest = {'sobjectType': 'Price_Request__c',
                                        'Discount__c': '',
                                        'Tax__c': '',
                                        'Freight__c': '', 
                                        'Installation_Cost__c': '',
                                        'Opportunity_Product__c':'',
                                        'Requested_Quantity__c':'',
                         				'Glass_Price_Discount__c':'',
                                        'Valid_From__c':'',
                                        'Valid_To__c':''
                                   };
                //resetting the Values in the form
                component.set("v.PriceRequest", newPriceRequest);
                var response = a.getReturnValue();
                
                var recordIdval = response['ID'];
                var status = response['STATUS'];
                
                if(status == 'SUCCESS') {
                    
                    helper.gotoURL(component, event, helper, recordIdval);
                    
                } else {
                    
                    component.set("v.showError", true);
                    component.set("v.msgHeading", status);
                    component.set("v.msgBody", response['MSG']);
                    
                    //helper.showToast(component, event, helper, status, response['MSG']);
                }
                
            } else if(state == "ERROR"){
             }
        });
        
		//adds the server-side action to the queue        
        $A.enqueueAction(action);

    },
    cancelCreate: function(component,event,helper) {
        
        var recordId = component.get("v.opportunityprodid");
        helper.gotoURL(component, event, helper, recordId);
    },
    
    closeModal: function(component,event,helper) {
        
        component.set("v.showError", false);
        component.set("v.msgHeading", '');
        component.set("v.msgBody", '');
    } 
})
({
    gotoURL : function (component, event, helper, recordId) {
        var device = $A.get("$Browser.formFactor");
        if(device == 'DESKTOP') {
            sforce.one.navigateToURL('/'+recordId);
        } else {
            var urlEvent = $A.get("e.force:navigateToURL");
            urlEvent.setParams({
                "url": "/"+recordId
            });
            urlEvent.fire();
        }
    },
    showSpinner : function(component,event,helper){
        component.set("v.toggleSpinner", true);  
    },
    
    hideSpinner : function(component,event,helper){
        component.set("v.toggleSpinner", false);
    },
    
    showToast : function(component, event, helper, type, message) {
        var toastEvent = $A.get("e.force:showToast");
        toastEvent.setParams({
            "title": type+"!",
            "message": message,
            "type": type
        });
        toastEvent.fire();
    }
})
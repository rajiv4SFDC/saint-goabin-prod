({
    getNearLocation :  function(component, event, helper) {
        var obj = event.getParam("projectObjLst");
        var targetMap = component.find("mapCmp");
       
        /*-- Update geoLocation obj with some addional Project details -- */
        var currObj = component.get("v.currentGeolocationObj");
        if( typeof currObj != "undefined"){
            currObj.projectName = component.get("v.projRecordFields").Name;
            currObj.salesArea = component.get("v.projRecordFields").Sales_Area__c;
            targetMap.stampCurrentandRelatedGeo(currObj,obj);
        }
        
      
    },
    
    projRecordUpdated: function(component, event, helper) {

    var changeType = event.getParams().changeType;

    if (changeType === "ERROR") { /* handle error; do this first! */ }
    else if (changeType === "LOADED") { 
    	console.log("LOADED record obj record data changes enter");
    	var projectFieldDetail = component.get("v.projRecordFields");
        console.log("projectFieldDetail data : ",projectFieldDetail);
        if( projectFieldDetail.Geolocation__Latitude__s != null &&
             projectFieldDetail.Geolocation__Longitude__s != null ){
            
            var geoObj = {};
            if( typeof projectFieldDetail.Geolocation__Latitude__s === 'string' ){
                geoObj.latitude = Number(projectFieldDetail.Geolocation__Latitude__s);
            }else{
                geoObj.latitude = projectFieldDetail.Geolocation__Latitude__s;
            }
            if( typeof projectFieldDetail.Geolocation__Longitude__s === 'string'){
                geoObj.longitude = Number(projectFieldDetail.Geolocation__Longitude__s);
            }else{
                geoObj.longitude = projectFieldDetail.Geolocation__Longitude__s;
            }
            component.set("v.currentGeolocationObj",geoObj);
            helper.getLocationCoordinate(component, event, helper);
            console.log("projectRadius val :",component.get("v.projectRadius"));

        }else{
            console.log("geo code not found on current record");

            var targetMap = component.find("mapCmp");
             targetMap.geoTagMissingHandlerOnMap(false);

        }
      
    	
    }
    else if (changeType === "REMOVED") { /* handle record removal */ }
    else if (changeType === "CHANGED") { /* handle record change */ }
	},

    locationChangeInDisplayMap : function(component, event, helper){
         var loc = event.getParam("token");
         if(loc.indexOf("/sObject/") >=0){
            component.destroy();
            console.log("DisplayGeoMAP cmp is destroyed");
            //$A.get('e.force:refreshView').fire();
            console.log("reload page called after destroy");
            location.reload();
            
         }
         //console.log("No DisplayGeoMAP cmp is destroyed");
    }

})
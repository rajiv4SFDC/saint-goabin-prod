({
	getLocationCoordinate : function(component, event, helper) {
        
        var targetNearByLocation = component.find("NearByLocationCmp");

        targetNearByLocation.getNearByProjects( component.get("v.currentGeolocationObj"),
        										component.get("v.recordId"),
        										component.get("v.defaultNearLocationOn"),
        										component.get("v.projectRadius"));
        console.log('radius :: ',component.get("v.projectRadius"));
        
	},
})
({
	searchRegionHelper : function(component, event, helper) {
		var ToDate;
        var EndDate, seletedRegion;
        ToDate = component.get("v.ToDate");
        EndDate = component.get("v.EndDate");
        seletedRegion = component.get("v.region");
        console.log('ToDate ' + JSON.stringify(ToDate));
        console.log('EndDate ' + JSON.stringify(EndDate));
        console.log('seletedRegion ' + JSON.stringify(seletedRegion));
         //Calling the Apex Function
        var action = component.get("c.fetchRecord");         
        //Setting the Apex Parameter
        //
        
        action.setParams({'ToDate' : ToDate, 'EndDate' : EndDate, 'seletedRegion' : seletedRegion});
        //Setting the Callback
        action.setCallback(this,function(response){
            //get the response state
            var state = response.getState();
            
            //check if result is successfull
            if(state == "SUCCESS") {
                
                component.set("v.RegionRecordList", response.getReturnValue());
                
            } else if(state == "ERROR"){
                
            }
        });
        
        //adds the server-side action to the queue        
        $A.enqueueAction(action);	

	}
})
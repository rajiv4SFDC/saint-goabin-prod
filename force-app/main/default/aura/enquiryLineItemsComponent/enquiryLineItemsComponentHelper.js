({
    loadExistingDataHelper: function(component,event){
        // Loading the eixiting record into component.
        var recId = component.get("v.LineItemrecId");
        if(recId && recId!='' && recId!="undefined")
        {
            var getExistingRec = component.get("c.getExistingEnqLineitemRec");
            getExistingRec.setParams({"recId":recId});
            getExistingRec.setCallback(this,function(a){
                //   alert(JSON.stringify(a.getReturnValue())); 
                
                
                this.onCategoryChangeHelper(component,event,a.getReturnValue().Product_category__c);
                this.onProductChangeHelper(component,event,a.getReturnValue().Product_Name__c);
                
                if(a.getReturnValue())
				{
                component.set("v.EnuLineItemRec",a.getReturnValue());
				if(a.getReturnValue().Id)
                component.set("v.LineItemrecId",a.getReturnValue().Id);
                component.set("v.EnuLineItemRec",a.getReturnValue());
                component.set("v.LineItemrecId",a.getReturnValue().Id);
                
                component.set("v.enqLineItem.cases",a.getReturnValue().Cases__c);
                component.set("v.enqLineItem.orderQuanitity",a.getReturnValue().Order_quantity__c);
                component.set("v.enqLineItem.UOM",a.getReturnValue().UOM__c);
                component.set("v.enqLineItem.listPriceCst",a.getReturnValue().listPriceCst__c);
                component.set("v.enqLineItem.basePrice",a.getReturnValue().Base_price__c);
                
                if(a.getReturnValue().Special_request_quantity__c == 'undefined'  )
                    component.set("v.enqLineItem.sp_requestQuanitity","N/A");   
                if(a.getReturnValue().sp_priceRequestCst == 'undefined')
                    component.set("v.enqLineItem.sp_priceRequestCst","N/A");
                else
                    component.set("v.enqLineItem.sp_priceRequestCst",a.getReturnValue().Special_price_request_cst__c);   
                if(a.getReturnValue().Valid_from__c == 'undefined')
                    component.set("v.enqLineItem.validFrom","N/A");
                else
                    component.set("v.enqLineItem.validFrom",a.getReturnValue().Valid_from__c);
                
                if(a.getReturnValue().Valid_to__c == 'undefined')
                    component.set("v.enqLineItem.validTo","N/A");
                else
                    component.set("v.enqLineItem.validTo",a.getReturnValue().Valid_to__c);
                component.set("v.enqLineItem.remarks",a.getReturnValue().remarks__c);
                
                component.set("v.enqLineItem.sp_requestQuanitity","N/A");   
                }
                
                
                var temp =component.get("v.EnuLineItemRec");
                console.log('The EnuLineItemRec is :'+JSON.stringify(temp.Product_category__c));
            });
            $A.enqueueAction(getExistingRec);            
            
        }     
        
    },
    
    onCategoryChangeHelper : function(component,event,prdCategory) {
        this.showSpinner(component, event);
        component.set("v.enqLineItem.category3","");
        component.set("v.enqLineItem.product3","");
        component.set("v.enqLineItem.product","");
        component.set("v.enqLineItem.thickness3","");
        component.set("v.enqLineItem.length3","");
        component.set("v.enqLineItem.packTypeTons3","");
        component.set("v.enqLineItem.cases","");
        component.set("v.enqLineItem.orderQuanitity",""); 
        component.set("v.enqLineItem.price3","");
        component.set("v.enqLineItem.price3","");
        component.set("v.enqLineItem.UOM","");
        component.set("v.enqLineItem.UOM","");
        component.set("v.enqLineItem.listPriceCst",""); 
        component.set("v.enqLineItem.basePrice","");
        component.set("v.enqLineItem.Standardpriceperunit","");
        component.set("v.enqLineItem.priceRequest",""); 
        component.set("v.enqLineItem.sp_requestQuanitity","");
        component.set("v.enqLineItem.sp_priceRequestCst","");
        
        
        
        
        var thicknessList =[];
        thicknessList.push('-None-');
        component.set("v.enqLineItem.thickness",thicknessList);   
        var lengthList =[];
        lengthList.push('-None-');
        component.set("v.enqLineItem.thickness",lengthList); 
        component.set("v.enqLineItem.length",lengthList);
        var widthList =[];
        widthList.push('-None-');
        component.set("v.enqLineItem.width",widthList);  
        var packTypeList =[];
        packTypeList.push('-None-');        
        component.set("v.enqLineItem.packTypeTons",packTypeList); 
        component.set("v.enqLineItem.cases","");
        var orderQuanitityList =[];
        orderQuanitityList.push('-None-');
        component.set("v.enqLineItem.orderQuanitity",orderQuanitityList); 	
        component.set("v.enqLineItem.listPriceCst","");
        component.set("v.enqLineItem.basePrice","");        
        component.set("v.enqLineItem.listPriceCst","");    
        
        component.set("v.enqLineItem.UOM","");
        component.set("v.enqLineItem.Standardpriceperunit","");	
        var priceList =[];
        priceList.push('-None-');
        priceList.push('List');
        priceList.push('Tier');
        priceList.push('Special');
        priceList.push('Search');
        component.set("v.enqLineItem.priceRequest","");
        component.set("v.enqLineItem.price",priceList);   
        component.set("v.enqLineItem.sp_requestQuanitity","");
        component.set("v.enqLineItem.sp_priceRequestCst","");
        component.set("v.enqLineItem.validFrom","");
        component.set("v.enqLineItem.validTo","");
        
        var CategoryChange = component.get("c.getCategoryChange");
        var categoryName = event.getSource().get("v.value"); 
        //  var categoryName = component.find("prdCat").get("v.value");
        // alert('The categoryName name :' +categoryName);
        
        // component.set("v.showEdit","true");
        
        var editmode = component.get("v.showEdit");
        if(editmode == "false")
            categoryName = prdCategory;
        
        //  alert('The categoryName name2 :' +categoryName+'....'+prdCategory);
        
        
        
        console.log('The coming prdCategory is :'+prdCategory+'....'+categoryName);
        CategoryChange.setParams({"categoryName":categoryName});
        
        CategoryChange.setCallback(this,function(a){
            component.set("v.enqLineItem.product",a.getReturnValue()); 
            this.hideSpinner(component, event);
        });
        $A.enqueueAction(CategoryChange);
       // this.hideSpinner(component, event);
        var produtName = component.get("v.EnuLineItemRec.Product_Name__c");
        //this.onProductChangeHelper(component,event,produtName);
    },
    onProductChangeHelper: function(component,event,prdName)
    {
        this.showSpinner(component, event);
        //  alert("prdName"+prdName);
        var thicknessList =[];
        thicknessList.push('-None-');
        component.set("v.enqLineItem.thickness",thicknessList);   
        var lengthList =[];
        lengthList.push('-None-');
        component.set("v.enqLineItem.thickness",lengthList); 
        component.set("v.enqLineItem.length",lengthList);
        var widthList =[];
        widthList.push('-None-');
        component.set("v.enqLineItem.width",widthList);  
        var packTypeList =[];
        packTypeList.push('-None-');        
        component.set("v.enqLineItem.packTypeTons",packTypeList); 
        component.set("v.enqLineItem.cases","");
        var orderQuanitityList =[];
        orderQuanitityList.push('-None-');
        component.set("v.enqLineItem.orderQuanitity",orderQuanitityList); 	
        component.set("v.enqLineItem.listPriceCst","");
        component.set("v.enqLineItem.basePrice","");    
        component.set("v.enqLineItem.listPriceCst","");    
        
        
        var priceList =[];
        priceList.push('-None-');
        priceList.push('List');
        priceList.push('Tier');
        priceList.push('Special');
        priceList.push('Search');
        component.set("v.enqLineItem.price",priceList);
        component.set("v.enqLineItem.priceRequest","");
        component.set("v.enqLineItem.sp_requestQuanitity","");
        component.set("v.enqLineItem.sp_priceRequestCst","");
        component.set("v.enqLineItem.validFrom","");
        component.set("v.enqLineItem.validTo","");
        component.set("v.enqLineItem.UOM","");
        component.set("v.enqLineItem.Standardpriceperunit","");	
        
        var CategoryChange = component.get("c.getCategoryChange");
        var categoryName = event.getSource().get("v.value"); 
        
        
        console.log("The coming categoryNameis :"+categoryName);
        CategoryChange.setParams({"categoryName":categoryName});
        
        var thicknessList =[];
        //  thicknessList.push('-None-');
        component.set("v.enqLineItem.thickness",thicknessList);
        
        
        console.log('product change coming');
        var ProductChange2 = component.get("c.getProductChange");
        var productName = event.getSource().get("v.value");
        var editmode = component.get("v.showEdit");
        if(editmode == "false")
            productName = prdName;
        
        
        
        ProductChange2.setParams({"productName":productName});
        ProductChange2.setCallback(this,function(a){
            var thick =[];
            thick.push(a.getReturnValue()[0]);
            component.set("v.thicktemp",a.getReturnValue()[0]);
            //  alert(a.getReturnValue()[0]+'....'+component.get("v.thicktemp"));
            //    var uomVar =[];
            //    uomVar.push(a.getReturnValue()[1]);
            
            component.set("v.enqLineItem.thickness",thick);
            component.set("v.enqLineItem.UOM",a.getReturnValue()[1]);
            
        });
        $A.enqueueAction(ProductChange2);   
        
        var length =[];
        var width = [];        
      //  var categoryVal = component.get("v.EnuLineItemRec.category3");
         var categoryVal = component.find("prdCat").get("v.value");
       // alert('The categoryVal is :'+categoryVal);
        var LengthAndWidth = component.get("c.getLengthAndWidth");
        LengthAndWidth.setParams({"productName":productName,"category":categoryVal});
        LengthAndWidth.setCallback(this,function(a){
            var allLengthWidths = [];    
            var allLenghts = [];
            var allWidths = [];
            allLenghts.push('-None-');
            allWidths.push('-None-');
            allLengthWidths.push('-None-');
            for(var i=0;i<a.getReturnValue().length;i++)
            {
                console.log('The vales are :'+a.getReturnValue()[i].substr(0,a.getReturnValue()[i].indexOf('-')));
                allLenghts.push(a.getReturnValue()[i]);
                allWidths.push(a.getReturnValue()[i].split('-')[1]);
                allLengthWidths.push(a.getReturnValue()[i]);
                
            }
            console.log('The coming allLenghtsis:'+allLenghts.sort());
            component.set("v.enqLineItem.length",allLenghts.sort());
            component.set("v.enqLineItem.width",allWidths.sort());
            
            
            console.log('the query :' +component.get("v.enqLineItem.length"));

        });
        $A.enqueueAction(LengthAndWidth);   
        
        
        var PackType = component.get("c.getPackType");
        PackType.setParams({"productName":productName});
        PackType.setCallback(this,function(a){
            component.set("v.enqLineItem.packTypeTons",a.getReturnValue());
            this.hideSpinner(component, event);
                                        component.set("v.EnuLineItemRec.Price__c","-None-");
            
         //   var cmpEvent = component.getEvent("enqCompPrdChgEvt");         
          //  cmpEvent.setParams({"serNo" : component.get("v.rowIndex")}); 
          //  cmpEvent.fire(); 

        });
        $A.enqueueAction(PackType);   
        
        /*********
        var getUOM = component.get("c.getUOM");
        getUOM.setParams({"productName":productName});
        getUOM.setCallback(this,function(a){
          component.set("v.enqLineItem.UOM",a.getReturnValue());
            alert(JSON.stringify(a.getReturnValue())+'...'+productName);
         });
       $A.enqueueAction(getUOM);   
    ********************/
        
      /******** Getting the Last invoice rate here only for CLEAR , PARSOL and INSPIRE categeory************/
        
		var prdCatVal = '';
		if(component.find("prdCat")) {
                prdCatVal = component.find("prdCat").get("v.value");
            }
			
	if(prdCatVal =='CLEAR' || prdCatVal=='PARSOL' || prdCatVal=='INSPIRE')	
    {	
         var billToCustomerId =  component.get("v.accid");
	  var InvRate = component.get("c.displayRecentInvoiceRate");
        InvRate.setParams({"accId":billToCustomerId,"productName":productName});
        InvRate.setCallback(this,function(a){
          //  alert('coming here'+a.getReturnValue());
            component.set("v.enqLineItem.LstInvRate",a.getReturnValue());
            this.hideSpinner(component, event);
        });
        $A.enqueueAction(InvRate);  
        
     }
        
        
    },
    onPriceChangeHelper: function(component,event,priceType2)
    {
        this.showSpinner(component, event);
        //  alert('the component coming:');
        var orderQuanitity2 = component.get("v.enqLineItem.orderQuanitity");
        var priceType = event.getSource().get("v.value");
        if(!priceType)
            priceType = priceType2;  
        var regionName = component.get("v.region");    
    // alert('before');		
        var prdIdVar = component.find("prdName");
        var prdId = prdIdVar.get("v.value");
        var prdCatVar = component.find("prdCat");
        var prdCat = prdCatVar.get("v.value");
	//	alert('after');
        //  alert('the component coming:');
        console.log('The prdId is :'+prdId+'....'+regionName+'....'+priceType);
        //  alert('The priceType is :'+priceType);
        //  
      
        if(priceType == 'List')
        {           
            var onPriceChangeMethod = component.get("c.getonPriceChange");
            onPriceChangeMethod.setParams({"productId":prdId,"region":regionName,"priceType":priceType,"prdCat":prdCat,"accountid":null});       
            onPriceChangeMethod.setCallback(this,function(a){
                // alert(JSON.stringify(a.getReturnValue()));
                component.set("v.enqLineItem.listPriceCst",a.getReturnValue());      
                
                component.set("v.enqLineItem.sp_requestQuanitity","N/A");
                component.set("v.enqLineItem.sp_priceRequestCst","N/A");
                component.set("v.enqLineItem.validFrom","N/A");
                component.set("v.enqLineItem.validTo","N/A");
                
                // formulas here. 
                // 1. standard price per unit = list price/thickness.
                //  var thicknessInNumber = component.find("prdThick").get("v.value").replace('-mm', '');
                var thicknessInNumber = component.get("v.enqLineItem.thickness[0]").replace('-mm', '');
                var totalVal = a.getReturnValue()*thicknessInNumber;
                console.log('The totalValis :'+totalVal);
                component.set("v.enqLineItem.Standardpriceperunit",totalVal);
                
                //  2. setting base price = standard price per unit * quantiity in Sq mtrs.
                //    SQM = Tons * 400 / thickness in mm
                //    base price = standard price per unit * SQM
                var SQM = component.get("v.enqLineItem.orderQuanitity") * 400 / thicknessInNumber;
                var basePr = totalVal*SQM;
                component.set("v.enqLineItem.basePrice",basePr);
                this.hideSpinner(component, event);
            });
            $A.enqueueAction(onPriceChangeMethod);           
        }
        else
            if(priceType == 'Tier')
            {           
                var accid = component.get("v.accid");
                // alert('the accid is '+accid);
                var salearea = component.get("v.salearea");
                console.log('The coming regionName is :'+regionName);
                var onPriceChangeMethod2 = component.get("{!c.getonPriceChange}");
                
                onPriceChangeMethod2.setParams({"productId":prdId,"region":regionName,"priceType":priceType,"prdCat":prdCat,"accountid":accid});       
                onPriceChangeMethod2.setCallback(this,function(a){
                    // alert(JSON.stringify(a.getReturnValue()));
                    var returnval2 = JSON.stringify(a.getReturnValue());
                    console.log('the exception 3rd');
                    if(a.getReturnValue()!= "")
                    {
                        console.log('the inside exception 3rd');
                        component.set("v.enqLineItem.listPriceCst",a.getReturnValue());  
                        // formulas here. 
                        // 1. standard price per unit = list price/thickness.
                        //  var thicknessInNumber = component.find("prdThick").get("v.value").replace('-mm', '');
                        var thicknessInNumber = component.get("v.enqLineItem.thickness[0]").replace('-mm', '');
                        var totalVal = a.getReturnValue()*thicknessInNumber;
                        console.log('The totalValis :'+totalVal);
                        component.set("v.enqLineItem.Standardpriceperunit",totalVal);
                        
                        //  2. setting base price = standard price per unit * quantiity in Sq mtrs.
                        //    SQM = Tons * 400 / thickness in mm
                        //    base price = standard price per unit * SQM
                        var SQM = component.get("v.enqLineItem.orderQuanitity") * 400 / thicknessInNumber;
                        var basePr = totalVal*SQM;
                        component.set("v.enqLineItem.basePrice",basePr.toFixed(2));
                        this.hideSpinner(component, event);
                    }
                    else
                    {
                        this.hideSpinner(component, event);
                        alert('No Tier pricing records found for this product: ');
                        component.set("v.EnuLineItemRec.Price__c","-None-");
                        component.set("v.enqLineItem.listPriceCst","");   
                        component.set("v.enqLineItem.basePrice","");
                        component.set("v.enqLineItem.Standardpriceperunit","");
                    }
                    
                    
                });
                $A.enqueueAction(onPriceChangeMethod2);     
                
                component.set("v.enqLineItem.sp_requestQuanitity","N/A");
                component.set("v.enqLineItem.sp_priceRequestCst","N/A");
                component.set("v.enqLineItem.validFrom","N/A");
                component.set("v.enqLineItem.validTo","N/A");
            }
            else
                if(priceType == 'Search')
                {  this.showSpinner(component, event);
                    var pricetype ='Tier';
					var accid = component.get("v.accid");
                    var onPriceChangeMethod = component.get("c.getonPriceChange");
                    onPriceChangeMethod.setParams({"productId":prdId,"region":regionName,"priceType":pricetype,"prdCat":prdCat,"accountid":accid});       
                    onPriceChangeMethod.setCallback(this,function(a){
                        // alert(JSON.stringify(a.getReturnValue()));
						var returnval2 = a.getReturnValue();

			         if(returnval2 != "")
					{
                        component.set("v.enqLineItem.listPriceCst",a.getReturnValue());      
                        
                        // formulas here. 
                        // 1. standard price per unit = list price/thickness.
                        //  var thicknessInNumber = component.find("prdThick").get("v.value").replace('-mm', '');
                        var thicknessInNumber = component.get("v.enqLineItem.thickness[0]").replace('-mm', '');
                        var totalVal = a.getReturnValue()*thicknessInNumber;
                        console.log('The totalValis :'+totalVal);
                        component.set("v.enqLineItem.Standardpriceperunit",totalVal.toFixed(2));
                        
                        //  2. setting base price = standard price per unit * quantiity in Sq mtrs.
                        //    SQM = Tons * 400 / thickness in mm
                        //    base price = standard price per unit * SQM
                        var SQM = component.get("v.enqLineItem.orderQuanitity") * 400 / thicknessInNumber;
                        var basePr = totalVal*SQM;
                        component.set("v.enqLineItem.basePrice",basePr.toFixed(2));
                        this.hideSpinner(component, event);
						this.displayPriceTypeisSearch(component,event);
					}
                      else
                          {
                           alert('No Tier pricing records found for this product: ');
						   component.set("v.EnuLineItemRec.Price__c","-None-");
                        component.set("v.enqLineItem.listPriceCst","");   
                        component.set("v.enqLineItem.basePrice","");
						   this.hideSpinner(component, event);
                            }						  
                    });
                    $A.enqueueAction(onPriceChangeMethod);  
                    
                    
                }
                else if(!orderQuanitity2)
                {
                    alert('order quanitity should not be blank');
					this.hideSpinner(component, event);
                }
                    else
                        if(priceType == 'Special' && (prdCat=='CLEAR' || prdCat == 'INSPIRE' ||prdCat == 'PARSOL' ))
                        {
                            
                            this.showSpinner(component, event);
                            var pricetype ='Tier';
							var accid = component.get("v.accid");
                            var onPriceChangeMethod = component.get("c.getonPriceChange");
                          
						  
						  onPriceChangeMethod.setParams({"productId":prdId,"region":regionName,"priceType":pricetype,"prdCat":prdCat,"accountid":accid});       
                            onPriceChangeMethod.setCallback(this,function(a){
                                // alert(JSON.stringify(a.getReturnValue()));
								var returnval2 = a.getReturnValue();
                                
			        if(returnval2 != "")
					 {
                          
                                component.set("v.enqLineItem.listPriceCst",a.getReturnValue());      
                                
                                
                                
                                // formulas here. 
                                // 1. standard price per unit = list price/thickness.
                                //  var thicknessInNumber = component.find("prdThick").get("v.value").replace('-mm', '');
                                var thicknessInNumber = component.get("v.enqLineItem.thickness[0]").replace('-mm', '');
                                var totalVal = a.getReturnValue()*thicknessInNumber;
                                console.log('The totalValis :'+totalVal);
                                component.set("v.enqLineItem.Standardpriceperunit",totalVal);
                                
                                //  2. setting base price = standard price per unit * quantiity in Sq mtrs.
                                //    SQM = Tons * 400 / thickness in mm
                                //    base price = standard price per unit * SQM
                                var SQM = component.get("v.enqLineItem.orderQuanitity") * 400 / thicknessInNumber;
                                var basePr = totalVal*SQM;
                                component.set("v.enqLineItem.basePrice",basePr);
                                
                                component.set("v.pr1.Requested_Quantity__c",component.get("v.enqLineItem.orderQuanitity"));
                                this.hideSpinner(component, event);
								this.displayPriceTypeisSpecial(component,event); 
							}	
							  else
							   {
							     alert('No Tier pricing records found for this product: ');
							component.set("v.EnuLineItemRec.Price__c","-None-");
                        component.set("v.enqLineItem.listPriceCst","");   
                        component.set("v.enqLineItem.basePrice","");
								  this.hideSpinner(component, event);
							   }
                            });
                            $A.enqueueAction(onPriceChangeMethod);  
                            component.set("v.Sales_OfficeList",component.get("v.region"));
                            // component.set("v.defaultSalesOffice",component.get("v.region"));
                            
                            
                        }  //  //(prdCat.equalsIgnoreCase('CLEAR') && prdCat.equalsIgnoreCase('Interior'))
                        else if(priceType == 'Special' &&  (prdCat != 'CLEAR' && prdCat != 'INSPIRE' && prdCat != 'PARSOL' ))
                        {
                            this.hideSpinner(component, event);
                            alert('Price type special is applicable only if the categeory is CLEAR,INSPIRE OR PARSOL');
                           component.set("v.EnuLineItemRec.Price__c","-None-");
                        }
        
                            else
                                if(priceType == '-None-')
                                {                   
                                    this.hideSpinner(component, event);
                                    component.set("v.enqLineItem.priceRequest","");
                                    component.set("v.enqLineItem.listPriceCst","");  
                                    component.set("v.enqLineItem.sp_requestQuanitity","");
                                    component.set("v.enqLineItem.sp_priceRequestCst","");
                                    component.set("v.enqLineItem.validFrom","");
                                    component.set("v.enqLineItem.validTo","");
                                    component.set("v.enqLineItem.UOM","");
                                    component.set("v.enqLineItem.Standardpriceperunit","");	
                                    component.set("v.enqLineItem.basePrice","");
                                }
    },
    displayPriceTypeisSearch :function(component,event)
    {
        component.set("v.enqLineItem.sp_requestQuanitity","");
        component.set("v.enqLineItem.sp_priceRequestCst","");
        component.set("v.enqLineItem.validFrom","");
        component.set("v.enqLineItem.validTo","");
        
	
        var prdName = component.find("prdName").get("v.value");
        var salesArea = component.get("v.region");
                var prdCategory = component.get("v.enqLineItem.category3");

        //  alert(prdName+'....'+salesArea);
        component.set("v.prMapData","false");
        var getPriceRequestOnSearch = component.get("c.getPriceRequestOnSearch");
		var billToCustomerId =  component.get("v.accid");
        getPriceRequestOnSearch.setParams({"prdName":prdName,"salesArea":salesArea,"billToCustomerId":billToCustomerId,"prdCategory":prdCategory});
        getPriceRequestOnSearch.setCallback(this,function(a){
            component.set("v.priceRequestOnSearch",a.getReturnValue());
            console.log('The price request is :'+JSON.stringify(a.getReturnValue()));
            if(a.getReturnValue()['success'])
            {
                component.set('v.priceRequestOnSearch',a.getReturnValue()['success']);
                var prMap ={};
                for(var i=0;i<a.getReturnValue()['success'].length;i++)
                {
                    prMap[a.getReturnValue()['success'][i].Name] = a.getReturnValue()['success'][i];
                }
                
                console.log('The prMapis123123:'+prMap);
                component.set("v.prMap",prMap);
                component.set("v.prMapData","true");
                // for Display Model,set the "isOpen" attribute to "true"
                component.set("v.isOpen", true);   
            }
            else if(a.getReturnValue()['failure'])
            {
                alert('No records Found with this cirteria');    
              //  component.set("v.enqLineItem.priceRequest","");
			  		component.set("v.EnuLineItemRec.Price__c",'-None-');

            }
                else
                {
                    alert(JSON.stingify(a.getReturnValue())); 
                  //  component.set("v.enqLineItem.priceRequest","");
                    
                }   
        });
        
        $A.enqueueAction(getPriceRequestOnSearch);
    },
    
    displayPriceTypeisSpecial :function(component,event)
    {
        // alert(' i am calling 12');
        // setting the piclist values for new price request creation. 
        var priceReq = component.get("c.GetFieldPikclistValues");
        priceReq.setParams({"ObjectApi_name":"price_request_PandP__c","picklistField":"Price_Type__c"});
        priceReq.setCallback(this,function(a){
            //   component.set("v.priceList2",a.getReturnValue());
            console.log('the test is :'+JSON.stringify(a.getReturnValue()));
            component.set("v.priceList2","Special Price");
            var prdCat='';
            if(component.find("prdCat"))
             prdCat = component.find("prdCat").get("v.value");
             component.set("v.Product_CategoryList",prdCat);
        });
        $A.enqueueAction(priceReq);
        
        var Sales_Office = component.get("c.GetFieldPikclistValues");
        Sales_Office.setParams({"ObjectApi_name":"price_request_PandP__c","picklistField":"Sales_Office__c"});
        Sales_Office.setCallback(this,function(a){
            component.set("v.Sales_OfficeList",a.getReturnValue());
            console.log('the test is :'+JSON.stringify(a.getReturnValue()));
            
            
        });
        // We need to auto poluate the sales office. So we are commented this action below. 
        //   $A.enqueueAction(Sales_Office);
        
        
        
        var Product_Category = component.get("c.GetFieldPikclistValues");
        Product_Category.setParams({"ObjectApi_name":"price_request_PandP__c","picklistField":"Product_Category__c"});
        Product_Category.setCallback(this,function(a){
          //  component.set("v.Product_CategoryList",a.getReturnValue());
            console.log('the test is :'+JSON.stringify(a.getReturnValue()));
            
        });
      //  $A.enqueueAction(Product_Category);
        
        
        
        
        component.set("v.enqLineItem.sp_requestQuanitity","");
        component.set("v.enqLineItem.sp_priceRequestCst","");
        component.set("v.enqLineItem.validFrom","");
        component.set("v.enqLineItem.validTo","");
        
        var prdName = component.find("prdName").get("v.value");
        var salesArea = component.get("v.region");
        var billtoCust = component.get("v.accid");
        var prdCategory = component.get("v.enqLineItem.category3");
        
       
        //  alert(prdName+'....'+salesArea);
         component.set("v.prMapData","false");
        var getPriceRequestOnSearch = component.get("c.getPriceRequestOnSearch");
        getPriceRequestOnSearch.setParams({"prdName":prdName,"salesArea":salesArea,"billToCustomerId":billtoCust,"prdCategory":prdCategory});
        getPriceRequestOnSearch.setCallback(this,function(a){
            component.set("v.priceRequestOnSearch",a.getReturnValue());
            console.log('The price request is :'+JSON.stringify(a.getReturnValue()));
            if(a.getReturnValue()['success'])
            {
                component.set('v.priceRequestOnSearch',a.getReturnValue()['success']);
                var prMap ={};
                for(var i=0;i<a.getReturnValue()['success'].length;i++)
                {
                    prMap[a.getReturnValue()['success'][i].Name] = a.getReturnValue()['success'][i];
                }
                console.log('The prMapis123123:'+JSON.stringify(prMap));
                component.set("v.prMap",prMap);
                 component.set("v.prMapData","true");
                // for Display Model,set the "isOpen" attribute to "true"
                component.set("v.isOpenNew", true);   
            }
            else if(a.getReturnValue()['failure'])
            {
              //  alert('No records Found with this cirteria');    
                component.set("v.isOpenNew", true);   
                component.set("v.enqLineItem.priceRequest","");
            }
                else
                {
                    alert(JSON.stingify(a.getReturnValue())); 
                    component.set("v.enqLineItem.priceRequest","");
                }   
            
          

        });
          // setting up the quanitity in sqmtrs value in price request popup. 
             var quantityInTons = component.get("v.enqLineItem.orderQuanitity");
         var thicknessInNumber = component.get("v.enqLineItem.thickness[0]").replace('-mm', '');
        var quantityInSqMt = (quantityInTons * 400 / thicknessInNumber).toFixed(2);
                    component.set("v.RequestedQuantSqmt",quantityInSqMt);
        
        $A.enqueueAction(getPriceRequestOnSearch);
    },
    //inputText
    checkdate :function(component,event,inputText)
    {
        var dateformat = /^(0?[1-9]|[12][0-9]|3[01])[\/\-](0?[1-9]|1[012])[\/\-]\d{4}$/;
        // Match the date format through regular expression
        if(inputText.value.match(dateformat))
        {
            //document.form1.text1.focus();
            //Test which seperator is used '/' or '-'
            var opera1 = inputText.value.split('/');
            var opera2 = inputText.value.split('-');
            lopera1 = opera1.length;
            lopera2 = opera2.length;
            // Extract the string into month, date and year
            if (lopera1>1)
            {
                var pdate = inputText.value.split('/');
            }
            else if (lopera2>1)
            {
                var pdate = inputText.value.split('-');
            }
            var dd = parseInt(pdate[0]);
            var mm  = parseInt(pdate[1]);
            var yy = parseInt(pdate[2]);
            // Create list of days of a month [assume there is no leap year by default]
            var ListofDays = [31,28,31,30,31,30,31,31,30,31,30,31];
            if (mm==1 || mm>2)
            {
                if (dd>ListofDays[mm-1])
                {
                    alert('Invalid date format!');
                    return false;
                }
            }
            if (mm==2)
            {
                var lyear = false;
                if ( (!(yy % 4) && yy % 100) || !(yy % 400)) 
                {
                    lyear = true;
                }
                if ((lyear==false) && (dd>=29))
                {
                    alert('Invalid date format!');
                    return false;
                }
                if ((lyear==true) && (dd>29))
                {
                    alert('Invalid date format!');
                    return false;
                }
            }
        }
        else
        {
            alert("Invalid date format!");
            // document.form1.text1.focus();
            return false;
        }
    },
    deleteRecordHelper :function(component,event)
    {
        
this.showSpinner(component, event);
        var RecId = component.get("v.LineItemrecId");
        var deleteEnqLineItem = component.get("c.deleteEnqLineItem");
        deleteEnqLineItem.setParams({"RecId":RecId});
        deleteEnqLineItem.setCallback(this,function(a){
            console.log('record deleted '+JSON.stringify(a.getReturnValue()));
            $A.get('e.force:refreshView').fire();
            this.hideSpinner(component, event);
        });
        $A.enqueueAction(deleteEnqLineItem);
    },
    showSpinner : function(component,event){
        component.set("v.toggleSpinner", true);  
    },
    
    hideSpinner : function(component,event){
        component.set("v.toggleSpinner", false);
    },
    savePrRecHelper :function(component,event,approvalLevel,tierPrice)
    { 
	// alert('coming into the helper');
       var pr1 = component.get("v.pr1");
             var enqRecId = component.get("v.EnqRecId");
         var selectedProduct = component.get("v.enqLineItem.product3");
        var priceType = component.find("priceTypeId").get("v.value");
       priceType = 'Tier';
        var Sales_Office = '';
        var isORSVal = component.get("v.isORS");
        if(isORSVal == 'NOT ORS')
         Sales_Office =  component.get("v.region");
        else
          Sales_Office = '';  
        console.log('the Sales_Office is :'+Sales_Office);
        var accid = component.get("v.accid");
       
        var Product_Category='';
            if(component.find("prdCat"))
             Product_Category = component.find("prdCat").get("v.value");
           
       var selectedCustomerRec = component.get("v.billToStrName");
        var fromDate = component.get("v.pr1.Valid_From__c");
        var Todate = component.get("v.pr1.Valid_To__c");
        var requiredPrice = component.get("v.pr1.Required_Price__c");
        var remarks =  component.get("v.pr1.Remarks__c");
         
        
		var prdlength ='';
		if(component.find("prdlength")) {
             prdlength = component.find("prdlength").get("v.value");
			  }
		
      // get the Min and Max price for the selected product. 
	  
	  
         var accid = component.get("v.accid");
                // alert('the accid is '+accid);
				 var regionName = component.get("v.region"); 
                var salearea = component.get("v.salearea");
                console.log('The coming regionName is :'+regionName);
               
                
		var orderQuanitity = component.get("v.enqLineItem.orderQuanitity");
        var quantityInTons = component.get("v.enqLineItem.orderQuanitity");
        var thicknessInNumber = component.get("v.enqLineItem.thickness[0]").replace('-mm', '');
        var quantityInSqMt = quantityInTons * 400 / thicknessInNumber;
                
        var lstrpice = component.get("v.enqLineItem.listPriceCst");
		
				
		var prdIdVar = component.find("prdName");
        var prdId = prdIdVar.get("v.value");
        var prdCatVar = component.find("prdCat");
        var prdCat = prdCatVar.get("v.value");
		
		
		
           
        
        var requiredPriceFloat = parseFloat(requiredPrice).toFixed(2);
       
        var quantityInSqMt = quantityInTons * 400 / thicknessInNumber;
              
            var savePRrecord = component.get("c.savePRrecord");
            var priceType ='Special Price';
            savePRrecord.setParams({"PRrec":component.get("v.pr1"),"priceType":priceType,"Sales_Office":Sales_Office,"Product_Category":Product_Category,
                                    "selectedCustomerRec":accid,"fromDate":fromDate,"Todate":Todate,"requiredPrice":requiredPrice,
                                    "remarks":remarks,"enqRecId":enqRecId,"selectedProduct":selectedProduct,"quantityInSqMt":quantityInSqMt,"requiredPriceFloat":requiredPriceFloat,"productLenghtWidth":prdlength,"approvalLevel":approvalLevel,"tierPrice":tierPrice});
            
        {
            savePRrecord.setCallback(this,function(a){   
          
                component.set("v.enqLineItem.priceRequest",a.getReturnValue());
                component.set("v.enqLineItem.sp_requestQuanitity",quantityInSqMt);
                component.set("v.enqLineItem.Required_Price_per_Sq_M",requiredPrice);
                component.set("v.isOpen", false);
                console.log('the exception 2nd');
                if(JSON.stringify(a.getReturnValue()) != "" && !JSON.stringify(a.getReturnValue()).includes('Exception'))
                {
                    console.log('the exception 2nd');
                    component.set("v.isOpenSpecial",false);
                    component.set("v.displayAddRow",true);   
                    //alert('the val :'+component.get("v.pr1.Valid_From__c"));
                    component.set("v.enqLineItem.validTo",component.get("v.pr1.Valid_To__c"));
                    component.set("v.enqLineItem.validFrom",component.get("v.pr1.Valid_From__c"));
                    this.hideSpinner(component, event);
               
                if(requiredPrice)    
                component.set("v.enqLineItem.Required_Price_per_Sq_M",requiredPrice);
                }
                else
                {
                   this.hideSpinner(component, event);
                    alert('Exception came: '+JSON.stringify(a.getReturnValue()));
                }    
            });  
            
            $A.enqueueAction(savePRrecord);
        }
	}
  /*******************************************************************************************  
    OnlenghtChangeHelper: function(component,event,lenghtVal){
          var serverCall = component.get("c.displayProductstockInformation");
        var prddimens = event.getSource().get("v.value");
        var prdName =  component.get("v.enqLineItem.product3");
     // alert('The prddimens is :'+prddimens+'.....'+prdName);
     
       serverCall.setParams({"productName":prdName,"ProductDimentions":prddimens});
        serverCall.setCallback(this,function(a){
                                      
                     // alert(JSON.stringify(a.getReturnValue()));                    
            component.set("v.stockDetails",a.getReturnValue());     
                      
        });
        $A.enqueueAction(serverCall);
        
    }
   *******************************************************************************************/
})
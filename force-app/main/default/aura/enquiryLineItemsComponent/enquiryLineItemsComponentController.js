({
    onloadingmethod :function(component,event,helper)
    {
        
        //alert('The foundDupProdSerialNo is:');
        // setting the LineItemrecId from the wrapper class. 
        var existingRecId = component.get("v.enqLineItem.recordId");   
        if(existingRecId)
            component.set("v.LineItemrecId",existingRecId);
        else
            component.set("v.showEdit","true");
        /***   
        // seeting up the defaul enquiry__c record data.
        var existingRec = component.set("v.enqLineItem");
         var enqSobjectRec = [];
        enqSobjectRec.push({'sobjectType':'Enquiry_Line_Item__c','Name':'Test'+i});
        *****/
        
        // loading empty record if showEdit is new and record id is false.
        var showEditVal = component.get("v.showEdit");
        if(existingRecId && showEditVal =='new')
        {
            // getting the default empty row for enquiry line item.
            var wraperdata = component.get("c.getEmptyLineItem");
            wraperdata.setCallback(this,function(a){
                component.set("v.enqLineItem",a.getReturnValue());
                //  alert(JSON.stringify(a.getReturnValue()));
            });
           $A.enqueueAction(wraperdata);
        }  
        var priceVar = '';
        if(component.find("priceVar"))
            priceVar = component.find("priceVar").get("v.value");
        else
            var priceVar = component.get("v.enqLineItem.price3");
        // alert('The priceVar is :'+priceVar);
        if(priceVar =='Search' || priceVar =='Special')
        {
            // component.set("v.enqLineItem.sp_requestQuanitity","");
            //  component.set("v.enqLineItem.sp_priceRequestCst","");
            // component.set("v.enqLineItem.validTo",""); 
         
          
          //      component.set("v.today",today.getFullYear() + "-" + monthDigit + "-" + today.getDate());
           
        }    
    },
    
    onCategoryChange :function(component,event,helper)
    {
      //  alert('category change');
        var categoryVal = event.getSource().get("v.value"); 
       // component.set("v.EnuLineItemRec.Product_category__c",categoryVal);
        // component.set("v.enqLineItem.category4",categoryVal);
      // alert(component.get("v.enqLineItem.category4")+'....'+categoryVal);
      component.set("v.enqLineItem.LstInvRate",''); 
        helper.onCategoryChangeHelper(component,event);
        
    }, 
    
    onResetClick :function(component,event,helper)
    {
        var message = event.getParam("message");
        var rowIndex = component.get("v.rowIndex");
        if(message == rowIndex)
        {
        component.find("prdCat").set("v.value","-None-");
        component.find("prdName").set("v.value","-None-");
            component.set("v.enqLineItem.LstInvRate","-None-");
       //  component.find("prdThick").set("v.value","-None-");    
        
          var thicknessList =[];
        thicknessList.push('-None-');
        component.set("v.enqLineItem.thickness",thicknessList);   
        var lengthList =[];
        lengthList.push('-None-');
        component.set("v.enqLineItem.thickness",lengthList); 
        component.set("v.enqLineItem.length",lengthList);
         component.set("v.enqLineItem.product",lengthList); 
        var widthList =[];
        widthList.push('-None-');
        component.set("v.enqLineItem.width",widthList);  
        var packTypeList =[];
        packTypeList.push('-None-');        
        component.set("v.enqLineItem.packTypeTons",packTypeList); 
        component.set("v.enqLineItem.cases","");
        var orderQuanitityList =[];
        orderQuanitityList.push('-None-');
        component.set("v.enqLineItem.orderQuanitity",orderQuanitityList); 	
            
            
            
           component.find("prdlength").set("v.value","-None-"); 
             component.find("packTypeTons").set("v.value","-None-"); 
             component.find("priceVar").set("v.value","-None-"); 
          
            
            component.set("v.enqLineItem.basePrice","");
            component.set("v.enqLineItem.Standardpriceperunit","");
            component.set("v.enqLineItem.priceRequest","");
            component.set("v.enqLineItem.sp_requestQuanitity","");
            component.set("v.enqLineItem.sp_priceRequestCst","");
            component.set("v.enqLineItem.Required_Price_per_Sq_M","");
            component.set("v.enqLineItem.validFrom","");
            component.set("v.enqLineItem.validTo","");
            component.set("v.enqLineItem.LstInvRate","");
            component.set("v.enqLineItem.UOM","");
            component.set("v.enqLineItem.cases","");
            
            
        }
       // alert('The app event:'+message);
    },
    
    onProductChange :function(component,event,helper)
    {
       // alert('coming here:');
          var productVal = event.getSource().get("v.value"); 
          component.set("v.EnuLineItemRec.Product_Name__c",productVal);
         component.set("v.enqLineItem.product3",productVal);
        var categoryVal = component.find("prdCat").get("v.value");
        component.set("v.enqLineItem.category3",categoryVal);
        if(productVal != '-None-')
        helper.onProductChangeHelper(component,event);
        
       //  var cmpEvent = component.getEvent("enqCompPrdChgEvt");         
       // cmpEvent.setParams({"serNo" : component.get("v.rowIndex")}); 
       // cmpEvent.fire(); 
    },
    
    resetDupPrd :function(component,event,helper)
    {
       // var args = event.getParam("arguments");
       // var Serno = args.serialNo;
       // alert(Serno);
    },
    
    stockChange :function(component,event,helper)
    {
        //var stockVal = component.find("stockChange");
        //stockVal.set("v.value","-None-");
        component.set("v.StockVal","-none-");
    },
    
    lenghtChange :function(component,event,helper)
    {
        var lenghtVal = event.getSource().get("v.value"); 
         component.set("v.EnuLineItemRec.Product_Length__c",lenghtVal);
         component.set("v.enqLineItem.length3",lenghtVal);
     /**********************************  Validation for dup product + category + product dimention **********************   
        var cmpEvent = component.getEvent("enqCompPrdChgEvt");         
            cmpEvent.setParams({"serNo" : component.get("v.rowIndex")}); 
            cmpEvent.fire(); 
        **********************************/
      //  helper.lenghtwidthchangehelper(component,event,lenghtVal);
    //  helper.OnlenghtChangeHelper(component,event,lenghtVal);
    },
    
    onStockDetails : function(component,event,helper)
    {
        
            var serverCall = component.get("c.displayProductstockInformation");
        var prddimens = component.get("v.enqLineItem.length3");
        var prdName =  component.get("v.enqLineItem.product3");
        if(!prddimens || !prdName)
            alert('Please select product & its dimentions');
        else{
     // alert('The prddimens is :'+prddimens+'.....'+prdName);
     
       serverCall.setParams({"productName":prdName,"ProductDimentions":prddimens});
        serverCall.setCallback(this,function(a){
            console.log('The ret val is:'+a.getReturnValue());
                     // alert(JSON.stringify(a.getReturnValue()));                    
            component.set("v.stockDetails",a.getReturnValue());  
            component.set("v.isOpenStockDetails",true);
                      
        });
        $A.enqueueAction(serverCall);
        }
    },
    
    OnPackTypeChange :function(component,event,helper)
    {
        var changedPackType = event.getSource().get("v.value");
        console.log('The changedpacktype is '+changedPackType);
         
           component.set("v.EnuLineItemRec.Pak_Type_Tons__c",changedPackType);
        var cases = component.get("v.enqLineItem.cases");
        if(isNaN(cases))
        {
            alert('Please enter only number in cases field');
            component.set("v.enqLineItem.cases","");
        }
        else
        {
        if(cases)
            component.set("v.enqLineItem.orderQuanitity",(changedPackType*cases).toFixed(2));
        
              var lstPriceCST = component.get("v.enqLineItem.listPriceCst");
                     if(lstPriceCST)
                     {				
                        // formulas here. 
                        // 1. standard price per unit = list price/thickness.
                        var thicknessInNumber = component.get("v.enqLineItem.thickness[0]").replace('-mm', '');
                        var totalVal = lstPriceCST*thicknessInNumber;
                        console.log('The totalValis :'+totalVal);
                        component.set("v.enqLineItem.Standardpriceperunit",totalVal.toFixed(2));
                        
                        //  2. setting base price = standard price per unit * quantiity in Sq mtrs.
                        //    SQM = Tons * 400 / thickness in mm
                        //    base price = standard price per unit * SQM
                        var SQM = component.get("v.enqLineItem.orderQuanitity") * 400 / thicknessInNumber;
                        var basePr = totalVal*SQM;
                        component.set("v.enqLineItem.basePrice",basePr.toFixed(2));
					 }	
        }
    },
    
    onOrderQuanitityChange :function(component,event,helper)
    {
        var casesCount = event.getSource().get("v.value");
        var changedPackType = component.find("packTypeTons").get("v.value");
        console.log('The orderQuanitity is '+casesCount+'.....'+changedPackType);
        
        
        if(isNaN(casesCount))
        {
            alert('Please enter only number in cases field');
			component.set("v.enqLineItem.cases","");
        }
        else
        {
        console.log('the va is:'+changedPackType*casesCount);
        if(casesCount && changedPackType)
            component.set("v.enqLineItem.orderQuanitity",(changedPackType*casesCount).toFixed(2));
        
        
              var lstPriceCST = component.get("v.enqLineItem.listPriceCst");
                     if(lstPriceCST)
                     {				
                        // formulas here. 
                        // 1. standard price per unit = list price/thickness.
                        var thicknessInNumber = component.get("v.enqLineItem.thickness[0]").replace('-mm', '');
                        var totalVal = lstPriceCST*thicknessInNumber;
                        console.log('The totalValis :'+totalVal);
                        component.set("v.enqLineItem.Standardpriceperunit",totalVal.toFixed(2));
                        
                        //  2. setting base price = standard price per unit * quantiity in Sq mtrs.
                        //    SQM = Tons * 400 / thickness in mm
                        //    base price = standard price per unit * SQM
                        var SQM = component.get("v.enqLineItem.orderQuanitity") * 400 / thicknessInNumber;
                        var basePr = totalVal*SQM;
                        component.set("v.enqLineItem.basePrice",basePr.toFixed(2));
					 }	
        
		
		
        }
    },
    
    onPriceChange :function(component,event,helper)
    {
        
        var errorMsgs =[];
            var prdCat = '';
			var prdName = '';
            var prdThick = '';
            var prdlength = '';
            var packTypeTons = '';
            var prdCases = '';
            var UOM = '';
            var prdlistPriceCst = '';
            var orderQuanitity = '';
            var prdbasePrice = '';
			var priceVar='';
            var sp_requestQuanitity = '';
            var sp_priceRequestCst = '';
            	
        	
			 component.set("v.enqLineItem.priceRequest","");
			  component.set("v.enqLineItem.listPriceCst","");
			  component.set("v.enqLineItem.basePrice","");
			  component.set("v.enqLineItem.Standardpriceperunit","");
			  component.set("v.enqLineItem.sp_requestQuanitity","");
			  component.set("v.enqLineItem.Required_Price_per_Sq_M","");
			  component.set("v.enqLineItem.validFrom","");
			  component.set("v.enqLineItem.validTo","");
			  
			
			 // getting the same price request which is matching with same product name. 
		var priceChangeVal = event.getSource().get("v.value");
		if(priceChangeVal=='Special')
		{
		var allLineList = component.get("v.enqLineItemsForChild");
		for(var i=0;i<allLineList.length;i++)
		{
		 var presentPrdName = component.get("v.enqLineItem.product3");
		  var presentRec = allLineList[i];
		//  alert(JSON.stringify(presentRec.product3)+'.....'+JSON.stringify(presentRec.priceRequest));
		  if(presentRec.product3 == presentPrdName && presentRec.priceRequest!='')
		  {
		      component.set("v.enqLineItem.priceRequest",presentRec.priceRequest);
			  component.set("v.enqLineItem.listPriceCst",presentRec.listPriceCst);
			  component.set("v.enqLineItem.basePrice",presentRec.basePrice);
			  component.set("v.enqLineItem.Standardpriceperunit",presentRec.Standardpriceperunit);
			  component.set("v.enqLineItem.sp_requestQuanitity",presentRec.sp_requestQuanitity);
			  component.set("v.enqLineItem.Required_Price_per_Sq_M",presentRec.Required_Price_per_Sq_M);
			  component.set("v.enqLineItem.validFrom",presentRec.validFrom);
			  component.set("v.enqLineItem.validTo",presentRec.validTo);
             
		  }
		}
	 }		
		  //	alert(JSON.stringify(component.get("v.enqLineItem.priceRequest")));
			var prName = component.get("v.enqLineItem.priceRequest");
       
       
        if(!prName && prName!= '')
        {
             helper.onPriceChangeHelper(component,event,priceType);
            console.log('The inside if here :'+prName);
        }   // alert('the pr name:'+prName);
        
        
        
        console.log('no error');
		if(prName=='')
		{
            if(component.find("prdCat")) {
                prdCat = component.find("prdCat").get("v.value");
            }
			console.log('no error2');
			 if(component.find("prdName")) {
             prdName = component.find("prdName").get("v.value");
			 }
			 console.log('no error333');
			  if(component.find("prdThick")) {
             prdThick = component.find("prdThick").get("v.value");
			  }
			 
			  if(component.find("packTypeTons")) {            
             packTypeTons = component.find("packTypeTons").get("v.value");
			  }
			  if(component.find("prdCases")) {
             prdCases = component.find("prdCases").get("v.value");
			  }
			  
			    if(prdCat =='-None-')
                errorMsgs.push('Please select product category');
            if(prdName =='-None-')
                errorMsgs.push('Please select product');            
            if(packTypeTons =='-None-')
                errorMsgs.push('Please select pack Type Tons');
            if(!prdCases)
                errorMsgs.push('Please select cases');        
           
			 if(errorMsgs.length > 0) {  
            	 alert(errorMsgs.join('\n'));	
                  component.set("v.EnuLineItemRec.Price__c","-None-");
			 }
			 else
			 {
                // console.log('We are coming into else');
			     var priceType = event.getSource().get("v.value");
        component.set("v.EnuLineItemRec.Price__c",priceType);
        helper.onPriceChangeHelper(component,event,priceType);
			 }
			 
			 
			}
		
		
      
    },
    saveRec :function(component,event,helper){
	
	var OldSubmittedVal = component.get("v.enqLineItem.OldSubmitted");
	//alert('the OldSubmittedVal is :' +OldSubmittedVal);
	
	if(OldSubmittedVal != true)
	   {
	
        var callSaveAction = component.get("v.callSaveAction");
        if(callSaveAction == "true")
        {
            var rowIndexVal = component.get("v.rowIndex");
         //   alert('value 1'+component.find("prdCat"));
            var returnVal;
            var prdCat = '';
			var prdName = '';
            var prdThick = '';
            var prdlength = '';
            var packTypeTons = '';
            var prdCases = '';
            var UOM = '';
            var prdlistPriceCst = '';
            var orderQuanitity = '';
            var prdbasePrice = '';
			var priceVar='';
            var sp_requestQuanitity = '';
            var sp_priceRequestCst = '';
			 var submitCheck ='';
           var LstInvRate ='';
		     submitCheck =  component.get("v.enqLineItem.submitRec");
            var Required_Price_per_Sq_M = component.get("v.enqLineItem.Required_Price_per_Sq_M");
            
            console.log('no error');
		
            if(component.find("prdCat")) {
                prdCat = component.find("prdCat").get("v.value");
            }
            
            if(component.find("LstInvRate"))
            {
                LstInvRate = component.find("LstInvRate").get("v.value");
            }
			console.log('no error2');
			 if(component.find("prdName")) {
             prdName = component.find("prdName").get("v.value");
			 }
			 console.log('no error3');
			 
			  if(component.find("prdThick")) {
             prdThick = component.find("prdThick").get("v.value");
			  }
			//  alert(prdThick);
			  if(component.find("prdlength")) {
             prdlength = component.find("prdlength").get("v.value");
			  }
			  console.log('no error4');
			  if(component.find("packTypeTons")) {            
             packTypeTons = component.find("packTypeTons").get("v.value");
			  }
			  if(component.find("prdCases")) {
             prdCases = component.find("prdCases").get("v.value");
			  }
			    console.log('no error5');
			  if(component.find("UOM")) {
             UOM = component.find("prdUOM").get("v.value");
			  }
			  if(component.find("prdlistPriceCst")) {
             prdlistPriceCst = component.find("prdlistPriceCst").get("v.value");
			  }
			  if(component.find("prdQty")) {
             orderQuanitity = component.find("prdQty").get("v.value");
			  }
			  if(component.find("prdbasePrice")) {
             prdbasePrice = component.find("prdbasePrice").get("v.value");
			  }
			  if(component.find("priceVar")) {
             priceVar = component.find("priceVar").get("v.value");
			  }
			 // if(component.find("sp_requestQuanitity")) {
             sp_requestQuanitity = component.get("v.enqLineItem.sp_requestQuanitity");
            sp_requestQuanitity = sp_requestQuanitity+'';
			//  }
			  if(component.find("sp_priceRequestCst")) {
             sp_priceRequestCst = component.get("v.enqLineItem.sp_priceRequestCst");
			  }
			  
			 
			 UOM = component.get("v.enqLineItem.UOM");
            console.log('The UOMis :'+UOM);
            var validFrom = component.get("v.enqLineItem.validFrom");
            var validTo = component.get("v.enqLineItem.validTo");
            console.log('the validTo is L'+validTo);
            var remarks = component.get("v.enqLineItem.remarks");        
            var enqRecId = component.get("v.EnqRecId");
            // var priceRequest = component.get("v.enqLineItem.priceRequest");
            var standardPriceUnit = component.get("v.enqLineItem.Standardpriceperunit");
            if(standardPriceUnit)
                standardPriceUnit = standardPriceUnit.toString();
            //  alert(standardPriceUnit);
            //  var Standardpriceperunit = '';
            //  console.log('before :'+prdThick);
            if(!prdThick)
                prdThick = component.get("v.thicktemp");   
			//	alert('The 2nd:'+prdThick);
            // console.log('before :'+prdThick);
            var proccedForSave = 'true';
            var priceRequest = component.get("v.enqLineItem.priceRequest");
            if(!priceRequest)
                priceRequest = '';
            console.log('the prdName is :'+prdName+'.....'+priceRequest+'...'+prdCat+'...'+packTypeTons);
            
            var errorMsgs =[];
            
            /**	       These validations re-written below. 
        if(prdCat =='-None-')
	 alert('please select product categeory');
   else if(prdName =='-None-')
          alert('please select product');
   else if(prdlength =='-None-')
          alert('please select product length and width');
   else if(packTypeTons =='-None-')
          alert('please select pack Type Tons');
   else if(!prdCases)
          alert('please select cases');        
   else if(priceVar =='-None-')
          alert('please select price type');
        ***/
            
            if(prdCat =='-None-')
                errorMsgs.push('Please select product categeory');
            if(prdName =='-None-')
                errorMsgs.push('Please select product');
            if(prdlength =='-None-')
                errorMsgs.push('Please select product length and width');
            if(packTypeTons =='-None-')
                errorMsgs.push('Please select pack Type Tons');
            if(!prdCases)
                errorMsgs.push('Please select cases');        
            if(priceVar =='-None-')
                errorMsgs.push('Please select price type');
            
			
			  if(priceVar =='Search' || priceVar =='Special')
			  {
			    if(!priceRequest)
				 errorMsgs.push('Please select Price request');
			}
            
            /*****
    else if(!sp_requestQuanitity && priceVar == "Search" || priceVar =="Special")    
          alert('Please enter Special request quantity');
     else if(!sp_priceRequestCst && priceVar == "Search" || priceVar =="Special")    
          alert('Please enter Special price request');   
      else if(!validFrom && priceVar == "Search" || priceVar =="Special")    
          alert('Please enter Valid from');    
       else if(!validTo && priceVar == "Search" || priceVar =="Special")    
          alert('Please enter valid to');    
     
         else if(isNaN(sp_requestQuanitity) && priceVar == "Search" || priceVar =="Special")    
         alert('Please enter Special request quantity');
    else if(isNaN(sp_priceRequestCst) && priceVar == "Search" || priceVar =="Special")    
         alert('Please enter Special price request');   
     
****/   
            console.log('The errorMsgs is :' +errorMsgs.length);
            if(errorMsgs.length > 0) {            
                component.getEvent("controlSaveEdit").setParams({"errorStatus": false,"rowIndexVal":rowIndexVal}).fire();
                
                
                alert(errorMsgs.join('\n'));
                console.log('The errorMsgs is :'+errorMsgs);
            }
            else
            {
               // alert(orderQuanitity+'....orderQuanitity');
                var recId = component.get("v.LineItemrecId");
                // LineItemrecId is null then insert otherwise upsert.
                //  priceRequest ='sdfsdf';
                console.log('The raja is:'+prdCat+'....'+remarks+'....'+orderQuanitity+'....'+prdlistPriceCst);
              //  alert(sp_requestQuanitity+'sp_requestQuanitity');
			//  alert('The val :'+prdThick);
                var saveLineItemRec = component.get("c.saveLineItemRec");
                saveLineItemRec.setParams({"prdCat":prdCat,"prdName":prdName,"prdThick":prdThick,"prdlength":prdlength,
                                           "packTypeTons":packTypeTons,"prdCases":prdCases,"UOM":UOM,
                                           "prdlistPriceCst":prdlistPriceCst,"prdbasePrice2":prdbasePrice,"priceVar":priceVar,
                                           "sp_requestQuanitity":sp_requestQuanitity,"sp_priceRequestCst":sp_priceRequestCst,
                                           "validFrom":validFrom,"validTo":validTo,"remarks":remarks,"enqRecId":enqRecId,"orderQuanitity":orderQuanitity,"recId":recId,"Standardpriceperunit": standardPriceUnit,"priceRequest":priceRequest,"Required_Price_per_Sq_M":Required_Price_per_Sq_M,"submitRecStatus":submitCheck,"LstInvRate":LstInvRate});
                
                saveLineItemRec.setCallback(this,function(a){
                    console.log("The return msg for new save:"+JSON.stringify(a.getReturnValue()));
                    /***   *******/
                  //  alert('the alert 1st'+'...'+JSON.stringify(a.getReturnValue()));
                    var exceptionFinding ='empty str';
                    if(a.getReturnValue().remarks__c)
                        exceptionFinding = JSON.stringify(a.getReturnValue().remarks__c );
                    // alert(exceptionFinding+'.........'+a.getReturnValue());
                    console.log('the exception 1st'+'...'+JSON.stringify(a.getReturnValue()));
                    if(!exceptionFinding.includes("exception"))
                    {
                        console.log('the inside exception 1st');
                        //   alert(component.get("v.EnuLineItemRec"));
                        component.set("v.EnuLineItemRec",a.getReturnValue());           
                        component.set("v.LineItemrecId",a.getReturnValue().Id);
                        component.getEvent("controlSaveEdit").setParams({"errorStatus": "true","rowIndexVal":rowIndexVal}).fire();
                        
                    } 
                    else
                    {
                        component.getEvent("controlSaveEdit").setParams({"errorStatus": "false"}).fire();
                        alert('exception thrown '+JSON.stringify(a.getReturnValue()));
                        
                    }
                    
                    // Once the user saved the record then, display the saved values to the user. 
                    component.set("v.enqLineItem.category3",prdCat);
                    component.set("v.enqLineItem.product3",prdName);
                    //    alert('the prdThick is :'+prdThick+'....'+component.get("v.enqLineItem.thickness")+'....'+component.get("v.enqLineItem.thickness"));
                    var thicks = component.get("v.enqLineItem.thickness[0]");
                    //  alert(thicks);
                    component.set("v.enqLineItem.thickness3",thicks);
                    
                    component.set("v.enqLineItem.length3",prdlength);
                    component.set("v.enqLineItem.packTypeTons3",packTypeTons);
                    component.set("v.enqLineItem.price3",priceVar);
                    
                    //  console.log('The EnuLineItemRec is :'+EnuLineItemRec);
                    
                });
                
                $A.enqueueAction(saveLineItemRec);
                component.set("v.showEdit","true");
            }
            
        }       
		}
		else{
		       component.getEvent("controlSaveEdit").setParams({"errorStatus": "true","rowIndexVal":rowIndexVal}).fire();

		}
    },
    showEditPage :function(component,event,handler)
    {
        component.set("v.showEdit","true");
    },
    
    showCancelPage :function(component,event,handler)
    {
        component.set("v.showEdit","false");
    },
    deleteRecord :function(component,event,handler)
    {
        
        helper.deleteRecordHelper(component,event);
        
    },
    
    regionChanged :function(component,event,handler)
    {
        console.log('the regions is changed: '+component.get("v.region"));
        component.set("v.enqLineItem.listPriceCst","");
        var priceType2 = component.get("v.enqLineItem.price3");
        //  helper.onPriceChangeHelper(component,event,priceType2);
        // ask ashim for this above uncomment code error.
        
    },
    selectedPR :function(component,event,handler)
    {
        var selectedItem = event.currentTarget;
        var prodObj = selectedItem.dataset.record;
        prodObj = prodObj.split('###');
        console.log('prodObj:'+prodObj);
       // alert(prodObj[1]);
        
        var orderQuanitity = parseFloat(component.get("v.enqLineItem.orderQuanitity"));
        var availableQty = parseFloat(prodObj[1]);
     //   alert('orderQuanitity:'+orderQuanitity);
      //  alert('availableQty:'+availableQty);
        
        if(availableQty < orderQuanitity) {
            
            alert('Requested Quantity is greater than the avialable quantity on selected Price Request');
            return false;
        } 
       // alert('TEST');
        
        var priceType2 ='List';
        // helper.onPriceChangeHelper(component,event,priceType2);
        
        console.log('the regions is changed: '+component.get("v.region"));
        component.set("v.enqLineItem.listPriceCst","");
        //    var priceType2 = component.get("v.enqLineItem.price3");
        var regionName = component.get("v.region");       
        var prdIdVar = component.find("prdName");
        var prdId = prdIdVar.get("v.value");
        var prdCatVar = component.find("prdCat");
        var prdCat = prdCatVar.get("v.value");
        console.log('The prdId is 222:'+prdId+'....'+regionName+'....'+priceType2+'.....');
        var onPriceChangeMethod = component.get("c.getonPriceChange");
        onPriceChangeMethod.setParams({"productId":prdId,"region":regionName,"priceType":priceType2,"prdCat":prdCat});       
        
        onPriceChangeMethod.setCallback(this,function(a){            
            component.set("v.enqLineItem.listPriceCst",a.getReturnValue());  
            console.log('The a.getReturnValue() is :'+JSON.stringify(a.getReturnValue()));
            // formulas here. 
            // 1. standard price per unit = list price/thickness.
            var thicknessInNumber = component.get("v.enqLineItem.thickness[0]").replace('-mm', '');
            console.log('the thicknessInNumber is i:' +thicknessInNumber)
            var totalVal = a.getReturnValue()*thicknessInNumber;
            console.log('The totalValis :'+totalVal);
            component.set("v.enqLineItem.Standardpriceperunit",totalVal);
            
            //  2. setting base price = standard price per unit * quantiity in Sq mtrs.
            //    SQM = Tons * 400 / thickness in mm
            //    base price = standard price per unit * SQM
            var SQM = component.get("v.enqLineItem.orderQuanitity") * 400 / thicknessInNumber;
            var basePr = totalVal*SQM;
            component.set("v.enqLineItem.basePrice",basePr);
            
        }); 
        
        
        
        
        $A.enqueueAction(onPriceChangeMethod);
        
        //var selectedItem = event.currentTarget;
        var Name = prodObj[0];
        console.log('data Name = '+ Name);
        component.set("v.enqLineItem.priceRequest",Name);
        var prMap = component.get("v.prMap");
        console.log('The prMapis :'+prMap);
        console.log("hello:"+prMap[Name].Name+'...'+prMap[Name].Backend_fromDate__c);
        
       
        component.set("v.enqLineItem.validFrom",prMap[Name].Backend_fromDate__c);
        component.set("v.enqLineItem.validTo",prMap[Name].Backend_toDate__c);
        
        
        // for Hide/Close Model,set the "isOpen" attribute to "Fasle"  
        component.set("v.isOpen", false);
        component.set("v.isOpenNew", false);
    },
    closeModel: function(component, event, helper) {
        // for Hide/Close Model,set the "isOpen" attribute to "Fasle"  
        component.set("v.isOpen", false);
        component.set("v.isOpenNew", false);
        component.set("v.isOpenSpecial", false);
         component.set("v.isOpenStockDetails", false);
        component.set("v.isOpenEnqLineItemStatusDetails",false);
    },
    
    
    
    
    savePrRec :function(component,event,helper)
    {
	   var approvalLevel='';
       var pr1 = component.get("v.pr1");
             var enqRecId = component.get("v.EnqRecId");
         var selectedProduct = component.get("v.enqLineItem.product3");
        var priceType = component.find("priceTypeId").get("v.value");
       priceType = 'Tier';
        var Sales_Office =  component.get("v.region");
        
        console.log('the Sales_Office is :'+Sales_Office);
        var accid = component.get("v.accid");
       
        var Product_Category='';
            if(component.find("prdCat"))
             Product_Category = component.find("prdCat").get("v.value");
           
       var selectedCustomerRec = component.get("v.billToStrName");
        var fromDate = component.get("v.pr1.Valid_From__c");
        var Todate = component.get("v.pr1.Valid_To__c");
        var requiredPrice = component.get("v.pr1.Required_Price__c");
        var remarks =  component.get("v.pr1.Remarks__c");
         
        
		var prdlength ='';
		if(component.find("prdlength")) {
             prdlength = component.find("prdlength").get("v.value");
			  }
		
      // get the Min and Max price for the selected product. 
	  
	   var errorMsgs =[];
	   var errorMsgs2 =[];
         var accid = component.get("v.accid");
                // alert('the accid is '+accid);
				 var regionName = component.get("v.region"); 
                var salearea = component.get("v.salearea");
                console.log('The coming regionName is :'+regionName);
               
                
		var orderQuanitity = component.get("v.enqLineItem.orderQuanitity");
        var quantityInTons = component.get("v.enqLineItem.orderQuanitity");
        var thicknessInNumber = component.get("v.enqLineItem.thickness[0]").replace('-mm', '');
        var quantityInSqMt = quantityInTons * 400 / thicknessInNumber;
                
        var lstrpice = component.get("v.enqLineItem.listPriceCst");
		var returnval2;
				
				var prdIdVar = component.find("prdName");
        var prdId = prdIdVar.get("v.value");
        var prdCatVar = component.find("prdCat");
        var prdCat = prdCatVar.get("v.value");
		
		
		 var requiredPriceCutOff;
		var requiredPriceLessThanEqualTo15;
		
		  
        if(! requiredPrice)
            errorMsgs.push('please enter Required Price (per Sq. M)');
        
     
        if(isNaN(pr1.Requested_Quantity__c))
            errorMsgs.push('please enter only numbers for Requested Quantity (Sq.M)');
      
        if(isNaN(requiredPrice))
            errorMsgs.push('please enter only numbers for Required Price (per Sq. M)');
        
        
        if(!pr1.Valid_From__c)
            errorMsgs.push('please enter Valid From');
        if(!pr1.Valid_To__c)
            errorMsgs.push('please enter Valid To');
      
	  
	  if(errorMsgs.length > 0) {
            helper.hideSpinner(component, event); 
            alert(errorMsgs.join('\n'));
            console.log('The errorMsgs is :'+errorMsgs);
        }  else
        {
       var onPriceChangeMethod2 = component.get("c.getonPriceChangeTierPrice");
        onPriceChangeMethod2.setParams({"productId":prdId,"region":regionName,"priceType":priceType,"prdCat":prdCat,"accountid":accid});       
                onPriceChangeMethod2.setCallback(this,function(a){
                    
                     returnval2 = a.getReturnValue();
                    //alert('The returnval2 is :'+returnval2);
					returnval2 = parseFloat(returnval2).toFixed(2);
                      requiredPriceCutOff = (returnval2 * 0.75).toFixed(2);
					  requiredPriceLessThanEqualTo15  = (returnval2 * 0.85).toFixed(2);
					 
					// alert('The values are: '+returnval2+'.....'+requiredPriceCutOff+'....'+requiredPrice);
					 var requiredPriceMoreThanTo15  = '';
					 // if(requiredPriceCutOff >= requiredPrice)  // 90 >=4 
					//  alert('I am coming here:');
					 
					
					 
					
                    console.log('the exception 3rd');
                    if(a.getReturnValue()!= "")
                    {
                        console.log('the inside exception 3rd');
                    }
        
		
		 debugger;
		 console.log('Sup values are:'+requiredPrice +'.....'+requiredPriceCutOff +'.....'+requiredPrice+'....'+returnval2);
		  
		if(parseFloat(requiredPrice) < parseFloat(requiredPriceCutOff) || parseFloat(requiredPrice) > parseFloat(returnval2)) 	
					 errorMsgs2.push('Requested Price should be between of :'+requiredPriceCutOff+' and '+returnval2 );				
					 else					 
					  if( requiredPrice >=requiredPriceCutOff && requiredPrice <=requiredPriceLessThanEqualTo15) 
					  approvalLevel= 'Rm Approval';
					  else
					  if(requiredPrice >= requiredPriceLessThanEqualTo15 && requiredPrice <= returnval2)
					  approvalLevel= 'Nh Approval';

            if(errorMsgs2.length > 0) {
		   
            helper.hideSpinner(component, event); 
            alert(errorMsgs2.join('\n'));
            console.log('The errorMsgs is :'+errorMsgs2);
        }  else
        {
            helper.savePrRecHelper(component,event,approvalLevel,returnval2);
        }
		});
		$A.enqueueAction(onPriceChangeMethod2);  
		}
	}, 
    
    isOpenNewMethod :function(component,event,handler)
    {
        component.set("v.isOpenNew",false);
        
        var priceReq = "{'sobjectType': 'price_request_PandP__c'}";
        component.set("v.pr",priceReq);
        component.set("v.isOpenSpecial",true);
    },
    AddNewRow : function(component, event, helper){
        // fire the AddNewRowEvt Lightning Event 
        component.getEvent("AddRowEvt").setParams({"displayAddRow" : false}).fire();      
        
        //  component.getEvent("hideAndShowAdd").setParams({"displayAddRow" : false}).fire(); 
        //  component.set("v.displayAddRow",false);
    },
    
    removeRow : function(component, event, helper){
        // fire the DeleteRowEvt Lightning Event and pass the deleted Row Index to Event parameter/attribute
        // component.set("v.displayAddRow",true);
       // alert('calling at here:');
         var submitedStatus1 = component.get("v.enqLineItem.submitRec");
        if(submitedStatus1 == true)
        {
            alert('You can not delete line item for submitted Enquiry');
        }
        else
        {
        component.getEvent("DeleteRowEvt").setParams({"indexVar" : component.get("v.rowIndex"),"displayAddRow" : true}).fire();
        helper.deleteRecordHelper(component,event);
        }
        
        //  component.getEvent("hideAndShowAdd").setParams({"displayAddRow" : true}).fire(); 
        
     },
    
    nullify : function(component, event, helper)
    {
        component.set("v.validFromValidationError" , '');
    },
    
    fromDateCheck : function(component, event, helper) {
        
        var today = new Date();    
        var dd = today.getDate();
        var mm = today.getMonth() + 1; //January is 0!
        var yyyy = today.getFullYear();
        // if date is less then 10, then append 0 before date   
        if(dd < 10){
            dd = '0' + dd;
        } 
        // if month is less then 10, then append 0 before date    
        if(mm < 10){
            mm = '0' + mm;
        }
        
        var todayFormattedDate = yyyy+'-'+mm+'-'+dd;
        if(component.get("v.pr1.Valid_From__c") != '' && component.get("v.pr1.Valid_From__c") < todayFormattedDate){
            component.set("v.validFromValidationError" , true);
            alert('Date must be in present or in future');
        }else{
            component.set("v.validFromValidationError" , false);
        }
        
        var toDate = component.get("v.pr1.Valid_To__c");
        var fromDate = component.get("v.pr1.Valid_From__c");
        if(toDate) {
            
            var timeDiff = Date.parse(toDate) - Date.parse(fromDate);
            var diffDays = Math.ceil(timeDiff / (1000 * 3600 * 24)); 
            if(diffDays < 0) {
                
                alert('Invalid Date');
                component.set("v.validFromValidationError" , true);
            }
        }
    },
    
    toDateCheck : function(component, event, helper) {
        
        var today = new Date();    
        var dd = today.getDate();
        var mm = today.getMonth() + 1; //January is 0!
        var yyyy = today.getFullYear();
        // if date is less then 10, then append 0 before date   
        if(dd < 10){
            dd = '0' + dd;
        } 
        // if month is less then 10, then append 0 before date    
        if(mm < 10){
            mm = '0' + mm;
        }
        
        var todayFormattedDate = yyyy+'-'+mm+'-'+dd;
        if(component.get("v.pr1.Valid_To__c") != '' && component.get("v.pr1.Valid_To__c") < todayFormattedDate){
            component.set("v.validToValidationError" , true);
            alert('Date must be in present or in future');
        }else{
            component.set("v.validToValidationError" , false);
        }
        
        var toDate = component.get("v.pr1.Valid_To__c");
        var fromDate = component.get("v.pr1.Valid_From__c");
        if(toDate) {
            var timeDiff = Date.parse(toDate) - Date.parse(fromDate);
            var diffDays = Math.ceil(timeDiff / (1000 * 3600 * 24)); 
            if(diffDays < 0) {
                
                alert('Invalid Date');
                component.set("v.validToValidationError" , true);
            }
        }
    },
    
    clearFromDateInCaseInvalid : function(component, event, helper) {
        
        if(component.get("v.validFromValidationError")) {
            
            component.set("v.pr1.Valid_From__c", "");
        }
    },
    
    clearToDateInCaseInvalid : function(component, event, helper) {
        
        if(component.get("v.validToValidationError")) {
            
            component.set("v.pr1.Valid_To__c", "");
        }
    },
    
    onRequestQuantChange :function(component,event,helper)
  {
   // setting up the quanitity in sqmtrs value in price request popup. 
             var quantityInTons = component.get("v.pr1.Requested_Quantity__c");
         var thicknessInNumber = component.get("v.enqLineItem.thickness[0]").replace('-mm', '');
        var quantityInSqMt = (quantityInTons * 400 / thicknessInNumber).toFixed(2);
                    component.set("v.RequestedQuantSqmt",quantityInSqMt);
    //  alert('cming here:'+quantityInSqMt);
  },
    onEnqLineItemStatusDetails : function(component,event,helper)
    {
        
            var serverCall = component.get("c.displayEnquiryLineItemStatusRec");
        var existingRecId = component.get("v.enqLineItem.recordId"); 
        if(!existingRecId)
            alert('Please create the enquiry line item record and check for status');
        else{
    
     
       serverCall.setParams({"enlId":existingRecId});
        serverCall.setCallback(this,function(a){
            console.log('The ret val is:'+a.getReturnValue());
                     // alert(JSON.stringify(a.getReturnValue()));  

			// if(a.getReturnValue().Enquiry_Line_Reference__c  =='No Status record found for this enquiry')
			
           component.set("v.EnqLineStatus",a.getReturnValue());  
            component.set("v.isOpenEnqLineItemStatusDetails",true);
                      
        });
        $A.enqueueAction(serverCall);
        }
    }
    
})
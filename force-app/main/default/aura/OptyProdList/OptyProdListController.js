({    doInit : function(component, event) {
  //console.log("OptyProdList loaded");
  var id =  component.get("v.recordId");     
  var action = component.get("c.getOptyProd");
  action.setParams({
    "optyId": id
  });

  action.setCallback(this, function(a) {
    var state = a.getState(); 
    //console.log("OptyProdList {doInit} - getOptyProd()");
    var result = a.getReturnValue();

    if(result.lst.length > 0){
      var y = result.lst[0];
      component.set("v.optyProdId",y.Id);

      if(y.Id!=null && y.Id!='undefined'){
        //console.log("OptyProdList {doInit} - RetrieveOptyID Event fired from IF block");
       // if(!component.get("v.screen")){
        var myEvent = $A.get("e.c:RetrieveOptyID");
        myEvent.setParams({"prodrecID": y.Id});
        myEvent.fire();  
       // }
        
      }
      else{
        //console.log("OptyProdList {doInit} - RetrieveOptyID Event fired from ELSE block");
      }

    }
    component.set("v.visibilityFlag",1);
    component.set("v.initialLoad",true);
    component.set("v.opty",result); 
  });
  $A.enqueueAction(action);

},

updateSaveFlag : function(component, event) {
//component.set("v.currentId",)
 component.set("v.initialLoad",false);
 //console.log("OptyProdList-{updateSaveFlag}");
 var f = event.getParam("flag"); 
 component.set("v.flag",f); 
 //console.log("FLAG " + f);
 
 
 //console.log("optyProdList initialLoad value :"+component.get("v.initialLoad"));

  //New code starts
var visibility = component.get("v.visibilityFlag");
 var toggleDiv = component.find("prod");
    //console.log("TOGGLE");
    if((f == false)&& (visibility == 1)){
        //console.log("TOGGLE ++ HIDE");
        component.set("v.visibilityFlag",2);
        $A.util.toggleClass(toggleDiv, "toggleDisplay");
      
    }
    else if((f == true)&& (visibility == 2)){
      // 
       // console.log("TOGGLE ++ SHOW");
        component.set("v.visibilityFlag",1);
        $A.util.toggleClass(toggleDiv, "toggleDisplay"); 
       
    }
     //New code ends
},

setOptyProdId : function(component, event) {
  //console.log("Before setting index value:"+component.get("v.indexValue"));
  var recId = component.find("prod").get("v.value"); 
  var indexcount = 0;
  var optyLst = component.get("v.opty");
  
  //console.log("Inside IF: recId"+recId);

  for(var i=0; i<optyLst.lst.length; i++){
    if(optyLst.lst[i].Id==recId){
      //console.log("Inside IF: i.Id"+optyLst.lst[i].Id);
      //console.log("Inside IF: recId"+recId);
      indexcount++;
      break;
    }
  } 
  component.find("prod").set("v.value", recId.toString());
  component.set("v.indexValue",indexcount);
  //console.log("After setting index value:"+component.get("v.indexValue"));
  if(recId!=null && recId!='undefined'){
    //console.log("OptyProdList setOptyProdId {getOptyProdId} - RetrieveOptyID Event fired ");
    var  myEvent = $A.get("e.c:RetrieveOptyID");
    myEvent.setParams({"prodrecID": recId});
    myEvent.fire();     

  }
}

})
({
    onloadingmethod :function(component,event,helper)
    {
       
          var orsinformation = component.get("c.displayORSInformation");
          orsinformation.setCallback(this,function(a){
             // alert('coming here');
            //   alert("a.getReturnValue()"+JSON.stringify(a.getReturnValue()));
            var iterator1  =  a.getReturnValue().split('-');
              var state = orsinformation.getState();
            if (state === "SUCCESS") {

                   component.set("v.isORS",iterator1[0]);
                   component.set("v.orsVal",iterator1[1]);
                 // alert(component.get("v.isORS")+'......'+component.get("v.orsVal"));
                
                if(iterator1[0] == 'ORS')
                    //alert('coming here');
                 helper.fetchAccountRecsHelper(component,event);
              }
              else if (state === "ERROR") {
                var errors = orsinformation.getError();
                if (errors) {
                    if (errors[0] && errors[0].message) {
                        alert("Error message: " + 
                                 errors[0].message);
                    }
                } else {
                    alert("Unknown error");
                }
            }
        });
       $A.enqueueAction(orsinformation);
        
        var enquRecId = component.get("v.EnquiryId");
        if(enquRecId)
        {
            helper.showSpinner(component, event, helper);
            component.set("v.showCancel","true");
            
            //alert("123123");
            helper.callExistingEnq(component,event);
            helper.populateAccStatus(component,event,enquRecId);
            var accListVar = [];      
			 var accListVar2 = [];   
            accListVar.push({'sobjectType':'Account','Glass_reach_account_name__c':'-None-'});
			 accListVar2.push({'sobjectType':'Account','Name':'-None-'});
            component.set("v.accList",accListVar);
            component.set("v.shipList",accListVar2);
            
            var salesAreaVales = component.get("c.getSalesAreaValues");
            salesAreaVales.setCallback(this,function(a){
                component.set("v.salesAreaValues",a.getReturnValue());
                helper.hideSpinner(component, event, helper);
            });
            $A.enqueueAction(salesAreaVales);
            
            // getting the default empty row for enquiry line item.
            var wraperdata = component.get("c.getEmptyLineItem");
            wraperdata.setCallback(this,function(a){
                component.set("v.enqLineItemList",a.getReturnValue());
                //  alert(JSON.stringify(a.getReturnValue()));
            });
            $A.enqueueAction(wraperdata);
            var recsCount = component.get("v.enqLineItems");
            //component.set("v.showAddLineItem","false");    
            
        }
        else
        {
            component.set("v.showAddLineItem", false);   
            component.set("v.showEdit","true");      
            helper.showSpinner(component, event, helper);
            
            var accListVar = [];    
			  var accListVar2 = [];    
			
          component.set("v.accList",accListVar);
            component.set("v.shipList",accListVar2);
            
            var salesAreaVales = component.get("c.getSalesAreaValues");
            salesAreaVales.setCallback(this,function(a){
                component.set("v.salesAreaValues",a.getReturnValue());
                helper.hideSpinner(component, event, helper);
            });
            $A.enqueueAction(salesAreaVales);
            
        }
    },
    fetchAccountRecs2 : function(component, event, helper) {
	
	 var accListVar = [];    
			  var accListVar2 = [];    
			
			
	  accListVar.push({'sobjectType':'Account','Glass_reach_account_name__c':'-None-'});
			accListVar2.push({'sobjectType':'Account','Name':'-None-'});
            component.set("v.accList",accListVar);
            component.set("v.shipList",accListVar2);
			component.set("v.accAddressVal","");
             component.set("v.ShipToAddress","");
        component.set("v.Enquiry.Bill_to_customer__r.Address__c","");
        component.set("v.Enquiry.Ship_To_Customer__r.Address__c","");

       
        component.set("v.regionVal",event.getSource().get("v.value"));
        var salesArea = event.getSource().get("v.value");
        //alert(component.get("v.regionVal")+'the val is ');
        component.set("v.billToStr","");
        component.set("v.shipToStr","");
        
     //   component.set("v.shipList","-None-");
        if(salesArea =='-None-')
            component.set("v.AccStatus","");
        // alert(component.find("billtoVar").get("v.value"));
        helper.fetchAccountRecsHelper(component,event);
        
        
    },
    fetchShipToCustomer: function(component,event,helper){
        var selectAccId = event.getSource().get("v.value");
        // alert('the selectAccId is :'+selectAccId);
         var billtoVar = component.get("v.billToStr");
         var accAddressMap1 = component.get("v.accAddressMap");
         if(billtoVar)
         {
             if(accAddressMap1 && accAddressMap1[billtoVar])
                 component.set("v.Enquiry.Bill_to_customer__r.Address__c",accAddressMap1[billtoVar]);
             //  alert('The EnquiryId is :'+accAddressMap1[component.get("v.billToStr")]);
         }
        helper.fetchShipToCustomerHelper(component,event,selectAccId);
    },
    
    fetchShipToAddress :function(component,event,helper){
        var selectedShipTo = event.getSource().get("v.value");
     
        var shipToAddressMap = component.get('v.ShipToAddressMap');
     //  alert(shipToAddressMap[selectedShipTo]);
    if(shipToAddressMap && shipToAddressMap[selectedShipTo])
        component.set("v.Enquiry.Ship_To_Customer__r.Address__c", shipToAddressMap[selectedShipTo]);
    },
    
    saveRecord :function(component,event,helper){
        // alert('The ashim alert:'+component.find("billtoVar").get("v.value"));
        // alert(component.get("v.billtoVar"));
        
		
	
        component.set("v.callSaveAction","true");
       
        var enqLineItemsCount = component.get("v.enqLineItems").length;
        console.log('coming into normal save'+enqLineItemsCount);
        if(enqLineItemsCount == 0) {
            
           var glass_diirect_enqRec = component.get("v.glass_diirect_enq");
		 
	  if(glass_diirect_enqRec == 'false')
	  {
          
  // alert('coming inside');	  /**************************************************************************************************************************************************************************************/
            helper.showSpinner(component, event, helper);
            var saveEnquiry = component.get("c.saveEnquiryRec");
          //  var regionVal = component.find("regionVar").get("v.value");
          var regionVal = '';
          
           var ifORS = component.get("v.isORS");
        regionVal =component.get("v.orsVal"); 
       
        if(component.find("regionVar")) 
        {
             
            if(ifORS == 'NOT ORS')
            regionVal = component.find("regionVar").get("v.value"); 
            //selectedSalesArea =  event.getSource().get("v.value");
        }  
          
          // alert('coming here only');
            var billtoVal = component.find("billtoVar").get("v.value");
            var shitptoVal = component.find("shitptoVar").get("v.value");
            
            var billToStr2 = component.get("v.billToStr");
            var shipToStr2 = component.get("v.shipToStr");
            
            var result ='';
            var resultId ='';
            
            console.log('the values are :'+regionVal+'....'+billtoVal+'....'+shitptoVal);
            if(regionVal =='-None-')
            {
                alert('please select sales area');
                helper.hideSpinner(component, event, helper);   
            }
            else if(billtoVal =='-None-' && shitptoVal =='-None-')
            {
                alert('Please change bill to customer & ship to customer. Else please click on cancel to go back');    
                helper.hideSpinner(component, event, helper);
            }
                else if(billtoVal =='-None-')
                {
                    helper.hideSpinner(component, event, helper);
                    alert('please select bill to customer');
                }
            
                    else if(shitptoVal =='-None-')
                    {
                        helper.hideSpinner(component, event, helper);
                        alert('please select ship to customer');
                    }
            
            
                        else
                        {
                            
                            var enquRecId = component.get("v.EnquiryId");
                            console.log('The values are the 123 '+regionVal+'...'+billtoVal+'...'+shitptoVal);
                            console.log('The enquRecId is :'+enquRecId);
                            if(!enquRecId)
                            {
                                
                                saveEnquiry.setParams({"region":regionVal,"billToCustomer":billtoVal,"shipToCustomer":shitptoVal});
                                saveEnquiry.setCallback(this,function(a){
                                    var returnMsg = JSON.stringify(a.getReturnValue());
                                    var returnmsg1 = a.getReturnValue();
                                    //  alert(returnMsg);
                                    //  console.log('The returnMsg is :'+returnMsg);
                                    if(returnMsg.includes("Exception"))
                                    {
                                        console.log('The exception came'+returnMsg); 
                                        helper.hideSpinner(component, event, helper);
                                    }
                                    else
                                    {
                                       // alert('sdfsdf'+a.getReturnValue());
                                        component.set("v.EnquiryId", a.getReturnValue());   
                                        component.set("v.showEdit","false");
                                        // console.log(component.get("v.EnquiryId")+component.get("v.EnquiryId"));
                                        helper.callExistingEnq(component, event,returnMsg);
                                        helper.gotoURL(component,event,returnmsg1);
                                        helper.hideSpinner(component, event, helper);
                                    }
                                });
                                $A.enqueueAction(saveEnquiry);
                            }
                            else
                            {
                                var billToStr1 = component.get("v.billToStr");
                                var shipToStr1 = component.get("v.shipToStr");
                                //  alert('coming into else part'+billtoVal+'...'+billToStr1+'...'+shipToStr1);
                                var enqRec = {sobjectType:"Enquiry__c", 
                                              Id:enquRecId,
                                              Sales_Office_ERP__c:regionVal,
                                              Bill_to_customer__c:billToStr1,
                                              Ship_To_Customer__c:shipToStr1
                                             }; 
                                
                                //  alert('the 123123123'+JSON.stringify(enqRec));
                                
                                var updateEnqRecordVar = component.get("c.updateEnqRecord");
                                updateEnqRecordVar.setParams({"enquRecId":enquRecId,"regionVal":regionVal,"billToStr1":billtoVal,"shipToStr1":shitptoVal});
                                updateEnqRecordVar.setCallback(this,function(a){
                                    console.log('The a is :' + JSON.stringify(a.getReturnValue()));
                                    result = a.getReturnValue();
                                    resultId = a.getReturnValue().Id;
                                    console.log(resultId+'...resultId');
                                    component.set("v.showEdit","false");
                                    helper.callExistingEnq(component, event);
                                    helper.populateAccStatus(component,event,resultId);
                                    helper.hideSpinner(component, event, helper);
                                });	
                                $A.enqueueAction(updateEnqRecordVar);	
                                // if(result.Comments__c.includes('exception'))
                                //   alert('Error thown while updating Enquiry record:'+ result.Comments__c);
                            }
                            component.set("v.showEdit","false");
                            helper.callExistingEnq(component, event);
                            helper.populateAccStatus(component,event,billtoVal);
                        }  
        }
		   else
		     alert('You can not edit glass reach enquiry record. Please click on cancel');
		}
		  
    },
    
    editRecord :function(component,event,helper){
        var submitedStatus = component.get("v.submitedStatus");
        if(submitedStatus == true)
        {
            alert('You can not add line item for submitted Enquiry');
        }
        else
        {
        helper.showSpinner(component, event, helper);
        var enqRec = component.get("v.Enquiry");
        
     //   if(enqRec.Glass_Direct_Enquiry__c)
     //   {
         //   alert('you can not edit Glass Direct Enquiry');
            
         //   component.set("v.showEdit","true");
            //   helper.callExistingEnq(component, event);
            //  helper.hideSpinner(component, event, helper);
     //   }
    //    else
    //    {
            // below 2 lines + above 2 lines commented because Glass direct enqiry should not edit. Only Line items should able to edit.  
            component.set("v.showEdit","true");
            helper.callExistingEnq(component, event);
            
            console.log('edit record calling'+component.get("v.showEdit")+'......'+component.get("v.EnquiryId"));
            helper.hideSpinner(component, event, helper);
      //  }
        }
    },
    OnNewButton :function(component,event,helper){
        
        component.set("v.clikedNew","new");
        //  component.set("v.showEdit","true");
        // getting the default empty row for enquiry line item.
        var wraperdata = component.get("c.getEmptyLineItem");
        wraperdata.setCallback(this,function(a){
            component.set("v.enqLineItem",a.getReturnValue());
            //  alert(JSON.stringify(a.getReturnValue()));
        });
        $A.enqueueAction(wraperdata);
        
    },
    // function for create new object Row in Contact List 
    addNewRow: function(component, event, helper) {
        // call the comman "createObjectData" helper method for add new Object Row to List  
        //   alert('calling here');
        var submitedStatus = component.get("v.submitedStatus");
        if(submitedStatus == true)
        {
            alert('You can not add line item for submitted Enquiry');
        }
        else
        {
        helper.createObjectData(component, event);
        
        var showHide = event.getParam("displayAddRow");
        component.set("v.displayAddRow",false);
        console.log('the show hide is :'+showHide);
        }
    },
    
    // function for delete the row 
    removeDeletedRow: function(component, event, helper) {
        // get the selected row Index for delete, from Lightning Event Attribute  
        var index = event.getParam("indexVar");
        
        // get the all List (enqLineItems attribute) and remove the Object Element Using splice method    
        var AllRowsList = component.get("v.enqLineItems");
        AllRowsList.splice(index, 1);
        // set the enqLineItems after remove selected row element  
        component.set("v.enqLineItems", AllRowsList);
        helper.showAdLineItenBtn(component,event);
        
        var showHide = event.getParam("displayAddRow");
        component.set("v.displayAddRow",true);
        console.log('the show hide is :'+showHide);
    },
    CancelRecord: function(component,event,helper)
    {
        var enquRecId = component.get("v.EnquiryId");
        if(enquRecId)
        {          
            helper.gotoURL(component,event,enquRecId);        
        }
        else
        {
            helper.gotoURLNewPage(component,event);        
        }
        
        component.set("v.showEdit","false");
        
    },
    captureShowHide :function(component,event,helper)
    {
        var showHide = event.getParam("displayAddRow");
        component.set("v.displayAddRow",true);
        console.log('the show hide is :'+showHide);
    },
    controlSave :function(component,event,helper)
    {
        console.log('coming into contrl save save');
        var enqLineItemsCount = component.get("v.enqLineItems").length;
        
        var eventListenerVal = event.getParam("errorStatus");
        
        if(eventListenerVal =="true")       
            component.set("v.totalSucessCount",component.get("v.totalSucessCount")+1);
        
        var totalSucessCount = component.get("v.totalSucessCount");        
        
        
        var tempval = component.get("v.totaleventFiredCount")+1;
        component.set("v.totaleventFiredCount",tempval);
        
        
        var totaleventFiredCount =component.get("v.totaleventFiredCount");
        
        
        
        
        console.log('is calling or not:'+totaleventFiredCount+'...'+enqLineItemsCount+'....'+totalSucessCount);
        // If the count is not same that means error thrown in the child components. Make the callSaveAction false for next use
        //  alert('coming values are:'+totaleventFiredCount+'...'+enqLineItemsCount);
        //alert('the temp is:'+(parseInt(totaleventFiredCount)-1));
        if((parseInt(totaleventFiredCount)) == enqLineItemsCount)
        {
            console.log('1st statement:');
            // alert('1st itteratoin');
            if(enqLineItemsCount != totalSucessCount)
            {
                console.log('2nd statement:');
                component.set("v.callSaveAction","false");
                component.set("v.totaleventFiredCount",0);
                 component.set("v.totalSucessCount",0);
            }
            else
            {
                helper.saveEnqRecord(component,event);
                component.set("v.showEdit","false");
            }
        }
        
        
        //  console.log('the final vals: '+totaleventFiredCount+'....'+enqLineItemsCount+'....'+totalSucessCount);
        
        
        
        
    },
    
    submitEnquiryRecord: function(component,event,helper)
    {
        var lineItemsCount = component.get("v.enqLineItems").length;
        var temp2 = component.get("v.enqLineItemList").length;

       // alert(temp+'........'+temp2);
      if(lineItemsCount > 0)
      {
       
        var EnquiryId = component.get("v.EnquiryId");
        var submitEnquiryRec = component.get("c.submitEnquiry");
        submitEnquiryRec.setParams({"recid":EnquiryId});
        submitEnquiryRec.setCallback(this,function(a){
           if(a.getReturnValue())
           {
               if(JSON.stringify(a.getReturnValue()).includes('sucess'))
               {
                   alert("Enquiry submitted successfully");
                   helper.gotoURL(component,event,EnquiryId);    
               }
                   
               else
                   alert(JSON.stringify(a.getReturnValue()));
           }    
            else
                alert('Exception thrown by system'+JSON.stringify(a.getReturnValue())+'...'+EnquiryId); 
                                         
     });
    $A.enqueueAction(submitEnquiryRec);
    } 
        else
        {
            alert('Please add atleast one line item to submit');
        }
    },
    
    duplicatePrdValidation :function(component,event,helper)
    {
                var slNo = event.getParam("serNo"); 
        var enqLineItemsList = component.get("v.enqLineItems");
        var dupList = [];
        for (var i = 0; i < enqLineItemsList.length; i++) 
         {
            var tempStr = enqLineItemsList[i].category3+'-'+enqLineItemsList[i].product3+'-'+enqLineItemsList[i].length3;  
             if(!dupList.includes(tempStr))
             dupList.push(tempStr);
             else
             {
            alert('You have selected duplicate product with category for same dimentions, Please select different product OR category');
          // component.set("v.foundDupProdSerialNo",slNo);            
      //  var childComponent = component.find("LineItemCmp");
      //  childComponent.captureErrorRow('slNo');
           //enqLineItemsList[slNo].remarks = '';
                enqLineItemsList[slNo].category3 = '-None-';
                  enqLineItemsList[slNo].product3 = '';  
                  enqLineItemsList[slNo].length3 = '-None-';  
              var ParentToChildevt = $A.get("e.c:enquiryComponentParentValidation");
        ParentToChildevt.setParams({
            "message" : slNo});
        ParentToChildevt.fire();
             
    
               
             }
         }
        component.set("v.enqLineItems",enqLineItemsList);

       // alert('i am calling:'+enqLineItemsList[message].category3);

    }

    
    
})
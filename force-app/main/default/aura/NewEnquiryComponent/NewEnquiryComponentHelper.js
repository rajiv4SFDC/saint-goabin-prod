({
    showAdLineItenBtn: function(component,event) {
        
        var enquRecId = component.get("v.EnquiryId");
        var enqLineItems = component.get("v.enqLineItems");
        
        component.set("v.showAddLineItem", false);
        if(enquRecId && enqLineItems.length == 0) {
            
            component.set("v.showAddLineItem", true);
        }  //address__c
    },
    
    callExistingEnq : function(component,event,returnMsg) { 
        
        // var fetchAccountRecs22 = component.get("c.fetchAccountRecs2");
        // $A.enqueueAction(fetchAccountRecs22);
        var checkHideCond = component.get("v.showEdit");
        
        // checking for existing record is loaded.
        var EnquiryId = component.get("v.EnquiryId");
        
        // get the existing lineitems records.
        //      var getLineItemsIds = component.get("c.getLineItemsIds");
        //   getLineItemsIds.setParams({"enqRecId":EnquiryId});
        //   getLineItemsIds.setCallback(this,function(a){
        //            component.set("v.lineItemIds",a.getReturnValue());                  
        //     console.log(JSON.stringify(a.getReturnValue())+'44444444444');                        
        //  });
        //  $A.enqueueAction(getLineItemsIds);
        
        
        
        /***********
        var getReadOnlyLineItemData = component.get("c.getReadOnlyLineItemData");
        getReadOnlyLineItemData.setParams({"enqRecId":EnquiryId});
        getReadOnlyLineItemData.setCallback(this,function(a){
           //  alert('coming here');
            component.set("v.enqLineItemsViewList",a.getReturnValue());
            console.log('The wrap list222222222222 :'+JSON.stringify(a.getReturnValue()));
        });
        $A.enqueueAction(getReadOnlyLineItemData);
        *****************/
        
        
        
        if(returnMsg && returnMsg != 'undefined')
            EnquiryId = returnMsg; 
        console.log('The EnquiryId is 111:'+EnquiryId);
        var oldEnq = component.get("c.getEnquiryRecord");
        if(!$A.util.isUndefinedOrNull(EnquiryId))
        {
            //get the data from apex class and display in page. 
            
            oldEnq.setParams({"recId":EnquiryId});
            oldEnq.setCallback(this,function(a){
                //  alert('77777'+JSON.stringify(a.getReturnValue())); 
                var returnEnq = a.getReturnValue();
                //if(returnEnq.Bill_to_customer__r.Ship_to_customer__c == true)
                //returnEnq.Ship_To_Customer__r.Name = returnEnq.Bill_to_customer__r.Name;
                
                
				component.set("v.enqName",returnEnq.Name);
				
                console.log('The returnEnq is : ' +JSON.stringify(returnEnq));
                console.log('unfinding :'+returnEnq.Sales_Office__c+'...............'+returnEnq.submitted__c);
                component.set("v.submitedStatus",returnEnq.submitted__c);
                component.set("v.regionVal",returnEnq.Sales_Office__c);
				
			//	if(returnEnq.Bill_to_customer__r.Address__c)
            //    component.set("v.accAddressVal",returnEnq.Bill_to_customer__r.Address__c);
                
                if(returnEnq.Glass_Direct_Enquiry__c == true)
                component.set("v.glass_diirect_enq","true");
                else
                component.set("v.glass_diirect_enq","false");    
				
				
                
				if(returnEnq && returnEnq.Bill_to_customer__c)
                {
                component.set("v.billToStr", returnEnq.Bill_to_customer__c);
				
              //  alert('testing'+returnEnq.Bill_to_customer__r.Name);
			  if(returnEnq.Bill_to_customer__r.Glass_reach_account_name__c	)
                component.set("v.billToStrName",returnEnq.Bill_to_customer__r.Glass_reach_account_name__c	);
               
               if(returnEnq.Ship_To_Customer__c && returnEnq.Ship_To_Customer__r.Address__c)
                component.set("v.ShipToAddress",returnEnq.Ship_To_Customer__r.Address__c);     

                   if(returnEnq.Bill_to_customer__r.Address__c)
                component.set("v.accAddressVal",returnEnq.Bill_to_customer__r.Address__c);				
                }
				
				
                // alert('The checkHideCond is :' +checkHideCond);
                if(checkHideCond == "true")
                {
                    //  alert(component.find("regionVar"));
                    if(component.find("regionVar"))
                        component.find("regionVar").set("v.value", a.getReturnValue().Sales_Office__c);
                  //  component.set("v.ShipToAddress",a.getReturnValue().Ship_To_Customer__r.Address__c);
                    
                    //  alert(component.find("regionVar"));
                    this.fetchAccountRecsHelper(component,event);  
                    var response  = a.getReturnValue();
					
					
                  //  alert('testing22'+returnEnq.Bill_to_customer__c);
                    if(response.Bill_to_customer__c)
                    component.set("v.billToStr", response.Bill_to_customer__c);
                     //alert('testing22'+returnEnq.Bill_to_customer__r.Name+'....'+returnEnq.Bill_to_customer__r.Glass_reach_account_name__c);
					if(response.Bill_to_customer__r.Glass_reach_account_name__c	)
                    component.set("v.billToStrName",response.Bill_to_customer__r.Glass_reach_account_name__c);
					
					if(response.Bill_to_customer__c)
					{
					if(response.Bill_to_customer__r.Address__c)
                component.set("v.accAddressVal",response.Bill_to_customer__r.Address__c);
                     }
					 
                    console.log('The value is the :'+response.Bill_to_customer__c);
                    //component.set("billtoVar", response.Bill_to_customer__c);
                    this.fetchShipToCustomerHelper(component,event,response.Bill_to_customer__c);
                    component.set("v.shipToStr",response.Ship_To_Customer__c);
                    component.set("v.regionVal",response.Sales_Office__c);
                    
                    // alert('The response.Ship_To_Customer__c is :'+response.Ship_To_Customer__c);
                    //component.set("shitptoVar", response.Ship_To_Customer__c);
                    component.set("v.Glass_Direct_Enquiry", response.Glass_Direct_Enquiry__c);
					
					if(response.Ship_To_Customer__c && response.Ship_To_Customer__r.Address__c)
					{
					 component.set("v.ShipToAddress",response.Ship_To_Customer__r.Address__c);
					 }
                 //   alert('coming here');
                    console.log('status is:' +response.Status__c);
                    
                    // alert('coming here');
                    
                    /**  
                     if(response.Status__c == true) 
                    component.set("v.AccStatus","Active");
                    else 
                    component.set("v.AccStatus","InActive");  
                    ****/
                    //  alert(component.get("v.billToStr"));
                }
                else
                {
				//alert('testing22'+returnEnq.Bill_to_customer__r.Name+'....'+returnEnq.Bill_to_customer__r.Glass_reach_account_name__c);
                    //   component.set("v.Enquiry",a.getReturnValue());
                    component.set("v.Enquiry",returnEnq);
                }
                
                
            });
        }
        $A.enqueueAction(oldEnq);
        
        
        var displayExistingLineItems2 = component.get("c.displayExistingLineItems");
        displayExistingLineItems2.setParams({"enqRecId":EnquiryId});
        displayExistingLineItems2.setCallback(this,function(a){
             // alert('coming here');
            //   alert("a.getReturnValue()"+JSON.stringify(a.getReturnValue()));
            component.set("v.enqLineItems",a.getReturnValue());
            this.showAdLineItenBtn(component, event);  
            console.log('The wrap list :'+JSON.stringify(a.getReturnValue()));
        });
        $A.enqueueAction(displayExistingLineItems2);
        
        
        
    },
    fetchAccountRecsHelper : function(component, event) {
        
        console.log(' i am calling raja222:');
         this.showSpinner(component, event);
        
        // var selectedSalesArea = event.getSource().get("v.value"); 
        var selectedSalesArea = '';
         var accAddMap ={};  
        /*****************************************************************************************************/
         var ifORS = component.get("v.isORS");
        selectedSalesArea =component.get("v.orsVal"); 
       
        if(component.find("regionVar")) 
        {
             
            if(ifORS == 'NOT ORS')
            selectedSalesArea = component.find("regionVar").get("v.value"); 
            //selectedSalesArea =  event.getSource().get("v.value");
        }  
              
           // alert('The coming selectedSalesArea is:'+selectedSalesArea);
            var fetchAccounts = component.get("c.getBillToCustomers2");
            //  alert('The selectedSalesArea is :' +selectedSalesArea);
            fetchAccounts.setParams({"salesarea":selectedSalesArea,"ifORS":ifORS});
            fetchAccounts.setCallback(this,function(a){
                
                console.log('The a.getReturnValue() is:'+JSON.stringify(a.getReturnValue()));
              if(a.getReturnValue().length>0)
              {
                var accListVar = [];    
                 
                   accListVar.push({'sobjectType':'Account','Glass_reach_account_name__c':'-None-'});
                 for(var i=0;i<a.getReturnValue().length;i++)
                 {
                 //  alert(JSON.stringify(a.getReturnValue()));
                  //  var nameval = JSON.stringify(a.getReturnValue([i]).Glass_reach_account_name__c);
                     accListVar.push(a.getReturnValue()[i]);
                  //   alert(a.getReturnValue()[i].Address__c);
				  if(a.getReturnValue()[i].Id)
				  {
                     accAddMap[a.getReturnValue()[i].Id] = a.getReturnValue()[i].Address__c;
                     component.set("v.accList",accListVar);
                     component.set("v.accAddressMap",accAddMap);
					 }
                 }   
                  
                 //alert('before is:L'+JSON.stringify(component.get("v.accAddressMap")));                 
                component.set('v.accList',accListVar);
               // alert('adter is:L'+JSON.stringify(component.get('v.accList')));   
               
               // setting the new address for bill to customer. 
                   var EnquiryId = component.get("v.EnquiryId");
                //  alert('The EnquiryId is :'+EnquiryId);
                  if(EnquiryId)
                  {
                      if(accAddMap && accAddMap[component.get("v.billToStr")])
                          component.set("v.Enquiry.Bill_to_customer__r.Address__c",accAddMap[component.get("v.billToStr")]);
                   //  alert('The EnquiryId is :'+accAddMap[component.get("v.billToStr")]);
                  }
                                              

              }
                else
                {
                     var accListVar = [];    
                   accListVar.push({'sobjectType':'Account','Glass_reach_account_name__c':'-None-'});
                    component.set('v.accList',accListVar);
                }
                this.hideSpinner(component, event);
            });   
            $A.enqueueAction(fetchAccounts);      
        
        
    },
    fetchShipToCustomerHelper: function(component,event,billtoId){
        this.showSpinner(component, event);
        // var selectAccId = event.getSource().get("v.value"); 
        //  var selectAccId = component.find("billtoVar").get("v.value");
        var selectAccId;
        console.log('the billtoId is :'+billtoId);
        if(billtoId !="undefined")
            selectAccId = billtoId;
        var selectRec = event.getSource(); 
        console.log('The selectAccId'+selectAccId+'.........'+selectRec);
        var accList = component.get("v.accList");
        for (var i = 0; i < accList.length; i++) {
            console.log('The status isL'+accList[i].Status__c);
			if(accList[i].Id)
			{
            if(selectAccId != "undefined" && selectAccId == accList[i].Id)
            {
                if(accList[i].Status__c == true) 
                    component.set("v.AccStatus","Active");
                else 
                    component.set("v.AccStatus","InActive");  
                break;
            }  
			}
        }
        if(selectAccId && selectAccId !='-None-')
        {
        //    alert('coming here'+selectAccId);
        var getShipTo = component.get("c.getShipToCustomers");
        getShipTo.setParams({"accountId":selectAccId});
        getShipTo.setCallback(this,function(a){
            var map = a.getReturnValue();
          //  alert(JSON.stringify(map['SHIPTOLIST']));
            component.set('v.shipList', map['SHIPTOLIST']);     
            component.set('v.ShipToAddressMap', map['SHIPADDRESSMAP']);
            console.log('The ship list is :'+JSON.stringify(a.getReturnValue())+'....'+selectAccId);
            this.hideSpinner(component, event);
        });
        $A.enqueueAction(getShipTo);
        }
        else{
            var accListVar = [];      
       //  accListVar.push({'sobjectType':'Account','Name':'-None-'});      
          component.set("v.shipList","");    
             this.hideSpinner(component, event);
        }
            
    },
    createObjectData: function(component, event) {
        // get the contactList from component and add(push) New Object to List  
        var RowItemList = component.get("v.enqLineItems");
        //   alert('the enqLineItems count '+RowItemList.length);
         this.showSpinner(component, event);
        // getting the default empty row for enquiry line item.
        var wraperdata = component.get("c.getEmptyLineItem");
        wraperdata.setCallback(this,function(a){
            RowItemList.push(a.getReturnValue());
            //     alert(JSON.stringify(a.getReturnValue()));
            //      alert('the RowItemList lenght is :'+RowItemList.length);
            component.set("v.enqLineItems", RowItemList);
            
            this.showAdLineItenBtn(component,event);
            this.hideSpinner(component, event);
        });
        $A.enqueueAction(wraperdata);
        // console.log('the RowItemList is :'+RowItemList.count());
        // set the updated list to attribute (contactList) again    
        
    },
    
    gotoURL : function (component, event, recordId) {
        var device = $A.get("$Browser.formFactor");
        console.log('redirect url id:'+recordId);
        if(device == 'DESKTOP') {
            // sforce.one.navigateToURL('/apex/Enquiry_lighning_component?id=a0XN0000003dlAa');
            sforce.one.navigateToURL('/lightning/r/Enquiry__c/'+recordId+'/view');
            
        } else {
             /****
            var urlEvent = $A.get("e.force:navigateToURL");
            urlEvent.setParams({
                "url": "/"+recordId
            });
            urlEvent.fire();
			*****/
			
			    sforce.one.navigateToSObject(recordId);
        }
    },
    
    gotoURLNewPage : function (component, event) {
        var device = $A.get("$Browser.formFactor");
        // console.log('redirect url id:'+recordId);
        if(device == 'DESKTOP') {
            // sforce.one.navigateToURL('/apex/Enquiry_lighning_component?id=a0XN0000003dlAa');
            sforce.one.navigateToURL('/lightning/o/Enquiry__c/new');
            
        } else {
            var urlEvent = $A.get("e.force:navigateToURL");
            urlEvent.setParams({
                "url": "/"+recordId
            });
            urlEvent.fire();
        }
    },
    
    populateAccStatus : function(component,event,EnqId)
    {
        var getAccStatus = component.get("c.getAccStatus");
        getAccStatus.setParams({"EnqId":EnqId});
        getAccStatus.setCallback(this,function(a){
            component.set("v.AccStatus",a.getReturnValue());
            console.log('The acc status is:'+JSON.stringify(a.getReturnValue())+'....'+EnqId);
        });
        $A.enqueueAction(getAccStatus);
    },
    
    saveEnqRecord :function(component,event)
    {
	 // alert('coming 1st alert:');
        var saveEnquiry = component.get("c.saveEnquiryRec");
        var regionVal = '';
        
         var ifORS = component.get("v.isORS");
        regionVal =component.get("v.orsVal"); 
       
        if(component.find("regionVar")) 
        {
             
            if(ifORS == 'NOT ORS')
            regionVal = component.find("regionVar").get("v.value"); 
            //selectedSalesArea =  event.getSource().get("v.value");
        }  
        
          
        var billtoVal = component.find("billtoVar").get("v.value");
        var shitptoVal = component.find("shitptoVar").get("v.value");
        
        var billToStr2 = component.get("v.billToStr");
        var shipToStr2 = component.get("v.shipToStr");
     //   alert('coming 2nd alert:');
        var result ='';
        var resultId ='';
        
        console.log('the values are :'+regionVal+'....'+billtoVal+'....'+shitptoVal);
        if(regionVal =='-None-')
            alert('please select sales area');
        else if(billtoVal =='-None-' && shitptoVal =='-None-')
            alert('Please change bill to customer & ship to customer. Else please click on cancel to go back');        
            else if(billtoVal =='-None-')
                alert('please select bill to customer');
                else if(shitptoVal =='-None-')
                    alert('please select ship to customer');
        
                    else
                    {
                        
                        var enquRecId = component.get("v.EnquiryId");
                        console.log('The values are the 123 '+regionVal+'...'+billtoVal+'...'+shitptoVal);
                        console.log('The enquRecId is :'+enquRecId);
                        if(!enquRecId)
                        {
                            
                            saveEnquiry.setParams({"region":regionVal,"billToCustomer":billtoVal,"shipToCustomer":shitptoVal});
                            saveEnquiry.setCallback(this,function(a){
                                var returnMsg = JSON.stringify(a.getReturnValue());
                                var returnmsg1 = a.getReturnValue();
                              //  alert('The returnmsg1 is :'+returnmsg1);
                                //  alert(returnMsg);
                                //  console.log('The returnMsg is :'+returnMsg);
                                if(returnMsg.includes("Exception"))
                                {
                                    console.log('The exception came'+returnMsg); 
                                }
                                else
                                {
                                    component.set("v.EnquiryId", a.getReturnValue());   
                                    component.set("v.showEdit","false");
                                    // console.log(component.get("v.EnquiryId")+component.get("v.EnquiryId"));
                                    this.callExistingEnq(component, event,returnMsg);
                                    this.gotoURL(component,event,returnmsg1);
                                }
                            });
                            $A.enqueueAction(saveEnquiry);
                        }
                        else
                        {
                            var billToStr1 = component.get("v.billToStr");
                            var shipToStr1 = component.get("v.shipToStr");
                            //  alert('coming into else part'+billtoVal+'...'+billToStr1+'...'+shipToStr1);
                            var enqRec = {sobjectType:"Enquiry__c", 
                                          Id:enquRecId,
                                          Sales_Office_ERP__c:regionVal,
                                          Bill_to_customer__c:billToStr1,
                                          Ship_To_Customer__c:shipToStr1
                                         }; 
                            
                            //  alert('the 123123123'+JSON.stringify(enqRec));
                            
                            var updateEnqRecordVar = component.get("c.updateEnqRecord");
                            updateEnqRecordVar.setParams({"enquRecId":enquRecId,"regionVal":regionVal,"billToStr1":billtoVal,"shipToStr1":shitptoVal});
                            updateEnqRecordVar.setCallback(this,function(a){
                                console.log('The a is :' + JSON.stringify(a.getReturnValue()));
                                result = a.getReturnValue();
								if(a.getReturnValue() && a.getReturnValue().Id)
                                resultId = a.getReturnValue().Id;
                                console.log(resultId+'...resultId');
                                component.set("v.showEdit","false");
                                this.callExistingEnq(component, event);
                                this.populateAccStatus(component,event,resultId);
                            });	
                            $A.enqueueAction(updateEnqRecordVar);	
                            // if(result.Comments__c.includes('exception'))
                            //   alert('Error thown while updating Enquiry record:'+ result.Comments__c);
                        }
                        component.set("v.showEdit","false");
                        this.callExistingEnq(component, event);
                        this.populateAccStatus(component,event,resultId);
                        this.CancelRecordHelper(component,event);  
                    }  
    },
   
	showSpinner : function(component,event){
        component.set("v.toggleSpinner", true);  
    },
    
    hideSpinner : function(component,event){
        component.set("v.toggleSpinner", false);
    },
    
     CancelRecordHelper: function(component,event)
    {
        var enquRecId = component.get("v.EnquiryId");
        if(enquRecId)
        {          
            this.gotoURL(component,event,enquRecId);        
        }
        else
        {
            this.gotoURLNewPage(component,event);        
        }
        
        component.set("v.showEdit","false");
        
    }

    // Name
    
})
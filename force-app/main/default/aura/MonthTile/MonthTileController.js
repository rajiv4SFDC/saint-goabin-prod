({  
     doInit : function(component, event) {
         component.set("v.Spinner", true); 
         component.set("v.editMode",false); 
         //console.log("MonthTileHolder loaded");
         
         var req = component.get("c.getProductUOM");
         req.setParams({"oppProductId": component.get("v.oppProdID")});
         req.setCallback(this,function(res) {
             
             var state = res.getState(); //Checking response status
             if (component.isValid() && state === "SUCCESS") {
                 component.set("v.productUOM", res.getReturnValue());
             }
         })
         $A.enqueueAction(req);
         
    },
         
    getMonthList : function(component, event, helper) {
        component.set("v.Spinner", true); 
        component.set("v.editMode",false);
      //console.log("MonthTileHolder {getMonthList}");
        component.set("v.isDisabled",true);
        var monthlst = event.getParam("monthList");
        if(monthlst.length==0){
            component.set("v.isDisabled",false);
        }
        else{
              component.set("v.Spinner", true);
        }
        component.set("v.serverLst",monthlst);
        
        if(monthlst[0] != null){
            component.set("v.currentYr" ,monthlst[0].Year__c);
            var startYr = monthlst[0].Year__c;            
           // console.log("Start Year" + startYr); 
        }else{
            var d = new Date();
            var startYr = d.getFullYear();
            component.set("v.currentYr" ,startYr);
            //console.log("Uninitialised list with start year :"+startYr);
        }
    
        var monthdict = { 0:'JAN',
                         1:'FEB',
                         2:'MAR',
                         3:'APR',
                         4:'MAY',
                         5:'JUN',
                         6:'JUL',
                         7:'AUG',
                         8:'SEP',
                         9:'OCT',
                         10:'NOV',
                         11:'DEC'            
                        }
        
        var yeardict = {};
       // console.log("Monthlist before yeardict initially-"+monthlst[0]);
       if(monthlst.length >= 1){
            for(var i = 0; i < monthlst.length; i++){                
                if(monthlst[i].Year__c in yeardict){
                    var x = yeardict[monthlst[i].Year__c];
                  //  console.log("X --> "+x);
                    x.push(monthlst[i]);
                }
                else{
                  //  console.log("Creating yeardict:"+ monthlst[i]);
                    var val = [];
                    val.push(monthlst[i]);
                   // console.log("VAL --> " + val);
                    yeardict[monthlst[i].Year__c] = val;
                   // console.log(yeardict[monthlst[i].Year__c]);
                }
            }
        }
        else{
      
                
        }
      
        var lst =[];
        if(Object.keys(yeardict).length > 0) {      
        lst = yeardict[startYr];
      //  console.log("Result value in lst from yeardict : "+ JSON.stringify(yeardict));
        }else{
           // console.log("Creating forecasts for this product for the first time");
        }
        var objwrap ={id:0 , month:"" , year:"" , quantity:0 , optyprod:"", balance:0, appropriable:0 };
        var result = [];
        for(var i = 0,j=0; i<12 ; i++){
            if(lst.length && monthdict[i] == lst[j].Month__c){               
                var objwrap ={id:i , month:lst[j].Month__c , year:startYr , quantity:lst[j].Quantity__c , optyprod:"", balance:parseInt(lst[j].Quantity__c-lst[j].Total_Appropriable_Quantity__c), appropriable:parseInt(lst[j].Total_Appropriable_Quantity__c)};
                if(j<(lst.length -1)){
                    j++ ;
                }
                
            }else{
              //  console.log("Inside else :"+monthdict[i]);
                var objwrap ={id:i , month:monthdict[i], year:startYr , quantity:0 , optyprod:"",balance:0, appropriable:0};
            }
            result.push(objwrap);
        }
        
     //   console.log("dumped result list - "+JSON.stringify(result));
        var finalMap = {};
        finalMap[startYr] = result;
        //console.log("Server List for current year- "+JSON.stringify(yeardict[startYr]));
        component.set("v.serverMap",yeardict);
      //  console.log("dumped result list server - "+JSON.stringify(yeardict));
        component.set("v.resultMap",finalMap);
        component.set("v.cancelMap",finalMap);
        //console.log("Result list for current year from MAP- "+JSON.stringify(finalMap[startYr]));
        component.set("v.forecastLst",result); 
       // console.log("Result list for current year- "+JSON.stringify(result));
       component.set("v.Spinner", false);
       
    } ,
  
   editForecast : function(component, event, helper) {  
        //var result = component.get("v.resultMap");         
       // console.log("Changed EditForecast Called");        
       //var t = result[curYr];
       // component.set("v.Spinner",true);
        var curYr =component.get("v.currentYr");
        var optyProd = component.get("v.optyProdId"); 
        
        var item = component.get("v.forecastLst");

        var selectmap = {};     

        var updates = [];    

        component.set("v.notDisabled",true);
        component.set("v.editMode",true);
        var  myEvent = $A.get("e.c:saveFlagEvent");
        myEvent.setParams({"flag": false});
        myEvent.fire();  
       // component.set("v.Spinner",false);  
     },
 
    select : function(component, event, helper) {
       // console.log("dumped result list server - "+JSON.stringify(component.get("v.serverMap")));
        //console.log("MonthTileHolder {select}");       
        var selectedItem = event.currentTarget;
        var recId = selectedItem.dataset.record;
        component.set("v.record",recId);
       // console.log("Id to be edited-" + recId);
        var monthdict = { 0:'JAN',
                         1:'FEB',
                         2:'MAR',
                         3:'APR',
                         4:'MAY',
                         5:'JUN',
                         6:'JUL',
                         7:'AUG',
                         8:'SEP',
                         9:'OCT',
                         10: 'NOV',
                         11:'DEC'            
                        }
        var curYr = component.get("v.currentYr");
       // console.log("iN SELECT, CURRENT YEAR IS :" + curYr);
        var check = component.get("v.resultMap");
        //console.log("Selecting a month card by resultmap :"+JSON.stringify(check[curYr]));
        var lst = check[component.get("v.currentYr")];
       // console.log("Selecting a month card :"+JSON.stringify(lst));
        var forecast = component.get("v.forecastLst");
        var selMonth = monthdict[recId];
       // console.log("Selecting a month  :"+selMonth);
        var qty = lst[recId].quantity;
       // console.log("Selecting a month card quantity:"+qty);
        var server = component.get("v.serverMap");
    
        //console.log("Setting modal value from maPPPPPPPPP :"+JSON.stringify(lst[recId]));       
        component.set("v.isOpen", true); 
        var mod= component.get("v.modal");
     
    },
    
    retrieveoptyproductId : function(component,event) {
      // console.log("MonthTileHolder {retrieveoptyproductId}");
       component.set("v.Spinner", true); 
        var id = event.getParam("prodrecID");
        component.set("v.optyProdId", id);
        if(id!=null && id!='undefined' && id.length!=0){
           // console.log("MonthTileHolder {retrieveoptyproductId} ID obtained from {OptyProdList}"+component.get("v.optyProdId"));
            var action = component.get("c.getOptyProdId");
            action.setParams({
                "optyprodId": id
            });
           
            action.setCallback(this, function(a) {
                var state = a.getState();
               // console.log("MonthTileHolder {retrieveoptyproductId} getOptyProdId()");
                if(JSON.stringify(a.getReturnValue())!= null && JSON.stringify(a.getReturnValue())!= 'undefined'){
                    var x = JSON.parse(a.getReturnValue()); 
                   // console.log("MonthTileHolder {retrieveoptyproductId} getOptyProdId() Estimated Quantity Inside IF Block :"+x.Estimated_Quantity__c);
                    component.set("v.optyProd", x);
                    if(x.Estimated_Quantity__c == 0 || x.Estimated_Quantity__c == 'undefined'){
                        component.set("v.totalQuantity", parseInt(0));
                        //console.log("Before setting Inside retrieve opty,displayQty value :----------"+component.get("v.displayQuantity"));
                        component.set("v.displayQuantity", parseInt(0));  
                        component.set("v.cancellationTotal", parseInt(0)); 
                        //console.log("After setting Inside retrieve opty,displayQty value :----------"+component.get("v.displayQuantity"));
             //console.log("Total qty value Inside retrieve opty,totalQty value :----------"+component.get("v.totalQuantity"));
                        component.set("v.deltaQuantity", parseInt(0));
                    }
                    else{
                       // console.log("MonthTileHolder {retrieveoptyproductId} getOptyProdId() Estimated Quantity Inside ELSE Block :"+x.Estimated_Quantity__c);
                        component.set("v.totalQuantity", x.Estimated_Quantity__c);
                        //console.log("Total quantity--------Inside func"+x.Estimated_Quantity__c);
                        component.set("v.deltaQuantity",x.Estimated_Quantity__c);
                        component.set("v.displayQuantity",x.Estimated_Quantity__c);  
                        component.set("v.cancellationTotal", x.Estimated_Quantity__c);                       
                    }
                    component.set("v.thickness",x.Thickness__c);  
                   // component.set("v.prodStatus",x.Status__c);  
                   // console.log("Delta quantity--------Inside func"+component.get("v.deltaQuantity"));
                   // console.log("Total quanity--------"+component.get("v.totalQuantity"));
                   // console.log("Ton quanity--------"+x.Ton__c);
                }
            });
            
            //console.log("Delta's old value==========="+component.get("v.deltaQuantity");
            //console.log("Total quantity--------"+component.get("v.totalQuantity"));
            $A.enqueueAction(action);  
        }
    },
    
     populateHeader : function(component,event) {
        component.set("v.openedFromOptyProd",true);
       // console.log("MonthTileHolder {populateHeader}");
        var id = event.getParam("prodrecID");
        component.set("v.optyProdId", id);
        if(id!=null && id!='undefined'){
            var action = component.get("c.getOptyProdId");
            action.setParams({
                "optyprodId": id
            });
           
            action.setCallback(this, function(a) {
               // console.log("MonthTileHolder {populateHeader} getOptyProdId()");
                var state = a.getState();
                //console.log("Status-----------------"+state);
                if(JSON.stringify(a.getReturnValue())!= null && JSON.stringify(a.getReturnValue())!= 'undefined'){
                    var x = JSON.parse(a.getReturnValue()); 
                    //console.log("getOptyProdId inside MonthTileHolder x val-"+JSON.stringify(x));
                    component.set("v.optyProd", x);
                    if(x.Estimated_Quantity__c == 0){
                        component.set("v.totalQuantity", x.Estimated_Quantity__c);
                        //console.log("Before setting Inside retrieve opty,displayQty value :----------"+component.get("v.displayQuantity"));
                        component.set("v.displayQuantity",x.Estimated_Quantity__c);  
                        //console.log("After setting Inside retrieve opty,displayQty value :----------"+component.get("v.displayQuantity"));
            // console.log("Total qty value Inside retrieve opty,totalQty value :----------"+component.get("v.totalQuantity"));
                        component.set("v.deltaQuantity",x.Estimated_Quantity__c);
                        component.set("v.cancellationTotal", x.Estimated_Quantity__c);   
                    }
                    else{
                        component.set("v.totalQuantity", x.Estimated_Quantity__c);
                      //  console.log("Total quantity--------Inside func"+x.Estimated_Quantity__c);
                        component.set("v.deltaQuantity",x.Estimated_Quantity__c);
                        component.set("v.displayQuantity",x.Estimated_Quantity__c);  
                        component.set("v.cancellationTotal", x.Estimated_Quantity__c);                         
                    }
                    component.set("v.prodStatus",x.Status__c);  
                    component.set("v.thickness",x.Thickness__c);          
                    //console.log("Delta quantity--------Inside func"+component.get("v.deltaQuantity"));
                    //console.log("Total quanity--------"+component.get("v.totalQuantity"));
                    //console.log("Ton quanity--------"+x.Ton__c);    
                }
            });
            
            //console.log("Delta's old value==========="+component.get("v.deltaQuantity");
            //console.log("Total quanity--------"+component.get("v.totalQuantity"));
            $A.enqueueAction(action);  
        }
    },
    validate  : function(component, event, helper) {
        //console.log("MonthTileHolder {validate} ");
        //console.log('Validating');
        var inp = component.get("v.displayQuantity");
        var num = /^[0-9]+$/;

        if(isNaN(inp)){
            component.set("v.displayQuantity", parseInt(0));
            component.set("v.totalQuantity",parseInt(0));
            //console.log("Total Qty entered wrong======================"+inp);
            //console.log("Correction======================"+component.get("v.totalQuantity"));
        }
        else if(!isNaN(inp)&&num.test(inp)){
            // var x = component.find("totalqty").get("v.value");
             component.set("v.totalQuantity", parseInt(inp));
             component.set("v.displayQuantity", parseInt(inp));
             //console.log("Total Qty entered correct======================"+parseInt(inp));
        }
        else if(inp<0){
            var toastEvent = $A.get("e.force:showToast");
            toastEvent.setParams({
                "title": "Invalid quantity value!",
                "type": "error",
                "message": "Quantity value can't be a value less than 0."
            });
            toastEvent.fire();
            component.set("v.displayQuantity", parseInt(inp));
            component.set("v.totalQuantity",parseInt(0));
            //console.log("Total Qty entered wrong======================"+inp);
            //console.log("Correction======================"+component.get("v.totalQuantity"));
        }
        else if(inp.length==0){
            component.set("v.displayQuantity", null);
            component.set("v.totalQuantity",parseInt(0));
            //console.log("Total Qty entered wrong======================"+inp);
            //console.log("Correction======================"+component.get("v.totalQuantity"));
        }  
        //console.log("Qty entered ======================"+ component.get("v.displayQuantity"));
    },

    validateModal  : function(component, event, helper) {
    
    	var recId = event.currentTarget.id;
        component.set("v.record",recId);
       // console.log("Id to be edited-" + recId);
        var monthdict = { 0:'JAN',
                         1:'FEB',
                         2:'MAR',
                         3:'APR',
                         4:'MAY',
                         5:'JUN',
                         6:'JUL',
                         7:'AUG',
                         8:'SEP',
                         9:'OCT',
                         10: 'NOV',
                         11:'DEC'            
                        }
        var curYr = component.get("v.currentYr");
        // console.log("iN SELECT, CURRENT YEAR IS :" + curYr);
        var check = component.get("v.resultMap");
        //console.log("Selecting a month card by resultmap :"+JSON.stringify(check[curYr]));
        var lst = check[component.get("v.currentYr")];
        //console.log("Selecting a month card from current year:"+JSON.stringify(lst));
        var forecast = component.get("v.forecastLst");
        var selMonth = monthdict[recId];
        // console.log("Selecting a month  :"+selMonth);
        var qtyval = lst[recId].quantity;
        //console.log("Selecting a month card quantity:"+JSON.stringify(lst[recId].quantity));
        //console.log("Validate Modal qty value :"+qtyval);

        if(qtyval == '' || qtyval == null || qtyval == 'undefined'){
           // console.log("Setting oldqty");
            component.set("v.oldQuantity",0);
        }
        else component.set("v.oldQuantity",qtyval);
     //  console.log("After setting Validate Modal qty value :"+component.get("v.oldQuantity"));
      // console.log("MonthTileHolder {validateModal} ");
      // console.log("Id :"+ component.get("v.record"));  
       var id = event.currentTarget.id;
     //  console.log(event.currentTarget.value);
        
        var qty = event.currentTarget.value;
        qty = qty.replace(/\s/g, "");
        event.currentTarget.value = qty;

      //  console.log("Trimmed value:"+event.currentTarget.value);
         
        var num = /^([0-9]+)$/;
        
       if(isNaN(qty)){       
           // console.log("Check 1");    
              //  event.currentTarget.value = parseInt(0);  
            var negativeArr = component.get("v.negativeCount");
            negativeArr[id] = 0;
            component.set("v.negativeCount",negativeArr);
         //   console.log("Negative check array======================"+ component.get("v.negativeCount"));
            component.set("v.negativeCheck",0);   
         //   console.log("Total Qty entered incorrect in modal======================"+ JSON.stringify(qty));   
            var updatedValue = parseInt(0);    
            lst[recId].quantity = updatedValue;
            component.set("v.resultMap", check);   
            component.set("v.invalidQuantityFlag",true);
            component.set("v.updatedQuantity",parseInt(0));   
            var val = parseInt(component.get("v.deltaQuantity")) - component.get("v.oldQuantity");
            component.set("v.deltaQuantity",val);  
           // console.log("Subtracting negative from Pending :"+val);
        }           
        else if(qty<0 || (!isNaN(qty) && qty.toString().indexOf('.') != -1)){
        	//console.log("Check 3");  
            component.set("v.invalidQuantityFlag",true);
            // event.currentTarget.value = '';
            var updatedValue = parseInt(0);  
            lst[recId].quantity = updatedValue;
            component.set("v.updatedQuantity",parseInt(0));   
            component.set("v.resultMap", check);   
            var negativeArr = component.get("v.negativeCount");
            negativeArr[id] = 0;
            component.set("v.negativeCount",negativeArr);
          //  console.log("Negative check array======================"+ component.get("v.negativeCount"));
            component.set("v.negativeCheck",0);   
          //  console.log("Total Qty entered incorrect in modal======================"+ JSON.stringify(qty));
      
            var val = parseInt(component.get("v.deltaQuantity")) - component.get("v.oldQuantity");
            component.set("v.deltaQuantity",val);  
           // console.log("Subtracting negative from Pending :"+val);
            
        }                
        else if(qty.length==0){ 
          //  console.log("Check 4");          
            event.currentTarget.value = '';      
            var updatedValue = parseInt(0);   
            lst[recId].quantity = updatedValue;
            component.set("v.resultMap", check);   
            component.set("v.updatedQuantity",parseInt(0));
            var negativeArr = component.get("v.negativeCount");
            negativeArr[id] = 1;
            component.set("v.negativeCount",negativeArr);
            //console.log("Negative check array======================"+ component.get("v.negativeCount"));
    
             var diff = component.get("v.updatedQuantity") - parseInt(component.get("v.oldQuantity"));
        if(diff != 0){
        var val = parseInt(component.get("v.deltaQuantity")) + parseInt(diff);
        component.set("v.deltaQuantity",val);         

        var flagAlreadyInUpdateList =false;      
        
            var item = component.get("v.updateLst");
            lst[recId].optyprod = component.get("v.optyProdId");

            for(var i=0;i < item.length ;i++){
            //Due to binding if any forecast's quantity already in updateLst is modified, it is automatically reflected in the updateLst
              if(lst[recId].id==item[i].id && lst[recId].year==item[i].year){
               // item[i].quantity = t[recId].quantity;
                flagAlreadyInUpdateList =true;
              }

             // console.log("Inside Loop flagAlreadyInUpdateList :" + flagAlreadyInUpdateList);
            }
            if(!flagAlreadyInUpdateList){
              item.push(lst[recId]);
            }
           // console.log("flagAlreadyInUpdateList :" + flagAlreadyInUpdateList);
            component.set("v.updateLst",item);
           // console.log("Update List :" + JSON.stringify(component.get("v.updateLst")));
        }
    }
         else if(!isNaN(qty)&&num.test(qty)){  
          //  console.log("Check 2");
            event.currentTarget.value = parseInt(qty);
         //   console.log("In check-2 :"+event.currentTarget.value);
            lst[recId].quantity = parseInt(qty);
            component.set("v.resultMap", check);   
            var negativeArr = component.get("v.negativeCount");
            negativeArr[id] = 1;
            component.set("v.negativeCount",negativeArr);
         //   console.log("Negative check array======================"+ component.get("v.negativeCount"));
            var x =1 ;
            for(var i = 0 ; i < negativeArr.length ; i++){
                x = x & negativeArr[i];
            }
            component.set("v.negativeCheck",x);
          //  console.log("Negative check value:"+component.get("v.negativeCheck"));
            component.set("v.updatedQuantity",parseInt(qty));   

            //Calculating Pending value and updating UpdateLst
            
          //    console.log("Old Qty======================"+component.get("v.oldQuantity"));
      //  console.log("New Qty======================"+component.get("v.updatedQuantity"));

        if(component.get("v.oldQuantity") == 'undefined'){
            component.set("v.oldQuantity",parseInt(0));
        }

       var diff = component.get("v.updatedQuantity") - parseInt(component.get("v.oldQuantity"));
        if(diff != 0){
        var val = parseInt(component.get("v.deltaQuantity")) + parseInt(diff);
        component.set("v.deltaQuantity",val);         

        var flagAlreadyInUpdateList =false;      
        
            var item = component.get("v.updateLst");
            lst[recId].optyprod = component.get("v.optyProdId");

            for(var i=0;i < item.length ;i++){
            //Due to binding if any forecast's quantity already in updateLst is modified, it is automatically reflected in the updateLst
              if(lst[recId].id==item[i].id && lst[recId].year==item[i].year){
               // item[i].quantity = t[recId].quantity;
                flagAlreadyInUpdateList =true;
              }

            //  console.log("Inside Loop flagAlreadyInUpdateList :" + flagAlreadyInUpdateList);
            }
            if(!flagAlreadyInUpdateList){
              item.push(lst[recId]);
            }
          //  console.log("flagAlreadyInUpdateList :" + flagAlreadyInUpdateList);
            component.set("v.updateLst",item);
         //   console.log("Update List :" + JSON.stringify(component.get("v.updateLst")));
      } 
  }
  else{
      //  console.log("Inside else block");
         var negativeArr = component.get("v.negativeCount");
            negativeArr[id] = 0;
            component.set("v.negativeCount",negativeArr);
         //   console.log("Negative check array======================"+ component.get("v.negativeCount"));
            component.set("v.negativeCheck",0);   
          //  console.log("Total Qty entered incorrect in modal======================"+ JSON.stringify(qty));   
            var updatedValue = parseInt(0);    
            lst[recId].quantity = updatedValue;
            component.set("v.resultMap", check);   
            component.set("v.invalidQuantityFlag",true);
            component.set("v.updatedQuantity",parseInt(0));   
            var val = parseInt(component.get("v.deltaQuantity")) - component.get("v.oldQuantity");
            component.set("v.deltaQuantity",val);  
         //   console.log("Subtracting negative from Pending :"+val);
      }     
  
       
    },
    
     backForecast :  function(component) {
     // console.log("MonthTileHolder {backForecast} ");
        var optyId = component.get("v.optyID");
            var navEvt = $A.get("e.force:navigateToSObject");
            navEvt.setParams({
                "recordId": optyId                
            });
            navEvt.fire();
  },
    
     cancelForecast :  function(component) {

      var cancelFlag = component.get("v.preventMultipleCancel");  

    //  console.log("MonthTileHolder {cancelForecast} ");
      if(cancelFlag == true){
      component.set("v.preventMultipleCancel",false);
     // component.set("v.Spinner", true);
        var monthlst = component.get("v.serverLst");   
         
           // console.log("Returned from DB :" + JSON.stringify(monthlst));
        
        if(monthlst[0] != null){
            component.set("v.currentYr" ,monthlst[0].Year__c);
            var startYr = monthlst[0].Year__c;            
           // console.log("Start Year" + startYr); 
        } else{
            var d = new Date();
            var startYr = d.getFullYear();
            component.set("v.currentYr" ,startYr);
            //console.log("Uninitialised list with start year :"+startYr);
        }
    
        var monthdict = { 0:'JAN',
                         1:'FEB',
                         2:'MAR',
                         3:'APR',
                         4:'MAY',
                         5:'JUN',
                         6:'JUL',
                         7:'AUG',
                         8:'SEP',
                         9:'OCT',
                         10:'NOV',
                         11:'DEC'            
                        }
        
        var yeardict = {};
        //console.log("Monthlist before yeardict initially-"+monthlst[0]);
       if(monthlst.length >= 1){
            for(var i = 0; i < monthlst.length; i++){                
                if(monthlst[i].Year__c in yeardict){
                    var x = yeardict[monthlst[i].Year__c];
                  //  console.log("X --> "+x);
                    x.push(monthlst[i]);
                }
                else{
                   // console.log("Creating yeardict:"+ monthlst[i]);
                    var val = [];
                    val.push(monthlst[i]);
                   // console.log("VAL --> " + val);
                    yeardict[monthlst[i].Year__c] = val;
                   // console.log(yeardict[monthlst[i].Year__c]);
                }
            }
        }
        else{      
                
        }

        var lst =[];
        if(Object.keys(yeardict).length > 0) {      
        lst = yeardict[startYr];
      //  console.log("Result value in lst from yeardict : "+ JSON.stringify(yeardict));
        }else{
           // console.log("Creating forecasts for this product for the first time");
        }
        var objwrap ={id:0 , month:"" , year:"" , quantity:0 , optyprod:"", balance:0, appropriable:0};
        var result = [];
        for(var i = 0,j=0; i<12 ; i++){
            if(lst.length && monthdict[i] == lst[j].Month__c){
               // console.log("Inside if :"+monthdict[i]);
                var objwrap ={id:i , month:lst[j].Month__c , year:startYr , quantity:lst[j].Quantity__c , optyprod:"",balance:parseInt(lst[j].Quantity__c-lst[j].Total_Appropriable_Quantity__c), appropriable:parseInt(lst[j].Total_Appropriable_Quantity__c)};
                if(j<(lst.length -1)){
                    j++ ;
                }
                
            }else{
              //  console.log("Inside else :"+monthdict[i]);
                var objwrap ={id:i , month:monthdict[i], year:startYr , quantity:0 , optyprod:"", balance:0, appropriable:0};
            }
            result.push(objwrap);
        }
        
     //   console.log("dumped result list - "+JSON.stringify(result));
        var finalMap = [];
        finalMap[startYr] = result;
      //  console.log("dumped result list - "+JSON.stringify(finalMap[startYr]));
        component.set("v.serverMap",yeardict);
      //  console.log("dumped result list server - "+JSON.stringify(yeardict));
        component.set("v.resultMap",finalMap);
        component.set("v.cancelMap",finalMap);
      //  console.log("dumped result list result- "+JSON.stringify(finalMap[startYr]));
        component.set("v.forecastLst",result); 
      //  console.log("dumped result list forecast- "+JSON.stringify(result));

       var x = [];
      component.set("v.updateLst",x);
      
      var cancellationTotal = component.get("v.cancellationTotal");
     
      component.set("v.totalQuantity",cancellationTotal);
       component.set("v.displayQuantity",cancellationTotal);
      component.set("v.deltaQuantity",cancellationTotal);

      //Resetting invalid quantity flag array 
        var negativeArr = component.get("v.negativeCount");
        for(var i = 0;i<negativeArr.length;i++)
        negativeArr[i] = 1;

        component.set("v.negativeCount",negativeArr);
     //   console.log("Negative check array======================"+ component.get("v.negativeCount"));

      
        var  myEvent = $A.get("e.c:saveFlagEvent");
        myEvent.setParams({"flag": true });
        myEvent.fire();     
        
      
        component.set("v.notDisabled",false);
       //  component.set("v.Spinner", false);
         component.set("v.preventMultipleCancel",true);
         component.set("v.editMode",false);
       }
         else if(cancelFlag==false){

         }
  },
    
   
    saveForecast :  function(component) {
     
     // console.log("MonthTileHolder {saveForecast} ");
      var negativeCheck =component.get("v.negativeCheck");

      var save = component.find("pending").get("v.value");
      var prodid = component.get("v.optyProdId");
      var optyId = component.get("v.optyID");
      var saveFlag = component.get("v.preventMultipleSaveCancel");
     // console.log("Save flag value" , saveFlag);
      var checkForEmptyUpdateList = component.get("v.updateLst");
     // console.log("Inside SAVE updatelist value :"+ JSON.stringify(checkForEmptyUpdateList));
      if(save != 0){
        component.set("v.pendingFlag",true);
      }
     else if((checkForEmptyUpdateList == 'undefined' || checkForEmptyUpdateList == null || checkForEmptyUpdateList == '')&&negativeCheck){
              component.set("v.saveFlag",true);
              var x = component.get("v.optyProdId");
              var  myEvent = $A.get("e.c:saveFlagEvent");
              myEvent.setParams({"flag": true,"savedProdId" :x});
              myEvent.fire();  
              component.set("v.notDisabled",false); 
              component.set("v.editMode",false); 
      }else if(checkForEmptyUpdateList != 'undefined' && checkForEmptyUpdateList != null && negativeCheck){
        if(save==0 && !save &&saveFlag==true){
             component.set("v.Spinner", true);
          component.set("v.preventMultipleSaveCancel",false);
        //  console.log("Save flag value Inside" , component.get("v.preventMultipleSaveCancel"));
          var cancellationTotal = component.get("v.totalQuantity");
            //sever call
       // console.log("Check 1");
        
            // console.log("Complete forecasting done!!!!!!!!!-"+save);
        var updatesList = JSON.stringify(component.get("v.updateLst"));
      // var updatesList = component.get("v.updateLst");
       // console.log("Stringified list--------------" +updatesList);
     
        var action = component.get("c.saveMonthForecasts");
         action.setParams({
            "months": updatesList
        });
       // console.log("Check 2");    
        action.setCallback(this, function(a) {
            var state = a.getState();
         //   console.log("MonthTileHolder {saveForecast}  saveMonthForecasts()");
            //console.log("Saved Records"+JSON.stringify(JSON.parse(a.getReturnValue())));
            var x = JSON.parse(a.getReturnValue());

            if(x.errorFlag){

            component.set("v.errorMessageValue",x.errorMessage);
            component.set("v.genericErrorFlag",true);
            component.set("v.preventMultipleSaveCancel",true); 
            }else if(!x.errorFlag && x.savedList!='undefined'){

           var emptyArry = [];      	
		  component.set("v.updateLst",emptyArry);			
          component.set("v.saveFlag",true);
           
      //   console.log("Check 3");      
         component.set("v.serverLst",x.savedList);
        
    //  console.log("Error message from Save - "+x.errorMessage);
            
              //Handling client side data
             component.set("v.isDisabled",true);
        var monthlst = component.get("v.serverLst");

     //   console.log("Forecasts list in month tile holder"+JSON.stringify(monthlst));

        if(monthlst[0] != null){
           // component.set("v.currentYr" ,monthlst[0].Year__c);
          //  var startYr = monthlst[0].Year__c;       
            var startYr = component.get("v.currentYr");     
       //     console.log("Start Year" + startYr); 
        }else{
            var d = new Date();
            //var startYr = d.getFullYear();
           // component.set("v.currentYr" ,startYr);
            var startYr = component.get("v.currentYr");
           // console.log("Uninitialised list with start year :"+startYr);
        }
    
        var monthdict = { 0:'JAN',
                         1:'FEB',
                         2:'MAR',
                         3:'APR',
                         4:'MAY',
                         5:'JUN',
                         6:'JUL',
                         7:'AUG',
                         8:'SEP',
                         9:'OCT',
                         10: 'NOV',
                         11:'DEC'            
                        }
        
        var yeardict = {};
       // console.log("Monthlist before yeardict initially-"+monthlst[0]);
      // console.log("Check 4");
       if(monthlst.length >= 1){
            for(var i = 0; i < monthlst.length; i++){                
                if(monthlst[i].Year__c in yeardict){
                    var x = yeardict[monthlst[i].Year__c];
                  //  console.log("X --> "+x);
                    x.push(monthlst[i]);
                }
                else{
                    //console.log("Creating yeardict:"+ monthlst[i]);
                    var val = [];
                    val.push(monthlst[i]);
                   // console.log("VAL --> " + val);
                    yeardict[monthlst[i].Year__c] = val;
                   // console.log(yeardict[monthlst[i].Year__c]);
                }
            }
        }
        else{
      
                
        }
      //  console.log("Check 5");
        var lst =[];
        if(Object.keys(yeardict).length > 0) {      
        lst = yeardict[startYr];
      //  console.log("Result value in lst from yeardict : "+ JSON.stringify(yeardict));
        }else{
           // console.log("Creating forecasts for this product for the first time");
        }
   //     console.log("Check 6");
        var objwrap ={id:0 , month:"" , year:"" , quantity:0 , optyprod:"", balance:0, appropriable:0};
        var result = [];
        for(var i = 0,j=0; i<12 ; i++){
            if(lst.length && monthdict[i] == lst[j].Month__c){
                var objwrap ={id:i , month:lst[j].Month__c , year:startYr , quantity:lst[j].Quantity__c , optyprod:"", balance:parseInt(lst[j].Quantity__c-lst[j].Total_Appropriable_Quantity__c), appropriable:parseInt(lst[j].Total_Appropriable_Quantity__c)};
                if(j<(lst.length -1)){
                    j++ ;
                }
                
            }else{
              //  console.log("Inside else :"+monthdict[i]);
                var objwrap ={id:i , month:monthdict[i], year:startYr , quantity:0 , optyprod:"", balance:0, appropriable:0};
            }
            result.push(objwrap);
        }
        
     //   console.log("dumped result list - "+JSON.stringify(result));
        var finalMap = {};
        finalMap[startYr] = result;
        //console.log("Server List for current year- "+JSON.stringify(yeardict[startYr]));
        component.set("v.serverMap",yeardict);
      //  console.log("dumped result list server - "+JSON.stringify(yeardict));
        component.set("v.resultMap",finalMap);
        component.set("v.cancelMap",finalMap);
       // console.log("Result list for current year from MAP- "+JSON.stringify(finalMap[startYr]));
        component.set("v.forecastLst",result); 
       // console.log("Result list for current year- "+JSON.stringify(result));      
        var checkScreen = component.get("v.openedFromOptyProd");
        if(checkScreen){
     //     console.log("MonthTileHolder {saveForecast} Screen is of optyProd:"+checkScreen);
          $A.get('e.force:refreshView').fire();
        }          
            
    //    console.log("Check 7");     
        component.set("v.notDisabled",false);
              var x = component.get("v.optyProdId");
              var  myEvent = $A.get("e.c:saveFlagEvent");
              myEvent.setParams({"flag": true,"savedProdId" :x});
              myEvent.fire();   
       //component.set("v.Spinner", false);          
       component.set("v.preventMultipleSaveCancel",true);      
       component.set("v.editMode",false);   
       component.set("v.cancellationTotal", cancellationTotal);     
            }

            //Client side data handling ends here
          component.set("v.Spinner", false);    
        });
            $A.enqueueAction(action);
        
            //Server side Function call ends here

   //      console.log("Check 8");
        }        
        else if((save>0 || save<0) && save != 'undefined'){        
            component.set("v.pendingFlag",true);         
        }
        else if(saveFlag == false){

        }     
     }
      else if(!negativeCheck){
        component.set("v.invalidQuantityFlag",true);
      }

        
  },
    
    getPrevYr : function(component, event){
        component.set("v.Spinner", true); 
          var negativeCheck =component.get("v.negativeCheck");
        if(negativeCheck){
        
    //  console.log("MonthTileHolder {getPrevYr}");
         var prevYr = parseInt(component.get("v.currentYr")) - 1;
        component.set("v.currentYr",prevYr);
       // console.log("Prev Year value-"+prevYr);
        var monthdict = { 0:'JAN',
                         1:'FEB',
                         2:'MAR',
                         3:'APR',
                         4:'MAY',
                         5:'JUN',
                         6:'JUL',
                         7:'AUG',
                         8:'SEP',
                         9:'OCT',
                         10: 'NOV',
                         11:'DEC'            
                        }
        var check =component.get("v.resultMap");
        
        if(check[prevYr]){            
             //console.log("Inside resultMap---"+JSON.stringify(check[prevYr]));
             component.set("v.forecastLst",check[prevYr]);
        }
        else{
       // console.log("Inside serverMap");
        var check = component.get("v.serverMap");
        //console.log("Server Map :"+JSON.stringify(check));
        var objwrap ={id:0 , month:"" , year:"" , quantity:0 , optyprod:"", balance:0, appropriable:0};
        var result = [];
        if(prevYr in check)
        { var lst = check[prevYr];
        // console.log("Next year list length"+lst.length);
            for(var i = 0,j=0; i<12 ; i++){
            if(monthdict[i] == lst[j].Month__c){
                //console.log("Inside if :"+monthdict[i]);
                var objwrap ={id:i , month:lst[j].Month__c , year:prevYr , quantity:lst[j].Quantity__c , optyprod:"" , balance:parseInt(lst[j].Quantity__c-lst[j].Total_Appropriable_Quantity__c), appropriable:parseInt(lst[j].Total_Appropriable_Quantity__c)};
                if(j<(lst.length -1)){
                    j++ ;
                }
                
            }else{
                //console.log("Inside if :"+monthdict[i]);
                var objwrap ={id:i , month:monthdict[i], year:prevYr , quantity:0 , optyprod:"", balance:0, appropriable:0};
            }
            result.push(objwrap);
        }
        }  
        else{
            var lst = [];
             for(var i = 0; i<12 ; i++){       
                var objwrap ={id:i , month:monthdict[i] , year:prevYr , quantity:0, optyprod:"", balance:0, appropriable:0};
             result.push(objwrap);
             }
        }
       
        var finalMap = component.get("v.resultMap");
        finalMap[prevYr] = result;
        component.set("v.forecastLst",result);  
        }

 }
 else if(!negativeCheck){
 component.set("v.invalidQuantityFlag",true);
 }
 component.set("v.Spinner", false);
    },
    
    getNextYr : function(component, event){
        component.set("v.Spinner", true); 
        var negativeCheck =component.get("v.negativeCheck");
        if(negativeCheck){
        
    //  console.log("MonthTileHolder {getNextYr}");
       // var monthlst = component("monthList");
        var nxtYr = parseInt(component.get("v.currentYr")) + 1;
        component.set("v.currentYr",nxtYr);
        //console.log("Next Year value-"+nxtYr);
        var monthdict = { 0:'JAN',
                         1:'FEB',
                         2:'MAR',
                         3:'APR',
                         4:'MAY',
                         5:'JUN',
                         6:'JUL',
                         7:'AUG',
                         8:'SEP',
                         9:'OCT',
                         10: 'NOV',
                         11:'DEC'            
                        }
        var check =component.get("v.resultMap");
        if(check[nxtYr]){
           //  console.log("Inside resultMap");
             component.set("v.forecastLst",check[nxtYr]);
        }
        else{
       // console.log("Inside serverMap");
        var check = component.get("v.serverMap");
       // console.log("Server Map :"+JSON.stringify(check));
        var objwrap ={id:0 , month:"" , year:"" , quantity:0 , optyprod:"", balance:0, appropriable:0};
        var result = [];
        if(nxtYr in check)
        { var lst = check[nxtYr];
        // console.log("Next year list length"+lst.length);
            for(var i = 0,j=0; i<12 ; i++){
            if(monthdict[i] == lst[j].Month__c){
                //console.log("Inside if :"+monthdict[i]);
                var objwrap ={id:i , month:lst[j].Month__c , year:nxtYr , quantity:lst[j].Quantity__c , optyprod:"", balance:parseInt(lst[j].Quantity__c-lst[j].Total_Appropriable_Quantity__c), appropriable:parseInt(lst[j].Total_Appropriable_Quantity__c)};
                if(j<(lst.length -1)){
                    j++ ;
                }
                
            }else{
                //console.log("Inside if :"+monthdict[i]);
                var objwrap ={id:i , month:monthdict[i], year:nxtYr , quantity:0 , optyprod:"", balance:0, appropriable:0};
            }
            result.push(objwrap);
        }
        }  
        else{
            var lst = [];
             for(var i = 0; i<12 ; i++){       
                var objwrap ={id:i , month:monthdict[i] , year:nxtYr , quantity:0, optyprod:"", balance:0, appropriable:0};
             result.push(objwrap);
             }
        }
       
        var finalMap = component.get("v.resultMap");
        finalMap[nxtYr] = result;
        component.set("v.forecastLst",result);  
        }     
 }
 else if(!negativeCheck){
    component.set("v.invalidQuantityFlag",true);
 }
 component.set("v.Spinner", false); 
    },

    closeSaveFlag  : function(component, event){
        component.set("v.saveFlag",false);
    },

     closeGenericFlag  : function(component, event){
        component.set("v.genericErrorFlag",false);
    },

     closePendingFlag  : function(component, event){
        component.set("v.pendingFlag",false);
    },

    closeinvalidQuantityFlag  : function(component, event){
        component.set("v.invalidQuantityFlag",false);
    },

    waiting: function(component, event, helper) {
    document.getElementById("Accspinner").style.display = "block";
 },
 
   doneWaiting: function(component, event, helper) {
   document.getElementById("Accspinner").style.display = "none";
 }
 
})
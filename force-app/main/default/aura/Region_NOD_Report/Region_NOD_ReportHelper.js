({
    
    geSelectedMonth : function(component,event,helper)  {
        var action2 = component.get("c.selectedMonth");
        var inputselmonth = component.find("selectedMonthDynamic");
        var opts1=[];
        var opts2 =[];
         opts2.push('-None-');
        action2.setCallback(this, function(a){
        	for(var i=0;i< a.getReturnValue().length;i++){
                opts1.push({"class": "optionClass", label: a.getReturnValue()[i], value: a.getReturnValue()[i]});
                console.log('The month val is:'+a.getReturnValue()[i]);
                opts2.push(a.getReturnValue()[i]);
            }
           // inputselmonth.set("v.options", opts1); 
            var opts3 =[];
            opts3 = opts2.reverse();
            console.log('The opts2 is :'+opts2.reverse());
            component.set("v.monthList",opts3);
        });
        $A.enqueueAction(action2);
    }
    
,
	geSelectedYear : function(component,event,helper)  {
        var action3 = component.get("c.selectedYear");
        var inputselYear = component.find("selectedYearDynamic");
        var opts2=[];
        var opts3=[];
        opts3.push('-None-');
        action3.setCallback(this, function(a){
        	for(var i=0;i< a.getReturnValue().length;i++){
                opts2.push({"class": "optionClass", label: a.getReturnValue()[i], value: a.getReturnValue()[i]});
                 console.log('The year val is:'+a.getReturnValue()[i]);
                opts3.push(a.getReturnValue()[i]+'');
            }
           // inputselYear.set("v.options", opts2); 
           component.set("v.yearList",opts3);
        });
        $A.enqueueAction(action3);
    }
})
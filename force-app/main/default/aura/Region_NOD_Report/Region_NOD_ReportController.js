({
	searchRecords : function(component, event, helper) {
		
        var seletedRegion = component.get("v.region");
        var selectedMonthByUser = component.get("v.SelectedMonth");
        var selectedYearByUser = component.get("v.SelectedYear");
	  
 var action1 = component.get("c.fetchRecord");
        action1.setParams({
            "seletedRegion":seletedRegion,
            "Month":selectedMonthByUser,
            "Year":selectedYearByUser
            
        });
        action1.setCallback(this, function(response){
            var state1 = response.getState();
            if(state1 === "SUCCESS"){
                console.log('debug in state ='+state1);
                if(response.getReturnValue() && response.getReturnValue()!='')
                component.set("v.RegionRecordList", response.getReturnValue());  
                else
                    alert('No records found with this criteria');
            }
        });
        $A.enqueueAction(action1);
        
    
    }
,
	doInit:function(component,event,helper){
        helper.geSelectedMonth(component, event, helper);   
        helper.geSelectedYear(component, event, helper);
	}    
})
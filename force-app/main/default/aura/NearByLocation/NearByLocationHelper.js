({    
    fetchNearByProjects : function(component, event, helper){
       
        var latitude = component.get("v.latitude");
        var longitude = component.get("v.longitude");
        var currentProjId = component.get("v.projectRecordID");
        var projRadius = component.get("v.projectRadius");
        
        var action = component.get('c.getNearByProjectsFrmDB');

        action.setParams({
            Lat : latitude,
            Lon : longitude,
            compare : projRadius,
            currentProjectId : currentProjId
        });
       
        action.setCallback(this, function(response){
            var state = response.getState();
            console.log('state:', state);
            if (state == "SUCCESS") {
                var resObj = JSON.parse(response.getReturnValue());
                    if(typeof resObj != "undefined" && resObj.length > 0){
                         component.set("v.projectObjLst",resObj);
                          /* Unhide Warning Divs */
                         //helper.unHideWarningDiv(component, event, helper);
                         helper.showMessageDivWithError(component, event, helper);
                    }else{
                         /* Unhide Success Divs */
                         //helper.unHideSuccessDiv(component, event, helper);
                         helper.showMessageDivWithSuccess(component, event, helper);
                    }
        
                /* Stamp queried near location to event attributes */
                helper.fireCallNearByLocationEvent(component, event, helper);
            }
            
        });
        
        $A.enqueueAction(action);
    },
    
    fireCallNearByLocationEvent : function(component, event, helper){
        
        var compEvent = component.getEvent("nearByProjectEvent");
        compEvent.setParams({"projectObjLst" : component.get("v.projectObjLst") });
        compEvent.fire();
    },

    unHideWarningDiv : function(component, event, helper){
        var targetDiv = component.find("duplicateWarningDiv"); 

        $A.util.removeClass(targetDiv[0], 'slds-hide');
        $A.util.removeClass(targetDiv[1], 'slds-hide');

    },

    unHideSuccessDiv : function(component, event, helper){
        var targetDiv = component.find("unHideSuccessDiv"); 
        $A.util.removeClass(targetDiv, 'slds-hide');
        //helper.showSuccessToast(component, event, helper);

    },

    showMessageDivWithError : function(component, event, helper){
         //var msgtxt = "The record you're about to update with Geo Tag looks like a duplicate. Open an existing record from list instead?";
         var msgtxt = component.get("v.windowErrorMsg"); 
         component.set("v.windowMsg",msgtxt);

         var targetDiv = component.find("messageDiv"); 
         $A.util.removeClass(targetDiv, 'slds-hide');

         var defaultNearLocationOn = component.get("v.defaultNearLocationOn");
             
             if(defaultNearLocationOn){
                 var targetDiv = component.find("duplicateListDiv"); 
                 $A.util.removeClass(targetDiv, 'slds-hide');

                 var targetDiv = component.find("hideLink"); 
                 $A.util.removeClass(targetDiv, 'slds-hide'); 
             }else{
                
                var targetDiv = component.find("viewLink"); 
                $A.util.removeClass(targetDiv, 'slds-hide');
             }
    },

    showMessageDivWithSuccess : function(component, event, helper){
         //var msgtxt = "No  potential duplicate found.";
         var msgtxt = component.get("v.windowSuccessMsg"); 
         component.set("v.windowMsg",msgtxt);
         var targetDiv = component.find("messageDiv"); 
         $A.util.removeClass(targetDiv, 'slds-hide');
    },

})
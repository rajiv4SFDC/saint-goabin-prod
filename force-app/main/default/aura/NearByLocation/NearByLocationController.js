({
	getNearByProjects : function(component, event, helper) {
          var params = event.getParam('arguments');
            if (params) {
                if(params.geolocationObj.longitude !== undefined && params.geolocationObj.latitude !== undefined){
                   
                    component.set("v.latitude",params.geolocationObj.latitude);
                    component.set("v.longitude",params.geolocationObj.longitude);
                    component.set("v.projectRecordID",params.projectRecordID);
                    component.set("v.defaultNearLocationOn",params.defaultNearLocationOn);
                    component.set("v.projectRadius",params.projectRadius);
                    helper.fetchNearByProjects(component, event, helper);
                }
            }
	},
    
    navToRecordDetails : function(component, event, helper){
       
      	var currentProjId = event.target.getAttribute('data-projectId');
         
        var navEvt = $A.get("e.force:navigateToSObject");
        navEvt.setParams({
          "recordId": currentProjId,
          "slideDevName": "detail"
        });
        navEvt.fire();
    },

    navToUserDetails : function(component, event, helper){
       
        var currentProjId = event.target.getAttribute('data-projectOwnerId');
         
        var navEvt = $A.get("e.force:navigateToSObject");
        navEvt.setParams({
          "recordId": currentProjId,
          "slideDevName": "detail"
        });
        navEvt.fire();
    },
    
    showDuplicateListDiv : function(component, event, helper) {
         var targetDiv = component.find("duplicateListDiv"); 
         $A.util.removeClass(targetDiv, 'slds-hide');

         var targetDiv = component.find("viewLink"); 
         $A.util.addClass(targetDiv, 'slds-hide');

         var targetDiv = component.find("hideLink"); 
         $A.util.removeClass(targetDiv, 'slds-hide'); 
    },

    hideDuplicateListDiv : function(component, event, helper) {
         var targetDiv = component.find("duplicateListDiv"); 
         $A.util.addClass(targetDiv, 'slds-hide');

         var targetDiv = component.find("hideLink"); 
         $A.util.addClass(targetDiv, 'slds-hide'); 

         var targetDiv = component.find("viewLink"); 
         $A.util.removeClass(targetDiv, 'slds-hide');
    },

})
({
    // this method will call when invoice lookup value is change.
    itemsChange : function(component, event, helper) {
		var selectedValue = component.get("v.selectedLookUpInvoices").Name;	
        var allValues = [];
        var nameofInvoice, editCheckForCLearData;
        editCheckForCLearData = component.get("v.isEditMode");
        console.log('coming on 00000000000000000 ' + selectedValue);
        if(editCheckForCLearData == true) {	
            component.set("v.priceReqDefaultOption", "Choose PriceRequest");
            component.set("v.priceReqPandPDefaultOption", "Choose Price Request PandP");
        } else {
            // set null value in readonly fields when user cancel selected value in invoice lookup.
            component.set("v.setItemTonnage", '');
            component.set("v.setInvoiceRate", '');
            component.set("v.setQuantity", '');
            component.set("v.setThickness", '');
            component.set("v.productAutoFetchValue", '');
            component.set("v.setProductStock", '');
            component.set("v.displayAccountName", '');
        }
        // This block will split the invoice name and fetch related invoice from backend
        if(typeof(selectedValue) !== "undefined" && selectedValue !== null) {
            // prevent recursion of lookup popup
            if(component.get("v.preventRecursiveModalCheck") == true) {
                console.log('coming on edit inside recursion');
                component.set("v.preventRecursiveModalCheck", false);
                nameofInvoice = selectedValue.split('-');
                component.set("v.invoiceSelectedValue", nameofInvoice[0]);
                var action = component.get("c.getSelectedInvoices");
                action.setParams({'invoiceName' : nameofInvoice[0]});
                action.setCallback(this, function(response) {           
                    if (response.getState() == "SUCCESS") {              
                        allValues = response.getReturnValue();
                        
                        component.set("v.invoiceChooseRecords", allValues);
                        console.log('invoiceRecords is 33333' + JSON.stringify(component.get("v.invoiceChooseRecords")));
                        component.set("v.openModalCheck", true);
                        var cmpTarget = component.find('ModalChoosebox');
                        var cmpBack = component.find('ModalChoosebackdrop');
                        $A.util.addClass(cmpTarget, 'slds-fade-in-open');
                        $A.util.addClass(cmpBack, 'slds-backdrop--open'); 
                    }
                });
                $A.enqueueAction(action);
            }
            
        }
	},
    
    // call when page load, used for edit the page, fetching values of current record and setting in attributes to update.
    doInit : function(component, event, helper) {
        
        var idofCurrentEditPage = component.get("v.recordId");
        if(typeof(idofCurrentEditPage) != "undefined") {
            
            var allValues;
            console.log('idofCurrentEditPageIDdddd idofCurrentEditPageIDdddd'+ idofCurrentEditPage); 
            // set editmode as true if user click on edit  
            component.set("v.isEditMode", true);
            var action = component.get("c.getInsertedRecords");
            action.setParams({'sendCurrentRecordId' : component.get("v.recordId")});
            action.setCallback(this, function(response) {           
                if (response.getState() == "SUCCESS") {              
                    allValues = response.getReturnValue();
                    component.set("v.recordTobEdit", allValues);
                    console.log('============' + component.get("v.recordTobEdit[0].Name"));
                    component.set("v.setItemTonnage", component.get("v.recordTobEdit[0].Item_Tonnage__c"));
                    component.set("v.setInvoiceRate", component.get("v.recordTobEdit[0].Invoice_Rate__c"));
                    component.set("v.setQuantity", component.get("v.recordTobEdit[0].Quantity__c"));
                    component.set("v.setThickness", component.get("v.recordTobEdit[0].Thickness__c"));
                    component.set("v.setProductStock", component.get("v.recordTobEdit[0].Plant_Location__c"));
                    component.set("v.productAutoFetchValue", component.get("v.recordTobEdit[0].Product_Name__c"));
                    component.set("v.creditNotesTicketingList[0].Dimension__c", component.get("v.recordTobEdit[0].Dimension__c"));
                    component.set("v.creditNotesTicketingList[0].Remarks__c", component.get("v.recordTobEdit[0].Remarks__c"));
                    component.set("v.selectedLookUpEnquiry", component.get("v.recordTobEdit[0].Enquiry__c"));
                    component.set("v.selectedLookUpInvoices", component.get("v.recordTobEdit[0].Invoice__c"));
                    component.set("v.priceReqDefaultOption", component.get("v.recordTobEdit[0].Price_Request__r.Name"));
                    component.set("v.priceReqPandPDefaultOption", component.get("v.recordTobEdit[0].price_request_PandP__r.Name"));
                    component.set("v.displayAccountName", component.get("v.recordTobEdit[0].Account__r.Name"));
                    component.set("v.accountId", component.get("v.recordTobEdit[0].Account__c"));
                    
                    component.set("v.OldEnqNo",component.get("v.recordTobEdit[0].Enquiry__r.Name"));
                    component.set("v.OldInvoiceNo",component.get("v.recordTobEdit[0].Invoice__r.Name"));
                }
            });
            $A.enqueueAction(action);
        }
	},
    /*
    openChoosemodal : function(component, event, helper) {
        var selectedItem = event.currentTarget;
        var invoiceRec = selectedItem.dataset.record;
		invoiceRec = invoiceRec.split('###');
        var nameofInvoice = invoiceRec[0].split('-');
        var allValues = [];
        
        var action = component.get("c.getSelectedInvoices");
        action.setParams({'invoiceName' : nameofInvoice[0]});
        action.setCallback(this, function(response) {           
            if (response.getState() == "SUCCESS") {              
                allValues = response.getReturnValue();
                component.set("v.invoiceChooseRecords", allValues);
                console.log('invoiceChooseRecords is *********' + component.get("v.invoiceChooseRecords"));
                
                var cmpTarget = component.find('ModalChoosebox');
                var cmpBack = component.find('ModalChoosebackdrop');
                $A.util.addClass(cmpTarget, 'slds-fade-in-open');
                $A.util.addClass(cmpBack, 'slds-backdrop--open'); 
            }
        });
        $A.enqueueAction(action);
        component.set("v.openModalCheck", true);
	},*/
    closeChooseModal:function(component,event,helper) {
        var selectedItem = event.currentTarget;
        var invoiceRec = selectedItem.dataset.record;
        var allValues, allValuesPandP;
        invoiceRec = invoiceRec.split('###');
        component.set("v.selectedLookUpInvoices.Name", invoiceRec[0]);	// store invoice name
        component.set("v.selectedLookUpInvoices.Id", invoiceRec[1]);	// store invoice id
        component.set("v.selectedLookUpInvoices.Product__r.Name", invoiceRec[2]);	// store product name
        component.set("v.selectedLookUpInvoices.Item_Tonnage__c", invoiceRec[3]);
        component.set("v.selectedLookUpInvoices.Invoice_Rate__c", invoiceRec[4]);
        component.set("v.selectedLookUpInvoices.Quantity__c", invoiceRec[5]);
        component.set("v.selectedLookUpInvoices.Product__r.Thickness_mm__c", invoiceRec[6]);
        component.set("v.selectedLookUpInvoices.Product_Stock__r.Plant_Code__c", invoiceRec[7]);
        component.set("v.productId", invoiceRec[8]);	// store product id
        component.set("v.accountId", invoiceRec[9]);	// store account id
        
        // set value for Product to display to users on UI.
        component.set("v.productAutoFetchValue", invoiceRec[2]);
        component.set("v.displayAccountName", invoiceRec[10]); // display on page(UI)
        component.set("v.selctedProductRec", invoiceRec[2]);
        // set invoice value in attributes from selected lookup record.
        component.set("v.setItemTonnage", invoiceRec[3]);
        component.set("v.setInvoiceRate", invoiceRec[4]);
        component.set("v.setQuantity", invoiceRec[5]);
        component.set("v.setThickness", invoiceRec[6]);
        component.set("v.setProductStock", invoiceRec[7]);
        
        // server call to fetch price request records which have selected product
        var action = component.get("c.getPriceRequestRecords");
        action.setParams({'priceRequestRecords' : component.get("v.productId")});
        action.setCallback(this, function(response) {           
            if (response.getState() == "SUCCESS") {              
                allValues = response.getReturnValue();
                component.set("v.priceRequestPicklistSet", allValues);
                console.log('invoiceRecords is price req' + JSON.stringify(component.get("v.priceRequestPicklistSet")));
            }
        });
        $A.enqueueAction(action);
        
        // server call to fetch price request PandP records which have selected product
        var actionPandP = component.get("c.getPriceRequestPandPRecords");
        actionPandP.setParams({'priceRequestPandPRecords' : component.get("v.productId")});
        actionPandP.setCallback(this, function(response) {           
            if (response.getState() == "SUCCESS") {              
                allValuesPandP = response.getReturnValue();
                component.set("v.priceRequestPandPPicklistSet", allValuesPandP);
                console.log('invoiceRecords is pandP' + JSON.stringify(component.get("v.priceRequestPicklistSet")));
            }
        });
        $A.enqueueAction(actionPandP);
        
     //   console.log('invoicePopupValue:'+ component.get("v.invoicePopupValue"));   
        component.set("v.preventRecursiveModalCheck", true);
        var cmpTarget = component.find('ModalChoosebox');
        var cmpBack = component.find('ModalChoosebackdrop');
        $A.util.removeClass(cmpBack,'slds-backdrop--open');
        $A.util.removeClass(cmpTarget, 'slds-fade-in-open'); 
    },
    
    saveCreditNotes:function(component,event,helper) {
        helper.saveCreditNoteshelper(component,event,helper);
    }
})
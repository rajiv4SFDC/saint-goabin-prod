({
	
    saveCreditNoteshelper : function(component, event, helper) {
        var checkEditStatus = component.get("v.isEditMode");
        
        // To update functionality
        if(checkEditStatus == true) {
            console.log('iffffffffffffffffffffffffffffff');
            var creditNotesTicketList = component.get("v.creditNotesTicketingList");
            var getEnquiry = component.get("v.selectedLookUpEnquiry");
            var getPricePandP = component.get("v.selectedLookUpPriceRequestPP");
            var getInvoiceAll = component.get("v.selectedLookUpInvoices");
            
            console.log('getPricePandP edit helper --------- :'+ JSON.stringify(getPricePandP));
            
            // this code is for manipulation of undesired component error.
            component.set("v.creditNotesTicketingList[0].Name", 'sd');
            creditNotesTicketList[0].Name = component.get("v.creditNotesTicketingList[0].Name");
            
            console.log('getInvoiceAll edit--------- :'+ JSON.stringify(getInvoiceAll));
            
            if(typeof(getEnquiry) != "undefined" && getEnquiry != null) {
                creditNotesTicketList[0].Enquiry__c = getEnquiry.Id;
                console.log('getEnquiry ### --------- :'+ JSON.stringify(creditNotesTicketList[0].Enquiry__c));
            }
            
            if(typeof(getInvoiceAll) != "undefined" && getInvoiceAll != null) {
                creditNotesTicketList[0].Invoice__c = getInvoiceAll.Id;
                creditNotesTicketList[0].Item_Tonnage__c = component.get("v.setItemTonnage");
                creditNotesTicketList[0].Invoice_Rate__c = component.get("v.setInvoiceRate");
                creditNotesTicketList[0].Quantity__c = component.get("v.setQuantity");
                creditNotesTicketList[0].Product_Name__c = component.get("v.productAutoFetchValue");
                creditNotesTicketList[0].Account__c = component.get("v.accountId");
                creditNotesTicketList[0].Thickness__c = component.get("v.setThickness");
                creditNotesTicketList[0].setProductStock = component.get("v.setItemTonnage");
                console.log('creditNotesTicketList update ****** :'+ JSON.stringify(creditNotesTicketList));
            }
            
           
            var action = component.get("c.editCreditNotesTickt");
            action.setParams({'creditNotesTicketingListParam' : JSON.stringify(creditNotesTicketList), 'creditRecordID' : component.get("v.recordId")});
            action.setCallback(this, function(response){
                var state = response.getState();
                if(state == "SUCCESS") {
                    component.set("v.getCNRecordId", response.getReturnValue());
                    console.log('getCNRecordId ' + component.get('v.getCNRecordId'));
                    
                    this.showMessage('Credit Notes Ticketing was updated.', 'success', 'dismissible', 5000);
                    
                    // navigate to updated record page
                    var navEvt = $A.get("e.force:navigateToSObject");
                    navEvt.setParams({
                        "recordId": component.get('v.getCNRecordId'),
                        "slideDevName": "related"
                    });
                    navEvt.fire();
                    
                } else if(state == "ERROR") {
                    var errors = response.getError();
                    var errorMessage = '';
                    var updateId;
                    updateId = component.get("v.recordId");
                    if (errors) {
                        if (errors[0] && errors[0].message) {
                            errorMessage = errors[0].message;
                            console.log('errorMessage ' + errorMessage);
                            if(errorMessage.includes("first error: FIELD_CUSTOM_VALIDATION_EXCEPTION, ")) {
                                console.log('coming in edit if ' + errorMessage.indexOf("first error: FIELD_CUSTOM_VALIDATION_EXCEPTION, "));
                                console.log('coming in if for edit ' + errorMessage.lastIndexOf(": []"));
                                errorMessage = errorMessage.substring(errorMessage.indexOf("N,") + 2, errorMessage.lastIndexOf(": []"));
                                console.log('coming in if edit errorMessage ' + errorMessage);
                            }
                            console.log('coming outside edit errorMessage ' + errorMessage);
                            this.showMessage(errorMessage, 'error', 'pester', 5000);
                            return;
                        }
                    } else {
                        console.log("Unknown error");
                    }
                }
            });
            $A.enqueueAction(action);
        } else {
            // this block is for Insertion of credit notes ticketing records
            var creditNotesTicketList = component.get("v.creditNotesTicketingList");
            var getEnquiry = component.get("v.selectedLookUpEnquiry");
            var getPricePandP = component.get("v.selectedLookUpPriceRequestPP");
            var getInvoiceAll = component.get("v.selectedLookUpInvoices");
            var accountId;
            
            // this code is for manipulation of undesired component error.
            component.set("v.creditNotesTicketingList[0].Name", 'sd');
            creditNotesTicketList[0].Name = component.get("v.creditNotesTicketingList[0].Name");
            
            console.log('getInvoiceAll --------- :'+ JSON.stringify(getInvoiceAll));
            if(typeof(getEnquiry) != "undefined" && getEnquiry != null) {
                creditNotesTicketList[0].Enquiry__c = getEnquiry.Id;
            }
           	
            if(typeof(getInvoiceAll) != "undefined" && getInvoiceAll != null) {
                creditNotesTicketList[0].Invoice__c = getInvoiceAll.Id;
                creditNotesTicketList[0].Item_Tonnage__c = getInvoiceAll.Item_Tonnage__c;
                creditNotesTicketList[0].Invoice_Rate__c = getInvoiceAll.Invoice_Rate__c;
                creditNotesTicketList[0].Quantity__c = getInvoiceAll.Quantity__c;
                creditNotesTicketList[0].Product_Name__c = getInvoiceAll.Product__r.Name;
                creditNotesTicketList[0].Thickness__c = getInvoiceAll.Product__r.Thickness_mm__c;
                creditNotesTicketList[0].Plant_Location__c = getInvoiceAll.Product_Stock__r.Plant_Code__c;
                
            }
            accountId = component.get("v.accountId");
            if(typeof(accountId) != "undefined" && accountId != null) {
                creditNotesTicketList[0].Account__c = accountId;
                console.log('accountId 555 --------- :'+ accountId);
            }
            
            console.log('creditNotesTicketList ****** :'+ JSON.stringify(creditNotesTicketList));
            
            var action = component.get("c.saveCreditNotesTickt");
            action.setParams({'creditNotesTicketingListParam' : JSON.stringify(creditNotesTicketList)});
            action.setCallback(this, function(response){
                var state = response.getState();
                if(state == "SUCCESS") {
                    component.set("v.getCNRecordId", response.getReturnValue()); // setting newly created record id which is returned by apex method.
                    console.log('getCNRecordId ' + component.get('v.getCNRecordId'));
                    
                    this.showMessage('Credit Notes Ticketing was created.', 'success', 'dismissible', 5000);
                    
                    // navigate to newly created record standard page
                    var navEvt = $A.get("e.force:navigateToSObject");
                    navEvt.setParams({
                        "recordId": component.get('v.getCNRecordId'),
                        "slideDevName": "related"
                    });
                    navEvt.fire();
                    
                } else if(state == "ERROR") {
                    var errors = response.getError();
                    var errorMessage = '';
                    if (errors) {
                        if (errors[0] && errors[0].message) {
                            errorMessage = errors[0].message;
                            console.log('errorMessage ' + errorMessage);
                            if(errorMessage.includes("Insert failed. First exception on row 0; first error: FIELD_CUSTOM_VALIDATION_EXCEPTION, ")) {
                                console.log('coming in if ' + errorMessage.indexOf("Insert failed. First exception on row 0; first error: FIELD_CUSTOM_VALIDATION_EXCEPTION, "));
                                console.log('coming in if dfd ' + errorMessage.lastIndexOf(": []"));
                                errorMessage = errorMessage.substring(errorMessage.indexOf("N,") + 2, errorMessage.lastIndexOf(": []"));
                                console.log('coming in if errorMessage ' + errorMessage);
                            }
                            
                            // If Required field is missing
                            if(errorMessage.includes("Insert failed. First exception on row 0; first error: REQUIRED_FIELD_MISSING, ")) {
                                console.log('coming in required ' + errorMessage.lastIndexOf(":"));
                                errorMessage = errorMessage.substring(errorMessage.indexOf("G,") + 2, errorMessage.lastIndexOf(":"));
                                console.log('coming in if required errorMessage ' + errorMessage);
                            }
                            
                            console.log('coming outside errorMessage ' + errorMessage);
                            this.showMessage(errorMessage, 'error', 'pester', 5000);
                            return;
                        }
                    } else {
                        console.log("Unknown error");
                    }
                }
            });
            $A.enqueueAction(action);
        }
	},
    
    showMessage:function(msg, msgtype, display_mode, duration){
        
        if(!duration) {
            
            duration = 5000;
        }
        //display_mode: dismissible, pester, sticky 
        var toastEvent = $A.get("e.force:showToast");
        toastEvent.setParams({
            message: msg,
            messageTemplate: msg,
            duration:duration,
            key: 'info_alt',
            type: msgtype,
            mode: display_mode
        });
        toastEvent.fire();
    }
})
({
    doInit : function(component, event, helper) {
	    //Send LC Host as parameter to VF page so VF page can send message to LC; make it all dynamic
        component.set('v.lcHost', window.location.hostname);
  		//Add message listener
        window.addEventListener("message", function(event) {
            //Can enable origin control for more security
            //if (event.origin != vfOrigin) {
                //console.log('Wrong Origin');
                // Not the expected origin: Reject the message!
                //return;
            //}
            
            // Handle the message
            if(event.data.state == 'LOADED'){
                //Set vfHost which will be used later to send message
             console.log('data from vf 1 :: ', event.data);
            
                component.set("v.vfHost",event.data.vfHost);
                
                //Send data to VF page to draw map
                //helper.sendToVF(component, event, helper);
                var mapData = component.get("v.mapData");
                var mapOptionsCenter = component.get("v.mapOptionsCenter");

                if( typeof mapData !== "undefined" && typeof mapOptionsCenter !== "undefined"
                        && mapOptionsCenter != null ){
                     helper.sendToVF(component, event, helper);
                
                }else{
                      
                      var isGeoPresentChk = component.get("v.isGeoPresent");
                      console.log('geo presennnnnn :: ', component.get("v.isGeoPresent") );

                      if( isGeoPresentChk == false ){
                          var vfWindow = component.find("vfFrame").getElement().contentWindow;

                          var message = {
                                           "showGeoMissingError" : true,
                                        } ;
                          var objMsg = JSON.parse(JSON.stringify(message));              

                          vfWindow.postMessage(objMsg, component.get("v.vfHost"));
                      }
                    console.log("Some thing went wrong in Map.cmp doInit method or No Data has been pushed yet in Map.cmp");
                }
                
            }else if(event.data.mapLoadedDone == 'true'){
                 var trgSpinner = component.find("spinner");
                 $A.util.toggleClass(trgSpinner, "slds-hide");
            }


        }, false);
	},
    
	stampCurrentandRelatedGeo : function(component, event, helper) {
		 var params = event.getParam('arguments');
        if (params) {
            component.set("v.geolocationObj",params.geolocationObj);
            component.set("v.projectObjLst",params.projectObjLst);
            /* Call the google map vf to create map view*/
             helper.createMapData(component, event, helper);
             helper.createMapOptionsCenter(component, event, helper);

            //Send data to VF page to draw map
            var vfHostVal = component.get("v.vfHost");
             if( vfHostVal != '' ){
                 helper.sendToVF(component, event, helper);
             }else{
                console.log("vf host is nullll");
             }   
        }
	},

    geoTagMissingHandlerOnMap : function(component, event, helper) {

        //var trgSpinner = component.find("spinner");
        //$A.util.toggleClass(trgSpinner, "slds-hide");
        component.set("v.isGeoPresent",false);
    }
    
})
({
	sendToVF : function(component, event, helper) {
        //Prepare message in the format required in VF page
        var message = {
			            "loadGoogleMap" : true,
            			"mapData": component.get('v.mapData'), 
            			"mapOptions": component.get('v.mapOptions'),  
                       	'mapOptionsCenter': component.get('v.mapOptionsCenter')
        		} ;
        
        //Send message to VF
        helper.sendMessage(component, helper, message);
    },
    
    sendMessage: function(component, helper, message){
        //Send message to VF
        message.origin = window.location.hostname;
        var vfWindow = component.find("vfFrame").getElement().contentWindow;
        
        var objMsg = JSON.parse(JSON.stringify(message));
        console.log("from sendmsg method :: ",component.get("v.vfHost"));
        vfWindow.postMessage(objMsg, component.get("v.vfHost"));
        component.set("v.isMapLoaded",true);

        //var trgSpinner = component.find("spinner");
        //$A.util.toggleClass(trgSpinner, "slds-hide");
    },
    
    createMapData : function(component, event, helper){
        
        var projLst = component.get("v.projectObjLst");
        var locAry = [];
        
        for(var i=0;i< projLst.length;i++){
            var obj = {};
            obj.lat = projLst[i].Geolocation__c.latitude;
            obj.lng = projLst[i].Geolocation__c.longitude;
            obj.markerText = "<b>Project</b> : "+projLst[i].Name + "<br/><b>Sales Area : </b>"+projLst[i].Sales_Area__c;
            //obj.salesArea = projLst[i].salesArea;
            locAry.push(obj);        
        }
        
        component.set("v.mapData",locAry);
     
    },
    
    createMapOptionsCenter : function(component, event, helper){
        
        var currentGeoObj = component.get("v.geolocationObj");
        var obj = {};
        obj.lat = currentGeoObj.latitude ;
        obj.lng = currentGeoObj.longitude ;
        obj.ProjName = "<b>Project</b> : "+currentGeoObj.projectName +"<br/><b>Sales Area : </b>"+ currentGeoObj.salesArea;
        //obj.salesArea = currentGeoObj.salesArea;
        component.set("v.mapOptionsCenter",obj);  
    }
})
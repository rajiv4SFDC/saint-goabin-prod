({
	searchRecordHelper : function(component, event, helper) {
		var ToDate;
        var EndDate,selectedsoffice;
        //var inputsel = component.find("InputSelectDynamic");
        var selectedValue = component.get("v.Salesoffice");
        ToDate = component.get("v.ToDate");
        EndDate = component.get("v.EndDate");
        selectedsoffice = component.get("v.Salesoffice");
        var selectedSalesOfficeYear = component.get("v.salesOfficeSelectedYear");
        var selectedSalesOfficeMonth = component.get("v.SalesOfficeSelectedMonth");
        console.log('ToDate ' + JSON.stringify(ToDate));
        console.log('EndDate ' + JSON.stringify(EndDate));
        //Calling the Apex Function
        var action = component.get("c.fetchingRecords");         
        //Setting the Apex Parameter
        //
        
        action.setParams({'selectedMonth' : selectedSalesOfficeMonth, 'selectedYear' : selectedSalesOfficeYear ,'selectedsoffice' : selectedsoffice});
        action.setCallback(this,function(response){
            var state = response.getState();
            if(state == "SUCCESS") {
                component.set("v.salesOfficeList", response.getReturnValue());
            }
        });
        
        //adds the server-side action to the queue        
        $A.enqueueAction(action);
        
        
	}
,    
    searchRecordNames : function(component, event, helper) {
    	var action1 = component.get("c.salesOfficeNames");
        var inputsel = component.find("InputSelectDynamic");
        var opts=[];
        action1.setCallback(this, function(a) {
            for(var i=0;i< a.getReturnValue().length;i++){
                opts.push({"class": "optionClass", label: a.getReturnValue()[i], value: a.getReturnValue()[i]});
            }
            inputsel.set("v.options", opts);
        });
	
        $A.enqueueAction(action1); 
        }
,

    geSelectedMonth : function(component,event,helper)  {
        var action2 = component.get("c.selectedMonth");
        var inputselmonth = component.find("selectedMonthDynamic");
        var opts1=[];
        action2.setCallback(this, function(a){
        	for(var i=0;i< a.getReturnValue().length;i++){
                opts1.push({"class": "optionClass", label: a.getReturnValue()[i], value: a.getReturnValue()[i]});
            }
            inputselmonth.set("v.options", opts1); 
        });
        $A.enqueueAction(action2);
    }
    
,
	geSelectedYear : function(component,event,helper)  {
        var action3 = component.get("c.selectedYear");
        var inputselYear = component.find("selectedYearDynamic");
        var opts2=[];
        action3.setCallback(this, function(a){
        	for(var i=0;i< a.getReturnValue().length;i++){
                opts2.push({"class": "optionClass", label: a.getReturnValue()[i], value: a.getReturnValue()[i]});
            }
            inputselYear.set("v.options", opts2); 
        });
        $A.enqueueAction(action3);
    }    
})
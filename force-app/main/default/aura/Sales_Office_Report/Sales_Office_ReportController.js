({
	searchRecords : function(component, event, helper) {
		helper.searchRecordHelper(component, event, helper);
	}
,    
    doInit:function(component,event,helper){
        helper.searchRecordNames(component, event, helper);
        helper.geSelectedMonth(component, event, helper);   
        helper.geSelectedYear(component, event, helper);
	}
})
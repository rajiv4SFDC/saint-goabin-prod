({
    doInit : function(component, event, helper) {
        component.set("v.selectMyDayButton", false);
        component.set("v.checkInButton", true);
        component.set("v.checkOutButton", true);
        component.set("v.endMyDayButton", true);
        
        helper.showSpinner(component);
        var action = component.get("c.getCheckInOutRecords");
        action.setCallback(this, function(response) {  
            
            if (response.getState() == "SUCCESS") {
                
                var res11 = response.getReturnValue();
                var allReturnedResults = res11['CHECKIN_RECORDS'];
                component.set("v.recordsToDisplay", allReturnedResults);
                component.set("v.prospectOptions", res11['PROSPECTS']);
                if(allReturnedResults && allReturnedResults.length > 0) {
                    
                    component.set("v.selectMyDayButton", true);
                    component.set("v.checkInButton", false);
                    component.set("v.checkOutButton", true);
                    component.set("v.endMyDayButton", false);
                    
                    if(allReturnedResults[allReturnedResults.length - 1].CheckOut_Completed__c == false) {
                        
                        component.set("v.checkInButton", true);
                        component.set("v.endMyDayButton", true);
                    } 
                    
                    if(allReturnedResults[allReturnedResults.length - 1].Geo_Tagging__r.End_My_Day__Latitude__s) {
                        
                        component.set("v.checkInButton", true);
                        component.set("v.endMyDayButton", true);
                    }
                    
                } else {
                    
                    component.set("v.selectMyDayButton", false);
                    component.set("v.checkInButton", true);
                    component.set("v.checkOutButton", true);
                    component.set("v.endMyDayButton", true);
                }
            }
            
            helper.hideSpinner(component);
        });
        $A.enqueueAction(action);
    },
   
    // method for start my day
    startMyDay : function(component, event, helper) {
        
        helper.showSpinner(component);
        // check if user already created record for that particular day
        var checkGeoRecordExist = component.get("v.checkGeoRecordExist"); 
        if(checkGeoRecordExist == true) {
            
            helper.hideSpinner(component);
            alert('You already start day');
            return false;
        } else {
            
            console.log('coming inside');
            if (navigator.geolocation) {
                console.log('coming inside if');
                navigator.geolocation.getCurrentPosition(function(position) {
                    var crd = position.coords;
                    var lat,lon;
                    lat = `${crd.latitude}`;
                    lon = `${crd.longitude}`;
                    console.log('Your current position is1: ' + lat);
                    console.log('Your current position is2: ' + lon);
                    
                    component.set("v.startDayObjectList[0].Start_My_Day__latitude__s", lat);
                    component.set("v.startDayObjectList[0].Start_My_Day__longitude__s", lon);
                    
                    helper.callGeoTagRecords(component, event, helper);
                    
                },function(error){
                    console.log('coming inside esle');
                    var errorReason="";
                    switch(error.code) {
                        case error.PERMISSION_DENIED:
                            console.log("User denied the request for Geolocation.");
                            errorReason = "User denied the request for Geolocation";
                            break;
                        case error.POSITION_UNAVAILABLE:
                            console.log("Location information is unavailable.");
                            errorReason = "Location information is unavailable";
                            break;
                        case error.TIMEOUT:
                            console.log("The request to get user location timed out.");
                            errorReason = "The request to get user location timed out";
                            break;
                        case error.UNKNOWN_ERROR:
                            console.log("An unknown error occurred.");
                            errorReason = "An unknown error occurred";
                            break;
                    }
                });
                
            } else {
                
                helper.hideSpinner(component);
            }
        }
    },
    
    checkIn : function(component, event, helper) {
        
        helper.showSpinner(component);
        component.set("v.checkOutVariable",1);
        var checkInVariable = component.get("v.checkInVariable");
        if(checkInVariable == 1) {
            component.set("v.checkInVariable", checkInVariable + 1);
            
            if (navigator.geolocation) {
                console.log('coming inside if');
                navigator.geolocation.getCurrentPosition(function(position) {
                    var crd = position.coords;
                    var lat,lon;
                    lat = `${crd.latitude}`;
                    lon = `${crd.longitude}`;
                    console.log('Your current position is1: ' + lat);
                    console.log('Your current position is2: ' + lon);
                    
                    component.set("v.checkInObjectList[0].CheckIn__latitude__s", lat);
                    component.set("v.checkInObjectList[0].CheckIn__longitude__s", lon);
                    
                    window.setTimeout(
                        $A.getCallback(function() {
                            
                            helper.checkInRecords(component, event, helper);
                            
                        }), 2000
                    );
                                        
                },function(error){
                    console.log('coming inside esle');
                    var errorReason="";
                    switch(error.code) {
                        case error.PERMISSION_DENIED:
                            console.log("User denied the request for Geolocation.");
                            errorReason = "User denied the request for Geolocation";
                            break;
                        case error.POSITION_UNAVAILABLE:
                            console.log("Location information is unavailable.");
                            errorReason = "Location information is unavailable";
                            break;
                        case error.TIMEOUT:
                            console.log("The request to get user location timed out.");
                            errorReason = "The request to get user location timed out";
                            break;
                        case error.UNKNOWN_ERROR:
                            console.log("An unknown error occurred.");
                            errorReason = "An unknown error occurred";
                            break;
                    }
                });
                
            } else {
                
                helper.hideSpinner(component);
            }
        } else {
            helper.showSpinner(component);
            alert('You can not CheckIn again without Checkout');
            console.log('hi it is coming');
            return false;
        }
        
	},
    
    checkOut : function(component, event, helper) {
        
        //validate records
        var recordDetails = component.get("v.checkOutObjectList");
        //alert(JSON.stringify(recordDetails));
        if(recordDetails && recordDetails.length > 0) {
        	
            var reasonOfVisit = recordDetails[0].Reason_of_Visit__c;
            var mom = recordDetails[0].Minutes_of_Meeting__c;
            var acct = recordDetails[0].Account__c;
            var prospect = recordDetails[0].Prospect__c;
            //alert(JSON.stringify(acct)+'---'+acct.Id);
            if(!reasonOfVisit || !mom || (!acct && !prospect)) {
                
                helper.showMessage('Please enter mandatory fields (Reason of Visit, MOM, Account or Prospect)', 'error', 'dismissible', 5000);
                return;
            } else {
                
                if( (acct && acct.Id) && prospect) {
                    
                    helper.showMessage('Please enter either Account or Type not both', 'error', 'dismissible', 5000);
                	return;
                }
            }
            
        } else {
            
            helper.showMessage('Please enter mandatory fields', 'error', 'dismissible', 5000);
            return;
        }
        
        helper.showSpinner(component);
        component.set("v.checkInVariable", 1);
        //var checkOutVariable = component.get("v.checkOutVariable");
        if(true) {
            //component.set("v.checkOutVariable", checkOutVariable + 1);
            
            if (navigator.geolocation) {
                console.log('coming inside if');
                navigator.geolocation.getCurrentPosition(function(position) {
                    var crd = position.coords;
                    var lat,lon;
                    lat = `${crd.latitude}`;
                    lon = `${crd.longitude}`;
                    console.log('Your current position is1: ' + lat);
                    console.log('Your current position is2: ' + lon);
                    
                    component.set("v.checkOutObjectList[0].CheckOut__Latitude__s", lat);
                    component.set("v.checkOutObjectList[0].CheckOut__Longitude__s", lon);
                    
                    window.setTimeout(
                        $A.getCallback(function() {
                            
                            helper.checkOutRecords(component, event, helper);
                            
                        }), 2000
                    );
                    
                },function(error){
                    console.log('coming inside esle');
                    var errorReason="";
                    switch(error.code) {
                        case error.PERMISSION_DENIED:
                            console.log("User denied the request for Geolocation.");
                            errorReason = "User denied the request for Geolocation";
                            break;
                        case error.POSITION_UNAVAILABLE:
                            console.log("Location information is unavailable.");
                            errorReason = "Location information is unavailable";
                            break;
                        case error.TIMEOUT:
                            console.log("The request to get user location timed out.");
                            errorReason = "The request to get user location timed out";
                            break;
                        case error.UNKNOWN_ERROR:
                            console.log("An unknown error occurred.");
                            errorReason = "An unknown error occurred";
                            break;
                    }
                });
            } else {
                
                helper.hideSpinner(component);
            }
        } else {
            helper.hideSpinner(component);
            alert('You can not CheckOut again without CheckIn');
            console.log('hi it is coming');
            return false;
        }
	},
    
    endMyDay : function(component, event, helper) {
        
        helper.showSpinner(component);
        // check if user already created record for that particular day
        var checkGeoRecordExist = component.get("v.checkGeoRecordExist"); 
        if(checkGeoRecordExist == true) {
            
            helper.hideSpinner(component);
            alert('You already start day');
            return false;
        } else {
            
            console.log('coming inside');
            if (navigator.geolocation) {
                console.log('coming inside if');
                navigator.geolocation.getCurrentPosition(function(position) {
                    var crd = position.coords;
                    var lat,lon;
                    lat = `${crd.latitude}`;
                    lon = `${crd.longitude}`;
                    console.log('Your current position is1: ' + lat);
                    console.log('Your current position is2: ' + lon);
                    
                    component.set("v.endDayObjectList[0].End_My_Day__latitude__s", lat);
                    component.set("v.endDayObjectList[0].End_My_Day__longitude__s", lon);
                    
                    helper.endMyDayRecords(component, event, helper);
                    
                },function(error){
                    console.log('coming inside esle');
                    var errorReason="";
                    switch(error.code) {
                        case error.PERMISSION_DENIED:
                            console.log("User denied the request for Geolocation.");
                            errorReason = "User denied the request for Geolocation";
                            break;
                        case error.POSITION_UNAVAILABLE:
                            console.log("Location information is unavailable.");
                            errorReason = "Location information is unavailable";
                            break;
                        case error.TIMEOUT:
                            console.log("The request to get user location timed out.");
                            errorReason = "The request to get user location timed out";
                            break;
                        case error.UNKNOWN_ERROR:
                            console.log("An unknown error occurred.");
                            errorReason = "An unknown error occurred";
                            break;
                    }
                });
                
            } else {
                
                helper.hideSpinner(component);
            }
        }
	}
    
})
({
	callGeoTagRecords : function(component, event, helper) {
        
        var checkInOutData;
        checkInOutData = component.get("v.startDayObjectList");
        var action = component.get("c.createGeoTagRecords");
        action.setParams({'checkInData' : JSON.stringify(checkInOutData)});
        action.setCallback(this, function(response) { 
            
            if (response.getState() == "SUCCESS") {
                
                component.set("v.selectMyDayButton", true);
                component.set("v.checkInButton", false);
                component.set("v.checkOutButton", true);
                component.set("v.endMyDayButton", true);
                
                helper.showMessage('You have successfully LoggedIn', 'success', 'dismissible', 5000);
            }
            
            helper.hideSpinner(component);
        });
        $A.enqueueAction(action);
	},
    checkInRecords : function(component, event, helper) {
        
        helper.showSpinner(component);
        var checkInOutData;
        checkInOutData = component.get("v.checkInObjectList");
        var action = component.get("c.createcheckInRecords");
        action.setParams({'checkInData' : JSON.stringify(checkInOutData)});
        action.setCallback(this, function(response) {           
            if (response.getState() == "SUCCESS") {
                
                component.set("v.selectMyDayButton", true);
                component.set("v.checkInButton", true);
                component.set("v.checkOutButton", false);
                component.set("v.endMyDayButton", true);
                
                component.set("v.recordsToDisplay", response.getReturnValue());
                
                helper.showMessage('You have successfully CheckedIn', 'success', 'dismissible', 5000);
            }
            
            helper.hideSpinner(component);
        });
        $A.enqueueAction(action);
	},
    checkOutRecords : function(component, event, helper) {
        
        helper.showSpinner(component);
        var checkInOutData;
        checkInOutData = component.get("v.checkOutObjectList");
        var action = component.get("c.createcheckOutRecords");
        action.setParams({'checkInData' : JSON.stringify(checkInOutData)});
        action.setCallback(this, function(response) {  
            
            component.set('v.checkOutObjectList', []);            
            if (response.getState() == "SUCCESS") { 
                
                component.set("v.selectMyDayButton", true);
                component.set("v.checkInButton", false);
                component.set("v.checkOutButton", false);
                component.set("v.endMyDayButton", false);
                
                component.set("v.recordsToDisplay", response.getReturnValue());
				
				helper.showMessage('You have successfully CheckedOut', 'success', 'dismissible', 5000);                
            }
            
            helper.hideSpinner(component);
        });
        $A.enqueueAction(action);
	},
    
    endMyDayRecords : function(component, event, helper) {
        
        helper.showSpinner(component);
        var checkInOutData = component.get("v.endDayObjectList");
        var action = component.get("c.createEndDayRecords");
        action.setParams({'checkInData' : JSON.stringify(checkInOutData)});
        //alert(JSON.stringify(checkInOutData));
        action.setCallback(this, function(response) {  
            
            component.set("v.selectMyDayButton", true);
            component.set("v.checkInButton", true);
            component.set("v.checkOutButton", true);
            component.set("v.endMyDayButton", true);
            
            component.set("v.recordsToDisplay", response.getReturnValue());
            
            //alert(response.getState());
            if (response.getState() == "SUCCESS") {              
                
                helper.showMessage('You have successfully Logged Out', 'success', 'dismissible', 5000);
            }
            helper.hideSpinner(component);
        });
        $A.enqueueAction(action);
	},
    
    showSpinner:function(component){
        
        component.set("v.IsSpinner",true);
    },
    
    hideSpinner:function(component){
        
        component.set("v.IsSpinner",false);
    },
    
    showMessage:function(msg, msgtype, display_mode, duration){
        
        if(!duration) {
            
            duration = 5000;
        }
        //display_mode: dismissible, pester, sticky 
        var toastEvent = $A.get("e.force:showToast");
        toastEvent.setParams({
            message: msg,
            messageTemplate: msg,
            duration:duration,
            key: 'info_alt',
            type: msgtype,
            mode: display_mode
        });
        toastEvent.fire();
    },
    
    getGeoLocation:function(component, event, helper){
        
        
    }
})
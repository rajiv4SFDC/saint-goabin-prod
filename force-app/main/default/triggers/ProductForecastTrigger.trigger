trigger ProductForecastTrigger on Product_Forecast__c (after insert) {

    new ProductForecastTriggerHandler().run();
}
trigger UpdateCommentSection on Price_Request__c (after update) {
    if(Trigger.isAfter && Trigger.isUpdate){
         if(UpdateCommentSectionHepler.isFirstRun()){ 
         	UpdateCommentSectionHepler.callMainMethod(Trigger.new);
         }
    }
}
trigger PriceRequestPandpTrigger on price_request_PandP__c(before insert,before update) {

   if(trigger.isInsert && trigger.Isbefore){
        PriceRequestPandpTriggerHandler.ValidateSpecialRequestRec(trigger.new,null);

        PriceRequestPandpTriggerHandler.updatereference(trigger.new);
        PriceRequestPandpTriggerHandler.updatePriceManager(Trigger.new);
        PriceRequestPandpTriggerHandler.beforeInsertUpdatePrice(Trigger.new);
        PriceRequestPandpTriggerHandler.doCheckForBeforeUpdate(Trigger.new,Trigger.oldMap);
        PriceRequestPandpTriggerHandler.beforeInsertValues(trigger.new);
        
       // Remove this code after code ref-factor. 
       price_request_PandP__c pr = new price_request_PandP__c();
        pr.Price_Type__c   = 'Tier Price';        
        pr.Valid_From__c = System.today();
        pr.Valid_To__c = System.today();
        pr.Requested_Quantity__c = 1000;
        pr.Approval_status__c ='Approved';
        pr.Regional_head_sales_and_marketing_User__c = userInfo.getUserId();
        pr.Regional_Manager__c =  userInfo.getUserId();
        

   }
   
   if(trigger.isUpdate && trigger.Isbefore){
     //   new PriceRequestTriggerHandler().run();
   //  PriceRequestPandpTriggerHandler.updatePriceManager(Trigger.new);
     PriceRequestPandpTriggerHandler.doCheckForBeforeUpdate(Trigger.new,Trigger.oldMap);
        PriceRequestPandpTriggerHandler.updatereference(trigger.new);
        PriceRequestPandpTriggerHandler.beforeInsertUpdatePrice(Trigger.new);
         PriceRequestPandpTriggerHandler.ValidateSpecialRequestRec(trigger.new,trigger.oldMap);
   }
        
   }
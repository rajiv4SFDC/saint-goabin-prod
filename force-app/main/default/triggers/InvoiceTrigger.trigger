trigger InvoiceTrigger on Invoice__c (after insert,before insert,before update,after update) {
    
    if(trigger.isAfter && trigger.isinsert) {
        //InvoiceTriggerHandler.updateAccountForecastForInsert(trigger.new);
        //System.enqueuejob(new  QueueClassForInvoiceInsert(Trigger.new));
        InvoiceTriggerHandler.updateInvDateInAccount(Trigger.new);
        
        
        //Queueable apex for insertion
        //System.enqueuejob(new  TestQueueableApexForInsertion(Trigger.new));
        
       // InvoiceBatchHandlerForInsert invBatch = new InvoiceBatchHandlerForInsert(trigger.new);
        //Database.executeBatch(invBatch);
        
        
    }
    
    if(trigger.isbefore && trigger.isinsert || trigger.isupdate && trigger.isbefore) {
        InvoiceTriggerHandler.updatePriceReq(Trigger.new,Trigger.oldmap);
    }
    
    if(trigger.isUpdate && trigger.isAfter){
        //InvoiceTriggerHandler.updateAccountForecastForUpdate(trigger.new, trigger.oldMap); 
        //System.enqueuejob(new  QueueClassForInvoiceUpdate(Trigger.new, trigger.oldMap));
        
        
        //Queueable apex for insertion
        //System.enqueuejob(new  TestQueueClassForInvoiceUpdate(Trigger.new, trigger.oldMap));
        
        
        
       // InvoiceBatchHandlerForUpdate invBatch = new InvoiceBatchHandlerForUpdate(trigger.new, trigger.oldMap);
       // Database.executeBatch(invBatch);
        
    }
}
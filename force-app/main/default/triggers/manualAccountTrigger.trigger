trigger manualAccountTrigger on Manual_Accounting__c (before insert,before update,after insert,after update,after delete) {

if(trigger.isBefore && trigger.isInsert)
ManualAccountingTriggerHandler.beforeInsertHandler(trigger.new);

if(trigger.isAfter && trigger.IsInsert)
ManualAccountingTriggerHandler.afterInsertHandler(trigger.new);

if(trigger.isBefore && trigger.isUpdate)
ManualAccountingTriggerHandler.beforeUpdateHandler(trigger.new,trigger.OldMap);

if(trigger.isAfter && trigger.isUpdate)
ManualAccountingTriggerHandler.afterUpdateHandler(trigger.new,trigger.OldMap);

if(trigger.isAfter && trigger.isDelete)
ManualAccountingTriggerHandler.afterDeleteHandler(trigger.old);

//system.debug('The trigger is firing raja');
}
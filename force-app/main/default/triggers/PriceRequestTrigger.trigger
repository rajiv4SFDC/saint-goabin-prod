trigger PriceRequestTrigger on Price_Request__c (before insert,before update) {

   if(trigger.isInsert && trigger.Isbefore){
        new PriceRequestTriggerHandler().run();
        PriceRequestTriggerHandler1.updatereference(trigger.new);
        PriceRequestTriggerHandler1.updatePriceManager(Trigger.new);
        PriceRequestTriggerHandler1.doCheckForBeforeUpdate(Trigger.new,Trigger.oldMap);
   }
   
   if(trigger.isUpdate && trigger.Isbefore){
     //   new PriceRequestTriggerHandler().run();
   //  PriceRequestTriggerHandler1.updatePriceManager(Trigger.new);
     PriceRequestTriggerHandler1.doCheckForBeforeUpdate(Trigger.new,Trigger.oldMap);
        PriceRequestTriggerHandler1.updatereference(trigger.new);
   }
        
   }
trigger TriggerOnMaterialRequest on Material_Request__c (before Update,after insert, after update) {
    if(Trigger.isAfter && Trigger.isInsert) {
        ProductDimensionInsertion.dimensionFetch(Trigger.new);
    }
    if(Trigger.isAfter && Trigger.isUpdate) {
        ProductDimensionInsertion.dimensionFetch(Trigger.new);
    }
    
    if(Trigger.isBefore && Trigger.isUpdate) {
        MaterailRequestHandler.Fieldupdate(Trigger.new,trigger.oldmap);
    }
}
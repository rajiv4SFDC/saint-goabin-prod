trigger RequestItemTrigger on Request_Item__c (before insert,before update,after update) {

    new RequestItemTriggerHandler().run();
}
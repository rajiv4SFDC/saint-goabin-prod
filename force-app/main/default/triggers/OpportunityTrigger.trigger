trigger OpportunityTrigger on Opportunity (before insert,before update,after insert,after update) {
    if(Label.OpportunityTrigger=='YES'){
        new OpportunityTriggerHandler().run();
   }
}
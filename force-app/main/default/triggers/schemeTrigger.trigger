trigger schemeTrigger on Schemes__c (before insert, before update) {
    if(Trigger.isBefore && Trigger.isInsert)
        SchemeHandler.restrictInsertion(trigger.new);
}
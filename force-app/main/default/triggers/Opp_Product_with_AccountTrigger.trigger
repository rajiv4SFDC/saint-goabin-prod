trigger Opp_Product_with_AccountTrigger on Opportunity_Product_with_Account__c (before insert,after insert,after update) {

if(trigger.isAfter && trigger.isInsert)
Opp_Product_with_AccountTrigger_Handler.afterInsertHandler(Trigger.New);

if(trigger.isAfter && trigger.isUpdate)
Opp_Product_with_AccountTrigger_Handler.afterUpdateHandler(Trigger.new,Trigger.OldMap);
}
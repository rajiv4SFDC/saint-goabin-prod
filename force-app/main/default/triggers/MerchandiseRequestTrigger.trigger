trigger MerchandiseRequestTrigger on Merchandise_Request__c (before insert,after Update) {

	new MerchandiseRequestTriggerHandler().run();
}
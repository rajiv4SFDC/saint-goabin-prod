trigger PlantVisitTrigger on Plant_Visit__c (before insert, before update) {
	new PlantVisitTriggerHandler().run();
}
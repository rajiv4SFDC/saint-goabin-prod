trigger AccountForecastTrigger on Account_Forecast__c (before insert) {

  if(Trigger.isbefore && trigger.isInsert){
     Accountforecasttriggerhandler.handleaccountforecast(Trigger.new);
  }

}
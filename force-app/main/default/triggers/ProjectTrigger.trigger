trigger ProjectTrigger on Project__c (before update) {
    
    if(trigger.isBefore && trigger.isUpdate)
        ProjectTriggerHandler.beforeUpdateHandler(Trigger.new, Trigger.OldMap);
}
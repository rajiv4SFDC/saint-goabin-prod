trigger MockUpRequestTrigger on Mock_Up_Request__c (before insert) {
  new MockUpRequestTriggerHandler().run();
}
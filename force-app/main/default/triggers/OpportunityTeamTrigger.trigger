trigger OpportunityTeamTrigger on OpportunityTeamMember (before insert,before update,
                                                         after insert,after update,before delete,after delete) {
    new OpportunityTeamTriggerHandler().run();
}